#include "/mnt/d/lhcb/B2XcXc/inc/tools.h"

void printRatioBF(){
	TString fNameEff_Bd2DsD = "/mnt/d/lhcb/B2XcXc/Documents/effHists/Bd2DsD_eff_Tot.root";
	TString fNameEff_Bs2DsDs = "/mnt/d/lhcb/B2XcXc/Documents/effHists/Bs2DsDs_eff_Tot.root";

	vector<ValError> eff_Bd2DsD = readVEFromFile(fNameEff_Bd2DsD, "effs");
	vector<ValError> eff_Bs2DsDs = readVEFromFile(fNameEff_Bs2DsDs, "effs");

	TString fNameYields_Bd2DsD = "/mnt/d/lhcb/B2XcXc/Documents/yields/final_fit/yields_Bd2DsD.root";
	TString fNameYields_Bs2DsDs = "/mnt/d/lhcb/B2XcXc/Documents/yields/final_fit/yields_Bs2DsDs.root";

	vector<ValError> yields_Bd2DsD = readVEFromFile(fNameYields_Bd2DsD, "yields");
	vector<ValError> yields_Bs2DsDs = readVEFromFile(fNameYields_Bs2DsDs, "yields");

	ValError bf_vis_Bd2DsD(3.60e-5, 0.40e-5);
	ValError bf_vis_Bs2DsDs(1.27e-5, 0.15e-5);
	ValError PDG_ratio = bf_vis_Bd2DsD / bf_vis_Bs2DsDs;

	cout<< "PDGValue: " << PDG_ratio.roundToError(0, 2) << endl << endl;

	vector<ValError> B_PT(9, ValError(10.5, 0));
	//B_PT[0] = ValError(0, 0);		//2011
	//B_PT[1] = ValError(0, 0);		//2012
	//B_PT[2] = ValError(0, 0);		//1112
	B_PT[3] = ValError(14.1565, 0.050049);		//2015
	B_PT[4] = ValError(14.2148, 0.247841);		//2016
	B_PT[5] = ValError(14.1387, 0.070455);		//1516
	B_PT[6] = ValError(13.9367, 0.0924303);		//2017
	B_PT[7] = ValError(14.1369, 0.0895765);		//2018
	B_PT[8] = ValError(14.3356, 0.0838552);		//Run2

	//ValError fs_fd(0.2539, 0.0079);
	vector<ValError> fs_fd(B_PT.size());
	// fs_fd under 13TeV
	ValError fs_fd_0(0.263, 0.008);
	ValError fs_fd_shift(-17.6e-4, 2.1e-4);

	for(int i=0; i<B_PT.size(); i++){
		fs_fd[i] = fs_fd_0 + fs_fd_shift * B_PT[i];
	}

	vector<ValError> ratio(B_PT.size());

	system("mkdir -p /mnt/d/lhcb/B2XcXc/public_html/BF_ratio_norm/");
	TString sLog = "/mnt/d/lhcb/B2XcXc/public_html/BF_ratio_norm/log.dat";
	ofstream out(sLog);
	int w(15);

	//out<<setw(40)<<" "<<setw(w)<<"& 2011 "<<setw(w)<<"& 2012 "<<setw(w)<<"& Run1 "<<setw(w)<<"& 2015 "<<setw(w)<<"& 2016 "<<setw(w)<<"& 2017 "<<setw(w)<<"& 2018 "<<setw(w)<<"& Run2 \\\\" << endl;
	out<<setw(40)<<" "<<setw(w)<<"& 2015 "<<setw(w)<<"& 2016 "<<setw(w)<<"& 2017 "<<setw(w)<<"& 2018 "<<setw(w)<<"& Run2 \\\\" << endl;



	out<<setw(40)<<"$N_{\\BdDsD}$";
	//for(int i=0; i<B_PT.size(); i++){
	for(int i=3; i<B_PT.size(); i++){
		out<<setw(w)<<"& "<<"$"<<(int)yields_Bd2DsD[i].val<<"\\pm"<<(int)yields_Bd2DsD[i].err<<"$";
	}
	out<<"\\\\"<<endl;


	out<<setw(40)<<"$N_{\\BsDsDs}$";
	//for(int i=0; i<B_PT.size(); i++){
	for(int i=3; i<B_PT.size(); i++){
		out<<setw(w)<<"& "<<"$"<<(int)yields_Bs2DsDs[i].val<<"\\pm"<<(int)yields_Bs2DsDs[i].err<<"$";
	}
	out<<"\\\\"<<endl;

	out<<"\\midrule"<<endl;


	out<<setw(40)<<"$\\varepsilon_{\\BdDsD}$";
	//for(int i=0; i<B_PT.size(); i++){
	for(int i=3; i<B_PT.size(); i++){
		out<<setw(w)<<"& "<<"$"<<dbl2str(100*eff_Bd2DsD[i].val,3)<<"\\pm"<<dbl2str(100*eff_Bd2DsD[i].err,3)<<"$";
	}
	out<<"\\\\"<<endl;


	out<<setw(40)<<"$\\varepsilon_{\\BsDsDs}$";
	//for(int i=0; i<B_PT.size(); i++){
	for(int i=3; i<B_PT.size(); i++){
		out<<setw(w)<<"& "<<"$"<<dbl2str(100*eff_Bs2DsDs[i].val,3)<<"\\pm"<<dbl2str(100*eff_Bs2DsDs[i].err,3)<<"$";
	}
	out<<"\\\\"<<endl;

	out<<"\\midrule"<<endl;


	out<<setw(40)<<"$<B_{p_T}> (\\gev)$";
	//for(int i=0; i<B_PT.size(); i++){
	for(int i=3; i<B_PT.size(); i++){
		out<<setw(w)<<"& "<<"$"<<dbl2str(B_PT[i].val,2)<<"\\pm"<<dbl2str(B_PT[i].err,2)<<"$";
	}
	out<<"\\\\"<<endl;

	out<<setw(40)<<"$f_s/f_d$";
	//for(int i=0; i<B_PT.size(); i++){
	for(int i=3; i<B_PT.size(); i++){
		out<<setw(w)<<"& "<<"$"<<dbl2str(fs_fd[i].val,3)<<"\\pm"<<dbl2str(fs_fd[i].err,3)<<"$";
	}
	out<<"\\\\"<<endl;

	out<<"\\midrule"<<endl;


	out<<setw(40)<<"$\\frac{\\BF(\\BdDsD)}{\\BF(\\BsDsDs)}$";
	//for(int i=0; i<B_PT.size(); i++){
	for(int i=3; i<B_PT.size(); i++){
		ratio[i] = fs_fd[i] * (yields_Bd2DsD[i]/eff_Bd2DsD[i])/(yields_Bs2DsDs[i]/eff_Bs2DsDs[i]);
		out<<setw(w)<<"& "<<"$"<<dbl2str(ratio[i].val,2)<<"\\pm"<<dbl2str(ratio[i].err,2)<<"$";
	}
	out<<"\\\\"<<endl;

	out<<"\\midrule"<<endl;


	out<<setw(40)<<"Deviation form PDG";
	//for(int i=0; i<B_PT.size(); i++){
	for(int i=3; i<B_PT.size(); i++){
		double sigma = sqrt(ratio[i].err*ratio[i].err  +  PDG_ratio.err*PDG_ratio.err);
		double deviation = ratio[i].val - PDG_ratio.val;
		double nSigma = fabs(deviation/sigma);

		out<<setw(w)<<"& "<<"$"<<dbl2str(nSigma,1)<<"\\sigma$";
	}
	out<<"\\\\"<<endl;



	system("cat " + sLog);
	cout << endl;
	//cout << "output ratio into " + sLog << endl;

	gROOT->ProcessLine(".q");
}

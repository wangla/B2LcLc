#include "/mnt/d/lhcb/B2XcXc/inc/histoTools.h"
#include "/mnt/d/lhcb/B2XcXc/inc/getPresel.h"

void compareDataMC(){

	lhcbStyle(0.0603);

	vector<TString> treeNames;
	vector<TString> drawTitles;
	vector<TString> cuts;
	vector<TString> weights;
	vector<int> colors;
	vector<TString> drawOptions;

	bool draw[4] = {1, 0, 1, 0};

	vector<int> years;
	//years.push_back(2011);
	//years.push_back(2012);
	//years.push_back(1112);
	years.push_back(2);
	//years.push_back(2015);
	//years.push_back(2016);
	//years.push_back(1516);
	//years.push_back(2017);
	//years.push_back(2018);

	for(int i=0; i<years.size(); i++){
		int year = years[i];

        TString yearCut = "(1)";
        if(years[i] == 1112) yearCut = "(year == 2011 || year==2012)";
        else if(years[i] == 2) yearCut = "(year == 2015 || year==2016 || year == 2017 || year==2018)";
        else if(years[i] == 1516) yearCut = "(year == 2015 || year==2016)";
        else yearCut = "(year == " +i2s(years[i])+")";


		vector<double> legPos = {0, 0, 0, 0};
		//vector<double> legPos = {0.5, 0.5, 0.98, 0.95};
		//vector<double> legPos = {0.25, 0.50, 0.65, 0.90};



		vector<Histo1DWithInfo> Bs2LcLc_HistoInfos;
		Bs2LcLc_HistoInfos.push_back(Histo1DWithInfo("angle_p_Xc_plus_Kminus_Xc_plus", "angle_p_Xc_plus_Kminus_Xc_plus", "angle_p_Xc_plus_Kminus_Xc_plus", 0, 0.004, 50, false));
		Bs2LcLc_HistoInfos.push_back(Histo1DWithInfo("angle_p_Xc_plus_piplus_Xc_plus", "angle_p_Xc_plus_piplus_Xc_plus", "angle_p_Xc_plus_piplus_Xc_plus", 0, 0.004, 50, false));
		Bs2LcLc_HistoInfos.push_back(Histo1DWithInfo("angle_piplus_Xc_plus_Kminus_Xc_plus", "angle_piplus_Xc_plus_Kminus_Xc_plus", "angle_piplus_Xc_plus_Kminus_Xc_plus", 0, 0.004, 50, false));
		Bs2LcLc_HistoInfos.push_back(Histo1DWithInfo("angle_pbar_Xc_minus_Kplus_Xc_minus", "angle_pbar_Xc_minus_Kplus_Xc_minus", "angle_pbar_Xc_minus_Kplus_Xc_minus", 0, 0.004, 50, false));
		Bs2LcLc_HistoInfos.push_back(Histo1DWithInfo("angle_pbar_Xc_minus_piminus_Xc_minus", "angle_pbar_Xc_minus_piminus_Xc_minus", "angle_pbar_Xc_minus_piminus_Xc_minus", 0, 0.004, 50, false));
		Bs2LcLc_HistoInfos.push_back(Histo1DWithInfo("angle_piminus_Xc_minus_Kplus_Xc_minus", "angle_piminus_Xc_minus_Kplus_Xc_minus", "angle_piminus_Xc_minus_Kplus_Xc_minus", 0, 0.004, 50, false));
		Bs2LcLc_HistoInfos.push_back(Histo1DWithInfo("angle_p_Xc_plus_pbar_Xc_minus", "angle_p_Xc_plus_pbar_Xc_minus", "angle_p_Xc_plus_pbar_Xc_minus", 0, 0.004, 50, false));
		Bs2LcLc_HistoInfos.push_back(Histo1DWithInfo("angle_p_Xc_plus_Kplus_Xc_minus", "angle_p_Xc_plus_Kplus_Xc_minus", "angle_p_Xc_plus_Kplus_Xc_minus", 0, 0.004, 50, false));
		Bs2LcLc_HistoInfos.push_back(Histo1DWithInfo("angle_p_Xc_plus_piminus_Xc_minus", "angle_p_Xc_plus_piminus_Xc_minus", "angle_p_Xc_plus_piminus_Xc_minus", 0, 0.004, 50, false));
		Bs2LcLc_HistoInfos.push_back(Histo1DWithInfo("angle_Kminus_Xc_plus_pbar_Xc_minus", "angle_Kminus_Xc_plus_pbar_Xc_minus", "angle_Kminus_Xc_plus_pbar_Xc_minus", 0, 0.004, 50, false));
		Bs2LcLc_HistoInfos.push_back(Histo1DWithInfo("angle_Kminus_Xc_plus_Kplus_Xc_minus", "angle_Kminus_Xc_plus_Kplus_Xc_minus", "angle_Kminus_Xc_plus_Kplus_Xc_minus", 0, 0.004, 50, false));
		Bs2LcLc_HistoInfos.push_back(Histo1DWithInfo("angle_Kminus_Xc_plus_piminus_Xc_minus", "angle_Kminus_Xc_plus_piminus_Xc_minus", "angle_Kminus_Xc_plus_piminus_Xc_minus", 0, 0.004, 50, false));
		Bs2LcLc_HistoInfos.push_back(Histo1DWithInfo("angle_piplus_Xc_plus_pbar_Xc_minus", "angle_piplus_Xc_plus_pbar_Xc_minus", "angle_piplus_Xc_plus_pbar_Xc_minus", 0, 0.004, 50, false));
		Bs2LcLc_HistoInfos.push_back(Histo1DWithInfo("angle_piplus_Xc_plus_Kplus_Xc_minus", "angle_piplus_Xc_plus_Kplus_Xc_minus", "angle_piplus_Xc_plus_Kplus_Xc_minus", 0, 0.004, 50, false));
		Bs2LcLc_HistoInfos.push_back(Histo1DWithInfo("angle_piplus_Xc_plus_piminus_Xc_minus", "angle_piplus_Xc_plus_piminus_Xc_minus", "angle_piplus_Xc_plus_piminus_Xc_minus", 0, 0.004, 50, false));
		Bs2LcLc_HistoInfos.push_back(Histo1DWithInfo("p_Xc_plus_MC15TuneV1_ProbNNp", "p_Xc_plus_MC15TuneV1_ProbNNp", "p_Xc_plus_MC15TuneV1_ProbNNp", 0.1, 1, 50, false));
		Bs2LcLc_HistoInfos.push_back(Histo1DWithInfo("Kminus_Xc_plus_MC15TuneV1_ProbNNk", "Kminus_Xc_plus_MC15TuneV1_ProbNNk", "Kminus_Xc_plus_MC15TuneV1_ProbNNk", 0.1, 1, 50, false));
		Bs2LcLc_HistoInfos.push_back(Histo1DWithInfo("piplus_Xc_plus_MC15TuneV1_ProbNNpi", "piplus_Xc_plus_MC15TuneV1_ProbNNpi", "piplus_Xc_plus_MC15TuneV1_ProbNNpi", 0.1, 1, 50, false));
		Bs2LcLc_HistoInfos.push_back(Histo1DWithInfo("pbar_Xc_minus_MC15TuneV1_ProbNNp", "pbar_Xc_minus_MC15TuneV1_ProbNNp", "pbar_Xc_minus_MC15TuneV1_ProbNNp", 0.1, 1, 50, false));
		Bs2LcLc_HistoInfos.push_back(Histo1DWithInfo("Kplus_Xc_minus_MC15TuneV1_ProbNNk", "Kplus_Xc_minus_MC15TuneV1_ProbNNk", "Kplus_Xc_minus_MC15TuneV1_ProbNNk", 0.1, 1, 50, false));
		Bs2LcLc_HistoInfos.push_back(Histo1DWithInfo("piminus_Xc_minus_MC15TuneV1_ProbNNpi", "piminus_Xc_minus_MC15TuneV1_ProbNNpi", "piminus_Xc_minus_MC15TuneV1_ProbNNpi", 0.1, 1, 50, false));
		Bs2LcLc_HistoInfos.push_back(Histo1DWithInfo("prodProbNNx", getProdProbNNx("Bs2LcLc"), "prodProbNNx", 0.05, 1, 50, false));
		Bs2LcLc_HistoInfos.push_back(Histo1DWithInfo("log_B_IPCHI2_OWNPV", "log(B_IPCHI2_OWNPV)", "log(B_IPCHI2_OWNPV)", -6, 4, 50, false));
		Bs2LcLc_HistoInfos.push_back(Histo1DWithInfo("B_PT", "B_PT", "B_PT", 0, 5e4, 50, false));
		Bs2LcLc_HistoInfos.push_back(Histo1DWithInfo("B_ETA", "B_ETA", "B_ETA", 2, 5, 50, false));
		Bs2LcLc_HistoInfos.push_back(Histo1DWithInfo("nTracks", "nTracks", "nTracks", 0, 500, 50, false));
		Bs2LcLc_HistoInfos.push_back(Histo1DWithInfo("pbar_Xc_minus_PT", "pbar_Xc_minus_PT", "pbar_Xc_minus_PT", 0, 13000, 50, false));
		Bs2LcLc_HistoInfos.push_back(Histo1DWithInfo("Kplus_Xc_minus_PT", "Kplus_Xc_minus_PT", "Kplus_Xc_minus_PT", 0, 13000, 50, false));
		Bs2LcLc_HistoInfos.push_back(Histo1DWithInfo("piminus_Xc_minus_PT", "piminus_Xc_minus_PT", "piminus_Xc_minus_PT", 0, 13000, 50, false));
		Bs2LcLc_HistoInfos.push_back(Histo1DWithInfo("p_Xc_plus_PT", "p_Xc_plus_PT", "p_Xc_plus_PT", 0, 13000, 50, false));
		Bs2LcLc_HistoInfos.push_back(Histo1DWithInfo("Kminus_Xc_plus_PT", "Kminus_Xc_plus_PT", "Kminus_Xc_plus_PT", 0, 13000, 50, false));
		Bs2LcLc_HistoInfos.push_back(Histo1DWithInfo("piplus_Xc_plus_PT", "piplus_Xc_plus_PT", "piplus_Xc_plus_PT", 0, 13000, 50, false));
		Bs2LcLc_HistoInfos.push_back(Histo1DWithInfo("p_Xc_plus_ETA", "p_Xc_plus_ETA", "p_Xc_plus_ETA", 1.5, 5.5, 50, false));
		Bs2LcLc_HistoInfos.push_back(Histo1DWithInfo("Kminus_Xc_plus_ETA", "Kminus_Xc_plus_ETA", "Kminus_Xc_plus_ETA", 1.5, 5.5, 50, false));
		Bs2LcLc_HistoInfos.push_back(Histo1DWithInfo("piplus_Xc_plus_ETA", "piplus_Xc_plus_ETA", "piplus_Xc_plus_ETA", 1.5, 5.5, 50, false));
		Bs2LcLc_HistoInfos.push_back(Histo1DWithInfo("pbar_Xc_minus_ETA", "pbar_Xc_minus_ETA", "pbar_Xc_minus_ETA", 1.5, 5.5, 50, false));
		Bs2LcLc_HistoInfos.push_back(Histo1DWithInfo("Kplus_Xc_minus_ETA", "Kplus_Xc_minus_ETA", "Kplus_Xc_minus_ETA", 1.5, 5.5, 50, false));
		Bs2LcLc_HistoInfos.push_back(Histo1DWithInfo("piminus_Xc_minus_ETA", "piminus_Xc_minus_ETA", "piminus_Xc_minus_ETA", 1.5, 5.5, 50, false));


		vector<Histo1DWithInfo> Bs2LcLc_HistoInfos_corr;
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_p_Xc_plus_Kminus_Xc_plus", "angle_p_Xc_plus_Kminus_Xc_plus", "angle_p_Xc_plus_Kminus_Xc_plus", 0, 0.004, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_p_Xc_plus_piplus_Xc_plus", "angle_p_Xc_plus_piplus_Xc_plus", "angle_p_Xc_plus_piplus_Xc_plus", 0, 0.004, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piplus_Xc_plus_Kminus_Xc_plus", "angle_piplus_Xc_plus_Kminus_Xc_plus", "angle_piplus_Xc_plus_Kminus_Xc_plus", 0, 0.004, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_pbar_Xc_minus_Kplus_Xc_minus", "angle_pbar_Xc_minus_Kplus_Xc_minus", "angle_pbar_Xc_minus_Kplus_Xc_minus", 0, 0.004, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_pbar_Xc_minus_piminus_Xc_minus", "angle_pbar_Xc_minus_piminus_Xc_minus", "angle_pbar_Xc_minus_piminus_Xc_minus", 0, 0.004, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piminus_Xc_minus_Kplus_Xc_minus", "angle_piminus_Xc_minus_Kplus_Xc_minus", "angle_piminus_Xc_minus_Kplus_Xc_minus", 0, 0.004, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_p_Xc_plus_pbar_Xc_minus", "angle_p_Xc_plus_pbar_Xc_minus", "angle_p_Xc_plus_pbar_Xc_minus", 0, 0.004, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_p_Xc_plus_Kplus_Xc_minus", "angle_p_Xc_plus_Kplus_Xc_minus", "angle_p_Xc_plus_Kplus_Xc_minus", 0, 0.004, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_p_Xc_plus_piminus_Xc_minus", "angle_p_Xc_plus_piminus_Xc_minus", "angle_p_Xc_plus_piminus_Xc_minus", 0, 0.004, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kminus_Xc_plus_pbar_Xc_minus", "angle_Kminus_Xc_plus_pbar_Xc_minus", "angle_Kminus_Xc_plus_pbar_Xc_minus", 0, 0.004, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kminus_Xc_plus_Kplus_Xc_minus", "angle_Kminus_Xc_plus_Kplus_Xc_minus", "angle_Kminus_Xc_plus_Kplus_Xc_minus", 0, 0.004, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kminus_Xc_plus_piminus_Xc_minus", "angle_Kminus_Xc_plus_piminus_Xc_minus", "angle_Kminus_Xc_plus_piminus_Xc_minus", 0, 0.004, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piplus_Xc_plus_pbar_Xc_minus", "angle_piplus_Xc_plus_pbar_Xc_minus", "angle_piplus_Xc_plus_pbar_Xc_minus", 0, 0.004, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piplus_Xc_plus_Kplus_Xc_minus", "angle_piplus_Xc_plus_Kplus_Xc_minus", "angle_piplus_Xc_plus_Kplus_Xc_minus", 0, 0.004, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piplus_Xc_plus_piminus_Xc_minus", "angle_piplus_Xc_plus_piminus_Xc_minus", "angle_piplus_Xc_plus_piminus_Xc_minus", 0, 0.004, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("p_Xc_plus_MC15TuneV1_ProbNNp_corr", "p_Xc_plus_MC15TuneV1_ProbNNp_corr", "p_Xc_plus_MC15TuneV1_ProbNNp", 0.1, 1, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Xc_plus_MC15TuneV1_ProbNNk_corr", "Kminus_Xc_plus_MC15TuneV1_ProbNNk_corr", "Kminus_Xc_plus_MC15TuneV1_ProbNNk", 0.1, 1, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("piplus_Xc_plus_MC15TuneV1_ProbNNpi_corr", "piplus_Xc_plus_MC15TuneV1_ProbNNpi", "piplus_Xc_plus_MC15TuneV1_ProbNNpi", 0.1, 1, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("pbar_Xc_minus_MC15TuneV1_ProbNNp_corr", "pbar_Xc_minus_MC15TuneV1_ProbNNp_corr", "pbar_Xc_minus_MC15TuneV1_ProbNNp", 0.1, 1, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("Kplus_Xc_minus_MC15TuneV1_ProbNNk_corr", "Kplus_Xc_minus_MC15TuneV1_ProbNNk_corr", "Kplus_Xc_minus_MC15TuneV1_ProbNNk", 0.1, 1, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("piminus_Xc_minus_MC15TuneV1_ProbNNpi_corr", "piminus_Xc_minus_MC15TuneV1_ProbNNpi", "piminus_Xc_minus_MC15TuneV1_ProbNNpi", 0.1, 1, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("prodProbNNx_corr", "prodProbNNx", "prodProbNNx", 0.05, 1, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("log_B_IPCHI2_OWNPV", "log(B_IPCHI2_OWNPV)", "log(B_IPCHI2_OWNPV)", -6, 4, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("B_PT", "B_PT", "B_PT", 0, 5e4, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("B_ETA", "B_ETA", "B_ETA", 2, 5, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("nTracks", "nTracks", "nTracks", 0, 500, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("pbar_Xc_minus_PT", "pbar_Xc_minus_PT", "pbar_Xc_minus_PT", 0, 13000, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("Kplus_Xc_minus_PT", "Kplus_Xc_minus_PT", "Kplus_Xc_minus_PT", 0, 13000, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("piminus_Xc_minus_PT", "piminus_Xc_minus_PT", "piminus_Xc_minus_PT", 0, 13000, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("p_Xc_plus_PT", "p_Xc_plus_PT", "p_Xc_plus_PT", 0, 13000, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Xc_plus_PT", "Kminus_Xc_plus_PT", "Kminus_Xc_plus_PT", 0, 13000, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("piplus_Xc_plus_PT", "piplus_Xc_plus_PT", "piplus_Xc_plus_PT", 0, 13000, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("p_Xc_plus_ETA", "p_Xc_plus_ETA", "p_Xc_plus_ETA", 1.5, 5.5, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Xc_plus_ETA", "Kminus_Xc_plus_ETA", "Kminus_Xc_plus_ETA", 1.5, 5.5, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("piplus_Xc_plus_ETA", "piplus_Xc_plus_ETA", "piplus_Xc_plus_ETA", 1.5, 5.5, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("pbar_Xc_minus_ETA", "pbar_Xc_minus_ETA", "pbar_Xc_minus_ETA", 1.5, 5.5, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("Kplus_Xc_minus_ETA", "Kplus_Xc_minus_ETA", "Kplus_Xc_minus_ETA", 1.5, 5.5, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("piminus_Xc_minus_ETA", "piminus_Xc_minus_ETA", "piminus_Xc_minus_ETA", 1.5, 5.5, 50, false));


		treeNames.clear();
		drawTitles.clear();
		cuts.clear();
		weights.clear();
		colors.clear();
		drawOptions.clear();


		//drawTitles.push_back("DATA Sideband");
		//drawOptions.push_back("E1");
		drawTitles.push_back("DATA Sideband");
		drawOptions.push_back("E1");

		//drawTitles.push_back("MC Gbweight&PIDcor");
		//drawOptions.push_back("E1");

		drawTitles.push_back("MC Gbweight");
		drawOptions.push_back("E1");

		drawTitles.push_back("MC");
		drawOptions.push_back("E1");



		if(draw[3]){
			vector<vector<TH1D>> histosBs2LcLc;
			//histosBs2LcLc.push_back(getVarsHistos1D("/mnt/d/lhcb/B2XcXc/root/Bs/data/Data_Bs2LcLc_SB_Run2_TriggerSel.root", yearCut, "(1)", kRed, Bs2LcLc_HistoInfos));
			histosBs2LcLc.push_back(getVarsHistos1D("/mnt/d/lhcb/B2XcXc/root/Bs/data/Data_Bs2LcLc_SB_Run2_TriggerSel_PreSel.root", yearCut, "(1)", kBlack, Bs2LcLc_HistoInfos));
			//histosBs2LcLc.push_back(getVarsHistos1D("/mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bs2LcLc_Run2_truth_PIDcor_TrigSel.root", yearCut, "(1)", kBlue, Bs2LcLc_HistoInfos_corr));
			//histosBs2LcLc.push_back(getVarsHistos1D("/mnt/d/lhcb/B2XcXc/root/Bs/data/Data_Bs2LcLc_SB_Run2_TriggerSel_PreSel.root", yearCut, "(1)", kBlack, Bs2LcLc_HistoInfos));
			histosBs2LcLc.push_back(getVarsHistos1D("/mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bs2LcLc_Run2_truth_PIDcor_TrigSel_Presel.root", yearCut, "(data_MC_w)", kBlue, Bs2LcLc_HistoInfos_corr));
			//histosBs2LcLc.push_back(getVarsHistos1D("/mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bs2LcLc_Run2_truth_PIDcor_TrigSel_Presel.root", yearCut, "(data_MC_w)", 8, Bs2LcLc_HistoInfos));
			histosBs2LcLc.push_back(getVarsHistos1D("/mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bs2LcLc_Run2_truth_PIDcor_TrigSel_Presel.root", yearCut, "(1)", kRed, Bs2LcLc_HistoInfos));


			TString plotDir_Bs2LcLc = "/mnt/d/lhcb/B2XcXc/public_html/compareDataMC/Bs2LcLc";
			if(year != 2) plotDir_Bs2LcLc += "/every_year/"+i2s(year);

			system("mkdir -p " + plotDir_Bs2LcLc);
			drawHistos(histosBs2LcLc,
					Bs2LcLc_HistoInfos_corr,
					drawTitles, drawOptions,
					legPos,
					plotDir_Bs2LcLc);

			if(year == 2){
				// log plots
				plotDir_Bs2LcLc = "/mnt/d/lhcb/B2XcXc/public_html/compareDataMC/Bs2LcLc/log_plots";
				for(int i=0; i<Bs2LcLc_HistoInfos.size(); i++){
					Bs2LcLc_HistoInfos[i].setLog = true;
					Bs2LcLc_HistoInfos_corr[i].setLog = true;
				}
				drawHistos(histosBs2LcLc,
					Bs2LcLc_HistoInfos_corr,
					drawTitles, drawOptions,
					legPos,
					plotDir_Bs2LcLc);
			}
		}




		//// for Bd
		vector<Histo1DWithInfo> Bd2LcLc_HistoInfos;
		Bd2LcLc_HistoInfos.push_back(Histo1DWithInfo("angle_p_Xc_plus_Kminus_Xc_plus", "angle_p_Xc_plus_Kminus_Xc_plus", "angle_p_Xc_plus_Kminus_Xc_plus", 0, 0.004, 50, false));
		Bd2LcLc_HistoInfos.push_back(Histo1DWithInfo("angle_p_Xc_plus_piplus_Xc_plus", "angle_p_Xc_plus_piplus_Xc_plus", "angle_p_Xc_plus_piplus_Xc_plus", 0, 0.004, 50, false));
		Bd2LcLc_HistoInfos.push_back(Histo1DWithInfo("angle_piplus_Xc_plus_Kminus_Xc_plus", "angle_piplus_Xc_plus_Kminus_Xc_plus", "angle_piplus_Xc_plus_Kminus_Xc_plus", 0, 0.004, 50, false));
		Bd2LcLc_HistoInfos.push_back(Histo1DWithInfo("angle_pbar_Xc_minus_Kplus_Xc_minus", "angle_pbar_Xc_minus_Kplus_Xc_minus", "angle_pbar_Xc_minus_Kplus_Xc_minus", 0, 0.004, 50, false));
		Bd2LcLc_HistoInfos.push_back(Histo1DWithInfo("angle_pbar_Xc_minus_piminus_Xc_minus", "angle_pbar_Xc_minus_piminus_Xc_minus", "angle_pbar_Xc_minus_piminus_Xc_minus", 0, 0.004, 50, false));
		Bd2LcLc_HistoInfos.push_back(Histo1DWithInfo("angle_piminus_Xc_minus_Kplus_Xc_minus", "angle_piminus_Xc_minus_Kplus_Xc_minus", "angle_piminus_Xc_minus_Kplus_Xc_minus", 0, 0.004, 50, false));
		Bd2LcLc_HistoInfos.push_back(Histo1DWithInfo("angle_p_Xc_plus_pbar_Xc_minus", "angle_p_Xc_plus_pbar_Xc_minus", "angle_p_Xc_plus_pbar_Xc_minus", 0, 0.004, 50, false));
		Bd2LcLc_HistoInfos.push_back(Histo1DWithInfo("angle_p_Xc_plus_Kplus_Xc_minus", "angle_p_Xc_plus_Kplus_Xc_minus", "angle_p_Xc_plus_Kplus_Xc_minus", 0, 0.004, 50, false));
		Bd2LcLc_HistoInfos.push_back(Histo1DWithInfo("angle_p_Xc_plus_piminus_Xc_minus", "angle_p_Xc_plus_piminus_Xc_minus", "angle_p_Xc_plus_piminus_Xc_minus", 0, 0.004, 50, false));
		Bd2LcLc_HistoInfos.push_back(Histo1DWithInfo("angle_Kminus_Xc_plus_pbar_Xc_minus", "angle_Kminus_Xc_plus_pbar_Xc_minus", "angle_Kminus_Xc_plus_pbar_Xc_minus", 0, 0.004, 50, false));
		Bd2LcLc_HistoInfos.push_back(Histo1DWithInfo("angle_Kminus_Xc_plus_Kplus_Xc_minus", "angle_Kminus_Xc_plus_Kplus_Xc_minus", "angle_Kminus_Xc_plus_Kplus_Xc_minus", 0, 0.004, 50, false));
		Bd2LcLc_HistoInfos.push_back(Histo1DWithInfo("angle_Kminus_Xc_plus_piminus_Xc_minus", "angle_Kminus_Xc_plus_piminus_Xc_minus", "angle_Kminus_Xc_plus_piminus_Xc_minus", 0, 0.004, 50, false));
		Bd2LcLc_HistoInfos.push_back(Histo1DWithInfo("angle_piplus_Xc_plus_pbar_Xc_minus", "angle_piplus_Xc_plus_pbar_Xc_minus", "angle_piplus_Xc_plus_pbar_Xc_minus", 0, 0.004, 50, false));
		Bd2LcLc_HistoInfos.push_back(Histo1DWithInfo("angle_piplus_Xc_plus_Kplus_Xc_minus", "angle_piplus_Xc_plus_Kplus_Xc_minus", "angle_piplus_Xc_plus_Kplus_Xc_minus", 0, 0.004, 50, false));
		Bd2LcLc_HistoInfos.push_back(Histo1DWithInfo("angle_piplus_Xc_plus_piminus_Xc_minus", "angle_piplus_Xc_plus_piminus_Xc_minus", "angle_piplus_Xc_plus_piminus_Xc_minus", 0, 0.004, 50, false));
		Bd2LcLc_HistoInfos.push_back(Histo1DWithInfo("p_Xc_plus_MC15TuneV1_ProbNNp", "p_Xc_plus_MC15TuneV1_ProbNNp", "p_Xc_plus_MC15TuneV1_ProbNNp", 0.1, 1, 50, false));
		Bd2LcLc_HistoInfos.push_back(Histo1DWithInfo("Kminus_Xc_plus_MC15TuneV1_ProbNNk", "Kminus_Xc_plus_MC15TuneV1_ProbNNk", "Kminus_Xc_plus_MC15TuneV1_ProbNNk", 0.1, 1, 50, false));
		Bd2LcLc_HistoInfos.push_back(Histo1DWithInfo("piplus_Xc_plus_MC15TuneV1_ProbNNpi", "piplus_Xc_plus_MC15TuneV1_ProbNNpi", "piplus_Xc_plus_MC15TuneV1_ProbNNpi", 0.1, 1, 50, false));
		Bd2LcLc_HistoInfos.push_back(Histo1DWithInfo("pbar_Xc_minus_MC15TuneV1_ProbNNp", "pbar_Xc_minus_MC15TuneV1_ProbNNp", "pbar_Xc_minus_MC15TuneV1_ProbNNp", 0.1, 1, 50, false));
		Bd2LcLc_HistoInfos.push_back(Histo1DWithInfo("Kplus_Xc_minus_MC15TuneV1_ProbNNk", "Kplus_Xc_minus_MC15TuneV1_ProbNNk", "Kplus_Xc_minus_MC15TuneV1_ProbNNk", 0.1, 1, 50, false));
		Bd2LcLc_HistoInfos.push_back(Histo1DWithInfo("piminus_Xc_minus_MC15TuneV1_ProbNNpi", "piminus_Xc_minus_MC15TuneV1_ProbNNpi", "piminus_Xc_minus_MC15TuneV1_ProbNNpi", 0.1, 1, 50, false));
		Bd2LcLc_HistoInfos.push_back(Histo1DWithInfo("prodProbNNx", getProdProbNNx("Bd2LcLc"), "prodProbNNx", 0.05, 1, 50, false));
		Bd2LcLc_HistoInfos.push_back(Histo1DWithInfo("log_B_IPCHI2_OWNPV", "log(B_IPCHI2_OWNPV)", "log(B_IPCHI2_OWNPV)", -6, 4, 50, false));
		Bd2LcLc_HistoInfos.push_back(Histo1DWithInfo("B_PT", "B_PT", "B_PT", 0, 5e4, 50, false));
		Bd2LcLc_HistoInfos.push_back(Histo1DWithInfo("B_ETA", "B_ETA", "B_ETA", 2, 5, 50, false));
		Bd2LcLc_HistoInfos.push_back(Histo1DWithInfo("nTracks", "nTracks", "nTracks", 0, 500, 50, false));
		Bd2LcLc_HistoInfos.push_back(Histo1DWithInfo("pbar_Xc_minus_PT", "pbar_Xc_minus_PT", "pbar_Xc_minus_PT", 0, 13000, 50, false));
		Bd2LcLc_HistoInfos.push_back(Histo1DWithInfo("Kplus_Xc_minus_PT", "Kplus_Xc_minus_PT", "Kplus_Xc_minus_PT", 0, 13000, 50, false));
		Bd2LcLc_HistoInfos.push_back(Histo1DWithInfo("piminus_Xc_minus_PT", "piminus_Xc_minus_PT", "piminus_Xc_minus_PT", 0, 13000, 50, false));
		Bd2LcLc_HistoInfos.push_back(Histo1DWithInfo("p_Xc_plus_PT", "p_Xc_plus_PT", "p_Xc_plus_PT", 0, 13000, 50, false));
		Bd2LcLc_HistoInfos.push_back(Histo1DWithInfo("Kminus_Xc_plus_PT", "Kminus_Xc_plus_PT", "Kminus_Xc_plus_PT", 0, 13000, 50, false));
		Bd2LcLc_HistoInfos.push_back(Histo1DWithInfo("piplus_Xc_plus_PT", "piplus_Xc_plus_PT", "piplus_Xc_plus_PT", 0, 13000, 50, false));
		Bd2LcLc_HistoInfos.push_back(Histo1DWithInfo("p_Xc_plus_ETA", "p_Xc_plus_ETA", "p_Xc_plus_ETA", 1.5, 5.5, 50, false));
		Bd2LcLc_HistoInfos.push_back(Histo1DWithInfo("Kminus_Xc_plus_ETA", "Kminus_Xc_plus_ETA", "Kminus_Xc_plus_ETA", 1.5, 5.5, 50, false));
		Bd2LcLc_HistoInfos.push_back(Histo1DWithInfo("piplus_Xc_plus_ETA", "piplus_Xc_plus_ETA", "piplus_Xc_plus_ETA", 1.5, 5.5, 50, false));
		Bd2LcLc_HistoInfos.push_back(Histo1DWithInfo("pbar_Xc_minus_ETA", "pbar_Xc_minus_ETA", "pbar_Xc_minus_ETA", 1.5, 5.5, 50, false));
		Bd2LcLc_HistoInfos.push_back(Histo1DWithInfo("Kplus_Xc_minus_ETA", "Kplus_Xc_minus_ETA", "Kplus_Xc_minus_ETA", 1.5, 5.5, 50, false));
		Bd2LcLc_HistoInfos.push_back(Histo1DWithInfo("piminus_Xc_minus_ETA", "piminus_Xc_minus_ETA", "piminus_Xc_minus_ETA", 1.5, 5.5, 50, false));


		vector<Histo1DWithInfo> Bd2LcLc_HistoInfos_corr;
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_p_Xc_plus_Kminus_Xc_plus", "angle_p_Xc_plus_Kminus_Xc_plus", "angle_p_Xc_plus_Kminus_Xc_plus", 0, 0.004, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_p_Xc_plus_piplus_Xc_plus", "angle_p_Xc_plus_piplus_Xc_plus", "angle_p_Xc_plus_piplus_Xc_plus", 0, 0.004, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piplus_Xc_plus_Kminus_Xc_plus", "angle_piplus_Xc_plus_Kminus_Xc_plus", "angle_piplus_Xc_plus_Kminus_Xc_plus", 0, 0.004, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_pbar_Xc_minus_Kplus_Xc_minus", "angle_pbar_Xc_minus_Kplus_Xc_minus", "angle_pbar_Xc_minus_Kplus_Xc_minus", 0, 0.004, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_pbar_Xc_minus_piminus_Xc_minus", "angle_pbar_Xc_minus_piminus_Xc_minus", "angle_pbar_Xc_minus_piminus_Xc_minus", 0, 0.004, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piminus_Xc_minus_Kplus_Xc_minus", "angle_piminus_Xc_minus_Kplus_Xc_minus", "angle_piminus_Xc_minus_Kplus_Xc_minus", 0, 0.004, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_p_Xc_plus_pbar_Xc_minus", "angle_p_Xc_plus_pbar_Xc_minus", "angle_p_Xc_plus_pbar_Xc_minus", 0, 0.004, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_p_Xc_plus_Kplus_Xc_minus", "angle_p_Xc_plus_Kplus_Xc_minus", "angle_p_Xc_plus_Kplus_Xc_minus", 0, 0.004, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_p_Xc_plus_piminus_Xc_minus", "angle_p_Xc_plus_piminus_Xc_minus", "angle_p_Xc_plus_piminus_Xc_minus", 0, 0.004, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kminus_Xc_plus_pbar_Xc_minus", "angle_Kminus_Xc_plus_pbar_Xc_minus", "angle_Kminus_Xc_plus_pbar_Xc_minus", 0, 0.004, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kminus_Xc_plus_Kplus_Xc_minus", "angle_Kminus_Xc_plus_Kplus_Xc_minus", "angle_Kminus_Xc_plus_Kplus_Xc_minus", 0, 0.004, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kminus_Xc_plus_piminus_Xc_minus", "angle_Kminus_Xc_plus_piminus_Xc_minus", "angle_Kminus_Xc_plus_piminus_Xc_minus", 0, 0.004, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piplus_Xc_plus_pbar_Xc_minus", "angle_piplus_Xc_plus_pbar_Xc_minus", "angle_piplus_Xc_plus_pbar_Xc_minus", 0, 0.004, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piplus_Xc_plus_Kplus_Xc_minus", "angle_piplus_Xc_plus_Kplus_Xc_minus", "angle_piplus_Xc_plus_Kplus_Xc_minus", 0, 0.004, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piplus_Xc_plus_piminus_Xc_minus", "angle_piplus_Xc_plus_piminus_Xc_minus", "angle_piplus_Xc_plus_piminus_Xc_minus", 0, 0.004, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("p_Xc_plus_MC15TuneV1_ProbNNp_corr", "p_Xc_plus_MC15TuneV1_ProbNNp_corr", "p_Xc_plus_MC15TuneV1_ProbNNp", 0.1, 1, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Xc_plus_MC15TuneV1_ProbNNk_corr", "Kminus_Xc_plus_MC15TuneV1_ProbNNk_corr", "Kminus_Xc_plus_MC15TuneV1_ProbNNk", 0.1, 1, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("piplus_Xc_plus_MC15TuneV1_ProbNNpi_corr", "piplus_Xc_plus_MC15TuneV1_ProbNNpi", "piplus_Xc_plus_MC15TuneV1_ProbNNpi", 0.1, 1, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("pbar_Xc_minus_MC15TuneV1_ProbNNp_corr", "pbar_Xc_minus_MC15TuneV1_ProbNNp_corr", "pbar_Xc_minus_MC15TuneV1_ProbNNp", 0.1, 1, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("Kplus_Xc_minus_MC15TuneV1_ProbNNk_corr", "Kplus_Xc_minus_MC15TuneV1_ProbNNk_corr", "Kplus_Xc_minus_MC15TuneV1_ProbNNk", 0.1, 1, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("piminus_Xc_minus_MC15TuneV1_ProbNNpi_corr", "piminus_Xc_minus_MC15TuneV1_ProbNNpi", "piminus_Xc_minus_MC15TuneV1_ProbNNpi", 0.1, 1, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("prodProbNNx_corr", "prodProbNNx", "prodProbNNx", 0.05, 1, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("log_B_IPCHI2_OWNPV", "log(B_IPCHI2_OWNPV)", "log(B_IPCHI2_OWNPV)", -6, 4, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("B_PT", "B_PT", "B_PT", 0, 5e4, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("B_ETA", "B_ETA", "B_ETA", 2, 5, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("nTracks", "nTracks", "nTracks", 0, 500, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("pbar_Xc_minus_PT", "pbar_Xc_minus_PT", "pbar_Xc_minus_PT", 0, 13000, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("Kplus_Xc_minus_PT", "Kplus_Xc_minus_PT", "Kplus_Xc_minus_PT", 0, 13000, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("piminus_Xc_minus_PT", "piminus_Xc_minus_PT", "piminus_Xc_minus_PT", 0, 13000, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("p_Xc_plus_PT", "p_Xc_plus_PT", "p_Xc_plus_PT", 0, 13000, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Xc_plus_PT", "Kminus_Xc_plus_PT", "Kminus_Xc_plus_PT", 0, 13000, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("piplus_Xc_plus_PT", "piplus_Xc_plus_PT", "piplus_Xc_plus_PT", 0, 13000, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("p_Xc_plus_ETA", "p_Xc_plus_ETA", "p_Xc_plus_ETA", 1.5, 5.5, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Xc_plus_ETA", "Kminus_Xc_plus_ETA", "Kminus_Xc_plus_ETA", 1.5, 5.5, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("piplus_Xc_plus_ETA", "piplus_Xc_plus_ETA", "piplus_Xc_plus_ETA", 1.5, 5.5, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("pbar_Xc_minus_ETA", "pbar_Xc_minus_ETA", "pbar_Xc_minus_ETA", 1.5, 5.5, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("Kplus_Xc_minus_ETA", "Kplus_Xc_minus_ETA", "Kplus_Xc_minus_ETA", 1.5, 5.5, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("piminus_Xc_minus_ETA", "piminus_Xc_minus_ETA", "piminus_Xc_minus_ETA", 1.5, 5.5, 50, false));


		treeNames.clear();
		drawTitles.clear();
		cuts.clear();
		weights.clear();
		colors.clear();
		drawOptions.clear();


		//drawTitles.push_back("DATA Sideband");
		//drawOptions.push_back("E1");
		drawTitles.push_back("DATA Sideband");
		drawOptions.push_back("E1");

		//drawTitles.push_back("MC Gbweight&PIDcor");
		//drawOptions.push_back("E1");

		drawTitles.push_back("MC Gbweight");
		drawOptions.push_back("E1");

		drawTitles.push_back("MC");
		drawOptions.push_back("E1");



		if(draw[1]){
			vector<vector<TH1D>> histosBd2LcLc;
			//histosBd2LcLc.push_back(getVarsHistos1D("/mnt/d/lhcb/B2XcXc/root/Bs/data/Data_Bs2LcLc_SB_Run2_TriggerSel.root", yearCut, "(1)", kRed, Bd2LcLc_HistoInfos));
			histosBd2LcLc.push_back(getVarsHistos1D("/mnt/d/lhcb/B2XcXc/root/Bs/data/Data_Bs2LcLc_SB_Run2_TriggerSel_PreSel.root", yearCut, "(1)", kBlack, Bd2LcLc_HistoInfos));
			////histosBd2LcLc.push_back(getVarsHistos1D("/mnt/d/lhcb/B2XcXc/root/Bs/data/Data_Bs2LcLc_SB_Run2_TriggerSel_PreSel.root", yearCut, "(1)", kBlack, Bd2LcLc_HistoInfos));
			histosBd2LcLc.push_back(getVarsHistos1D("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2LcLc_Run2_truth_PIDcor_TrigSel_Presel.root", yearCut, "(data_MC_w)", kBlue, Bd2LcLc_HistoInfos_corr));
			histosBd2LcLc.push_back(getVarsHistos1D("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2LcLc_Run2_truth_PIDcor_TrigSel_Presel.root", yearCut, "(1)", kRed, Bd2LcLc_HistoInfos));


			TString plotDir_Bd2LcLc = "/mnt/d/lhcb/B2XcXc/public_html/compareDataMC/Bd2LcLc";
			if(year != 2) plotDir_Bd2LcLc += "/every_year/"+i2s(year);

			system("mkdir -p " + plotDir_Bd2LcLc);
			drawHistos(histosBd2LcLc,
					Bd2LcLc_HistoInfos_corr,
					drawTitles, drawOptions,
					legPos,
					plotDir_Bd2LcLc);

			if(year == 2){
				// log plots
				plotDir_Bd2LcLc = "/mnt/d/lhcb/B2XcXc/public_html/compareDataMC/Bd2LcLc/log_plots";
				for(int i=0; i<Bd2LcLc_HistoInfos.size(); i++){
					Bd2LcLc_HistoInfos[i].setLog = true;
					Bd2LcLc_HistoInfos_corr[i].setLog = true;
				}
				drawHistos(histosBd2LcLc,
					Bd2LcLc_HistoInfos_corr,
					drawTitles, drawOptions,
					legPos,
					plotDir_Bd2LcLc);
			}
		}
		
		

		vector<Histo1DWithInfo> Bd2DsD_HistoInfos;
		////Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("angle_Kminus_Dplus_piplus1_Dplus", "angle_Kminus_Dplus_piplus1_Dplus", "angle_Kminus_Dplus_piplus1_Dplus", 0, 0.004, 50, false));
		////Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("angle_Kminus_Dplus_piplus2_Dplus", "angle_Kminus_Dplus_piplus2_Dplus", "angle_Kminus_Dplus_piplus2_Dplus", 0, 0.004, 50, false));
		////Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("angle_piplus2_Dplus_piplus1_Dplus", "angle_piplus2_Dplus_piplus1_Dplus", "angle_piplus2_Dplus_piplus1_Dplus", 0, 0.004, 50, false));
		////Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("angle_Kplus_Dsminus_Kminus_Dsminus", "angle_Kplus_Dsminus_Kminus_Dsminus", "angle_Kplus_Dsminus_Kminus_Dsminus", 0, 0.004, 50, false));
		////Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("angle_Kplus_Dsminus_piminus_Dsminus", "angle_Kplus_Dsminus_piminus_Dsminus", "angle_Kplus_Dsminus_piminus_Dsminus", 0, 0.004, 50, false));
		////Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("angle_piminus_Dsminus_Kminus_Dsminus", "angle_piminus_Dsminus_Kminus_Dsminus", "angle_piminus_Dsminus_Kminus_Dsminus", 0, 0.004, 50, false));
		////Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("angle_Kminus_Dplus_Kplus_Dsminus", "angle_Kminus_Dplus_Kplus_Dsminus", "angle_Kminus_Dplus_Kplus_Dsminus", 0, 0.004, 50, false));
		////Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("angle_Kminus_Dplus_Kminus_Dsminus", "angle_Kminus_Dplus_Kminus_Dsminus", "angle_Kminus_Dplus_Kminus_Dsminus", 0, 0.004, 50, false));
		////Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("angle_Kminus_Dplus_piminus_Dsminus", "angle_Kminus_Dplus_piminus_Dsminus", "angle_Kminus_Dplus_piminus_Dsminus", 0, 0.004, 50, false));
		////Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("angle_piplus1_Dplus_Kplus_Dsminus", "angle_piplus1_Dplus_Kplus_Dsminus", "angle_piplus1_Dplus_Kplus_Dsminus", 0, 0.004, 50, false));
		////Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("angle_piplus1_Dplus_Kminus_Dsminus", "angle_piplus1_Dplus_Kminus_Dsminus", "angle_piplus1_Dplus_Kminus_Dsminus", 0, 0.004, 50, false));
		////Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("angle_piplus1_Dplus_piminus_Dsminus", "angle_piplus1_Dplus_piminus_Dsminus", "angle_piplus1_Dplus_piminus_Dsminus", 0, 0.004, 50, false));
		////Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("angle_piplus2_Dplus_Kplus_Dsminus", "angle_piplus2_Dplus_Kplus_Dsminus", "angle_piplus2_Dplus_Kplus_Dsminus", 0, 0.004, 50, false));
		////Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("angle_piplus2_Dplus_Kminus_Dsminus", "angle_piplus2_Dplus_Kminus_Dsminus", "angle_piplus2_Dplus_Kminus_Dsminus", 0, 0.004, 50, false));
		////Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("angle_piplus2_Dplus_piminus_Dsminus", "angle_piplus2_Dplus_piminus_Dsminus", "angle_piplus2_Dplus_piminus_Dsminus", 0, 0.004, 50, false));
		Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("Kplus_Dsminus_MC15TuneV1_ProbNNk", "Kplus_Dsminus_MC15TuneV1_ProbNNk", "Kplus_Dsminus_MC15TuneV1_ProbNNk", 0.1, 1, 50, false));
		Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("Kminus_Dsminus_MC15TuneV1_ProbNNk", "Kminus_Dsminus_MC15TuneV1_ProbNNk", "Kminus_Dsminus_MC15TuneV1_ProbNNk", 0.1, 1, 50, false));
		Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("piminus_Dsminus_MC15TuneV1_ProbNNpi", "piminus_Dsminus_MC15TuneV1_ProbNNpi", "piminus_Dsminus_MC15TuneV1_ProbNNpi", 0.1, 1, 50, false));
		Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("Kminus_Dplus_MC15TuneV1_ProbNNk", "Kminus_Dplus_MC15TuneV1_ProbNNk", "Kminus_Dplus_MC15TuneV1_ProbNNk", 0.1, 1, 50, false));
		Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("piplus1_Dplus_MC15TuneV1_ProbNNpi", "piplus1_Dplus_MC15TuneV1_ProbNNpi", "piplus1_Dplus_MC15TuneV1_ProbNNpi", 0.1, 1, 50, false));
		Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("piplus2_Dplus_MC15TuneV1_ProbNNpi", "piplus2_Dplus_MC15TuneV1_ProbNNpi", "piplus2_Dplus_MC15TuneV1_ProbNNpi", 0.1, 1, 50, false));
		Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("prodProbNNx", getProdProbNNx("Bd2DsD"), "prodProbNNx", 0.05, 1, 50, false));
		Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("log_B_IPCHI2_OWNPV", "log(B_IPCHI2_OWNPV)", "log(B_IPCHI2_OWNPV)", -6, 4, 50, false));
		Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("B_PT", "B_PT", "B_PT", 0, 5e4, 50, false));
		//Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("B_ETA", "B_ETA", "B_ETA", 2, 5, 50, false));
		//Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("nTracks", "nTracks", "nTracks", 0, 500, 50, false));
		//Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("Kplus_Dsminus_PT", "Kplus_Dsminus_PT", "Kplus_Dsminus_PT", 0, 13000, 50, false));
		//Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("Kminus_Dsminus_PT", "Kminus_Dsminus_PT", "Kminus_Dsminus_PT", 0, 13000, 50, false));
		//Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("piminus_Dsminus_PT", "piminus_Dsminus_PT", "piminus_Dsminus_PT", 0, 13000, 50, false));
		//Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("Kminus_Dplus_PT", "Kminus_Dplus_PT", "Kminus_Dplus_PT", 0, 13000, 50, false));
		//Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("piplus1_Dplus_PT", "piplus1_Dplus_PT", "piplus1_Dplus_PT", 0, 13000, 50, false));
		//Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("piplus2_Dplus_PT", "piplus2_Dplus_PT", "piplus2_Dplus_PT", 0, 13000, 50, false));
		//Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("Kplus_Dsminus_ETA", "Kplus_Dsminus_ETA", "Kplus_Dsminus_ETA", 1.5, 5.5, 50, false));
		//Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("Kminus_Dsminus_ETA", "Kminus_Dsminus_ETA", "Kminus_Dsminus_ETA", 1.5, 5.5, 50, false));
		//Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("piminus_Dsminus_ETA", "piminus_Dsminus_ETA", "piminus_Dsminus_ETA", 1.5, 5.5, 50, false));
		//Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("Kminus_Dplus_ETA", "Kminus_Dplus_ETA", "Kminus_Dplus_ETA", 1.5, 5.5, 50, false));
		//Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("piplus1_Dplus_ETA", "piplus1_Dplus_ETA", "piplus1_Dplus_ETA", 1.5, 5.5, 50, false));
		//Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("piplus2_Dplus_ETA", "piplus2_Dplus_ETA", "piplus2_Dplus_ETA", 1.5, 5.5, 50, false));

		vector<Histo1DWithInfo> Bd2DsD_HistoInfos_corr;
		////Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kminus_Dplus_piplus1_Dplus", "angle_Kminus_Dplus_piplus1_Dplus", "angle_Kminus_Dplus_piplus1_Dplus", 0, 0.004, 50, false));
		////Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kminus_Dplus_piplus2_Dplus", "angle_Kminus_Dplus_piplus2_Dplus", "angle_Kminus_Dplus_piplus2_Dplus", 0, 0.004, 50, false));
		////Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piplus2_Dplus_piplus1_Dplus", "angle_piplus2_Dplus_piplus1_Dplus", "angle_piplus2_Dplus_piplus1_Dplus", 0, 0.004, 50, false));
		////Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kplus_Dsminus_Kminus_Dsminus", "angle_Kplus_Dsminus_Kminus_Dsminus", "angle_Kplus_Dsminus_Kminus_Dsminus", 0, 0.004, 50, false));
		////Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kplus_Dsminus_piminus_Dsminus", "angle_Kplus_Dsminus_piminus_Dsminus", "angle_Kplus_Dsminus_piminus_Dsminus", 0, 0.004, 50, false));
		////Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piminus_Dsminus_Kminus_Dsminus", "angle_piminus_Dsminus_Kminus_Dsminus", "angle_piminus_Dsminus_Kminus_Dsminus", 0, 0.004, 50, false));
		////Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kminus_Dplus_Kplus_Dsminus", "angle_Kminus_Dplus_Kplus_Dsminus", "angle_Kminus_Dplus_Kplus_Dsminus", 0, 0.004, 50, false));
		////Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kminus_Dplus_Kminus_Dsminus", "angle_Kminus_Dplus_Kminus_Dsminus", "angle_Kminus_Dplus_Kminus_Dsminus", 0, 0.004, 50, false));
		////Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kminus_Dplus_piminus_Dsminus", "angle_Kminus_Dplus_piminus_Dsminus", "angle_Kminus_Dplus_piminus_Dsminus", 0, 0.004, 50, false));
		////Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piplus1_Dplus_Kplus_Dsminus", "angle_piplus1_Dplus_Kplus_Dsminus", "angle_piplus1_Dplus_Kplus_Dsminus", 0, 0.004, 50, false));
		////Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piplus1_Dplus_Kminus_Dsminus", "angle_piplus1_Dplus_Kminus_Dsminus", "angle_piplus1_Dplus_Kminus_Dsminus", 0, 0.004, 50, false));
		////Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piplus1_Dplus_piminus_Dsminus", "angle_piplus1_Dplus_piminus_Dsminus", "angle_piplus1_Dplus_piminus_Dsminus", 0, 0.004, 50, false));
		////Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piplus2_Dplus_Kplus_Dsminus", "angle_piplus2_Dplus_Kplus_Dsminus", "angle_piplus2_Dplus_Kplus_Dsminus", 0, 0.004, 50, false));
		////Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piplus2_Dplus_Kminus_Dsminus", "angle_piplus2_Dplus_Kminus_Dsminus", "angle_piplus2_Dplus_Kminus_Dsminus", 0, 0.004, 50, false));
		////Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piplus2_Dplus_piminus_Dsminus", "angle_piplus2_Dplus_piminus_Dsminus", "angle_piplus2_Dplus_piminus_Dsminus", 0, 0.004, 50, false));
		Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("Kplus_Dsminus_MC15TuneV1_ProbNNk_corr", "Kplus_Dsminus_MC15TuneV1_ProbNNk_corr", "Kplus_Dsminus_MC15TuneV1_ProbNNk", 0.1, 1, 50, false));
		Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Dsminus_MC15TuneV1_ProbNNk_corr", "Kminus_Dsminus_MC15TuneV1_ProbNNk_corr", "Kminus_Dsminus_MC15TuneV1_ProbNNk", 0.1, 1, 50, false));
		Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("piminus_Dsminus_MC15TuneV1_ProbNNpi_corr", "piminus_Dsminus_MC15TuneV1_ProbNNpi", "piminus_Dsminus_MC15TuneV1_ProbNNpi", 0.1, 1, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("piminus_Dsminus_MC15TuneV1_ProbNNpi_corr", "piminus_Dsminus_MC15TuneV1_ProbNNpi_corr", "piminus_Dsminus_MC15TuneV1_ProbNNpi", 0.1, 1, 50, false));
		Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Dplus_MC15TuneV1_ProbNNk_corr", "Kminus_Dplus_MC15TuneV1_ProbNNk_corr", "Kminus_Dplus_MC15TuneV1_ProbNNk", 0.1, 1, 50, false));
		Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("piplus1_Dplus_MC15TuneV1_ProbNNpi_corr", "piplus1_Dplus_MC15TuneV1_ProbNNpi", "piplus1_Dplus_MC15TuneV1_ProbNNpi", 0.1, 1, 50, false));
		Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("piplus2_Dplus_MC15TuneV1_ProbNNpi_corr", "piplus2_Dplus_MC15TuneV1_ProbNNpi", "piplus2_Dplus_MC15TuneV1_ProbNNpi", 0.1, 1, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("piplus1_Dplus_MC15TuneV1_ProbNNpi_corr", "piplus1_Dplus_MC15TuneV1_ProbNNpi_corr", "piplus1_Dplus_MC15TuneV1_ProbNNpi", 0.1, 1, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("piplus2_Dplus_MC15TuneV1_ProbNNpi_corr", "piplus2_Dplus_MC15TuneV1_ProbNNpi_corr", "piplus2_Dplus_MC15TuneV1_ProbNNpi", 0.1, 1, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("prodProbNNx_corr", "prodProbNNx", "prodProbNNx", 0.05, 1, 50, false));
		Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("prodProbNNx_corr", "Kplus_Dsminus_MC15TuneV1_ProbNNk_corr*Kminus_Dsminus_MC15TuneV1_ProbNNk_corr*piminus_Dsminus_MC15TuneV1_ProbNNpi_corr*Kminus_Dplus_MC15TuneV1_ProbNNk_corr*piplus1_Dplus_MC15TuneV1_ProbNNpi_corr*piplus2_Dplus_MC15TuneV1_ProbNNpi_corr", "prodProbNNx", 0.05, 1, 50, false));
		Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("log_B_IPCHI2_OWNPV", "log(B_IPCHI2_OWNPV)", "log(B_IPCHI2_OWNPV)", -6, 4, 50, false));
		Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("B_PT", "B_PT", "B_PT", 0, 5e4, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("B_ETA", "B_ETA", "B_ETA", 2, 5, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("nTracks", "nTracks", "nTracks", 0, 500, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("Kplus_Dsminus_PT", "Kplus_Dsminus_PT", "Kplus_Dsminus_PT", 0, 13000, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Dsminus_PT", "Kminus_Dsminus_PT", "Kminus_Dsminus_PT", 0, 13000, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("piminus_Dsminus_PT", "piminus_Dsminus_PT", "piminus_Dsminus_PT", 0, 13000, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Dplus_PT", "Kminus_Dplus_PT", "Kminus_Dplus_PT", 0, 13000, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("piplus1_Dplus_PT", "piplus1_Dplus_PT", "piplus1_Dplus_PT", 0, 13000, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("piplus2_Dplus_PT", "piplus2_Dplus_PT", "piplus2_Dplus_PT", 0, 13000, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("Kplus_Dsminus_ETA", "Kplus_Dsminus_ETA", "Kplus_Dsminus_ETA", 1.5, 5.5, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Dsminus_ETA", "Kminus_Dsminus_ETA", "Kminus_Dsminus_ETA", 1.5, 5.5, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("piminus_Dsminus_ETA", "piminus_Dsminus_ETA", "piminus_Dsminus_ETA", 1.5, 5.5, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Dplus_ETA", "Kminus_Dplus_ETA", "Kminus_Dplus_ETA", 1.5, 5.5, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("piplus1_Dplus_ETA", "piplus1_Dplus_ETA", "piplus1_Dplus_ETA", 1.5, 5.5, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("piplus2_Dplus_ETA", "piplus2_Dplus_ETA", "piplus2_Dplus_ETA", 1.5, 5.5, 50, false));

		treeNames.clear();
		drawTitles.clear();
		cuts.clear();
		weights.clear();
		colors.clear();
		drawOptions.clear();


		drawTitles.push_back("sWeighted DATA");
		drawOptions.push_back("E1");

		drawTitles.push_back("PIDcor & GBw MC");
		drawOptions.push_back("E1");

		//drawTitles.push_back("MC GBw");
		//drawOptions.push_back("E1");

		drawTitles.push_back("MC");
		drawOptions.push_back("E1");


		if(draw[0]){
			vector<vector<TH1D>> histosBd2DsD;
			histosBd2DsD.push_back(getVarsHistos1D("/mnt/d/lhcb/B2XcXc/root/Bd/data/Data_Bd2DsD_Run2_TriggerSel_PreSel_sWeighted.root", yearCut, "(sig_sw)", kBlack, Bd2DsD_HistoInfos));
			histosBd2DsD.push_back(getVarsHistos1D("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2DsD_Run2_truth_PIDcor_TrigSel_Presel.root", yearCut, "(data_MC_w)", kBlue, Bd2DsD_HistoInfos_corr));
			histosBd2DsD.push_back(getVarsHistos1D("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2DsD_Run2_truth_PIDcor_TrigSel_Presel.root", yearCut, "(1)", kRed, Bd2DsD_HistoInfos));
			//histosBd2DsD.push_back(getVarsHistos1D("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2DsD_Run2_truth_PIDcor_TrigSel_Presel.root", yearCut, "(data_MC_w)", kGreen, Bd2DsD_HistoInfos));
			//histosBd2DsD.push_back(getVarsHistos1D("/mnt/d/lhcb/B2XcXc/root/Bd/data/Data_Bd2DsD_TriggerSel_PreSel_sWeighted_"+i2s(year)+".root", yearCut, "(sig_sw)", kBlack, Bd2DsD_HistoInfos));
			//histosBd2DsD.push_back(getVarsHistos1D("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2DsD_truth_PIDcor_TrigSel_Presel_"+i2s(year)+".root", yearCut, "(data_MC_w)", kBlue, Bd2DsD_HistoInfos_corr));
			//histosBd2DsD.push_back(getVarsHistos1D("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2DsD_truth_PIDcor_TrigSel_Presel_"+i2s(year)+".root", yearCut, "(1)", kRed, Bd2DsD_HistoInfos));


			TString plotDir_Bd2DsD = "/mnt/d/lhcb/B2XcXc/public_html/compareDataMC/Bd2DsD";
			if(year != 2) plotDir_Bd2DsD += "/every_year/"+i2s(year);

			system("mkdir -p " + plotDir_Bd2DsD);
			drawHistos(histosBd2DsD,
					Bd2DsD_HistoInfos_corr,
					drawTitles, drawOptions,
					legPos,
					plotDir_Bd2DsD);

			if(year == 2){
				// log plots
				plotDir_Bd2DsD = "/mnt/d/lhcb/B2XcXc/public_html/compareDataMC/Bd2DsD/log_plots";
				if(year != 2) plotDir_Bd2DsD += "/every_year/"+i2s(year);

				for(int i=0; i<Bd2DsD_HistoInfos.size(); i++){
					Bd2DsD_HistoInfos[i].setLog = true;
					Bd2DsD_HistoInfos_corr[i].setLog = true;
				}
				drawHistos(histosBd2DsD,
					Bd2DsD_HistoInfos_corr,
					drawTitles, drawOptions,
					legPos,
					plotDir_Bd2DsD);
			}
		}

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		


		vector<Histo1DWithInfo> Bs2DsDs_HistoInfos;
		//Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("angle_Kminus_Dsplus_Kplus_Dsplus", "angle_Kminus_Dsplus_Kplus_Dsplus", "angle_Kminus_Dsplus_Kplus_Dsplus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("angle_Kminus_Dsplus_piplus_Dsplus", "angle_Kminus_Dsplus_piplus_Dsplus", "angle_Kminus_Dsplus_piplus_Dsplus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("angle_piplus_Dsplus_Kplus_Dsplus", "angle_piplus_Dsplus_Kplus_Dsplus", "angle_piplus_Dsplus_Kplus_Dsplus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("angle_Kplus_Dsminus_Kminus_Dsminus", "angle_Kplus_Dsminus_Kminus_Dsminus", "angle_Kplus_Dsminus_Kminus_Dsminus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("angle_Kplus_Dsminus_piminus_Dsminus", "angle_Kplus_Dsminus_piminus_Dsminus", "angle_Kplus_Dsminus_piminus_Dsminus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("angle_piminus_Dsminus_Kminus_Dsminus", "angle_piminus_Dsminus_Kminus_Dsminus", "angle_piminus_Dsminus_Kminus_Dsminus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("angle_Kminus_Dsplus_Kplus_Dsminus", "angle_Kminus_Dsplus_Kplus_Dsminus", "angle_Kminus_Dsplus_Kplus_Dsminus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("angle_Kminus_Dsplus_Kminus_Dsminus", "angle_Kminus_Dsplus_Kminus_Dsminus", "angle_Kminus_Dsplus_Kminus_Dsminus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("angle_Kminus_Dsplus_piminus_Dsminus", "angle_Kminus_Dsplus_piminus_Dsminus", "angle_Kminus_Dsplus_piminus_Dsminus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("angle_Kplus_Dsplus_Kplus_Dsminus", "angle_Kplus_Dsplus_Kplus_Dsminus", "angle_Kplus_Dsplus_Kplus_Dsminus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("angle_Kplus_Dsplus_Kminus_Dsminus", "angle_Kplus_Dsplus_Kminus_Dsminus", "angle_Kplus_Dsplus_Kminus_Dsminus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("angle_Kplus_Dsplus_piminus_Dsminus", "angle_Kplus_Dsplus_piminus_Dsminus", "angle_Kplus_Dsplus_piminus_Dsminus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("angle_piplus_Dsplus_Kplus_Dsminus", "angle_piplus_Dsplus_Kplus_Dsminus", "angle_piplus_Dsplus_Kplus_Dsminus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("angle_piplus_Dsplus_Kminus_Dsminus", "angle_piplus_Dsplus_Kminus_Dsminus", "angle_piplus_Dsplus_Kminus_Dsminus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("angle_piplus_Dsplus_piminus_Dsminus", "angle_piplus_Dsplus_piminus_Dsminus", "angle_piplus_Dsplus_piminus_Dsminus", 0, 0.004, 50, false));
		Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("Kplus_Dsminus_MC15TuneV1_ProbNNk", "Kplus_Dsminus_MC15TuneV1_ProbNNk", "Kplus_Dsminus_MC15TuneV1_ProbNNk", 0.1, 1, 50, false));
		Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("Kminus_Dsminus_MC15TuneV1_ProbNNk", "Kminus_Dsminus_MC15TuneV1_ProbNNk", "Kminus_Dsminus_MC15TuneV1_ProbNNk", 0.1, 1, 50, false));
		Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("piminus_Dsminus_MC15TuneV1_ProbNNpi", "piminus_Dsminus_MC15TuneV1_ProbNNpi", "piminus_Dsminus_MC15TuneV1_ProbNNpi", 0.1, 1, 50, false));
		Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("Kminus_Dsplus_MC15TuneV1_ProbNNk", "Kminus_Dsplus_MC15TuneV1_ProbNNk", "Kminus_Dsplus_MC15TuneV1_ProbNNk", 0.1, 1, 50, false));
		Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("Kplus_Dsplus_MC15TuneV1_ProbNNk", "Kplus_Dsplus_MC15TuneV1_ProbNNk", "Kplus_Dsplus_MC15TuneV1_ProbNNk", 0.1, 1, 50, false));
		Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("piplus_Dsplus_MC15TuneV1_ProbNNpi", "piplus_Dsplus_MC15TuneV1_ProbNNpi", "piplus_Dsplus_MC15TuneV1_ProbNNpi", 0.1, 1, 50, false));
		Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("prodProbNNx", getProdProbNNx("Bs2DsDs"), "prodProbNNx", 0.05, 1, 50, false));
		Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("log_B_IPCHI2_OWNPV", "log(B_IPCHI2_OWNPV)", "log(B_IPCHI2_OWNPV)", -6, 4, 50, false));
		Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("nTracks", "nTracks", "nTracks", 0, 500, 50, false));
		Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("Kplus_Dsminus_PT", "Kplus_Dsminus_PT", "Kplus_Dsminus_PT", 0, 13000, 50, false));
		Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("Kminus_Dsminus_PT", "Kminus_Dsminus_PT", "Kminus_Dsminus_PT", 0, 13000, 50, false));
		Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("piminus_Dsminus_PT", "piminus_Dsminus_PT", "piminus_Dsminus_PT", 0, 13000, 50, false));
		Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("Kminus_Dsplus_PT", "Kminus_Dsplus_PT", "Kminus_Dsplus_PT", 0, 13000, 50, false));
		Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("Kplus_Dsplus_PT", "Kplus_Dsplus_PT", "Kplus_Dsplus_PT", 0, 13000, 50, false));
		Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("piplus_Dsplus_PT", "piplus_Dsplus_PT", "piplus_Dsplus_PT", 0, 13000, 50, false));
		Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("Kplus_Dsminus_ETA", "Kplus_Dsminus_ETA", "Kplus_Dsminus_ETA", 1.5, 5.5, 50, false));
		Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("Kminus_Dsminus_ETA", "Kminus_Dsminus_ETA", "Kminus_Dsminus_ETA", 1.5, 5.5, 50, false));
		Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("piminus_Dsminus_ETA", "piminus_Dsminus_ETA", "piminus_Dsminus_ETA", 1.5, 5.5, 50, false));
		Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("Kminus_Dsplus_ETA", "Kminus_Dsplus_ETA", "Kminus_Dsplus_ETA", 1.5, 5.5, 50, false));
		Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("Kplus_Dsplus_ETA", "Kplus_Dsplus_ETA", "Kplus_Dsplus_ETA", 1.5, 5.5, 50, false));
		Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("piplus_Dsplus_ETA", "piplus_Dsplus_ETA", "piplus_Dsplus_ETA", 1.5, 5.5, 50, false));

		vector<Histo1DWithInfo> Bs2DsDs_HistoInfos_corr;
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kminus_Dsplus_Kplus_Dsplus", "angle_Kminus_Dsplus_Kplus_Dsplus", "angle_Kminus_Dsplus_Kplus_Dsplus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kminus_Dsplus_piplus_Dsplus", "angle_Kminus_Dsplus_piplus_Dsplus", "angle_Kminus_Dsplus_piplus_Dsplus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piplus_Dsplus_Kplus_Dsplus", "angle_piplus_Dsplus_Kplus_Dsplus", "angle_piplus_Dsplus_Kplus_Dsplus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kplus_Dsminus_Kminus_Dsminus", "angle_Kplus_Dsminus_Kminus_Dsminus", "angle_Kplus_Dsminus_Kminus_Dsminus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kplus_Dsminus_piminus_Dsminus", "angle_Kplus_Dsminus_piminus_Dsminus", "angle_Kplus_Dsminus_piminus_Dsminus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piminus_Dsminus_Kminus_Dsminus", "angle_piminus_Dsminus_Kminus_Dsminus", "angle_piminus_Dsminus_Kminus_Dsminus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kminus_Dsplus_Kplus_Dsminus", "angle_Kminus_Dsplus_Kplus_Dsminus", "angle_Kminus_Dsplus_Kplus_Dsminus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kminus_Dsplus_Kminus_Dsminus", "angle_Kminus_Dsplus_Kminus_Dsminus", "angle_Kminus_Dsplus_Kminus_Dsminus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kminus_Dsplus_piminus_Dsminus", "angle_Kminus_Dsplus_piminus_Dsminus", "angle_Kminus_Dsplus_piminus_Dsminus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kplus_Dsplus_Kplus_Dsminus", "angle_Kplus_Dsplus_Kplus_Dsminus", "angle_Kplus_Dsplus_Kplus_Dsminus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kplus_Dsplus_Kminus_Dsminus", "angle_Kplus_Dsplus_Kminus_Dsminus", "angle_Kplus_Dsplus_Kminus_Dsminus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kplus_Dsplus_piminus_Dsminus", "angle_Kplus_Dsplus_piminus_Dsminus", "angle_Kplus_Dsplus_piminus_Dsminus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piplus_Dsplus_Kplus_Dsminus", "angle_piplus_Dsplus_Kplus_Dsminus", "angle_piplus_Dsplus_Kplus_Dsminus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piplus_Dsplus_Kminus_Dsminus", "angle_piplus_Dsplus_Kminus_Dsminus", "angle_piplus_Dsplus_Kminus_Dsminus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piplus_Dsplus_piminus_Dsminus", "angle_piplus_Dsplus_piminus_Dsminus", "angle_piplus_Dsplus_piminus_Dsminus", 0, 0.004, 50, false));
		Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("Kplus_Dsminus_MC15TuneV1_ProbNNk_corr", "Kplus_Dsminus_MC15TuneV1_ProbNNk_corr", "Kplus_Dsminus_MC15TuneV1_ProbNNk", 0.1, 1, 50, false));
		Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Dsminus_MC15TuneV1_ProbNNk_corr", "Kminus_Dsminus_MC15TuneV1_ProbNNk_corr", "Kminus_Dsminus_MC15TuneV1_ProbNNk", 0.1, 1, 50, false));
		Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("piminus_Dsminus_MC15TuneV1_ProbNNpi_corr", "piminus_Dsminus_MC15TuneV1_ProbNNpi", "piminus_Dsminus_MC15TuneV1_ProbNNpi", 0.1, 1, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("piminus_Dsminus_MC15TuneV1_ProbNNpi_corr", "piminus_Dsminus_MC15TuneV1_ProbNNpi_corr", "piminus_Dsminus_MC15TuneV1_ProbNNpi", 0.1, 1, 50, false));
		Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Dsplus_MC15TuneV1_ProbNNk_corr", "Kminus_Dsplus_MC15TuneV1_ProbNNk_corr", "Kminus_Dsplus_MC15TuneV1_ProbNNk", 0.1, 1, 50, false));
		Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("Kplus_Dsplus_MC15TuneV1_ProbNNk_corr", "Kplus_Dsplus_MC15TuneV1_ProbNNk_corr", "Kplus_Dsplus_MC15TuneV1_ProbNNk", 0.1, 1, 50, false));
		Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("piplus_Dsplus_MC15TuneV1_ProbNNpi_corr", "piplus_Dsplus_MC15TuneV1_ProbNNpi", "piplus_Dsplus_MC15TuneV1_ProbNNpi", 0.1, 1, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("piplus_Dsplus_MC15TuneV1_ProbNNpi_corr", "piplus_Dsplus_MC15TuneV1_ProbNNpi_corr", "piplus_Dsplus_MC15TuneV1_ProbNNpi", 0.1, 1, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("prodProbNNx_corr", "prodProbNNx", "prodProbNNx", 0.05, 1, 50, false));
		Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("prodProbNNx_corr", "Kplus_Dsminus_MC15TuneV1_ProbNNk_corr*Kminus_Dsminus_MC15TuneV1_ProbNNk_corr*piminus_Dsminus_MC15TuneV1_ProbNNpi_corr*Kminus_Dsplus_MC15TuneV1_ProbNNk_corr*Kplus_Dsplus_MC15TuneV1_ProbNNk_corr*piplus_Dsplus_MC15TuneV1_ProbNNpi_corr", "prodProbNNx", 0.05, 1, 50, false));
		Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("log_B_IPCHI2_OWNPV_corr", "log(B_IPCHI2_OWNPV)", "log(B_IPCHI2_OWNPV)", -6, 4, 50, false));
		Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("nTracks_corr", "nTracks", "nTracks", 0, 500, 50, false));
		Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("Kplus_Dsminus_PT", "Kplus_Dsminus_PT", "Kplus_Dsminus_PT", 0, 13000, 50, false));
		Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Dsminus_PT", "Kminus_Dsminus_PT", "Kminus_Dsminus_PT", 0, 13000, 50, false));
		Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("piminus_Dsminus_PT", "piminus_Dsminus_PT", "piminus_Dsminus_PT", 0, 13000, 50, false));
		Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Dsplus_PT", "Kminus_Dsplus_PT", "Kminus_Dsplus_PT", 0, 13000, 50, false));
		Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("Kplus_Dsplus_PT", "Kplus_Dsplus_PT", "Kplus_Dsplus_PT", 0, 13000, 50, false));
		Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("piplus_Dsplus_PT", "piplus_Dsplus_PT", "piplus_Dsplus_PT", 0, 13000, 50, false));
		Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("Kplus_Dsminus_ETA", "Kplus_Dsminus_ETA", "Kplus_Dsminus_ETA", 1.5, 5.5, 50, false));
		Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Dsminus_ETA", "Kminus_Dsminus_ETA", "Kminus_Dsminus_ETA", 1.5, 5.5, 50, false));
		Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("piminus_Dsminus_ETA", "piminus_Dsminus_ETA", "piminus_Dsminus_ETA", 1.5, 5.5, 50, false));
		Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Dsplus_ETA", "Kminus_Dsplus_ETA", "Kminus_Dsplus_ETA", 1.5, 5.5, 50, false));
		Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("Kplus_Dsplus_ETA", "Kplus_Dsplus_ETA", "Kplus_Dsplus_ETA", 1.5, 5.5, 50, false));
		Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("piplus_Dsplus_ETA", "piplus_Dsplus_ETA", "piplus_Dsplus_ETA", 1.5, 5.5, 50, false));

		treeNames.clear();
		drawTitles.clear();
		cuts.clear();
		weights.clear();
		colors.clear();
		drawOptions.clear();


		//drawTitles.push_back("sWeighted DATA");
		//drawOptions.push_back("E1");
		drawTitles.push_back("sWeighted DATA");
		drawOptions.push_back("E1");

		drawTitles.push_back("PIDcor & GBw MC");
		drawOptions.push_back("E1");

		//drawTitles.push_back("MC GBw");
		//drawOptions.push_back("E1");

		drawTitles.push_back("MC");
		drawOptions.push_back("E1");


		if(draw[2]){
			vector<vector<TH1D>> histosBs2DsDs;
			histosBs2DsDs.push_back(getVarsHistos1D("/mnt/d/lhcb/B2XcXc/root/Bs/data/Data_Bs2DsDs_Run2_TriggerSel_PreSel_sWeighted.root", yearCut, "(sig_sw)", kBlack, Bs2DsDs_HistoInfos));
			histosBs2DsDs.push_back(getVarsHistos1D("/mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bs2DsDs_Run2_truth_PIDcor_TrigSel_Presel.root", yearCut, "(data_MC_w)", kBlue, Bs2DsDs_HistoInfos_corr));
			histosBs2DsDs.push_back(getVarsHistos1D("/mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bs2DsDs_Run2_truth_PIDcor_TrigSel_Presel.root", yearCut, "(1)", kRed, Bs2DsDs_HistoInfos));


			TString plotDir_Bs2DsDs = "/mnt/d/lhcb/B2XcXc/public_html/compareDataMC/Bs2DsDs";
			if(year != 2) plotDir_Bs2DsDs += "/every_year/"+i2s(year);

			system("mkdir -p " + plotDir_Bs2DsDs);
			drawHistos(histosBs2DsDs,
					Bs2DsDs_HistoInfos_corr,
					drawTitles, drawOptions,
					legPos,
					plotDir_Bs2DsDs);

			if(year == 2){
				// log plots
				plotDir_Bs2DsDs = "/mnt/d/lhcb/B2XcXc/public_html/compareDataMC/Bs2DsDs/log_plots";
				for(int i=0; i<Bs2DsDs_HistoInfos.size(); i++){
					Bs2DsDs_HistoInfos[i].setLog = true;
					Bs2DsDs_HistoInfos_corr[i].setLog = true;
				}
				drawHistos(histosBs2DsDs,
					Bs2DsDs_HistoInfos_corr,
					drawTitles, drawOptions,
					legPos,
					plotDir_Bs2DsDs);
			}
		}

		// compare PID vars


	}
	//// 把参考道图片对比更新到note
	//system(" rm -rf /mnt/c/lhcb/LcLc-latex/figs/Corr_var/Nor_var/{Bd2DsD,Bs2DsDs} ");
	//system(" cp -r /mnt/d/lhcb/B2XcXc/public_html/compareDataMC/Bd2DsD /mnt/c/lhcb/LcLc-latex/figs/Corr_var/Nor_var/ ");
	//system(" cp -r /mnt/d/lhcb/B2XcXc/public_html/compareDataMC/Bs2DsDs /mnt/c/lhcb/LcLc-latex/figs/Corr_var/Nor_var/ ");
	gROOT->ProcessLine(".q");
}

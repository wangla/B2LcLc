#!/bin/bash

mkdir logs


# python3 gbweight.py swBsFileFile MCFile


#swBsFile="/mnt/d/lhcb/B2XcXc/root/Bs/data/Data_Bs2DsDs_Run2_TriggerSel_PreSel_sWeighted.root"
#n=0
##for files in /mnt/d/lhcb/B2XcXc/root/Bs/MC/{MC_Bs2DsDs,MC_Bs2LcLc}*Run2*{PIDcor,TrigSel,Presel}.root
##for files in /mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bs2DsDs*Run2*Presel.root
#for files in /mnt/d/lhcb/B2XcXc/root/Bs/MC/{MC_Bs2DsDs,MC_Bs2LcLc}*Run2*_s*.root
#do
#	nohup python3 gbweight.py ${swBsFile} ${files} > logs/log_gbweight_Bs_${n} &
#	((n++))
#done



swBdFile="/mnt/d/lhcb/B2XcXc/root/Bd/data/Data_Bd2DsD_Run2_TriggerSel_PreSel_sWeighted.root"
n=0

for files in /mnt/d/lhcb/B2XcXc/root/Bd/MC/{MC_Bd2DsD,MC_Bd2LcLc}*Run2*{PIDcor,TrigSel,Presel}.root
#for files in /mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2DsD*Run2*Presel.root
#for files in /mnt/d/lhcb/B2XcXc/root/Bd/MC/{MC_Bd2DsD,MC_Bd2LcLc}*Run2*_s*.root
do
	nohup python3 gbweight.py ${swBdFile} ${files} > logs/log_gbweight_Bd_${n} &
	#python3 gbweight.py ${swBdFile} ${files}
	((n++))
done



#n=0
#
#for((year=2015; year<=2018; year++))
#do
#	swBdFile="/mnt/d/lhcb/B2XcXc/root/Bd/data/Data_Bd2DsD_TriggerSel_PreSel_sWeighted_${year}.root"
#	for files in /mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2DsD_truth_PIDcor_TrigSel_Presel_${year}.root
#	do
#		nohup python3 gbweight.py ${swBdFile} ${files} > logs/log_gbweight_Bd_${n} &
#		((n++))
#	done
#done

#!/bin/bash

mkdir logs

BdTrainingDir="/mnt/d/lhcb/B2XcXc/BDTTraining/Bd2LcLc/"
BsTrainingDir="/mnt/d/lhcb/B2XcXc/BDTTraining/Bs2LcLc/"

#n=1
#
#cp /mnt/d/lhcb/B2XcXc/root/Bs/data/Data_Bs2LcLc_SB_Run2_TriggerSel_PreSel.root /mnt/d/lhcb/B2XcXc/root/Bd/data/Data_Bd2LcLc_SB_Run2_TriggerSel_PreSel.root
#for file in /mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2LcLc_Run2_truth_PIDcor_TrigSel_Presel.root /mnt/d/lhcb/B2XcXc/root/Bd/data/Data_Bd2LcLc_SB_Run2_TriggerSel_PreSel.root
#do
#	echo "apply MVA Weight to " ${file}
#	nohup /mnt/d/lhcb/B2XcXc/inc/applyTMVA.sh ${file} "" ${BdTrainingDir} > logs/log_applyTMVA_Bd_${n} &
#	((n++))
#done



n=1

for file in /mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bs2LcLc_Run2_truth_PIDcor_TrigSel_Presel.root
do
	echo "apply MVA Weight to " ${file}
	nohup /mnt/d/lhcb/B2XcXc/inc/applyTMVA.sh ${file} "" ${BsTrainingDir} > logs/log_applyTMVA_Bs_${n} &
	((n++))
done

for file in /mnt/d/lhcb/B2XcXc/root/Bs/data/{Data_Bs2LcLc_SB_Run2_TriggerSel_PreSel.root,Data_Bs2LcLc_Run2_TriggerSel_PreSel.root}
do
    echo "apply MVA Weight to " ${file}
    nohup /mnt/d/lhcb/B2XcXc/inc/applyTMVA.sh ${file} "" ${BsTrainingDir} > logs/log_applyTMVA_Bs_${n} &
    ((n++))
done

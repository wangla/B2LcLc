#include <iostream>
#include "RooStats/ModelConfig.h"
#include "RooStats/ProfileLikelihoodCalculator.h"
#include "RooStats/HypoTestResult.h"
#include <RooDataSet.h>

#include "/mnt/d/lhcb/B2XcXc/inc/tools.h"
#include "/mnt/d/lhcb/B2XcXc/inc/RooDoubleCB.h"
#include "/mnt/d/lhcb/B2XcXc/inc/fitMCtoGetShape.h"
#include "/mnt/d/lhcb/B2XcXc/inc/getPresel.h"

using namespace RooFit;
using namespace RooStats;

void fit(TString cutString )
{
	 //---------Build background PDF------
	lhcbStyle(0.063);
	 
	cout<< "define the expBkg"<<endl;
	 
	 
	vector<int> years;
	//years.push_back(2015);
	//years.push_back(2016);
	//years.push_back(1516);
	//years.push_back(2017);
	//years.push_back(2018);
	years.push_back(2);

	int w(7);
	system("mkdir -p /mnt/d/lhcb/B2XcXc/public_html/fitParamTables/sPlot/");

	TString fTableData = "/mnt/d/lhcb/B2XcXc/public_html/fitParamTables/sPlot/data_Bd2DsD.dat";

	TString fTableData1 = "/mnt/d/lhcb/B2XcXc/public_html/fitParamTables/sPlot/data_Bd2DsD_yields.dat";
	TString fTableData2 = "/mnt/d/lhcb/B2XcXc/public_html/fitParamTables/sPlot/data_Bd2DsD_meanSigma.dat";
	TString fTableData3 = "/mnt/d/lhcb/B2XcXc/public_html/fitParamTables/sPlot/data_Bd2DsD_bkgParams.dat";
	ofstream out_data_yields(fTableData1.Data());
	ofstream out_data_meanSigma(fTableData2.Data());
	ofstream out_data_bkgParams(fTableData3.Data());

	TString fTableMC = "/mnt/d/lhcb/B2XcXc/public_html/fitParamTables/sPlot/MC_Bd2DsD.dat";
	ofstream out_MC(fTableMC.Data());

	system("mkdir -p /mnt/d/lhcb/B2XcXc/Documents/yields/sPlot/");
	TString sYields = "/mnt/d/lhcb/B2XcXc/Documents/yields/sPlot/yields_Bd2DsD.root";

    ////********* 11 12, 3.0fb-1
    ////15, 16, 17, 18,  5.8fb-1
    double lumi1[2] = {1.0, 2.0};
    double lumi2[4] = {0.3, 1.6, 1.7, 2.2};
	for(int i=0; i<years.size(); i++){
		TString yearStr = "Run2";
		if(years[i] ==2) yearStr = "Run"+i2s(years[i]);
		else yearStr = i2s(years[i]);
		
		
		TString yearCut = "(1)";
		if(years[i] == 1112) yearCut = "(year == 2011 || year==2012)";
		else if(years[i] == 2) yearCut = "(year == 2015 || year==2016 || year == 2017 || year==2018)";
		else if(years[i] == 1516) yearCut = "(year == 2015 || year==2016)";
		else yearCut = "(year == " +i2s(years[i])+")";
	 
		TString fitVar = "B_DTFM_M_0";
		//TString fitVar = "B_M";
	 	RooRealVar m(fitVar, "#it{m}_{DsD}", 5150, 5450.);
		double binWidth = 4.0;
		int nBins = (int)(m.getMax() - m.getMin())/binWidth;
		m.setBins(nBins);
	 	

	 	RooRealVar tau("tau", "tau", -0.002, -1, 0.); 
	 	RooExponential expBkg("expBkg","background model",m,tau) ;
	 	
	 	TString datacut = fitVar+">"+d2s(m.getMin())+" && "+fitVar+"<"+d2s(m.getMax())+" && "+cutString;
		datacut += "&&" + yearCut;


	 	TChain chData;
		if((years[i]>=2015 && years[i]<=2018) || years[i]==1516 || years[i]==2) chData.Add("/mnt/d/lhcb/B2XcXc/root/Bd/data/Data_Bd2DsD_Run2_TriggerSel_PreSel.root/DecayTree");
		else chData.Add("/mnt/d/lhcb/B2XcXc/root/Bd/data/Data_Bd2DsD_Run1_TriggerSel_PreSel.root/DecayTree");
	 	TTree* datatree = chData.CopyTree(datacut);
	 	datatree->SetBranchStatus("*", 0);
	 	datatree->SetBranchStatus(fitVar, 1);
		setBranchStatusTTF(datatree, datacut);
	 	RooDataSet *datars = new RooDataSet("dataset", "dataset", datatree, m);

		system("mkdir -p /mnt/d/lhcb/B2XcXc/public_html/fit/sPlot/Bd2DsD");
		system("mkdir -p /mnt/d/lhcb/B2XcXc/public_html/fit/sPlot/Bd2DsD/Bs");
    	TPaveText text(0.15, 0.85, 0.35, 0.95, "NDC");
	 	text.SetFillColor(0);
	 	text.SetFillStyle(0);
	 	text.SetLineColor(0);
	 	text.SetLineWidth(0);
	 	text.SetBorderSize(1);
	 	text.SetTextSize(2*0.05);

		text.AddText(yearStr);

        // try Ipatia
        RooRealVar alphalBd("alphalBd","#alpha_{1}", 2.8, 0.01, 5.);
        RooRealVar alpharBd("alpharBd","#alpha_{2}", 4, 0.1, 5.);
        RooRealVar nlBd("nlBd","n_{1}", 1.5, 0., 10);
        RooRealVar nrBd("nrBd","n_{2}", 4.0, 0.01, 10);
        RooRealVar sigmaBd("sigmaBd","#sigmaBd", 8, 6, 15);
        //RooRealVar sigmaBd("sigmaBd","#sigmaBd", 15, 10, 20);
        RooRealVar meanBd("meanBd","#mu", 5280, 5275, 5285);
        RooRealVar beta_Bd("beta_Bd","#beta_Bd", 0);
        RooRealVar zeta_Bd("zeta_Bd","#zeta_Bd", 1e-5);
        //RooRealVar l_Bd("l_Bd", "l_Bd", -3, -50, 0.);
        RooRealVar l_Bd("l_Bd", "l_Bd", -3, -50, 0.);

        if(years[i] == 2015) {
            alpharBd.setMax(10);
        }

		RooIpatia2 modelBd("modelBd", "modelBd", m, l_Bd, zeta_Bd, beta_Bd, sigmaBd, meanBd, alphalBd, nlBd, alpharBd, nrBd);


		//// B_M no right tail, try signal CB or with gaus.
		//RooRealVar alphalBd("alphalBd","alphalBd", 2.0, 1, 5.);
		//RooRealVar nlBd("nlBd","nlBd", 2.0, 0.1, 10);
		//RooRealVar sigmaBd("sigmaBd","#sigmaBd", 11.5, 8, 15);
		//RooRealVar meanBd("meanBd","#mu", 5280, 5275, 5285);

		//RooRealVar fSigmaBd("fSigmaBd","fSigmaBd", 1.7, 0.8, 3);
		//RooFormulaVar sigma_gausCB_Bd("sigma_gausCB_Bd", "sigma_gausCB_Bd", "@0*@1", RooArgList(sigmaBd, fSigmaBd));
		//RooGaussian gausCB_Bd("gausCB_Bd", "gausCB_Bd", m, meanBd, sigma_gausCB_Bd);

		//RooCBShape cbBd("cbBd", "cbBd", m, meanBd, sigmaBd, alphalBd, nlBd);

		//RooRealVar fCB_Bd("fCB_Bd", "fCB_Bd", 0.7, 0.5, 0.9999);
		//RooAddPdf modelBd("modelBd", "modelBd", RooArgList(cbBd, gausCB_Bd), RooArgList(fCB_Bd));


		//// B_DTFM_M with right tail, try 2CB
		//RooRealVar alphalBd("alphalBd","alphalBd", 1.5, 0.01, 2.);
		//RooRealVar nlBd("nlBd","nlBd", 2.0, 1, 15);
		//RooRealVar alpharBd("alpharBd", "alpharBd", -1.5, -2, -0.01);
		//RooRealVar nrBd("nrBd","nrBd", 2.0, 1, 15);
		//RooRealVar sigma1Bd("sigma1Bd","#sigma1Bd", 7, 5, 10);
		//RooRealVar sigma2Bd("sigma2Bd","#sigma2Bd", 7, 5, 10);
		//RooRealVar meanBd("meanBd","#mu", 5280, 5275, 5285);


		//RooCBShape cb1Bd("cb1Bd", "cb1Bd", m, meanBd, sigma1Bd, alphalBd, nlBd);
		//RooCBShape cb2Bd("cb2Bd", "cb2Bd", m, meanBd, sigma2Bd, alpharBd, nrBd);

		////RooRealVar fracCB1_Bd("fracCB1_Bd", "fracCB1_Bd", 0.5, 0.01, 0.9999);
		//RooRealVar fracCB1_Bd("fracCB1_Bd", "fracCB1_Bd", 0.5);
		//RooAddPdf modelBd("modelBd", "modelBd", RooArgList(cb1Bd, cb2Bd), RooArgList(fracCB1_Bd));


		//fitMCBMass_addPdf("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2DsD_Run2_truth_PIDcor_TrigSel_Presel.root", fitVar, datacut, "1", m.getMin(), m.getMax(), binWidth,
		//	modelBd, RooArgList(cb1Bd, cb2Bd),
		fitMCBMass("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2DsD_Run2_truth_PIDcor_TrigSel_Presel.root", fitVar, datacut, "1", m.getMin(), m.getMax(), binWidth,
			modelBd, 5230, 5340,
			//modelBd, 5150, 5450,
			fitVar, text, "/mnt/d/lhcb/B2XcXc/public_html/fit/sPlot/Bd2DsD/MC_Bd2DsD_"+yearStr+".pdf", false, false);

		out_MC<<setw(w)<<yearStr;
		out_MC<<" & "<<setw(w)<<"$"<<dbl2str(meanBd.getVal(),2)<<"\\pm"<<dbl2str(meanBd.getError(),2)<<"$";
		out_MC<<" & "<<setw(w)<<"$"<<dbl2str(sigmaBd.getVal(),2)<<"\\pm"<<dbl2str(sigmaBd.getError(),2)<<"$";
		//out_MC<<" & "<<setw(w)<<"$"<<dbl2str(sigma1Bd.getVal(),2)<<"\\pm"<<dbl2str(sigma1Bd.getError(),2)<<"$";
		//out_MC<<" & "<<setw(w)<<"$"<<dbl2str(sigma2Bd.getVal(),2)<<"\\pm"<<dbl2str(sigma2Bd.getError(),2)<<"$";
		out_MC<<" & "<<setw(w)<<"$"<<dbl2str(nlBd.getVal(),2)<<"\\pm"<<dbl2str(nlBd.getError(),2)<<"$";
		out_MC<<" & "<<setw(w)<<"$"<<dbl2str(nrBd.getVal(),2)<<"\\pm"<<dbl2str(nrBd.getError(),2)<<"$";
		out_MC<<" & "<<setw(w)<<"$"<<dbl2str(alphalBd.getVal(),2)<<"\\pm"<<dbl2str(alphalBd.getError(),2)<<"$";
		out_MC<<" & "<<setw(w)<<"$"<<dbl2str(alpharBd.getVal(),2)<<"\\pm"<<dbl2str(alpharBd.getError(),2)<<"$";


		//out_MC<<" & "<<setw(w)<<"$"<<dbl2str(fracCB1_Bd.getVal(),2)<<"\\pm"<<dbl2str(fracCB1_Bd.getError(),2)<<"$";
		//out_MC<<" & "<<setw(w)<<"$"<<dbl2str(fSigmaBd.getVal(),2)<<"\\pm"<<dbl2str(fSigmaBd.getError(),2)<<"$";
		//out_MC<<" & "<<setw(w)<<"$"<<dbl2str(fCB_Bd.getVal(),2)<<"\\pm"<<dbl2str(fCB_Bd.getError(),2)<<"$";
		out_MC<<" & "<<setw(w)<<"$"<<dbl2str(l_Bd.getVal(),2)<<"\\pm"<<dbl2str(l_Bd.getError(),2)<<"$";
		//out_MC<<" & "<<setw(w)<<"$"<<dbl2str(zeta_Bd.getVal(),2)<<"\\pm"<<dbl2str(zeta_Bd.getError(),2)<<"$";
		//out_MC<<" & "<<setw(w)<<"$"<<dbl2str(10*beta_Bd.getVal(),2)<<"\\pm"<<dbl2str(10*beta_Bd.getError(),2)<<"$";
		out_MC<<"\\\\"<<endl;
	



		//// B_M no right tail, try signal CB or with gaus.
		//RooRealVar alphalBs("alphalBs","alphalBs", 2.0, 1, 5.);
		//RooRealVar nlBs("nlBs","nlBs", 2.0, 0.1, 10);
		//RooRealVar sigmaBs("sigmaBs","#sigmaBs", 11.5, 8, 15);
		//RooRealVar deltaM("deltaM", "deltaM", 87.38);
    	//RooFormulaVar meanBs("meanBs","meanBs", "@0+@1", RooArgList(meanBd, deltaM));

		//RooRealVar fSigmaBs("fSigmaBs","fSigmaBs", 1.7, 0.8, 3);
		//RooFormulaVar sigma_gausCB_Bs("sigma_gausCB_Bs", "sigma_gausCB_Bs", "@0*@1", RooArgList(sigmaBs, fSigmaBs));
		//RooGaussian gausCB_Bs("gausCB_Bs", "gausCB_Bs", m, meanBs, sigma_gausCB_Bs);

		//RooCBShape cbBs("cbBs", "cbBs", m, meanBs, sigmaBs, alphalBs, nlBs);

		//RooRealVar fCB_Bs("fCB_Bs", "fCB_Bs", 0.7, 0.5, 0.9999);
		//RooAddPdf modelBs("modelBs", "modelBs", RooArgList(cbBs, gausCB_Bs), RooArgList(fCB_Bs));

        // try Ipatia
        RooRealVar alphalBs("alphalBs","#alpha_{1}", 2.8, 0.01, 5.);
        RooRealVar alpharBs("alpharBs","#alpha_{2}", 4, 0.1, 5.);
        RooRealVar nlBs("nlBs","n_{1}", 1.5, 0., 10);
        RooRealVar nrBs("nrBs","n_{2}", 4.0, 0.01, 10);
        RooRealVar sigmaBs("sigmaBs","#sigmaBs", 8, 6, 15);
        //RooRealVar sigmaBs("sigmaBs","#sigmaBs", 15, 8, 25);
        RooRealVar deltaM("deltaM", "deltaM", 87.38);
        RooFormulaVar meanBs("meanBs","meanBs", "@0+@1", RooArgList(meanBd, deltaM));
        RooRealVar beta_Bs("beta_Bs","#beta_Bs", 0);
        RooRealVar zeta_Bs("zeta_Bs","#zeta_Bs", 1e-5);
        RooRealVar l_Bs("l_Bs", "l_Bs", -3, -50, 0.);
        RooIpatia2 modelBs("ipatiaBs", "ipatiaBs", m, l_Bs, zeta_Bs, beta_Bs, sigmaBs, meanBs, alphalBs, nlBs, alpharBs, nrBs);

		//fitMCBMass_addPdf("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bs2DsD_Run2_truth_PIDcor_TrigSel_Presel.root", fitvar, datacut+"rdmNumber<0.2", "1", m.getMin(), m.getMax(), binWidth,
		//	modelBs, RooArgList(cbBs, gausCB_Bs), 5250, 5450,
		fitMCBMass("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bs2DsD_Run2_truth_PIDcor_TrigSel_Presel.root", fitVar, datacut+"rdmNumber<0.2", "1", m.getMin(), m.getMax(), binWidth,
			modelBs, 5250, 5450,
			fitVar, text, "/mnt/d/lhcb/B2XcXc/public_html/fit/sPlot/Bd2DsD/Bs/MC_Bs2DsD_"+yearStr+".pdf", false, true);




		alphalBd.setConstant(true);
		alpharBd.setConstant(true);
		nlBd.setConstant(true);
		nrBd.setConstant(true);
		sigmaBd.setConstant(true);
		//sigma1Bd.setConstant(true);
		//sigma2Bd.setConstant(true);
		//meanBd.setConstant(true);

		//fracCB1_Bd.setConstant(true);
		//beta_Bd.setConstant(true);
		//zeta_Bd.setConstant(true);
		l_Bd.setConstant(true);
		////fCB_Bd.setConstant(true);



		alphalBs.setConstant(true);
		alpharBs.setConstant(true);
		nlBs.setConstant(true);
		nrBs.setConstant(true);
		sigmaBs.setConstant(true);
		//meanBs.setConstant(true);
		//beta_Bs.setConstant(true);
		//zeta_Bs.setConstant(true);
		l_Bs.setConstant(true);
 
		//alphalBs.setConstant(true);
		//alpharBs.setConstant(true);
		//nlBs.setConstant(true);
		//nrBs.setConstant(true);
		////sigmaBs.setConstant(true);
		////meanBs.setConstant(true);
		////fSigmaBs.setConstant(true);
		////fCB_Bs.setConstant(true);







        RooRealVar fSigmaDataMC("fSigmaDataMC", "fSigmaDataMC", 1.1, 0.8, 1.5);


        //RooFormulaVar sigma1BdData("sigma1BdData", "sigma1BdData", "@0*@1", RooArgList(sigma1Bd, fSigmaDataMC));
        //RooFormulaVar sigma2BdData("sigma2BdData", "sigma2BdData", "@0*@1", RooArgList(sigma2Bd, fSigmaDataMC));
        //RooCBShape cb1BdData("cb1BdData", "cb1BdData", m, meanBd, sigma1BdData, alphalBd, nlBd);
        //RooCBShape cb2BdData("cb2BdData", "cb2BdData", m, meanBd, sigma2BdData, alpharBd, nrBd);
        //RooAddPdf modelBdData("modelBdData", "modelBdData", RooArgList(cb1BdData, cb2BdData), RooArgList(fracCB1_Bd));

        //RooFormulaVar sigma1BsData("sigma1BsData", "sigma1BsData", "@0*@1", RooArgList(sigma1Bd, fSigmaDataMC));
        //RooFormulaVar sigma2BsData("sigma2BsData", "sigma2BsData", "@0*@1", RooArgList(sigma2Bd, fSigmaDataMC));
        //RooCBShape cb1BsData("cb1BsData", "cb1BsData", m, meanBs, sigma1BdData, alphalBs, nlBs);
        //RooCBShape cb2BsData("cb2BsData", "cb2BsData", m, meanBs, sigma2BdData, alpharBs, nrBs);
        //RooAddPdf modelBsData("modelBsData", "modelBsData", RooArgList(cb1BsData, cb2BsData), RooArgList(fracCB1_Bs));

        RooFormulaVar sigmaBdData("sigmaBdData", "sigmaBdData", "@0*@1", RooArgList(sigmaBd, fSigmaDataMC));
        RooFormulaVar sigmaBsData("sigmaBsData", "sigmaBsData", "@0*@1", RooArgList(sigmaBs, fSigmaDataMC));

		RooRealVar sigmaNonDD("sigmaNonDD", "sigmaNonDD", 17.95);
		//RooRealVar sigmaNonDD("sigmaNonDD", "sigmaNonDD", 18, 15, 30);
		RooGaussian modelNonDD("modelNonDD", "modelNonDD", m, meanBd, sigmaNonDD);

        RooIpatia2 modelBdData("modelBdData", "modelBdData", m, l_Bd, zeta_Bd, beta_Bd, sigmaBdData, meanBd, alphalBd, nlBd, alpharBd, nrBd);
        RooIpatia2 modelBsData("modelBsData", "modelBsData", m, l_Bs, zeta_Bs, beta_Bs, sigmaBsData, meanBs, alphalBs, nlBs, alpharBs, nrBs);

        //RooIpatia2 ipatiaBdData("ipatiaBdData", "ipatiaBdData", m, l_Bd, zeta_Bd, beta_Bd, sigmaBdData, meanBd, alphalBd, nlBd, alpharBd, nrBd);
        //RooFormulaVar sigma_gausCB_BdData("sigma_gausCB_BdData", "sigma_gausCB_BdData", "@0*@1", RooArgList(sigma_gausCB_Bd, fSigmaDataMC));
        //RooGaussian gausCB_BdData("gausCB_BdData", "gausCB_BdData", m, meanBd, sigma_gausCB_BdData);

        //RooAddPdf modelBdData("modelBdData", "modelBdData", RooArgList(ipatiaBdData, gausCB_BdData), RooArgList(fIpatia_Bd));

        //RooIpatia2 modelBsData("modelBsData", "modelBsData", m, l_Bs, zeta_Bs, beta_Bs, sigmaBdData, meanBs, alphalBs, nlBs, alpharBs, nrBs);





        //RooRealVar m0("m0","m0", 5210, 5200, 5230);
        RooRealVar m0("m0","m0", 5210, 5200, 5250);
        RooRealVar c("c","c", 5, -50, 50);
        RooArgusBG argus_noSmearing("argus_noSmearing","prc background", m, m0, c);


        // Detector response function
        RooRealVar m0_g("m0_g", "m0_g", 0) ;
        //RooFormulaVar sigmaGaus("sigmaGaus", "sigmaGaus", "@0*@2+@1*(1-@2)", RooArgList(sigma1Bd, sigma2Bd, fracCB1_Bd));
        RooFormulaVar sigmaGaus("sigmaGaus", "sigmaGaus", "@0", RooArgList(sigmaBdData));
        //RooGaussian gauss("gauss", "gauss", m, m0_g, sigmaGaus);
        RooGaussian gauss("gauss", "gauss", m, m0_g, sigmaBd);


        RooFFTConvPdf argusBkg("argusBkg","argusBkg (X) gauss", m, argus_noSmearing, gauss);
        argusBkg.setBufferFraction(0.35);


		RooRealVar *nNonDD = new RooRealVar("nNonDD", "nNonDD", 0);
		
		if(years[i] == 2) nNonDD->setVal(1029);
		if(years[i] == 1516) nNonDD->setVal( 1029 * 1.9/5.8);
		else if(years[i]>= 2015 && years[i]<= 2018) nNonDD->setVal( 623.87 * lumi2[years[i]-2015]/5.8);
		else if(years[i] == 1112) nNonDD->setVal(0);
		else if(years[i]>= 2011 && years[i]<= 2012) nNonDD->setVal( 0 * lumi1[years[i]-2011]/3.0);


	 	RooRealVar *nBd = new RooRealVar("nBd","Number of signal Bd events", 30000, 0., 1e7);
		//RooRealVar fracNonDD("fracNonDD", "fracNonDD", 0.01, 0, 0.05);
		//RooRealVar fracNonDD("fracNonDD", "fracNonDD", 0.0371);
		//RooFormulaVar *nNonDDFormula = new RooFormulaVar("nNonDDFormula","Number of nonDD Bd events", "@0*@1", RooArgList(*nBd, fracNonDD));

	 	RooRealVar *nBs = new RooRealVar("nBs","Number of signal Bs bkg", 1000, 0., 5000);
	 	RooRealVar *nExpBkg = new RooRealVar("nExpBkg","Number of expBkg events",2800, 0., 1e7);
	 	RooRealVar *nArgusBkg = new RooRealVar("nArgusBkg","Number of Rec expBkg events",2800, 0., 1e7);
	
	 	//RooAddPdf* totPDF = new RooAddPdf("totPDF","Total pdf", RooArgList(modelBdData, modelNonDD, modelBsData, expBkg, argusBkg), RooArgList(*nBd, *nNonDDFormula, *nBs, *nExpBkg, *nArgusBkg));
	 	RooAddPdf* totPDF = new RooAddPdf("totPDF","Total pdf", RooArgList(modelBdData, modelNonDD, modelBsData, expBkg, argusBkg), RooArgList(*nBd, *nNonDD, *nBs, *nExpBkg, *nArgusBkg));
	 	//RooAddPdf* totPDF = new RooAddPdf("totPDF","Total pdf", RooArgList(modelBdData, modelNonDD, modelBsData, expBkg), RooArgList(*nBd, *nNonDD, *nBs, *nExpBkg));
	 	//RooAddPdf* totPDF = new RooAddPdf("totPDF","Total pdf", RooArgList(modelBdData, modelBsData, expBkg), RooArgList(*nBd, *nBs, *nExpBkg));


	 	cout<<"all ready"<<endl;
	 	//////////////////// Fit the data/////////////
	 	RooFitResult *result = totPDF->fitTo(*datars, Save(true), NumCPU(10));


		//ValError nNonDDVE = Roo2VE(*nBd)*Roo2VE(fracNonDD);
	 	//RooRealVar *nNonDD = new RooRealVar("nNonDD", "nNonDD", 30000, 0., 1e7);
		//nNonDD->setVal(nNonDDVE.val);
		//nNonDD->setError(nNonDDVE.err);

		out_data_yields<<setw(w)<<yearStr;
		out_data_yields<<" & "<<setw(w)<<"$"<<dbl2str(nBd->getVal(),0)<<"\\pm"<<dbl2str(nBd->getError(),0)<<"$";
		out_data_yields<<" & "<<setw(w)<<"$"<<dbl2str(nNonDD->getVal(),0)<<"\\pm"<<dbl2str(nNonDD->getError(),0)<<"$";
		out_data_yields<<" & "<<setw(w)<<"$"<<dbl2str(nBs->getVal(),0)<<"\\pm"<<dbl2str(nBs->getError(),0)<<"$";
		out_data_yields<<" & "<<setw(w)<<"$"<<dbl2str(nExpBkg->getVal(),0)<<"\\pm"<<dbl2str(nExpBkg->getError(),0)<<"$";
		out_data_yields<<" & "<<setw(w)<<"$"<<dbl2str(nArgusBkg->getVal(),0)<<"\\pm"<<dbl2str(nArgusBkg->getError(),0)<<"$";
		out_data_yields<<"\\\\"<<endl;
		out_data_meanSigma<<setw(w)<<yearStr;
		out_data_meanSigma<<" & "<<setw(w)<<"$"<<dbl2str(meanBd.getVal(),1)<<"\\pm"<<dbl2str(meanBd.getError(),1)<<"$";
		out_data_meanSigma<<" & "<<setw(w)<<"$"<<dbl2str(fSigmaDataMC.getVal(),2)<<"\\pm"<<dbl2str(fSigmaDataMC.getError(),2)<<"$";
		out_data_meanSigma<<" & "<<setw(w)<<"$"<<dbl2str(sigmaNonDD.getVal(),1)<<"\\pm"<<dbl2str(sigmaNonDD.getError(),1)<<"$";
		out_data_meanSigma<<"\\\\"<<endl;
		out_data_bkgParams<<setw(w)<<yearStr;
		out_data_bkgParams<<" & "<<setw(w)<<"$"<<dbl2str(tau.getVal()*1e2,1)<<"\\pm"<<dbl2str(tau.getError()*1e2,1)<<"$";
		out_data_bkgParams<<" & "<<setw(w)<<"$"<<dbl2str(m0.getVal(),1)<<"\\pm"<<dbl2str(m0.getError(),1)<<"$";
		out_data_bkgParams<<" & "<<setw(w)<<"$"<<dbl2str(c.getVal(),1)<<"\\pm"<<dbl2str(c.getError(),1)<<"$";
		out_data_bkgParams<<"\\\\"<<endl;



        int nBin;
        if(years[i] == 2011) nBin = 1;
        if(years[i] == 2012) nBin = 2;
        if(years[i] == 1112) nBin = 3;
        if(years[i] == 2015) nBin = 4;
        if(years[i] == 2016) nBin = 5;
        if(years[i] == 1516) nBin = 6;
        if(years[i] == 2017) nBin = 7;
        if(years[i] == 2018) nBin = 8;
        if(years[i] == 2) nBin = 9;	 	

		// save yields
		TFile fYields(sYields, "UPDATE");

		TH1D* hYields = (TH1D*)fYields.Get("yields");
		if(!hYields) hYields = new TH1D ("yields", "yields", 9, 0, 9);
		
		hYields->SetBinContent(nBin, nBd->getVal());
		hYields->SetBinError(nBin, nBd->getError());
		
		fYields.cd();
		hYields->Write("yields", TObject::kOverwrite);
		fYields.Close();

	 	

	 	RooPlot *mframe = m.frame(Title("Bd2DsD Run2 data"));
	 	
	 	cout<<"Fit the data"<<endl;
	 	
	 	datars->plotOn(mframe, Name("data"));
	 	totPDF->plotOn(mframe, Name("signalBd"), Components(modelBdData), LineColor(3), LineStyle(kDashed));
	 	totPDF->plotOn(mframe, Name("NonDD"), Components(modelNonDD), LineColor(9), LineStyle(kDashed));
	 	totPDF->plotOn(mframe, Name("Bs"), Components(modelBsData), LineColor(6), LineStyle(kDashed));
	 	totPDF->plotOn(mframe, Name("comb"), Components(expBkg), LineColor(4), LineStyle(kDashed));
	 	totPDF->plotOn(mframe, Name("prc"), Components(argusBkg), LineColor(kOrange), LineStyle(kDashed));
	 	totPDF->plotOn(mframe, Name("totPDF"), LineColor(kRed));
	 	datars->plotOn(mframe, Name("data"));
	 	//totPDF->paramOn(mframe, Layout(0.65, 0.95, 0.95));

		int result_status = result->status();
		cout << "result_status = "<< result_status<<endl;
		result->Print("v");
		
		
		RooHist *pull = mframe->pullHist();
		pull->GetYaxis()->SetTitle("Pull");
		RooPlot *framePull = m.frame();
		framePull->addPlotable(pull, "P");
		
		mframe->SetTitle(fitVar);
		mframe->SetXTitle("#it{m}_{B} [MeV/c^{2}]");
		mframe->SetYTitle(Form("Candidates / (%.1lf MeV/c^{2})", binWidth));
		mframe->GetXaxis()->SetTitleSize(0.2);
		mframe->GetYaxis()->SetTitleSize(0.2);
		
		framePull->SetTitle("");
		framePull->GetYaxis()->SetTitle("Pull");
		framePull->SetMinimum(-6.);
		framePull->SetMaximum(6.);
		framePull->SetMarkerStyle(2);
		//cout<<"-----------------1"<<endl;
		
		TCanvas *canv = new TCanvas("canv", "canv", 800, 600);
		canv->SetFillColor(10);
		canv->SetBorderMode(0);
		canv->cd();
		canv->cd(1);
		
		TPad *p1 = new TPad("pad1", "pad1", 0.02, 0.2, 0.98, 0.98, 0);
		TPad *p2 = new TPad("pad2", "pad2", 0.02, 0.04, 0.98, 0.2, 0);
		p1->Draw();
		p2->Draw();
		
		p1->cd();
		mframe->GetYaxis()->SetTitleOffset(1.00);
		mframe->GetYaxis()->SetLabelSize(0.05);
		mframe->GetYaxis()->SetTitleSize(0.05);
		mframe->GetYaxis()->SetNoExponent();
		mframe->GetXaxis()->SetTitleOffset(1.00);
		mframe->GetXaxis()->SetTitleSize(0.05);
		mframe->GetXaxis()->SetLabelSize(0.05);
		mframe->Draw("E1");
		text.Draw("same");


        TLegend leg(0.68, 0.55, 0.93, 0.93);
        leg.SetFillStyle(0);
        leg.AddEntry("signalBd", "#it{B^{0}#rightarrow D^{+}_{s}D^{-}}", "l");
        leg.AddEntry("NonDD", "#it{B^{0}#rightarrow D^{-}K^{+}K^{-}#pi^{+}}", "l");
        leg.AddEntry("Bs", "#it{B^{0}_{s}#rightarrow D^{+}_{s}D^{-}}", "l");
        leg.AddEntry("comb", "Comb. bkg", "l");
        leg.AddEntry("prc", "Prc. bkg", "l");


        leg.SetTextSize(0.85*mframe->GetYaxis()->GetTitleSize());
        leg.SetLineColor(0);
        leg.SetFillColor(0);
        leg.Draw("same");

		
		p2->cd();
		p2->SetTickx();
		framePull->GetYaxis()->SetTitleOffset(0.17);
		framePull->GetXaxis()->SetTitleSize(0.0);
		framePull->GetXaxis()->SetTickLength(0.09);
		framePull->GetYaxis()->SetTitleSize(0.2);
		framePull->GetXaxis()->SetLabelSize(0.0);
		framePull->GetYaxis()->SetLabelSize(0.2);
		framePull->GetYaxis()->SetNdivisions(5);
		framePull->Draw("E1");
		
		TLine* lineZero = new TLine(m.getMin(), 0, m.getMax(), 0);
		lineZero->SetLineStyle(kDashed);
		lineZero->SetLineColor(kBlack);
		lineZero->Draw();
		
		TLine* lineTwoSigUp = new TLine(m.getMin(), 3, m.getMax(), 3);
		lineTwoSigUp->SetLineColor(kRed);
		lineTwoSigUp->Draw();
		TLine* lineTwoSigDown = new TLine(m.getMin(), -3, m.getMax(), -3);
		lineTwoSigDown->SetLineColor(kRed);
		lineTwoSigDown->Draw();


	 	canv->SaveAs("/mnt/d/lhcb/B2XcXc/public_html/fit/sPlot/Bd2DsD/"+yearStr+"_data_Bd2DsD_sPlot.pdf");

		p1->SetLogy();
		canv->SaveAs("/mnt/d/lhcb/B2XcXc/public_html/fit/sPlot/Bd2DsD/"+yearStr+"_data_Bd2DsD_sPlot_log.pdf");

		//if(years[i] == 2 || years[i] == 1112){
    	//	datatree->SetBranchStatus("*", 1);

		//	RooAddPdf* modelSPlot = new RooAddPdf("modelSPlot","modelSPlot", RooArgList(modelBdData, modelNonDD, modelBsData, expBkg, argusBkg), RooArgList(*nBd, *nNonDD, *nBs, *nExpBkg, *nArgusBkg));
    	//	RooStats::SPlot* splot = new RooStats::SPlot("splot","splot", *datars, modelSPlot, RooArgList(*nBd, *nNonDD, *nBs, *nExpBkg, *nArgusBkg));


    	//	datars->Print("v");
    	//	splot->Print();
    	//	double  sig_sw, exp_sw, prc_sw;
    	//	TString rootname = "/mnt/d/lhcb/B2XcXc/root/Bd/data/Data_Bd2DsD_Run2_TriggerSel_PreSel_sWeighted.root";
		//	if(years[i] == 1112) rootname = "/mnt/d/lhcb/B2XcXc/root/Bd/data/Data_Bd2DsD_Run1_TriggerSel_PreSel_sWeighted.root";
    	//	TFile fSW(rootname, "RECREATE");
    	//	TTree *swTree = datatree->CloneTree(0);
    	//	swTree->Branch("sig_sw",&sig_sw, "sig_sw/D");
    	//	//swTree->Branch("exp_sw", &exp_sw, "exp_sw/D");
    	//	//swTree->Branch("prc_sw", &prc_sw, "prc_sw/D");
    	//	int index = 0;
    	//	for(int i=0;i<datatree->GetEntries();i++){
    	//		datatree->GetEntry(i);
    	//		index++;
    	//		const RooArgSet* row = datars->get(index-1);
    	//		RooRealVar *sig_sw_row = (RooRealVar*)row->find("nBd_sw");
    	//		//RooRealVar *exp_sw_row = (RooRealVar*)row->find("nExpBkg_sw");
    	//		//RooRealVar *prc_sw_row = (RooRealVar*)row->find("nArgusBkg_sw");
    	//	
    	//		sig_sw = sig_sw_row->getVal();
    	//		//exp_sw = exp_sw_row->getVal();
    	//		//prc_sw = prc_sw_row->getVal();
    	//		swTree->Fill();
    	//	}
    	//	swTree->Write();
    	//	fSW.Close();
		//	cout << "saved sweighted root: "+ rootname << endl;
		//}
	}

	out_MC.close();
	out_data_yields.close();
	out_data_meanSigma.close();
	out_data_bkgParams.close();


	cout << endl << "params print to "+fTableMC+" "<< endl;
	system("cat "+fTableMC);

	cout << endl << "params print to "+fTableData1+" "<< endl;
	system("rm " + fTableData);

	system("cat "+fTableData1+">>"+fTableData);
	system("cat "+fTableData2+">>"+fTableData);
	system("cat "+fTableData3+">>"+fTableData);

	system("rm /mnt/d/lhcb/B2XcXc/public_html/fitParamTables/sPlot/data_Bd2DsD_*.dat");
	system("cat "+fTableData);

	cout << "yields saved into " + sYields << endl;
}


void fit_data_Bd2DsD_sWeight()
{
	fit("(1)");
	gROOT->ProcessLine(".q");
}

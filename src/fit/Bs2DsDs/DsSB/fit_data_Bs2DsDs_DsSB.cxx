#include <iostream>
#include "RooStats/ModelConfig.h"
#include "RooStats/ProfileLikelihoodCalculator.h"
#include "RooStats/HypoTestResult.h"
#include <RooDataSet.h>

#include "/mnt/d/lhcb/B2XcXc/inc/tools.h"
#include "/mnt/d/lhcb/B2XcXc/inc/RooDoubleCB.h"
#include "/mnt/d/lhcb/B2XcXc/inc/fitMCtoGetShape.h"
#include "/mnt/d/lhcb/B2XcXc/inc/fitExponentialBkg.h"
#include "/mnt/d/lhcb/B2XcXc/inc/getPresel.h"

using namespace RooFit;
using namespace RooStats;

void fit(TString cutString)
{
	 //---------Build background PDF------
	lhcbStyle(0.063);
	 
	cout<< "define the expBkg"<<endl;
	 
	 
	vector<int> years;
	years.push_back(-1);

	int w(7);
	system("mkdir -p /mnt/d/lhcb/B2XcXc/public_html/fitParamTables/sPlot/");

	TString fTableData = "/mnt/d/lhcb/B2XcXc/public_html/fitParamTables/sPlot/data_Bs2DsDs_DsSB.dat";

	TString fTableData1 = "/mnt/d/lhcb/B2XcXc/public_html/fitParamTables/sPlot/data_Bs2DsDs_DsSB_yields.dat";
	TString fTableData2 = "/mnt/d/lhcb/B2XcXc/public_html/fitParamTables/sPlot/data_Bs2DsDs_DsSB_meanSigma.dat";
	TString fTableData3 = "/mnt/d/lhcb/B2XcXc/public_html/fitParamTables/sPlot/data_Bs2DsDs_DsSB_bkgParams.dat";
	ofstream out_data_yields(fTableData1.Data());
	ofstream out_data_meanSigma(fTableData2.Data());
	ofstream out_data_bkgParams(fTableData3.Data());

	TString fTableMC = "/mnt/d/lhcb/B2XcXc/public_html/fitParamTables/sPlot/MC_Bs2DsDs_DsSB.dat";
	ofstream out_MC(fTableMC.Data());


	for(int i=0; i<years.size(); i++){
		TString yearStr = "Run2";
		if(years[i] != -1) yearStr = i2s(years[i]);

		TString yearCut = "(1)";
		if(years[i] != -1) yearCut = "year == " + i2s(years[i]);

	 
		TString fitVar = "B_M";
		//TString fitVar = "B_DTFM_M_0";
	 	RooRealVar m(fitVar, "#it{m}_{DsD}", 5200, 5550.);
		double binWidth = 4.0;
		int nBins = (int)(m.getMax() - m.getMin())/binWidth;
		m.setBins(nBins);
	 	

	 	RooRealVar tau("tau", "tau", -0.002, -1, 0.); 
	 	RooExponential expBkg("expBkg","background model",m,tau) ;
	 	
	 	TString datacut = fitVar+">"+d2s(m.getMin())+" && "+fitVar+"<"+d2s(m.getMax())+" && "+cutString;
		datacut += "&&" + yearCut;


	 	TChain chData;
		chData.Add("/mnt/d/lhcb/B2XcXc/root/Bs/data/Data_Bs2DsDs_Run2_TriggerSel_DsSB_PreSel.root/DecayTree");
	 	TTree* datatree = chData.CopyTree(datacut);
	 	datatree->SetBranchStatus("*", 0);
	 	datatree->SetBranchStatus(fitVar, 1);
		setBranchStatusTTF(datatree, datacut);
	 	RooDataSet *datars = new RooDataSet("dataset", "dataset", datatree, m);

		system("mkdir -p /mnt/d/lhcb/B2XcXc/public_html/fit/sPlot/Bs2DsDs/DsSB");
		system("mkdir -p /mnt/d/lhcb/B2XcXc/public_html/fit/sPlot/Bs2DsDs/DsSB/Bs");
    	TPaveText text(0.15, 0.85, 0.35, 0.95, "NDC");
	 	text.SetFillColor(0);
	 	text.SetFillStyle(0);
	 	text.SetLineColor(0);
	 	text.SetLineWidth(0);
	 	text.SetBorderSize(1);
	 	text.SetTextSize(2*0.05);

		text.AddText(yearStr);



        RooRealVar meanBd("meanBd","#mu", 5279.8);
        RooRealVar meanBs("meanBs","#mu", 5366.9);
		RooRealVar sigmaNonDD("sigmaNonDD", "sigmaNonDD", 22, 8, 30);


		RooGaussian modelNonDDBd("modelNonDDBd", "modelNonDDBd", m, meanBd, sigmaNonDD);
		RooGaussian modelNonDDBs("modelNonDDBs", "modelNonDDBs", m, meanBs, sigmaNonDD);

	 	RooRealVar *nNonDDBd = new RooRealVar("nNonDDBd","Number of expBkg events",2800, 0., 1e7);
	 	RooRealVar *nNonDDBs = new RooRealVar("nNonDDBs","Number of expBkg events",2800, 0., 1e7);
	 	RooRealVar *nExpBkg = new RooRealVar("nExpBkg","Number of expBkg events",2800, 0., 1e7);
	
	 	RooAddPdf* totPDF = new RooAddPdf("totPDF","Total pdf", RooArgList(modelNonDDBd, modelNonDDBs, expBkg), RooArgList(*nNonDDBd, *nNonDDBs, *nExpBkg));
	 	//RooAddPdf* totPDF = new RooAddPdf("totPDF","Total pdf", RooArgList(modelNonDDBs, expBkg), RooArgList(*nNonDDBs, *nExpBkg));


	 	cout<<"all ready"<<endl;
	 	//////////////////// Fit the data/////////////
	 	RooFitResult *result = totPDF->fitTo(*datars, Save(true), NumCPU(10));


		//ValError nNonDDVE = Roo2VE(*nBd)*Roo2VE(fracNonDD);
	 	//RooRealVar *nNonDDBs = new RooRealVar("nNonDDBs", "nNonDDBs", 30000, 0., 1e7);
		//nNonDDBs->setVal(nNonDDVE.val);
		//nNonDDBs->setError(nNonDDVE.err);

		out_data_yields<<setw(w)<<yearStr;
		//out_data_yields<<" & "<<setw(w)<<"$"<<dbl2str(nBd->getVal(),0)<<"\\pm"<<dbl2str(nBd->getError(),0)<<"$";
		out_data_yields<<" & "<<setw(w)<<"$"<<dbl2str(nNonDDBd->getVal(),0)<<"\\pm"<<dbl2str(nNonDDBd->getError(),0)<<"$";
		out_data_yields<<" & "<<setw(w)<<"$"<<dbl2str(nNonDDBs->getVal(),0)<<"\\pm"<<dbl2str(nNonDDBs->getError(),0)<<"$";
		//out_data_yields<<" & "<<setw(w)<<"$"<<dbl2str(nBs->getVal(),0)<<"\\pm"<<dbl2str(nBs->getError(),0)<<"$";
		out_data_yields<<" & "<<setw(w)<<"$"<<dbl2str(nExpBkg->getVal(),0)<<"\\pm"<<dbl2str(nExpBkg->getError(),0)<<"$";
		//out_data_yields<<" & "<<setw(w)<<"$"<<dbl2str(nArgusBkg->getVal(),0)<<"\\pm"<<dbl2str(nArgusBkg->getError(),0)<<"$";
		out_data_yields<<"\\\\"<<endl;
		out_data_meanSigma<<setw(w)<<yearStr;
		out_data_meanSigma<<" & "<<setw(w)<<"$"<<dbl2str(meanBd.getVal(),1)<<"\\pm"<<dbl2str(meanBd.getError(),1)<<"$";
		out_data_meanSigma<<" & "<<setw(w)<<"$"<<dbl2str(meanBs.getVal(),1)<<"\\pm"<<dbl2str(meanBs.getError(),1)<<"$";
		//out_data_meanSigma<<" & "<<setw(w)<<"$"<<dbl2str(fSigmaDataMC.getVal(),2)<<"\\pm"<<dbl2str(fSigmaDataMC.getError(),2)<<"$";
		out_data_meanSigma<<" & "<<setw(w)<<"$"<<dbl2str(sigmaNonDD.getVal(),2)<<"\\pm"<<dbl2str(sigmaNonDD.getError(),2)<<"$";
		out_data_meanSigma<<"\\\\"<<endl;
		out_data_bkgParams<<setw(w)<<yearStr;
		out_data_bkgParams<<" & "<<setw(w)<<"$"<<dbl2str(tau.getVal()*1e2,1)<<"\\pm"<<dbl2str(tau.getError()*1e2,1)<<"$";
		//out_data_bkgParams<<" & "<<setw(w)<<"$"<<dbl2str(m0.getVal(),1)<<"\\pm"<<dbl2str(m0.getError(),1)<<"$";
		//out_data_bkgParams<<" & "<<setw(w)<<"$"<<dbl2str(c.getVal(),1)<<"\\pm"<<dbl2str(c.getError(),1)<<"$";
		out_data_bkgParams<<"\\\\"<<endl;



	 	RooPlot *mframe = m.frame(Title("Bs2DsDs Run2 data"));
	 	
	 	cout<<"Fit the data"<<endl;
	 	
	 	datars->plotOn(mframe, Name("data"));
	 	//totPDF->plotOn(mframe, Name("signal Bd"), Components(modelBdData), LineColor(3), LineStyle(kDashed));
	 	totPDF->plotOn(mframe, Name("nonDD Gauss"), Components(modelNonDDBd), LineColor(9), LineStyle(kDashed));
	 	totPDF->plotOn(mframe, Name("nonDD Gauss"), Components(modelNonDDBs), LineColor(13), LineStyle(kDashed));
	 	//totPDF->plotOn(mframe, Name("Bs bkg"), Components(modelBsData), LineColor(6), LineStyle(kDashed));
	 	totPDF->plotOn(mframe, Name("exp bkg"), Components(expBkg), LineColor(4), LineStyle(kDashed));
	 	//totPDF->plotOn(mframe, Name("prc bkg"), Components(argusBkg), LineColor(kOrange), LineStyle(kDashed));
	 	totPDF->plotOn(mframe, Name("totPDF"), LineColor(kRed));
	 	datars->plotOn(mframe, Name("data"));
	 	totPDF->paramOn(mframe, Layout(0.65, 0.95, 0.95));

		int result_status = result->status();
		cout << "result_status = "<< result_status<<endl;
		result->Print("v");
		
		
		RooHist *pull = mframe->pullHist();
		pull->GetYaxis()->SetTitle("Pull");
		RooPlot *framePull = m.frame();
		framePull->addPlotable(pull, "P");
		
		mframe->SetTitle(fitVar);
		mframe->SetXTitle("#it{m}_{B} [MeV/c^{2}]");
		mframe->SetYTitle(Form("Candidates / (%.1lf MeV/c^{2})", binWidth));
		mframe->GetXaxis()->SetTitleSize(0.2);
		mframe->GetYaxis()->SetTitleSize(0.2);
		
		framePull->SetTitle("");
		framePull->GetYaxis()->SetTitle("Pull");
		framePull->SetMinimum(-6.);
		framePull->SetMaximum(6.);
		framePull->SetMarkerStyle(2);
		//cout<<"-----------------1"<<endl;
		
		TCanvas *canv = new TCanvas("canv", "canv", 800, 600);
		canv->SetFillColor(10);
		canv->SetBorderMode(0);
		canv->cd();
		canv->cd(1);
		
		TPad *p1 = new TPad("pad1", "pad1", 0.02, 0.2, 0.98, 0.98, 0);
		TPad *p2 = new TPad("pad2", "pad2", 0.02, 0.04, 0.98, 0.2, 0);
		p1->Draw();
		p2->Draw();
		
		p1->cd();
		mframe->GetYaxis()->SetTitleOffset(1.00);
		mframe->GetYaxis()->SetLabelSize(0.05);
		mframe->GetYaxis()->SetTitleSize(0.05);
		mframe->GetYaxis()->SetNoExponent();
		mframe->GetXaxis()->SetTitleOffset(1.00);
		mframe->GetXaxis()->SetTitleSize(0.05);
		mframe->GetXaxis()->SetLabelSize(0.05);
		mframe->Draw("E1");
		text.Draw("same");
		
		p2->cd();
		p2->SetTickx();
		framePull->GetYaxis()->SetTitleOffset(0.17);
		framePull->GetXaxis()->SetTitleSize(0.0);
		framePull->GetXaxis()->SetTickLength(0.09);
		framePull->GetYaxis()->SetTitleSize(0.2);
		framePull->GetXaxis()->SetLabelSize(0.0);
		framePull->GetYaxis()->SetLabelSize(0.2);
		framePull->GetYaxis()->SetNdivisions(5);
		framePull->Draw("E1");
		
		TLine* lineZero = new TLine(m.getMin(), 0, m.getMax(), 0);
		lineZero->SetLineStyle(kDashed);
		lineZero->SetLineColor(kBlack);
		lineZero->Draw();
		
		TLine* lineTwoSigUp = new TLine(m.getMin(), 3, m.getMax(), 3);
		lineTwoSigUp->SetLineColor(kRed);
		lineTwoSigUp->Draw();
		TLine* lineTwoSigDown = new TLine(m.getMin(), -3, m.getMax(), -3);
		lineTwoSigDown->SetLineColor(kRed);
		lineTwoSigDown->Draw();


	 	canv->SaveAs("/mnt/d/lhcb/B2XcXc/public_html/fit/sPlot/Bs2DsDs/DsSB/"+yearStr+"_data_Bs2DsDs_DsSB.pdf");
		cout << "nonDD*2: "<< nNonDDBs->getVal()*2 << endl;
	}

	out_MC.close();
	out_data_yields.close();
	out_data_meanSigma.close();
	out_data_bkgParams.close();


	cout << endl << "params print to "+fTableMC+" "<< endl;
	system("cat "+fTableMC);

	cout << endl << "params print to "+fTableData1+" "<< endl;
	system("rm " + fTableData);

	system("cat "+fTableData1+">>"+fTableData);
	system("cat "+fTableData2+">>"+fTableData);
	system("cat "+fTableData3+">>"+fTableData);

	system("rm /mnt/d/lhcb/B2XcXc/public_html/fitParamTables/sPlot/data_Bs2DsDs_DsSB_*.dat");
	system("cat "+fTableData);
}
void fit_Ds(TString cutString)
{
	 //---------Build background PDF------
	lhcbStyle(0.063);
	 
	cout<< "define the expBkg"<<endl;
	 
	 
	vector<int> years;
	years.push_back(-1);

	int w(7);
	system("mkdir -p /mnt/d/lhcb/B2XcXc/public_html/fitParamTables/sPlot/");

	TString fTableData = "/mnt/d/lhcb/B2XcXc/public_html/fitParamTables/sPlot/data_Bs2DsDs_DsSB.dat";

	TString fTableData1 = "/mnt/d/lhcb/B2XcXc/public_html/fitParamTables/sPlot/data_Bs2DsDs_DsSB_yields.dat";
	TString fTableData2 = "/mnt/d/lhcb/B2XcXc/public_html/fitParamTables/sPlot/data_Bs2DsDs_DsSB_meanSigma.dat";
	TString fTableData3 = "/mnt/d/lhcb/B2XcXc/public_html/fitParamTables/sPlot/data_Bs2DsDs_DsSB_bkgParams.dat";
	ofstream out_data_yields(fTableData1.Data());
	ofstream out_data_meanSigma(fTableData2.Data());
	ofstream out_data_bkgParams(fTableData3.Data());

	TString fTableMC = "/mnt/d/lhcb/B2XcXc/public_html/fitParamTables/sPlot/MC_Bs2DsDs_DsSB.dat";
	ofstream out_MC(fTableMC.Data());


	for(int i=0; i<years.size(); i++){
		TString yearStr = "Run2";
		if(years[i] != -1) yearStr = i2s(years[i]);

		TString yearCut = "(1)";
		if(years[i] != -1) yearCut = "year == " + i2s(years[i]);

	 
		TString fitVar = "Dsminus_M";
	 	RooRealVar m(fitVar, "#it{m}_{DsD}", 1918.35, 2018.35);
		double binWidth = 1.5;
		int nBins = (int)(m.getMax() - m.getMin())/binWidth;
		m.setBins(nBins);
	 	
		m.setRange("Ds_left_SB", 1918.35, 1943.35);
		m.setRange("Ds_peak", 1943.35, 1993.35);
		m.setRange("Ds_right_SB", 1993.35, 2018.35);

	 	RooRealVar tau("tau", "tau", -0.002, -1, 0.); 
	 	RooExponential expBkg("expBkg","background model",m,tau) ;
	 	
	 	TString datacut = fitVar+">"+d2s(m.getMin())+" && "+fitVar+"<"+d2s(m.getMax())+" && "+cutString;
		datacut += "&&" + yearCut;


	 	TChain chData;
		chData.Add("/mnt/d/lhcb/B2XcXc/root/Bs/data/Data_Bs2DsDs_Run2_TriggerSel_DsSB_PreSel.root/DecayTree");
	 	TTree* datatree = chData.CopyTree(datacut);
	 	datatree->SetBranchStatus("*", 0);
	 	datatree->SetBranchStatus(fitVar, 1);
	 	datatree->SetBranchStatus("Dsminus_M", 1);
		setBranchStatusTTF(datatree, datacut);
	 	RooDataSet *datars = new RooDataSet("dataset", "dataset", datatree, m);

		system("mkdir -p /mnt/d/lhcb/B2XcXc/public_html/fit/sPlot/Bs2DsDs/DsSB");
		system("mkdir -p /mnt/d/lhcb/B2XcXc/public_html/fit/sPlot/Bs2DsDs/DsSB/Bs");
    	TPaveText text(0.15, 0.85, 0.35, 0.95, "NDC");
	 	text.SetFillColor(0);
	 	text.SetFillStyle(0);
	 	text.SetLineColor(0);
	 	text.SetLineWidth(0);
	 	text.SetBorderSize(1);
	 	text.SetTextSize(2*0.05);

		text.AddText(yearStr);





        RooRealVar alphalDs("alphalDs","#alpha_{1}", 2.8, 0.01, 5.);
        RooRealVar alpharDs("alpharDs","#alpha_{2}", 4, 0.1, 5.);
        RooRealVar nlDs("nlDs","n_{1}", 1.5, 0., 10);
        RooRealVar nrDs("nrDs","n_{2}", 4.0, 0.01, 10);
        RooRealVar meanDs("meanDs","#mu", 1968, 1950, 1975);
		RooRealVar sigmaDs("sigmaDs", "sigmaDs", 10, 5, 15);
        RooRealVar beta_Ds("beta_Ds","#beta_Ds", 0);
        RooRealVar zeta_Ds("zeta_Ds","#zeta_Ds", 1e-5);
        RooRealVar l_Ds("l_Ds", "l_Ds", -3, -50, 0.);

		RooIpatia2 modelDs("modelDs", "modelDs", m, l_Ds, zeta_Ds, beta_Ds, sigmaDs, meanDs, alphalDs, nlDs, alpharDs, nrDs);


	 	RooRealVar *nDs = new RooRealVar("nDs","Number of expBkg events",2800, 0., 1e7);
	 	RooRealVar *nExpBkg = new RooRealVar("nExpBkg","Number of expBkg events",2800, 0., 1e7);
	
	 	//RooAddPdf* totPDF = new RooAddPdf("totPDF","Total pdf", RooArgList(modelBdData, modelDs, modelBsData, expBkg, argusBkg), RooArgList(*nBd, *nNonDDFormula, *nBs, *nExpBkg, *nArgusBkg));
	 	RooAddPdf* totPDF = new RooAddPdf("totPDF","Total pdf", RooArgList(modelDs, expBkg), RooArgList(*nDs, *nExpBkg));


	 	//////////////////// Fit the data/////////////
	 	RooFitResult *result = totPDF->fitTo(*datars, Save(true), NumCPU(10));

		ValError bkg_left(nDs->getVal()*(expBkg.createIntegral(m, m, "Ds_left_SB")->getVal()),  nDs->getError()*(expBkg.createIntegral(m, m, "Ds_left_SB")->getVal()));
		ValError bkg_right(nDs->getVal()*(expBkg.createIntegral(m, m, "Ds_right_SB")->getVal()),  nDs->getError()*(expBkg.createIntegral(m, m, "Ds_right_SB")->getVal()));
		ValError bkg_peak(nDs->getVal()*(expBkg.createIntegral(m, m, "Ds_peak")->getVal()),  nDs->getError()*(expBkg.createIntegral(m, m, "Ds_peak")->getVal()));

		cout << "Ds_left_SB: " << "Ds_right_SB: " << "Ds_peak: " << endl;
		cout << bkg_left.val <<", " << bkg_right.val<<", "  << bkg_peak.val << endl;

		//ValError nNonDDVE = Roo2VE(*nBd)*Roo2VE(fracNonDD);
	 	//RooRealVar *nDs = new RooRealVar("nDs", "nDs", 30000, 0., 1e7);
		//nDs->setVal(nNonDDVE.val);
		//nDs->setError(nNonDDVE.err);

		out_data_yields<<setw(w)<<yearStr;
		//out_data_yields<<" & "<<setw(w)<<"$"<<dbl2str(nBd->getVal(),0)<<"\\pm"<<dbl2str(nBd->getError(),0)<<"$";
		out_data_yields<<" & "<<setw(w)<<"$"<<dbl2str(nDs->getVal(),0)<<"\\pm"<<dbl2str(nDs->getError(),0)<<"$";
		//out_data_yields<<" & "<<setw(w)<<"$"<<dbl2str(nBs->getVal(),0)<<"\\pm"<<dbl2str(nBs->getError(),0)<<"$";
		out_data_yields<<" & "<<setw(w)<<"$"<<dbl2str(nExpBkg->getVal(),0)<<"\\pm"<<dbl2str(nExpBkg->getError(),0)<<"$";
		//out_data_yields<<" & "<<setw(w)<<"$"<<dbl2str(nArgusBkg->getVal(),0)<<"\\pm"<<dbl2str(nArgusBkg->getError(),0)<<"$";
		out_data_yields<<"\\\\"<<endl;
		out_data_meanSigma<<setw(w)<<yearStr;
		out_data_meanSigma<<" & "<<setw(w)<<"$"<<dbl2str(meanDs.getVal(),1)<<"\\pm"<<dbl2str(meanDs.getError(),1)<<"$";
		//out_data_meanSigma<<" & "<<setw(w)<<"$"<<dbl2str(fSigmaDataMC.getVal(),2)<<"\\pm"<<dbl2str(fSigmaDataMC.getError(),2)<<"$";
		out_data_meanSigma<<" & "<<setw(w)<<"$"<<dbl2str(sigmaDs.getVal(),2)<<"\\pm"<<dbl2str(sigmaDs.getError(),2)<<"$";
		out_data_meanSigma<<"\\\\"<<endl;
		out_data_bkgParams<<setw(w)<<yearStr;
		out_data_bkgParams<<" & "<<setw(w)<<"$"<<dbl2str(tau.getVal()*1e2,1)<<"\\pm"<<dbl2str(tau.getError()*1e2,1)<<"$";
		//out_data_bkgParams<<" & "<<setw(w)<<"$"<<dbl2str(m0.getVal(),1)<<"\\pm"<<dbl2str(m0.getError(),1)<<"$";
		//out_data_bkgParams<<" & "<<setw(w)<<"$"<<dbl2str(c.getVal(),1)<<"\\pm"<<dbl2str(c.getError(),1)<<"$";
		out_data_bkgParams<<"\\\\"<<endl;



	 	RooPlot *mframe = m.frame(Title("Bs2DsDs Run2 data"));
	 	
	 	cout<<"Fit the data"<<endl;
	 	
	 	datars->plotOn(mframe, Name("data"));
	 	//totPDF->plotOn(mframe, Name("signal Bd"), Components(modelBdData), LineColor(3), LineStyle(kDashed));
	 	totPDF->plotOn(mframe, Name("nonDD Gauss"), Components(modelDs), LineColor(9), LineStyle(kDashed));
	 	//totPDF->plotOn(mframe, Name("Bs bkg"), Components(modelBsData), LineColor(6), LineStyle(kDashed));
	 	totPDF->plotOn(mframe, Name("exp bkg"), Components(expBkg), LineColor(4), LineStyle(kDashed));
	 	//totPDF->plotOn(mframe, Name("prc bkg"), Components(argusBkg), LineColor(kOrange), LineStyle(kDashed));
	 	totPDF->plotOn(mframe, Name("totPDF"), LineColor(kRed));
	 	datars->plotOn(mframe, Name("data"));
	 	totPDF->paramOn(mframe, Layout(0.65, 0.95, 0.95));

		int result_status = result->status();
		cout << "result_status = "<< result_status<<endl;
		result->Print("v");
		
		
		RooHist *pull = mframe->pullHist();
		pull->GetYaxis()->SetTitle("Pull");
		RooPlot *framePull = m.frame();
		framePull->addPlotable(pull, "P");
		
		mframe->SetTitle(fitVar);
		mframe->SetXTitle("#it{m}_{B} [MeV/c^{2}]");
		mframe->SetYTitle(Form("Candidates / (%.1lf MeV/c^{2})", binWidth));
		mframe->GetXaxis()->SetTitleSize(0.2);
		mframe->GetYaxis()->SetTitleSize(0.2);
		
		framePull->SetTitle("");
		framePull->GetYaxis()->SetTitle("Pull");
		framePull->SetMinimum(-6.);
		framePull->SetMaximum(6.);
		framePull->SetMarkerStyle(2);
		//cout<<"-----------------1"<<endl;
		
		TCanvas *canv = new TCanvas("canv", "canv", 800, 600);
		canv->SetFillColor(10);
		canv->SetBorderMode(0);
		canv->cd();
		canv->cd(1);
		
		TPad *p1 = new TPad("pad1", "pad1", 0.02, 0.2, 0.98, 0.98, 0);
		TPad *p2 = new TPad("pad2", "pad2", 0.02, 0.04, 0.98, 0.2, 0);
		p1->Draw();
		p2->Draw();
		
		p1->cd();
		mframe->GetYaxis()->SetTitleOffset(1.00);
		mframe->GetYaxis()->SetLabelSize(0.05);
		mframe->GetYaxis()->SetTitleSize(0.05);
		mframe->GetYaxis()->SetNoExponent();
		mframe->GetXaxis()->SetTitleOffset(1.00);
		mframe->GetXaxis()->SetTitleSize(0.05);
		mframe->GetXaxis()->SetLabelSize(0.05);
		mframe->Draw("E1");
		text.Draw("same");
		
		p2->cd();
		p2->SetTickx();
		framePull->GetYaxis()->SetTitleOffset(0.17);
		framePull->GetXaxis()->SetTitleSize(0.0);
		framePull->GetXaxis()->SetTickLength(0.09);
		framePull->GetYaxis()->SetTitleSize(0.2);
		framePull->GetXaxis()->SetLabelSize(0.0);
		framePull->GetYaxis()->SetLabelSize(0.2);
		framePull->GetYaxis()->SetNdivisions(5);
		framePull->Draw("E1");
		
		TLine* lineZero = new TLine(m.getMin(), 0, m.getMax(), 0);
		lineZero->SetLineStyle(kDashed);
		lineZero->SetLineColor(kBlack);
		lineZero->Draw();
		
		TLine* lineTwoSigUp = new TLine(m.getMin(), 3, m.getMax(), 3);
		lineTwoSigUp->SetLineColor(kRed);
		lineTwoSigUp->Draw();
		TLine* lineTwoSigDown = new TLine(m.getMin(), -3, m.getMax(), -3);
		lineTwoSigDown->SetLineColor(kRed);
		lineTwoSigDown->Draw();


	 	canv->SaveAs("/mnt/d/lhcb/B2XcXc/public_html/fit/sPlot/Bs2DsDs/DsSB/"+yearStr+"_data_Ds.pdf");
	}

	out_MC.close();
	out_data_yields.close();
	out_data_meanSigma.close();
	out_data_bkgParams.close();


	cout << endl << "params print to "+fTableMC+" "<< endl;
	system("cat "+fTableMC);

	cout << endl << "params print to "+fTableData1+" "<< endl;
	system("rm " + fTableData);

	system("cat "+fTableData1+">>"+fTableData);
	system("cat "+fTableData2+">>"+fTableData);
	system("cat "+fTableData3+">>"+fTableData);

	system("rm /mnt/d/lhcb/B2XcXc/public_html/fitParamTables/sPlot/data_Bs2DsDs_DsSB_*.dat");
	system("cat "+fTableData);
}

void fit_data_Bs2DsDs_DsSB()
{
	fit("( (fabs(Dsminus_M-1968.35)<50 && fabs(Dsminus_M-1968.35)>37.5) ||  (fabs(Dsplus_M-1968.35)<50 && fabs(Dsplus_M-1968.35)>37.5) )&& (Dsplus_ENDVERTEX_Z-B_ENDVERTEX_Z>0) && (Dsminus_ENDVERTEX_Z-B_ENDVERTEX_Z>0) && (Dsplus_FDCHI2_OWNPV>2 && Dsminus_FDCHI2_OWNPV>2)");
	//fit_Ds("fabs(Dsminus_M-1968.35)<50  && fabs(Dsplus_M-1968.35)<50 && B_DTFM_M_0>5200 && B_DTFM_M_0<5550 && (Dsplus_ENDVERTEX_Z-B_ENDVERTEX_Z>0) && (Dsminus_ENDVERTEX_Z-B_ENDVERTEX_Z>0) && (Dsplus_FDCHI2_OWNPV>2 && Dsminus_FDCHI2_OWNPV>2)");
	gROOT->ProcessLine(".q");
}

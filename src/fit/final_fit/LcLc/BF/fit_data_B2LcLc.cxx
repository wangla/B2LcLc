#include <iostream>
#include "RooStats/ModelConfig.h"
#include "RooStats/ProfileLikelihoodCalculator.h"
#include "RooStats/HypoTestResult.h"
#include <RooDataSet.h>

#include "/mnt/d/lhcb/B2XcXc/inc/tools.h"
#include "/mnt/d/lhcb/B2XcXc/inc/RooDoubleCB.h"
#include "/mnt/d/lhcb/B2XcXc/inc/fitMCtoGetShape.h"
#include "/mnt/d/lhcb/B2XcXc/inc/getPresel.h"

using namespace RooFit;
using namespace RooStats;

void toyStudy(TString toyDir, TString yearStr);
vector<vector<ValError>> fitVarsPull(TString nameFile, TString toyDir);
void fit_BFs(TString cutString )
{
	 //---------Build background PDF------
	lhcbStyle(0.063);
	 
	cout<< "define the expBkg"<<endl;
	 
	 
	vector<int> years;
	//years.push_back(2015);
	//years.push_back(2016);
	years.push_back(1112);
	years.push_back(1516);
	years.push_back(2017);
	years.push_back(2018);

	int w(7);
	system("mkdir -p /mnt/d/lhcb/B2XcXc/public_html/fitParamTables/final_fit/simultaneous_fit");

	TString fTableData = "/mnt/d/lhcb/B2XcXc/public_html/fitParamTables/final_fit/simultaneous_fitdata.dat";

	TString fTableData1 = "/mnt/d/lhcb/B2XcXc/public_html/fitParamTables/final_fit/simultaneous_fitdata_yields.dat";
	TString fTableData2 = "/mnt/d/lhcb/B2XcXc/public_html/fitParamTables/final_fit/simultaneous_fitdata_meanSigma.dat";
	TString fTableData3 = "/mnt/d/lhcb/B2XcXc/public_html/fitParamTables/final_fit/simultaneous_fitdata_bkgParams.dat";
	ofstream out_data_yields(fTableData1.Data());
	ofstream out_data_meanSigma(fTableData2.Data());
	ofstream out_data_bkgParams(fTableData3.Data());

	TString fTableMCBd = "/mnt/d/lhcb/B2XcXc/public_html/fitParamTables/final_fit/simultaneous_fitMC_Bd.dat";
	TString fTableMCBs = "/mnt/d/lhcb/B2XcXc/public_html/fitParamTables/final_fit/simultaneous_fitMC_Bs.dat";
	ofstream out_MCBd(fTableMCBd.Data());
	ofstream out_MCBs(fTableMCBs.Data());

	system("mkdir -p /mnt/d/lhcb/B2XcXc/Documents/yields/final_fit/");
	TString sYields = "/mnt/d/lhcb/B2XcXc/Documents/yields/final_fit/simultaneous_fit_yields.root";



	system("mkdir -p /mnt/d/lhcb/B2XcXc/public_html/fit/final_fit/B2LcLc/simultaneous_fit");

	// difine vars to save parameters of signal and ctrl mode.
	TString fitVar = "B_DTFM_M_0";
	double binWidth = 8;


		//********************************* 同时拟合的参数 **************************************//
		// 这里的 [4] 是指 1112， 1516， 2017， 2018
	////************* construct model for mDsD data samples
	////
	RooRealVar* mDsD = NULL;
	
	//// try Ipatia
	RooRealVar* alphalBd2DsD = NULL;
	RooRealVar* alpharBd2DsD = NULL;
	RooRealVar* nlBd2DsD = NULL;
	RooRealVar* nrBd2DsD = NULL;
	RooRealVar* sigmaBd2DsD = NULL;
	RooRealVar* meanBd = NULL;
	//RooRealVar* fSigmaBd2DsD = NULL;
	//RooRealVar* fCB_Bd2DsD = NULL;
	RooRealVar* beta_Bd2DsD = NULL;
	RooRealVar* zeta_Bd2DsD = NULL;
	RooRealVar* l_Bd2DsD = NULL;

	RooRealVar* fSigmaDataMCBd = NULL;
	RooIpatia2* modelBd2DsD = NULL;

	RooGaussian* modelNonDDBd2DsD = NULL;
	
	
	RooRealVar* sigmaBs2DsD = NULL;
	RooRealVar* nlBs2DsD = NULL;
	RooRealVar* nrBs2DsD = NULL;
	RooRealVar* alphalBs2DsD = NULL;
	RooRealVar* alpharBs2DsD = NULL;
	//RooRealVar* fSigmaBs2DsD = NULL;
	//RooRealVar* fCB_Bs2DsD = NULL;
	RooRealVar* beta_Bs2DsD = NULL;
	RooRealVar* zeta_Bs2DsD = NULL;
	RooRealVar* l_Bs2DsD = NULL;
	
	RooIpatia2* modelBs2DsD = NULL;


	
	RooRealVar* tau_DsD[4] = {NULL};
	RooExponential* expBkg_DsD[4] = {NULL};
	
	RooRealVar* m0_DsD[4] = {NULL};
	RooRealVar* c_DsD[4] = {NULL};

    RooArgusBG* argus_noSmearing_DsD[4] = {NULL};

	//// Detector response function
	RooRealVar* m0_g_DsD[4] = {NULL};
	
	RooGaussian* gauss_DsD[4] = {NULL};
	RooFFTConvPdf* argusBkg_DsD[4] = {NULL};


	RooRealVar* nBd_DsD[4] = {NULL};
	RooRealVar* nNonDD_DsD[4] = {NULL};
	RooRealVar* nBs_DsD[4] = {NULL};
	RooRealVar* nExpBkg_DsD[4] = {NULL};
	RooRealVar* nArgusBkg_DsD[4] = {NULL};

	RooAddPdf* totPDF_DsD[4] = {NULL};




	////************* construct model for mDsDs data samples
	////
	RooRealVar* mDsDs = NULL;
	
	//// try Ipatia
	RooRealVar* alphalBs2DsDs = NULL;
	RooRealVar* alpharBs2DsDs = NULL;
	RooRealVar* nlBs2DsDs = NULL;
	RooRealVar* nrBs2DsDs = NULL;
	RooRealVar* sigmaBs2DsDs = NULL;
	RooRealVar* meanBs = NULL;
	//RooRealVar* fSigmaBs2DsDs = NULL;
	//RooRealVar* fCB_Bs2DsDs = NULL;
	RooRealVar* beta_Bs2DsDs = NULL;
	RooRealVar* zeta_Bs2DsDs = NULL;
	RooRealVar* l_Bs2DsDs = NULL;

	
	RooRealVar* fSigmaDataMCBs = NULL;
	RooIpatia2* modelBs2DsDs = NULL;

	RooGaussian* modelNonDDBs2DsDs = NULL;



	RooRealVar* tau_DsDs[4] = {NULL};
	RooExponential* expBkg_DsDs[4] = {NULL};
	
	RooRealVar* m0_DsDs[4] = {NULL};
	RooRealVar* c_DsDs[4] = {NULL};

	RooArgusBG* argus_noSmearing_DsDs[4] = {NULL};

	//// Detector response function
	RooRealVar* m0_g_DsDs[4] = {NULL};
	
	RooGaussian* gauss_DsDs[4] = {NULL};
	RooFFTConvPdf* argusBkg_DsDs[4] = {NULL};
	RooAbsPdf* Bd_misID_shape = NULL;

	
	RooRealVar* nBs_DsDs[4] = {NULL};
	RooRealVar* nNonDD_DsDs[4] = {NULL};
	RooRealVar* nBdBkg_DsDs[4] = {NULL};
	RooRealVar* nExpBkg_DsDs[4] = {NULL};
	RooRealVar* nArgusBkg_DsDs[4] = {NULL};

	RooAddPdf* totPDF_DsDs[4] = {NULL};



	////************* construct model for mLcLc data samples
	////
	RooRealVar* mLcLc = NULL;
	
	RooRealVar* alphalBd2LcLc = {NULL};
	RooRealVar* alpharBd2LcLc = {NULL};
	RooRealVar* nlBd2LcLc = {NULL};
	RooRealVar* nrBd2LcLc = {NULL};
	RooRealVar* sigmaBd2LcLc = {NULL};
	RooRealVar* beta_Bd2LcLc = {NULL};
	RooRealVar* zeta_Bd2LcLc = {NULL};
	RooRealVar* l_Bd2LcLc = {NULL};


	RooIpatia2* modelBd2LcLc = {NULL};


	
	RooRealVar* alphalBs2LcLc = {NULL};
	RooRealVar* alpharBs2LcLc = {NULL};
	RooRealVar* nlBs2LcLc = {NULL};
	RooRealVar* nrBs2LcLc = {NULL};
	RooRealVar* sigmaBs2LcLc = {NULL};
	RooRealVar* beta_Bs2LcLc = {NULL};
	RooRealVar* zeta_Bs2LcLc = {NULL};
	RooRealVar* l_Bs2LcLc = {NULL};


	RooIpatia2* modelBs2LcLc = {NULL};



	RooRealVar* tau_Bd2LcLc[4] = {NULL};
	RooExponential* expBkg_LcLc[4] = {NULL};
	
	RooRealVar* nBd_LcLc[4] = {NULL};
	RooRealVar* nBs_LcLc[4] = {NULL};
	RooRealVar* nExpBkg_LcLc[4] = {NULL};

	RooAddPdf* totPDF_LcLc[4] = {NULL};




	
	TChain chData_LcLc[4];
	TChain chData_DsD[4];
	TChain chData_DsDs[4];


	TString datacut_LcLc[4];
	TString datacut_DsD[4];
	TString datacut_DsDs[4];

	TTree* datatree_LcLc[4] = {NULL};
	TTree* datatree_DsD[4] = {NULL};
	TTree* datatree_DsDs[4] = {NULL};


	RooDataSet* datars_LcLc[4] = {NULL};
	RooDataSet* datars_DsD[4] = {NULL};
	RooDataSet* datars_DsDs[4] = {NULL};

	RooAbsReal* nll_LcLc[4] = {NULL};
	RooAbsReal* nll_DsD[4] = {NULL};
	RooAbsReal* nll_DsDs[4] = {NULL};

	RooArgSet* fitresult_LcLc[4] = {NULL};
	RooArgSet* fitresult_DsD[4] = {NULL};
	RooArgSet* fitresult_DsDs[4] = {NULL};

	

	//// 参数全部都取 Run2 的 common 参数
	//final_fit 是获取显著度的拟合，从 final_fit 获取形状参数
	TFile fWS_final_LcLc("/mnt/d/lhcb/B2XcXc/Documents/toy/final_fit/B2LcLc/fitVarsWorkspace.root");
	TFile fWS_final_DsD("/mnt/d/lhcb/B2XcXc/Documents/toy/final_fit/Bd2DsD/fitVarsWorkspace.root");
	TFile fWS_final_DsDs("/mnt/d/lhcb/B2XcXc/Documents/toy/final_fit/Bs2DsDs/fitVarsWorkspace.root");

	// ***************** 从filnal fit的ws中读取形状参数，并构造signal & ctrl
	RooWorkspace* ws_final_LcLcRun2 = (RooWorkspace*)fWS_final_LcLc.Get("workspaceRun2");
	RooWorkspace* ws_final_DsDRun2 = (RooWorkspace*)fWS_final_DsD.Get("workspaceRun2");
	RooWorkspace* ws_final_DsDsRun2 = (RooWorkspace*)fWS_final_DsDs.Get("workspaceRun2");

	if(!ws_final_LcLcRun2 || !ws_final_DsDRun2 || !ws_final_DsDsRun2){
		cerr << "!ws_final_LcLcRun2 || !ws_final_DsDRun2 || !ws_final_DsDsRun2: " << !ws_final_LcLcRun2 << !ws_final_DsDRun2 << !ws_final_DsDsRun2 <<endl;
		return;
	}


	//************************* Read MC shapes from final fit WorkSpace ******************************/////
	

	////************* construct model for mDsD data samples
	////
	mDsD = (RooRealVar*) ws_final_DsDRun2->var(fitVar);
	
	//// try Ipatia
	alphalBd2DsD = (RooRealVar*) ws_final_DsDRun2->var("alphalBd");
	alpharBd2DsD = (RooRealVar*) ws_final_DsDRun2->var("alpharBd");
	nlBd2DsD = (RooRealVar*) ws_final_DsDRun2->var("nlBd");
	nrBd2DsD = (RooRealVar*) ws_final_DsDRun2->var("nrBd");
	sigmaBd2DsD = (RooRealVar*) ws_final_DsDRun2->var("sigmaBd");
	meanBd = (RooRealVar*) ws_final_DsDRun2->var("meanBd");
	//fSigmaBd2DsD = (RooRealVar*) ws_final_DsDRun2->var("fSigmaBd");
	//fCB_Bd2DsD = (RooRealVar*) ws_final_DsDRun2->var("fCB_Bd");
	beta_Bd2DsD = (RooRealVar*) ws_final_DsDRun2->var("beta_Bd");
	zeta_Bd2DsD = (RooRealVar*) ws_final_DsDRun2->var("zeta_Bd");
	l_Bd2DsD = (RooRealVar*) ws_final_DsDRun2->var("l_Bd");

	//meanBd->setRange(5275, 5283);

	alphalBd2DsD->setConstant(true);
	alpharBd2DsD->setConstant(true);
	nlBd2DsD->setConstant(true);
	nrBd2DsD->setConstant(true);
	sigmaBd2DsD->setConstant(true);
	//meanBd->setConstant(true);
	beta_Bd2DsD->setConstant(true);
	zeta_Bd2DsD->setConstant(true);
	l_Bd2DsD->setConstant(true);

	alphalBd2DsD->SetName("alphalBd2DsD");
	alpharBd2DsD->SetName("alpharBd2DsD");
	nlBd2DsD->SetName("nlBd2DsD");
	nrBd2DsD->SetName("nrBd2DsD");
	sigmaBd2DsD->SetName("sigmaBd2DsD");
	meanBd->SetName("meanBd");
	beta_Bd2DsD->SetName("beta_Bd2DsD");
	zeta_Bd2DsD->SetName("zeta_Bd2DsD");
	l_Bd2DsD->SetName("l_Bd2DsD");


	fSigmaDataMCBd = (RooRealVar*) ws_final_DsDRun2->var("fSigmaDataMC");
	//fSigmaDataMCBd->setConstant(true);

	RooFormulaVar* sigmaBd2DsDData = new RooFormulaVar("sigmaBd2DsDData", "sigmaBd2DsDData", "@0*@1", RooArgList(*sigmaBd2DsD, *fSigmaDataMCBd));

	modelBd2DsD = new RooIpatia2("ipatiaBd2DsD", "ipatiaBd2DsD", *mDsD, *l_Bd2DsD, *zeta_Bd2DsD, *beta_Bd2DsD, *sigmaBd2DsDData, *meanBd, *alphalBd2DsD, *nlBd2DsD, *alpharBd2DsD, *nrBd2DsD);

	modelNonDDBd2DsD = (RooGaussian*) ws_final_DsDRun2->pdf("modelNonDD");

	fSigmaDataMCBd->SetName("fSigmaDataMCBd");
	modelNonDDBd2DsD->SetName("modelNonDDBd2DsD");



	////************* construct model for mDsDs data samples
	////
	mDsDs = (RooRealVar*) ws_final_DsDsRun2->var(fitVar);
	
	//// try Ipatia
	alphalBs2DsDs = (RooRealVar*) ws_final_DsDsRun2->var("alphalBs");
	alpharBs2DsDs = (RooRealVar*) ws_final_DsDsRun2->var("alpharBs");
	nlBs2DsDs = (RooRealVar*) ws_final_DsDsRun2->var("nlBs");
	nrBs2DsDs = (RooRealVar*) ws_final_DsDsRun2->var("nrBs");
	sigmaBs2DsDs = (RooRealVar*) ws_final_DsDsRun2->var("sigmaBs");
	meanBs = (RooRealVar*) ws_final_DsDsRun2->var("meanBs");
	beta_Bs2DsDs = (RooRealVar*) ws_final_DsDsRun2->var("beta_Bs");
	zeta_Bs2DsDs = (RooRealVar*) ws_final_DsDsRun2->var("zeta_Bs");
	l_Bs2DsDs = (RooRealVar*) ws_final_DsDsRun2->var("l_Bs");

	alphalBs2DsDs->setConstant(true);
	alpharBs2DsDs->setConstant(true);
	nlBs2DsDs->setConstant(true);
	nrBs2DsDs->setConstant(true);
	sigmaBs2DsDs->setConstant(true);
	//meanBs->setConstant(true);
	beta_Bs2DsDs->setConstant(true);
	zeta_Bs2DsDs->setConstant(true);
	l_Bs2DsDs->setConstant(true);
	
	alphalBs2DsDs->SetName("alphalBs2DsDs");
	alpharBs2DsDs->SetName("alpharBs2DsDs");
	nlBs2DsDs->SetName("nlBs2DsDs");
	nrBs2DsDs->SetName("nrBs2DsDs");
	sigmaBs2DsDs->SetName("sigmaBs2DsDs");
	meanBs->SetName("meanBs");
	beta_Bs2DsDs->SetName("beta_Bs2DsDs");
	zeta_Bs2DsDs->SetName("zeta_Bs2DsDs");
	l_Bs2DsDs->SetName("l_Bs2DsDs");
	
	fSigmaDataMCBs = (RooRealVar*) ws_final_DsDsRun2->var("fSigmaDataMC");
	//fSigmaDataMCBs->setConstant(true);

	RooFormulaVar* sigmaBs2DsDsData = new RooFormulaVar("sigmaBs2DsDsData", "sigmaBs2DsDsData", "@0*@1", RooArgList(*sigmaBs2DsDs, *fSigmaDataMCBs));

	modelBs2DsDs = new RooIpatia2("ipatiaBs2DsDs", "ipatiaBs2DsDs", *mDsDs, *l_Bs2DsDs, *zeta_Bs2DsDs, *beta_Bs2DsDs, *sigmaBs2DsDsData, *meanBs, *alphalBs2DsDs, *nlBs2DsDs, *alpharBs2DsDs, *nrBs2DsDs);
	
	modelNonDDBs2DsDs = (RooGaussian*) ws_final_DsDsRun2->pdf("modelNonDD");

	fSigmaDataMCBs->SetName("fSigmaDataMCBs");
	modelNonDDBs2DsDs->SetName("modelNonDDBs2DsDs");

	Bd_misID_shape = (RooAbsPdf*) ws_final_DsDsRun2->pdf("Bd_misID_shape");




	//*************   Bs->DsD, 使用Bs的mean值 ***********************///
	sigmaBs2DsD = (RooRealVar*) ws_final_DsDRun2->var("sigmaBs");
	nlBs2DsD = (RooRealVar*) ws_final_DsDRun2->var("nlBs");
	nrBs2DsD = (RooRealVar*) ws_final_DsDRun2->var("nrBs");
	alphalBs2DsD = (RooRealVar*) ws_final_DsDRun2->var("alphalBs");
	alpharBs2DsD = (RooRealVar*) ws_final_DsDRun2->var("alpharBs");
	beta_Bs2DsD = (RooRealVar*) ws_final_DsDRun2->var("beta_Bs");
	zeta_Bs2DsD = (RooRealVar*) ws_final_DsDRun2->var("zeta_Bs");
	l_Bs2DsD = (RooRealVar*) ws_final_DsDRun2->var("l_Bs");
	sigmaBs2DsD->setConstant(true);
	nlBs2DsD->setConstant(true);
	nrBs2DsD->setConstant(true);
	alphalBs2DsD->setConstant(true);
	alpharBs2DsD->setConstant(true);
	beta_Bs2DsD->setConstant(true);
	zeta_Bs2DsD->setConstant(true);
	l_Bs2DsD->setConstant(true);
	
	sigmaBs2DsD->SetName("sigmaBs2DsD");
	nlBs2DsD->SetName("nlBs2DsD");
	nrBs2DsD->SetName("nrBs2DsD");
	alphalBs2DsD->SetName("alphalBs2DsD");
	alpharBs2DsD->SetName("alpharBs2DsD");
	beta_Bs2DsD->SetName("beta_Bs2DsD");
	zeta_Bs2DsD->SetName("zeta_Bs2DsD");
	l_Bs2DsD->SetName("l_Bs2DsD");
	
	RooFormulaVar* sigmaBs2DsDData = new RooFormulaVar("sigmaBs2DsDData", "sigmaBs2DsDData", "@0*@1", RooArgList(*sigmaBs2DsD, *fSigmaDataMCBs));

	modelBs2DsD = new RooIpatia2("ipatiaBs2DsD", "ipatiaBs2DsD", *mDsD, *l_Bs2DsD, *zeta_Bs2DsD, *beta_Bs2DsD, *sigmaBs2DsDData, *meanBs, *alphalBs2DsD, *nlBs2DsD, *alpharBs2DsD, *nrBs2DsD);



	////************* construct model for mLcLc data samples
	////
	mLcLc = (RooRealVar*) ws_final_LcLcRun2->var(fitVar);
	
	alphalBd2LcLc = (RooRealVar*) ws_final_LcLcRun2->var("alphalBd");
	alpharBd2LcLc = (RooRealVar*) ws_final_LcLcRun2->var("alpharBd");
	nlBd2LcLc = (RooRealVar*) ws_final_LcLcRun2->var("nlBd");
	nrBd2LcLc = (RooRealVar*) ws_final_LcLcRun2->var("nrBd");
	sigmaBd2LcLc = (RooRealVar*) ws_final_LcLcRun2->var("sigmaBd");
	beta_Bd2LcLc = (RooRealVar*) ws_final_LcLcRun2->var("beta_Bd");
	zeta_Bd2LcLc = (RooRealVar*) ws_final_LcLcRun2->var("zeta_Bd");
	l_Bd2LcLc = (RooRealVar*) ws_final_LcLcRun2->var("l_Bd");

	alphalBd2LcLc->setConstant(true);
	alpharBd2LcLc->setConstant(true);
	nlBd2LcLc->setConstant(true);
	nrBd2LcLc->setConstant(true);
	sigmaBd2LcLc->setConstant(true);
	beta_Bd2LcLc->setConstant(true);
	zeta_Bd2LcLc->setConstant(true);
	l_Bd2LcLc->setConstant(true);

	alphalBd2LcLc->SetName("alphalBd2LcLc");
	alpharBd2LcLc->SetName("alpharBd2LcLc");
	nlBd2LcLc->SetName("nlBd2LcLc");
	nrBd2LcLc->SetName("nrBd2LcLc");
	sigmaBd2LcLc->SetName("sigmaBd2LcLc");
	beta_Bd2LcLc->SetName("beta_Bd2LcLc");
	zeta_Bd2LcLc->SetName("zeta_Bd2LcLc");
	l_Bd2LcLc->SetName("l_Bd2LcLc");

	RooFormulaVar* sigmaBd2LcLcData = new RooFormulaVar("sigmaBd2LcLcData", "sigmaBd2LcLcData", "@0*@1", RooArgList(*sigmaBd2LcLc, *fSigmaDataMCBd));

	modelBd2LcLc = new RooIpatia2("modelBd2LcLc", "modelBd2LcLc", *mLcLc, *l_Bd2LcLc, *zeta_Bd2LcLc, *beta_Bd2LcLc, *sigmaBd2LcLcData, *meanBd, *alphalBd2LcLc, *nlBd2LcLc, *alpharBd2LcLc, *nrBd2LcLc);


	
	alphalBs2LcLc = (RooRealVar*) ws_final_LcLcRun2->var("alphalBs");
	alpharBs2LcLc = (RooRealVar*) ws_final_LcLcRun2->var("alpharBs");
	nlBs2LcLc = (RooRealVar*) ws_final_LcLcRun2->var("nlBs");
	nrBs2LcLc = (RooRealVar*) ws_final_LcLcRun2->var("nrBs");
	sigmaBs2LcLc = (RooRealVar*) ws_final_LcLcRun2->var("sigmaBs");
	beta_Bs2LcLc = (RooRealVar*) ws_final_LcLcRun2->var("beta_Bs");
	zeta_Bs2LcLc = (RooRealVar*) ws_final_LcLcRun2->var("zeta_Bs");
	l_Bs2LcLc = (RooRealVar*) ws_final_LcLcRun2->var("l_Bs");

	alphalBs2LcLc->setConstant(true);
	alpharBs2LcLc->setConstant(true);
	nlBs2LcLc->setConstant(true);
	nrBs2LcLc->setConstant(true);
	sigmaBs2LcLc->setConstant(true);
	beta_Bs2LcLc->setConstant(true);
	zeta_Bs2LcLc->setConstant(true);
	l_Bs2LcLc->setConstant(true);

	alphalBs2LcLc->SetName("alphalBs2LcLc");
	alpharBs2LcLc->SetName("alpharBs2LcLc");
	nlBs2LcLc->SetName("nlBs2LcLc");
	nrBs2LcLc->SetName("nrBs2LcLc");
	sigmaBs2LcLc->SetName("sigmaBs2LcLc");
	beta_Bs2LcLc->SetName("beta_Bs2LcLc");
	zeta_Bs2LcLc->SetName("zeta_Bs2LcLc");
	l_Bs2LcLc->SetName("l_Bs2LcLc");

	RooFormulaVar* sigmaBs2LcLcData = new RooFormulaVar("sigmaBs2LcLcData", "sigmaBs2LcLcData", "@0*@1", RooArgList(*sigmaBs2LcLc, *fSigmaDataMCBs));

	modelBs2LcLc = new RooIpatia2("modelBs2LcLc", "modelBs2LcLc", *mLcLc, *l_Bs2LcLc, *zeta_Bs2LcLc, *beta_Bs2LcLc, *sigmaBs2LcLcData, *meanBs, *alphalBs2LcLc, *nlBs2LcLc, *alpharBs2LcLc, *nrBs2LcLc);


	TFile fDump("Dump.root", "RECREATE");

	for(int i=0; i<4; i++){
		//现在Run1的数据还没下来，先跳过
		if(years[i] == 1112) continue;

		TString yearStr = "Run2";
		if(years[i] ==2) yearStr = "Run"+i2s(years[i]);
		else yearStr = i2s(years[i]);

		TString yearCut = "(1)";
		if(years[i] == 1112) yearCut = "(year == 2011 || year==2012)";
		else if(years[i] == 2) yearCut = "(year == 2015 || year==2016 || year == 2017 || year==2018)";
		else if(years[i] == 1516) yearCut = "(year == 2015 || year==2016)";
		else yearCut = "(year == " +i2s(years[i])+")";

		cout << "Construct pdfs for "<<yearStr << " year." << endl;


		RooWorkspace* ws_final_LcLcYear = (RooWorkspace*)fWS_final_LcLc.Get("workspace"+yearStr);
		RooWorkspace* ws_final_DsDYear = (RooWorkspace*)fWS_final_DsD.Get("workspace"+yearStr);
		RooWorkspace* ws_final_DsDsYear = (RooWorkspace*)fWS_final_DsDs.Get("workspace"+yearStr);

		if(!ws_final_LcLcYear || !ws_final_DsDYear || !ws_final_DsDsYear){
			cerr << "year: "<< yearStr << ", !ws_final_LcLcYear || !ws_final_DsDYear || !ws_final_DsDsYear: " << !ws_final_LcLcYear <<", "<< !ws_final_DsDYear <<", "<< !ws_final_DsDsYear <<endl;
			return;
		}
	

    	TPaveText text(0.15, 0.85, 0.35, 0.95, "NDC");
	 	text.SetFillColor(0);
	 	text.SetFillStyle(0);
	 	text.SetLineColor(0);
	 	text.SetLineWidth(0);
	 	text.SetBorderSize(1);
	 	text.SetTextSize(2*0.05);

		text.AddText(yearStr);


		system("mkdir -p /mnt/d/lhcb/B2XcXc/public_html/fit/final_fit/B2LcLc/simultaneous_fit/");



		
	
		tau_DsD[i] = (RooRealVar*) ws_final_DsDYear->var("tau");
		expBkg_DsD[i] = new RooExponential("expBkg_DsD"+yearStr, "background model"+yearStr, *mDsD, *tau_DsD[i]) ;
		
		m0_DsD[i] = (RooRealVar*) ws_final_DsDYear->var("m0");
		c_DsD[i] = (RooRealVar*) ws_final_DsDYear->var("c");
		
		argus_noSmearing_DsD[i] = new RooArgusBG("argus_noSmearing_DsD"+yearStr, "rec background model"+yearStr, *mDsD, *m0_DsD[i], *c_DsD[i]);
		
		//// Detector response function
		m0_g_DsD[i] = (RooRealVar*) ws_final_DsDYear->var("m0_g");
		m0_g_DsD[i]->setConstant(true);
		
		gauss_DsD[i] = new RooGaussian("gauss_DsD"+yearStr, "gauss_DsD"+yearStr, *mDsD, *m0_g_DsD[i], *sigmaBd2DsDData);
		argusBkg_DsD[i] = new RooFFTConvPdf("smearedArgus_DsD"+yearStr, "argusBkg_DsD (X) gauss_DsD"+yearStr, *mDsD, *argus_noSmearing_DsD[i], *gauss_DsD[i]);
		argusBkg_DsD[i]->setBufferFraction(0.35);
		
		
		nBd_DsD[i] = (RooRealVar*) ws_final_DsDYear->var("nBd");
		nNonDD_DsD[i] = (RooRealVar*) ws_final_DsDYear->var("nNonDD");
		nBs_DsD[i] = (RooRealVar*) ws_final_DsDYear->var("nBs");
		nExpBkg_DsD[i] = (RooRealVar*) ws_final_DsDYear->var("nExpBkg");
		nArgusBkg_DsD[i] = (RooRealVar*) ws_final_DsDYear->var("nArgusBkg");

		nNonDD_DsD[i]->setConstant(true);


		////同时拟合的2015 2016 拟合不上, 设置约束
		//nBd_DsD[i]->setRange(0, 8e4);
		//nBs_DsD[i]->setRange(0, 1e3);
		//nExpBkg_DsD[i]->setRange(0, 5e3);
		//nArgusBkg_DsD[i]->setRange(0, 8e3);
		
		totPDF_DsD[i] = new RooAddPdf("totPDF_DsD"+yearStr, "Total pdf"+yearStr, RooArgList(*modelBd2DsD, *modelNonDDBd2DsD, *modelBs2DsD, *expBkg_DsD[i], *argusBkg_DsD[i]), RooArgList(*nBd_DsD[i], *nNonDD_DsD[i], *nBs_DsD[i], *nExpBkg_DsD[i], *nArgusBkg_DsD[i]));
		
		tau_DsD[i]->SetName("tau_DsD"+yearStr);
		m0_DsD[i]->SetName("m0_DsD"+yearStr);
		c_DsD[i]->SetName("c_DsD"+yearStr);
		m0_g_DsD[i]->SetName("m0_g_DsD"+yearStr);
		nBd_DsD[i]->SetName("nBd_DsD"+yearStr);
		nNonDD_DsD[i]->SetName("nNonDD_DsD"+yearStr);
		nBs_DsD[i]->SetName("nBs_DsD"+yearStr);
		nExpBkg_DsD[i]->SetName("nExpBkg_DsD"+yearStr);
		nArgusBkg_DsD[i]->SetName("nArgusBkg_DsD"+yearStr);
		
		
		
		
		tau_DsDs[i] = (RooRealVar*) ws_final_DsDsYear->var("tau");
		expBkg_DsDs[i] = new RooExponential("expBkg_DsDs"+yearStr, "background model"+yearStr, *mDsDs, *tau_DsDs[i]) ;
		
		m0_DsDs[i] = (RooRealVar*) ws_final_DsDsYear->var("m0");
		c_DsDs[i] = (RooRealVar*) ws_final_DsDsYear->var("c");
		
		argus_noSmearing_DsDs[i] = new RooArgusBG("argus_noSmearing_DsDs"+yearStr, "rec background model"+yearStr, *mDsDs, *m0_DsDs[i], *c_DsDs[i]);
		
		//// Detector response function
		m0_g_DsDs[i] = (RooRealVar*) ws_final_DsDsYear->var("m0_g");
		m0_g_DsDs[i]->setConstant(true);
		
		gauss_DsDs[i] = new RooGaussian("gauss_DsDs"+yearStr, "gauss_DsDs"+yearStr, *mDsDs, *m0_g_DsDs[i], *sigmaBs2DsDsData);
		argusBkg_DsDs[i] = new RooFFTConvPdf("smearedArgus_DsDs"+yearStr, "argusBkg_DsDs (X) gauss_DsDs"+yearStr, *mDsDs, *argus_noSmearing_DsDs[i], *gauss_DsDs[i]);
		argusBkg_DsDs[i]->setBufferFraction(0.35);
		
		
		
		nBs_DsDs[i] = (RooRealVar*) ws_final_DsDsYear->var("nBs");
		nNonDD_DsDs[i] = (RooRealVar*) ws_final_DsDsYear->var("nNonDD");
		nBdBkg_DsDs[i] = (RooRealVar*) ws_final_DsDsYear->var("nBdBkg");
		nExpBkg_DsDs[i] = (RooRealVar*) ws_final_DsDsYear->var("nExpBkg");
		nArgusBkg_DsDs[i] = (RooRealVar*) ws_final_DsDsYear->var("nArgusBkg");
	
		nNonDD_DsDs[i]->setConstant(true);
		//// 设置上限约束
		//nBs_DsDs[i]->setRange(0, 8e3);
		//nBdBkg_DsDs[i]->setRange(0, 4e2);
		//nExpBkg_DsDs[i]->setRange(0, 1e3);
		//nArgusBkg_DsDs[i]->setRange(0, 6e3);
	
		totPDF_DsDs[i] = new RooAddPdf("totPDF_DsDs"+yearStr, "Total pdf"+yearStr, RooArgList(*modelBs2DsDs, *modelNonDDBs2DsDs, *Bd_misID_shape, *expBkg_DsDs[i], *argusBkg_DsDs[i]), RooArgList(*nBs_DsDs[i], *nNonDD_DsDs[i], *nBdBkg_DsDs[i],  *nExpBkg_DsDs[i], *nArgusBkg_DsDs[i]));



		tau_DsDs[i]->SetName("tau_DsDs"+yearStr);
		m0_DsDs[i]->SetName("m0_DsDs"+yearStr);
		c_DsDs[i]->SetName("c_DsDs"+yearStr);
		m0_g_DsDs[i]->SetName("m0_g_DsDs"+yearStr);
		nBs_DsDs[i]->SetName("nBs_DsDs"+yearStr);
		nNonDD_DsDs[i]->SetName("nNonDD_DsDs"+yearStr);
		nBdBkg_DsDs[i]->SetName("nBdBkg_DsDs"+yearStr);
		nExpBkg_DsDs[i]->SetName("nExpBkg_DsDs"+yearStr);
		nArgusBkg_DsDs[i]->SetName("nArgusBkg_DsDs"+yearStr);
		



		tau_Bd2LcLc[i] = (RooRealVar*) ws_final_LcLcYear->var("tau");
		expBkg_LcLc[i] = new RooExponential("expBkg_LcLc"+yearStr, "background model"+yearStr, *mLcLc, *tau_Bd2LcLc[i]);
		
		nBd_LcLc[i] = (RooRealVar*) ws_final_LcLcYear->var("nBd");
		nBs_LcLc[i] = (RooRealVar*) ws_final_LcLcYear->var("nBs");
		nExpBkg_LcLc[i] = (RooRealVar*) ws_final_LcLcYear->var("nExpBkg");
		
		totPDF_LcLc[i] = new RooAddPdf("totPDF_LcLc"+yearStr, "Total pdf"+yearStr, RooArgList(*modelBd2LcLc, *modelBs2LcLc, *expBkg_LcLc[i]), RooArgList(*nBd_LcLc[i], *nBs_LcLc[i], *nExpBkg_LcLc[i]));

		//tau_Bd2LcLc[i]->setRange(-0.003, 0);
		//nBd_LcLc[i]->setRange(0, 100);
		//nBs_LcLc[i]->setRange(0, 100);
		//nExpBkg_LcLc[i]->setRange(0, 200);

		tau_Bd2LcLc[i]->SetName("tau_Bd2LcLc"+yearStr);
		nBd_LcLc[i]->SetName("nBd_LcLc"+yearStr);
		nBs_LcLc[i]->SetName("nBs_LcLc"+yearStr);
		nExpBkg_LcLc[i]->SetName("nExpBkg_LcLc"+yearStr);
		



		cout << "Construct RooDataSet for LcLc of "<<yearStr << " year: " << endl;
		// 读取每年的dataset
		//************** LcLc
		// 未解盲，先写SB
		chData_LcLc[i].Add("/mnt/d/lhcb/B2XcXc/root/Bs/data/Data_Bs2LcLc_SB_Run2_TriggerSel_PreSel.root/DecayTree");
		
		datacut_LcLc[i] = yearCut +" && "+ fitVar+">"+d2s(mLcLc->getMin())+" && "+fitVar+"<"+d2s(mLcLc->getMax())+" && "+cutString;
		
		datatree_LcLc[i] = chData_LcLc[i].CopyTree(datacut_LcLc[i]);
		
		datatree_LcLc[i]->SetBranchStatus("*", 0);
		datatree_LcLc[i]->SetBranchStatus(fitVar, 1);
		setBranchStatusTTF(datatree_LcLc[i], datacut_LcLc[i]);
		
		datars_LcLc[i] = new RooDataSet("dataset_LcLc"+yearStr, "dataset_LcLc"+yearStr, datatree_LcLc[i], *mLcLc);



		cout << "Construct RooDataSet for DsD of "<<yearStr << " year: " << endl;
		//************** DsD
		chData_DsD[i].Add("/mnt/d/lhcb/B2XcXc/root/Bd/data/Data_Bd2DsD_Run2_TriggerSel_PreSel.root/DecayTree");

		datacut_DsD[i] = yearCut +" && "+ fitVar+">"+d2s(mDsD->getMin())+" && "+fitVar+"<"+d2s(mDsD->getMax())+" && "+cutString;

		datatree_DsD[i] = chData_DsD[i].CopyTree(datacut_DsD[i]);

		datatree_DsD[i]->SetBranchStatus("*", 0);
		datatree_DsD[i]->SetBranchStatus(fitVar, 1);
		setBranchStatusTTF(datatree_DsD[i], datacut_DsD[i]);

		datars_DsD[i] = new RooDataSet("dataset_DsD"+yearStr, "dataset_DsD"+yearStr, datatree_DsD[i], *mDsD);



		cout << "Construct RooDataSet for DsDs of "<<yearStr << " year: " << endl;
		//************** DsDs
		chData_DsDs[i].Add("/mnt/d/lhcb/B2XcXc/root/Bs/data/Data_Bs2DsDs_Run2_TriggerSel_PreSel.root/DecayTree");
		
		datacut_DsDs[i] = yearCut +" && "+ fitVar+">"+d2s(mDsDs->getMin())+" && "+fitVar+"<"+d2s(mDsDs->getMax())+" && "+cutString;
		
		datatree_DsDs[i] = chData_DsDs[i].CopyTree(datacut_DsDs[i]);
		
		datatree_DsDs[i]->SetBranchStatus("*", 0);
		datatree_DsDs[i]->SetBranchStatus(fitVar, 1);
		setBranchStatusTTF(datatree_DsDs[i], datacut_DsDs[i]);
		
		datars_DsDs[i] = new RooDataSet("dataset_DsDs"+yearStr, "dataset_DsDs"+yearStr, datatree_DsDs[i], *mDsDs);




		delete datatree_LcLc[i];
		datatree_LcLc[i] = NULL;
		delete datatree_DsD[i];
		datatree_DsD[i] = NULL;
		delete datatree_DsDs[i];
		datatree_DsDs[i] = NULL;


		cout << "Construct Nlls of "<<yearStr << " year: " << endl;
		//nll_LcLc[i] = totPDF_LcLc[i]->createNLL(*datars_LcLc[i], Extended(kTRUE), NumCPU(10));
		//nll_DsDs[i] = totPDF_DsDs[i]->createNLL(*datars_DsDs[i], Extended(kTRUE), NumCPU(10));
		//nll_DsD[i] = totPDF_DsD[i]->createNLL(*datars_DsD[i], Extended(kTRUE), NumCPU(10));

		nll_LcLc[i] = totPDF_LcLc[i]->createNLL(*datars_LcLc[i], NumCPU(10));
		nll_DsDs[i] = totPDF_DsDs[i]->createNLL(*datars_DsDs[i], NumCPU(10));
		nll_DsD[i] = totPDF_DsD[i]->createNLL(*datars_DsD[i], NumCPU(10));

		cout << "Get parameters of "<<yearStr << " year: " << endl;
		fitresult_LcLc[i] = totPDF_LcLc[i]->getParameters(*datars_LcLc[i]);
		fitresult_DsDs[i] = totPDF_DsDs[i]->getParameters(*datars_DsDs[i]);
		fitresult_DsD[i] = totPDF_DsD[i]->getParameters(*datars_DsD[i]);
	}
	fWS_final_LcLc.Close();
	fWS_final_DsD.Close();
	fWS_final_DsDs.Close();



	//RooArgSet nll_list1(*nll_DsD[0], *nll_DsD[1], *nll_DsD[2], *nll_DsD[3],       *nll_DsDs[0], *nll_DsDs[1]);
	//RooArgSet nll_list2(*nll_DsDs[2], *nll_DsDs[3],      *nll_LcLc[0], *nll_LcLc[1], *nll_LcLc[2], *nll_LcLc[3]);
	RooArgSet nll_list1(*nll_DsD[1], *nll_DsD[2], *nll_DsD[3],       *nll_DsDs[1]);
	RooArgSet nll_list2(*nll_DsDs[2], *nll_DsDs[3],      *nll_LcLc[1], *nll_LcLc[2], *nll_LcLc[3]);

	RooArgSet nll_list(nll_list1, nll_list2);
	//RooArgSet nll_list(nll_list2);

	//RooAddition *nll = new RooAddition("nll","", nll_list1, nll_list2);
	RooAddition *nll = new RooAddition("nll", "", nll_list);
	RooMinuit minuit(*nll);
	minuit.setStrategy(1);
	minuit.setVerbose(kFALSE);
	minuit.setPrintLevel(2);
	minuit.migrad();
	minuit.migrad();
	minuit.hesse();
	minuit.hesse();





	  // save vars into workspace for toy study
    system("mkdir -p /mnt/d/lhcb/B2XcXc/Documents/toy/final_fit/simultaneous_fit");
    TFile fWS("/mnt/d/lhcb/B2XcXc/Documents/toy/final_fit/simultaneous_fit/fitVarsWorkspace.root", "UPDATE");
    RooWorkspace ws("workspace", "workspace");


	ws.import(*mDsD);
	
	ws.import(*alphalBd2DsD);
	ws.import(*alpharBd2DsD);
	ws.import(*nlBd2DsD);
	ws.import(*nrBd2DsD);
	ws.import(*sigmaBd2DsD);
	ws.import(*meanBd);
	//ws.import(*fSigmaBd2DsD);
	//ws.import(*fCB_Bd2DsD);
	ws.import(*beta_Bd2DsD);
	ws.import(*zeta_Bd2DsD);
	ws.import(*l_Bd2DsD);

	ws.import(*fSigmaDataMCBd);

	ws.import(*modelNonDDBd2DsD);
	
	
	ws.import(*sigmaBs2DsD);
	ws.import(*nlBs2DsD);
	ws.import(*nrBs2DsD);
	ws.import(*alphalBs2DsD);
	ws.import(*alpharBs2DsD);
	//ws.import(*fSigmaBs2DsD);
	//ws.import(*fCB_Bs2DsD);
	ws.import(*beta_Bs2DsD);
	ws.import(*zeta_Bs2DsD);
	ws.import(*l_Bs2DsD);
	


	


	////************* construct model for mDsDs data samples
	////
	ws.import(*mDsDs);
	
	//// try Ipatia
	ws.import(*alphalBs2DsDs);
	ws.import(*alpharBs2DsDs);
	ws.import(*nlBs2DsDs);
	ws.import(*nrBs2DsDs);
	ws.import(*sigmaBs2DsDs);
	ws.import(*meanBs);
	ws.import(*beta_Bs2DsDs);
	ws.import(*zeta_Bs2DsDs);
	ws.import(*l_Bs2DsDs);

	
	ws.import(*fSigmaDataMCBs);

	ws.import(*modelNonDDBs2DsDs);
	ws.import(*Bd_misID_shape);





	////************* construct model for mLcLc data samples
	////
	ws.import(*mLcLc);
	
	ws.import(*alphalBd2LcLc);
	ws.import(*alpharBd2LcLc);
	ws.import(*nlBd2LcLc);
	ws.import(*nrBd2LcLc);
	ws.import(*sigmaBd2LcLc);
	ws.import(*beta_Bd2LcLc);
	ws.import(*zeta_Bd2LcLc);
	ws.import(*l_Bd2LcLc);




	
	ws.import(*alphalBs2LcLc);
	ws.import(*alpharBs2LcLc);
	ws.import(*nlBs2LcLc);
	ws.import(*nrBs2LcLc);
	ws.import(*sigmaBs2LcLc);
	ws.import(*beta_Bs2LcLc);
	ws.import(*zeta_Bs2LcLc);
	ws.import(*l_Bs2LcLc);


	for(int i=0; i<4; i++){
		//现在Run1的数据还没下来，先跳过
		if(years[i] == 1112) continue;

		ws.import(*tau_DsD[i]);
		ws.import(*m0_DsD[i]);
		ws.import(*c_DsD[i]);
		//// Detector response function
		ws.import(*m0_g_DsD[i]);


		ws.import(*nBd_DsD[i]);
		ws.import(*nNonDD_DsD[i]);
		ws.import(*nBs_DsD[i]);
		ws.import(*nExpBkg_DsD[i]);
		ws.import(*nArgusBkg_DsD[i]);



		ws.import(*tau_DsDs[i]);
		ws.import(*m0_DsDs[i]);
		ws.import(*c_DsDs[i]);

		//// Detector response function
		ws.import(*m0_g_DsDs[i]);

		
		ws.import(*nBs_DsDs[i]);
		ws.import(*nNonDD_DsDs[i]);
		ws.import(*nBdBkg_DsDs[i]);
		ws.import(*nExpBkg_DsDs[i]);
		ws.import(*nArgusBkg_DsDs[i]);



		ws.import(*tau_Bd2LcLc[i]);
		
		ws.import(*nBd_LcLc[i]);
		ws.import(*nBs_LcLc[i]);
		ws.import(*nExpBkg_LcLc[i]);
	}
    fWS.cd();
    ws.Write("", TObject::kOverwrite);
    fWS.Close();

	for(int i=0; i<4; i++){
		//现在Run1的数据还没下来，先跳过
		if(years[i] == 1112) continue;

        TString yearStr = "Run2";
        if(years[i] ==2) yearStr = "Run"+i2s(years[i]);
        else yearStr = i2s(years[i]);

        TString yearCut = "(1)";
        if(years[i] == 1112) yearCut = "(year == 2011 || year==2012)";
        else if(years[i] == 2) yearCut = "(year == 2015 || year==2016 || year == 2017 || year==2018)";
        else if(years[i] == 1516) yearCut = "(year == 2015 || year==2016)";
        else yearCut = "(year == " +i2s(years[i])+")";

		//********************* plot for Bd2DsD *****************///
		TPaveText text_DsD(0.15, 0.85, 0.35, 0.95, "NDC");
		text_DsD.SetFillColor(0);
		text_DsD.SetFillStyle(0);
		text_DsD.SetLineColor(0);
		text_DsD.SetLineWidth(0);
		text_DsD.SetBorderSize(1);
		text_DsD.SetTextSize(2*0.05);
		
		text_DsD.AddText(yearStr);

		TCanvas *canv_DsD = new TCanvas("canv_DsD"+yearStr, "canv_DsD"+yearStr, 800, 600);
		canv_DsD->SetFillColor(10);
		canv_DsD->SetBorderMode(0);
		canv_DsD->cd();
		canv_DsD->cd(1);
		
	 	RooPlot *mframe_DsD = mDsD->frame(Title("Bd2DsD "+yearStr+" data"));
	 	
	 	cout<<"Fit the data"<<endl;
	 	
	 	datars_DsD[i]->plotOn(mframe_DsD, Name("data"));
	 	totPDF_DsD[i]->plotOn(mframe_DsD, Name("signal Bd"), Components(*modelBd2DsD), LineColor(3), LineStyle(kDashed));
		totPDF_DsD[i]->plotOn(mframe_DsD, Name("nonDD Gauss"), Components(*modelNonDDBd2DsD), LineColor(13), LineStyle(kDashed));
	 	totPDF_DsD[i]->plotOn(mframe_DsD, Name("Bs bkg"), Components(*modelBs2DsD), LineColor(6), LineStyle(kDashed));
	 	totPDF_DsD[i]->plotOn(mframe_DsD, Name("exp bkg"), Components(*expBkg_DsD[i]), LineColor(4), LineStyle(kDashed));
	 	totPDF_DsD[i]->plotOn(mframe_DsD, Name("prc bkg"), Components(*argusBkg_DsD[i]), LineColor(kOrange), LineStyle(kDashed));
	 	totPDF_DsD[i]->plotOn(mframe_DsD, Name("totPDF_DsD[i]"), LineColor(kRed));
	 	datars_DsD[i]->plotOn(mframe_DsD, Name("data"));
	 	//totPDF_DsD[i]->paramOn(mframe_DsD, Layout(0.65, 0.95, 0.95));

		
		RooHist *pull_DsD = mframe_DsD->pullHist();
		pull_DsD->GetYaxis()->SetTitle("Pull");
		RooPlot *framePull_DsD = mDsD->frame();
		framePull_DsD->addPlotable(pull_DsD, "P");
		
		mframe_DsD->SetTitle(fitVar);
		mframe_DsD->SetXTitle("#it{mDsD} [MeV/c^{2}]");
		mframe_DsD->SetYTitle(Form("Candidates / (%.1lf MeV/c^{2})", binWidth));
		mframe_DsD->GetXaxis()->SetTitleSize(0.2);
		mframe_DsD->GetYaxis()->SetTitleSize(0.2);
		
		framePull_DsD->SetTitle("");
		framePull_DsD->GetYaxis()->SetTitle("Pull");
		framePull_DsD->SetMinimum(-6.);
		framePull_DsD->SetMaximum(6.);
		framePull_DsD->SetMarkerStyle(2);
		//cout<<"-----------------1"<<endl;
		
		TPad *p1_DsD = new TPad("pad1", "pad1", 0.02, 0.2, 0.98, 0.98, 0);
		TPad *p2_DsD = new TPad("pad2", "pad2", 0.02, 0.04, 0.98, 0.2, 0);
		p1_DsD->Draw();
		p2_DsD->Draw();
		
		p1_DsD->cd();
		mframe_DsD->GetYaxis()->SetTitleOffset(1.00);
		mframe_DsD->GetYaxis()->SetLabelSize(0.05);
		mframe_DsD->GetYaxis()->SetTitleSize(0.05);
		mframe_DsD->GetYaxis()->SetNoExponent();
		mframe_DsD->GetXaxis()->SetTitleOffset(1.00);
		mframe_DsD->GetXaxis()->SetTitleSize(0.05);
		mframe_DsD->GetXaxis()->SetLabelSize(0.05);
		mframe_DsD->Draw("E1");
		text_DsD.Draw("same");
		
		p2_DsD->cd();
		p2_DsD->SetTickx();
		framePull_DsD->GetYaxis()->SetTitleOffset(0.17);
		framePull_DsD->GetXaxis()->SetTitleSize(0.0);
		framePull_DsD->GetXaxis()->SetTickLength(0.09);
		framePull_DsD->GetYaxis()->SetTitleSize(0.2);
		framePull_DsD->GetXaxis()->SetLabelSize(0.0);
		framePull_DsD->GetYaxis()->SetLabelSize(0.2);
		framePull_DsD->GetYaxis()->SetNdivisions(5);
		framePull_DsD->Draw("E1");
		
		TLine* lineZero_DsD = new TLine(mDsD->getMin(), 0, mDsD->getMax(), 0);
		lineZero_DsD->SetLineStyle(kDashed);
		lineZero_DsD->SetLineColor(kBlack);
		lineZero_DsD->Draw();
		
		TLine* lineTwoSigUp_DsD = new TLine(mDsD->getMin(), 3, mDsD->getMax(), 3);
		lineTwoSigUp_DsD->SetLineColor(kRed);
		lineTwoSigUp_DsD->Draw();
		TLine* lineTwoSigDown_DsD = new TLine(mDsD->getMin(), -3, mDsD->getMax(), -3);
		lineTwoSigDown_DsD->SetLineColor(kRed);
		lineTwoSigDown_DsD->Draw();


	 	//canv_DsD->SaveAs("/mnt/d/lhcb/B2XcXc/public_html/fit/final_fit/B2LcLc/simultaneous_fit/"+yearStr+"_data_Bd2DsD_final_fit.pdf");

		p1_DsD->SetLogy();
		canv_DsD->SaveAs("/mnt/d/lhcb/B2XcXc/public_html/fit/final_fit/B2LcLc/simultaneous_fit/"+yearStr+"_data_Bd2DsD_final_fit_log.pdf");



		//********************* plot for Bs2DsDs *****************///
		TPaveText text_DsDs(0.15, 0.85, 0.35, 0.95, "NDC");
		text_DsDs.SetFillColor(0);
		text_DsDs.SetFillStyle(0);
		text_DsDs.SetLineColor(0);
		text_DsDs.SetLineWidth(0);
		text_DsDs.SetBorderSize(1);
		text_DsDs.SetTextSize(2*0.05);
		
		text_DsDs.AddText(yearStr);

		TCanvas *canv_DsDs = new TCanvas("canv_DsDs"+yearStr, "canv_DsDs"+yearStr, 800, 600);
		canv_DsDs->SetFillColor(10);
		canv_DsDs->SetBorderMode(0);
		canv_DsDs->cd();
		canv_DsDs->cd(1);
		
	 	RooPlot *mframe_DsDs = mDsDs->frame(Title("Bs2DsDs "+yearStr+" data"));
	 	
	 	cout<<"Fit the data"<<endl;
	 	
	 	datars_DsDs[i]->plotOn(mframe_DsDs, Name("data"));
	 	totPDF_DsDs[i]->plotOn(mframe_DsDs, Name("signal Bd"), Components(*modelBs2DsDs), LineColor(3), LineStyle(kDashed));
		totPDF_DsDs[i]->plotOn(mframe_DsDs, Name("nonDD Gauss"), Components(*modelNonDDBs2DsDs), LineColor(13), LineStyle(kDashed));
		totPDF_DsDs[i]->plotOn(mframe_DsDs, Name("Bd misID"), Components(*Bd_misID_shape), LineColor(2), LineStyle(kDashed));
	 	totPDF_DsDs[i]->plotOn(mframe_DsDs, Name("exp bkg"), Components(*expBkg_DsDs[i]), LineColor(4), LineStyle(kDashed));
	 	totPDF_DsDs[i]->plotOn(mframe_DsDs, Name("prc bkg"), Components(*argusBkg_DsDs[i]), LineColor(kOrange), LineStyle(kDashed));
	 	totPDF_DsDs[i]->plotOn(mframe_DsDs, Name("totPDF_DsDs[i]"), LineColor(kRed));
	 	datars_DsDs[i]->plotOn(mframe_DsDs, Name("data"));
	 	//totPDF_DsDs[i]->paramOn(mframe_DsDs, Layout(0.65, 0.95, 0.95));

		
		RooHist *pull_DsDs = mframe_DsDs->pullHist();
		pull_DsDs->GetYaxis()->SetTitle("Pull");
		RooPlot *framePull_DsDs = mDsDs->frame();
		framePull_DsDs->addPlotable(pull_DsDs, "P");
		
		mframe_DsDs->SetTitle(fitVar);
		mframe_DsDs->SetXTitle("#it{mDsDs} [MeV/c^{2}]");
		mframe_DsDs->SetYTitle(Form("Candidates / (%.1lf MeV/c^{2})", binWidth));
		mframe_DsDs->GetXaxis()->SetTitleSize(0.2);
		mframe_DsDs->GetYaxis()->SetTitleSize(0.2);
		
		framePull_DsDs->SetTitle("");
		framePull_DsDs->GetYaxis()->SetTitle("Pull");
		framePull_DsDs->SetMinimum(-6.);
		framePull_DsDs->SetMaximum(6.);
		framePull_DsDs->SetMarkerStyle(2);
		//cout<<"-----------------1"<<endl;
		
		TPad *p1_DsDs = new TPad("pad1", "pad1", 0.02, 0.2, 0.98, 0.98, 0);
		TPad *p2_DsDs = new TPad("pad2", "pad2", 0.02, 0.04, 0.98, 0.2, 0);
		p1_DsDs->Draw();
		p2_DsDs->Draw();
		
		p1_DsDs->cd();
		mframe_DsDs->GetYaxis()->SetTitleOffset(1.00);
		mframe_DsDs->GetYaxis()->SetLabelSize(0.05);
		mframe_DsDs->GetYaxis()->SetTitleSize(0.05);
		mframe_DsDs->GetYaxis()->SetNoExponent();
		mframe_DsDs->GetXaxis()->SetTitleOffset(1.00);
		mframe_DsDs->GetXaxis()->SetTitleSize(0.05);
		mframe_DsDs->GetXaxis()->SetLabelSize(0.05);
		mframe_DsDs->Draw("E1");
		text_DsDs.Draw("same");
		
		p2_DsDs->cd();
		p2_DsDs->SetTickx();
		framePull_DsDs->GetYaxis()->SetTitleOffset(0.17);
		framePull_DsDs->GetXaxis()->SetTitleSize(0.0);
		framePull_DsDs->GetXaxis()->SetTickLength(0.09);
		framePull_DsDs->GetYaxis()->SetTitleSize(0.2);
		framePull_DsDs->GetXaxis()->SetLabelSize(0.0);
		framePull_DsDs->GetYaxis()->SetLabelSize(0.2);
		framePull_DsDs->GetYaxis()->SetNdivisions(5);
		framePull_DsDs->Draw("E1");
		
		TLine* lineZero_DsDs = new TLine(mDsDs->getMin(), 0, mDsDs->getMax(), 0);
		lineZero_DsDs->SetLineStyle(kDashed);
		lineZero_DsDs->SetLineColor(kBlack);
		lineZero_DsDs->Draw();
		
		TLine* lineTwoSigUp_DsDs = new TLine(mDsDs->getMin(), 3, mDsDs->getMax(), 3);
		lineTwoSigUp_DsDs->SetLineColor(kRed);
		lineTwoSigUp_DsDs->Draw();
		TLine* lineTwoSigDown_DsDs = new TLine(mDsDs->getMin(), -3, mDsDs->getMax(), -3);
		lineTwoSigDown_DsDs->SetLineColor(kRed);
		lineTwoSigDown_DsDs->Draw();


	 	//canv_DsDs->SaveAs("/mnt/d/lhcb/B2XcXc/public_html/fit/final_fit/B2LcLc/simultaneous_fit/"+yearStr+"_data_Bs2DsDs_final_fit.pdf");

		p1_DsDs->SetLogy();
		canv_DsDs->SaveAs("/mnt/d/lhcb/B2XcXc/public_html/fit/final_fit/B2LcLc/simultaneous_fit/"+yearStr+"_data_Bs2DsDs_final_fit_log.pdf");



		//********************* plot for LcLc *****************///
		TPaveText text_LcLc(0.15, 0.85, 0.35, 0.95, "NDC");
		text_LcLc.SetFillColor(0);
		text_LcLc.SetFillStyle(0);
		text_LcLc.SetLineColor(0);
		text_LcLc.SetLineWidth(0);
		text_LcLc.SetBorderSize(1);
		text_LcLc.SetTextSize(2*0.05);
		
		text_LcLc.AddText(yearStr);

		TCanvas *canv_LcLc = new TCanvas("canv_LcLc"+yearStr, "canv_LcLc"+yearStr, 800, 600);
		canv_LcLc->SetFillColor(10);
		canv_LcLc->SetBorderMode(0);
		canv_LcLc->cd();
		canv_LcLc->cd(1);
		
	 	RooPlot *mframe_LcLc = mLcLc->frame(Title("LcLc "+yearStr+" data"));
	 	
	 	cout<<"Fit the data"<<endl;
	 	
	 	datars_LcLc[i]->plotOn(mframe_LcLc, Name("data"));
	 	totPDF_LcLc[i]->plotOn(mframe_LcLc, Name("signal Bd"), Components(*modelBd2LcLc), LineColor(3), LineStyle(kDashed));
	 	totPDF_LcLc[i]->plotOn(mframe_LcLc, Name("signal Bs"), Components(*modelBs2LcLc), LineColor(6), LineStyle(kDashed));
	 	totPDF_LcLc[i]->plotOn(mframe_LcLc, Name("exp bkg"), Components(*expBkg_LcLc[i]), LineColor(4), LineStyle(kDashed));
	 	totPDF_LcLc[i]->plotOn(mframe_LcLc, Name("totPDF_LcLc[i]"), LineColor(kRed));
	 	datars_LcLc[i]->plotOn(mframe_LcLc, Name("data"));
	 	//totPDF_LcLc[i]->paramOn(mframe_LcLc, Layout(0.65, 0.95, 0.95));

		
		RooHist *pull_LcLc = mframe_LcLc->pullHist();
		pull_LcLc->GetYaxis()->SetTitle("Pull");
		RooPlot *framePull_LcLc = mLcLc->frame();
		framePull_LcLc->addPlotable(pull_LcLc, "P");
		
		mframe_LcLc->SetTitle(fitVar);
		mframe_LcLc->SetXTitle("#it{mLcLc} [MeV/c^{2}]");
		mframe_LcLc->SetYTitle(Form("Candidates / (%.1lf MeV/c^{2})", binWidth));
		mframe_LcLc->GetXaxis()->SetTitleSize(0.2);
		mframe_LcLc->GetYaxis()->SetTitleSize(0.2);
		
		framePull_LcLc->SetTitle("");
		framePull_LcLc->GetYaxis()->SetTitle("Pull");
		framePull_LcLc->SetMinimum(-6.);
		framePull_LcLc->SetMaximum(6.);
		framePull_LcLc->SetMarkerStyle(2);
		//cout<<"-----------------1"<<endl;
		
		TPad *p1_LcLc = new TPad("pad1", "pad1", 0.02, 0.2, 0.98, 0.98, 0);
		TPad *p2_LcLc = new TPad("pad2", "pad2", 0.02, 0.04, 0.98, 0.2, 0);
		p1_LcLc->Draw();
		p2_LcLc->Draw();
		
		p1_LcLc->cd();
		mframe_LcLc->GetYaxis()->SetTitleOffset(1.00);
		mframe_LcLc->GetYaxis()->SetLabelSize(0.05);
		mframe_LcLc->GetYaxis()->SetTitleSize(0.05);
		mframe_LcLc->GetYaxis()->SetNoExponent();
		mframe_LcLc->GetXaxis()->SetTitleOffset(1.00);
		mframe_LcLc->GetXaxis()->SetTitleSize(0.05);
		mframe_LcLc->GetXaxis()->SetLabelSize(0.05);
		mframe_LcLc->Draw("E1");
		text_LcLc.Draw("same");
		
		p2_LcLc->cd();
		p2_LcLc->SetTickx();
		framePull_LcLc->GetYaxis()->SetTitleOffset(0.17);
		framePull_LcLc->GetXaxis()->SetTitleSize(0.0);
		framePull_LcLc->GetXaxis()->SetTickLength(0.09);
		framePull_LcLc->GetYaxis()->SetTitleSize(0.2);
		framePull_LcLc->GetXaxis()->SetLabelSize(0.0);
		framePull_LcLc->GetYaxis()->SetLabelSize(0.2);
		framePull_LcLc->GetYaxis()->SetNdivisions(5);
		framePull_LcLc->Draw("E1");
		
		TLine* lineZero_LcLc = new TLine(mLcLc->getMin(), 0, mLcLc->getMax(), 0);
		lineZero_LcLc->SetLineStyle(kDashed);
		lineZero_LcLc->SetLineColor(kBlack);
		lineZero_LcLc->Draw();
		
		TLine* lineTwoSigUp_LcLc = new TLine(mLcLc->getMin(), 3, mLcLc->getMax(), 3);
		lineTwoSigUp_LcLc->SetLineColor(kRed);
		lineTwoSigUp_LcLc->Draw();
		TLine* lineTwoSigDown_LcLc = new TLine(mLcLc->getMin(), -3, mLcLc->getMax(), -3);
		lineTwoSigDown_LcLc->SetLineColor(kRed);
		lineTwoSigDown_LcLc->Draw();


	 	canv_LcLc->SaveAs("/mnt/d/lhcb/B2XcXc/public_html/fit/final_fit/B2LcLc/simultaneous_fit/"+yearStr+"_data_LcLc_final_fit.pdf");

		////p1_LcLc->SetLogy();
		////canv_LcLc->SaveAs("/mnt/d/lhcb/B2XcXc/public_html/fit/final_fit/B2LcLc/simultaneous_fit/"+yearStr+"_data_LcLc_final_fit_log.pdf");



		//out_data_yields<<setw(w)<<"Bd2DsD simultaneous fit params:"<<endl;
		//out_data_yields<<setw(w)<<yearStr;
		//out_data_yields<<" & "<<setw(w)<<"$"<<dbl2str(nBd_DsD[i]->getVal(),1)<<"\\pm"<<dbl2str(nBd_DsD[i]->getError(),1)<<"$";
		//out_data_yields<<" & "<<setw(w)<<"$"<<dbl2str(nNonDD_DsD[i]->getVal(),0)<<"\\pm"<<dbl2str(nNonDD_DsD[i]->getError(),0)<<"$";
        //out_data_yields<<" & "<<setw(w)<<"$"<<dbl2str(nBs_DsD[i]->getVal(),1)<<"\\pm"<<dbl2str(nBs_DsD[i]->getError(),1)<<"$";
		//out_data_yields<<" & "<<setw(w)<<"$"<<dbl2str(nExpBkg_DsD[i]->getVal(),1)<<"\\pm"<<dbl2str(nExpBkg_DsD[i]->getError(),1)<<"$";
		//out_data_yields<<" & "<<setw(w)<<"$"<<dbl2str(nArgusBkg_DsD[i]->getVal(),1)<<"\\pm"<<dbl2str(nArgusBkg_DsD[i]->getError(),1)<<"$";
		//out_data_yields<<"\\\\"<<endl;
		//out_data_meanSigma<<setw(w)<<yearStr;
		//out_data_meanSigma<<" & "<<setw(w)<<"$"<<dbl2str(meanBd->getVal(),1)<<"\\pm"<<dbl2str(meanBd->getError(),1)<<"$";
		//out_data_meanSigma<<" & "<<setw(w)<<"$"<<dbl2str(fSigmaDataMCBd->getVal(),1)<<"\\pm"<<dbl2str(fSigmaDataMCBd->getError(),1)<<"$";
		//out_data_meanSigma<<"\\\\"<<endl;
		//out_data_bkgParams<<setw(w)<<yearStr;
		//out_data_bkgParams<<" & "<<setw(w)<<"$"<<dbl2str(tau_DsD[i]->getVal()*1e2,1)<<"\\pm"<<dbl2str(tau_DsD[i]->getError()*1e2,1)<<"$";
		//out_data_bkgParams<<" & "<<setw(w)<<"$"<<dbl2str(m0_DsD[i]->getVal(),1)<<"\\pm"<<dbl2str(m0_DsD[i]->getError(),1)<<"$";
		//out_data_bkgParams<<" & "<<setw(w)<<"$"<<dbl2str(c_DsD[i]->getVal(),1)<<"\\pm"<<dbl2str(c_DsD[i]->getError(),1)<<"$";
		//out_data_bkgParams<<"\\\\"<<endl<<endl<<endl<<endl;



		//out_data_yields<<setw(w)<<"Bs2DsDs simultaneous fit params:"<<endl;
		//out_data_yields<<setw(w)<<yearStr;
        //out_data_yields<<" & "<<setw(w)<<"$"<<dbl2str(nBs_DsDs[i]->getVal(),1)<<"\\pm"<<dbl2str(nBs_DsDs[i]->getError(),1)<<"$";
		//out_data_yields<<" & "<<setw(w)<<"$"<<dbl2str(nNonDD_DsDs[i]->getVal(),0)<<"\\pm"<<dbl2str(nNonDD_DsDs[i]->getError(),0)<<"$";
		//out_data_yields<<" & "<<setw(w)<<"$"<<dbl2str(nBdBkg_DsDs[i]->getVal(),1)<<"\\pm"<<dbl2str(nBdBkg_DsDs[i]->getError(),1)<<"$";
		//out_data_yields<<" & "<<setw(w)<<"$"<<dbl2str(nExpBkg_DsDs[i]->getVal(),1)<<"\\pm"<<dbl2str(nExpBkg_DsDs[i]->getError(),1)<<"$";
		//out_data_yields<<" & "<<setw(w)<<"$"<<dbl2str(nArgusBkg_DsDs[i]->getVal(),1)<<"\\pm"<<dbl2str(nArgusBkg_DsDs[i]->getError(),1)<<"$";
		//out_data_yields<<"\\\\"<<endl;
		//out_data_meanSigma<<setw(w)<<yearStr;
		//out_data_meanSigma<<" & "<<setw(w)<<"$"<<dbl2str(meanBs->getVal(),1)<<"\\pm"<<dbl2str(meanBs->getError(),1)<<"$";
		//out_data_meanSigma<<" & "<<setw(w)<<"$"<<dbl2str(fSigmaDataMCBs->getVal(),1)<<"\\pm"<<dbl2str(fSigmaDataMCBs->getError(),1)<<"$";
		//out_data_meanSigma<<"\\\\"<<endl;
		//out_data_bkgParams<<setw(w)<<yearStr;
		//out_data_bkgParams<<" & "<<setw(w)<<"$"<<dbl2str(tau_DsDs[i]->getVal()*1e2,1)<<"\\pm"<<dbl2str(tau_DsDs[i]->getError()*1e2,1)<<"$";
		//out_data_bkgParams<<" & "<<setw(w)<<"$"<<dbl2str(m0_DsDs[i]->getVal(),1)<<"\\pm"<<dbl2str(m0_DsDs[i]->getError(),1)<<"$";
		//out_data_bkgParams<<" & "<<setw(w)<<"$"<<dbl2str(c_DsDs[i]->getVal(),1)<<"\\pm"<<dbl2str(c_DsDs[i]->getError(),1)<<"$";
		//out_data_bkgParams<<"\\\\"<<endl<<endl<<endl<<endl;
	}


	out_MCBd.close();
	out_data_yields.close();
	out_data_meanSigma.close();
	out_data_bkgParams.close();


	cout << endl << "params print to "+fTableMCBd+" "<< endl;
	system("cat "+fTableMCBd);
	cout << endl;
	system("cat "+fTableMCBs);

	cout << endl << "params print to "+fTableData+" "<< endl;
	system("rm " + fTableData);

	system("cat "+fTableData1+">>"+fTableData);
	system("cat "+fTableData2+">>"+fTableData);
	system("cat "+fTableData3+">>"+fTableData);

	system("rm /mnt/d/lhcb/B2XcXc/public_html/fitParamTables/final_fit/data_B2LcLc_*.dat");
	cout << endl;
	system("cat "+fTableData);

	cout << "yields saved into " + sYields << endl;
	fDump.Close();


	RooFitResult *result = minuit.save();
	RooArgList paramlist = result->floatParsFinal();
	RooMultiVarGaussian mg("mg","mg", paramlist, *result);
	RooArgList veclist;
	for(int i=0; i<paramlist.getSize(); i++)
	{
		veclist.add(paramlist[i]);
		cout << i << " "<<"parameters:"<<endl;
		paramlist[i].Print();
	}
	system("rm Dump.root");
}


void fit_data_B2LcLc()
{
	fit_BFs(getFinalSel());
	gROOT->ProcessLine(".q");
}

void toyStudy(TString toyDir, TString yearStr)
{
	lhcbStyle(0.063);
	
	TFile fWS(toyDir+"/fitVarsWorkspace.root");
	RooWorkspace* ws = (RooWorkspace*)fWS.Get("workspace"+yearStr);
	if(!ws){
		cerr << "No workspace"+yearStr + " in " + toyDir+"/fitVarsWorkspace.root" << endl;
		return;
	}
	
	int nBins = 50;
	
	TString namefPoisson = toyDir+"/toyVarsPoisson.root";
	TString fitVar = "B_DTFM_M_0";


	cout << "save possion resonanced vars..." << endl;
	// making possion resonance
	cout << "nPoisson: ";
	TRandom3 rand;

	bool savefPoisson = true;

	if(savefPoisson){
		TFile fPoisson(namefPoisson, "RECREATE");
		TTree* t = new TTree("DecayTree", "DecayTree");
		
		// 先把泊松分布的变量存到root里面	
		double nBd_D, nBs_D, c_D, tau_D, nExpBkg_D, nArgusBkg_D, meanBs_D;
		t->Branch("nBd", &nBd_D, "nBd/D");
		t->Branch("nBs", &nBs_D, "nBs/D");
		//t->Branch("c", &c_D, "c/D");
		t->Branch("tau", &tau_D, "tau/D");
		t->Branch("nExpBkg", &nExpBkg_D, "nExpBkg/D");
		//t->Branch("nArgusBkg", &nArgusBkg_D, "nArgusBkg/D");
		t->Branch("meanBs", &meanBs_D, "meanBs/D");

		RooRealVar* nBd = (RooRealVar*) ws->var("nBd");
		RooRealVar* nBs = (RooRealVar*) ws->var("nBs");
		//RooRealVar* c = (RooRealVar*) ws->var("c");
		RooRealVar* tau = (RooRealVar*) ws->var("tau");
		RooRealVar* nExpBkg = (RooRealVar*) ws->var("nExpBkg");
		//RooRealVar* nArgusBkg = (RooRealVar*) ws->var("nArgusBkg");
		RooRealVar* meanBs = (RooRealVar*) ws->var("meanBs");

		for(int nPoisson=0; nPoisson<1000; nPoisson++){
			nBd_D = rand.Poisson(nBd->getVal());
			nBs_D = rand.Poisson(nBs->getVal());
			//c_D = rand.Poisson(c->getVal()*1.0e4)/1.0e4;
			tau_D = -1.0*rand.Poisson(fabs(tau->getVal())*1.0e6)/1.0e6;
			nExpBkg_D = rand.Poisson(nExpBkg->getVal());
			//nArgusBkg_D = rand.Poisson(nArgusBkg->getVal());
			meanBs_D = rand.Poisson(meanBs->getVal()*1.0e4)/1.0e4;

			t->Fill();
		}
		fPoisson.cd();
		t->Write();
		fPoisson.Close();
	}


	//TFile fPoisson(namefPoisson);
	//TTree* t = (TTree*)fPoisson.Get("DecayTree");

	//TString namePoissonTmp = namefPoisson+"Tmp";
	//TFile fPoissonTmp(namePoissonTmp, "RECREATE");
	//fPoissonTmp.cd();
	//TTree* tNew = t->CloneTree(0);

	//// 读取需要震荡的参数
	////double nBd_D, nBs_D, c_D, tau_D, nExpBkg_D, nArgusBkg_D, meanBs_D;
	//double nBd_D, nBs_D, tau_D, nExpBkg_D, meanBs_D;
	//t->SetBranchAddress("nBd", &nBd_D);
	//t->SetBranchAddress("nBs", &nBs_D);
	////t->SetBranchAddress("c", &c_D);
	//t->SetBranchAddress("tau", &tau_D);
	//t->SetBranchAddress("nExpBkg", &nExpBkg_D);
	////t->SetBranchAddress("nArgusBkg", &nArgusBkg_D);
	//t->SetBranchAddress("meanBs", &meanBs_D);


	//// 震荡后产生的参数
	////double nBd_fitVal, nBs_fitVal, nExpBkg_fitVal, nArgusBkg_fitVal, c_fitVal, tau_fitVal, meanBs_fitVal;
	//double nBd_fitVal, nBs_fitVal, nExpBkg_fitVal, tau_fitVal, meanBs_fitVal;
	//t->SetBranchStatus("nBd_fitVal", 0);
	//t->SetBranchStatus("nBs_fitVal", 0);
	//t->SetBranchStatus("nExpBkg_fitVal", 0);
	////t->SetBranchStatus("nArgusBkg_fitVal", 0);
	////t->SetBranchStatus("c_fitVal", 0);
	//t->SetBranchStatus("tau_fitVal", 0);
	//t->SetBranchStatus("meanBs_fitVal", 0);
	//tNew->Branch("nBd_fitVal", &nBd_fitVal, "nBd_fitVal/D");
	//tNew->Branch("nBs_fitVal", &nBs_fitVal, "nBs_fitVal/D");
	//tNew->Branch("nExpBkg_fitVal", &nExpBkg_fitVal, "nExpBkg_fitVal/D");
	////tNew->Branch("nArgusBkg_fitVal", &nArgusBkg_fitVal, "nArgusBkg_fitVal/D");
	////tNew->Branch("c_fitVal", &c_fitVal, "c_fitVal/D");
	//tNew->Branch("tau_fitVal", &tau_fitVal, "tau_fitVal/D");
	//tNew->Branch("meanBs_fitVal", &meanBs_fitVal, "meanBs_fitVal/D");
	//
	////double nBd_fitErr, nBs_fitErr, nExpBkg_fitErr, nArgusBkg_fitErr, c_fitErr, tau_fitErr, meanBs_fitErr;
	//double nBd_fitErr, nBs_fitErr, nExpBkg_fitErr, tau_fitErr, meanBs_fitErr;
	//t->SetBranchStatus("nBd_fitErr", 0);
	//t->SetBranchStatus("nBs_fitErr", 0);
	//t->SetBranchStatus("nExpBkg_fitErr", 0);
	////t->SetBranchStatus("nArgusBkg_fitErr", 0);
	////t->SetBranchStatus("c_fitErr", 0);
	//t->SetBranchStatus("tau_fitErr", 0);
	//t->SetBranchStatus("meanBs_fitErr", 0);
	//tNew->Branch("nBd_fitErr", &nBd_fitErr, "nBd_fitErr/D");
	//tNew->Branch("nBs_fitErr", &nBs_fitErr, "nBs_fitErr/D");
	//tNew->Branch("nExpBkg_fitErr", &nExpBkg_fitErr, "nExpBkg_fitErr/D");
	////tNew->Branch("nArgusBkg_fitErr", &nArgusBkg_fitErr, "nArgusBkg_fitErr/D");
	////tNew->Branch("c_fitErr", &c_fitErr, "c_fitErr/D");
	//tNew->Branch("tau_fitErr", &tau_fitErr, "tau_fitErr/D");
	//tNew->Branch("meanBs_fitErr", &meanBs_fitErr, "meanBs_fitErr/D");
	//
	//double nBd_bias, nBs_bias, nExpBkg_bias, tau_bias, meanBs_bias;
	//t->SetBranchStatus("nBd_bias", 0);
	//t->SetBranchStatus("nBs_bias", 0);
	//t->SetBranchStatus("nExpBkg_bias", 0);
	////t->SetBranchStatus("nArgusBkg_bias", 0);
	////t->SetBranchStatus("c_bias", 0);
	//t->SetBranchStatus("tau_bias", 0);
	//t->SetBranchStatus("meanBs_bias", 0);
	//tNew->Branch("nBd_bias", &nBd_bias, "nBd_bias/D");
	//tNew->Branch("nBs_bias", &nBs_bias, "nBs_bias/D");
	//tNew->Branch("nExpBkg_bias", &nExpBkg_bias, "nExpBkg_bias/D");
	////tNew->Branch("nArgusBkg_bias", &nArgusBkg_bias, "nArgusBkg_bias/D");
	////tNew->Branch("c_bias", &c_bias, "c_bias/D");
	//tNew->Branch("tau_bias", &tau_bias, "tau_bias/D");
	//tNew->Branch("meanBs_bias", &meanBs_bias, "meanBs_bias/D");
	//
	//double nBd_pull, nBs_pull, nExpBkg_pull, tau_pull, meanBs_pull;
	//t->SetBranchStatus("nBd_pull", 0);
	//t->SetBranchStatus("nBs_pull", 0);
	//t->SetBranchStatus("nExpBkg_pull", 0);
	////t->SetBranchStatus("nArgusBkg_pull", 0);
	////t->SetBranchStatus("c_pull", 0);
	//t->SetBranchStatus("tau_pull", 0);
	//t->SetBranchStatus("meanBs_pull", 0);
	//tNew->Branch("nBd_pull", &nBd_pull, "nBd_pull/D");
	//tNew->Branch("nBs_pull", &nBs_pull, "nBs_pull/D");
	//tNew->Branch("nExpBkg_pull", &nExpBkg_pull, "nExpBkg_pull/D");
	////tNew->Branch("nArgusBkg_pull", &nArgusBkg_pull, "nArgusBkg_pull/D");
	////tNew->Branch("c_pull", &c_pull, "c_pull/D");
	//tNew->Branch("tau_pull", &tau_pull, "tau_pull/D");
	//tNew->Branch("meanBs_pull", &meanBs_pull, "meanBs_pull/D");
	//
	//for(int nPoisson=0; nPoisson<1000; nPoisson++){
	////for(int nPoisson=0; nPoisson<500; nPoisson++){
	//	t->GetEntry(nPoisson);

	//	//TPaveText text(0.15, 0.85, 0.35, 0.95, "NDC");
	//	TPaveText text(0.15, 0.75, 0.35, 0.90, "NDC");
	//	text.SetFillColor(0);
	//	text.SetFillStyle(0);
	//	text.SetLineColor(0);
	//	text.SetLineWidth(0);
	//	text.SetBorderSize(1);
	//	text.SetTextSize(0.05);
	//	
	//	text.AddText(yearStr);
	//	text.AddText("nPoisson: "+i2s(nPoisson));
	//	
	//	////---------Build background PDF------
	//	RooRealVar* m = (RooRealVar*) ws->var(fitVar);
	//	
	//	//RooRealVar* m0 = (RooRealVar*) ws->var("m0");
	//	//RooRealVar* c = (RooRealVar*) ws->var("c");
	//	//m0->setConstant(true);
	//	//c->setVal(c_D);
	//	////c->setConstant(true);

    //	//RooArgusBG* argus_noSmearing = new RooArgusBG("argus_noSmearing","rec background model", *m, *m0, *c);

	//	////// Detector response function
	//	//RooRealVar* m0_g = (RooRealVar*) ws->var("m0_g");
	//	//RooRealVar* sigma_g = (RooRealVar*) ws->var("sigma_g");
	//	//m0_g->setConstant(true);
	//	//sigma_g->setConstant(true);
	//	//
	//	//RooGaussian* gauss = new RooGaussian("gauss", "gauss", *m, *m0_g, *sigma_g);
	//	//RooFFTConvPdf* argusBkg = new RooFFTConvPdf("smearedArgus","argusBkg (X) gauss", *m, *argus_noSmearing, *gauss);
	//	
	//	RooRealVar* tau = (RooRealVar*) ws->var("tau");
	//	tau->setVal(tau_D);
	//	//tau->setConstant(true);
	//	
	//	RooExponential* expBkg = new RooExponential("expBkg","background model", *m, *tau) ;
	//	
	//	
	//	RooRealVar* alphalBs = (RooRealVar*) ws->var("alphalBs");
	//	RooRealVar* alpharBs = (RooRealVar*) ws->var("alpharBs");
	//	RooRealVar* nlBs = (RooRealVar*) ws->var("nlBs");
	//	RooRealVar* nrBs = (RooRealVar*) ws->var("nrBs");
	//	RooRealVar* sigmaBs = (RooRealVar*) ws->var("sigmaBs");
	//	RooRealVar* meanBs = (RooRealVar*) ws->var("meanBs");;
	//	meanBs->setVal(meanBs_D);
	//	//RooRealVar* fSigmaBs = (RooRealVar*) ws->var("fSigmaBs");
	//	//RooRealVar* fCB_Bs = (RooRealVar*) ws->var("fCB_Bs");
	//	RooRealVar* beta_Bs = (RooRealVar*) ws->var("beta_Bs");
	//	RooRealVar* zeta_Bs = (RooRealVar*) ws->var("zeta_Bs");
	//	RooRealVar* l_Bs = (RooRealVar*) ws->var("l_Bs");

	//	alphalBs->setConstant(true);
	//	alpharBs->setConstant(true);
	//	nlBs->setConstant(true);
	//	nrBs->setConstant(true);
	//	sigmaBs->setConstant(true);
	//	//meanBs->setConstant(true);
	//	//fSigmaBs->setConstant(true);
	//	//fCB_Bs->setConstant(true);
	//	beta_Bs->setConstant(true);
	//	zeta_Bs->setConstant(true);
	//	l_Bs->setConstant(true);

	//	//RooFormulaVar* sigma_gausCB_Bs = new RooFormulaVar("sigma_gausCB_Bs", "sigma_gausCB_Bs", "@0*@1", RooArgList(*sigmaBs, *fSigmaBs));
	//	//RooGaussian* gausCB_Bs = new RooGaussian("gausCB_Bs", "gausCB_Bs", *m, *meanBs, *sigma_gausCB_Bs);

	//	////RooDoubleCB* cbBs = new RooDoubleCB("cbBs", "CB for B_{s}", *m, *meanBs, *sigmaBs, *alphalBs, *nlBs, *alpharBs, *nrBs);
	//	//RooCBShape* cbBs = new RooCBShape("cbBs", "cbBs", *m, *meanBs, *sigmaBs, *alphalBs, *nlBs);
	//	//RooAddPdf* modelBs = new RooAddPdf("modelBs", "modelBs", RooArgList(*cbBs, *gausCB_Bs), RooArgList(*fCB_Bs));
	//	
	//	RooIpatia2* modelBs = new RooIpatia2("modelBs", "modelBs", *m, *l_Bs, *zeta_Bs, *beta_Bs, *sigmaBs, *meanBs, *alphalBs, *nlBs, *alpharBs, *nrBs);
	//	
	//	RooRealVar* alphalBd = (RooRealVar*) ws->var("alphalBd");
	//	RooRealVar* alpharBd = (RooRealVar*) ws->var("alpharBd");
	//	RooRealVar* nlBd = (RooRealVar*) ws->var("nlBd");
	//	RooRealVar* nrBd = (RooRealVar*) ws->var("nrBd");
	//	RooRealVar* sigmaBd = (RooRealVar*) ws->var("sigmaBd");
    //	RooFormulaVar* meanBd = new RooFormulaVar("meanBd","meanBd", "@0-87.38", RooArgList(*meanBs));
	//	//RooRealVar* fSigmaBd = (RooRealVar*) ws->var("fSigmaBd");
	//	//RooRealVar* fCB_Bd = (RooRealVar*) ws->var("fCB_Bd");
	//	RooRealVar* beta_Bd = (RooRealVar*) ws->var("beta_Bd");
	//	RooRealVar* zeta_Bd = (RooRealVar*) ws->var("zeta_Bd");
	//	RooRealVar* l_Bd = (RooRealVar*) ws->var("l_Bd");

	//	alphalBd->setConstant(true);
	//	alpharBd->setConstant(true);
	//	nlBd->setConstant(true);
	//	nrBd->setConstant(true);
	//	sigmaBd->setConstant(true);
	//	//meanBd->setConstant(true);
	//	//fSigmaBd->setConstant(true);
	//	//fCB_Bd->setConstant(true);
	//	beta_Bd->setConstant(true);
	//	zeta_Bd->setConstant(true);
	//	l_Bd->setConstant(true);




	//	//RooFormulaVar* sigma_gausCB_Bd = new RooFormulaVar("sigma_gausCB_Bd", "sigma_gausCB_Bd", "@0*@1", RooArgList(*sigmaBd, *fSigmaBd));
	//	//RooGaussian* gausCB_Bd = new RooGaussian("gausCB_Bd", "gausCB_Bd", *m, *meanBd, *sigma_gausCB_Bd);

	//	//RooCBShape* cbBd = new RooCBShape("cbBd", "cbBd", *m, *meanBd, *sigmaBd, *alphalBd, *nlBd);
	//	//RooAddPdf* modelBd = new RooAddPdf("modelBd", "modelBd", RooArgList(*cbBd, *gausCB_Bd), RooArgList(*fCB_Bd));
	//	
	//	RooIpatia2* modelBd = new RooIpatia2("modelBd", "modelBd", *m, *l_Bd, *zeta_Bd, *beta_Bd, *sigmaBd, *meanBd, *alphalBd, *nlBd, *alpharBd, *nrBd);
	//	
	//	RooRealVar* nBd = (RooRealVar*) ws->var("nBd");
	//	RooRealVar* nBs = (RooRealVar*) ws->var("nBs");
	//	RooRealVar* nExpBkg = (RooRealVar*) ws->var("nExpBkg");
	//	//RooRealVar* nArgusBkg = (RooRealVar*) ws->var("nArgusBkg");

	//	// 产额也设置下，也许会快一些
	//	nBd->setVal(nBd_D);
	//	nBs->setVal(nBs_D);
	//	nExpBkg->setVal(nExpBkg_D);
	//	//nArgusBkg->setVal(nArgusBkg_D);
	//	
	//	tau->setRange(-0.2, 0.2);
	//	meanBs->setRange(5320, 5420);

	//	nBd->setRange(0, 1e5);
	//	nBs->setRange(0, 1e5);
	//	nExpBkg->setRange(0, 1e5);

	//	RooDataSet *toy_Bd = modelBd->generate(*m, nBd_D);
	//	RooDataSet *toy_Bs = modelBs->generate(*m, nBs_D);
	//	RooDataSet *toy_ExpBkg = expBkg->generate(*m, nExpBkg_D);
	//	//RooDataSet *toy_ArgusBkg = argusBkg->generate(*m, nArgusBkg_D);
	//	
	//	RooDataSet* datars = new RooDataSet("datars", "datars", *m);
	//	datars->append(*toy_Bd);
	//	datars->append(*toy_Bs);
	//	datars->append(*toy_ExpBkg);
	//	//datars->append(*toy_ArgusBkg);
	//	
	//	//cout << nBd_D << ", "<< nBs_D <<  ", "<< nExpBkg_D <<  ", "<< nArgusBkg_D << endl;
	//	cout << nBd_D << ", "<< nBs_D <<  ", "<< nExpBkg_D  << endl;
	//	
	//	//double nTotEvents = nBd_D + nBs_D + nExpBkg_D + nArgusBkg_D;
	//	double nTotEvents = nBd_D + nBs_D + nExpBkg_D;
	//	
	//	//RooAddPdf* totPDF = new RooAddPdf("totPDF","Total pdf", RooArgList(*modelBd, *modelBs, *expBkg, *argusBkg), RooArgList(*nBd, *nBs, *nExpBkg, *nArgusBkg));
	//	RooAddPdf* totPDF = new RooAddPdf("totPDF","Total pdf", RooArgList(*modelBd, *modelBs, *expBkg), RooArgList(*nBd, *nBs, *nExpBkg));



	//	//////////////////// Fit the data/////////////
	//	//RooFitResult *result = totPDF->fitTo(*datars, Save(true), NumCPU(14));
	//	RooFitResult *result = totPDF->fitTo(*datars, Save(true));

	//	nBd_fitVal = nBd->getVal();
	//	nBs_fitVal = nBs->getVal();
	//	nExpBkg_fitVal = nExpBkg->getVal();
	//	//nArgusBkg_fitVal = nArgusBkg->getVal();
	//	//c_fitVal = c->getVal();
	//	tau_fitVal = tau->getVal();
	//	meanBs_fitVal = meanBs->getVal();
	//	
	//	nBd_fitErr = nBd->getError();
	//	nBs_fitErr = nBs->getError();
	//	nExpBkg_fitErr = nExpBkg->getError();
	//	//nArgusBkg_fitErr = nArgusBkg->getError();
	//	//c_fitErr = c->getError();
	//	tau_fitErr = tau->getError();
	//	meanBs_fitErr = meanBs->getError();
	//	
	//	nBd_bias = (nBd_fitVal - 40.2);
	//	nBs_bias = (nBs_fitVal - 52.51);
	//	nExpBkg_bias = (nExpBkg_fitVal - 209.2);
	//	tau_bias = (tau_fitVal - tau_D);
	//	meanBs_bias = (meanBs_fitVal - meanBs_D);
	//	
	//	nBd_pull = nBd_bias/nBd_fitErr;
	//	nBs_pull = nBs_bias /nBs_fitErr;
	//	nExpBkg_pull = nExpBkg_bias /nExpBkg_fitErr;
	//	tau_pull = tau_bias /tau_fitErr;
	//	meanBs_pull = meanBs_bias /meanBs_fitErr;
	//	
	//	
	//	RooPlot *mframe = m->frame(Title("B2LcLc Run2"));
	//	
	//	cout<<"Fit the data"<<endl;
	//	
	//	datars->plotOn(mframe, Name("data"));
	//	totPDF->plotOn(mframe, Name("model Bd"), Components(*modelBd), LineColor(3), LineStyle(kDashed));
	//	totPDF->plotOn(mframe, Name("model Bs"), Components(*modelBs), LineColor(6), LineStyle(kDashed));
	//	totPDF->plotOn(mframe, Name("exp bkg"), Components(*expBkg), LineColor(4), LineStyle(kDashed));
	//	//totPDF->plotOn(mframe, Name("prc bkg"), Components(*argusBkg), LineColor(kOrange), LineStyle(kDashed));
	//	totPDF->plotOn(mframe, Name("totPDF"), LineColor(kRed));
	//	datars->plotOn(mframe, Name("data"));
	//	totPDF->paramOn(mframe, Layout(0.65, 0.90, 0.85));
	//	
	//	int result_status = result->status();
	//	cout << "result_status = "<< result_status<<endl;
	//	result->Print("v");
	//	
	//	
	//	RooHist *pull = mframe->pullHist();
	//	pull->GetYaxis()->SetTitle("Pull");
	//	RooPlot *framePull = m->frame();
	//	framePull->addPlotable(pull, "P");
	//	
	//	int binWidth = (int)(m->getMax() - m->getMin())/nBins;
	//	mframe->SetTitle(fitVar);
	//	mframe->SetXTitle("#it{m}_{B} [MeV/c^{2}]");
	//	mframe->SetYTitle(Form("Candidates / (%d MeV/c^{2})", binWidth));
	//	mframe->GetXaxis()->SetTitleSize(0.2);
	//	mframe->GetYaxis()->SetTitleSize(0.2);
	//	
	//	framePull->SetTitle("");
	//	framePull->GetYaxis()->SetTitle("Pull");
	//	framePull->SetMinimum(-6.);
	//	framePull->SetMaximum(6.);
	//	framePull->SetMarkerStyle(2);
	//	//cout<<"-----------------1"<<endl;
	//	
	//	TCanvas *canv = new TCanvas("canv", "canv", 800, 600);
	//	canv->SetFillColor(10);
	//	canv->SetBorderMode(0);
	//	canv->cd();
	//	canv->cd(1);
	//	
	//	TPad *p1 = new TPad("pad1", "pad1", 0.02, 0.2, 0.98, 0.98, 0);
	//	TPad *p2 = new TPad("pad2", "pad2", 0.02, 0.04, 0.98, 0.2, 0);
	//	p1->Draw();
	//	p2->Draw();
	//	
	//	p1->cd();
	//	mframe->GetYaxis()->SetTitleOffset(1.00);
	//	mframe->GetYaxis()->SetLabelSize(0.05);
	//	mframe->GetYaxis()->SetTitleSize(0.05);
	//	mframe->GetYaxis()->SetNoExponent();
	//	mframe->GetXaxis()->SetTitleOffset(1.00);
	//	mframe->GetXaxis()->SetTitleSize(0.05);
	//	mframe->GetXaxis()->SetLabelSize(0.05);
	//	mframe->Draw("E1");
	//	text.Draw("same");
	//	
	//	p2->cd();
	//	p2->SetTickx();
	//	framePull->GetYaxis()->SetTitleOffset(0.17);
	//	framePull->GetXaxis()->SetTitleSize(0.0);
	//	framePull->GetXaxis()->SetTickLength(0.09);
	//	framePull->GetYaxis()->SetTitleSize(0.2);
	//	framePull->GetXaxis()->SetLabelSize(0.0);
	//	framePull->GetYaxis()->SetLabelSize(0.2);
	//	framePull->GetYaxis()->SetNdivisions(5);
	//	framePull->Draw("E1");
	//	
	//	TLine* lineZero = new TLine(m->getMin(), 0, m->getMax(), 0);
	//	lineZero->SetLineStyle(kDashed);
	//	lineZero->SetLineColor(kBlack);
	//	lineZero->Draw();
	//	
	//	TLine* lineTwoSigUp = new TLine(m->getMin(), 3, m->getMax(), 3);
	//	lineTwoSigUp->SetLineColor(kRed);
	//	lineTwoSigUp->Draw();
	//	TLine* lineTwoSigDown = new TLine(m->getMin(), -3, m->getMax(), -3);
	//	lineTwoSigDown->SetLineColor(kRed);
	//	lineTwoSigDown->Draw();
	//	
	//	system("mkdir -p "+toyDir+"/plots");
	//	canv->SaveAs(toyDir+"/plots/"+yearStr+"_data_B2LcLc_final_fit_"+i2s(nPoisson)+".pdf");
	//	
	//	delete datars;
	//	
	//	tNew->Fill();
	//}
	//cout<< endl;
	//
	//fPoissonTmp.cd();
	//tNew->Write();
	//fPoissonTmp.Close();
	//fPoisson.Close();

	//system("mv " + namePoissonTmp + " " + namefPoisson);



	
	
	//// 读取泊松中心值
	//RooRealVar* nBd = (RooRealVar*) ws->var("nBd");
	//RooRealVar* nBs = (RooRealVar*) ws->var("nBs");
	//RooRealVar* nExpBkg = (RooRealVar*) ws->var("nExpBkg");
	////RooRealVar* nArgusBkg = (RooRealVar*) ws->var("nArgusBkg");
	////RooRealVar* c = (RooRealVar*) ws->var("c");
	//RooRealVar* tau = (RooRealVar*) ws->var("tau");
	//RooRealVar* meanBs = (RooRealVar*) ws->var("meanBs");
	//
	////cout << "Mean: " << nBd->getVal() << ", "<< nBs->getVal() <<  ", "<< nExpBkg->getVal() <<  ", "<< nArgusBkg->getVal() << ", " << c->getVal() << ", " << tau->getVal()  << ", " << meanBs->getVal() << ", " << endl;
	//cout << "Mean: " << nBd->getVal() << ", "<< nBs->getVal() <<  ", "<< nExpBkg->getVal() <<  ", "<< tau->getVal()  << ", " << meanBs->getVal() << ", " << endl;
	//cout << "Poisson vars saved in " + namefPoisson << endl;
	//
	//
	//fWS.Close();



	//vector<TString> varNames;
	//varNames.push_back("\\texttt{nBd}");
	//varNames.push_back("\\texttt{nBs}");
	//varNames.push_back("\\texttt{nExpBkg}");
	////varNames.push_back("\\texttt{nArgusBkg}");
	////varNames.push_back("\\texttt{c}");
	//varNames.push_back("\\texttt{tau}");
	//varNames.push_back("\\texttt{meanBs}");

	//vector<double> varSetVals;
	//varSetVals.push_back(nBd->getVal());
	//varSetVals.push_back(nBs->getVal());
	//varSetVals.push_back(nExpBkg->getVal());
	////varSetVals.push_back(nArgusBkg->getVal());
	////varSetVals.push_back(c->getVal());
	//varSetVals.push_back(tau->getVal());
	//varSetVals.push_back(meanBs->getVal());
	//

	//vector<vector<ValError>> tableInfo  = fitVarsPull(namefPoisson, toyDir);
	//for(int i=0; i<tableInfo.size(); i++){
	//	int nfixed = 1;
	//	if(varNames[i].Contains("tau")) nfixed=5;

	//	cout  <<setw(12)<< varNames[i];
	//	cout  <<setw(12)<< "& "<< dbl2str(varSetVals[i], nfixed);

	//	for(int j=0; j<tableInfo[0].size(); j++){
	//		if(j>=2) nfixed = 2;

	//		if(j!=2 && j!=3)
	//			cout  <<setw(12)<< "& "<< dbl2str(tableInfo[i][j].val, nfixed);
	//		else
	//			cout  <<setw(12)<< "& "<< tableInfo[i][j].roundToError(1, nfixed);
	//	}
	//	cout << " \\\\" << endl;
	//}
}

vector<vector<ValError>> fitVarsPull(TString nameFile, TString toyDir)
{
	cout << "Fit vars pull using Gaus founction." << endl;
	vector<TString> vars;
	vars.push_back("nBd");
	vars.push_back("nBs");
	vars.push_back("nExpBkg");
	//vars.push_back("nArgusBkg");
	//vars.push_back("c");
	vars.push_back("tau");
	vars.push_back("meanBs");

	vector<double> pull_edges;
	pull_edges.push_back(5);
	pull_edges.push_back(5);
	pull_edges.push_back(5);
	//pull_edges.push_back(5.);
	//pull_edges.push_back(5.);
	pull_edges.push_back(5);
	pull_edges.push_back(5);

	vector<double> bias_edges;
	bias_edges.push_back(35.);
	bias_edges.push_back(35.);
	bias_edges.push_back(60.);
	//bias_edges.push_back(500.);
	//bias_edges.push_back(20.);
	bias_edges.push_back(0.004);
	bias_edges.push_back(10.);

	vector<DoubleDouble> fitVal_edges;
	fitVal_edges.push_back(DoubleDouble(10, 70));
	fitVal_edges.push_back(DoubleDouble(15, 90));
	fitVal_edges.push_back(DoubleDouble(150, 270));
	//fitVal_edges.push_back(DoubleDouble(2700, 3800));
	//fitVal_edges.push_back(DoubleDouble(15, 45));
	fitVal_edges.push_back(DoubleDouble(-0.004, 0.0005));
	fitVal_edges.push_back(DoubleDouble(5360, 5369));

	vector<DoubleDouble> fitErr_edges;
	fitErr_edges.push_back(DoubleDouble(5.5, 9.5));
	fitErr_edges.push_back(DoubleDouble(6, 10.5));
	fitErr_edges.push_back(DoubleDouble(13.5, 18));
	//fitErr_edges.push_back(DoubleDouble(100, 190));
	//fitErr_edges.push_back(DoubleDouble(3, 4.5));
	fitErr_edges.push_back(DoubleDouble(0.54e-3, 0.70e-3));
	fitErr_edges.push_back(DoubleDouble(0.5, 1.25));

	vector<vector<ValError>> table(vars.size(), vector<ValError>(5, ValError(0, 0)));
	//var-0: mean of FitVal
	//var-1: mean of FitErr
	//var-2: mean of Pull Gaus
	//var-3: sigma of Pull Gaus
	//var-4: chi2/ndf of Pull fit

	TFile f(nameFile);
	TTree* t = (TTree*)f.Get("DecayTree");
	if(!t) {
		cerr << "No DecayTree in file: " + nameFile << endl;
		return table;
	}

	// fitVal, fitErr histos
	for(int i=0; i<vars.size(); i++){
    	TPaveText text(0.65, 0.85, 0.95, 0.95, "NDC");
	 	text.SetFillColor(0);
	 	text.SetFillStyle(0);
	 	text.SetLineColor(0);
	 	text.SetLineWidth(0);
	 	text.SetBorderSize(1);
	 	//text.SetTextSize(0.08);

	 	RooRealVar observable(vars[i]+"_fitVal", vars[i]+"_fitVal", fitVal_edges[i].one, fitVal_edges[i].two);
		observable.setBins(50);

		TH1D varHist(vars[i]+"_HistoFitVal", vars[i]+"_HistoFitVal", 50, fitVal_edges[i].one, fitVal_edges[i].two);
		t->Draw(vars[i]+"_fitVal>>" + vars[i]+"_HistoFitVal");

		RooDataHist *datars = new RooDataHist("dataset", "dataset", observable, &varHist);
		
	 	RooPlot *mframe = observable.frame(Title(vars[i]+"_fitVal"));
	 	
	 	cout<<"Fit the data"<<endl;
	 	
	 	datars->plotOn(mframe, Name("data"));
		
		double binWidth = 2*(fitVal_edges[i].two - fitVal_edges[i].one)/50;
		mframe->SetTitle(vars[i]+"_fitVal");
		mframe->SetXTitle(vars[i]+"_fitVal");
		mframe->SetYTitle("Candidates / "+d2s(binWidth));
		mframe->GetXaxis()->SetTitleSize(0.2);
		mframe->GetYaxis()->SetTitleSize(0.2);
		
		TCanvas *canv = new TCanvas("canv", "canv", 800, 600);
		canv->SetFillColor(10);
		canv->SetBorderMode(0);
		canv->cd();
		canv->cd(1);
		
		TPad *p1 = new TPad("pad1", "pad1", 0.02, 0.2, 0.98, 0.98, 0);
		p1->Draw();
		
		p1->cd();
		mframe->GetYaxis()->SetTitleOffset(1.00);
		mframe->GetYaxis()->SetLabelSize(0.05);
		mframe->GetYaxis()->SetTitleSize(0.05);
		mframe->GetYaxis()->SetNoExponent();
		mframe->GetXaxis()->SetTitleOffset(1.00);
		mframe->GetXaxis()->SetTitleSize(0.05);
		mframe->GetXaxis()->SetLabelSize(0.05);
		mframe->Draw("E1");
		text.AddText("Mean: "+d2s(varHist.GetMean()));
		text.AddText("Std Dev: "+d2s(varHist.GetStdDev()));
		text.Draw("same");

		table[i][0] = ValError(varHist.GetMean(), 0);
		
		system("mkdir -p "+toyDir);
		canv->SaveAs(toyDir+"/varFitVal_"+vars[i]+".pdf");
	}
	for(int i=0; i<vars.size(); i++){
    	TPaveText text(0.65, 0.85, 0.95, 0.95, "NDC");
	 	text.SetFillColor(0);
	 	text.SetFillStyle(0);
	 	text.SetLineColor(0);
	 	text.SetLineWidth(0);
	 	text.SetBorderSize(1);
	 	//text.SetTextSize(0.08);

	 	RooRealVar observable(vars[i]+"_fitErr", vars[i]+"_fitErr", fitErr_edges[i].one, fitErr_edges[i].two);
		observable.setBins(50);

		TH1D varHist(vars[i]+"_HistoFitErr", vars[i]+"_HistoFitErr", 50, fitErr_edges[i].one, fitErr_edges[i].two);
		t->Draw(vars[i]+"_fitErr>>" + vars[i]+"_HistoFitErr");

		RooDataHist *datars = new RooDataHist("dataset", "dataset", observable, &varHist);
		
	 	RooPlot *mframe = observable.frame(Title(vars[i]+"_fitErr"));
	 	
	 	cout<<"Fit the data"<<endl;
	 	
	 	datars->plotOn(mframe, Name("data"));
		
		double binWidth = 2*(fitErr_edges[i].two - fitErr_edges[i].one)/50;
		mframe->SetTitle(vars[i]+"_fitErr");
		mframe->SetXTitle(vars[i]+"_fitErr");
		mframe->SetYTitle("Candidates / "+d2s(binWidth));
		mframe->GetXaxis()->SetTitleSize(0.2);
		mframe->GetYaxis()->SetTitleSize(0.2);
		
		TCanvas *canv = new TCanvas("canv", "canv", 800, 600);
		canv->SetFillColor(10);
		canv->SetBorderMode(0);
		canv->cd();
		canv->cd(1);
		
		TPad *p1 = new TPad("pad1", "pad1", 0.02, 0.2, 0.98, 0.98, 0);
		p1->Draw();
		
		p1->cd();
		mframe->GetYaxis()->SetTitleOffset(1.00);
		mframe->GetYaxis()->SetLabelSize(0.05);
		mframe->GetYaxis()->SetTitleSize(0.05);
		mframe->GetYaxis()->SetNoExponent();
		mframe->GetXaxis()->SetTitleOffset(1.00);
		mframe->GetXaxis()->SetTitleSize(0.05);
		mframe->GetXaxis()->SetLabelSize(0.05);
		mframe->Draw("E1");
		text.AddText("Mean: "+d2s(varHist.GetMean()));
		text.AddText("Std Dev: "+d2s(varHist.GetMeanError()));
		text.Draw("same");
		
		table[i][1] = ValError(varHist.GetMean(), 0);

		system("mkdir -p "+toyDir);
		canv->SaveAs(toyDir+"/varFitErr_"+vars[i]+".pdf");
	}

	// fit pull
	for(int i=0; i<vars.size(); i++){
    	TPaveText text(0.25, 0.85, 0.45, 0.95, "NDC");
	 	text.SetFillColor(0);
	 	text.SetFillStyle(0);
	 	text.SetLineColor(0);
	 	text.SetLineWidth(0);
	 	text.SetBorderSize(1);
	 	text.SetTextSize(0.08);


	 	RooRealVar observable(vars[i]+"_pull", vars[i]+"_pull", -1.*pull_edges[i], pull_edges[i]);
		observable.setBins(50);

	 	RooDataSet *datars = new RooDataSet("dataset", "dataset", t, observable);
		
		// Detector response function
		RooRealVar mean("#it{#mu}", "m0", 0, -2, 2) ;
		RooRealVar sigma("#it{#sigma}", "sigma", 2, 0.1, 5);
		RooGaussian gauss("gauss", "gauss", observable, mean, sigma);

		
	 	RooFitResult *result = gauss.fitTo(*datars, Save(true), NumCPU(10));


	 	RooPlot *mframe = observable.frame(Title(vars[i]+"_pull"));
	 	
	 	cout<<"Fit the data"<<endl;
	 	
	 	datars->plotOn(mframe, Name("data"));
	 	gauss.plotOn(mframe, Name("gauss"), LineColor(kBlue));
	 	datars->plotOn(mframe, Name("data"));
	 	gauss.paramOn(mframe, Layout(0.70, 0.95, 0.95));

		int result_status = result->status();
		cout << "result_status = "<< result_status<<endl;
		result->Print("v");
		
		
		RooHist *pull = mframe->pullHist();
		pull->GetYaxis()->SetTitle("Pull");
		RooPlot *framePull = observable.frame();
		framePull->addPlotable(pull, "P");
		
		double binWidth = 2*pull_edges[i]/50;
		mframe->SetTitle(vars[i]+"_pull");
		mframe->SetXTitle(vars[i]+"_pull");
		mframe->SetYTitle("Candidates / "+d2s(binWidth));
		mframe->GetXaxis()->SetTitleSize(0.2);
		mframe->GetYaxis()->SetTitleSize(0.2);
		
		framePull->SetTitle("");
		framePull->GetYaxis()->SetTitle("Pull");
		framePull->SetMinimum(-6.);
		framePull->SetMaximum(6.);
		framePull->SetMarkerStyle(2);
		//cout<<"-----------------1"<<endl;
		
		TCanvas *canv = new TCanvas("canv", "canv", 800, 600);
		canv->SetFillColor(10);
		canv->SetBorderMode(0);
		canv->cd();
		canv->cd(1);
		
		TPad *p1 = new TPad("pad1", "pad1", 0.02, 0.2, 0.98, 0.98, 0);
		TPad *p2 = new TPad("pad2", "pad2", 0.02, 0.04, 0.98, 0.2, 0);
		p1->Draw();
		p2->Draw();
		
		p1->cd();
		mframe->GetYaxis()->SetTitleOffset(1.00);
		mframe->GetYaxis()->SetLabelSize(0.05);
		mframe->GetYaxis()->SetTitleSize(0.05);
		mframe->GetYaxis()->SetNoExponent();
		mframe->GetXaxis()->SetTitleOffset(1.00);
		mframe->GetXaxis()->SetTitleSize(0.05);
		mframe->GetXaxis()->SetLabelSize(0.05);
		mframe->Draw("E1");
		text.AddText("#chi^{2}/ndf: "+d2s(mframe->chiSquare()));
		text.Draw("same");
		
		p2->cd();
		p2->SetTickx();
		framePull->GetYaxis()->SetTitleOffset(0.17);
		framePull->GetXaxis()->SetTitleSize(0.0);
		framePull->GetXaxis()->SetTickLength(0.09);
		framePull->GetYaxis()->SetTitleSize(0.2);
		framePull->GetXaxis()->SetLabelSize(0.0);
		framePull->GetYaxis()->SetLabelSize(0.2);
		framePull->GetYaxis()->SetNdivisions(5);
		framePull->Draw("E1");
		
		TLine* lineZero = new TLine(-1*pull_edges[i], 0, pull_edges[i], 0);
		lineZero->SetLineStyle(kDashed);
		lineZero->SetLineColor(kBlack);
		lineZero->Draw();
		
		TLine* lineTwoSigUp = new TLine(-1*pull_edges[i], 3, pull_edges[i], 3);
		lineTwoSigUp->SetLineColor(kRed);
		lineTwoSigUp->Draw();
		TLine* lineTwoSigDown = new TLine(-1*pull_edges[i], -3, pull_edges[i], -3);
		lineTwoSigDown->SetLineColor(kRed);
		lineTwoSigDown->Draw();


		table[i][2] = ValError(mean.getVal(), mean.getError());
		table[i][3] = ValError(sigma.getVal(), sigma.getError());
		table[i][4] = ValError(mframe->chiSquare(), 0);

		system("mkdir -p "+toyDir);
		canv->SaveAs(toyDir+"/varPull_"+vars[i]+".pdf");
	}


	// fit bias, bias = fitVal - setVal
	for(int i=0; i<vars.size(); i++){
    	TPaveText text(0.25, 0.85, 0.45, 0.95, "NDC");
	 	text.SetFillColor(0);
	 	text.SetFillStyle(0);
	 	text.SetLineColor(0);
	 	text.SetLineWidth(0);
	 	text.SetBorderSize(1);
	 	text.SetTextSize(0.08);

		TH1D varHist("varBiasHisto_"+vars[i], "varBiasHisto_"+vars[i], 50, -1*bias_edges[i], bias_edges[i]);
		t->Draw(vars[i]+"-"+vars[i]+"_fitVal>>" + "varBiasHisto_"+vars[i]);

	 	RooRealVar observable(vars[i]+"_bias", vars[i]+"_bias", -1.*bias_edges[i], bias_edges[i]);
		observable.setBins(50);

		RooDataHist *datars = new RooDataHist("dataset", "dataset", observable, &varHist);
		
		// Detector response function
		RooRealVar mean("#it{#mu}", "m0", 0, -5, 5) ;
		RooRealVar sigma("#it{#sigma}", "sigma", 10, 0, 200);
		if(vars[i].Contains("nArgus") || vars[i].Contains("nExp")) sigma.setVal(100);
		//if(vars[i].Contains("tau")) sigma.setVal(0.1);

		RooGaussian gauss("gauss", "gauss", observable, mean, sigma);

		
	 	RooFitResult *result = gauss.fitTo(*datars, Save(true), NumCPU(10));


	 	RooPlot *mframe = observable.frame(Title(vars[i]+"_bias"));
	 	
	 	cout<<"Fit the data"<<endl;
	 	
	 	datars->plotOn(mframe, Name("data"));
	 	gauss.plotOn(mframe, Name("gauss"), LineColor(kBlue));
	 	datars->plotOn(mframe, Name("data"));
	 	gauss.paramOn(mframe, Layout(0.70, 0.95, 0.95));

		int result_status = result->status();
		cout << "result_status = "<< result_status<<endl;
		result->Print("v");
		
		
		RooHist *pull = mframe->pullHist();
		pull->GetYaxis()->SetTitle("Pull");
		RooPlot *framePull = observable.frame();
		framePull->addPlotable(pull, "P");
		
		double binWidth = 2*bias_edges[i]/50;
		mframe->SetTitle(vars[i]+"_bias");
		mframe->SetXTitle(vars[i]+"_bias");
		mframe->SetYTitle("Candidates / "+d2s(binWidth));
		mframe->GetXaxis()->SetTitleSize(0.2);
		mframe->GetYaxis()->SetTitleSize(0.2);
		
		framePull->SetTitle("");
		framePull->GetYaxis()->SetTitle("Pull");
		framePull->SetMinimum(-6.);
		framePull->SetMaximum(6.);
		framePull->SetMarkerStyle(2);
		//cout<<"-----------------1"<<endl;
		
		TCanvas *canv = new TCanvas("canv", "canv", 800, 600);
		canv->SetFillColor(10);
		canv->SetBorderMode(0);
		canv->cd();
		canv->cd(1);
		
		TPad *p1 = new TPad("pad1", "pad1", 0.02, 0.2, 0.98, 0.98, 0);
		TPad *p2 = new TPad("pad2", "pad2", 0.02, 0.04, 0.98, 0.2, 0);
		p1->Draw();
		p2->Draw();
		
		p1->cd();
		mframe->GetYaxis()->SetTitleOffset(1.00);
		mframe->GetYaxis()->SetLabelSize(0.05);
		mframe->GetYaxis()->SetTitleSize(0.05);
		mframe->GetYaxis()->SetNoExponent();
		mframe->GetXaxis()->SetTitleOffset(1.00);
		mframe->GetXaxis()->SetTitleSize(0.05);
		mframe->GetXaxis()->SetLabelSize(0.05);
		mframe->Draw("E1");
		text.AddText("#chi^{2}/ndf: "+d2s(mframe->chiSquare()));
		text.Draw("same");
		
		p2->cd();
		p2->SetTickx();
		framePull->GetYaxis()->SetTitleOffset(0.17);
		framePull->GetXaxis()->SetTitleSize(0.0);
		framePull->GetXaxis()->SetTickLength(0.09);
		framePull->GetYaxis()->SetTitleSize(0.2);
		framePull->GetXaxis()->SetLabelSize(0.0);
		framePull->GetYaxis()->SetLabelSize(0.2);
		framePull->GetYaxis()->SetNdivisions(5);
		framePull->Draw("E1");
		
		TLine* lineZero = new TLine(-1*bias_edges[i], 0, bias_edges[i], 0);
		lineZero->SetLineStyle(kDashed);
		lineZero->SetLineColor(kBlack);
		lineZero->Draw();
		
		TLine* lineTwoSigUp = new TLine(-1*bias_edges[i], 3, bias_edges[i], 3);
		lineTwoSigUp->SetLineColor(kRed);
		lineTwoSigUp->Draw();
		TLine* lineTwoSigDown = new TLine(-1*bias_edges[i], -3, bias_edges[i], -3);
		lineTwoSigDown->SetLineColor(kRed);
		lineTwoSigDown->Draw();


		system("mkdir -p "+toyDir);
		canv->SaveAs(toyDir+"/varBias_"+vars[i]+".pdf");
	}

	return table;
}

#include "/mnt/d/lhcb/B2XcXc/inc/tools.h"
#include "/mnt/d/lhcb/B2XcXc/inc/getPresel.h"

void tmva(){

	TChain chsig;
	chsig.Add("/mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bs2LcLc_Run2_truth_PIDcor_TrigSel_Presel.root/DecayTree");

	TChain chbkg;
	//chbkg.Add("/mnt/d/lhcb/B2XcXc/root/Bs/data/Data_Bs2LcLc_SB_Run2_TriggerSel_PreSel.root/DecayTree");
	chbkg.Add("/mnt/d/lhcb/B2XcXc/root/Bs/data/Data_Bs2LcLc_Run2_TriggerSel_PreSel_Lc60MeV.root/DecayTree");

	string bkgcutstr = "(B_DTFM_M_0>5450 && B_DTFM_M_0<6500)";
	//string sigcutstr = "rdmNumber <0.65";
	string sigcutstr = "rdmNumber <2";

    string sigWeight = "dataCondWeight*data_MC_w";
    string bkgWeight = "1";
   
    TTree* tsig = chsig.CopyTree(sigcutstr.c_str());
    TTree* tbkg = chbkg.CopyTree(bkgcutstr.c_str());
    cout<<"sig: "<<tsig->GetEntries()<<endl;
    cout<<"bkg: "<<tbkg->GetEntries()<<endl;

    TString outfileName( "TMVA.root" );
    TFile* outputFile = TFile::Open( outfileName, "RECREATE" );

    std::string factoryOptions( "!V:!Silent:Transformations=I;D;P;G,D" );

    TMVA::Factory *factory = new TMVA::Factory( "TMVAClassification", outputFile, factoryOptions );
    TMVA::DataLoader *dataloader=new TMVA::DataLoader("dataset");



   dataloader->AddVariable( "B_DIRA_OWNPV", "B_DIRA_OWNPV", "", 'D' );
   dataloader->AddVariable( "B_ENDVERTEX_CHI2", "B_ENDVERTEX_CHI2", "", 'D' );
   dataloader->AddVariable( "B_FDCHI2_OWNPV", "B_FDCHI2_OWNPV", "", 'D' );
   dataloader->AddVariable( "B_PT", "B_PT", "", 'D' );
   dataloader->AddVariable( "log(B_IPCHI2_OWNPV)", "log(B_IPCHI2_OWNPV)", "", 'D' );

   dataloader->AddVariable( "log(Xc_plus_IPCHI2_OWNPV)", "log(Xc_plus_IPCHI2_OWNPV)", "", 'D' );
   dataloader->AddVariable( "log(Xc_minus_IPCHI2_OWNPV)", "log(Xc_minus_IPCHI2_OWNPV)", "", 'D' );
   dataloader->AddVariable( "Xc_plus_PT",  "Xc_plus_PT",  "", 'D' );
   dataloader->AddVariable( "Xc_minus_PT", "Xc_minus_PT", "", 'D' );

   dataloader->AddVariable( "log(Kplus_Xc_minus_IPCHI2_OWNPV)", "log(Kplus_Xc_minus_IPCHI2_OWNPV)", "", 'D' );
   dataloader->AddVariable( "Kplus_Xc_minus_PT", "Kplus_Xc_minus_PT", "", 'D' );
   dataloader->AddVariable( "log(pbar_Xc_minus_IPCHI2_OWNPV)", "log(pbar_Xc_minus_IPCHI2_OWNPV)", "", 'D' );
   dataloader->AddVariable( "pbar_Xc_minus_PT", "pbar_Xc_minus_PT", "", 'D' );
   dataloader->AddVariable( "log(piminus_Xc_minus_IPCHI2_OWNPV)", "log(piminus_Xc_minus_IPCHI2_OWNPV)", "", 'D' );
   dataloader->AddVariable( "piminus_Xc_minus_PT", "piminus_Xc_minus_PT", "", 'D' );

   dataloader->AddVariable( "log(Kminus_Xc_plus_IPCHI2_OWNPV)", "log(Kminus_Xc_plus_IPCHI2_OWNPV)", "", 'D' );
   dataloader->AddVariable( "Kminus_Xc_plus_PT", "Kminus_Xc_plus_PT", "", 'D' );
   dataloader->AddVariable( "log(p_Xc_plus_IPCHI2_OWNPV)", "log(p_Xc_plus_IPCHI2_OWNPV)", "", 'D' );
   dataloader->AddVariable( "p_Xc_plus_PT", "p_Xc_plus_PT", "", 'D' );
   dataloader->AddVariable( "log(piplus_Xc_plus_IPCHI2_OWNPV)", "log(piplus_Xc_plus_IPCHI2_OWNPV)", "", 'D' );
   dataloader->AddVariable( "piplus_Xc_plus_PT", "piplus_Xc_plus_PT", "", 'D' );


   dataloader->AddVariable( "p_Xc_plus_MC15TuneV1_ProbNNp_corr", "p_Xc_plus_MC15TuneV1_ProbNNp_corr", "", 'D' );
   dataloader->AddVariable( "Kminus_Xc_plus_MC15TuneV1_ProbNNk_corr", "Kminus_Xc_plus_MC15TuneV1_ProbNNk_corr", "", 'D' );
   //dataloader->AddVariable( "piplus_Xc_plus_MC15TuneV1_ProbNNpi_corr", "piplus_Xc_plus_MC15TuneV1_ProbNNpi_corr", "", 'D' );
   dataloader->AddVariable( "piplus_Xc_plus_MC15TuneV1_ProbNNpi", "piplus_Xc_plus_MC15TuneV1_ProbNNpi", "", 'D' );
   dataloader->AddVariable( "pbar_Xc_minus_MC15TuneV1_ProbNNp_corr", "pbar_Xc_minus_MC15TuneV1_ProbNNp_corr", "", 'D' );
   dataloader->AddVariable( "Kplus_Xc_minus_MC15TuneV1_ProbNNk_corr", "Kplus_Xc_minus_MC15TuneV1_ProbNNk_corr", "", 'D' );
   //dataloader->AddVariable( "piminus_Xc_minus_MC15TuneV1_ProbNNpi_corr", "piminus_Xc_minus_MC15TuneV1_ProbNNpi_corr", "", 'D' );
   dataloader->AddVariable( "piminus_Xc_minus_MC15TuneV1_ProbNNpi", "piminus_Xc_minus_MC15TuneV1_ProbNNpi", "", 'D' );


    Double_t signalWeight     = 1.0;
    Double_t backgroundWeight = 1.0;

    dataloader->AddSignalTree    ( tsig,     signalWeight     );
    dataloader->AddBackgroundTree( tbkg, backgroundWeight );
    //dataloader->PrepareTrainingAndTestTree("", "", "nTrain_Signal=0:nTrain_Background=0:SplitMode=Random:NormMode=NumEvents:!V" );
    dataloader->PrepareTrainingAndTestTree("", "", "nTrain_Signal=6000:nTrain_Background=4500:SplitMode=Random:NormMode=NumEvents:!V" );

    dataloader->SetBackgroundWeightExpression(bkgWeight.c_str());
    dataloader->SetSignalWeightExpression(sigWeight.c_str());

    //factory->BookMethod( dataloader, TMVA::Types::kMLP, "MLP", "H:!V:NeuronType=tanh:VarTransform=N:NCycles=1000:HiddenLayers=N+15:TestRate=15:!UseRegulator"  );
    //factory->BookMethod( dataloader, TMVA::Types::kBDT, "BDT", "!H:!V:NTrees=1000:MinNodeSize=2.5%:MaxDepth=1:BoostType=AdaBoost:AdaBoostBeta=0.2:UseBaggedBoost:BaggedSampleFraction=0.2:SeparationType=GiniIndex:nCuts=20" );
    //factory->BookMethod( dataloader, TMVA::Types::kBDT, "BDTG", "!H:!V:NTrees=1000:MinNodeSize=2.5%:BoostType=Grad:Shrinkage=0.20:UseBaggedBoost:BaggedSampleFraction=0.2:nCuts=20:MaxDepth=1");
    factory->BookMethod( dataloader, TMVA::Types::kBDT, "BDT", "!H:!V:NTrees=1000:MinNodeSize=2.5%:MaxDepth=1:BoostType=AdaBoost:AdaBoostBeta=0.2:UseBaggedBoost:BaggedSampleFraction=0.2:SeparationType=GiniIndex:nCuts=20" );
    factory->BookMethod( dataloader, TMVA::Types::kBDT, "BDTG", "!H:!V:NTrees=1000:MinNodeSize=2.5%:BoostType=Grad:Shrinkage=0.20:UseBaggedBoost:BaggedSampleFraction=0.2:nCuts=20:MaxDepth=1");
    factory->TrainAllMethodsForClassification();
    factory->TestAllMethods();
    factory->EvaluateAllMethods();

    // Save the output
    outputFile->Close();

    std::cout << "==> Wrote root file: " << outputFile->GetName() << std::endl;
    std::cout << "==> TMVAClassification is done!" << std::endl;
    std::cout << std::endl;
    std::cout << "==> Too view the results, launch the GUI: \"root -l ../macros/TMVAGui.C\"" << std::endl;
    std::cout << std::endl;
    delete factory;
    delete dataloader;
    // Launch the GUI for the root macros
    if (!gROOT->IsBatch()) TMVA::TMVAGui( outfileName );
}


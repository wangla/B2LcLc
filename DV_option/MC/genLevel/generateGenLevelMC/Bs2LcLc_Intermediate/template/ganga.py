Year = 'YEAR'
DataOrMC = 'MC_OR_DATA'
MagType = 'MAG_TYPE'
Line="STRIPPING_LINE"
Account= "ACCOUNT"

myJobName = 'Bs2LcLc-{0}-{1}-{2}-{3}'.format(Line,Year,DataOrMC,MagType)

myApplication = GaudiExec()
myApplication.directory = "/afs/cern.ch/work/l/lai/DaVinciDev_v44r10p5"
myApplication.platform = 'x86_64-centos7-gcc62-opt'
myApplication.options   = [ 'option.py']

data = BKQuery(Input_dict[Year]).getDataset()

mySplitter = SplitByFiles( filesPerJob = 10, maxFiles = -1, ignoremissing = True, bulksubmit=False )
myBackend = Dirac()
j = Job (
        name         = myJobName,
        application  = myApplication,
        splitter     = mySplitter,
        outputfiles  = [ LocalFile('Tuple.root')],
        backend      = myBackend,
#        inputdata    = data[0:100],
#        inputdata    = data,
        do_auto_resubmit = True
        )
j.submit(keep_going=True, keep_on_fail=True)


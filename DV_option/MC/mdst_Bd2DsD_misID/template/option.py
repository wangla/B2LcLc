from os import environ
from GaudiKernel.SystemOfUnits import *
from Gaudi.Configuration import *
from Configurables import GaudiSequencer, CombineParticles, OfflineVertexFitter
from Configurables import DecayTreeTuple, EventTuple, TupleToolTrigger, TupleToolTISTOS,FilterDesktop
from Configurables import BackgroundCategory, TupleToolDecay, TupleToolVtxIsoln,TupleToolPid,EventCountHisto,TupleToolRecoStats,TupleToolKinematic, SubstitutePID
from Configurables import TupleToolVeto, TupleToolTagging, TupleToolL0Data, TupleToolL0Calo
from Configurables import TupleToolMCTruth, TupleToolMCBackgroundInfo
from Configurables import MCTupleToolAngles, MCTupleToolHierarchy, MCTupleToolKinematic, MCTupleToolPrimaries, MCTupleToolReconstructed, MCTupleToolInteractions, PrintMCTree, PrintMCDecayTreeTool  
from GaudiConfUtils.ConfigurableGenerators import  CombineParticles
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand, AutomaticData
from GaudiConfUtils.ConfigurableGenerators import DaVinci__N3BodyDecays

year = 'YEAR'

doMC = 1
doLocalTest = 0

stream = 'CharmCompleteEvent'
if doMC: stream = 'AllStreams'
rootInTES = '/Event/%s'%stream

from Configurables import DecayTreeTuple
from Configurables import TupleToolTrigger, TupleToolDecay, TupleToolTISTOS


# Trigger lines
myTriggerList = [
             #L0
              'L0HadronDecision'
             ,'L0ElectronDecision'
             ,'L0ElectronHiDecision'
             ,'L0MuonDecision'
             ,'L0DiMuonDecision'
             ,'L0MuonHighDecision'
             #Hlt1
             ,'Hlt1TrackMVADecision'
             ,'Hlt1TwoTrackMVADecision'
             ,'Hlt1GlobalDecision'
             ,'Hlt1TrackAllL0Decision'
             #Hlt2
             ,'Hlt2Topo2BodyBBDTDecision'
             ,'Hlt2Topo3BodyBBDTDecision'
             ,'Hlt2Topo4BodyBBDTDecision'
             ,'Hlt2BHadB02PpPpPmPmDecision'
             ,'Hlt2Topo2BodyDecision'
             ,'Hlt2Topo3BodyDecision'
             ,'Hlt2Topo4BodyDecision'
             ,'Hlt2GlobalDecision'
]



############
from DecayTreeTuple.Configuration import *
from Configurables import LoKi__Hybrid__TupleTool

#######--------------------B + -> Lcp
B2DsDsTuple = DecayTreeTuple("B2DsDsTuple")
#B2DsDsTuple.Inputs = [rootInTES+"/Phys/CC2DDLine/Particles"]
B2DsDsTuple.Inputs = ["Phys/CC2DDLine/Particles"]
#B2DsDsTuple.Decay = "[B0 -> ^(Lambda_c+ -> ^p+ ^K- ^pi+) ^p~-]CC"
#B2DsDsTuple.addBranches({
#    "B"            :    "[B0 -> (Lambda_c+ -> p+ K- pi+) p~-]CC",
#    "Lc"           :    "[B0 -> ^(Lambda_c+ -> p+ K- pi+) p~-]CC",
#    "p_Lc"         :    "[B0 -> (Lambda_c+ -> ^p+ K- pi+) p~-]CC",
#    "Kminus_Lc"    :    "[B0 -> (Lambda_c+ -> p+ ^K- pi+) p~-]CC",
#    "piplus_Lc"    :    "[B0 -> (Lambda_c+ -> p+ K- ^pi+) p~-]CC",
#    "pbar"         :    "[B0 -> (Lambda_c+ -> p+ K- pi+) ^p~-]CC",
#})

B2DsDsTuple.Decay = "[psi(3770) -> ^(D_s- -> ^K+ ^K- ^pi-) ^(D_s+ -> ^K- ^K+ ^pi+)]CC"
B2DsDsTuple.addBranches({
    "B"                 :    "[psi(3770) ->  (D_s- ->  K+  K-  pi-)  (D_s+ ->  K-  K+  pi+)]CC",
    "Dsminus"           :    "[psi(3770) -> ^(D_s- ->  K+  K-  pi-)  (D_s+ ->  K-  K+  pi+)]CC",
    "Kplus_Dsminus"     :    "[psi(3770) ->  (D_s- -> ^K+  K-  pi-)  (D_s+ ->  K-  K+  pi+)]CC",
    "Kminus_Dsminus"    :    "[psi(3770) ->  (D_s- ->  K+ ^K-  pi-)  (D_s+ ->  K-  K+  pi+)]CC",
    "piminus_Dsminus"   :    "[psi(3770) ->  (D_s- ->  K+  K- ^pi-)  (D_s+ ->  K-  K+  pi+)]CC",
    "Dsplus"            :    "[psi(3770) ->  (D_s- ->  K+  K-  pi-) ^(D_s+ ->  K-  K+  pi+)]CC",
    "Kminus_Dsplus"     :    "[psi(3770) ->  (D_s- ->  K+  K-  pi-)  (D_s+ -> ^K-  K+  pi+)]CC",
    "Kplus_Dsplus"      :    "[psi(3770) ->  (D_s- ->  K+  K-  pi-)  (D_s+ ->  K- ^K+  pi+)]CC",
    "piplus_Dsplus"     :    "[psi(3770) ->  (D_s- ->  K+  K-  pi-)  (D_s+ ->  K-  K+ ^pi+)]CC",
})

## Reference point
B2DsDsTuple.ToolList += [ "TupleToolKinematic" ]
B2DsDsTuple.addTool(TupleToolKinematic, name = "TupleToolKinematic")
B2DsDsTuple.TupleToolKinematic.Verbose = True

###################################
# Lc Constraints:
###################################
### B Lambda_c~- Lambda_c+ (mass + PV)constraint Tool
B2DsDsTuple.addTool(TupleToolDecay, name = "DTFM_PVC")
B2DsDsTuple_DTFM_PVC = B2DsDsTuple.B.addTupleTool('TupleToolDecayTreeFitter/DTFM_PVC')
B2DsDsTuple_DTFM_PVC.UpdateDaughters = True
B2DsDsTuple_DTFM_PVC.Verbose = True
B2DsDsTuple_DTFM_PVC.constrainToOriginVertex = True
B2DsDsTuple_DTFM_PVC.daughtersToConstrain += [ 'D_s+']
B2DsDsTuple_DTFM_PVC.daughtersToConstrain += [ 'D_s-']

## B Lambda_c+ (mass)constraint Tool
B2DsDsTuple.addTool(TupleToolDecay, name = "DTFM")
B2DsDsTuple_DTFM = B2DsDsTuple.B.addTupleTool('TupleToolDecayTreeFitter/DTFM')
B2DsDsTuple_DTFM.UpdateDaughters = True
B2DsDsTuple_DTFM.Verbose = True
B2DsDsTuple_DTFM.constrainToOriginVertex = False
B2DsDsTuple_DTFM.daughtersToConstrain += [ 'D_s+']
B2DsDsTuple_DTFM.daughtersToConstrain += [ 'D_s-']

## B (PV)constraint Tool
B2DsDsTuple.addTool(TupleToolDecay, name = "PVC")
B2DsDsTuple_PVC = B2DsDsTuple.B.addTupleTool('TupleToolDecayTreeFitter/PVC')
B2DsDsTuple_PVC.UpdateDaughters = True
B2DsDsTuple_PVC.Verbose = True
B2DsDsTuple_PVC.constrainToOriginVertex = True
#

###########################
##DTFDict Bs2DsDsbar
###########################
from Configurables import LoKi__Hybrid__DictOfFunctors
from Configurables import LoKi__Hybrid__Dict2Tuple
from Configurables import LoKi__Hybrid__DTFDict as DTFDict
DictTuple = B2DsDsTuple.B.addTupleTool(LoKi__Hybrid__Dict2Tuple, "DTFTuple")
DictTuple.addTool(DTFDict,"DTF")
DictTuple.Source = "LoKi::Hybrid::DTFDict/DTF"
DictTuple.NumVar = 46     # reserve a suitable size for the dictionaire
DictTuple.DTF.constrainToOriginVertex = True
DictTuple.DTF.daughtersToConstrain = ['D_s+', 'D_s-']
DictTuple.DTF.addTool(LoKi__Hybrid__DictOfFunctors,"dict")
DictTuple.DTF.Source = "LoKi::Hybrid::DictOfFunctors/dict"
DictTuple.DTF.dict.Variables = {
    "DTFDict_B_PT"      : "PT",
    "DTFDict_B_M"       : "M",
    'DTFDict_B_PX'                :   "PX",
    'DTFDict_B_PY'                :   "PY",
    'DTFDict_B_PZ'                :   "PZ",
    'DTFDict_B_PT'                :   "PT",
    'DTFDict_B_E'                 :   "E" ,
    "DTFDict_B_MINIPCHI2"         :   "MIPCHI2DV()" ,
    "DTFDict_B_DIRA_OWNPV"        :   "BPVDIRA" ,
    "DTFDict_B_ENDVERTEX_CHI2"    :   "VFASPF(VCHI2)" ,
    "DTFDict_B_ENDVERTEX_NDOF"    :   "VFASPF(VDOF)" ,
    "DTFDict_B_FDCHI2_OWNPV"      :   "BPVVDCHI2" ,
#    # Lc
#    "DTFDict_Lc_MM"              :   "CHILD(MM              ,ABSID=='Lambda_c+')",
#    "DTFDict_Lc_M"               :   "CHILD(M               ,ABSID=='Lambda_c+')",
#    "DTFDict_Lc_PX"              :   "CHILD(PX              ,ABSID=='Lambda_c+')",
#    "DTFDict_Lc_PY"              :   "CHILD(PY              ,ABSID=='Lambda_c+')",
#    "DTFDict_Lc_PZ"              :   "CHILD(PZ              ,ABSID=='Lambda_c+')",
#    "DTFDict_Lc_PT"              :   "CHILD(PT              ,ABSID=='Lambda_c+')",
#    "DTFDict_Lc_E"               :   "CHILD(E               ,ABSID=='Lambda_c+')",
#    "DTFDict_Lc_MINIPCHI2"       :   "CHILD(MIPCHI2DV()     ,ABSID=='Lambda_c+')",
#    "DTFDict_Lc_DIRA_OWNPV"      :   "CHILD(BPVDIRA         ,ABSID=='Lambda_c+')",
#    "DTFDict_Lc_ENDVERTEX_CHI2"  :   "CHILD(VFASPF(VCHI2)   ,ABSID=='Lambda_c+')",
#    "DTFDict_Lc_ENDVERTEX_NDOF"  :   "CHILD(VFASPF(VDOF)    ,ABSID=='Lambda_c+')",
#    "DTFDict_Lc_FDCHI2_OWNPV"    :   "CHILD(BPVVDCHI2       ,ABSID=='Lambda_c+')",
    # D_s+
    "DTFDict_Dplus_MM"              :   "CHILD(MM              ,ABSID=='D_s+')",
    "DTFDict_Dplus_M"               :   "CHILD(M               ,ABSID=='D_s+')",
    "DTFDict_Dplus_PX"              :   "CHILD(PX              ,ABSID=='D_s+')",
    "DTFDict_Dplus_PY"              :   "CHILD(PY              ,ABSID=='D_s+')",
    "DTFDict_Dplus_PZ"              :   "CHILD(PZ              ,ABSID=='D_s+')",
    "DTFDict_Dplus_PT"              :   "CHILD(PT              ,ABSID=='D_s+')",
    "DTFDict_Dplus_E"               :   "CHILD(E               ,ABSID=='D_s+')",
    "DTFDict_Dplus_MINIPCHI2"       :   "CHILD(MIPCHI2DV()     ,ABSID=='D_s+')",
    "DTFDict_Dplus_DIRA_OWNPV"      :   "CHILD(BPVDIRA         ,ABSID=='D_s+')",
    "DTFDict_Dplus_ENDVERTEX_CHI2"  :   "CHILD(VFASPF(VCHI2)   ,ABSID=='D_s+')",
    "DTFDict_Dplus_ENDVERTEX_NDOF"  :   "CHILD(VFASPF(VDOF)    ,ABSID=='D_s+')",
    "DTFDict_Dplus_FDCHI2_OWNPV"    :   "CHILD(BPVVDCHI2       ,ABSID=='D_s+')",
    # Ds-
    "DTFDict_Dsminus_MM"              :   "CHILD(MM              ,ABSID=='D_s-')",
    "DTFDict_Dsminus_M"               :   "CHILD(M               ,ABSID=='D_s-')",
    "DTFDict_Dsminus_PX"              :   "CHILD(PX              ,ABSID=='D_s-')",
    "DTFDict_Dsminus_PY"              :   "CHILD(PY              ,ABSID=='D_s-')",
    "DTFDict_Dsminus_PZ"              :   "CHILD(PZ              ,ABSID=='D_s-')",
    "DTFDict_Dsminus_PT"              :   "CHILD(PT              ,ABSID=='D_s-')",
    "DTFDict_Dsminus_E"               :   "CHILD(E               ,ABSID=='D_s-')",
    "DTFDict_Dsminus_MINIPCHI2"       :   "CHILD(MIPCHI2DV()     ,ABSID=='D_s-')",
    "DTFDict_Dsminus_DIRA_OWNPV"      :   "CHILD(BPVDIRA         ,ABSID=='D_s-')",
    "DTFDict_Dsminus_ENDVERTEX_CHI2"  :   "CHILD(VFASPF(VCHI2)   ,ABSID=='D_s-')",
    "DTFDict_Dsminus_ENDVERTEX_NDOF"  :   "CHILD(VFASPF(VDOF)    ,ABSID=='D_s-')",
    "DTFDict_Dsminus_FDCHI2_OWNPV"    :   "CHILD(BPVVDCHI2       ,ABSID=='D_s-')",

   }

#

B2DsDsTuple.ToolList += [ "TupleToolAngles"
                    , "TupleToolEventInfo"
                    , "TupleToolGeometry"
                    , "TupleToolKinematic"
                    , "TupleToolPid"
                    , "TupleToolPrimaries"
                    , "TupleToolPropertime"
                    , "TupleToolTrackInfo"
                    # , "TupleToolCovariances"
                    , "TupleToolBremInfo"
                    , "TupleToolMuonPid"
                    , "TupleToolRecoStats"
                    , "TupleToolDira"
                    , "TupleToolTrackPosition"
                    , "TupleToolL0Data"
                    , "TupleToolMCTruth"
                    , "TupleToolMCBackgroundInfo"
                    ]


B2DsDsTuple.ToolList+= [ "TupleToolTISTOS" ]
B2DsDsTuple.addTool(TupleToolTISTOS, name = "TupleToolTISTOS")
B2DsDsTuple.TupleToolTISTOS.Verbose = True
B2DsDsTuple.TupleToolTISTOS.VerboseL0 = True
B2DsDsTuple.TupleToolTISTOS.VerboseHlt1 = True
B2DsDsTuple.TupleToolTISTOS.VerboseHlt2 = True
B2DsDsTuple.TupleToolTISTOS.TriggerList = myTriggerList #trigger_list.trigger_list() 


from Configurables import TupleToolRecoStats
B2DsDsTuple.addTool(TupleToolRecoStats, name = "TupleToolRecoStats")
B2DsDsTuple.TupleToolRecoStats.Verbose = True


# Add eta                                                            
LoKi_All = B2DsDsTuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_All")
LoKi_All.Variables = {
    'ETA' : 'ETA',
    'MinIPCHI2' : 'MIPCHI2DV(PRIMARY)'
    }


## MCTruth info
MCTruth = TupleToolMCTruth()
MCTruth.ToolList = [
    "MCTupleToolAngles"
        ,"MCTupleToolHierarchy"
            ,"MCTupleToolKinematic" ]


if doMC:
    B2DsDsTuple.addTool(MCTruth, name = "TupleToolMCTruth")




from Configurables import DaVinci, CheckPV, GaudiSequencer, LoKi__HDRFilter

#from Configurables import CondDB
#CondDB ( LatestGlobalTagByDataType = year )

#from Configurables import EventNodeKiller
#eventNodeKiller = EventNodeKiller('Stripkiller')
#eventNodeKiller.Nodes = [ '/Event/AllStreams', '/Event/Strip' ]



#######################################################################
from Configurables import DaVinci
#DaVinci().CondDBtag = 'sim-20161124-2-vc-md100'
#DaVinci().DDDBtag = 'dddb-20150724'
DaVinci().EvtMax = -1                      # Number of events
DaVinci().PrintFreq = 5000
DaVinci().SkipEvents = 0                       # Events to skip
DaVinci().TupleFile = "Tuple.root"             # NB2DsDsTuple
DaVinci().Simulation = doMC
DaVinci().Lumi = not DaVinci().Simulation


#DaVinci().appendToMainSequence( [ eventNodeKiller ] )
#DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().UserAlgorithms = [ B2DsDsTuple]
DaVinci().DataType = year


if doMC:
    local_dict = {
        #'2016' : ["/eos/lhcb/grid/prod/lhcb/MC/2016/ALLSTREAMS.DST/00173956/0000/00173956_00000205_7.AllStreams.dst"],
        '2016' : ["/eos/lhcb/grid/prod/lhcb/MC/2016/ALLSTREAMS.MDST/00160522/0000/00160522_00000392_7.AllStreams.mdst"],
    }
    DaVinci().InputType = 'MDST'
    DaVinci().RootInTES = rootInTES
else:
    local_dict = {
    }
    DaVinci().InputType = 'DST'

###------------------Local Test
if doLocalTest:
    DaVinci().Input = local_dict[year]



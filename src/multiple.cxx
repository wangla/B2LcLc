#include "/mnt/d/lhcb/B2XcXc/inc/tools.h"
#include "/mnt/d/lhcb/B2XcXc/inc/getPresel.h"

//void multiple(int i)
void multiple()
{
	//int i=0;

	list<ElementForUniquing> listElements;
	vector<TString> fileNames;
	fileNames.push_back("/mnt/d/lhcb/B2XcXc/root/Bd/data/Data_Bd2DsD_Run2_TriggerSel_PreSel.root");
	fileNames.push_back("/mnt/d/lhcb/B2XcXc/root/Bs/data/Data_Bs2DsDs_Run2_TriggerSel_PreSel.root");
	fileNames.push_back("/mnt/d/lhcb/B2XcXc/root/Bs/data/Data_Bs2LcLc_SB_Run2_TriggerSel_PreSel.root");

	//fileNames.push_back("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2DsD_Run2_truth_PIDcor_TrigSel_Presel.root");
	//fileNames.push_back("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2LcLc_Run2_truth_PIDcor_TrigSel_Presel.root");
	//fileNames.push_back("/mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bs2DsDs_Run2_truth_PIDcor_TrigSel_Presel.root");
	//fileNames.push_back("/mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bs2LcLc_Run2_truth_PIDcor_TrigSel_Presel.root");

	//fileNames.push_back("/mnt/d/lhcb/B2XcXc/root/Bd/MC/truthed/MC_Bd2LcLc_2015_Down_truth_PIDcor.root");
	//fileNames.push_back("/mnt/d/lhcb/B2XcXc/root/Bd/MC/truthed/MC_Bd2LcLc_2016_Down_truth_PIDcor.root");
	//fileNames.push_back("/mnt/d/lhcb/B2XcXc/root/Bd/MC/truthed/MC_Bd2LcLc_2017_Down_truth_PIDcor.root");
	//fileNames.push_back("/mnt/d/lhcb/B2XcXc/root/Bd/MC/truthed/MC_Bd2LcLc_2018_Down_truth_PIDcor.root");
	//fileNames.push_back("/mnt/d/lhcb/B2XcXc/root/Bd/MC/truthed/MC_Bd2LcLc_2015_Up_truth_PIDcor.root");
	//fileNames.push_back("/mnt/d/lhcb/B2XcXc/root/Bd/MC/truthed/MC_Bd2LcLc_2016_Up_truth_PIDcor.root");
	//fileNames.push_back("/mnt/d/lhcb/B2XcXc/root/Bd/MC/truthed/MC_Bd2LcLc_2017_Up_truth_PIDcor.root");
	//fileNames.push_back("/mnt/d/lhcb/B2XcXc/root/Bd/MC/truthed/MC_Bd2LcLc_2018_Up_truth_PIDcor.root");
	//fileNames.push_back("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2LcLc_Run2_truth_PIDcor.root");

	list<ElementForUniquing>::iterator listIt;


	for(int i=0; i<fileNames.size(); i++){
		cout << "Count multiple candidates for " + fileNames[i] << endl;

		listElements.clear();
		fillListElements(listElements, fileNames[i], getFinalSel());
		//fillListElements(listElements, fileNames[i], "1");
		double nDataBd2DsDU = (countUniqueEvents(listElements)).val;
		double nDataBd2DsD = 1.*listElements.size();

		cout << nDataBd2DsDU << ", " << nDataBd2DsD << endl;
		cout<<100*(nDataBd2DsD-nDataBd2DsDU)/nDataBd2DsDU << "%"<<endl;

		int nCountRemove = 0;
		for(listIt = listElements.begin(); listIt != listElements.end(); listIt++){
			if(listIt->toRemove == true){
				nCountRemove++;
			}
		}
		//cout << "nCountRemove: " << nCountRemove << endl;

		//tagMultiple(fileNames[i], listElements);
	}

	gROOT->ProcessLine(".q");
}

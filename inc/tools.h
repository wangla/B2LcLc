#ifndef _TOOLS_H_
#define _TOOLS_H_

class StringNum
{
   public:

      StringNum(int _num, TString _str)
         :num(_num), str(_str)
      {}

      StringNum(TString _str, int _num)
         :num(_num), str(_str)
      {}

      int num;
      TString str;
};

class NumNum{
	public:
	
		NumNum(int _one, int _two)
			:one(_one), two(_two)
		{}
		
		int one;
		int two;
};

class DoubleDouble{
	public:
	
		DoubleDouble(double _one, double _two)
			:one(_one), two(_two)
		{}
		
		double one;
		double two;
};

class EventTag{
   public:

   unsigned int runNumber;
   unsigned long long int eventNumber; 
   short polarity;

   EventTag(unsigned int _runNumber,unsigned long long int  _eventNumber, short _polarity)
      :runNumber(_runNumber), eventNumber(_eventNumber), polarity(_polarity)
      {}

   EventTag()
      :runNumber(0), eventNumber(0), polarity(0)
      {}

   bool operator==(EventTag const& other) const;
};

bool EventTag::operator==(EventTag const& other) const{
   return this->runNumber == other.runNumber && this->polarity == other.polarity && this->eventNumber == other.eventNumber;
}

class ElementForUniquing{
	public: 

	EventTag eventTag; 
	double varClassification;
	int rank;
	int numInTree;
	bool passCut;
	bool toRemove;
	double weight;
	double binVar;


	ElementForUniquing(double _varClassification, int _rank = -1, int _numInTree = 0, bool _passCut = true, bool _toRemove = false, double _weight = 1., double _binVar = 0.)
		:eventTag(0,0,0), varClassification(_varClassification), rank(_rank), numInTree(_numInTree), passCut(_passCut), toRemove(_toRemove), weight(_weight), binVar(_binVar)
		{}

	ElementForUniquing()
		:eventTag(0,0,0), varClassification(0), rank(-1), numInTree(0), passCut(true), toRemove(false), weight(1.), binVar(0.)
		{}
};

void reduceTreeByFactor(double factor, TString nameFile, TString nameReducedFile, TString nameTree = "DecayTree");
void projectEfficiencyPlots(vector<TH3*> passHists, vector<TH3*> totHists, TString plotNameStart, TString yLabel = "",
      vector<TString> xLabel = vector<TString>(3, ""), double min = -20., double max = -20., vector<TString> plotOptions = vector<TString>(0),
      vector<TString> legEntries = vector<TString>(0), vector<double> legPos = vector<double>(4,0), vector<int> colourTab = vector<int>(0), double xThreshold = 0.);

void projectEfficiencyPlots(vector<TH2*> passHists, vector<TH2*> totHists, TString plotNameStart, TString yLabel = "",
      vector<TString> xLabel = vector<TString>(2, ""), double min = -20., double max = -20., vector<TString> plotOptions = vector<TString>(0),
      vector<TString> legEntries = vector<TString>(0), vector<double> legPos = vector<double>(4,0), vector<int> colourTab = vector<int>(0), double xThreshold = 0.);

void projectEfficiencyPlots(vector<TString> fileNames, vector<TString> totHistNames, vector<TString> passHistNames, TString plotNameStart, TString yLabel = "",
      vector<TString> xLabel = vector<TString>(3, ""), double min = -20., double max = -20., vector<TString> plotOptions = vector<TString>(0),
      vector<TString> legEntries = vector<TString>(0), vector<double> legPos = vector<double>(4,0), vector<int> colourTab = vector<int>(0), double xThreshold = 0.);

void projectEfficiencyPlots2D(vector<TString> fileNames, vector<TString> totHistNames, vector<TString> passHistNames, TString plotNameStart, TString yLabel = "",
      vector<TString> xLabel = vector<TString>(2, ""), double min = -20., double max = -20., vector<TString> plotOptions = vector<TString>(0),
      vector<TString> legEntries = vector<TString>(0), vector<double> legPos = vector<double>(4,0), vector<int> colourTab = vector<int>(0), double xThreshold = 0.);

void copyTreeWithNewVar(TTree*& tNew, TTree* tOld, TString cut = "", TString formula = "", TString newVarName = "");
void copyTreeWithNewVars(TTree*& tNew, TTree* tOld, TString cut = "", vector<TString> formulas = vector<TString>(0), vector<TString> newVarNames = vector<TString>(0));
void copyTreeWithNewVars(TString fileNewName, TString fileOldName, TString cut, vector<TString> formulas, vector<TString> newVarNames, vector<TString> varsToSwitchOn = vector<TString>(0), TString tOldName = "DecayTree");
void copyTreeWithNewVars(TString fileNewName, TString fileOldName, TString cut, TString formula, TString newVarName, vector<TString> varsToSwitchOn = vector<TString>(0), TString tOldName = "DecayTree");

void fillListElements(list<ElementForUniquing>& listElements, TString fileName, TString cut = "", TString weightName = "",
      TString varClassification = "", short fixPolarity = -10, TString treeName = "DecayTree");

bool instring(TString in, TString str)
{
	return str.Contains(in);
}

double s2d(TString TStr)
{
	double ret;
	string str = TStr.Data();
	istringstream is(str);
	is >> ret;
	return ret;
}

TString d2s(double d)
{
	//string ret;
	ostringstream os;
	os<<d;
	return os.str();
}

TString i2s(int i)
{
	//string ret;
	ostringstream os;
	os<<i;
	return os.str();
}
TString dbl2str(double nbr, int nfixed = 0){
	stringstream ss;
	if(nfixed>=1) ss<<fixed<<setprecision(nfixed)<<nbr;
	else ss<<nbr;
	return ss.str();
}

double getPE(double hypothesisMass, double PX, double PY, double PZ){
	return sqrt(pow(PX, 2)+pow(PY, 2)+pow(PZ, 2)+pow(hypothesisMass, 2));
}
double getMass(double PE, double PX, double PY, double PZ){
	return sqrt(pow(PE, 2)-pow(PX, 2)-pow(PY, 2)-pow(PZ, 2));
}
double angle(double px1, double py1, double pz1, double px2, double py2, double pz2){
	double cosa = (px1*px2+py1*py2+pz1*pz2)/sqrt((pow(px1, 2)+pow(py1, 2)+pow(pz1, 2))*(pow(px2, 2)+pow(py2, 2)+pow(pz2, 2)));
	return acos(cosa);
}
vector <double> get_gen_eff(double num_up, double num_down, double eff_up, double eff_err_up, double eff_down, double eff_err_down){
  double denom_up	= num_up	/ eff_up;
  double denom_down = num_down / eff_down;

  double Sum_num	= num_up	+ num_down;
  double Sum_denom = denom_up + denom_down;																																																		std::vector<double> weighted_eff_err;

  double weighted_eff	 = eff_up*(denom_up/Sum_denom) + eff_down*(denom_down/Sum_denom) ;
  double weighted_err = eff_err_up*(denom_up/Sum_denom)*TMath::Sqrt(num_up/Sum_num) + eff_err_down*(denom_down/Sum_denom)*TMath::Sqrt(num_down/Sum_num);

  weighted_eff_err.push_back(weighted_eff);
  weighted_eff_err.push_back(weighted_err);

  return weighted_eff_err;
}


vector <double> get_gen_eff(vector <double> num_up, vector <double> num_down, vector <double> eff_up, vector <double> eff_err_up, vector <double> eff_down, vector <double> eff_err_down){
	if(num_up.size() !=  num_down.size() || num_down.size() !=  eff_up.size() || eff_up.size() !=  eff_err_up.size() || eff_err_up.size() !=  eff_down.size() || eff_down.size() !=  eff_err_down.size()){
		cerr << "ERROR in get_gen_eff: different vector <double> size!" << endl;
		return vector <double>();
	}

	vector<double> weighted_eff_err_up{1, 0};
	vector<double> weighted_eff_err_down{1, 0};
	int allEntriesUp = 0;
	int allEntriesDn = 0;

	for(int i=0; i<num_up.size(); i++){
		weighted_eff_err_up = get_gen_eff(num_up[i], allEntriesUp, eff_up[i], eff_err_up[i], weighted_eff_err_up[0], weighted_eff_err_up[1]);
		weighted_eff_err_down = get_gen_eff(num_down[i], allEntriesDn, eff_down[i], eff_err_down[i], weighted_eff_err_down[0], weighted_eff_err_down[1]);
		allEntriesUp += num_up[i];
		allEntriesDn += num_down[i];
	}

	vector<double> weighted_eff_err = get_gen_eff(allEntriesUp, allEntriesDn, weighted_eff_err_up[0], weighted_eff_err_up[1], weighted_eff_err_down[0], weighted_eff_err_down[1]);

	return weighted_eff_err;
}

TString getTime()
{
	time_t timep;
	time (&timep);
	char tmp[64];
	strftime(tmp, sizeof(tmp), "%Y-%m-%d %H:%M:%S", localtime(&timep));
	return tmp;
}

TH1D* getHistFromTMVA(TString _filename, TString method, TString new_name="1"){
  TFile* _file = new TFile(_filename);
  char command_line[500];

  sprintf(command_line, "dataset->cd()");
  gROOT->ProcessLine(command_line);

  // go to general Method directory
  if(method.Contains("BDT"))
	sprintf(command_line, "Method_BDT->cd()");
  else if(method.Contains("Likelihood"))
	sprintf(command_line, "Method_Likelihood->cd()");
  else if(method.Contains("Boost"))//PDEFoamBoost, BoostedFisher
	sprintf(command_line, "Method_Boost->cd()");
  else if(method.Contains("KNN"))
	sprintf(command_line, "Method_KNN->cd()");
  else if(method.Contains("DNN_CPU"))
	sprintf(command_line, "Method_DNN_CPU->cd()");
  else if(method.Contains("HMatrix"))
	sprintf(command_line, "Method_HMatrix->cd()");
  else if(method.Contains("LD"))
	sprintf(command_line, "Method_LD->cd()");
  else if(method.Contains("Fisher"))
	sprintf(command_line, "Method_Fisher->cd()");
  else if(method.Contains("FDA"))
	sprintf(command_line, "Method_FDA->cd()");
  else if(method.Contains("MLP"))
	sprintf(command_line, "Method_MLP->cd()");
  else if(method.Contains("CFMlpANN"))
	sprintf(command_line, "Method_CFMlpANN->cd()");
  else if(method.Contains("TMlpANN"))
	sprintf(command_line, "Method_TMlpANN->cd()");
  else if(method.Contains("SVM"))
	sprintf(command_line, "Method_SVM->cd()");
  else
	cout<<" ==> Cannot handle this method, please check !!"<<endl;

  //if(method.Contains("DNN_CPU") != TString::npos)

  gROOT->ProcessLine(command_line);

  // go to method directory
  sprintf(command_line, "%s->cd()", method.Data());
  gROOT->ProcessLine(command_line);

  // get ROC curve
  sprintf(command_line, "MVA_%s_trainingRejBvsS", method.Data());
  TH1D* hist_tmva = (TH1D*) gROOT->FindObject(command_line);
  sprintf(command_line, "MVA_%s_trainingRejBvsS_%s", method.Data(), new_name.Data());

  // clone and rename
  hist_tmva->SetName(command_line);

  // return and try to clean memory
  return hist_tmva;
  delete _file;
}

//active variables to readable state, and close other varibles
bool setBranchStatusTTF(TTree* t, TString cuts)
{
  TObjArray* array = t->GetListOfBranches();
  TString name;
  bool ret(false);
  for(int i(0); i<array->GetEntries(); ++i){
	 name = ((*array)[i])->GetName();
	 if(cuts.Contains(name)){
		t->SetBranchStatus(name, 1);
		ret = true;
	 }
  }
  return ret;
}
bool setBranchStatusTTF(TChain& ch, TString cuts)
{
  TObjArray* array = ch.GetListOfBranches();
  TString name;
  bool ret(false);
  for(int i(0); i<array->GetEntries(); ++i){
	 name = ((*array)[i])->GetName();
	 if(cuts.Contains(name)){
		ch.SetBranchStatus(name, 1);
		ret = true;
	 }
  }
  return ret;
}
TString getHex(int num){
	printf("  0x%x\n", num);
	return Form("  0x%x\n", num);
}
////生成泊松branch
////输入的文件添加泊松branch之后，原文件被覆盖
void addPoissonBranch(TString fileName, unsigned int nTimes, bool useEvtNumber){
	TFile f(fileName);
	TTree* t = (TTree*)f.Get("DecayTree");

	if(!t){
	  cerr<<"ERROR: in addPoissonTable, no tree found. Abort.";
	  return;
	}

	cout<<endl;
	cout<<endl<<"===================================================="<<endl;
	cout<<"ERROR from TTReeFormula don't matter from here"<<endl;
	t->SetBranchStatus("nPoissonWeights", 0);
	t->SetBranchStatus("poissonWeight", 0);
	cout<<"===================================================="<<endl<<endl;

	unsigned long long int eventNumber;
	t->SetBranchAddress("eventNumber", &eventNumber);

	TString fileNameInter(fileName);
	fileNameInter.Insert( fileNameInter.Length()-5,"_tmp");
	TFile fNew(fileNameInter, "RECREATE");
	TTree* tNew = t->CloneTree(0);

	//prepare new branch

	unsigned char poissonWeight[nTimes];

	tNew->Branch("nPoissonWeights", &nTimes, "nPoissonWeights/i");
	tNew->Branch("poissonWeight", &poissonWeight, "poissonWeight[nPoissonWeights]/b");


	//TRandom3 rand;
	TRandom rand;

	//looping over the tree and filling it

	cout<<"looping over the tree to add poisson weights... "<<endl;

	for(int i(0); i<t->GetEntries(); ++i){
	  if(i % (t->GetEntries()/10) == 0) cout<<100*i/(t->GetEntries())<<"\%	"<<flush;

	  t->GetEntry(i);

	  if(useEvtNumber) rand.SetSeed(eventNumber);
	  for(unsigned j(0); j<nTimes; ++j) poissonWeight[j] = rand.Poisson(1.);

	  tNew->Fill();
	}

	cout<<"done!"<<endl;

	fNew.cd();
	tNew->Write("", TObject::kOverwrite);

	//close all

	f.Close();
	fNew.Close();

	system( "mv "+fileNameInter+" "+fileName );
}
// all users - please change the name of this file to lhcbStyle.C
// // Commits to lhcbdocs svn of .C files are not allowed
void lhcbStyle(double fontsize){
	// define names for colours
//	Int_t black  = 1;
//	Int_t red	= 2;
//	Int_t green  = 3;
//	Int_t blue	= 4;
//	Int_t yellow = 5; 
//	Int_t magenta= 6;
//	Int_t cyan	= 7;
//	Int_t purple = 9;


	////////////////////////////////////////////////////////////////////
	// PURPOSE:
	//
	// This macro defines a standard style for (black-and-white) 
	// "publication quality" LHCb ROOT plots. 
	//
	// USAGE:
	//
	// Include the lines
	//	gROOT->ProcessLine(".L lhcbstyle.C");
	//	lhcbStyle();
	// at the beginning of your root macro.
	//
	// Example usage is given in myPlot.C
	//
	// COMMENTS:
	//
	// Font:
	// 
	// The font is chosen to be 132, this is Times New Roman (like the text of
	//  your document) with precision 2.
	//
	// "Landscape histograms":
	//
	// The style here is designed for more or less square plots.
	// For longer histograms, or canvas with many pads, adjustements are needed. 
	// For instance, for a canvas with 1x5 histograms:
	//  TCanvas* c1 = new TCanvas("c1", "L0 muons", 600, 800);
	//  c1->Divide(1,5);
	//  Adaptions like the following will be needed:
	//  gStyle->SetTickLength(0.05,"x");
	//  gStyle->SetTickLength(0.01,"y");
	//  gStyle->SetLabelSize(0.15,"x");
	//  gStyle->SetLabelSize(0.1,"y");
	//  gStyle->SetStatW(0.15);
	//  gStyle->SetStatH(0.5);
	//
	// Authors: Thomas Schietinger, Andrew Powell, Chris Parkes, Niels Tuning
	// Maintained by Editorial board member (currently Niels)
	///////////////////////////////////////////////////////////////////

	// Use times new roman, precision 2 
	Int_t lhcbFont		= 132;  // Old LHCb style: 62;
	// Line thickness
	Double_t lhcbWidth	= 2.00; // Old LHCb style: 3.00;
	// Text size
	Double_t lhcbTSize	= fontsize;//0.06; 

	// use plain black on white colors
	gROOT->SetStyle("Plain"); 
	TStyle *lhcbStyle= new TStyle("lhcbStyle","LHCb plots style");

	//lhcbStyle->SetErrorX(0); //  don't suppress the error bar along X

	lhcbStyle->SetFillColor(1);
	lhcbStyle->SetFillStyle(1001);	// solid
	lhcbStyle->SetFrameFillColor(0);
	lhcbStyle->SetFrameBorderMode(0);
	lhcbStyle->SetPadBorderMode(0);
	lhcbStyle->SetPadColor(0);
	lhcbStyle->SetCanvasBorderMode(0);
	lhcbStyle->SetCanvasColor(0);
	lhcbStyle->SetStatColor(0);
	lhcbStyle->SetLegendBorderSize(0);
	lhcbStyle->SetLegendFont(132);

	// If you want the usual gradient palette (blue -> red)

	// If you want colors that correspond to gray scale in black and white:
//	int colors[8] = {0,5,7,3,6,2,4,1};
//	lhcbStyle->SetPalette(8,colors);

	//***********
	lhcbStyle->SetPalette(kTemperatureMap);
	lhcbStyle->SetNumberContours(255);

	//const Int_t NRGBs = 5;
	//const Int_t NCont = 255;

	//Double_t stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
	//Double_t red[NRGBs]	= { 0.00, 0.00, 0.87, 1.00, 0.51 };
	//Double_t green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
	//Double_t blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
	//TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
	//lhcbStyle->SetNumberContours(NCont);
	//***********


	// set the paper & margin sizes
	lhcbStyle->SetPaperSize(20,26);
	lhcbStyle->SetPadTopMargin(0.05);
	lhcbStyle->SetPadRightMargin(0.05); // increase for colz plots
	lhcbStyle->SetPadBottomMargin(0.16);
	lhcbStyle->SetPadLeftMargin(0.14);

	// use large fonts
	lhcbStyle->SetTextFont(lhcbFont);
	lhcbStyle->SetTextSize(lhcbTSize);
	lhcbStyle->SetLabelFont(lhcbFont,"x");
	lhcbStyle->SetLabelFont(lhcbFont,"y");
	lhcbStyle->SetLabelFont(lhcbFont,"z");
	lhcbStyle->SetLabelSize(lhcbTSize,"x");
	lhcbStyle->SetLabelSize(lhcbTSize,"y");
	lhcbStyle->SetLabelSize(lhcbTSize,"z");
	lhcbStyle->SetTitleFont(lhcbFont);
	lhcbStyle->SetTitleFont(lhcbFont,"x");
	lhcbStyle->SetTitleFont(lhcbFont,"y");
	lhcbStyle->SetTitleFont(lhcbFont,"z");
	lhcbStyle->SetTitleSize(1.2*lhcbTSize,"x");
	lhcbStyle->SetTitleSize(1.2*lhcbTSize,"y");
	lhcbStyle->SetTitleSize(1.2*lhcbTSize,"z");

	// use medium bold lines and thick markers
	lhcbStyle->SetLineWidth(lhcbWidth);
	lhcbStyle->SetFrameLineWidth(lhcbWidth);
	lhcbStyle->SetHistLineWidth(lhcbWidth);
	lhcbStyle->SetFuncWidth(lhcbWidth);
	lhcbStyle->SetGridWidth(lhcbWidth);
	lhcbStyle->SetLineStyleString(2,"[12 12]"); // postscript dashes
	lhcbStyle->SetMarkerStyle(20);
	lhcbStyle->SetMarkerSize(1);

	// label offsets
	lhcbStyle->SetLabelOffset(0.010,"X");
	lhcbStyle->SetLabelOffset(0.010,"Y");

	// by default, do not display histogram decorations:
	lhcbStyle->SetOptStat(0);  
	//lhcbStyle->SetOptStat("emr");  // show only nent -e , mean - m , rms -r
	// full opts at http://root.cern.ch/root/html/TStyle.html#TStyle:SetOptStat
	lhcbStyle->SetStatFormat("6.3g"); // specified as c printf options
	lhcbStyle->SetOptTitle(0);
	lhcbStyle->SetOptFit(0);
	//lhcbStyle->SetOptFit(1011); // order is probability, Chi2, errors, parameters
	//titles
	lhcbStyle->SetTitleOffset(0.95,"X");
	lhcbStyle->SetTitleOffset(0.95,"Y");
	lhcbStyle->SetTitleOffset(1.2,"Z");
	lhcbStyle->SetTitleFillColor(0);
	lhcbStyle->SetTitleStyle(0);
	lhcbStyle->SetTitleBorderSize(0);
	lhcbStyle->SetTitleFont(lhcbFont,"title");
	lhcbStyle->SetTitleX(0.0);
	lhcbStyle->SetTitleY(1.0); 
	lhcbStyle->SetTitleW(1.0);
	lhcbStyle->SetTitleH(0.05);

	// look of the statistics box:
	lhcbStyle->SetStatBorderSize(0);
	lhcbStyle->SetStatFont(lhcbFont);
	lhcbStyle->SetStatFontSize(0.05);
	lhcbStyle->SetStatX(0.9);
	lhcbStyle->SetStatY(0.9);
	lhcbStyle->SetStatW(0.25);
	lhcbStyle->SetStatH(0.15);

	// put tick marks on top and RHS of plots
//	lhcbStyle->SetPadTickX(1);
//	lhcbStyle->SetPadTickY(1);

	// histogram divisions: only 5 in x to avoid label overlaps
	lhcbStyle->SetNdivisions(505,"x");
	lhcbStyle->SetNdivisions(510,"y");

	gROOT->SetStyle("lhcbStyle");
	gROOT->ForceStyle();

	// add LHCb label
	TPaveText* lhcbName = new TPaveText(gStyle->GetPadLeftMargin() + 0.05,
		 0.87 - gStyle->GetPadTopMargin(),
		 gStyle->GetPadLeftMargin() + 0.20,
		 0.95 - gStyle->GetPadTopMargin(),
		 "BRNDC");
	lhcbName->AddText("LHCb");
	lhcbName->SetFillColor(0);
	lhcbName->SetTextAlign(12);
	lhcbName->SetBorderSize(0);

	TText *lhcbLabel = new TText();
	lhcbLabel->SetTextFont(lhcbFont);
	lhcbLabel->SetTextColor(1);
	lhcbLabel->SetTextSize(lhcbTSize);
	lhcbLabel->SetTextAlign(12);

	TLatex *lhcbLatex = new TLatex();
	lhcbLatex->SetTextFont(lhcbFont);
	lhcbLatex->SetTextColor(1);
	lhcbLatex->SetTextSize(lhcbTSize);
	lhcbLatex->SetTextAlign(12);

	//own colours
	
	TColor* col1 = gROOT->GetColor(92);
	col1->SetRGB(0.992, 0.6823, 0.3804);  //orange for comb
	TColor* col2 = gROOT->GetColor(93);
	col2->SetRGB(0.6706, 0.8510, 0.9137); //light blue for JPsi leacking
	TColor* col3 = gROOT->GetColor(94);
	col3->SetRGB(0.8431, 0.098, 0.1098);  //red for  prc
	TColor* col4 = gROOT->GetColor(95);
	col4->SetRGB(0.1725, 0.4824, 0.7137); //dark blue for psi2S Prc
	TColor* col5 = gROOT->GetColor(96);
	col5->SetRGB(1., 1., 191/255.);		//very light yello for rare
	TColor* col6 = gROOT->GetColor(97);
	col6->SetRGB(190./255., 174./255., 212/255.);		//purple
	TColor* col7 = gROOT->GetColor(98);
	col7->SetRGB(240./255., 2./255., 127/255.);		//magenta
	TColor* col8 = gROOT->GetColor(99);
	col8->SetRGB(191./255., 91./255., 23/255.);		//brown

	////这个地方解开注释会提示空指针
	//TColor* col9 = gROOT->GetColor(100);
	//col9->SetRGB(127./255., 201./255., 127/255.);		//green


	cout << "-------------------------" << endl;  
	cout << "Set LHCb Style - Feb 2012" << endl;
	cout << "-------------------------" << endl;  

}

bool selTree(TTree* tree, TString newFileName, TString cuts){
	TFile* newFile = new TFile(newFileName, "recreate");
	if(!tree){
		cerr<<"ERROR: in selTree, no tree found. Abort.";
		return false;
	}
	tree->SetBranchStatus("*", 1);

	if(cuts=="") cuts="1";
	TTree* newTree = tree->CloneTree(0);

	 TTreeFormula cutsTTF("cutTTF", cuts, tree);

	int nEntries = tree->GetEntries();

	for(int i=0; i<nEntries; i++){
		tree->GetEntry(i);
		if(i % (nEntries/10) == 0) cout<<" "<<ceil(100*i/(1.*nEntries))<<"\% "<<flush;
		if(cutsTTF.EvalInstance()){
			newTree->Fill();
		}
	}
	cout << endl;


	newFile->cd();
	newTree->Write();

	newFile->Close();

	return true;
}
bool selTree(TString treeFileName, TString newFileName, TString cuts){
	TFile f(treeFileName);
	TTree* tree = (TTree*)f.Get("DecayTree");

	printf("Select tree: %s\n", treeFileName.Data());
	printf("Output tree: %s\n\n", newFileName.Data());

	selTree(tree, newFileName, cuts);
	f.Close();
	return true;
}

void superImposeEfficiencyPlots(vector<TH1*> passHists, vector<TH1*> totHists, TString plotName, TString yLabel = "",
	  TString xLabel = "", double min = -20., double max = -20., vector<TString> plotOptions = vector<TString>(0), 
	  vector<TString> legEntries = vector<TString>(0), vector<double> legPos = vector<double>(4,0),
	  TString divideOption = "B", vector<int> colourTab = vector<int>(0), double xThreshold = 0., bool wantLogX = false, double scale = 1.)
{
	unsigned int nPlots(passHists.size());

	if( plotOptions.size() == 0 ) plotOptions = vector<TString>(nPlots, "E1");

	if(totHists.size() != nPlots || plotOptions.size() != nPlots)
	{
	  cerr<<"ERROR: in superImposeEfficiencyPlots, mismatch in the number of plots"<<endl;
	  return;
	}

	//check if the histograms are fine

	for(unsigned int i(0); i < nPlots; ++i)
	{
	  if(passHists.at(i) == NULL || totHists.at(i) == NULL)
	  {
		 cerr<<"ERROR: in superImposeEfficiencyPlots, "<<i<<"th histogram is NULL "<<passHists.at(i)<<" "<<totHists.at(i)<<endl;
		 return;
	  }
	}

	//make fraction

	vector<TH1*> effHists(nPlots, NULL); 

	for(unsigned int i(0); i < nPlots; ++i)
	{
	  effHists.at(i) = (TH1*)passHists.at(i)->Clone("effHist"+i2s(i)); 
	  effHists.at(i)->Divide(passHists.at(i), totHists.at(i), 1., 1., divideOption);
	}

	//see if need to set max and min

	if(min < -10. && max < -10. )
	{
	  min = numeric_limits<double>::max();
	  max = numeric_limits<double>::min();
	  double dummy(0);

	  //in that case, set max and min yourself
	  for(unsigned int i(0); i < nPlots; ++i)
	  {
		 for(int j(1); j <= effHists[i]->GetNbinsX(); ++j)
		 {
			dummy = effHists[i]->GetBinContent(j) + effHists[i]->GetBinError(j);
			if(max < dummy) max = dummy;

			dummy = effHists[i]->GetBinContent(j) - effHists[i]->GetBinError(j);
			if(min > dummy) min = dummy;
		 }
	  }

	  double shift(0.05*(max-min)); 
	  max += shift;
	  min -= shift;
	}
	
	//beautify  

	TH1D hcaca("hcaca", "hcaca", 1, effHists.at(0)->GetXaxis()->GetXmin(), effHists.at(0)->GetXaxis()->GetXmax());
	hcaca.SetLineColor(0);
	hcaca.SetMarkerColor(0);
	if(xLabel != "") hcaca.GetXaxis()->SetTitle(xLabel);
	if(yLabel != "") 
	{
	  hcaca.GetYaxis()->SetTitleOffset(1.0);
	  hcaca.GetYaxis()->SetTitle(yLabel);
	}
	hcaca.GetYaxis()->SetRangeUser(scale*min, scale*max);
	//hcaca.GetYaxis()->SetRangeUser(min, max);

	double nColor(1);  

	cout<<"nPlots: "<<nPlots<<" colourTab: "<<colourTab.size()<<endl;

	if( colourTab.size() < nPlots) 
	{
	  colourTab.push_back(1); 
	  colourTab.push_back(2); 
	  colourTab.push_back(95); 
	  colourTab.push_back(92); 
	  colourTab.push_back(93); 
	  colourTab.push_back(94); 
	  colourTab.push_back(96); 
	  colourTab.push_back(28); 
	  colourTab.push_back(29); 
	  colourTab.push_back(30); 
	  colourTab.push_back(38); 
	  colourTab.push_back(39); 
	  colourTab.push_back(43); 
	  colourTab.push_back(46); 
	  colourTab.push_back(49); 
	}

	for(unsigned int i(0); i<nPlots; ++i)
	{
	  effHists.at(i)->Scale(scale);
	  nColor = colourTab.at(i % 15);
	  effHists.at(i)->SetLineColor(nColor); 
	  effHists.at(i)->SetMarkerColor(nColor);
	  effHists.at(i)->GetYaxis()->SetRangeUser(scale*min, scale*max);
	  //effHists.at(i)->GetYaxis()->SetRangeUser(min, max);
	  if(xLabel != "") effHists.at(i)->GetXaxis()->SetTitle(xLabel);
	  if(yLabel != "") 
	  {
		 effHists.at(i)->GetYaxis()->SetTitleOffset(1.);
		 effHists.at(i)->GetYaxis()->SetTitle(yLabel);
	  }
	}

	//plot

	TCanvas canv("canv", "canv", 800, 600);
	canv.SetLeftMargin(0.15);
	canv.SetRightMargin(0.10);

	TLegend leg(legPos.at(0), legPos.at(1), legPos.at(2), legPos.at(3)); 
	leg.SetTextSize(effHists.at(0)->GetYaxis()->GetTitleSize());
	leg.SetLineColor(0);
	leg.SetFillColor(0);

	hcaca.Draw();
	if(nPlots > 0) effHists.at(0)->Draw( plotOptions.at(0)+"same" ); 
	for(unsigned int i(1); i<nPlots; ++i) effHists.at(i)->Draw( plotOptions.at(i)+"same" );

	if(legEntries.size() == nPlots )
	{
	  for(unsigned int i(0); i<nPlots; ++i) 
	  {
		 if(legEntries.at(i) != "") leg.AddEntry(effHists.at(i), legEntries.at(i) );
	  }
	  leg.Draw();
	}

	TLine lineThreshold(xThreshold, min, xThreshold, max);
	lineThreshold.SetLineColor(1);
	lineThreshold.SetLineStyle(2);
	if(xThreshold > 0) lineThreshold.Draw();


	if(wantLogX) canv.SetLogx(true);

	canv.Print(plotName );
}

void superImposeEfficiencyPlots(vector<TString> fileNamesPass, vector<TString> histNamesPass, vector<TString> fileNamesTot, vector<TString> histNamesTot,
	  TString plotName, TString yLabel = "", TString xLabel = "", double min = -20., double max = -20., vector<TString> plotOptions = vector<TString>(0), 
	  vector<TString> legEntries = vector<TString>(0), vector<double> legPos = vector<double>(4,0),
	  TString divideOption = "B", vector<int> colourTab = vector<int>(0), double xThreshold = 0., bool wantLogX = false, double scale = 1.)
{
	unsigned int nTot(fileNamesPass.size());

	if(nTot == 0)
	{
	  cerr<<"WARNING: in superImposeEfficiencyPlots, nothing to plot!"<<endl;
	  return;
	}

	if(histNamesPass.size() != nTot || fileNamesTot.size() != nTot || histNamesTot.size() != nTot)
	{
	  cerr<<"ERROR: in superImposeEfficiencyPlots, mismatch in the number of histograms. "<<endl;
	  return;
	}

	vector<TFile*> filesPass(nTot, NULL);
	vector<TFile*> filesTot(nTot, NULL);
	vector<TH1*> histPass(nTot, NULL);
	vector<TH1*> histTot(nTot, NULL);

	for(unsigned int i(0); i < nTot; ++i)
	{
	  filesPass.at(i) = new TFile( fileNamesPass.at(i) );
	  histPass.at(i) = (TH1*)filesPass.at(i)->Get( histNamesPass.at(i) );
	  filesTot.at(i) = new TFile( fileNamesTot.at(i) );
	  histTot.at(i) = (TH1*)filesTot.at(i)->Get( histNamesTot.at(i) );
	}

	superImposeEfficiencyPlots(histPass, histTot, plotName, yLabel, xLabel, min, max, plotOptions, legEntries, legPos, divideOption, colourTab, xThreshold, wantLogX, scale);

	for(unsigned int i(0); i < nTot; ++i)
	{
	  if(filesPass.at(i) != NULL)
	  {
		 filesPass.at(i)->Close();
		 delete filesPass.at(i);
	  }

	  if(filesTot.at(i) != NULL)
	  {
		 filesTot.at(i)->Close();
		 delete filesTot.at(i);
	  }
	}
}

void projectSliceTriggerPlots(vector<TH2*> passHists, vector<TH2*> totHists, TString plotNameStart, TString yLabel = "",
	TString xLabel = "", double min = -20., double max = -20., vector<TString> plotOptions = vector<TString>(0), vector<TString> legEntries = vector<TString>(0), 
	vector<double> legPos = vector<double>(4,0), vector<int> colourTab = vector<int>(0), double xThreshold = 0., double scale = 1.)
{
	unsigned int nPlots(passHists.size());		

	if(totHists.size() != nPlots ) 
	{
	  cerr<<"ERROR: in function projectTriggerPlots, mismatch in number of hists"<<endl;
	  return;
	}

	//check if the histograms are fine

	for(unsigned int i(0); i < nPlots; ++i)
	{
	  if(passHists.at(i) == NULL || totHists.at(i) == NULL)
	  {
		 cerr<<"ERROR: in projectSliceTriggerPlots, "<<i<<"th histogram is NULL "<<passHists.at(i)<<" "<<totHists.at(i)<<endl;
		 return;
	  }
	}

	//make projections

	vector<TH1*> passProjX(nPlots, NULL); 
	vector<TH1*> passProjY(nPlots, NULL); 

	vector<TH1*> totProjX(nPlots, NULL); 
	vector<TH1*> totProjY(nPlots, NULL); 

	int nBinsX;

	nBinsX = passHists.at(0) -> GetNbinsX(); 

	for(int j(1); j <= nBinsX; ++j)
	{
	  for(unsigned int i(0); i < nPlots; ++i)
	  {
		 passProjY.at(i) = passHists.at(i)->ProjectionY( "passProjY"+i2s(i), j, j);
		 totProjY.at(i) = totHists.at(i)->ProjectionY( "totProjY"+i2s(i), j, j );
	  }
	  superImposeEfficiencyPlots(passProjY, totProjY, plotNameStart+"_slice"+i2s(j)+".pdf", yLabel, 
			xLabel, min, max, plotOptions, legEntries, legPos, "B", colourTab, xThreshold, false, scale);
	}
}

void projectSliceTriggerPlots(vector<TH2*> passDataHists, vector<TH2*> totDataHists, 
	vector<TH2*> passMCHists, vector<TH2*> totMCHists, TString plotNameStart, TString yLabel = "",
	TString xLabel = "", double min = -20., double max = -20., vector<TString> plotOptions = vector<TString>(0), vector<TString> legEntries = vector<TString>(0),
	vector<double> legPos = vector<double>(4,0), vector<int> colourTab = vector<int>(0), double xThreshold = 0., double scale = 1.)
{
	unsigned int nPlots(passDataHists.size());		

	if(totDataHists.size() != nPlots || passMCHists.size() != nPlots || totMCHists.size() != nPlots) 
	{
	  cerr<<"ERROR: in function projectTriggerPlots, mismatch in pass and tot hists"<<endl;
	  return;
	}


	//check if the histograms are fine


	for(unsigned int i(0); i < nPlots; ++i)
	{
	  if(passDataHists.at(i) == NULL || totDataHists.at(i) == NULL || passMCHists.at(i) == NULL || totDataHists.at(i) == NULL)
	  {
		 cerr<<"ERROR: in projectSliceTriggerPlots, "<<i<<"th histogram is NULL "<<passDataHists.at(i)<<" "<<totDataHists.at(i)<<" "<<passMCHists.at(i)<<" "<<totMCHists.at(i)<<endl;
		 return;
	  }
	}

	//make projections

	vector<TH1*> passDataProjX(nPlots, NULL); 
	vector<TH1*> passDataProjY(nPlots, NULL); 

	vector<TH1*> totDataProjX(nPlots, NULL); 
	vector<TH1*> totDataProjY(nPlots, NULL); 

	vector<TH1*> passMCProjX(nPlots, NULL); 
	vector<TH1*> passMCProjY(nPlots, NULL); 

	vector<TH1*> totMCProjX(nPlots, NULL); 
	vector<TH1*> totMCProjY(nPlots, NULL); 

	vector<TH1*> effDataProjX(nPlots, NULL); 
	vector<TH1*> effDataProjY(nPlots, NULL); 

	vector<TH1*> effMCProjX(nPlots, NULL); 
	vector<TH1*> effMCProjY(nPlots, NULL); 

	int nBinsX;
	//int nBinsY;



	nBinsX = passMCHists.at(0) -> GetNbinsX(); 

	for(int j(1); j <= nBinsX; ++j)
	{

	  for(unsigned int i(0); i < nPlots; ++i)
	  {
		 passDataProjY.at(i) = passDataHists.at(i)->ProjectionY( "passDataProjY"+i2s(i), j, j);
		 totDataProjY.at(i) = totDataHists.at(i)->ProjectionY( "totDataProjY"+i2s(i), j, j );

		 effDataProjY.at(i) = (TH1*)passDataProjY.at(i)->Clone("effDataProjY"+i2s(i));
		 effDataProjY.at(i)->Divide(passDataProjY.at(i), totDataProjY.at(i), 1., 1., "B");

		 passMCProjY.at(i) = passMCHists.at(i)->ProjectionY( "passMCProjY"+i2s(i), j, j);
		 totMCProjY.at(i) = totMCHists.at(i)->ProjectionY( "totMCProjY"+i2s(i), j, j );

		 effMCProjY.at(i) = (TH1*)passMCProjY.at(i)->Clone("effMCProjY"+i2s(i));
		 effMCProjY.at(i)->Divide(passMCProjY.at(i), totMCProjY.at(i), 1., 1., "B");

	  }
	  superImposeEfficiencyPlots(effDataProjY, effMCProjY, plotNameStart+"_slice"+i2s(j)+".pdf", yLabel, 
			xLabel, min, max, plotOptions, legEntries, legPos, "P", colourTab, xThreshold, false, scale);
	}
}





void projectSliceTriggerPlots2D(vector<TString> fileNames, vector<TString> totHistNames, vector<TString> passHistNames,
	TString plotNameStart, TString yLabel = "", 
	TString xLabel =  "", double min = -20., double max = -20., vector<TString> plotOptions = vector<TString>(0), vector<TString> legEntries = vector<TString>(0), 
	vector<double> legPos = vector<double>(4,0), vector<int> colourTab = vector<int>(0), double xThreshold = 0., double scale = 1.)
{
	unsigned int nPlots(fileNames.size()); 

	if(totHistNames.size() != nPlots || passHistNames.size() != nPlots)
	{
	  cerr<<"ERROR: in function projectSliceTriggerPlots2D, mismatch in the number of histograms"<<endl;
	  return; 
	}

	//recover histograms from files

	vector<TFile*> files(nPlots, NULL);
	vector<TH2*>	totHists(nPlots, NULL);
	vector<TH2*>	passHists(nPlots, NULL);

	for(unsigned int i(0); i < nPlots; ++i)
	{
	  files.at(i) = new TFile(fileNames.at(i));
	  totHists.at(i) = (TH2*)files.at(i)->Get(totHistNames.at(i));
	  passHists.at(i) = (TH2*)files.at(i)->Get(passHistNames.at(i));
	}

	//plot everything

	projectSliceTriggerPlots(passHists, totHists, plotNameStart, yLabel,
		 xLabel, min, max, plotOptions, legEntries, legPos, colourTab, xThreshold, scale);

	//close all

	for(unsigned int i(0); i < nPlots; ++i)
	{
	  files.at(i)->Close();
	  if(files.at(i) != NULL) delete files.at(i);
	}
}


void projectSliceTriggerPlots2D(vector<TString> dataFileNames, vector<TString> totDataHistNames, vector<TString> passDataHistNames, 
	vector<TString> MCFileNames, vector<TString> totMCHistNames, vector<TString> passMCHistNames,
	TString plotNameStart, TString yLabel = "",
	TString xLabel = "", double min =  -20., double max = -20., vector<TString> plotOptions = vector<TString>(0), vector<TString> legEntries= vector<TString>(0), 
	vector<double> legPos = vector<double>(4,0), vector<int> colourTab = vector<int>(0), 
	double xThreshold = 0., double scale = 1.){
	unsigned int nPlots(dataFileNames.size()); 

	if(totDataHistNames.size() != nPlots || passDataHistNames.size() != nPlots || totMCHistNames.size() != nPlots || passMCHistNames.size() != nPlots || MCFileNames.size() != nPlots)
	{
	  cerr<<"ERROR: in function projectSliceTriggerPlots2D, mismatch in the number of histograms"<<endl;
	  return; 
	}

	//recover histograms from files

	vector<TFile*> dataFiles(nPlots, NULL);
	vector<TH2*>	dataTotHists(nPlots, NULL);
	vector<TH2*>	dataPassHists(nPlots, NULL);

	vector<TFile*> MCFiles(nPlots, NULL);
	vector<TH2*>	MCTotHists(nPlots, NULL);
	vector<TH2*>	MCPassHists(nPlots, NULL);

	for(unsigned int i(0); i < nPlots; ++i)
	{
	  dataFiles.at(i) = new TFile(dataFileNames.at(i));
	  dataTotHists.at(i) = (TH2*)dataFiles.at(i)->Get(totDataHistNames.at(i));
	  dataPassHists.at(i) = (TH2*)dataFiles.at(i)->Get(passDataHistNames.at(i));

	  MCFiles.at(i) = new TFile(MCFileNames.at(i));
	  MCTotHists.at(i) = (TH2*)MCFiles.at(i)->Get(totMCHistNames.at(i));
	  MCPassHists.at(i) = (TH2*)MCFiles.at(i)->Get(passMCHistNames.at(i));
	}

	//plot everything

	projectSliceTriggerPlots(dataPassHists, dataTotHists, MCPassHists, MCTotHists, plotNameStart, yLabel,
		 xLabel, min, max, plotOptions, legEntries, legPos, colourTab, xThreshold, scale);

	//close all

	for(unsigned int i(0); i < nPlots; ++i)
	{
	  dataFiles.at(i)->Close();
	  MCFiles.at(i)->Close();
	  if(dataFiles.at(i) != NULL) delete dataFiles.at(i);
	  if(MCFiles.at(i) != NULL) delete MCFiles.at(i);
	}
}

void projectEfficiencyPlots(vector<TH3*> passHists, vector<TH3*> totHists, TString plotNameStart, TString yLabel,
      vector<TString> xLabel, double min, double max, vector<TString> plotOptions, vector<TString> legEntries, vector<double> legPos, vector<int> colourTab, double xThreshold)
{
   unsigned int nPlots(passHists.size());

   if(totHists.size() != nPlots)
   {
      cerr<<"ERROR: in function projectEfficiencyPlots, mismatch in pass and tot hists"<<endl;
      return;
   }

   if( xLabel.size() != 3) xLabel = vector<TString>(3, "");

   //check if the histograms are fine


   for(unsigned int i(0); i < nPlots; ++i)
   {
      if(passHists.at(i) == NULL || totHists.at(i) == NULL)
      {
         cerr<<"ERROR: in projectEfficiencyPlots, "<<i<<"th histogram is NULL "<<passHists.at(i)<<" "<<totHists.at(i)<<endl;
         return;
      }
   }

   //make projections

   vector<TH1*> passProjX(nPlots, NULL);
   vector<TH1*> passProjY(nPlots, NULL);
   vector<TH1*> passProjZ(nPlots, NULL);

   vector<TH1*> totProjX(nPlots, NULL);
   vector<TH1*> totProjY(nPlots, NULL);
   vector<TH1*> totProjZ(nPlots, NULL);

   int nBinsX;
   int nBinsY;
   int nBinsZ;

   for(unsigned int i(0); i < nPlots; ++i)
   {
      nBinsX = passHists.at(i) -> GetNbinsX();
      nBinsY = passHists.at(i) -> GetNbinsY();
      nBinsZ = passHists.at(i) -> GetNbinsZ();

      passProjX.at(i) = passHists.at(i)->ProjectionX( "passProjX"+i2s(i), 1, nBinsY, 1, nBinsZ );
      passProjY.at(i) = passHists.at(i)->ProjectionY( "passProjY"+i2s(i), 1, nBinsX, 1, nBinsZ );
      passProjZ.at(i) = passHists.at(i)->ProjectionZ( "passProjZ"+i2s(i), 1, nBinsX, 1, nBinsY );

      totProjX.at(i) = totHists.at(i)->ProjectionX( "totProjX"+i2s(i), 1, nBinsY, 1, nBinsZ );
      totProjY.at(i) = totHists.at(i)->ProjectionY( "totProjY"+i2s(i), 1, nBinsX, 1, nBinsZ );
      totProjZ.at(i) = totHists.at(i)->ProjectionZ( "totProjZ"+i2s(i), 1, nBinsX, 1, nBinsY );
   }

   //plot the projections

   superImposeEfficiencyPlots(passProjX, totProjX, plotNameStart+"_projX.pdf", yLabel,
         xLabel.at(0), min, max, plotOptions, legEntries, legPos, "B", colourTab, xThreshold);

   superImposeEfficiencyPlots(passProjY, totProjY, plotNameStart+"_projY.pdf", yLabel,
         xLabel.at(1), min, max, plotOptions, legEntries, legPos, "B", colourTab, xThreshold);

   //superImposeEfficiencyPlots(passProjX, totProjX, plotNameStart+"_projX.pdf", yLabel,
   //      xLabel.at(0), min, max, plotOptions, legEntries, legPos, "B", colourTab, xThreshold);

   //superImposeEfficiencyPlots(passProjY, totProjY, plotNameStart+"_projY.pdf", yLabel,
   //      xLabel.at(1), min, max, plotOptions, legEntries, legPos, "B", colourTab, xThreshold);

   //superImposeEfficiencyPlots(passProjZ, totProjZ, plotNameStart+"_projZ.pdf", yLabel,
   //      xLabel.at(2), min, max, plotOptions, legEntries, legPos, "B", colourTab, xThreshold);

   //superImposeEfficiencyPlots(passProjX, totProjX, plotNameStart+"_projX_LogX.pdf", yLabel,
   //      xLabel.at(0), min, max, plotOptions, legEntries, legPos, "B", colourTab, xThreshold, true);

   //superImposeEfficiencyPlots(passProjY, totProjY, plotNameStart+"_projY_LogX.pdf", yLabel,
   //      xLabel.at(1), min, max, plotOptions, legEntries, legPos, "B", colourTab, xThreshold, true);

   //superImposeEfficiencyPlots(passProjZ, totProjZ, plotNameStart+"_projZ_LogX.pdf", yLabel,
   //      xLabel.at(2), min, max, plotOptions, legEntries, legPos, "B", colourTab, xThreshold, true);
}



void projectEfficiencyPlots(vector<TString> fileNames, vector<TString> totHistNames, vector<TString> passHistNames, TString plotNameStart, TString yLabel,
   vector<TString> xLabel, double min, double max, vector<TString> plotOptions, vector<TString> legEntries, vector<double> legPos, vector<int> colourTab, double xThreshold)
{
   unsigned int nPlots(fileNames.size());

   if(totHistNames.size() != nPlots || passHistNames.size() != nPlots)
   {
      cerr<<"ERROR: in function projectEfficiencyPlots, mismatch in the number of histograms"<<endl;
      return;
   }

   //recover histograms from files

   vector<TFile*> files(nPlots, NULL);
   vector<TH3*> totHists(nPlots, NULL);
   vector<TH3*> passHists(nPlots, NULL);

   for(unsigned int i(0); i < nPlots; ++i)
   {
      files.at(i) = new TFile(fileNames.at(i));
      totHists.at(i) = (TH3*)files.at(i)->Get(totHistNames.at(i));
      passHists.at(i) = (TH3*)files.at(i)->Get(passHistNames.at(i));
   }

   //plot everything

   projectEfficiencyPlots(passHists, totHists, plotNameStart, yLabel,
         xLabel, min, max, plotOptions, legEntries, legPos, colourTab, xThreshold);

   //close all

   for(unsigned int i(0); i < nPlots; ++i)
   {
      files.at(i)->Close();
      if(files.at(i) != NULL) delete files.at(i);
   }
}




void projectEfficiencyPlots2D(vector<TString> fileNames, vector<TString> totHistNames, vector<TString> passHistNames, TString plotNameStart, TString yLabel,
   vector<TString> xLabel, double min, double max, vector<TString> plotOptions, vector<TString> legEntries, vector<double> legPos, vector<int> colourTab, double xThreshold)
{
   unsigned int nPlots(fileNames.size());

   if(totHistNames.size() != nPlots || passHistNames.size() != nPlots)
   {
      cerr<<"ERROR: in function projectEfficiencyPlots2D, mismatch in the number of histograms"<<endl;
      return;
   }

   //recover histograms from files

   vector<TFile*> files(nPlots, NULL);
   vector<TH2*> totHists(nPlots, NULL);
   vector<TH2*> passHists(nPlots, NULL);

   for(unsigned int i(0); i < nPlots; ++i)
   {
      files.at(i) = new TFile(fileNames.at(i));
      totHists.at(i) = (TH2*)files.at(i)->Get(totHistNames.at(i));
      passHists.at(i) = (TH2*)files.at(i)->Get(passHistNames.at(i));
   }

   //plot everything

   projectEfficiencyPlots(passHists, totHists, plotNameStart, yLabel,
         xLabel, min, max, plotOptions, legEntries, legPos, colourTab, xThreshold);

   //close all

   for(unsigned int i(0); i < nPlots; ++i)
   {
      files.at(i)->Close();
      if(files.at(i) != NULL) delete files.at(i);
   }
}






void projectEfficiencyPlots(vector<TH2*> passHists, vector<TH2*> totHists, TString plotNameStart, TString yLabel,
   vector<TString> xLabel, double min, double max, vector<TString> plotOptions, vector<TString> legEntries, vector<double> legPos, vector<int> colourTab, double xThreshold)
{
   unsigned int nPlots(passHists.size());

   if(totHists.size() != nPlots)
   {
      cerr<<"ERROR: in function projectEfficiencyPlots, mismatch in pass and tot hists"<<endl;
      return;
   }

   if( xLabel.size() != 2) xLabel = vector<TString>(2, "");

   //check if the histograms are fine


   for(unsigned int i(0); i < nPlots; ++i)
   {
      if(passHists.at(i) == NULL || totHists.at(i) == NULL)
      {
         cerr<<"ERROR: in projectEfficiencyPlots, "<<i<<"th histogram is NULL "<<passHists.at(i)<<" "<<totHists.at(i)<<endl;
         return;
      }
   }

   //make projections

   vector<TH1*> passProjX(nPlots, NULL);
   vector<TH1*> passProjY(nPlots, NULL);

   vector<TH1*> totProjX(nPlots, NULL);
   vector<TH1*> totProjY(nPlots, NULL);

   int nBinsX;
   int nBinsY;

   for(unsigned int i(0); i < nPlots; ++i)
   {
      nBinsX = passHists.at(i) -> GetNbinsX();
      nBinsY = passHists.at(i) -> GetNbinsY();

      passProjX.at(i) = passHists.at(i)->ProjectionX( "passProjX"+i2s(i), 1, nBinsY);
      passProjY.at(i) = passHists.at(i)->ProjectionY( "passProjY"+i2s(i), 1, nBinsX);

      totProjX.at(i) = totHists.at(i)->ProjectionX( "totProjX"+i2s(i), 1, nBinsY );
      totProjY.at(i) = totHists.at(i)->ProjectionY( "totProjY"+i2s(i), 1, nBinsX );
   }

   //plot the projections

   superImposeEfficiencyPlots(passProjX, totProjX, plotNameStart+"_projX.pdf", yLabel,
         xLabel.at(0), min, max, plotOptions, legEntries, legPos, "B", colourTab, xThreshold);

   superImposeEfficiencyPlots(passProjY, totProjY, plotNameStart+"_projY.pdf", yLabel,
         xLabel.at(1), min, max, plotOptions, legEntries, legPos, "B", colourTab, xThreshold);
}

class TreeWithInfo{
	public:

	  TreeWithInfo(int _index, TString _treeFileName, int _stage, bool _isMuon)
		 :index(_index), treeFileName(_treeFileName), stage(_stage), isMuon(_isMuon)
	  {}


	  int index;
	  TString treeFileName;
	  int stage;
	  bool isMuon;

	  void displayOnScreen()
	  {
		 cout<<endl<<endl<<"=======================Tree Display============================================"<<endl;
		 cout<<"Tree Name: "<<treeFileName<<endl;
		 cout<<"The tree is a: ";
		 if(stage == 0) cout<<"gen level no decprodcut";
		 if(stage == 1) cout<<"gen level ";
		 if(stage == 2) cout<<"reco level ";
		 if(isMuon) cout<<"muon ";
		 if(!isMuon) cout<<"electron ";
		 cout<<"tree"<<endl;
		 cout<<endl<<endl<<"=======================Tree Display end========================================"<<endl;
	  }
};

double getEntries(TTree* t, TString cut = "", TString weightName = "", bool resetBranches = true){
	if(cut == "") cut = "1";
	if(weightName == "") return t->GetEntries(cut);
	if(resetBranches){
	  t->SetBranchStatus("*", 0);
	  setBranchStatusTTF(t, cut);
	  setBranchStatusTTF(t, weightName);
	}

	double sumW(0);
	TTreeFormula ttf("ttf", cut, t);
	TTreeFormula ttfW("ttfW", weightName, t);

	for(int i(0); i<t->GetEntries(); ++i){
	  t->GetEntry(i);
	  if(ttf.EvalInstance()) sumW+=ttfW.EvalInstance();
	}

	if(resetBranches) t->SetBranchStatus("*", 1);

	return sumW;
}
double getEntries(TString filename, TString cut = "", TString weightName = "", TString nametree = "DecayTree", bool resetBranches = true){
	TFile f(filename);
	TTree* t = (TTree*)f.Get(nametree);
	if(!t){
	  cout<<"WARNING: in getEntries, tree "<<nametree<<" in file "<<filename<<" not found, return 0"<<endl;
	  f.Close();
	  return 0;
	}
	double ret;
	ret = getEntries(t, cut, weightName, resetBranches);
	f.Close();
	return ret;
}

void divideByBinWidth(TH1* h){
	if(!h)
	{
	  cerr<<"ERROR in function divideByBinWidth, histogram not valid"<<endl;
	  return;
	}
	int nbins(h->GetNbinsX());
	double content;
	double error;

	for(int i(1); i<=nbins; ++i)
	{
	  content = h->GetBinContent(i);
	  error = h->GetBinError(i);
	  h->SetBinContent( i, content / h->GetXaxis()->GetBinWidth(i) );
	  h->SetBinError( i, error / h->GetXaxis()->GetBinWidth(i) );
	}
}

void addBranch(TString fileName, TString branchName, TString formulaStr){
	TFile file(fileName);
	TTree* tree = (TTree*)file.Get("DecayTree");
	if(!tree){
		cerr<< "ERROR: in addBranch: no 'DecayTree' under tuple: " << fileName << endl;
		return;
	}

	tree->SetBranchStatus("*", 1);
	tree->SetBranchStatus(branchName, 0);

	printf("Fill DecayTree in root file: %s\n", fileName.Data());
	printf("New branch name: %s\n", branchName.Data());
	printf("Branch formula: %s\n", formulaStr.Data());

	TTreeFormula branchTTF("treeBranchFormula", formulaStr, tree);

	TString fileNameTmp = fileName;
	fileNameTmp.Insert(fileNameTmp.Length()-5, "Tmp");
	TFile fTmp(fileNameTmp, "RECREATE");

	TTree* newTree = tree->CloneTree(0);

	double branchValue;
	newTree->Branch(branchName, &branchValue, branchName+"/D");

	int nEntries = tree->GetEntries();
	for(int i=0; i<nEntries; i++){
		tree->GetEntry(i);
		if(i % (nEntries/10) == 0) cout<<" "<<ceil(100*i/(1.*nEntries))<<"\% "<<flush;

		//printf("Entry %-8d Value: %.0lf\n", i, branchTTF.EvalInstance());
		branchValue = branchTTF.EvalInstance();
		newTree->Fill();
	}
	printf("\n");


	fTmp.cd();
	newTree->Write();


	file.Close();
	fTmp.Close();
	rename(fileNameTmp, fileName);
}
void addBranch(TString fileName, vector<TString> branchName, vector<TString> formulaStr){
	TFile file(fileName);
	TTree* tree = (TTree*)file.Get("DecayTree");
	if(!tree){
		cerr<< "ERROR: in addBranch: no 'DecayTree' under tuple: " << fileName << endl;
		return;
	}
	if(branchName.size() != formulaStr.size()){
 		cerr<< "ERROR: in addBranch: different size of branchName and formulaStr" << endl;
 		return;
	}

	tree->SetBranchStatus("*", 1);

	vector<TTreeFormula*> branchTTF(branchName.size());

	printf("Fill DecayTree in root file: %s\n", fileName.Data());
	for(int i=0; i<branchName.size(); i++){
		tree->SetBranchStatus(branchName[i], 0);
		printf("New branch name: %s\n", branchName[i].Data());
		//printf("Branch formula: %s\n", formulaStr[i].Data());
		branchTTF[i] = new TTreeFormula(("treeBranchFormula"+i2s(i)), formulaStr[i], tree);
	}

	TString fileNameTmp = fileName;
	fileNameTmp.Insert(fileNameTmp.Length()-5, "Tmp");
	TFile fTmp(fileNameTmp, "RECREATE");

	TTree* newTree = tree->CloneTree(0);

	vector<double> branchValue(branchName.size());
	for(int i=0; i<branchName.size(); i++){
		newTree->Branch(branchName[i], &branchValue[i], branchName[i]+"/D");
	}

	int nEntries = tree->GetEntries();
	for(int i=0; i<nEntries; i++){
		tree->GetEntry(i);
		if(i % (nEntries/10) == 0) cout<<" "<<ceil(100*i/(1.*nEntries))<<"\% "<<flush;

		//printf("Entry %-8d Value: %.0lf\n", i, branchTTF.EvalInstance());
		for(int i=0; i<branchName.size(); i++){
			branchValue[i] = branchTTF[i]->EvalInstance();
		}
		newTree->Fill();
	}
	printf("\n");


	fTmp.cd();
	newTree->Write();


	file.Close();
	fTmp.Close();
	rename(fileNameTmp, fileName);
}

void addBranch(vector<TString> fileNames, TString branchName, TString formulaStr){
	for(int i=0; i<fileNames.size(); i++){
		addBranch(fileNames[i], branchName, formulaStr);
	}
}


void addBranch(vector<TString> fileNames, vector<TString> branchName, vector<TString> formulaStr){
	for(int i=0; i<fileNames.size(); i++){
		addBranch(fileNames[i], branchName, formulaStr);
	}
}

void mergeTrees(TString targetName, TString treeFile1, TString treeFile2, TString treeFile3="", TString treeFile4="", TString treeFile5="", TString treeFile6="", TString treeFile7="", TString treeFile8="", TString treeFile9="", TString treeFile10="", TString treeFile11="", TString treeFile12=""){
	TChain chain("DecayTree", "DecayTree");
	if(treeFile1 != "") chain.Add( (treeFile1+"/DecayTree"));
	if(treeFile2 != "") chain.Add( (treeFile2+"/DecayTree"));
	if(treeFile3 != "") chain.Add( (treeFile3+"/DecayTree"));
	if(treeFile4 != "") chain.Add( (treeFile4+"/DecayTree"));
	if(treeFile5 != "") chain.Add( (treeFile5+"/DecayTree"));
	if(treeFile6 != "") chain.Add( (treeFile6+"/DecayTree"));
	if(treeFile7 != "") chain.Add( (treeFile7+"/DecayTree"));
	if(treeFile8 != "") chain.Add( (treeFile8+"/DecayTree"));
	if(treeFile9 != "") chain.Add( (treeFile9+"/DecayTree"));
	if(treeFile10 != "") chain.Add( (treeFile10+"/DecayTree"));
	if(treeFile11 != "") chain.Add( (treeFile11+"/DecayTree"));
	if(treeFile12 != "") chain.Add( (treeFile12+"/DecayTree"));

	chain.SetBranchStatus("*", 1);

	cout<<"merging..."<<endl;
	cout << "output: " << targetName << endl;
	chain.Merge(targetName);
}

void mergeTrees(TString targetName, vector<TString> fileNames, TString tupleName){
	TChain chain("DecayTree", "DecayTree");
	for(unsigned int i(0); i<fileNames.size(); ++i) chain.Add( (fileNames[i]+"/"+tupleName) );
	
	chain.SetBranchStatus("*", 1);

	cout<<"merging... "<<endl;
	cout << "output: " << targetName << endl;
	chain.Merge(targetName);
}

vector<double> getDataCondWeights(vector<double> entries){
		if(entries.size() != 8){
			cerr << "ERROR in getDataCondWeights: size of vector<double> error!" << endl;
			return vector<double>{};
		}
		vector<double> DataCondWeights(8, 0);

		/// Down + Up
		double lumi2015Down = 0.162388;
		double lumi2015Up	= 0.122232;
		double lumi2016Down = 0.842363;
		double lumi2016Up	= 0.777885;
		double lumi2017Down = 0.862052;
		double lumi2017Up	= 0.820601;
		double lumi2018Down = 1.024091;
		double lumi2018Up	= 1.107321;

		double allEntriees = 0;
		for(int i=0; i<8; i++){
				allEntriees += entries[i];
		}
		double allLumi = lumi2015Down + lumi2015Up + lumi2016Down + lumi2016Up + lumi2017Down + lumi2017Up + lumi2018Down + lumi2018Up;

		DataCondWeights[0] = (lumi2015Down * (allEntriees)) / (entries[0]*(allLumi));
		DataCondWeights[1] = (lumi2015Up	* (allEntriees)) / (entries[1]*(allLumi));
		DataCondWeights[2] = (lumi2016Down * (allEntriees)) / (entries[2]*(allLumi));
		DataCondWeights[3] = (lumi2016Up	* (allEntriees)) / (entries[3]*(allLumi));
		DataCondWeights[4] = (lumi2017Down * (allEntriees)) / (entries[4]*(allLumi));
		DataCondWeights[5] = (lumi2017Up	* (allEntriees)) / (entries[5]*(allLumi));
		DataCondWeights[6] = (lumi2018Down * (allEntriees)) / (entries[6]*(allLumi));
		DataCondWeights[7] = (lumi2018Up	* (allEntriees)) / (entries[7]*(allLumi));

		return DataCondWeights;
}
void MergeTreesWithWeights(vector<TString> listTrees, TString oldWeight, vector<double> newWeights, TString mergedWeightName, TString mergedtreename)
{
	unsigned int n(listTrees.size());
	//check if everything OK
	if(newWeights.size() != n){
	  cout<<"ERROR: in MergeTreesWithWeights: problem with the size in the arguments. Nothing is done."<<endl;
	  return;
	}
	//prepare a chain of trees that have to be copied


	TFile fnew(mergedtreename, "RECREATE");

	TChain chain;
	for(unsigned int i(0); i<n; ++i) chain.Add((listTrees[i] + "/DecayTree"));
	chain.SetBranchStatus("*", 1);
	chain.SetBranchStatus(mergedWeightName, 0);

	//set formulaTTF that compute the new weights
	if( oldWeight == "") oldWeight="1";
	TTreeFormula formulaTTF("formula", oldWeight, &chain);
	double formulaValue;

	//prepare the merged tree

	TTree* tnew = chain.CloneTree(0);
	double newWeight(0);
	tnew->Branch(mergedWeightName, &newWeight, (mergedWeightName+"/D"));

	//fill the merged tree
	
	cout<<"Merging tree..."<<endl;
	for(int i(0); i<chain.GetEntries(); ++i){
	  if(i % (chain.GetEntries()/10) == 0) cout<<100*i/(chain.GetEntries())<<" \% "<<endl;
	  chain.GetEntry(i);
	  formulaValue = formulaTTF.EvalInstance(i);
	  newWeight = formulaValue * newWeights[chain.GetTreeNumber()];
	  tnew->Fill();
	}
	cout<<endl;

	//save and erase everything
	
	tnew->Write("", TObject::kOverwrite);
	fnew.Close();
}

class ValError{
   public:

   double val;
   double err;

   ValError();
   ValError(double _val, double _err);
   ValError(ValError const& ve);


   //ValError& operator=(ValError const& ve);
   ValError& operator*=(ValError const& ve);
   ValError& operator*=(double alpha);
   ValError& operator/=(ValError const& ve);
   ValError operator*(ValError const& ve) const;
   ValError operator/(ValError const& ve) const;
   ValError& operator+=(ValError const& ve);
   ValError operator+(ValError const& ve);
   ValError& operator-=(ValError const& ve);
   ValError operator-(ValError const& ve);
   ValError& operator/=(double alpha);
   ValError operator/(double alpha);
   bool operator==(ValError const& ve) const;


   TString roundToError(int printOption = 0, int nfixed = -1) const; //0: for screen printing,
                                                         //1: for latex document printing
                                                         //2: for Rene Brun's piece of shit

};

ValError operator*(double scalar, ValError const& ve);

ostream& operator<<(ostream&, ValError const&);
void outLatex(ostream& out, ValError const&);


ValError sqrt(ValError ve);
TString roundToError(ValError ve);
ValError averageValError(ValError va1, ValError va2 = ValError(), ValError va3 = ValError(), ValError va4 = ValError(),
      ValError va5 = ValError(), ValError va6 = ValError(), ValError va7 = ValError(), ValError va8 = ValError(), ValError va9 = ValError());
ValError averageValError(vector<ValError> vecVa, int mode = 1); //1: err is variance 2: err is estimated error on the mean
ValError getRatio(ValError a, ValError b);
ValError getRatioBinomial(ValError a, ValError b);
ValError getRatioWeightedBinomial(ValError pass, ValError tot);
ValError getProduct(ValError a, ValError b);
ValError getMeanWithStdDev(vector<double> vals);
ValError getMeanWithStdDev(vector<ValError> vals);
double getStdDevWithKnownMean(vector<ValError> vals, double mean);
double getStdDevWithKnownMean(vector<double> vals, double mean);
double getCovarianceWithKnownMean(vector<ValError> xs, vector<ValError> ys, double x0, double y0, bool wantUncertaintiesOnDiagonal);
double getCovarianceWithKnownMean(vector<double> xs, vector<double> ys, double x0, double y0, vector<double> dxs);
double getCovariance(vector<ValError> xs, vector<ValError> ys);
double getCovariance(vector<double> xs, vector<double> ys);


TString ValError::roundToError(int printOption, int nfixed) const
{
   TString ret;
   if(nfixed == 0){
      if(printOption == 0) ret = dbl2str(val)+"+-"+dbl2str(err);
      if(printOption == 1) ret = "$"+dbl2str(val)+"\\pm"+dbl2str(err)+"$";
      if(printOption == 2) ret = dbl2str(val)+"#pm"+dbl2str(err);
      return ret;
   }

   double err2(err);
   double val2(val);

   if(nfixed < 0){
      int power(floor(TMath::Log10(err2)));
      double todivide(TMath::Power(10, power-2));
      int err3dig(TMath::Nint(err2/todivide));

      if(err3dig<=354 || err3dig>=950){
         todivide = TMath::Power(10, power-1);
         nfixed = 1-power;
      }
      if(err3dig>=355 && err3dig<=949){
         todivide = TMath::Power(10, power);
         nfixed = 0-power;
      }

      err2 = todivide*TMath::Nint(err2/todivide);
      val2 = todivide*TMath::Nint(val2/todivide);
   }


   if(printOption == 0) ret = dbl2str(val2, nfixed)+"+-"+dbl2str(err2, nfixed);
   if(printOption == 1) ret = "$"+dbl2str(val2, nfixed)+"\\pm"+dbl2str(err2, nfixed)+"$";
   if(printOption == 2) ret = dbl2str(val2, nfixed)+"#pm"+dbl2str(err2, nfixed);
   return ret;
}




ValError::ValError()
:val(0), err(0)
{}

ValError::ValError(double _val, double _err)
:val(_val), err(_err)
{}

ValError::ValError(ValError const& ve)
:val(ve.val), err(ve.err)
{}


ValError& ValError::operator*=(ValError const& ve)
{
   err = sqrt(ve.val*ve.val*err*err + val*val*ve.err*ve.err);
   val *= ve.val;
   return *this;
}

ValError& ValError::operator*=(double alpha)
{
   err *= alpha;
   val *= alpha;
   return *this;
}

ValError& ValError::operator/=(double alpha)
{
   err /= alpha;
   val /= alpha;
   return *this;
}

ValError& ValError::operator/=(ValError const& veb)
{
   err = sqrt( veb.val*veb.val*err*err + val*val*veb.err*veb.err ) / (veb.val*veb.val);
   val = val/veb.val;
   return *this;
}

ValError ValError::operator*(ValError const& ve) const
{
   ValError ret(*this);
   ret *= ve;
   return ret;
}
ValError ValError::operator/(ValError const& ve) const
{
   ValError ret(*this);
   ret /= ve;
   return ret;
}


bool ValError::operator==(ValError const& ve) const
{
   if(val == ve.val && err == ve.err) return true;
   return false;
}

ValError operator*(double scalar, ValError const& ve)
{
   ValError ret(ve);
   ret *= scalar;
   return ret;
}

ostream& operator<<(ostream& out, ValError const& ve)
{
   out<<ve.roundToError(0, -1);
   return out;
}


ValError& ValError::operator+=(ValError const& ve)
{
   val += ve.val;
   err = sqrt(err*err + ve.err*ve.err);
   return *this;
}


ValError ValError::operator+(ValError const& ve)
{
   ValError ret(*this);
   ret += ve;
   return ret;
}


ValError& ValError::operator-=(ValError const& ve)
{
   val -= ve.val;
   err = sqrt(err*err + ve.err*ve.err);
   return *this;
}


ValError ValError::operator-(ValError const& ve)
{
   ValError ret(*this);
   ret -= ve;
   return ret;
}

ValError ValError::operator/(double alpha)
{
   ValError ret(*this);
   ret /= alpha;
   return ret;
}

void outLatex(ostream& out, ValError const& ve)
{
   out<<ve.roundToError(1, -1);
}



ValError averageValError(vector<ValError> vecVa, int mode){
   double sumInverseSquaredErrors(0);
   double sumWeightedValues(0);
   double sumWeightedValueSqd(0);

   for(unsigned int i(0); i<vecVa.size(); ++i){
      if(vecVa[i].val < DBL_MAX && vecVa[i].val > -DBL_MAX && vecVa[i].err < DBL_MAX && vecVa[i].err > DBL_MIN){
         sumInverseSquaredErrors += 1. / (vecVa[i].err*vecVa[i].err) ;
         sumWeightedValues += vecVa[i].val / (vecVa[i].err*vecVa[i].err) ;
         sumWeightedValueSqd += vecVa[i].val*vecVa[i].val / (vecVa[i].err*vecVa[i].err) ;
      }
   }

   ValError va;

   va.val = sumWeightedValues / sumInverseSquaredErrors;
   va.err = sqrt( sumWeightedValueSqd/sumInverseSquaredErrors - va.val*va.val  );
   if(mode == 2) va.err = va.err / sqrt(sumInverseSquaredErrors);

   return va;
}

ValError averageValError(ValError va0, ValError va1, ValError va2, ValError va3, ValError va4, ValError va5, ValError va6, ValError va7, ValError va8, ValError va9)
{
   ValError tabVa[10] = {va0, va1, va2, va3, va4, va5, va6, va7, va8, va9};

   vector<ValError> vecVa;
   for(int i(0); i<10; ++i)
   {
      if(tabVa[i].err >= 0)  vecVa.push_back(tabVa[i]);
   }
   return averageValError(vecVa);
}

ValError sqrt(ValError ve)
{
   ValError ret;
   ret.val = sqrt(ve.val);
   ret.err = ve.err*0.5/sqrt(ve.val);
   return ret;
}


ValError getRatioBinomial(ValError a, ValError b)
{
   double eff(a.val/b.val);
   ValError ret;
   ret.val = eff;
   ret.err = sqrt((1/b.val) * eff*(1-eff));
   return ret;
}

ValError getRatio(ValError a, ValError b)
{
   return a/b;
}

ValError getRatioWeightedBinomial(ValError pass, ValError tot)
{
   if(pass.val == 0 && tot.val == 0) return ValError(0,0);

   ValError ret;
   ret.val = pass.val/tot.val;

   ret.err =
      sqrt( TMath::Abs(  ((1.-2* pass.val/tot.val)*pass.err*pass.err + pass.val*pass.val*tot.err*tot.err / (tot.val*tot.val) ) / ( tot.val*tot.val  ) ) )  ;
   return ret;

}

ValError getProduct(ValError a, ValError b)
{
   return a*b;
}

TString roundToError(ValError ve)
{
   return ve.roundToError(0, -1);
}

ValError getMeanWithStdDev(vector<double> vals)
{
   ValError ret(0,0);
   unsigned int n(vals.size());
   if(n == 0) return ret;
   if(n == 1)
   {
      ret.val = vals.at(0);
      return ret;
   }

   double nd(1.*n);
   double sum2(0);
   double sum(0);

   for(unsigned int i(0); i < n; ++i)
   {
      sum += vals.at(i);
      sum2 += vals.at(i) * vals.at(i);
   }

   ret.val = sum/nd;

   double err2(0);

   err2 = (1/(nd-1.))*sum2 - (1./((nd-1.)*nd))*sum*sum;
   if(err2 < 1e-13 ) ret.err = 0.;
   if(err2 >= 1e-13) ret.err = sqrt(err2);

   return ret;
}


ValError getMeanWithStdDev(vector<ValError> vals)
{
   vector<double> valsp(vals.size());

   for(unsigned int i(0); i < vals.size(); ++i) valsp.at(i) = vals.at(i).val;

   return getMeanWithStdDev(valsp);
}

double getStdDevWithKnownMean(vector<ValError> vals, double mean)
{
   vector<double> valsp(vals.size() );

   for(unsigned int i(0); i < vals.size(); ++i) valsp.at(i) = vals.at(i).val;

   return getStdDevWithKnownMean(valsp, mean);
}

double getStdDevWithKnownMean(vector<double> vals, double mean)
{
   unsigned int n(vals.size());
   if(n == 0) return 0.;

   double sumDevs(0.);

   for(unsigned int i(0); i < n; ++i) sumDevs += (vals.at(i)-mean)*(vals.at(i)-mean);

   return sqrt( sumDevs/(1.*n) );
}


double getCovarianceWithKnownMean(vector<ValError> xs, vector<ValError> ys, double x0, double y0, bool wantUncertaintiesOnDiagonal){
   if(xs.size() != ys.size()){
      cerr<<"ERROR: in getCovarianceWithKnownMean, mismatch in sample size"<<endl; 
      return -1;
   }

   unsigned int n(xs.size()); 

   vector<double> xsp(xs.size());
   vector<double> ysp(ys.size());
   vector<double> dxsp(xs.size(), 0.);

   for(unsigned int i(0); i<n; ++i){
      xsp.at(i) = xs.at(i).val;
      ysp.at(i) = ys.at(i).val;
      if(wantUncertaintiesOnDiagonal) dxsp.at(i) = xs.at(i).err;
   }
   
   return getCovarianceWithKnownMean(xsp, ysp, x0, y0, dxsp);
}

double getCovarianceWithKnownMean(vector<double> xs, vector<double> ys, double x0, double y0, vector<double> dxs){
   if(xs.size() != ys.size()){
      cerr<<"ERROR: in getCovarianceWithKnownMean, mismatch in sample size"<<endl; 
      return -1;
   }

   unsigned int n(xs.size()); 
   
   double sumProd(0.);

   for(unsigned int i(0); i<n; ++i){
       sumProd += (xs.at(i)-x0) * (ys.at(i)-y0);
       if(xs.at(i) == ys.at(i) && dxs.size()) sumProd += dxs.at(i) * dxs.at(i);
   }

   return sumProd / (1.*n);
}


double getCovariance(vector<ValError> xs, vector<ValError> ys){
   if(xs.size() != ys.size()){
      cerr<<"ERROR: in getCovarianceWithKnownMean, mismatch in sample size"<<endl;
      return -1;
   }

   unsigned int n(xs.size());

   vector<double> xsp(xs.size());
   vector<double> ysp(ys.size());

   for(unsigned int i(0); i<n; ++i){
      xsp.at(i) = xs.at(i).val;
      ysp.at(i) = ys.at(i).val;
   }

   return getCovariance(xsp, ysp);
}


double getCovariance(vector<double> xs, vector<double> ys){
   if(xs.size() != ys.size()){
      cerr<<"ERROR: in getCovariance, mismatch in sample size"<<endl;
      return -1;
   }

   unsigned int n(xs.size());

   double sumProd(0.);
   double x0(0.);
   double y0(0.);


   for(unsigned int i(0); i<n; ++i){
      x0 += xs.at(i);
      y0 += ys.at(i);
   }

   x0 = x0/(1.*n);
   y0 = y0/(1.*n);

   for(unsigned int i(0); i<n; ++i) sumProd += (xs.at(i)-x0) * (ys.at(i)-y0);

   return sumProd / (1.*n - 1.);
}

ValError getEntriesWithError(TTree* t, TString cut, TString weightName){
	if(cut == "") cut = "1";
	if(weightName == "") weightName = "1";
	t->SetBranchStatus("*", 0);
	setBranchStatusTTF(t, cut);
	setBranchStatusTTF(t, weightName);
	double w(0);
	double sumW(0);
	double sumW2(0);

	TTreeFormula ttf("ttf", cut, t);
	TTreeFormula ttfWeight("ttfWeight", weightName, t );

	for(int i(0); i<t->GetEntries(); ++i)
	{
		t->GetEntry(i);
		if(ttf.EvalInstance()) 
		{
			w = ttfWeight.EvalInstance();
			sumW+=w;
			sumW2+= w*w;
		}
	}

	t->SetBranchStatus("*", 1);
	return ValError(sumW, sqrt(sumW2));
}
ValError getEntriesWithError(TString filename, TString cut, TString weightName, TString nametree = "DecayTree"){
	TFile f(filename);
	TTree* t = (TTree*)f.Get(nametree);
	if(!t){
		cout<<"WARNING: in getEntriesWithError, tree "<<nametree<<" in file "<<filename<<" not found, return 0"<<endl;
		f.Close();
		return ValError(0,0);
	}
	ValError ret;
	ret = getEntriesWithError(t, cut, weightName);
	f.Close();
	return ret;
}
bool compareElementForUniquingByEventTag(ElementForUniquing& efu1, ElementForUniquing& efu2){
   if(efu1.eventTag.polarity != efu2.eventTag.polarity) return efu1.eventTag.polarity < efu2.eventTag.polarity;
   if(efu1.eventTag.runNumber != efu2.eventTag.runNumber) return efu1.eventTag.runNumber < efu2.eventTag.runNumber;
   return efu1.eventTag.eventNumber < efu2.eventTag.eventNumber;
}
ValError countUniqueEvents(list<ElementForUniquing>& listElements){
	listElements.sort(compareElementForUniquingByEventTag);
	
	list<ElementForUniquing>::iterator lastIt;
	list<ElementForUniquing>::iterator listIt;
	list<ElementForUniquing>::iterator firstIt;
	list<ElementForUniquing>::iterator nextIt;
	list<ElementForUniquing>::iterator checkIt;
	list<ElementForUniquing>::iterator beforeLastIt;
	
	
	for(listIt = listElements.begin(); listIt != listElements.end(); ++listIt) listIt->toRemove = false;
	
	double maxVar(-DBL_MAX);
	
	cout<<"tagging double events... "<<flush;
	
	beforeLastIt = listElements.end();
	--beforeLastIt;
	
	for(listIt = listElements.begin(); listIt != beforeLastIt && lastIt != listElements.end(); ++listIt){
		nextIt = listIt;
		++nextIt;
		
		if(nextIt->eventTag == listIt->eventTag){
			maxVar = listIt->varClassification;
			firstIt = listIt;
			
			lastIt = firstIt; 
			while(lastIt->eventTag == firstIt->eventTag && lastIt != listElements.end()){
				if(lastIt->varClassification > maxVar) maxVar = lastIt->varClassification;
				++lastIt;
			}

			vector<int> doubleElements;
			//doubleElements.push_back(listIt->numInTree);

			int nCandidates = 0;
			// radomly decide which to retain
			for(checkIt = firstIt; checkIt != lastIt; ++checkIt){
				doubleElements.push_back(checkIt->numInTree);
				nCandidates ++;
			}

			TRandom3 rand;
			int entryRetain = doubleElements.at(rand.Integer(nCandidates));

			for(checkIt = firstIt; checkIt != lastIt; ++checkIt){
				if(checkIt->passCut){
					if(checkIt->numInTree != entryRetain) checkIt->toRemove = true;
				}
				//if(checkIt->varClassification  < maxVar && checkIt->passCut) checkIt->toRemove = true;
				//if(checkIt->varClassification == maxVar && checkIt->passCut) maxVar += 1.;
			}
			
			listIt = lastIt;
		}
	}
	cout<<"done!"<<endl;
	
	ValError sumW(0., 0.);
	double w;
	
	firstIt = listElements.begin();
	lastIt = listElements.end();
	
	for(checkIt = firstIt; checkIt != lastIt; ++checkIt)
	{
	   if(!checkIt->toRemove && checkIt->passCut)
	   {
	      w = checkIt->weight;
	      sumW.val += w;
	      sumW.err += w*w;
	   }
	}
	
	sumW.err = sqrt(sumW.err);
	
	return sumW;
}
int getBeautifulColour(int i){
	if(i % 10 == 0) return 1;  
	if(i % 10 == 1) return 2;
	if(i % 10 == 2) return 3;
	if(i % 10 == 3) return 4;
	if(i % 10 == 4) return 5;
	if(i % 10 == 5) return 6;
	
	if(i % 10 == 6) return 7;
	if(i % 10 == 7) return 9;
	if(i % 10 == 8) return 34;
	if(i % 10 == 9) return 48;
	return 1;
}
vector<int> getTenBeautifulColours(){
	vector<int> ret(10, 0);
	
	ret.at(0) = 1;
	ret.at(1) = 2;
	ret.at(2) = 3;
	ret.at(3) = 4;
	ret.at(4) = 5;
	ret.at(5) = 6;
	
	ret.at(6) = 7;
	ret.at(7) = 9;
	ret.at(8) = 34;
	ret.at(9) = 48;
	
	return ret;
}
void saveStringInFile(TString toWrite, TString nameFile){
	ofstream out( nameFile );
	if(!out.is_open()){
		cerr<<"ERROR: in saveStringInFile, impossible to open the file"<<endl;
		out.close();
	}
	
	out<<toWrite;
	out.close();
}
TString readSingleLineFile(TString nameFile){
   ifstream in( nameFile );

   if(in.fail()){
      cerr<<"ERROR: in readSingleLineFile, impossible to read the file"<<endl;
      in.close();
      return "";
   }

   string ret;
   getline(in, ret);
   in.close();
   return ret;
}
void printEffToFile(vector<ValError> eff, vector<TString> files){
	int size = files.size();
	if(eff.size() != size){
		cerr << "ERROR in printEffToFile, different size" << endl;
		return;
	}
	for(int i=0; i<size; i++){
		TString str = eff[i].roundToError(0, 6);
		saveStringInFile(str, files[i]);
	}
}
ValError strToValError(TString TStr){
	ValError vr;

	string str = TStr.Data();
	int pos = str.find_first_of("+-");
	vr.val = s2d(str.substr(0, pos));
	vr.err = s2d(str.substr(pos+2, str.size()-(pos+2)));

	return vr;
}
//ValError getEffFromFiles(TString file){
//    ValError eff;
//    TString line = readSingleLineFile(file);
//    eff = strToValError(line);
//
//    return eff;
//}
//vector<ValError> getEffFromFiles(vector<TString> files){
//	int nSize = files.size();
//	vector<ValError> eff(nSize);
//	for(int i=0; i<nSize; i++){
//		eff[i] = getEffFromFiles(files[i]);
//	}
//
//	return eff;
//}
void makePull(TH1* pull, TH1 const& h1, TH1 const& h2, bool wantUniform)
{
   if(!pull)
   {
      cerr<<"ERROR: in makePull, pointer not valid"<<endl;
      return;
   }

   //set the bins correctly

   pull->Reset();

   int nBins(h1.GetNbinsX());
   if(wantUniform) pull->SetBins(nBins, 0, 1);

   if(!wantUniform)
   {
      double binTabs[nBins+1];

      for(int i(0); i<=nBins; ++i) binTabs[i] = h1.GetXaxis()->GetBinUpEdge(i);

      pull->SetBins(nBins, binTabs);
   }

   //fill the histo

   ValError ve1;
   ValError ve2;
   ValError delta;

   for(int i(1); i<=nBins; ++i)
   {
      ve1.val = h1.GetBinContent(i);
      ve2.val = h2.GetBinContent(i);
      ve1.err = h1.GetBinError(i);
      ve2.err = h2.GetBinError(i);
      delta = ve2-ve1;
      if(delta.err != 0)
      {
         pull->SetBinContent(i, delta.val/delta.err);
         pull->SetBinError(i, 1.);
      }
      else
      {
         pull->SetBinContent(i, 1);
         pull->SetBinError(i, 0);
      }
   }

   pull->GetYaxis()->SetTitle("pull");
}

void copyTreeWithNewVar(TTree*& tNew, TTree* tOld, TString cut, TString formula, TString newVarName){
   vector<TString> formulas;
   vector<TString> newVarNames;

   if(formula != "" && newVarName != ""){
      formulas.push_back(formula);
      newVarNames.push_back(newVarName);
   }

   copyTreeWithNewVars(tNew, tOld, cut, formulas, newVarNames);
}


void copyTreeWithNewVars(TString fileNewName, TString fileOldName, TString cut, TString formula, TString newVarName, vector<TString> varsToSwitchOn, TString tOldName){
   vector<TString> formulas(1, formula);
   vector<TString> newVarNames(1, newVarName);

   copyTreeWithNewVars(fileNewName, fileOldName, cut, formulas, newVarNames, varsToSwitchOn, tOldName);
}

void copyTreeWithNewVars(TString fileNewName, TString fileOldName, TString cut, vector<TString> formulas, vector<TString> newVarNames, vector<TString> varsToSwitchOn, TString tOldName){
   TFile fOld(fileOldName);
   TTree* tOld = (TTree*)fOld.Get(tOldName);

   if(!tOld){
      cerr<<"ERROR: in copyTreeWithNewVars, tOld not found"<<endl;
      return;
   }


   cout<<endl<<"======================================================================="<<endl;
   cout<<"Errors from TTreeFormula do not matter from here"<<endl;
   if(varsToSwitchOn.size() > 0){
      tOld->SetBranchStatus("*", 0);
      for(unsigned int i(0); i<varsToSwitchOn.size(); ++i){
         setBranchStatusTTF(tOld, varsToSwitchOn.at(i));
         if(varsToSwitchOn.at(i).Contains("*")) tOld->SetBranchStatus(varsToSwitchOn.at(i), 1);
      }
   }
   cout<<"From now on errors matter"<<endl;
   cout<<"======================================================================="<<endl<<endl;

   TFile fNew(fileNewName, "RECREATE");
   TTree* tNew(NULL);

   copyTreeWithNewVars(tNew, tOld, cut, formulas, newVarNames);

   fNew.cd();
   tNew->Write("", TObject::kOverwrite);

   fNew.Close();
   fOld.Close();

   cout<<"Tree saved in file "<<fileNewName<<endl;
}

void copyTreeWithNewVars(TTree*& tNew, TTree* tOld, TString cut, vector<TString> formulas, vector<TString> newVarNames){
   if(formulas.size() != newVarNames.size()){
      cerr<<"ERROR: in copyTreeWithNewVar, mismatch. Abort."<<endl;
      return;
   }

   int nVars(formulas.size());

   setBranchStatusTTF(tOld, cut);
   for(int i(0); i<nVars; ++i) setBranchStatusTTF(tOld, formulas.at(i));

   if(cut == "") cut = "1";
   TTreeFormula cutTTF("cutTTF", cut, tOld);

   vector<TTreeFormula*> varTTFs(nVars, NULL);
   for(int i(0); i<nVars; ++i) varTTFs[i] = new TTreeFormula(("varTTF"+i2s(i)), formulas.at(i), tOld);

   cout<<endl<<"======================================================================="<<endl;
   cout<<"Errors from TTreeFormula do not matter from here"<<endl;
   for(int i(0); i<nVars; ++i) tOld->SetBranchStatus(newVarNames.at(i), 0);
   cout<<"From now on errors matter"<<endl;
   cout<<"======================================================================="<<endl<<endl;


   tNew = tOld->CloneTree(0);

   vector<double> newVarVals(nVars, 0.);
   for(int i(0); i<nVars; ++i) tNew->Branch(newVarNames.at(i), &newVarVals.at(i), (newVarNames.at(i)+"/D") );

   cout<<"Filling tree with new var... "<<flush;

   int mod(tOld->GetEntries()/10);

   for(int i(0); i<tOld->GetEntries(); ++i){
      if( i % mod == 1) cout<<(100*i)/tOld->GetEntries()<<"/% "<<flush;

      tOld->GetEntry(i);

      if(cutTTF.EvalInstance() ){
         for(unsigned int i(0); i<varTTFs.size(); ++i) newVarVals.at(i) = varTTFs.at(i)->EvalInstance();
         tNew->Fill();
      }
   }

   cout<<endl<<"done! New tree has: "<<tNew->GetEntries()<<" entries."<<endl;

   for(int i(0); i<nVars; ++i){
      if(varTTFs.at(i) != NULL) delete varTTFs.at(i);
   }
}

void reduceTreeByFactor(double factor, TString nameFile, TString nameReducedFile, TString nameTree)
{
   TFile f(nameFile);
   TTree* t = (TTree*)f.Get(nameTree);

   if(!t)
   {
      cerr<<"ERROR in reduceTreeByFactor: no tree found, do nothing"<<endl;
   }

   TRandom3 rand;

   TFile f2(nameReducedFile, "RECREATE");
   TTree* t2 = t->CloneTree(0);
   for(int i(0); i<t->GetEntries(); ++i)
   {
      t->GetEntry(i);
      if(rand.Uniform(0., factor) < 1.) t2->Fill();
   }

   t2->Write("", TObject::kOverwrite);

   f2.Close();
   f.Close();
}

void fillListElements(list<ElementForUniquing>& listElements, TString fileName, TString cut, TString weightName, 
      TString varClassification, short fixPolarity, TString treeName)
{
   TFile f( fileName );
   TTree* t = (TTree*)f.Get( treeName );

   if( t == NULL )
   {
      cerr<<"ERROR: in fillListElements, no tree found"<<endl;
      f.Close();
      return; 
   }

   //set the branches

   unsigned int runNumber;
   unsigned long long int eventNumber;
   short polarity(0);
   double var(0.);
   double weight(0.);

   if(cut == "") cut = "(1.)";
   if(weightName == "") weightName = "(1.)";
   if(varClassification == "") varClassification = "(1.)";

   t->SetBranchStatus("*", 0);
   setBranchStatusTTF(t, cut);
   setBranchStatusTTF(t, weightName);
   setBranchStatusTTF(t, varClassification);
   t->SetBranchStatus("runNumber", 1);
   t->SetBranchStatus("eventNumber", 1);
   if(fixPolarity > -10) t->SetBranchStatus("Polarity", 1);

   TTreeFormula cutTTF("cutTTF", cut, t);
   TTreeFormula weightTTF("weightTTF", weightName, t);
   TTreeFormula varTTF("varTTF", varClassification, t);
   t->SetBranchAddress("runNumber", &runNumber);
   t->SetBranchAddress("eventNumber", &eventNumber);
   if(fixPolarity > -10) t->SetBranchAddress("Polarity", &polarity);

   //fill the list

   ElementForUniquing element; 
   element.passCut = true;
   element.toRemove = false;
   element.binVar = 0.;

   cout<<"filling list from tree... "<<flush;

   for(int i(0); i<t->GetEntries(); ++i)
   {
      if(i % (t->GetEntries()/10) == 0) cout<<100*i/(t->GetEntries())<<" \% "<<flush;

      t->GetEntry(i); 

      if(cutTTF.EvalInstance())
      {
         weight = weightTTF.EvalInstance();
         var = varTTF.EvalInstance();
         
         element.eventTag.runNumber = runNumber;
         element.eventTag.eventNumber = eventNumber;
         element.eventTag.polarity = polarity;
         element.varClassification = var;
         element.numInTree = i;
         element.weight = weight;

         listElements.push_back(element);
      }
   }

   cout<<"done!"<<endl;
   f.Close();
}

void gaussOscillateHisto(TH3* hist)
{
   TRandom rand;

   double val(0); 
   double err(0);
   double newVal(0);

   for(int i(1); i<=hist->GetNbinsX(); ++i)
   {
      for(int j(1); j<=hist->GetNbinsX(); ++j)
      {
         for(int k(1); k<=hist->GetNbinsX(); ++k)
         {
            val = hist->GetBinContent(i,j,k);
            err = hist->GetBinError(i,j,k);

            newVal = rand.Gaus(val, err); 

            hist->SetBinContent(i, j, k, newVal);
         }
      }
   }
}

void saveVectorInFile(vector<double> const& vec, TString fileName, TString vecName)
{
   TVector vec2(vec.size());
   
   for(unsigned int i(0); i<vec.size(); ++i) vec2(i) = vec.at(i);

   TFile f(fileName, "UPDATE");
   vec2.Write(vecName, TObject::kOverwrite);
   f.Close();
}

void readVectorFromFile(vector<double>& vec, TString fileName, TString vecName)
{
   TFile f(fileName);
   TVector* vec2 = (TVector*)f.Get(vecName );

   if(!vec2)
   {
      cerr<<"ERROR: in readVectorFromFile, no vector found"<<endl;
      f.Close();
      return;
   }

   vec.resize(vec2->GetNrows());

   for(int i(0); i < vec2->GetNrows(); ++i) vec.at(i) = (*vec2)(i);

   f.Close();
}

void saveVectorInFile(vector<ValError> const& vec, TString fileName, TString vecName)
{
   TMatrix matrix(2, vec.size());
   for(unsigned int i(0); i < vec.size(); ++i)
   {
      matrix(0, i) = vec.at(i).val;
      matrix(1, i) = vec.at(i).err;
   }
   
   TFile f(fileName, "UPDATE");
   matrix.Write(vecName, TObject::kOverwrite);
   f.Close();
}

void readVectorFromFile(vector<ValError>& vec, TString fileName, TString vecName)
{
   TFile f(fileName);
   TMatrix* matrix = (TMatrix*)f.Get(vecName );

   if(!matrix)
   {
      cerr<<"ERROR: in readVectorFromFile, no vector found"<<endl;
      f.Close();
      return;
   }

   if(matrix->GetNrows() != 2)
   {
      cerr<<"ERROR: in readVectorFromFile, matrix has wrong number of rows: matrix.GetNrows()"<<endl;
      f.Close();
      return;
   }

   vec.resize(matrix->GetNcols());

   for(int i(0); i < matrix->GetNcols(); ++i) 
   { 
      vec.at(i).val = (*matrix)(0, i);
      vec.at(i).err = (*matrix)(1, i);
   }

   f.Close();
}

ValError getValErrorFromFile(TString fileName, TString histoName, int whichBin){
	ValError eff(0, 0);

	TFile f(fileName);
	TH1D* histo = (TH1D*)f.Get(histoName);

	if(!histo){
		cerr << "No histo '" << histoName << "' found in file: " << fileName << endl;
		return eff;
	}

	eff.val = histo->GetBinContent(whichBin);
	eff.err = histo->GetBinError(whichBin);

	f.Close();
	return eff;
}
//  mdoe == 1, Bd2DsD
//          2, Bd2LcLc
//          3, Bs2DsDs
//          4, Bs2LcLc
void addCombineMassVars(TString fileName, int mode)
{
	TFile f(fileName);
	TTree* t;

	t = (TTree*)f.Get("DecayTree");

	if(!t) cerr<<"ERROR: in addCombineMassVars, no tree found"<<endl;

	TString fileNameTmp = fileName+".Tmp";

	// Preparing extra variables
	unsigned long long int eventNumber;
	t->SetBranchAddress("eventNumber", &eventNumber);

	// B2LcLc
	TVector3 p_Xc_plus_P, Kminus_Xc_plus_P, piplus_Xc_plus_P, pbar_Xc_minus_P, Kplus_Xc_minus_P, piminus_Xc_minus_P;
	double p_Xc_plus_E, Kminus_Xc_plus_E, piplus_Xc_plus_E, pbar_Xc_minus_E, Kplus_Xc_minus_E, piminus_Xc_minus_E;

	// Bs2DsD
	TVector3 Kminus_Dplus_P, piplus1_Dplus_P, piplus2_Dplus_P;
	double Kminus_Dplus_E, piplus1_Dplus_E, piplus2_Dplus_E;
	// Bs2DsDs
	TVector3 Kminus_Dsplus_P, Kplus_Dsplus_P, piplus_Dsplus_P, Kplus_Dsminus_P, Kminus_Dsminus_P, piminus_Dsminus_P;
	double Kminus_Dsplus_E, Kplus_Dsplus_E, piplus_Dsplus_E, Kplus_Dsminus_E, Kminus_Dsminus_E, piminus_Dsminus_E;


	if(mode == 1){
		t->SetBranchAddress("Kminus_Dplus_PX",&Kminus_Dplus_P[0]);
		t->SetBranchAddress("Kminus_Dplus_PY",&Kminus_Dplus_P[1]);
		t->SetBranchAddress("Kminus_Dplus_PZ",&Kminus_Dplus_P[2]);
		t->SetBranchAddress("Kminus_Dplus_PE",&Kminus_Dplus_E);

		t->SetBranchAddress("piplus1_Dplus_PX",&piplus1_Dplus_P[0]);
		t->SetBranchAddress("piplus1_Dplus_PY",&piplus1_Dplus_P[1]);
		t->SetBranchAddress("piplus1_Dplus_PZ",&piplus1_Dplus_P[2]);
		t->SetBranchAddress("piplus1_Dplus_PE",&piplus1_Dplus_E);

		t->SetBranchAddress("piplus2_Dplus_PX",&piplus2_Dplus_P[0]);
		t->SetBranchAddress("piplus2_Dplus_PY",&piplus2_Dplus_P[1]);
		t->SetBranchAddress("piplus2_Dplus_PZ",&piplus2_Dplus_P[2]);
		t->SetBranchAddress("piplus2_Dplus_PE",&piplus2_Dplus_E);

		t->SetBranchAddress("Kplus_Dsminus_PX",&Kplus_Dsminus_P[0]);
		t->SetBranchAddress("Kplus_Dsminus_PY",&Kplus_Dsminus_P[1]);
		t->SetBranchAddress("Kplus_Dsminus_PZ",&Kplus_Dsminus_P[2]);
		t->SetBranchAddress("Kplus_Dsminus_PE",&Kplus_Dsminus_E);

		t->SetBranchAddress("Kminus_Dsminus_PX",&Kminus_Dsminus_P[0]);
		t->SetBranchAddress("Kminus_Dsminus_PY",&Kminus_Dsminus_P[1]);
		t->SetBranchAddress("Kminus_Dsminus_PZ",&Kminus_Dsminus_P[2]);
		t->SetBranchAddress("Kminus_Dsminus_PE",&Kminus_Dsminus_E);

		t->SetBranchAddress("piminus_Dsminus_PX",&piminus_Dsminus_P[0]);
		t->SetBranchAddress("piminus_Dsminus_PY",&piminus_Dsminus_P[1]);
		t->SetBranchAddress("piminus_Dsminus_PZ",&piminus_Dsminus_P[2]);
		t->SetBranchAddress("piminus_Dsminus_PE",&piminus_Dsminus_E);
	}
	
	if(mode == 2 || mode == 4){
		t->SetBranchAddress("p_Xc_plus_PX",&p_Xc_plus_P[0]);
		t->SetBranchAddress("p_Xc_plus_PY",&p_Xc_plus_P[1]);
		t->SetBranchAddress("p_Xc_plus_PZ",&p_Xc_plus_P[2]);
		t->SetBranchAddress("p_Xc_plus_PE",&p_Xc_plus_E);

		t->SetBranchAddress("Kminus_Xc_plus_PX",&Kminus_Xc_plus_P[0]);
		t->SetBranchAddress("Kminus_Xc_plus_PY",&Kminus_Xc_plus_P[1]);
		t->SetBranchAddress("Kminus_Xc_plus_PZ",&Kminus_Xc_plus_P[2]);
		t->SetBranchAddress("Kminus_Xc_plus_PE",&Kminus_Xc_plus_E);

		t->SetBranchAddress("piplus_Xc_plus_PX",&piplus_Xc_plus_P[0]);
		t->SetBranchAddress("piplus_Xc_plus_PY",&piplus_Xc_plus_P[1]);
		t->SetBranchAddress("piplus_Xc_plus_PZ",&piplus_Xc_plus_P[2]);
		t->SetBranchAddress("piplus_Xc_plus_PE",&piplus_Xc_plus_E);

		t->SetBranchAddress("pbar_Xc_minus_PX",&pbar_Xc_minus_P[0]);
		t->SetBranchAddress("pbar_Xc_minus_PY",&pbar_Xc_minus_P[1]);
		t->SetBranchAddress("pbar_Xc_minus_PZ",&pbar_Xc_minus_P[2]);
		t->SetBranchAddress("pbar_Xc_minus_PE",&pbar_Xc_minus_E);

		t->SetBranchAddress("Kplus_Xc_minus_PX",&Kplus_Xc_minus_P[0]);
		t->SetBranchAddress("Kplus_Xc_minus_PY",&Kplus_Xc_minus_P[1]);
		t->SetBranchAddress("Kplus_Xc_minus_PZ",&Kplus_Xc_minus_P[2]);
		t->SetBranchAddress("Kplus_Xc_minus_PE",&Kplus_Xc_minus_E);

		t->SetBranchAddress("piminus_Xc_minus_PX",&piminus_Xc_minus_P[0]);
		t->SetBranchAddress("piminus_Xc_minus_PY",&piminus_Xc_minus_P[1]);
		t->SetBranchAddress("piminus_Xc_minus_PZ",&piminus_Xc_minus_P[2]);
		t->SetBranchAddress("piminus_Xc_minus_PE",&piminus_Xc_minus_E);
	}
	
	if(mode == 3){
		t->SetBranchAddress("Kminus_Dsplus_PX",&Kminus_Dsplus_P[0]);
		t->SetBranchAddress("Kminus_Dsplus_PY",&Kminus_Dsplus_P[1]);
		t->SetBranchAddress("Kminus_Dsplus_PZ",&Kminus_Dsplus_P[2]);
		t->SetBranchAddress("Kminus_Dsplus_PE",&Kminus_Dsplus_E);

		t->SetBranchAddress("Kplus_Dsplus_PX",&Kplus_Dsplus_P[0]);
		t->SetBranchAddress("Kplus_Dsplus_PY",&Kplus_Dsplus_P[1]);
		t->SetBranchAddress("Kplus_Dsplus_PZ",&Kplus_Dsplus_P[2]);
		t->SetBranchAddress("Kplus_Dsplus_PE",&Kplus_Dsplus_E);

		t->SetBranchAddress("piplus_Dsplus_PX",&piplus_Dsplus_P[0]);
		t->SetBranchAddress("piplus_Dsplus_PY",&piplus_Dsplus_P[1]);
		t->SetBranchAddress("piplus_Dsplus_PZ",&piplus_Dsplus_P[2]);
		t->SetBranchAddress("piplus_Dsplus_PE",&piplus_Dsplus_E);

		t->SetBranchAddress("Kplus_Dsminus_PX",&Kplus_Dsminus_P[0]);
		t->SetBranchAddress("Kplus_Dsminus_PY",&Kplus_Dsminus_P[1]);
		t->SetBranchAddress("Kplus_Dsminus_PZ",&Kplus_Dsminus_P[2]);
		t->SetBranchAddress("Kplus_Dsminus_PE",&Kplus_Dsminus_E);

		t->SetBranchAddress("Kminus_Dsminus_PX",&Kminus_Dsminus_P[0]);
		t->SetBranchAddress("Kminus_Dsminus_PY",&Kminus_Dsminus_P[1]);
		t->SetBranchAddress("Kminus_Dsminus_PZ",&Kminus_Dsminus_P[2]);
		t->SetBranchAddress("Kminus_Dsminus_PE",&Kminus_Dsminus_E);

		t->SetBranchAddress("piminus_Dsminus_PX",&piminus_Dsminus_P[0]);
		t->SetBranchAddress("piminus_Dsminus_PY",&piminus_Dsminus_P[1]);
		t->SetBranchAddress("piminus_Dsminus_PZ",&piminus_Dsminus_P[2]);
		t->SetBranchAddress("piminus_Dsminus_PE",&piminus_Dsminus_E);
	}
	
	double mK(493.677);
	double mPi(139.57018);
	double mp(938.272);

	double m_KK_Dsminus;
	double m_KK_Dsplus;
	
	//preparing sub-masses
	// D+ misID to Ds+
	double m_Dsminus_pi2K;
	double m_DsDs_Dsminus_pi2K;
	double m_Dsplus_pi2K;
	double m_DsDs_Dsplus_pi2K;

	// Ds+ misID to D+
	double m_Dplus_K2pi1;
	double m_Dplus_K2pi2;
	double m_KK_Dplus_K2pi1;
	double m_KK_Dplus_K2pi2;

	// Lc+ misID to Ds+
	double m_Dsminus_p2K;
	double m_Dsplus_p2K;

	// Lc+ misID to D+
	double m_Dplus_p2pi1;
	double m_Dplus_p2pi2;

	// Ds+ misID to Lc+
	double m_Lcplus_K2p;
	double m_Lcminus_K2p;
	double m_KK_Lcplus_K2p;
	double m_KK_Lcminus_K2p;

	// D+ misID to Lc+
	double m_Lcplus_pi2p;
	double m_Lcminus_pi2p;

	t->SetBranchStatus("m_Dsminus_pi2K", 0);
	t->SetBranchStatus("m_Dsminus_p2K", 0);
	t->SetBranchStatus("m_KK_Dsminus", 0);

	t->SetBranchStatus("m_Dplus_K2pi1", 0);
	t->SetBranchStatus("m_Dplus_K2pi2", 0);

	t->SetBranchStatus("m_Dplus_p2pi1", 0);
	t->SetBranchStatus("m_Dplus_p2pi2", 0);

	t->SetBranchStatus("m_KK_Dplus_K2pi1", 0);
	t->SetBranchStatus("m_KK_Dplus_K2pi2", 0);



	t->SetBranchStatus("m_Lcplus_K2p", 0);
	t->SetBranchStatus("m_Lcminus_K2p", 0);
	t->SetBranchStatus("m_Lcplus_pi2p", 0);
	t->SetBranchStatus("m_Lcminus_pi2p", 0);
	t->SetBranchStatus("m_KK_Lcplus_K2p", 0);
	t->SetBranchStatus("m_KK_Lcminus_K2p", 0);


	t->SetBranchStatus("m_Dsminus_pi2K", 0);
	t->SetBranchStatus("m_DsDs_Dsminus_pi2K", 0);
	t->SetBranchStatus("m_Dsplus_pi2K", 0);
	t->SetBranchStatus("m_DsDs_Dsplus_pi2K", 0);

	t->SetBranchStatus("m_Dsminus_p2K", 0);
	t->SetBranchStatus("m_Dsplus_p2K", 0);

	t->SetBranchStatus("m_KK_Dsminus", 0);
	t->SetBranchStatus("m_KK_Dsplus", 0);


	// Preparing the KFold branch
	TFile f2(fileNameTmp, "RECREATE");
	TTree* t2 = t->CloneTree(0);


	if(mode == 1){
		t2->Branch("m_Dsminus_pi2K", &m_Dsminus_pi2K, "m_Dsminus_pi2K/D");
		t2->Branch("m_Dsminus_p2K", &m_Dsminus_p2K, "m_Dsminus_p2K/D");
		t2->Branch("m_KK_Dsminus", &m_KK_Dsminus, "m_KK_Dsminus/D");

		t2->Branch("m_Dplus_K2pi1", &m_Dplus_K2pi1, "m_Dplus_K2pi1/D");
		t2->Branch("m_Dplus_K2pi2", &m_Dplus_K2pi2, "m_Dplus_K2pi2/D");

		t2->Branch("m_Dplus_p2pi1", &m_Dplus_p2pi1, "m_Dplus_p2pi1/D");
		t2->Branch("m_Dplus_p2pi2", &m_Dplus_p2pi2, "m_Dplus_p2pi2/D");

		t2->Branch("m_KK_Dplus_K2pi1", &m_KK_Dplus_K2pi1, "m_KK_Dplus_K2pi1/D");
		t2->Branch("m_KK_Dplus_K2pi2", &m_KK_Dplus_K2pi2, "m_KK_Dplus_K2pi2/D");
	}
	if(mode == 2 || mode == 4){
		t2->Branch("m_Lcplus_K2p", &m_Lcplus_K2p, "m_Lcplus_K2p/D");
		t2->Branch("m_Lcminus_K2p", &m_Lcminus_K2p, "m_Lcminus_K2p/D");
		t2->Branch("m_Lcplus_pi2p", &m_Lcplus_pi2p, "m_Lcplus_pi2p/D");
		t2->Branch("m_Lcminus_pi2p", &m_Lcminus_pi2p, "m_Lcminus_pi2p/D");
		t2->Branch("m_KK_Lcplus_K2p", &m_KK_Lcplus_K2p, "m_KK_Lcplus_K2p/D");
		t2->Branch("m_KK_Lcminus_K2p", &m_KK_Lcminus_K2p, "m_KK_Lcminus_K2p/D");
	}
	
	if(mode == 3){
		t2->Branch("m_Dsminus_pi2K", &m_Dsminus_pi2K, "m_Dsminus_pi2K/D");
		t2->Branch("m_DsDs_Dsminus_pi2K", &m_DsDs_Dsminus_pi2K, "m_DsDs_Dsminus_pi2K/D");
		t2->Branch("m_Dsplus_pi2K", &m_Dsplus_pi2K, "m_Dsplus_pi2K/D");
		t2->Branch("m_DsDs_Dsplus_pi2K", &m_DsDs_Dsplus_pi2K, "m_DsDs_Dsplus_pi2K/D");

		t2->Branch("m_Dsminus_p2K", &m_Dsminus_p2K, "m_Dsminus_p2K/D");
		t2->Branch("m_Dsplus_p2K", &m_Dsplus_p2K, "m_Dsplus_p2K/D");

		t2->Branch("m_KK_Dsminus", &m_KK_Dsminus, "m_KK_Dsminus/D");
		t2->Branch("m_KK_Dsplus", &m_KK_Dsplus, "m_KK_Dsplus/D");
	}
	
	
	cout<<"Adding combine mass for "<<fileName<<endl;
	
	for(int i(0); i<t->GetEntries(); ++i)
	{   
		if(i % (t->GetEntries()/10) == 0) cout<<100*i/(t->GetEntries())<<" \% "<<flush;
		t->GetEntry(i);

		// for Bs2DsD, Ds+(K- K+ pi+) misID to D+(K- pi+ pi+)
		//				or Lc+(p K- pi+)  misID to D+(K- pi+ pi+)
		if(mode == 1)
		{
			TLorentzVector totP_Dsminus_pi2K(Kplus_Dsminus_P+Kminus_Dsminus_P+piminus_Dsminus_P,   Kplus_Dsminus_E+sqrt( mPi*mPi+Kminus_Dsminus_P.Mag2())+piminus_Dsminus_E );
			m_Dsminus_pi2K = totP_Dsminus_pi2K.M();

			TLorentzVector totP_Dsminus_p2K(Kplus_Dsminus_P+Kminus_Dsminus_P+piminus_Dsminus_P,   Kplus_Dsminus_E+sqrt( mp*mp+Kminus_Dsminus_P.Mag2())+piminus_Dsminus_E );
			m_Dsminus_p2K = totP_Dsminus_p2K.M();

			TLorentzVector totP_KK_Dsminus(Kplus_Dsminus_P+Kminus_Dsminus_P,   Kplus_Dsminus_E+Kminus_Dsminus_E );
			m_KK_Dsminus = totP_KK_Dsminus.M();



			TLorentzVector totP_Dplus_K2pi1(Kminus_Dplus_P+piplus1_Dplus_P+piplus2_Dplus_P,   Kminus_Dplus_E+sqrt( mK*mK+piplus1_Dplus_P.Mag2())+piplus2_Dplus_E );
			m_Dplus_K2pi1 = totP_Dplus_K2pi1.M();

			TLorentzVector totP_Dplus_K2pi2(Kminus_Dplus_P+piplus1_Dplus_P+piplus2_Dplus_P,   Kminus_Dplus_E+piplus1_Dplus_E+sqrt( mK*mK+piplus2_Dplus_P.Mag2()) );
			m_Dplus_K2pi2 = totP_Dplus_K2pi2.M();

			TLorentzVector totP_KK_Dplus_K2pi1(Kminus_Dplus_P+piplus1_Dplus_P,   Kminus_Dplus_E+sqrt( mK*mK+piplus1_Dplus_P.Mag2()) );
			m_KK_Dplus_K2pi1 = totP_KK_Dplus_K2pi1.M();

			TLorentzVector totP_KK_Dplus_K2pi2(Kminus_Dplus_P+piplus2_Dplus_P,   Kminus_Dplus_E+sqrt( mK*mK+piplus2_Dplus_P.Mag2()) );
			m_KK_Dplus_K2pi2 = totP_KK_Dplus_K2pi2.M();

			TLorentzVector totP_Dplus_p2pi1(Kminus_Dplus_P+piplus1_Dplus_P+piplus2_Dplus_P,   Kminus_Dplus_E+sqrt( mp*mp+piplus1_Dplus_P.Mag2())+piplus2_Dplus_E );
			m_Dplus_p2pi1 = totP_Dplus_p2pi1.M();

			TLorentzVector totP_Dplus_p2pi2(Kminus_Dplus_P+piplus1_Dplus_P+piplus2_Dplus_P,   Kminus_Dplus_E+piplus1_Dplus_E+sqrt( mp*mp+piplus2_Dplus_P.Mag2()) );
			m_Dplus_p2pi2 = totP_Dplus_p2pi2.M();
		}
		
		// for Bs2DsDs, D+(K- pi+ pi+) misID to Ds+(K- K+ pi+)
		//				or Lc+(p K- pi+)  misID to Ds+(K- K+ pi+)
		if(mode == 3)
		{
			TLorentzVector totP_Dsminus_pi2K(Kplus_Dsminus_P+Kminus_Dsminus_P+piminus_Dsminus_P,   Kplus_Dsminus_E+sqrt( mPi*mPi+Kminus_Dsminus_P.Mag2())+piminus_Dsminus_E );
			m_Dsminus_pi2K = totP_Dsminus_pi2K.M();

			TLorentzVector totP_DsDs_Dsminus_pi2K(Kminus_Dsplus_P+Kplus_Dsplus_P+piplus_Dsplus_P+Kplus_Dsminus_P+Kminus_Dsminus_P+piminus_Dsminus_P,   Kminus_Dsplus_E+Kplus_Dsplus_E+piplus_Dsplus_E+Kplus_Dsminus_E+sqrt( mPi*mPi+Kminus_Dsminus_P.Mag2())+piminus_Dsminus_E);
			m_DsDs_Dsminus_pi2K = totP_DsDs_Dsminus_pi2K.M();

			TLorentzVector totP_Dsminus_p2K(Kplus_Dsminus_P+Kminus_Dsminus_P+piminus_Dsminus_P,   Kplus_Dsminus_E+sqrt( mp*mp+Kminus_Dsminus_P.Mag2())+piminus_Dsminus_E );
			m_Dsminus_p2K = totP_Dsminus_p2K.M();

			TLorentzVector totP_KK_Dsminus(Kplus_Dsminus_P+Kminus_Dsminus_P,   Kplus_Dsminus_E+Kminus_Dsminus_E );
			m_KK_Dsminus = totP_KK_Dsminus.M();



			TLorentzVector totP_Dsplus_pi2K(Kminus_Dsplus_P+Kplus_Dsplus_P+piplus_Dsplus_P,   Kminus_Dsplus_E+sqrt( mPi*mPi+Kplus_Dsplus_P.Mag2())+piplus_Dsplus_E );
			m_Dsplus_pi2K = totP_Dsplus_pi2K.M();

			TLorentzVector totP_DsDs_Dsplus_pi2K(Kminus_Dsplus_P+Kplus_Dsplus_P+piplus_Dsplus_P+Kplus_Dsminus_P+Kminus_Dsminus_P+piminus_Dsminus_P,   Kminus_Dsplus_E+sqrt( mPi*mPi+Kplus_Dsplus_P.Mag2())+piplus_Dsplus_E+Kplus_Dsminus_E+Kminus_Dsminus_E+piminus_Dsminus_E);
			m_DsDs_Dsplus_pi2K = totP_DsDs_Dsplus_pi2K.M();

			TLorentzVector totP_Dsplus_p2K(Kminus_Dsplus_P+Kplus_Dsplus_P+piplus_Dsplus_P,   Kminus_Dsplus_E+sqrt( mp*mp+Kplus_Dsplus_P.Mag2())+piplus_Dsplus_E );
			m_Dsplus_p2K = totP_Dsplus_p2K.M();

			TLorentzVector totP_KK_Dsplus(Kplus_Dsplus_P+Kminus_Dsplus_P,   Kplus_Dsplus_E+Kminus_Dsplus_E );
			m_KK_Dsplus = totP_KK_Dsplus.M();
		}
		
		// for B2LcLc, D+(K- pi+ pi+) misID to Lc+(p K- pi+)
		//				or Ds+(K- K+ pi+) misID to Lc+(p K- pi+)
		if(mode == 2 || mode == 4)
		{
			TLorentzVector totP_Lcplus_K2p(p_Xc_plus_P+Kminus_Xc_plus_P+piplus_Xc_plus_P,   sqrt(mK*mK+p_Xc_plus_P.Mag2())+Kminus_Xc_plus_E+piplus_Xc_plus_E );
			m_Lcplus_K2p = totP_Lcplus_K2p.M();

			TLorentzVector totP_Lcminus_K2p(pbar_Xc_minus_P+Kplus_Xc_minus_P+piminus_Xc_minus_P,   sqrt(mK*mK+pbar_Xc_minus_P.Mag2())+Kplus_Xc_minus_E+piminus_Xc_minus_E );
			m_Lcminus_K2p = totP_Lcminus_K2p.M();

			TLorentzVector totP_KK_Lcplus_K2p(p_Xc_plus_P+Kminus_Xc_plus_P,   sqrt(mK*mK+p_Xc_plus_P.Mag2())+Kminus_Xc_plus_E);
			m_KK_Lcplus_K2p = totP_KK_Lcplus_K2p.M();

			TLorentzVector totP_KK_Lcminus_K2p(pbar_Xc_minus_P+Kplus_Xc_minus_P,   sqrt(mK*mK+pbar_Xc_minus_P.Mag2())+Kplus_Xc_minus_E );
			m_KK_Lcminus_K2p = totP_KK_Lcminus_K2p.M();



			TLorentzVector totP_Lcplus_pi2p(p_Xc_plus_P+Kminus_Xc_plus_P+piplus_Xc_plus_P,   sqrt(mPi*mPi+p_Xc_plus_P.Mag2())+Kminus_Xc_plus_E+piplus_Xc_plus_E );
			m_Lcplus_pi2p = totP_Lcplus_pi2p.M();

			TLorentzVector totP_Lcminus_pi2p(pbar_Xc_minus_P+Kplus_Xc_minus_P+piminus_Xc_minus_P,   sqrt(mPi*mPi+pbar_Xc_minus_P.Mag2())+Kplus_Xc_minus_E+piminus_Xc_minus_E );
			m_Lcminus_pi2p = totP_Lcminus_pi2p.M();
		}
		
		t2->Fill();
	}

	f2.cd();
	t2->Write("",TObject::kOverwrite);
	f.Close();
	f2.Close();
	rename(fileNameTmp, fileName);
}
void addCombineMassVars(vector<TString> fileNames, int mode){
	for(int i=0; i<fileNames.size(); i++){
		addCombineMassVars(fileNames[i], mode);
	}
}

TString getPEString(TString m, TString PX, TString PY, TString PZ){
	return "sqrt(pow("+PX+",2) +pow("+PY+",2) +pow("+PZ+",2) +pow("+m+",2))";
}
TString getMassString(TString PE, TString PX, TString PY, TString PZ){
	return "sqrt(pow("+PE+",2) - (pow("+PX+",2) +pow("+PY+",2) +pow("+PZ+",2)))";
}
TString getCombMassString(TString p1, TString p2){
	TString p1_PE = p1+"_PE";
	TString p1_PX = p1+"_PX";
	TString p1_PY = p1+"_PY";
	TString p1_PZ = p1+"_PZ";

	TString p2_PE = p2+"_PE";
	TString p2_PX = p2+"_PX";
	TString p2_PY = p2+"_PY";
	TString p2_PZ = p2+"_PZ";

	TString tot_PE, tot_PX, tot_PY, tot_PZ;
	tot_PE = "(" +p1_PE+" + "+p2_PE+")";
	tot_PX = "(" +p1_PX+" + "+p2_PX+")";
	tot_PY = "(" +p1_PY+" + "+p2_PY+")";
	tot_PZ = "(" +p1_PZ+" + "+p2_PZ+")";

	return getMassString(tot_PE, tot_PX, tot_PY, tot_PZ);
}
TString getCombMassString(vector<TString> particles){
    vector<TString> p_PE(particles.size());
    vector<TString> p_PX(particles.size());
    vector<TString> p_PY(particles.size());
    vector<TString> p_PZ(particles.size());

	TString tot_PE="(", tot_PX="(", tot_PY="(", tot_PZ="(";

	for(int i=0; i<particles.size(); i++){
    	p_PE[i] = particles[i]+"_PE";
    	p_PX[i] = particles[i]+"_PX";
    	p_PY[i] = particles[i]+"_PY";
    	p_PZ[i] = particles[i]+"_PZ";
	}
	tot_PE += p_PE[0];
	tot_PX += p_PX[0];
	tot_PY += p_PY[0];
	tot_PZ += p_PZ[0];
	for(int i=1; i<particles.size(); i++){
		tot_PE += " + "+p_PE[0];
		tot_PX += " + "+p_PX[0];
		tot_PY += " + "+p_PY[0];
		tot_PZ += " + "+p_PZ[0];
	}
	tot_PE += ")";
	tot_PX += ")";
	tot_PY += ")";
	tot_PZ += ")";

	return getMassString(tot_PE, tot_PX, tot_PY, tot_PZ);
}
void addRadomNumber(TString fileName){
	TFile file(fileName);
	TTree* tree = (TTree*)file.Get("DecayTree");
	if(!tree){
		cerr<< "ERROR: in addRadomNumber: no 'DecayTree' under tuple: " << fileName << endl;
		return;
	}

	tree->SetBranchStatus("rdmNumber", 0);

	printf("Fill DecayTree in root file: %s\n", fileName.Data());
	printf("New branch name: %s\n", "rdmNumber");

	TString fileNameTmp = fileName;
	fileNameTmp.Insert(fileNameTmp.Length()-5, "Tmp");
	TFile fTmp(fileNameTmp, "RECREATE");

	TTree* newTree = tree->CloneTree(0);

	double rdmNumber;
	newTree->Branch("rdmNumber", &rdmNumber, "rdmNumber/D");

	int nEntries = tree->GetEntries();

	TRandom rand;
	for(int i=0; i<nEntries; i++){
		tree->GetEntry(i);
		if(i % (nEntries/10) == 0) cout<<" "<<ceil(100*i/(1.*nEntries))<<"\% "<<flush;

		rdmNumber = rand.Uniform(1.0);
		newTree->Fill();
	}
	printf("\n");


	fTmp.cd();
	newTree->Write();


	file.Close();
	fTmp.Close();
	rename(fileNameTmp, fileName);
}
void addRadomNumber(vector<TString> fileNames){
	for(int i=0; i<fileNames.size(); i++){
		addRadomNumber(fileNames[i]);
	}
}

ValError mergeRun2Eff(vector<ValError> effs){
	ValError effRun2(1, 0);

	if(effs.size() != 4){
		cerr << "Bad input in mergeRun2Eff" << endl;
		return effRun2;
	}

	ValError allLumi(0.28462000 + 1.6202480 + 1.6826530 + 2.1314120, 0);
	ValError lumi2015(0.28462000, 0);
	ValError lumi2016(1.6202480, 0);
	ValError lumi2017(1.6826530, 0);
	ValError lumi2018(2.1314120, 0);

	effRun2 = effs[0]*(lumi2015/allLumi) + effs[1]*(lumi2016/allLumi) + effs[2]*(lumi2017/allLumi) + effs[3]*(lumi2018/allLumi);

	return effRun2;
}

TH1D mergeRun2Eff(vector<TH1D> effs){
	TH1D* effRun2 = (TH1D*)effs[0].Clone("effs");

	if(effs.size() != 4){
		cerr << "Bad input in mergeRun2Eff" << endl;
		return *effRun2;
	}

	for(int n=1; n<=effRun2->GetNbinsX(); n++){
		vector<ValError> bins;
		for(int i=0; i<4; i++){
			ValError tmp(effs[i].GetBinContent(n), effs[i].GetBinError(n));
			bins.push_back(tmp);
		}
		ValError binRun2 = mergeRun2Eff(bins);
		effRun2->SetBinContent(n, binRun2.val);
		effRun2->SetBinError(n, binRun2.err);
	}
	return *effRun2;
}
ValError merge1516Eff(vector<ValError> effs){
	ValError eff1516(1, 0);

	if(effs.size() != 2){
		cerr << "Bad input in merge1516Eff" << endl;
		return eff1516;
	}

	ValError allLumi(0.28462000 + 1.6202480, 0);
	ValError lumi2015(0.28462000, 0);
	ValError lumi2016(1.6202480, 0);

	eff1516 = effs[0]*(lumi2015/allLumi) + effs[1]*(lumi2016/allLumi);

	return eff1516;
}

TH1D merge1516Eff(vector<TH1D> effs){
	TH1D* eff1516 = (TH1D*)effs[0].Clone("effs");

	if(effs.size() != 2){
		cerr << "Bad input in merge1516Eff" << endl;
		return *eff1516;
	}

	for(int n=1; n<=eff1516->GetNbinsX(); n++){
		vector<ValError> bins;
		for(int i=0; i<2; i++){
			ValError tmp(effs[i].GetBinContent(n), effs[i].GetBinError(n));
			bins.push_back(tmp);
		}
		ValError bin1516 = merge1516Eff(bins);
		eff1516->SetBinContent(n, bin1516.val);
		eff1516->SetBinError(n, bin1516.err);
	}
	return *eff1516;
}
ValError mergeRun1Eff(vector<ValError> effs){
	ValError effRun1(1, 0);

	if(effs.size() != 2){
		cerr << "Bad input in mergeRun1Eff" << endl;
		return effRun1;
	}

	ValError allLumi(1.0 + 2.0, 0);
	ValError lumi2011(1.0, 0);
	ValError lumi2012(2.0, 0);

	effRun1 = effs[0]*(lumi2011/allLumi) + effs[1]*(lumi2012/allLumi);

	return effRun1;
}

TH1D mergeRun1Eff(vector<TH1D> effs){
	TH1D* effRun1 = (TH1D*)effs[0].Clone("effs");

	if(effs.size() != 4){
		cerr << "Bad input in mergeRun1Eff" << endl;
		return *effRun1;
	}

	for(int n=1; n<=effRun1->GetNbinsX(); n++){
		vector<ValError> bins;
		for(int i=0; i<2; i++){
			ValError tmp(effs[i].GetBinContent(n), effs[i].GetBinError(n));
			bins.push_back(tmp);
		}
		ValError binRun1 = mergeRun1Eff(bins);
		effRun1->SetBinContent(n, binRun1.val);
		effRun1->SetBinError(n, binRun1.err);
	}
	return *effRun1;
}
// note: the Lb->Lc+ Ds- select by LcDs stripping
void addCombMass_LcDs(TString fileName)
{
	TFile f(fileName);
	TTree* t;

	t = (TTree*)f.Get("DecayTree");

	if(!t) cerr<<"ERROR: in addCombMass_LcDs, no tree found"<<endl;

	TString fileNameTmp = fileName+".Tmp";

	// Preparing extra variables

	TVector3 p_Xc_plus_P, Kminus_Xc_plus_P, piplus_Xc_plus_P;
	double p_Xc_plus_E, Kminus_Xc_plus_E, piplus_Xc_plus_E;

	TVector3 Kplus_Ds_minus_P, Kminus_Ds_minus_P, piminus_Ds_minus_P;
	double Kplus_Ds_minus_E, Kminus_Ds_minus_E, piminus_Ds_minus_E;


	
	t->SetBranchAddress("p_Xc_plus_PX",&p_Xc_plus_P[0]);
	t->SetBranchAddress("p_Xc_plus_PY",&p_Xc_plus_P[1]);
	t->SetBranchAddress("p_Xc_plus_PZ",&p_Xc_plus_P[2]);
	t->SetBranchAddress("p_Xc_plus_PE",&p_Xc_plus_E);

	t->SetBranchAddress("Kminus_Xc_plus_PX",&Kminus_Xc_plus_P[0]);
	t->SetBranchAddress("Kminus_Xc_plus_PY",&Kminus_Xc_plus_P[1]);
	t->SetBranchAddress("Kminus_Xc_plus_PZ",&Kminus_Xc_plus_P[2]);
	t->SetBranchAddress("Kminus_Xc_plus_PE",&Kminus_Xc_plus_E);

	t->SetBranchAddress("piplus_Xc_plus_PX",&piplus_Xc_plus_P[0]);
	t->SetBranchAddress("piplus_Xc_plus_PY",&piplus_Xc_plus_P[1]);
	t->SetBranchAddress("piplus_Xc_plus_PZ",&piplus_Xc_plus_P[2]);
	t->SetBranchAddress("piplus_Xc_plus_PE",&piplus_Xc_plus_E);
	
	t->SetBranchAddress("Kplus_Ds_minus_PX",&Kplus_Ds_minus_P[0]);
	t->SetBranchAddress("Kplus_Ds_minus_PY",&Kplus_Ds_minus_P[1]);
	t->SetBranchAddress("Kplus_Ds_minus_PZ",&Kplus_Ds_minus_P[2]);
	t->SetBranchAddress("Kplus_Ds_minus_PE",&Kplus_Ds_minus_E);

	t->SetBranchAddress("Kminus_Ds_minus_PX",&Kminus_Ds_minus_P[0]);
	t->SetBranchAddress("Kminus_Ds_minus_PY",&Kminus_Ds_minus_P[1]);
	t->SetBranchAddress("Kminus_Ds_minus_PZ",&Kminus_Ds_minus_P[2]);
	t->SetBranchAddress("Kminus_Ds_minus_PE",&Kminus_Ds_minus_E);

	t->SetBranchAddress("piminus_Ds_minus_PX",&piminus_Ds_minus_P[0]);
	t->SetBranchAddress("piminus_Ds_minus_PY",&piminus_Ds_minus_P[1]);
	t->SetBranchAddress("piminus_Ds_minus_PZ",&piminus_Ds_minus_P[2]);
	t->SetBranchAddress("piminus_Ds_minus_PE",&piminus_Ds_minus_E);


	
	double mK(493.677);
	double mPi(139.57018);
	double mp(938.272);

	double m_KK_Ds_minus;
	
	// possible misID to LcLc
	double m_Ds_minus_K2p;
	double m_LcDs_Ds_minus_K2p;


	// Preparing the KFold branch
	TFile f2(fileNameTmp, "RECREATE");
	TTree* t2(NULL);



	t->SetBranchAddress("m_KK_Ds_minus", 0);
	t->SetBranchAddress("m_Ds_minus_K2p", 0);
	t->SetBranchAddress("m_LcDs_Ds_minus_K2p", 0);

	t->SetBranchAddress("m_KK_Dsminus", 0);
	t->SetBranchAddress("m_Dsminus_K2p", 0);
	t->SetBranchAddress("m_LcDs_Dsminus_K2p", 0);



	t2 = t->CloneTree(0);



	t2->Branch("m_KK_Ds_minus", &m_KK_Ds_minus, "m_KK_Ds_minus/D");
	t2->Branch("m_Ds_minus_K2p", &m_Ds_minus_K2p, "m_Ds_minus_K2p/D");
	t2->Branch("m_LcDs_Ds_minus_K2p", &m_LcDs_Ds_minus_K2p, "m_LcDs_Ds_minus_K2p/D");

	
	
	cout<<"Adding combine mass for "<<fileName<<endl;
	
	for(int i(0); i<t->GetEntries(); ++i)
	{   
		if(i % (t->GetEntries()/10) == 0) cout<<100*i/(t->GetEntries())<<" \% "<<flush;
		t->GetEntry(i);


		TLorentzVector totP_KK_Ds_minus(Kplus_Ds_minus_P+Kminus_Ds_minus_P,   Kplus_Ds_minus_E+Kminus_Ds_minus_E );
		m_KK_Ds_minus = totP_KK_Ds_minus.M();

		TLorentzVector totP_Ds_minus_K2p(Kplus_Ds_minus_P+Kminus_Ds_minus_P,   Kplus_Ds_minus_E+ sqrt( mp*mp+Kminus_Ds_minus_P.Mag2()));
		m_Ds_minus_K2p = totP_Ds_minus_K2p.M();


		TLorentzVector totP_LcDs_Ds_minus_K2p(p_Xc_plus_P+Kminus_Xc_plus_P+piplus_Xc_plus_P+Kplus_Ds_minus_P+Kminus_Ds_minus_P+piminus_Ds_minus_P,   p_Xc_plus_E+Kminus_Xc_plus_E+piplus_Xc_plus_E+Kplus_Ds_minus_E+sqrt( mp*mp+Kminus_Ds_minus_P.Mag2())+piminus_Ds_minus_E);
		m_LcDs_Ds_minus_K2p = totP_LcDs_Ds_minus_K2p.M();

		t2->Fill();
	}
	
	t2->Write("",TObject::kOverwrite);
	f.Close();
	f2.Close();
	rename(fileNameTmp, fileName);
}

void addAngleList(int mode, vector<TString>* names, vector<TString>* formulas){
	if(names->size() != formulas->size()){
		cerr << "getAngleList: names->size() != formulas->size() " << endl;
		return;
	}

	if(mode == 2 || mode == 4){
		names->push_back("angle_p_Xc_plus_Kminus_Xc_plus"); formulas->push_back("TMath::ACos((p_Xc_plus_PX*Kminus_Xc_plus_PX+p_Xc_plus_PY*Kminus_Xc_plus_PY+p_Xc_plus_PZ*Kminus_Xc_plus_PZ)/(p_Xc_plus_P*Kminus_Xc_plus_P))");
		names->push_back("angle_p_Xc_plus_piplus_Xc_plus"); formulas->push_back("TMath::ACos((p_Xc_plus_PX*piplus_Xc_plus_PX+p_Xc_plus_PY*piplus_Xc_plus_PY+p_Xc_plus_PZ*piplus_Xc_plus_PZ)/(p_Xc_plus_P*piplus_Xc_plus_P))");
		names->push_back("angle_piplus_Xc_plus_Kminus_Xc_plus"); formulas->push_back("TMath::ACos((piplus_Xc_plus_PX*Kminus_Xc_plus_PX+piplus_Xc_plus_PY*Kminus_Xc_plus_PY+piplus_Xc_plus_PZ*Kminus_Xc_plus_PZ)/(piplus_Xc_plus_P*Kminus_Xc_plus_P))");
		names->push_back("angle_pbar_Xc_minus_Kplus_Xc_minus"); formulas->push_back("TMath::ACos((pbar_Xc_minus_PX*Kplus_Xc_minus_PX+pbar_Xc_minus_PY*Kplus_Xc_minus_PY+pbar_Xc_minus_PZ*Kplus_Xc_minus_PZ)/(pbar_Xc_minus_P*Kplus_Xc_minus_P))");
		names->push_back("angle_pbar_Xc_minus_piminus_Xc_minus"); formulas->push_back("TMath::ACos((pbar_Xc_minus_PX*piminus_Xc_minus_PX+pbar_Xc_minus_PY*piminus_Xc_minus_PY+pbar_Xc_minus_PZ*piminus_Xc_minus_PZ)/(pbar_Xc_minus_P*piminus_Xc_minus_P))");
		names->push_back("angle_piminus_Xc_minus_Kplus_Xc_minus"); formulas->push_back("TMath::ACos((piminus_Xc_minus_PX*Kplus_Xc_minus_PX+piminus_Xc_minus_PY*Kplus_Xc_minus_PY+piminus_Xc_minus_PZ*Kplus_Xc_minus_PZ)/(piminus_Xc_minus_P*Kplus_Xc_minus_P))");


		names->push_back("angle_p_Xc_plus_pbar_Xc_minus"); formulas->push_back("TMath::ACos((p_Xc_plus_PX*pbar_Xc_minus_PX+p_Xc_plus_PY*pbar_Xc_minus_PY+p_Xc_plus_PZ*pbar_Xc_minus_PZ)/(p_Xc_plus_P*pbar_Xc_minus_P))");
		names->push_back("angle_p_Xc_plus_Kplus_Xc_minus"); formulas->push_back("TMath::ACos((p_Xc_plus_PX*Kplus_Xc_minus_PX+p_Xc_plus_PY*Kplus_Xc_minus_PY+p_Xc_plus_PZ*Kplus_Xc_minus_PZ)/(p_Xc_plus_P*Kplus_Xc_minus_P))");
		names->push_back("angle_p_Xc_plus_piminus_Xc_minus"); formulas->push_back("TMath::ACos((p_Xc_plus_PX*piminus_Xc_minus_PX+p_Xc_plus_PY*piminus_Xc_minus_PY+p_Xc_plus_PZ*piminus_Xc_minus_PZ)/(p_Xc_plus_P*piminus_Xc_minus_P))");
		names->push_back("angle_Kminus_Xc_plus_pbar_Xc_minus"); formulas->push_back("TMath::ACos((Kminus_Xc_plus_PX*pbar_Xc_minus_PX+Kminus_Xc_plus_PY*pbar_Xc_minus_PY+Kminus_Xc_plus_PZ*pbar_Xc_minus_PZ)/(Kminus_Xc_plus_P*pbar_Xc_minus_P))");
		names->push_back("angle_Kminus_Xc_plus_Kplus_Xc_minus"); formulas->push_back("TMath::ACos((Kminus_Xc_plus_PX*Kplus_Xc_minus_PX+Kminus_Xc_plus_PY*Kplus_Xc_minus_PY+Kminus_Xc_plus_PZ*Kplus_Xc_minus_PZ)/(Kminus_Xc_plus_P*Kplus_Xc_minus_P))");
		names->push_back("angle_Kminus_Xc_plus_piminus_Xc_minus"); formulas->push_back("TMath::ACos((Kminus_Xc_plus_PX*piminus_Xc_minus_PX+Kminus_Xc_plus_PY*piminus_Xc_minus_PY+Kminus_Xc_plus_PZ*piminus_Xc_minus_PZ)/(Kminus_Xc_plus_P*piminus_Xc_minus_P))");
		names->push_back("angle_piplus_Xc_plus_pbar_Xc_minus"); formulas->push_back("TMath::ACos((piplus_Xc_plus_PX*pbar_Xc_minus_PX+piplus_Xc_plus_PY*pbar_Xc_minus_PY+piplus_Xc_plus_PZ*pbar_Xc_minus_PZ)/(piplus_Xc_plus_P*pbar_Xc_minus_P))");
		names->push_back("angle_piplus_Xc_plus_Kplus_Xc_minus"); formulas->push_back("TMath::ACos((piplus_Xc_plus_PX*Kplus_Xc_minus_PX+piplus_Xc_plus_PY*Kplus_Xc_minus_PY+piplus_Xc_plus_PZ*Kplus_Xc_minus_PZ)/(piplus_Xc_plus_P*Kplus_Xc_minus_P))");
		names->push_back("angle_piplus_Xc_plus_piminus_Xc_minus"); formulas->push_back("TMath::ACos((piplus_Xc_plus_PX*piminus_Xc_minus_PX+piplus_Xc_plus_PY*piminus_Xc_minus_PY+piplus_Xc_plus_PZ*piminus_Xc_minus_PZ)/(piplus_Xc_plus_P*piminus_Xc_minus_P))");
	}
	if(mode == 1){
		names->push_back("angle_Kminus_Dplus_piplus1_Dplus"); formulas->push_back("TMath::ACos((Kminus_Dplus_PX*piplus1_Dplus_PX+Kminus_Dplus_PY*piplus1_Dplus_PY+Kminus_Dplus_PZ*piplus1_Dplus_PZ)/(Kminus_Dplus_P*piplus1_Dplus_P))");
		names->push_back("angle_Kminus_Dplus_piplus2_Dplus"); formulas->push_back("TMath::ACos((Kminus_Dplus_PX*piplus2_Dplus_PX+Kminus_Dplus_PY*piplus2_Dplus_PY+Kminus_Dplus_PZ*piplus2_Dplus_PZ)/(Kminus_Dplus_P*piplus2_Dplus_P))");
		names->push_back("angle_piplus2_Dplus_piplus1_Dplus"); formulas->push_back("TMath::ACos((piplus2_Dplus_PX*piplus1_Dplus_PX+piplus2_Dplus_PY*piplus1_Dplus_PY+piplus2_Dplus_PZ*piplus1_Dplus_PZ)/(piplus2_Dplus_P*piplus1_Dplus_P))");
		names->push_back("angle_Kplus_Dsminus_Kminus_Dsminus"); formulas->push_back("TMath::ACos((Kplus_Dsminus_PX*Kminus_Dsminus_PX+Kplus_Dsminus_PY*Kminus_Dsminus_PY+Kplus_Dsminus_PZ*Kminus_Dsminus_PZ)/(Kplus_Dsminus_P*Kminus_Dsminus_P))");
		names->push_back("angle_Kplus_Dsminus_piminus_Dsminus"); formulas->push_back("TMath::ACos((Kplus_Dsminus_PX*piminus_Dsminus_PX+Kplus_Dsminus_PY*piminus_Dsminus_PY+Kplus_Dsminus_PZ*piminus_Dsminus_PZ)/(Kplus_Dsminus_P*piminus_Dsminus_P))");
		names->push_back("angle_piminus_Dsminus_Kminus_Dsminus"); formulas->push_back("TMath::ACos((piminus_Dsminus_PX*Kminus_Dsminus_PX+piminus_Dsminus_PY*Kminus_Dsminus_PY+piminus_Dsminus_PZ*Kminus_Dsminus_PZ)/(piminus_Dsminus_P*Kminus_Dsminus_P))");


		names->push_back("angle_Kminus_Dplus_Kplus_Dsminus"); formulas->push_back("TMath::ACos((Kminus_Dplus_PX*Kplus_Dsminus_PX+Kminus_Dplus_PY*Kplus_Dsminus_PY+Kminus_Dplus_PZ*Kplus_Dsminus_PZ)/(Kminus_Dplus_P*Kplus_Dsminus_P))");
		names->push_back("angle_Kminus_Dplus_Kminus_Dsminus"); formulas->push_back("TMath::ACos((Kminus_Dplus_PX*Kminus_Dsminus_PX+Kminus_Dplus_PY*Kminus_Dsminus_PY+Kminus_Dplus_PZ*Kminus_Dsminus_PZ)/(Kminus_Dplus_P*Kminus_Dsminus_P))");
		names->push_back("angle_Kminus_Dplus_piminus_Dsminus"); formulas->push_back("TMath::ACos((Kminus_Dplus_PX*piminus_Dsminus_PX+Kminus_Dplus_PY*piminus_Dsminus_PY+Kminus_Dplus_PZ*piminus_Dsminus_PZ)/(Kminus_Dplus_P*piminus_Dsminus_P))");
		names->push_back("angle_piplus1_Dplus_Kplus_Dsminus"); formulas->push_back("TMath::ACos((piplus1_Dplus_PX*Kplus_Dsminus_PX+piplus1_Dplus_PY*Kplus_Dsminus_PY+piplus1_Dplus_PZ*Kplus_Dsminus_PZ)/(piplus1_Dplus_P*Kplus_Dsminus_P))");
		names->push_back("angle_piplus1_Dplus_Kminus_Dsminus"); formulas->push_back("TMath::ACos((piplus1_Dplus_PX*Kminus_Dsminus_PX+piplus1_Dplus_PY*Kminus_Dsminus_PY+piplus1_Dplus_PZ*Kminus_Dsminus_PZ)/(piplus1_Dplus_P*Kminus_Dsminus_P))");
		names->push_back("angle_piplus1_Dplus_piminus_Dsminus"); formulas->push_back("TMath::ACos((piplus1_Dplus_PX*piminus_Dsminus_PX+piplus1_Dplus_PY*piminus_Dsminus_PY+piplus1_Dplus_PZ*piminus_Dsminus_PZ)/(piplus1_Dplus_P*piminus_Dsminus_P))");
		names->push_back("angle_piplus2_Dplus_Kplus_Dsminus"); formulas->push_back("TMath::ACos((piplus2_Dplus_PX*Kplus_Dsminus_PX+piplus2_Dplus_PY*Kplus_Dsminus_PY+piplus2_Dplus_PZ*Kplus_Dsminus_PZ)/(piplus2_Dplus_P*Kplus_Dsminus_P))");
		names->push_back("angle_piplus2_Dplus_Kminus_Dsminus"); formulas->push_back("TMath::ACos((piplus2_Dplus_PX*Kminus_Dsminus_PX+piplus2_Dplus_PY*Kminus_Dsminus_PY+piplus2_Dplus_PZ*Kminus_Dsminus_PZ)/(piplus2_Dplus_P*Kminus_Dsminus_P))");
		names->push_back("angle_piplus2_Dplus_piminus_Dsminus"); formulas->push_back("TMath::ACos((piplus2_Dplus_PX*piminus_Dsminus_PX+piplus2_Dplus_PY*piminus_Dsminus_PY+piplus2_Dplus_PZ*piminus_Dsminus_PZ)/(piplus2_Dplus_P*piminus_Dsminus_P))");
	}
	if(mode == 3){
		names->push_back("angle_Kminus_Dsplus_Kplus_Dsplus"); formulas->push_back("TMath::ACos((Kminus_Dsplus_PX*Kplus_Dsplus_PX+Kminus_Dsplus_PY*Kplus_Dsplus_PY+Kminus_Dsplus_PZ*Kplus_Dsplus_PZ)/(Kminus_Dsplus_P*Kplus_Dsplus_P))");
		names->push_back("angle_Kminus_Dsplus_piplus_Dsplus"); formulas->push_back("TMath::ACos((Kminus_Dsplus_PX*piplus_Dsplus_PX+Kminus_Dsplus_PY*piplus_Dsplus_PY+Kminus_Dsplus_PZ*piplus_Dsplus_PZ)/(Kminus_Dsplus_P*piplus_Dsplus_P))");
		names->push_back("angle_piplus_Dsplus_Kplus_Dsplus"); formulas->push_back("TMath::ACos((piplus_Dsplus_PX*Kplus_Dsplus_PX+piplus_Dsplus_PY*Kplus_Dsplus_PY+piplus_Dsplus_PZ*Kplus_Dsplus_PZ)/(piplus_Dsplus_P*Kplus_Dsplus_P))");
		names->push_back("angle_Kplus_Dsminus_Kminus_Dsminus"); formulas->push_back("TMath::ACos((Kplus_Dsminus_PX*Kminus_Dsminus_PX+Kplus_Dsminus_PY*Kminus_Dsminus_PY+Kplus_Dsminus_PZ*Kminus_Dsminus_PZ)/(Kplus_Dsminus_P*Kminus_Dsminus_P))");
		names->push_back("angle_Kplus_Dsminus_piminus_Dsminus"); formulas->push_back("TMath::ACos((Kplus_Dsminus_PX*piminus_Dsminus_PX+Kplus_Dsminus_PY*piminus_Dsminus_PY+Kplus_Dsminus_PZ*piminus_Dsminus_PZ)/(Kplus_Dsminus_P*piminus_Dsminus_P))");
		names->push_back("angle_piminus_Dsminus_Kminus_Dsminus"); formulas->push_back("TMath::ACos((piminus_Dsminus_PX*Kminus_Dsminus_PX+piminus_Dsminus_PY*Kminus_Dsminus_PY+piminus_Dsminus_PZ*Kminus_Dsminus_PZ)/(piminus_Dsminus_P*Kminus_Dsminus_P))");


		names->push_back("angle_Kminus_Dsplus_Kplus_Dsminus"); formulas->push_back("TMath::ACos((Kminus_Dsplus_PX*Kplus_Dsminus_PX+Kminus_Dsplus_PY*Kplus_Dsminus_PY+Kminus_Dsplus_PZ*Kplus_Dsminus_PZ)/(Kminus_Dsplus_P*Kplus_Dsminus_P))");
		names->push_back("angle_Kminus_Dsplus_Kminus_Dsminus"); formulas->push_back("TMath::ACos((Kminus_Dsplus_PX*Kminus_Dsminus_PX+Kminus_Dsplus_PY*Kminus_Dsminus_PY+Kminus_Dsplus_PZ*Kminus_Dsminus_PZ)/(Kminus_Dsplus_P*Kminus_Dsminus_P))");
		names->push_back("angle_Kminus_Dsplus_piminus_Dsminus"); formulas->push_back("TMath::ACos((Kminus_Dsplus_PX*piminus_Dsminus_PX+Kminus_Dsplus_PY*piminus_Dsminus_PY+Kminus_Dsplus_PZ*piminus_Dsminus_PZ)/(Kminus_Dsplus_P*piminus_Dsminus_P))");
		names->push_back("angle_Kplus_Dsplus_Kplus_Dsminus"); formulas->push_back("TMath::ACos((Kplus_Dsplus_PX*Kplus_Dsminus_PX+Kplus_Dsplus_PY*Kplus_Dsminus_PY+Kplus_Dsplus_PZ*Kplus_Dsminus_PZ)/(Kplus_Dsplus_P*Kplus_Dsminus_P))");
		names->push_back("angle_Kplus_Dsplus_Kminus_Dsminus"); formulas->push_back("TMath::ACos((Kplus_Dsplus_PX*Kminus_Dsminus_PX+Kplus_Dsplus_PY*Kminus_Dsminus_PY+Kplus_Dsplus_PZ*Kminus_Dsminus_PZ)/(Kplus_Dsplus_P*Kminus_Dsminus_P))");
		names->push_back("angle_Kplus_Dsplus_piminus_Dsminus"); formulas->push_back("TMath::ACos((Kplus_Dsplus_PX*piminus_Dsminus_PX+Kplus_Dsplus_PY*piminus_Dsminus_PY+Kplus_Dsplus_PZ*piminus_Dsminus_PZ)/(Kplus_Dsplus_P*piminus_Dsminus_P))");
		names->push_back("angle_piplus_Dsplus_Kplus_Dsminus"); formulas->push_back("TMath::ACos((piplus_Dsplus_PX*Kplus_Dsminus_PX+piplus_Dsplus_PY*Kplus_Dsminus_PY+piplus_Dsplus_PZ*Kplus_Dsminus_PZ)/(piplus_Dsplus_P*Kplus_Dsminus_P))");
		names->push_back("angle_piplus_Dsplus_Kminus_Dsminus"); formulas->push_back("TMath::ACos((piplus_Dsplus_PX*Kminus_Dsminus_PX+piplus_Dsplus_PY*Kminus_Dsminus_PY+piplus_Dsplus_PZ*Kminus_Dsminus_PZ)/(piplus_Dsplus_P*Kminus_Dsminus_P))");
		names->push_back("angle_piplus_Dsplus_piminus_Dsminus"); formulas->push_back("TMath::ACos((piplus_Dsplus_PX*piminus_Dsminus_PX+piplus_Dsplus_PY*piminus_Dsminus_PY+piplus_Dsplus_PZ*piminus_Dsminus_PZ)/(piplus_Dsplus_P*piminus_Dsminus_P))");
	}
}


void savePullPlot(RooPlot& graph, string fileName)
{
	RooHist* hist = graph.pullHist(0,0,true);
	TGraphAsymmErrors tgae(hist->GetN());
	tgae.SetTitle("");
	TH1D *proj = new TH1D("pullsproj", "", 26, -6.5,6.5);
	double x(0);
	double y(0);
	for(int i(0); i<hist->GetN(); ++i)
	{
		hist->GetPoint(i,x,y);
		tgae.SetPoint(i,x,y); 
		tgae.SetPointError(i, hist->GetErrorXlow(i), hist->GetErrorXhigh(i),
				hist->GetErrorYlow(i), hist->GetErrorYhigh(i));
		proj->Fill(y);
	}

	TCanvas c_pullplot("pullplot", "pullplot", 600, 150);
	tgae.SetMarkerStyle(20);
	tgae.SetMarkerSize(1);
	tgae.SetMarkerColor(1);

	tgae.GetXaxis()->SetLimits(graph.GetXaxis()->GetXmin(), graph.GetXaxis()->GetXmax());
	tgae.GetYaxis()->SetRangeUser(-6,6);
	//tgae.GetYaxis()->SetLabelSize(0.);
	tgae.GetXaxis()->SetNdivisions(0);
	tgae.GetYaxis()->SetNdivisions(5);//(503);
  // tgae.GetYaxis()->SetLabelSize(0);//(0.133);
  // tgae.GetXaxis()->SetLabelSize(0);//(0.133);
	tgae.GetYaxis()->SetTitle("Pull");

	//double textSize = graph.GetYaxis()->GetTitleSize();
	//tgae.GetYaxis()->SetTitleSize(4.05*textSize);

	TLine line1(graph.GetXaxis()->GetXmin(), -3, graph.GetXaxis()->GetXmax(), -3);
	line1.SetLineColor(2);
	TLine line2(graph.GetXaxis()->GetXmin(), 3, graph.GetXaxis()->GetXmax(), 3);
	line2.SetLineColor(2);
	TLine line3(graph.GetXaxis()->GetXmin(), 0, graph.GetXaxis()->GetXmax(), 0);
	line3.SetLineColor(1);

	tgae.Draw("AP");
	line1.Draw();
	line2.Draw();
	line3.Draw();

	TFile f(fileName.c_str(), "RECREATE");

	cout<<"TOP MARGIN: "<<c_pullplot.GetTopMargin()<<endl;
	c_pullplot.SetTopMargin(0.06);
	c_pullplot.Write();

	// TCanvas *c = new TCanvas();
	// proj->Draw("hist");
	proj->Fit("gaus");
	// c->Write();
	proj->Write();

	f.Close();

	// delete proj;
	// delete c;
}


void beautifyPullPlot(TGraphAsymmErrors* graph, RooPlot& frame, TCanvas* cpull, double textSizeConversion)
{
	if(textSizeConversion < 0) textSizeConversion = 4.05;
	
	graph->GetXaxis()->SetNdivisions(0);
	graph->GetYaxis()->SetNdivisions(4, false);
	graph->GetYaxis()->SetLabelSize(textSizeConversion*frame.GetYaxis()->GetLabelSize());
	graph->GetYaxis()->SetTitle("Pull");
	double textSize = frame.GetYaxis()->GetTitleSize();
	graph->GetYaxis()->SetTitleSize(textSizeConversion*textSize);
	graph->GetYaxis()->SetTitleOffset(0.2);
	cpull->SetTopMargin(0.11);
}

vector<ValError> readVEFromFile(TString file, TString nameHisto){
	vector<ValError> effs;

	TFile f(file);
	TH1D* histo = (TH1D*)f.Get(nameHisto);
	if(!histo) {
		cerr << "No " +nameHisto+ " in " + file +"!" << endl;
		return effs;
	}

	for(int i=1; i<=histo->GetNbinsX(); i++){
		effs.push_back(ValError(histo->GetBinContent(i), histo->GetBinError(i)));
	}
	f.Close();
	return effs;
}

ValError Roo2VE(RooRealVar var){
	return ValError(var.getVal(), var.getError());
}

// tag Multiple candidates in tree, if one event contain serval candidates, only keep one of them randomly.
void tagMultiple(TString filename, list<ElementForUniquing> listElements){
	TFile f(filename);
	TTree* t = (TTree*)f.Get("DecayTree");

	TString nameNew = filename;
	TFile fNew(nameNew.Insert(nameNew.Length()-5, "_MultupleRemoved"), "RECREATE");

	fNew.cd();
	TTree* tNew = t->CloneTree(0);

	list<ElementForUniquing>::iterator listIt;

	unsigned long i = 0;
	unsigned long nTot = listElements.size();


	for(listIt = listElements.begin(); listIt != listElements.end(); listIt++){
		if(i % (nTot/10) == 0) cout<<100*i/nTot<<" \% "<<flush;

		t->GetEntry(listIt->numInTree);

		if(!listIt->toRemove) tNew->Fill();

		i++;
	}

	f.Close();

	fNew.cd();
	tNew->Write();
	fNew.Close();
}

void tupleMultiply(TString fileName, int times)
{
	vector<TString> tmpName(times);
	for(int i=0; i<times; i++){
		tmpName[i] = fileName+"_Tmp"+i2s(i);
		system("cp "+fileName+" "+ tmpName[i]);
	}

	TString fileNameMulty = fileName.Insert( fileName.Length()-5,"_Multiply"+i2s(times));
	mergeTrees(fileNameMulty, tmpName, "DecayTree");

    for(int i=0; i<times; i++){
		system("rm "+tmpName[i]);
    }
}
#endif

#include <iostream>
#include "RooStats/ModelConfig.h"
#include "RooStats/ProfileLikelihoodCalculator.h"
#include "RooStats/HypoTestResult.h"
#include <RooDataSet.h>

#include "/mnt/d/lhcb/B2XcXc/inc/tools.h"
#include "/mnt/d/lhcb/B2XcXc/inc/RooDoubleCB.h"
#include "/mnt/d/lhcb/B2XcXc/inc/fitMCtoGetShape.h"
#include "/mnt/d/lhcb/B2XcXc/inc/getPresel.h"

using namespace RooFit;
using namespace RooStats;

void toyStudy(TString toyDir, TString yearStr);
vector<vector<ValError>> fitVarsPull(TString nameFile, TString toyDir);
void fit(TString cutString )
{
	 //---------Build background PDF------
	lhcbStyle(0.063);
	 
	cout<< "define the expBkg"<<endl;
	 
	 
	vector<int> years;
	years.push_back(2015);
	years.push_back(2016);
	years.push_back(1516);
	years.push_back(2017);
	years.push_back(2018);
	years.push_back(2);

	int w(7);
	system("mkdir -p /mnt/d/lhcb/B2XcXc/public_html/fitParamTables/final_fit/");

	TString fTableData = "/mnt/d/lhcb/B2XcXc/public_html/fitParamTables/final_fit/data_Bd2DsD.dat";

	TString fTableData1 = "/mnt/d/lhcb/B2XcXc/public_html/fitParamTables/final_fit/data_Bd2DsD_yields.dat";
	TString fTableData2 = "/mnt/d/lhcb/B2XcXc/public_html/fitParamTables/final_fit/data_Bd2DsD_meanSigma.dat";
	TString fTableData3 = "/mnt/d/lhcb/B2XcXc/public_html/fitParamTables/final_fit/data_Bd2DsD_bkgParams.dat";
	ofstream out_data_yields(fTableData1.Data());
	ofstream out_data_meanSigma(fTableData2.Data());
	ofstream out_data_bkgParams(fTableData3.Data());

	TString fTableMC = "/mnt/d/lhcb/B2XcXc/public_html/fitParamTables/final_fit/MC_Bd2DsD.dat";
	ofstream out_MC(fTableMC.Data());

	system("mkdir -p /mnt/d/lhcb/B2XcXc/Documents/yields/final_fit/");
	TString sYields = "/mnt/d/lhcb/B2XcXc/Documents/yields/final_fit/yields_Bd2DsD.root";

	////********* 11 12, 3.0fb-1
	////15, 16, 17, 18,  5.8fb-1
	double lumi1[2] = {1.0, 2.0};
	double lumi2[4] = {0.3, 1.6, 1.7, 2.2};
	for(int i=0; i<years.size(); i++){
		TString yearStr = "Run2";
		if(years[i] ==2) yearStr = "Run"+i2s(years[i]);
		else yearStr = i2s(years[i]);


		TString yearCut = "(1)";
		if(years[i] == 1112) yearCut = "(year == 2011 || year==2012)";
		else if(years[i] == 2) yearCut = "(year == 2015 || year==2016 || year == 2017 || year==2018)";
		else if(years[i] == 1516) yearCut = "(year == 2015 || year==2016)";
		else yearCut = "(year == " +i2s(years[i])+")";

	 
		TString fitVar = "B_DTFM_M_0";
	 	RooRealVar m(fitVar, "#it{m}_{DsD}", 5150, 5450.);
		double binWidth = 4.0;
		int nBins = (int)(m.getMax() - m.getMin())/binWidth;
		m.setBins(nBins);
	 	

	 	RooRealVar tau("tau", "tau", -0.002, -1, 0.); 
	 	RooExponential expBkg("expBkg","background model",m,tau) ;
	 	
	 	TString datacut = fitVar+">"+d2s(m.getMin())+" && "+fitVar+"<"+d2s(m.getMax())+" && "+cutString;
		datacut += "&&" + yearCut;
	 	

	 	TChain chData;
		chData.Add("/mnt/d/lhcb/B2XcXc/root/Bd/data/Data_Bd2DsD_Run2_TriggerSel_PreSel.root/DecayTree");
	 	TTree* datatree = chData.CopyTree(datacut);
	 	datatree->SetBranchStatus("*", 0);
	 	datatree->SetBranchStatus(fitVar, 1);
		setBranchStatusTTF(datatree, datacut);
	 	RooDataSet *datars = new RooDataSet("dataset", "dataset", datatree, m);



		system("mkdir -p /mnt/d/lhcb/B2XcXc/public_html/fit/final_fit/Bd2DsD");
		system("mkdir -p /mnt/d/lhcb/B2XcXc/public_html/fit/final_fit/Bd2DsD/Bs");
		system("mkdir -p /mnt/d/lhcb/B2XcXc/public_html/fit/final_fit/Bd2DsD/syst_2CB");
		system("mkdir -p /mnt/d/lhcb/B2XcXc/public_html/fit/final_fit/Bd2DsD/syst_2CB/Bs");
    	TPaveText text(0.15, 0.85, 0.35, 0.95, "NDC");
	 	text.SetFillColor(0);
	 	text.SetFillStyle(0);
	 	text.SetLineColor(0);
	 	text.SetLineWidth(0);
	 	text.SetBorderSize(1);
	 	text.SetTextSize(2*0.05);

		text.AddText(yearStr);

		// try Ipatia
		RooRealVar alphalBd("alphalBd","#alpha_{1}", 2.8, 0.01, 5.);
		RooRealVar alpharBd("alpharBd","#alpha_{2}", 4, 0.1, 5.);
		RooRealVar nlBd("nlBd","n_{1}", 1.5, 0., 10);
		RooRealVar nrBd("nrBd","n_{2}", 4.0, 0.01, 10);
		RooRealVar sigmaBd("sigmaBd","#sigmaBd", 8, 6, 12);
		//RooRealVar sigmaBd("sigmaBd","#sigmaBd", 8, 6, 18);
		RooRealVar meanBd("meanBd","#mu", 5280, 5275, 5285);
		RooRealVar beta_Bd("beta_Bd","#beta_Bd", 0);
		RooRealVar zeta_Bd("zeta_Bd","#zeta_Bd", 1e-5);
		RooRealVar l_Bd("l_Bd", "l_Bd", -3, -50, 0.);

		if(years[i] == 2015) {
			alpharBd.setMax(10);
		}
		//if(years[i] == 2016){
		//	alpharBd.setMax(10);
		//}

		RooIpatia2 modelBd("modelBd", "modelBd", m, l_Bd, zeta_Bd, beta_Bd, sigmaBd, meanBd, alphalBd, nlBd, alpharBd, nrBd);




		//RooIpatia2 ipatiaBd("ipatiaBd", "ipatiaBd", m, l_Bd, zeta_Bd, beta_Bd, sigmaBd, meanBd, alphalBd, nlBd, alpharBd, nrBd);
		//RooRealVar fSigmaGausBd("fSigmaGausBd","fSigmaGausBd", 1.7, 0.5, 3);
		//RooFormulaVar sigma_gausCB_Bd("sigma_gausCB_Bd", "sigma_gausCB_Bd", "@0*@1", RooArgList(sigmaBd, fSigmaGausBd));
		//RooGaussian gausCB_Bd("gausCB_Bd", "gausCB_Bd", m, meanBd, sigma_gausCB_Bd);

		//RooRealVar fIpatia_Bd("fIpatia_Bd", "fIpatia_Bd", 0.5, 0., 1);
		//RooAddPdf modelBd("modelBd", "modelBd", RooArgList(ipatiaBd, gausCB_Bd), RooArgList(fIpatia_Bd));


		//// B_M no right tail, try signal CB or with gaus.
		//RooRealVar alphalBd("alphalBd","alphalBd", 2.0, 1, 5.);
		//RooRealVar nlBd("nlBd","nlBd", 2.0, 0.1, 10);
		//RooRealVar sigmaBd("sigmaBd","#sigmaBd", 11.5, 8, 15);
		//RooRealVar meanBd("meanBd","#mu", 5280, 5275, 5285);

		//RooRealVar fSigmaGausBd("fSigmaGausBd","fSigmaGausBd", 1.7, 0.8, 3);
		//RooFormulaVar sigma_gausCB_Bd("sigma_gausCB_Bd", "sigma_gausCB_Bd", "@0*@1", RooArgList(sigmaBd, fSigmaGausBd));
		//RooGaussian gausCB_Bd("gausCB_Bd", "gausCB_Bd", m, meanBd, sigma_gausCB_Bd);

		//RooCBShape cbBd("cbBd", "cbBd", m, meanBd, sigmaBd, alphalBd, nlBd);

		//RooRealVar fCB_Bd("fCB_Bd", "fCB_Bd", 0.7, 0.5, 0.9999);
		//RooAddPdf modelBd("modelBd", "modelBd", RooArgList(cbBd, gausCB_Bd), RooArgList(fCB_Bd));


		//// B_DTFM_M with right tail, try 2CB
		//RooRealVar alphalBd("alphalBd","alphalBd", 2, 0.01, 5.);
		//RooRealVar nlBd("nlBd","nlBd", 2.0, 1, 20);
		////RooRealVar nlBd("nlBd","nlBd", 10);
		//RooRealVar alpharBd("alpharBd", "alpharBd", -2, -5, -0.01);
		////RooRealVar nrBd("nrBd","nrBd", 2.0, 1, 10);
		//RooRealVar nrBd("nrBd","nrBd", 10);
		//RooRealVar sigma1Bd("sigma1Bd","#sigma1Bd", 7, 5, 15);
		//RooRealVar sigma2Bd("sigma2Bd","#sigma2Bd", 7, 5, 15);
		//RooRealVar meanBd("meanBd","#mu", 5280, 5275, 5285);
        //
        //
		//RooCBShape cb1Bd("cb1Bd", "cb1Bd", m, meanBd, sigma1Bd, alphalBd, nlBd);
		//RooCBShape cb2Bd("cb2Bd", "cb2Bd", m, meanBd, sigma2Bd, alpharBd, nrBd);

		////RooRealVar fracCB1_Bd("fracCB1_Bd", "fracCB1_Bd", 0.5, 0.01, 0.9999);
		//RooRealVar fracCB1_Bd("fracCB1_Bd", "fracCB1_Bd", 0.5);
		//RooAddPdf modelBd("modelBd", "modelBd", RooArgList(cb1Bd, cb2Bd), RooArgList(fracCB1_Bd));


		//fitMCBMass_addPdf("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2DsD_Run2_truth_PIDcor_TrigSel_Presel.root", fitVar, datacut, "1", m.getMin(), m.getMax(), binWidth,
		//	modelBd, RooArgList(cb1Bd, cb2Bd), 5230, 5340,
		//	fitVar, text, "/mnt/d/lhcb/B2XcXc/public_html/fit/final_fit/Bd2DsD/syst_2CB/MC_Bd2DsD_"+yearStr+".pdf", false, false);
		fitMCBMass("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2DsD_Run2_truth_PIDcor_TrigSel_Presel.root", fitVar, datacut, "1", m.getMin(), m.getMax(), binWidth,
			modelBd, 5230, 5340,
			fitVar, text, "/mnt/d/lhcb/B2XcXc/public_html/fit/final_fit/Bd2DsD/MC_Bd2DsD_"+yearStr+".pdf", false, false);

		alphalBd.setConstant(true);
		alpharBd.setConstant(true);
		nlBd.setConstant(true);
		nrBd.setConstant(true);
		sigmaBd.setConstant(true);
		l_Bd.setConstant(true);
			////fSigmaGausBd.setConstant(true);
		//sigma1Bd.setConstant(true);
		//sigma2Bd.setConstant(true);
			////meanBd.setConstant(true);

			////fIpatia_Bd.setConstant(true);
		//fracCB1_Bd.setConstant(true);
			////beta_Bd.setConstant(true);
			////zeta_Bd.setConstant(true);
			//////fCB_Bd.setConstant(true);

		out_MC<<setw(w)<<yearStr;
		out_MC<<" & "<<setw(w)<<"$"<<dbl2str(meanBd.getVal(),2)<<"\\pm"<<dbl2str(meanBd.getError(),2)<<"$";
		out_MC<<" & "<<setw(w)<<"$"<<dbl2str(sigmaBd.getVal(),2)<<"\\pm"<<dbl2str(sigmaBd.getError(),2)<<"$";

		//out_MC<<" & "<<setw(w)<<"$"<<dbl2str(sigma1Bd.getVal(),2)<<"\\pm"<<dbl2str(sigma1Bd.getError(),2)<<"$";
		//out_MC<<" & "<<setw(w)<<"$"<<dbl2str(sigma2Bd.getVal(),2)<<"\\pm"<<dbl2str(sigma2Bd.getError(),2)<<"$";

		out_MC<<" & "<<setw(w)<<"$"<<dbl2str(nlBd.getVal(),2)<<"\\pm"<<dbl2str(nlBd.getError(),2)<<"$";
		out_MC<<" & "<<setw(w)<<"$"<<dbl2str(nrBd.getVal(),2)<<"\\pm"<<dbl2str(nrBd.getError(),2)<<"$";
		out_MC<<" & "<<setw(w)<<"$"<<dbl2str(alphalBd.getVal(),2)<<"\\pm"<<dbl2str(alphalBd.getError(),2)<<"$";
		out_MC<<" & "<<setw(w)<<"$"<<dbl2str(alpharBd.getVal(),2)<<"\\pm"<<dbl2str(alpharBd.getError(),2)<<"$";
		out_MC<<" & "<<setw(w)<<"$"<<dbl2str(l_Bd.getVal(),2)<<"\\pm"<<dbl2str(l_Bd.getError(),2)<<"$";


		//out_MC<<" & "<<setw(w)<<"$"<<dbl2str(fracCB1_Bd.getVal(),2)<<"\\pm"<<dbl2str(fracCB1_Bd.getError(),2)<<"$";
			////out_MC<<" & "<<setw(w)<<"$"<<dbl2str(fSigmaGausBd.getVal(),2)<<"\\pm"<<dbl2str(fSigmaGausBd.getError(),2)<<"$";
			////out_MC<<" & "<<setw(w)<<"$"<<dbl2str(fCB_Bd.getVal(),2)<<"\\pm"<<dbl2str(fCB_Bd.getError(),2)<<"$";
			////out_MC<<" & "<<setw(w)<<"$"<<dbl2str(zeta_Bd.getVal(),2)<<"\\pm"<<dbl2str(zeta_Bd.getError(),2)<<"$";
			////out_MC<<" & "<<setw(w)<<"$"<<dbl2str(10*beta_Bd.getVal(),2)<<"\\pm"<<dbl2str(10*beta_Bd.getError(),2)<<"$";
		out_MC<<"\\\\"<<endl;




		////// B_M no right tail, try signal CB or with gaus.
		////RooRealVar alphalBs("alphalBs","alphalBs", 2.0, 1, 5.);
		////RooRealVar nlBs("nlBs","nlBs", 2.0, 0.1, 10);
		////RooRealVar sigmaBs("sigmaBs","#sigmaBs", 11.5, 8, 15);
		////RooRealVar deltaM("deltaM", "deltaM", 87.38);
    	////RooFormulaVar meanBs("meanBs","meanBs", "@0+@1", RooArgList(meanBd, deltaM));

		////RooRealVar fSigmaBs("fSigmaBs","fSigmaBs", 1.7, 0.8, 3);
		////RooFormulaVar sigma_gausCB_Bs("sigma_gausCB_Bs", "sigma_gausCB_Bs", "@0*@1", RooArgList(sigmaBs, fSigmaBs));
		////RooGaussian gausCB_Bs("gausCB_Bs", "gausCB_Bs", m, meanBs, sigma_gausCB_Bs);

		////RooCBShape cbBs("cbBs", "cbBs", m, meanBs, sigmaBs, alphalBs, nlBs);

		////RooRealVar fCB_Bs("fCB_Bs", "fCB_Bs", 0.7, 0.5, 0.9999);
		////RooAddPdf modelBs("modelBs", "modelBs", RooArgList(cbBs, gausCB_Bs), RooArgList(fCB_Bs));

		// try Ipatia
		RooRealVar alphalBs("alphalBs","#alpha_{1}", 2.8, 0.01, 5.);
		RooRealVar alpharBs("alpharBs","#alpha_{2}", 4, 0.1, 5.);
		RooRealVar nlBs("nlBs","n_{1}", 1.5, 0., 10);
		RooRealVar nrBs("nrBs","n_{2}", 4.0, 0.01, 10);
		RooRealVar sigmaBs("sigmaBs","#sigmaBs", 8, 6, 12);
		RooRealVar deltaM("deltaM", "deltaM", 87.38);
    	RooFormulaVar meanBs("meanBs","meanBs", "@0+@1", RooArgList(meanBd, deltaM));
		RooRealVar beta_Bs("beta_Bs","#beta_Bs", 0);
		RooRealVar zeta_Bs("zeta_Bs","#zeta_Bs", 1e-5);
		RooRealVar l_Bs("l_Bs", "l_Bs", -3, -50, 0.);
		RooIpatia2 modelBs("ipatiaBs", "ipatiaBs", m, l_Bs, zeta_Bs, beta_Bs, sigmaBs, meanBs, alphalBs, nlBs, alpharBs, nrBs);


		//// B_DTFM_M with right tail, try 2CB
		//RooRealVar alphalBs("alphalBs","alphalBs", 2, 0.01, 5.);
		//RooRealVar nlBs("nlBs","nlBs", 2.0, 1, 20);
		////RooRealVar nlBs("nlBs","nlBs", 10);
		//RooRealVar alpharBs("alpharBs", "alpharBs", -2, -5, -0.01);
		////RooRealVar nrBs("nrBs","nrBs", 2.0, 1, 20);
		//RooRealVar nrBs("nrBs","nrBs", 10);
		//RooRealVar deltaM("deltaM", "deltaM", 87.38);
    	//RooFormulaVar meanBs("meanBs","meanBs", "@0+@1", RooArgList(meanBd, deltaM));
		//RooRealVar sigma1Bs("sigma1Bs","#sigma1Bs", 7, 3, 15);
		//RooRealVar sigma2Bs("sigma2Bs","#sigma2Bs", 7, 3, 15);
        //
        //
		//RooCBShape cb1Bs("cb1Bs", "cb1Bs", m, meanBs, sigma1Bs, alphalBs, nlBs);
		//RooCBShape cb2Bs("cb2Bs", "cb2Bs", m, meanBs, sigma2Bs, alpharBs, nrBs);

		//RooRealVar fracCB1_Bs("fracCB1_Bs", "fracCB1_Bs", 0.5);
		//RooAddPdf modelBs("modelBs", "modelBs", RooArgList(cb1Bs, cb2Bs), RooArgList(fracCB1_Bs));






		//fitMCBMass_addPdf("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bs2DsD_Run2_truth_PIDcor_TrigSel_Presel.root", fitVar, datacut+"&& rdmNumber<0.1", "1", m.getMin(), m.getMax(), binWidth,
		//	modelBs, RooArgList(cb1Bs, cb2Bs), 5250, 5450,
		//	fitVar, text, "/mnt/d/lhcb/B2XcXc/public_html/fit/final_fit/Bd2DsD/syst_2CB/Bs/MC_Bs2DsD_"+yearStr+".pdf", false, true);
		fitMCBMass("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bs2DsD_Run2_truth_PIDcor_TrigSel_Presel.root", fitVar, datacut+"&& rdmNumber<0.1", "1", m.getMin(), m.getMax(), binWidth,
			modelBs, 5250, 5450,
			fitVar, text, "/mnt/d/lhcb/B2XcXc/public_html/fit/final_fit/Bd2DsD/Bs/MC_Bs2DsD_"+yearStr+".pdf", false, true);

		alphalBs.setConstant(true);
		alpharBs.setConstant(true);
		nlBs.setConstant(true);
		nrBs.setConstant(true);
		sigma1Bs.setConstant(true);
		sigma2Bs.setConstant(true);
		////meanBs.setConstant(true);

		fracCB1_Bs.setConstant(true);
		////beta_Bs.setConstant(true);
		////zeta_Bs.setConstant(true);
		////l_Bs.setConstant(true);
		////fCB_Bs.setConstant(true);






	

		//RooRealVar fSigmaDataMC("fSigmaDataMC", "fSigmaDataMC", 1.1, 0.8, 1.5);


		//RooFormulaVar sigma1BdData("sigma1BdData", "sigma1BdData", "@0*@1", RooArgList(sigma1Bd, fSigmaDataMC));
		//RooFormulaVar sigma2BdData("sigma2BdData", "sigma2BdData", "@0*@1", RooArgList(sigma2Bd, fSigmaDataMC));
		//RooCBShape cb1BdData("cb1BdData", "cb1BdData", m, meanBd, sigma1BdData, alphalBd, nlBd);
		//RooCBShape cb2BdData("cb2BdData", "cb2BdData", m, meanBd, sigma2BdData, alpharBd, nrBd);
		//RooAddPdf modelBdData("modelBdData", "modelBdData", RooArgList(cb1BdData, cb2BdData), RooArgList(fracCB1_Bd));

		//RooFormulaVar sigma1BsData("sigma1BsData", "sigma1BsData", "@0*@1", RooArgList(sigma1Bs, fSigmaDataMC));
		//RooFormulaVar sigma2BsData("sigma2BsData", "sigma2BsData", "@0*@1", RooArgList(sigma2Bs, fSigmaDataMC));
		//RooCBShape cb1BsData("cb1BsData", "cb1BsData", m, meanBs, sigma1BsData, alphalBs, nlBs);
		//RooCBShape cb2BsData("cb2BsData", "cb2BsData", m, meanBs, sigma2BsData, alpharBs, nrBs);
		//RooAddPdf modelBsData("modelBsData", "modelBsData", RooArgList(cb1BsData, cb2BsData), RooArgList(fracCB1_Bs));

		////RooFormulaVar sigmaBdData("sigmaBdData", "sigmaBdData", "@0*@1", RooArgList(sigmaBd, fSigmaDataMC));
		////RooFormulaVar sigmaBsData("sigmaBsData", "sigmaBsData", "@0*@1", RooArgList(sigmaBs, fSigmaDataMC));

        //RooRealVar sigmaNonDD("sigmaNonDD", "sigmaNonDD", 15);
        //RooGaussian modelNonDD("modelNonDD", "modelNonDD", m, meanBd, sigmaNonDD);

		////RooIpatia2 modelBdData("modelBdData", "modelBdData", m, l_Bd, zeta_Bd, beta_Bd, sigmaBdData, meanBd, alphalBd, nlBd, alpharBd, nrBd);
		////RooIpatia2 modelBsData("modelBsData", "modelBsData", m, l_Bs, zeta_Bs, beta_Bs, sigmaBsData, meanBs, alphalBs, nlBs, alpharBs, nrBs);





    	//RooRealVar m0("m0","m0", 5210, 5200, 5230);
    	//RooRealVar c("c","c", 5, -50, 50);
    	//RooArgusBG argus_noSmearing("argus_noSmearing","prc background", m, m0, c);


		//// Detector response function
		//RooRealVar m0_g("m0_g", "m0_g", 0) ;
		//RooFormulaVar sigmaGaus("sigmaGaus", "sigmaGaus", "@0*@2+@1*(1-@2)", RooArgList(sigma1Bd, sigma2Bd, fracCB1_Bd));
		////RooFormulaVar sigmaGaus("sigmaGaus", "sigmaGaus", "@0", RooArgList(sigmaBdData));
		//RooGaussian gauss("gauss", "gauss", m, m0_g, sigmaGaus);


		//RooFFTConvPdf argusBkg("argusBkg","argusBkg (X) gauss", m, argus_noSmearing, gauss);
		//argusBkg.setBufferFraction(0.35);




		//
		//RooRealVar *nNonDD = new RooRealVar("nNonDD", "nNonDD", 0);

		//if(years[i] == 2) nNonDD->setVal(623.87);
		//if(years[i] == 1516) nNonDD->setVal( 623.87 * 1.9/5.8);
		//else if(years[i]>= 2015 && years[i]<= 2018) nNonDD->setVal( 623.87 * lumi2[years[i]-2015]/5.8);
		//else if(years[i] == 1112) nNonDD->setVal(0);
		//else if(years[i]>= 2011 && years[i]<= 2012) nNonDD->setVal( 0 * lumi1[years[i]-2011]/3.0);


	 	//RooRealVar *nBd = new RooRealVar("nBd","Number of signal Bd events", 30000, 0., 1e5);
	 	//RooRealVar *nBs = new RooRealVar("nBs","Number of signal Bs bkg", 1000, 0., 5000);
	 	//RooRealVar *nExpBkg = new RooRealVar("nExpBkg","Number of expBkg events",2800, 0., 1e7);
	 	//RooRealVar *nArgusBkg = new RooRealVar("nArgusBkg","Number of Rec expBkg events",2800, 0., 1e7);
	
	 	//RooAddPdf* totPDF = new RooAddPdf("totPDF","Total pdf", RooArgList(modelBdData, modelNonDD, modelBsData, expBkg, argusBkg), RooArgList(*nBd, *nNonDD, *nBs, *nExpBkg, *nArgusBkg));


	 	//cout<<"all ready"<<endl;
	 	////////////////////// Fit the data/////////////
	 	//RooFitResult *result = totPDF->fitTo(*datars, Save(true), NumCPU(10));

		//out_data_yields<<setw(w)<<yearStr;
		//out_data_yields<<" & "<<setw(w)<<"$"<<dbl2str(nBd->getVal(),1)<<"\\pm"<<dbl2str(nBd->getError(),1)<<"$";
		////out_data_yields<<" & "<<setw(w)<<"$"<<dbl2str(nNonDD->getVal(),0)<<"\\pm"<<dbl2str(nNonDD->getError(),0)<<"$";
		//out_data_yields<<" & "<<setw(w)<<"$"<<dbl2str(nBs->getVal(),1)<<"\\pm"<<dbl2str(nBs->getError(),1)<<"$";
		//out_data_yields<<" & "<<setw(w)<<"$"<<dbl2str(nExpBkg->getVal(),1)<<"\\pm"<<dbl2str(nExpBkg->getError(),1)<<"$";
		//out_data_yields<<" & "<<setw(w)<<"$"<<dbl2str(nArgusBkg->getVal(),1)<<"\\pm"<<dbl2str(nArgusBkg->getError(),1)<<"$";
		//out_data_yields<<"\\\\"<<endl;
		//out_data_meanSigma<<setw(w)<<yearStr;
		//out_data_meanSigma<<" & "<<setw(w)<<"$"<<dbl2str(meanBd.getVal(),1)<<"\\pm"<<dbl2str(meanBd.getError(),1)<<"$";
		//out_data_meanSigma<<" & "<<setw(w)<<"$"<<dbl2str(fSigmaDataMC.getVal(),1)<<"\\pm"<<dbl2str(fSigmaDataMC.getError(),1)<<"$";
		//out_data_meanSigma<<"\\\\"<<endl;
		//out_data_bkgParams<<setw(w)<<yearStr;
		//out_data_bkgParams<<" & "<<setw(w)<<"$"<<dbl2str(tau.getVal()*1e2,1)<<"\\pm"<<dbl2str(tau.getError()*1e2,1)<<"$";
		//out_data_bkgParams<<" & "<<setw(w)<<"$"<<dbl2str(m0.getVal(),1)<<"\\pm"<<dbl2str(m0.getError(),1)<<"$";
		//out_data_bkgParams<<" & "<<setw(w)<<"$"<<dbl2str(c.getVal(),1)<<"\\pm"<<dbl2str(c.getError(),1)<<"$";
		//out_data_bkgParams<<"\\\\"<<endl;



		////if(years[i]==-1){
		//	// save vars into workspace for toy study
		//	system("mkdir -p /mnt/d/lhcb/B2XcXc/Documents/toy/final_fit/Bd2DsD");
		//	TFile fWS("/mnt/d/lhcb/B2XcXc/Documents/toy/final_fit/Bd2DsD/fitVarsWorkspace.root", "UPDATE");
		//	RooWorkspace ws("workspace"+yearStr, "workspace"+yearStr);
		//	ws.import(fSigmaDataMC);

		//	ws.import(m);

		//	ws.import(m0);
		//	ws.import(c);

		//	ws.import(m0_g);
		//	//ws.import(sigma_g);

		//	ws.import(tau);

		//	ws.import(modelNonDD);

		//	ws.import(alphalBd);
		//	ws.import(alpharBd);
		//	ws.import(nlBd);
		//	ws.import(nrBd);
		//	//ws.import(sigmaBd);
		//	ws.import(sigma1Bd);
		//	ws.import(sigma2Bd);
		//	ws.import(meanBd);
		//	ws.import(fracCB1_Bd);
		//		////ws.import(fSigmaGausBd);
		//		////ws.import(fCB_Bd);
		//	//ws.import(beta_Bd);
		//	//ws.import(zeta_Bd);
		//	//ws.import(l_Bd);

		//	ws.import(meanBs);
		//	ws.import(deltaM);
		//	ws.import(nlBs);
		//	ws.import(nrBs);
		//	ws.import(alphalBs);
		//	ws.import(alpharBs);
		//	//ws.import(sigmaBs);
		//	ws.import(sigma1Bs);
		//	ws.import(sigma2Bs);
		//	ws.import(fracCB1_Bs);
		//		//ws.import(fSigmaGausBd);
		//		//ws.import(fSigmaBs);
		//		////ws.import(fCB_Bs);
		//	//ws.import(beta_Bs);
		//	//ws.import(zeta_Bs);
		//	//ws.import(l_Bs);

		//	ws.import(*nBd);
		//	ws.import(*nNonDD);
		//	ws.import(*nBs);
		//	ws.import(*nExpBkg);
		//	ws.import(*nArgusBkg);

		//	fWS.cd();
		//	ws.Write("", TObject::kOverwrite);
		//	fWS.Close();
		////}

        //int nBin;
        //if(years[i] == 2011) nBin = 1;
        //if(years[i] == 2012) nBin = 2;
        //if(years[i] == 1112) nBin = 3;
        //if(years[i] == 2015) nBin = 4;
        //if(years[i] == 2016) nBin = 5;
        //if(years[i] == 1516) nBin = 6;
        //if(years[i] == 2017) nBin = 7;
        //if(years[i] == 2018) nBin = 8;
        //if(years[i] == 2) nBin = 9;

		//// save yields
		//TFile fYields(sYields, "UPDATE");

		//TH1D* hYields = (TH1D*)fYields.Get("yields");
		//if(!hYields) hYields = new TH1D ("yields", "yields", 9, 0, 9);
		//
		//hYields->SetBinContent(nBin, nBd->getVal());
		//hYields->SetBinError(nBin, nBd->getError());
		//
		//fYields.cd();
		//hYields->Write("yields", TObject::kOverwrite);
		//fYields.Close();

	 	//

	 	//RooPlot *mframe = m.frame(Title("Bd2DsD Run2 data"));
	 	//
	 	//cout<<"Fit the data"<<endl;
	 	//
	 	//datars->plotOn(mframe, Name("data"));
	 	//totPDF->plotOn(mframe, Name("signal Bd"), Components(modelBdData), LineColor(3), LineStyle(kDashed));
		//totPDF->plotOn(mframe, Name("nonDD Gauss"), Components(modelNonDD), LineColor(13), LineStyle(kDashed));
	 	//totPDF->plotOn(mframe, Name("Bs bkg"), Components(modelBsData), LineColor(6), LineStyle(kDashed));
	 	//totPDF->plotOn(mframe, Name("exp bkg"), Components(expBkg), LineColor(4), LineStyle(kDashed));
	 	//totPDF->plotOn(mframe, Name("prc bkg"), Components(argusBkg), LineColor(kOrange), LineStyle(kDashed));
	 	//totPDF->plotOn(mframe, Name("totPDF"), LineColor(kRed));
	 	//datars->plotOn(mframe, Name("data"));
	 	////totPDF->paramOn(mframe, Layout(0.65, 0.95, 0.95));

		//int result_status = result->status();
		//cout << "result_status = "<< result_status<<endl;
		//result->Print("v");
		//
		//
		//RooHist *pull = mframe->pullHist();
		//pull->GetYaxis()->SetTitle("Pull");
		//RooPlot *framePull = m.frame();
		//framePull->addPlotable(pull, "P");
		//
		//mframe->SetTitle(fitVar);
		//mframe->SetXTitle("#it{m}_{B} [MeV/c^{2}]");
		//mframe->SetYTitle(Form("Candidates / (%.1lf MeV/c^{2})", binWidth));
		//mframe->GetXaxis()->SetTitleSize(0.2);
		//mframe->GetYaxis()->SetTitleSize(0.2);
		//
		//framePull->SetTitle("");
		//framePull->GetYaxis()->SetTitle("Pull");
		//framePull->SetMinimum(-6.);
		//framePull->SetMaximum(6.);
		//framePull->SetMarkerStyle(2);
		////cout<<"-----------------1"<<endl;
		//
		//TCanvas *canv = new TCanvas("canv", "canv", 800, 600);
		//canv->SetFillColor(10);
		//canv->SetBorderMode(0);
		//canv->cd();
		//canv->cd(1);
		//
		//TPad *p1 = new TPad("pad1", "pad1", 0.02, 0.2, 0.98, 0.98, 0);
		//TPad *p2 = new TPad("pad2", "pad2", 0.02, 0.04, 0.98, 0.2, 0);
		//p1->Draw();
		//p2->Draw();
		//
		//p1->cd();
		//mframe->GetYaxis()->SetTitleOffset(1.00);
		//mframe->GetYaxis()->SetLabelSize(0.05);
		//mframe->GetYaxis()->SetTitleSize(0.05);
		//mframe->GetYaxis()->SetNoExponent();
		//mframe->GetXaxis()->SetTitleOffset(1.00);
		//mframe->GetXaxis()->SetTitleSize(0.05);
		//mframe->GetXaxis()->SetLabelSize(0.05);
		//mframe->Draw("E1");
		//text.Draw("same");
		//
		//p2->cd();
		//p2->SetTickx();
		//framePull->GetYaxis()->SetTitleOffset(0.17);
		//framePull->GetXaxis()->SetTitleSize(0.0);
		//framePull->GetXaxis()->SetTickLength(0.09);
		//framePull->GetYaxis()->SetTitleSize(0.2);
		//framePull->GetXaxis()->SetLabelSize(0.0);
		//framePull->GetYaxis()->SetLabelSize(0.2);
		//framePull->GetYaxis()->SetNdivisions(5);
		//framePull->Draw("E1");
		//
		//TLine* lineZero = new TLine(m.getMin(), 0, m.getMax(), 0);
		//lineZero->SetLineStyle(kDashed);
		//lineZero->SetLineColor(kBlack);
		//lineZero->Draw();
		//
		//TLine* lineTwoSigUp = new TLine(m.getMin(), 3, m.getMax(), 3);
		//lineTwoSigUp->SetLineColor(kRed);
		//lineTwoSigUp->Draw();
		//TLine* lineTwoSigDown = new TLine(m.getMin(), -3, m.getMax(), -3);
		//lineTwoSigDown->SetLineColor(kRed);
		//lineTwoSigDown->Draw();


	 	////canv->SaveAs("/mnt/d/lhcb/B2XcXc/public_html/fit/final_fit/Bd2DsD/"+yearStr+"_data_Bd2DsD_final_fit.pdf");
		////p1->SetLogy();
		////canv->SaveAs("/mnt/d/lhcb/B2XcXc/public_html/fit/final_fit/Bd2DsD/"+yearStr+"_data_Bd2DsD_final_fit_log.pdf");

        //canv->SaveAs("/mnt/d/lhcb/B2XcXc/public_html/fit/final_fit/Bd2DsD/syst_2CB/"+yearStr+"_data_Bd2DsD_final_fit.pdf");
        //p1->SetLogy();
        //canv->SaveAs("/mnt/d/lhcb/B2XcXc/public_html/fit/final_fit/Bd2DsD/syst_2CB/"+yearStr+"_data_Bd2DsD_final_fit_log.pdf");
	}

	out_MC.close();
	out_data_yields.close();
	out_data_meanSigma.close();
	out_data_bkgParams.close();


	cout << endl << "params print to "+fTableMC+" "<< endl;
	system("cat "+fTableMC);

	cout << endl << "params print to "+fTableData1+" "<< endl;
	system("rm " + fTableData);

	system("cat "+fTableData1+">>"+fTableData);
	system("cat "+fTableData2+">>"+fTableData);
	system("cat "+fTableData3+">>"+fTableData);

	system("rm /mnt/d/lhcb/B2XcXc/public_html/fitParamTables/final_fit/data_Bd2DsD_*.dat");
	system("cat "+fTableData);

	cout << "yields saved into " + sYields << endl;
}


void fit_data_Bd2DsD()
{
	fit(getFinalSel());
	//toyStudy("/mnt/d/lhcb/B2XcXc/Documents/toy/final_fit/Bd2DsD", "Run2");
	gROOT->ProcessLine(".q");
}
void toyStudy(TString toyDir, TString yearStr)
{
	lhcbStyle(0.063);
	
	TFile fWS(toyDir+"/fitVarsWorkspace.root");
	RooWorkspace* ws = (RooWorkspace*)fWS.Get("workspace"+yearStr);
	if(!ws){
		cerr << "No workspace"+yearStr + " in " + toyDir+"/fitVarsWorkspace.root" << endl;
		return;
	}
	
	
	cout << "save possion resonanced vars..." << endl;
	// making possion resonance
	cout << "nPoisson: ";
	TRandom3 rand;
	
	TString namefPoisson = toyDir+"/toyVarsPoisson.root";

	TString fitVar = "B_DTFM_M_0";

	//bool savefPoisson = true;

	//if(savefPoisson){
	//	TFile fPoisson(namefPoisson, "RECREATE");
	//	TTree* t = new TTree("DecayTree", "DecayTree");
	//	
	//	// 先把泊松分布的变量存到root里面	
	//	double nBd_D, nBs_D, m0_D, c_D, tau_D, nExpBkg_D, nArgusBkg_D, meanBd_D;
	//	t->Branch("nBd", &nBd_D, "nBd/D");
	//	t->Branch("nBs", &nBs_D, "nBs/D");
	//	t->Branch("m0", &m0_D, "m0/D");
	//	t->Branch("c", &c_D, "c/D");
	//	t->Branch("tau", &tau_D, "tau/D");
	//	t->Branch("nExpBkg", &nExpBkg_D, "nExpBkg/D");
	//	t->Branch("nArgusBkg", &nArgusBkg_D, "nArgusBkg/D");
	//	t->Branch("meanBd", &meanBd_D, "meanBd/D");

	//	RooRealVar* nBd = (RooRealVar*) ws->var("nBd");
	//	RooRealVar* nBs = (RooRealVar*) ws->var("nBs");
	//	RooRealVar* m0 = (RooRealVar*) ws->var("m0");
	//	RooRealVar* c = (RooRealVar*) ws->var("c");
	//	RooRealVar* tau = (RooRealVar*) ws->var("tau");
	//	RooRealVar* nExpBkg = (RooRealVar*) ws->var("nExpBkg");
	//	RooRealVar* nArgusBkg = (RooRealVar*) ws->var("nArgusBkg");
	//	RooRealVar* meanBd = (RooRealVar*) ws->var("meanBd");

	//	for(int nPoisson=0; nPoisson<1000; nPoisson++){
	//		nBd_D = rand.Poisson(nBd->getVal());
	//		nBs_D = rand.Poisson(nBs->getVal());
	//		m0_D = rand.Poisson(m0->getVal()*1.0e4)/1.0e4;
	//		c_D = rand.Poisson(c->getVal()*1.0e4)/1.0e4;
	//		tau_D = -1.0*rand.Poisson(fabs(tau->getVal())*1.0e4)/1.0e4;
	//		nExpBkg_D = rand.Poisson(nExpBkg->getVal());
	//		nArgusBkg_D = rand.Poisson(nArgusBkg->getVal());
	//		meanBd_D = rand.Poisson(meanBd->getVal()*1.0e4)/1.0e4;

	//		t->Fill();
	//	}
	//	fPoisson.cd();
	//	t->Write();
	//	fPoisson.Close();
	//}


	//TFile fPoisson(namefPoisson);
	//TTree* t = (TTree*)fPoisson.Get("DecayTree");

	//TString namePoissonTmp = namefPoisson+"Tmp";
	//TFile fPoissonTmp(namePoissonTmp, "RECREATE");
	//fPoissonTmp.cd();
	//TTree* tNew = t->CloneTree(0);

	//// 读取需要震荡的参数
	//double nBd_D, nBs_D, m0_D, c_D, tau_D, nExpBkg_D, nArgusBkg_D, meanBd_D;
	//t->SetBranchAddress("nBd", &nBd_D);
	//t->SetBranchAddress("nBs", &nBs_D);
	//t->SetBranchAddress("m0", &m0_D);
	//t->SetBranchAddress("c", &c_D);
	//t->SetBranchAddress("tau", &tau_D);
	//t->SetBranchAddress("nExpBkg", &nExpBkg_D);
	//t->SetBranchAddress("nArgusBkg", &nArgusBkg_D);
	//t->SetBranchAddress("meanBd", &meanBd_D);


	//// 震荡后产生的参数
	//double nBd_fitVal, nBs_fitVal, m0_fitVal, nExpBkg_fitVal, nArgusBkg_fitVal, c_fitVal, tau_fitVal, meanBd_fitVal;
	//t->SetBranchStatus("nBd_fitVal", 0);
	//t->SetBranchStatus("nBs_fitVal", 0);
	//t->SetBranchStatus("m0_fitVal", 0);
	//t->SetBranchStatus("nExpBkg_fitVal", 0);
	//t->SetBranchStatus("nArgusBkg_fitVal", 0);
	//t->SetBranchStatus("c_fitVal", 0);
	//t->SetBranchStatus("tau_fitVal", 0);
	//t->SetBranchStatus("meanBd_fitVal", 0);
	//tNew->Branch("nBd_fitVal", &nBd_fitVal, "nBd_fitVal/D");
	//tNew->Branch("nBs_fitVal", &nBs_fitVal, "nBs_fitVal/D");
	//tNew->Branch("m0_fitVal", &m0_fitVal, "m0_fitVal/D");
	//tNew->Branch("nExpBkg_fitVal", &nExpBkg_fitVal, "nExpBkg_fitVal/D");
	//tNew->Branch("nArgusBkg_fitVal", &nArgusBkg_fitVal, "nArgusBkg_fitVal/D");
	//tNew->Branch("c_fitVal", &c_fitVal, "c_fitVal/D");
	//tNew->Branch("tau_fitVal", &tau_fitVal, "tau_fitVal/D");
	//tNew->Branch("meanBd_fitVal", &meanBd_fitVal, "meanBd_fitVal/D");
	//
	//double nBd_fitErr, nBs_fitErr, m0_fitErr, nExpBkg_fitErr, nArgusBkg_fitErr, c_fitErr, tau_fitErr, meanBd_fitErr;
	//t->SetBranchStatus("nBd_fitErr", 0);
	//t->SetBranchStatus("nBs_fitErr", 0);
	//t->SetBranchStatus("m0_fitErr", 0);
	//t->SetBranchStatus("nExpBkg_fitErr", 0);
	//t->SetBranchStatus("nArgusBkg_fitErr", 0);
	//t->SetBranchStatus("c_fitErr", 0);
	//t->SetBranchStatus("tau_fitErr", 0);
	//t->SetBranchStatus("meanBd_fitErr", 0);
	//tNew->Branch("nBd_fitErr", &nBd_fitErr, "nBd_fitErr/D");
	//tNew->Branch("nBs_fitErr", &nBs_fitErr, "nBs_fitErr/D");
	//tNew->Branch("m0_fitErr", &m0_fitErr, "m0_fitErr/D");
	//tNew->Branch("nExpBkg_fitErr", &nExpBkg_fitErr, "nExpBkg_fitErr/D");
	//tNew->Branch("nArgusBkg_fitErr", &nArgusBkg_fitErr, "nArgusBkg_fitErr/D");
	//tNew->Branch("c_fitErr", &c_fitErr, "c_fitErr/D");
	//tNew->Branch("tau_fitErr", &tau_fitErr, "tau_fitErr/D");
	//tNew->Branch("meanBd_fitErr", &meanBd_fitErr, "meanBd_fitErr/D");
	//
	//double nBd_pull, nBs_pull, m0_pull, nExpBkg_pull, nArgusBkg_pull, c_pull, tau_pull, meanBd_pull;
	//t->SetBranchStatus("nBd_pull", 0);
	//t->SetBranchStatus("nBs_pull", 0);
	//t->SetBranchStatus("m0_pull", 0);
	//t->SetBranchStatus("nExpBkg_pull", 0);
	//t->SetBranchStatus("nArgusBkg_pull", 0);
	//t->SetBranchStatus("c_pull", 0);
	//t->SetBranchStatus("tau_pull", 0);
	//t->SetBranchStatus("meanBd_pull", 0);
	//tNew->Branch("nBd_pull", &nBd_pull, "nBd_pull/D");
	//tNew->Branch("nBs_pull", &nBs_pull, "nBs_pull/D");
	//tNew->Branch("m0_pull", &m0_pull, "m0_pull/D");
	//tNew->Branch("nExpBkg_pull", &nExpBkg_pull, "nExpBkg_pull/D");
	//tNew->Branch("nArgusBkg_pull", &nArgusBkg_pull, "nArgusBkg_pull/D");
	//tNew->Branch("c_pull", &c_pull, "c_pull/D");
	//tNew->Branch("tau_pull", &tau_pull, "tau_pull/D");
	//tNew->Branch("meanBd_pull", &meanBd_pull, "meanBd_pull/D");
	//
	//system("rm -rf "+toyDir+"/plots");
	//for(int nPoisson=0; nPoisson<1000; nPoisson++){
	//	t->GetEntry(nPoisson);

	//	//TPaveText text(0.15, 0.85, 0.35, 0.95, "NDC");
	//	TPaveText text(0.15, 0.75, 0.35, 0.90, "NDC");
	//	text.SetFillColor(0);
	//	text.SetFillStyle(0);
	//	text.SetLineColor(0);
	//	text.SetLineWidth(0);
	//	text.SetBorderSize(1);
	//	text.SetTextSize(0.05);
	//	
	//	text.AddText(yearStr);
	//	text.AddText("nPoisson: "+i2s(nPoisson));
	//	
	//	////---------Build background PDF------
	//	RooRealVar* m = (RooRealVar*) ws->var(fitVar);
	//	
	//	RooRealVar* tau = (RooRealVar*) ws->var("tau");
	//	tau->setVal(tau_D);
	//	//tau->setConstant(true);
	//	
	//	RooExponential* expBkg = new RooExponential("expBkg","background model", *m, *tau) ;
	//	
	//	//// try Ipatia
	//	RooRealVar* alphalBd = (RooRealVar*) ws->var("alphalBd");
	//	RooRealVar* alpharBd = (RooRealVar*) ws->var("alpharBd");
	//	RooRealVar* nlBd = (RooRealVar*) ws->var("nlBd");
	//	RooRealVar* nrBd = (RooRealVar*) ws->var("nrBd");
	//	RooRealVar* sigmaBd = (RooRealVar*) ws->var("sigmaBd");
	//	RooRealVar* meanBd = (RooRealVar*) ws->var("meanBd");
	//	//RooRealVar* fSigmaGausBd = (RooRealVar*) ws->var("fSigmaGausBd");
	//	//RooRealVar* fCB_Bd = (RooRealVar*) ws->var("fCB_Bd");
	//	RooRealVar* beta_Bd = (RooRealVar*) ws->var("beta_Bd");
	//	RooRealVar* zeta_Bd = (RooRealVar*) ws->var("zeta_Bd");
	//	RooRealVar* l_Bd = (RooRealVar*) ws->var("l_Bd");
	//	alphalBd->setConstant(true);
	//	alpharBd->setConstant(true);
	//	nlBd->setConstant(true);
	//	nrBd->setConstant(true);
	//	sigmaBd->setConstant(true);
	//	meanBd->setVal(meanBd_D);
	//	//meanBd->setConstant(true);
	//	//fSigmaGausBd->setConstant(true);
	//	//fCB_Bd->setConstant(true);
	//	beta_Bd->setConstant(true);
	//	zeta_Bd->setConstant(true);
	//	l_Bd->setConstant(true);
	//	
	//	//RooFormulaVar* sigma_gausCB_Bd = new RooFormulaVar("sigma_gausCB_Bd", "sigma_gausCB_Bd", "@0*@1", RooArgList(*sigmaBd, *fSigmaGausBd));
	//	//RooGaussian* gausCB_Bd = new RooGaussian("gausCB_Bd", "gausCB_Bd", *m, *meanBd, *sigma_gausCB_Bd);

	//	////RooIpatia2* modelBd = new RooIpatia2("ipatiaBd", "ipatiaBd", *m, *l_Bd, *zeta_Bd, *beta_Bd, *sigmaBd, *meanBd, *alphalBd, *nlBd, *alpharBd, *nrBd);
	//	//RooCBShape* cbBd = new RooCBShape("cbBd", "cbBd", *m, *meanBd, *sigmaBd, *alphalBd, *nlBd);
	//	//RooAddPdf* modelBd = new RooAddPdf("modelBd", "modelBd", RooArgList(*cbBd, *gausCB_Bd), RooArgList(*fCB_Bd));
	//
	//	RooIpatia2* modelBd = new RooIpatia2("ipatiaBd", "ipatiaBd", *m, *l_Bd, *zeta_Bd, *beta_Bd, *sigmaBd, *meanBd, *alphalBd, *nlBd, *alpharBd, *nrBd);


	//
	//	RooRealVar* m0 = (RooRealVar*) ws->var("m0");
	//	RooRealVar* c = (RooRealVar*) ws->var("c");
	//	m0->setVal(m0_D);
	//	c->setVal(c_D);
	//	//m0->setConstant(true);
	//	//c->setConstant(true);

    //	RooArgusBG* argus_noSmearing = new RooArgusBG("argus_noSmearing","rec background model", *m, *m0, *c);

	//	//// Detector response function
	//	RooRealVar* m0_g = (RooRealVar*) ws->var("m0_g");
	//	m0_g->setConstant(true);
	//	
	//	RooGaussian* gauss = new RooGaussian("gauss", "gauss", *m, *m0_g, *sigmaBd);
	//	RooFFTConvPdf* argusBkg = new RooFFTConvPdf("smearedArgus","argusBkg (X) gauss", *m, *argus_noSmearing, *gauss);
	//	argusBkg->setBufferFraction(0.25);
	//	
	//	
	//	//// for Bs bkg, Bs2DsD 使用Bd的sigma
	//	//RooRealVar* sigmaBs = (RooRealVar*) ws->var("sigmaBs");
	//	RooRealVar* deltaM = new RooRealVar("deltaM", "deltaM", 87.38);
    //	RooFormulaVar* meanBs = new RooFormulaVar("meanBs","meanBs", "@0+@1", RooArgList(*meanBd, *deltaM));
	//	//RooRealVar* meanBs = (RooRealVar*) ws->var("meanBs");
	//	RooRealVar* nlBs = (RooRealVar*) ws->var("nlBs");
	//	RooRealVar* nrBs = (RooRealVar*) ws->var("nrBs");
	//	RooRealVar* alphalBs = (RooRealVar*) ws->var("alphalBs");
	//	RooRealVar* alpharBs = (RooRealVar*) ws->var("alpharBs");
	//	//RooRealVar* fSigmaBs = (RooRealVar*) ws->var("fSigmaBs");
	//	//RooRealVar* fCB_Bs = (RooRealVar*) ws->var("fCB_Bs");
	//	RooRealVar* beta_Bs = (RooRealVar*) ws->var("beta_Bs");
	//	RooRealVar* zeta_Bs = (RooRealVar*) ws->var("zeta_Bs");
	//	RooRealVar* l_Bs = (RooRealVar*) ws->var("l_Bs");
	//	//sigmaBs->setConstant(true);
	//	//meanBs->setConstant(true);
	//	nlBs->setConstant(true);
	//	nrBs->setConstant(true);
	//	alphalBs->setConstant(true);
	//	alpharBs->setConstant(true);
	//	//fSigmaBs->setConstant(true);
	//	//fCB_Bs->setConstant(true);
	//	beta_Bs->setConstant(true);
	//	zeta_Bs->setConstant(true);
	//	l_Bs->setConstant(true);
	//	
	//	//RooFormulaVar* sigma_gausCB_Bs = new RooFormulaVar("sigma_gausCB_Bs", "sigma_gausCB_Bs", "@0*@1", RooArgList(*sigmaBs, *fSigmaBs));
	//	//RooGaussian* gausCB_Bs = new RooGaussian("gausCB_Bs", "gausCB_Bs", *m, *meanBs, *sigma_gausCB_Bs);

	//	////RooDoubleCB* cbBs = new RooDoubleCB("cbBs", "CB for B_{s}", *m, *meanBs, *sigmaBs, *alphalBs, *nlBs, *alpharBs, *nrBs);
	//	//RooCBShape* cbBs = new RooCBShape("cbBs", "cbBs", *m, *meanBs, *sigmaBs, *alphalBs, *nlBs);
	//	//RooAddPdf* modelBs = new RooAddPdf("modelBs", "modelBs", RooArgList(*cbBs, *gausCB_Bs), RooArgList(*fCB_Bs));
	//	RooIpatia2* modelBs = new RooIpatia2("ipatiaBs", "ipatiaBs", *m, *l_Bs, *zeta_Bs, *beta_Bs, *sigmaBd, *meanBs, *alphalBs, *nlBs, *alpharBs, *nrBs);

	//	
	//	RooRealVar* nBd = (RooRealVar*) ws->var("nBd");
	//	RooRealVar* nBs = (RooRealVar*) ws->var("nBs");
	//	RooRealVar* nExpBkg = (RooRealVar*) ws->var("nExpBkg");
	//	RooRealVar* nArgusBkg = (RooRealVar*) ws->var("nArgusBkg");

	//	// 产额也设置下，也许会快一些
	//	nBd->setVal(nBd_D);
	//	nBs->setVal(nBs_D);
	//	nExpBkg->setVal(nExpBkg_D);
	//	nArgusBkg->setVal(nArgusBkg_D);
	//	
	//	RooDataSet *toy_Bd = modelBd->generate(*m, nBd_D);
	//	RooDataSet *toy_Bs = modelBs->generate(*m, nBs_D);
	//	RooDataSet *toy_ExpBkg = expBkg->generate(*m, nExpBkg_D);
	//	RooDataSet *toy_ArgusBkg = argusBkg->generate(*m, nArgusBkg_D);
	//	
	//	RooDataSet* datars = new RooDataSet("datars", "datars", *m);
	//	datars->append(*toy_Bd);
	//	datars->append(*toy_Bs);
	//	datars->append(*toy_ExpBkg);
	//	datars->append(*toy_ArgusBkg);
	//	
	//	cout << nBd_D << ", "<< nBs_D <<  ", "<< nExpBkg_D <<  ", "<< nArgusBkg_D << endl;
	//	
	//	double nTotEvents = nBd_D + nBs_D + nExpBkg_D + nArgusBkg_D;
	//	
	//	RooAddPdf* totPDF = new RooAddPdf("totPDF","Total pdf", RooArgList(*modelBd, *modelBs, *expBkg, *argusBkg), RooArgList(*nBd, *nBs, *nExpBkg, *nArgusBkg));
	//	
	//
	//	
	//	//////////////////// Fit the data/////////////
	//	RooFitResult *result = totPDF->fitTo(*datars, Save(true), NumCPU(10));
	//	//RooFitResult *result = totPDF->fitTo(*datars, Save(true));

	//	nBd_fitVal = nBd->getVal();
	//	nBs_fitVal = nBs->getVal();
	//	m0_fitVal = m0->getVal();
	//	nExpBkg_fitVal = nExpBkg->getVal();
	//	nArgusBkg_fitVal = nArgusBkg->getVal();
	//	c_fitVal = c->getVal();
	//	tau_fitVal = tau->getVal();
	//	meanBd_fitVal = meanBd->getVal();
	//	
	//	nBd_fitErr = nBd->getError();
	//	nBs_fitErr = nBs->getError();
	//	m0_fitErr = m0->getError();
	//	nExpBkg_fitErr = nExpBkg->getError();
	//	nArgusBkg_fitErr = nArgusBkg->getError();
	//	c_fitErr = c->getError();
	//	tau_fitErr = tau->getError();
	//	meanBd_fitErr = meanBd->getError();
	//	
	//	nBd_pull = (nBd_fitVal - nBd_D)/nBd_fitErr;
	//	nBs_pull = (nBs_fitVal - nBs_D)/nBs_fitErr;
	//	m0_pull = (m0_fitVal - m0_D)/m0_fitErr;
	//	nExpBkg_pull = (nExpBkg_fitVal - nExpBkg_D)/nExpBkg_fitErr;
	//	nArgusBkg_pull = (nArgusBkg_fitVal - nArgusBkg_D)/nArgusBkg_fitErr;
	//	c_pull = (c_fitVal - c_D)/c_fitErr;
	//	tau_pull = (tau_fitVal - tau_D)/tau_fitErr;
	//	meanBd_pull = (meanBd_fitVal - meanBd_D)/meanBd_fitErr;
	//	
	//	
	//	RooPlot *mframe = m->frame(Title("Bd2DsD Run2 data"));
	//	
	//	cout<<"Fit the data"<<endl;
	//	
	//	datars->plotOn(mframe, Name("data"));
	//	totPDF->plotOn(mframe, Name("signal Bd"), Components(*modelBd), LineColor(3), LineStyle(kDashed));
	//	totPDF->plotOn(mframe, Name("Bs bkg"), Components(*modelBs), LineColor(6), LineStyle(kDashed));
	//	totPDF->plotOn(mframe, Name("exp bkg"), Components(*expBkg), LineColor(4), LineStyle(kDashed));
	//	totPDF->plotOn(mframe, Name("prc bkg"), Components(*argusBkg), LineColor(kOrange), LineStyle(kDashed));
	//	totPDF->plotOn(mframe, Name("totPDF"), LineColor(kRed));
	//	datars->plotOn(mframe, Name("data"));
	//	//totPDF->paramOn(mframe, Layout(0.65, 0.90, 0.85));
	//	
	//	int result_status = result->status();
	//	cout << "result_status = "<< result_status<<endl;
	//	result->Print("v");
	//	
	//	
	//	RooHist *pull = mframe->pullHist();
	//	pull->GetYaxis()->SetTitle("Pull");
	//	RooPlot *framePull = m->frame();
	//	framePull->addPlotable(pull, "P");
	//	
	//	int binWidth = (int)(5450 - 5150)/80;
	//	mframe->SetTitle(fitVar);
	//	mframe->SetXTitle("#it{m}_{B} [MeV/c^{2}]");
	//	mframe->SetYTitle(Form("Candidates / (%d MeV/c^{2})", binWidth));
	//	mframe->GetXaxis()->SetTitleSize(0.2);
	//	mframe->GetYaxis()->SetTitleSize(0.2);
	//	
	//	framePull->SetTitle("");
	//	framePull->GetYaxis()->SetTitle("Pull");
	//	framePull->SetMinimum(-6.);
	//	framePull->SetMaximum(6.);
	//	framePull->SetMarkerStyle(2);
	//	//cout<<"-----------------1"<<endl;
	//	
	//	TCanvas *canv = new TCanvas("canv", "canv", 800, 600);
	//	canv->SetFillColor(10);
	//	canv->SetBorderMode(0);
	//	canv->cd();
	//	canv->cd(1);
	//	
	//	TPad *p1 = new TPad("pad1", "pad1", 0.02, 0.2, 0.98, 0.98, 0);
	//	TPad *p2 = new TPad("pad2", "pad2", 0.02, 0.04, 0.98, 0.2, 0);
	//	p1->Draw();
	//	p2->Draw();
	//	
	//	p1->cd();
	//	mframe->GetYaxis()->SetTitleOffset(1.00);
	//	mframe->GetYaxis()->SetLabelSize(0.05);
	//	mframe->GetYaxis()->SetTitleSize(0.05);
	//	mframe->GetYaxis()->SetNoExponent();
	//	mframe->GetXaxis()->SetTitleOffset(1.00);
	//	mframe->GetXaxis()->SetTitleSize(0.05);
	//	mframe->GetXaxis()->SetLabelSize(0.05);
	//	mframe->Draw("E1");
	//	text.Draw("same");
	//	
	//	p2->cd();
	//	p2->SetTickx();
	//	framePull->GetYaxis()->SetTitleOffset(0.17);
	//	framePull->GetXaxis()->SetTitleSize(0.0);
	//	framePull->GetXaxis()->SetTickLength(0.09);
	//	framePull->GetYaxis()->SetTitleSize(0.2);
	//	framePull->GetXaxis()->SetLabelSize(0.0);
	//	framePull->GetYaxis()->SetLabelSize(0.2);
	//	framePull->GetYaxis()->SetNdivisions(5);
	//	framePull->Draw("E1");
	//	
	//	TLine* lineZero = new TLine(5150, 0, 5450, 0);
	//	lineZero->SetLineStyle(kDashed);
	//	lineZero->SetLineColor(kBlack);
	//	lineZero->Draw();
	//	
	//	TLine* lineTwoSigUp = new TLine(5150, 3, 5450, 3);
	//	lineTwoSigUp->SetLineColor(kRed);
	//	lineTwoSigUp->Draw();
	//	TLine* lineTwoSigDown = new TLine(5150, -3, 5450, -3);
	//	lineTwoSigDown->SetLineColor(kRed);
	//	lineTwoSigDown->Draw();
	//	
	//	system("mkdir -p "+toyDir+"/plots");
	//	//canv->SaveAs(toyDir+"/plots/"+yearStr+"_data_Bd2DsD_final_fit_"+i2s(nPoisson)+".pdf");
	//	
	//	p1->SetLogy();
	//	canv->SaveAs(toyDir+"/plots/"+yearStr+"_data_Bd2DsD_final_fit_log_"+i2s(nPoisson)+".pdf");
	//	
	//	delete datars;
	//	
	//	tNew->Fill();
	//}
	//cout<< endl;
	//
	//fPoissonTmp.cd();
	//tNew->Write();
	//fPoissonTmp.Close();
	//fPoisson.Close();

	//system("mv " + namePoissonTmp + " " + namefPoisson);
	
	
	 // 读取泊松中心值
	RooRealVar* nBd = (RooRealVar*) ws->var("nBd");
	RooRealVar* nBs = (RooRealVar*) ws->var("nBs");
	RooRealVar* m0 = (RooRealVar*) ws->var("m0");
	RooRealVar* nExpBkg = (RooRealVar*) ws->var("nExpBkg");
	RooRealVar* nArgusBkg = (RooRealVar*) ws->var("nArgusBkg");
	RooRealVar* c = (RooRealVar*) ws->var("c");
	RooRealVar* tau = (RooRealVar*) ws->var("tau");
	RooRealVar* meanBd = (RooRealVar*) ws->var("meanBd");
	
	cout << "Mean: " << nBd->getVal() << ", "<< nBs->getVal() <<  ", "<< m0->getVal() <<  ", "<< nExpBkg->getVal() <<  ", "<< nArgusBkg->getVal() << ", " << c->getVal() << ", " << tau->getVal()  << ", " << meanBd->getVal() << ", " << endl;
	cout << "Poisson vars saved in " + namefPoisson << endl;
	
	
	fWS.Close();



	vector<TString> varNames;
	varNames.push_back("\\texttt{nBd}");
	varNames.push_back("\\texttt{nBs}");
	varNames.push_back("\\texttt{nExpBkg}");
	varNames.push_back("\\texttt{nArgusBkg}");
	varNames.push_back("\\texttt{m0}");
	varNames.push_back("\\texttt{c}");
	varNames.push_back("\\texttt{tau}");
	varNames.push_back("\\texttt{meanBd}");

	vector<double> varSetVals;
	varSetVals.push_back(nBd->getVal());
	varSetVals.push_back(nBs->getVal());
	varSetVals.push_back(nExpBkg->getVal());
	varSetVals.push_back(nArgusBkg->getVal());
	varSetVals.push_back(m0->getVal());
	varSetVals.push_back(c->getVal());
	varSetVals.push_back(tau->getVal());
	varSetVals.push_back(meanBd->getVal());
	

	vector<vector<ValError>> tableInfo  = fitVarsPull(namefPoisson, toyDir);
	for(int i=0; i<tableInfo.size(); i++){
		int nfixed = 1;
		if(varNames[i].Contains("tau")) nfixed=5;

		cout  <<setw(12)<< varNames[i];
		cout  <<setw(12)<< "& "<< dbl2str(varSetVals[i], nfixed);

		for(int j=0; j<tableInfo[0].size(); j++){
			if(j>=2) nfixed = 2;

			if(j!=2 && j!=3)
				cout  <<setw(12)<< "& "<< dbl2str(tableInfo[i][j].val, nfixed);
			else
				cout  <<setw(12)<< "& "<< tableInfo[i][j].roundToError(1, nfixed);
		}
		cout << " \\\\" << endl;
	}
}


vector<vector<ValError>> fitVarsPull(TString nameFile, TString toyDir)
{
	cout << "Fit vars pull using Gaus founction." << endl;
	vector<TString> vars;
	vars.push_back("nBd");
	vars.push_back("nBs");
	vars.push_back("nExpBkg");
	vars.push_back("nArgusBkg");
	vars.push_back("m0");
	vars.push_back("c");
	vars.push_back("tau");
	vars.push_back("meanBd");

	vector<double> pull_edges;
	pull_edges.push_back(1.5);
	pull_edges.push_back(3.);
	pull_edges.push_back(3.5);
	pull_edges.push_back(5.);
	pull_edges.push_back(3.);
	pull_edges.push_back(3.);
	pull_edges.push_back(5.);
	pull_edges.push_back(5.);

	vector<double> bias_edges;
	bias_edges.push_back(240);
	bias_edges.push_back(90);
	bias_edges.push_back(700);
	bias_edges.push_back(500);
	bias_edges.push_back(6);
	bias_edges.push_back(20);
	bias_edges.push_back(0.004);
	bias_edges.push_back(0.3);

	vector<DoubleDouble> fitVal_edges;
	fitVal_edges.push_back(DoubleDouble(32200, 33900));
	fitVal_edges.push_back(DoubleDouble(230, 430));
	fitVal_edges.push_back(DoubleDouble(900, 2200));
	fitVal_edges.push_back(DoubleDouble(3900, 5000));
	fitVal_edges.push_back(DoubleDouble(5200, 5215));
	fitVal_edges.push_back(DoubleDouble(-20, 16));
	fitVal_edges.push_back(DoubleDouble(-0.011, -0.002));
	fitVal_edges.push_back(DoubleDouble(5276, 5283));

	vector<DoubleDouble> fitErr_edges;
	fitErr_edges.push_back(DoubleDouble(170, 205));
	fitErr_edges.push_back(DoubleDouble(19, 28));
	fitErr_edges.push_back(DoubleDouble(90, 450));
	fitErr_edges.push_back(DoubleDouble(90, 350));
	fitErr_edges.push_back(DoubleDouble(0, 20));
	fitErr_edges.push_back(DoubleDouble(0, 80));
	fitErr_edges.push_back(DoubleDouble(5e-4, 1.8e-3));
	fitErr_edges.push_back(DoubleDouble(47e-3, 52e-3));

	vector<vector<ValError>> table(vars.size(), vector<ValError>(5, ValError(0, 0)));
	//var-0: mean of FitVal
	//var-1: mean of FitErr
	//var-2: mean of Pull Gaus
	//var-3: sigma of Pull Gaus
	//var-4: chi2/ndf of Pull fit

	TFile f(nameFile);
	TTree* t = (TTree*)f.Get("DecayTree");
	if(!t) {
		cerr << "No DecayTree in file: " + nameFile << endl;
		return table;
	}

	// fitVal, fitErr histos
	for(int i=0; i<vars.size(); i++){
    	TPaveText text(0.65, 0.85, 0.95, 0.95, "NDC");
	 	text.SetFillColor(0);
	 	text.SetFillStyle(0);
	 	text.SetLineColor(0);
	 	text.SetLineWidth(0);
	 	text.SetBorderSize(1);
	 	//text.SetTextSize(0.08);

	 	RooRealVar observable(vars[i]+"_fitVal", vars[i]+"_fitVal", fitVal_edges[i].one, fitVal_edges[i].two);
		observable.setBins(50);

		TH1D varHist(vars[i]+"_HistoFitVal", vars[i]+"_HistoFitVal", 50, fitVal_edges[i].one, fitVal_edges[i].two);
		t->Draw(vars[i]+"_fitVal>>" + vars[i]+"_HistoFitVal");

		RooDataHist *datars = new RooDataHist("dataset", "dataset", observable, &varHist);
		
	 	RooPlot *mframe = observable.frame(Title(vars[i]+"_fitVal"));
	 	
	 	cout<<"Fit the data"<<endl;
	 	
	 	datars->plotOn(mframe, Name("data"));
		
		double binWidth = 2*(fitVal_edges[i].two - fitVal_edges[i].one)/50;
		mframe->SetTitle(vars[i]+"_fitVal");
		mframe->SetXTitle(vars[i]+"_fitVal");
		mframe->SetYTitle("Candidates / "+d2s(binWidth));
		mframe->GetXaxis()->SetTitleSize(0.2);
		mframe->GetYaxis()->SetTitleSize(0.2);
		
		TCanvas *canv = new TCanvas("canv", "canv", 800, 600);
		canv->SetFillColor(10);
		canv->SetBorderMode(0);
		canv->cd();
		canv->cd(1);
		
		TPad *p1 = new TPad("pad1", "pad1", 0.02, 0.2, 0.98, 0.98, 0);
		p1->Draw();
		
		p1->cd();
		mframe->GetYaxis()->SetTitleOffset(1.00);
		mframe->GetYaxis()->SetLabelSize(0.05);
		mframe->GetYaxis()->SetTitleSize(0.05);
		mframe->GetYaxis()->SetNoExponent();
		mframe->GetXaxis()->SetTitleOffset(1.00);
		mframe->GetXaxis()->SetTitleSize(0.05);
		mframe->GetXaxis()->SetLabelSize(0.05);
		mframe->Draw("E1");
		text.AddText("Mean: "+d2s(varHist.GetMean()));
		text.AddText("Std Dev: "+d2s(varHist.GetMeanError()));
		text.Draw("same");

		table[i][0] = ValError(varHist.GetMean(), 0);
		
		system("mkdir -p "+toyDir);
		canv->SaveAs(toyDir+"/varFitVal_"+vars[i]+".pdf");
	}
	for(int i=0; i<vars.size(); i++){
    	TPaveText text(0.65, 0.85, 0.95, 0.95, "NDC");
	 	text.SetFillColor(0);
	 	text.SetFillStyle(0);
	 	text.SetLineColor(0);
	 	text.SetLineWidth(0);
	 	text.SetBorderSize(1);
	 	//text.SetTextSize(0.08);

	 	RooRealVar observable(vars[i]+"_fitErr", vars[i]+"_fitErr", fitErr_edges[i].one, fitErr_edges[i].two);
		observable.setBins(50);

		TH1D varHist(vars[i]+"_HistoFitErr", vars[i]+"_HistoFitErr", 50, fitErr_edges[i].one, fitErr_edges[i].two);
		t->Draw(vars[i]+"_fitErr>>" + vars[i]+"_HistoFitErr");

		RooDataHist *datars = new RooDataHist("dataset", "dataset", observable, &varHist);
		
	 	RooPlot *mframe = observable.frame(Title(vars[i]+"_fitErr"));
	 	
	 	cout<<"Fit the data"<<endl;
	 	
	 	datars->plotOn(mframe, Name("data"));
		
		double binWidth = 2*(fitErr_edges[i].two - fitErr_edges[i].one)/50;
		mframe->SetTitle(vars[i]+"_fitErr");
		mframe->SetXTitle(vars[i]+"_fitErr");
		mframe->SetYTitle("Candidates / "+d2s(binWidth));
		mframe->GetXaxis()->SetTitleSize(0.2);
		mframe->GetYaxis()->SetTitleSize(0.2);
		
		TCanvas *canv = new TCanvas("canv", "canv", 800, 600);
		canv->SetFillColor(10);
		canv->SetBorderMode(0);
		canv->cd();
		canv->cd(1);
		
		TPad *p1 = new TPad("pad1", "pad1", 0.02, 0.2, 0.98, 0.98, 0);
		p1->Draw();
		
		p1->cd();
		mframe->GetYaxis()->SetTitleOffset(1.00);
		mframe->GetYaxis()->SetLabelSize(0.05);
		mframe->GetYaxis()->SetTitleSize(0.05);
		mframe->GetYaxis()->SetNoExponent();
		mframe->GetXaxis()->SetTitleOffset(1.00);
		mframe->GetXaxis()->SetTitleSize(0.05);
		mframe->GetXaxis()->SetLabelSize(0.05);
		mframe->Draw("E1");
		text.AddText("Mean: "+d2s(varHist.GetMean()));
		text.AddText("Std Dev: "+d2s(varHist.GetMeanError()));
		text.Draw("same");
		
		table[i][1] = ValError(varHist.GetMean(), 0);

		system("mkdir -p "+toyDir);
		canv->SaveAs(toyDir+"/varFitErr_"+vars[i]+".pdf");
	}

	// fit pull
	for(int i=0; i<vars.size(); i++){
    	TPaveText text(0.25, 0.85, 0.45, 0.95, "NDC");
	 	text.SetFillColor(0);
	 	text.SetFillStyle(0);
	 	text.SetLineColor(0);
	 	text.SetLineWidth(0);
	 	text.SetBorderSize(1);
	 	text.SetTextSize(0.08);


	 	RooRealVar observable(vars[i]+"_pull", vars[i]+"_pull", -1.*pull_edges[i], pull_edges[i]);
		observable.setBins(50);

	 	RooDataSet *datars = new RooDataSet("dataset", "dataset", t, observable);
		
		// Detector response function
		RooRealVar mean("#it{#mu}", "m0", 0, -2, 2) ;
		RooRealVar sigma("#it{#sigma}", "sigma", 2, 0.1, 5);
		RooGaussian gauss("gauss", "gauss", observable, mean, sigma);

		if(vars[i] == "c" || vars[i] == "m0"){
			mean.setRange(-0.1, 0.1);

			sigma.setRange(0, 0.2);
			sigma.setVal(0.2);
			//sigma.setConstant(true);
		}
		
	 	RooFitResult *result = gauss.fitTo(*datars, Save(true), NumCPU(10));


	 	RooPlot *mframe = observable.frame(Title(vars[i]+"_pull"));
	 	
	 	cout<<"Fit the data"<<endl;
	 	
	 	datars->plotOn(mframe, Name("data"));
	 	gauss.plotOn(mframe, Name("gauss"), LineColor(kBlue));
	 	datars->plotOn(mframe, Name("data"));
	 	gauss.paramOn(mframe, Layout(0.70, 0.95, 0.95));

		int result_status = result->status();
		cout << "result_status = "<< result_status<<endl;
		result->Print("v");
		
		
		RooHist *pull = mframe->pullHist();
		pull->GetYaxis()->SetTitle("Pull");
		RooPlot *framePull = observable.frame();
		framePull->addPlotable(pull, "P");
		
		double binWidth = 2*pull_edges[i]/50;
		mframe->SetTitle(vars[i]+"_pull");
		mframe->SetXTitle(vars[i]+"_pull");
		mframe->SetYTitle("Candidates / "+d2s(binWidth));
		mframe->GetXaxis()->SetTitleSize(0.2);
		mframe->GetYaxis()->SetTitleSize(0.2);
		
		framePull->SetTitle("");
		framePull->GetYaxis()->SetTitle("Pull");
		framePull->SetMinimum(-6.);
		framePull->SetMaximum(6.);
		framePull->SetMarkerStyle(2);
		//cout<<"-----------------1"<<endl;
		
		TCanvas *canv = new TCanvas("canv", "canv", 800, 600);
		canv->SetFillColor(10);
		canv->SetBorderMode(0);
		canv->cd();
		canv->cd(1);
		
		TPad *p1 = new TPad("pad1", "pad1", 0.02, 0.2, 0.98, 0.98, 0);
		TPad *p2 = new TPad("pad2", "pad2", 0.02, 0.04, 0.98, 0.2, 0);
		p1->Draw();
		p2->Draw();
		
		p1->cd();
		mframe->GetYaxis()->SetTitleOffset(1.00);
		mframe->GetYaxis()->SetLabelSize(0.05);
		mframe->GetYaxis()->SetTitleSize(0.05);
		mframe->GetYaxis()->SetNoExponent();
		mframe->GetXaxis()->SetTitleOffset(1.00);
		mframe->GetXaxis()->SetTitleSize(0.05);
		mframe->GetXaxis()->SetLabelSize(0.05);
		mframe->Draw("E1");
		text.AddText("#chi^{2}/ndf: "+d2s(mframe->chiSquare()));
		text.Draw("same");
		
		p2->cd();
		p2->SetTickx();
		framePull->GetYaxis()->SetTitleOffset(0.17);
		framePull->GetXaxis()->SetTitleSize(0.0);
		framePull->GetXaxis()->SetTickLength(0.09);
		framePull->GetYaxis()->SetTitleSize(0.2);
		framePull->GetXaxis()->SetLabelSize(0.0);
		framePull->GetYaxis()->SetLabelSize(0.2);
		framePull->GetYaxis()->SetNdivisions(5);
		framePull->Draw("E1");
		
		TLine* lineZero = new TLine(-1*pull_edges[i], 0, pull_edges[i], 0);
		lineZero->SetLineStyle(kDashed);
		lineZero->SetLineColor(kBlack);
		lineZero->Draw();
		
		TLine* lineTwoSigUp = new TLine(-1*pull_edges[i], 3, pull_edges[i], 3);
		lineTwoSigUp->SetLineColor(kRed);
		lineTwoSigUp->Draw();
		TLine* lineTwoSigDown = new TLine(-1*pull_edges[i], -3, pull_edges[i], -3);
		lineTwoSigDown->SetLineColor(kRed);
		lineTwoSigDown->Draw();


		table[i][2] = ValError(mean.getVal(), mean.getError());
		table[i][3] = ValError(sigma.getVal(), sigma.getError());
		table[i][4] = ValError(mframe->chiSquare(), 0);

		system("mkdir -p "+toyDir);
		canv->SaveAs(toyDir+"/varPull_"+vars[i]+".pdf");
	}


	// fit bias, bias = fitVal - setVal
	for(int i=0; i<vars.size(); i++){
    	TPaveText text(0.25, 0.85, 0.45, 0.95, "NDC");
	 	text.SetFillColor(0);
	 	text.SetFillStyle(0);
	 	text.SetLineColor(0);
	 	text.SetLineWidth(0);
	 	text.SetBorderSize(1);
	 	text.SetTextSize(0.08);

		TH1D varHist("varBiasHisto_"+vars[i], "varBiasHisto_"+vars[i], 50, -1*bias_edges[i], bias_edges[i]);
		t->Draw(vars[i]+"-"+vars[i]+"_fitVal>>" + "varBiasHisto_"+vars[i]);

	 	RooRealVar observable(vars[i]+"_bias", vars[i]+"_bias", -1.*bias_edges[i], bias_edges[i]);
		observable.setBins(50);

		RooDataHist *datars = new RooDataHist("dataset", "dataset", observable, &varHist);
		
		// Detector response function
		RooRealVar mean("#it{#mu}", "m0", 0, -5, 5) ;
		RooRealVar sigma("#it{#sigma}", "sigma", 10, 0, 200);
		if(vars[i].Contains("nArgus") || vars[i].Contains("nExp")) sigma.setVal(100);
		//if(vars[i].Contains("tau")) sigma.setVal(0.1);

		RooGaussian gauss("gauss", "gauss", observable, mean, sigma);

		
	 	RooFitResult *result = gauss.fitTo(*datars, Save(true), NumCPU(10));


	 	RooPlot *mframe = observable.frame(Title(vars[i]+"_bias"));
	 	
	 	cout<<"Fit the data"<<endl;
	 	
	 	datars->plotOn(mframe, Name("data"));
	 	gauss.plotOn(mframe, Name("gauss"), LineColor(kBlue));
	 	datars->plotOn(mframe, Name("data"));
	 	gauss.paramOn(mframe, Layout(0.70, 0.95, 0.95));

		int result_status = result->status();
		cout << "result_status = "<< result_status<<endl;
		result->Print("v");
		
		
		RooHist *pull = mframe->pullHist();
		pull->GetYaxis()->SetTitle("Pull");
		RooPlot *framePull = observable.frame();
		framePull->addPlotable(pull, "P");
		
		double binWidth = 2*bias_edges[i]/50;
		mframe->SetTitle(vars[i]+"_bias");
		mframe->SetXTitle(vars[i]+"_bias");
		mframe->SetYTitle("Candidates / "+d2s(binWidth));
		mframe->GetXaxis()->SetTitleSize(0.2);
		mframe->GetYaxis()->SetTitleSize(0.2);
		
		framePull->SetTitle("");
		framePull->GetYaxis()->SetTitle("Pull");
		framePull->SetMinimum(-6.);
		framePull->SetMaximum(6.);
		framePull->SetMarkerStyle(2);
		//cout<<"-----------------1"<<endl;
		
		TCanvas *canv = new TCanvas("canv", "canv", 800, 600);
		canv->SetFillColor(10);
		canv->SetBorderMode(0);
		canv->cd();
		canv->cd(1);
		
		TPad *p1 = new TPad("pad1", "pad1", 0.02, 0.2, 0.98, 0.98, 0);
		TPad *p2 = new TPad("pad2", "pad2", 0.02, 0.04, 0.98, 0.2, 0);
		p1->Draw();
		p2->Draw();
		
		p1->cd();
		mframe->GetYaxis()->SetTitleOffset(1.00);
		mframe->GetYaxis()->SetLabelSize(0.05);
		mframe->GetYaxis()->SetTitleSize(0.05);
		mframe->GetYaxis()->SetNoExponent();
		mframe->GetXaxis()->SetTitleOffset(1.00);
		mframe->GetXaxis()->SetTitleSize(0.05);
		mframe->GetXaxis()->SetLabelSize(0.05);
		mframe->Draw("E1");
		text.AddText("#chi^{2}/ndf: "+d2s(mframe->chiSquare()));
		text.Draw("same");
		
		p2->cd();
		p2->SetTickx();
		framePull->GetYaxis()->SetTitleOffset(0.17);
		framePull->GetXaxis()->SetTitleSize(0.0);
		framePull->GetXaxis()->SetTickLength(0.09);
		framePull->GetYaxis()->SetTitleSize(0.2);
		framePull->GetXaxis()->SetLabelSize(0.0);
		framePull->GetYaxis()->SetLabelSize(0.2);
		framePull->GetYaxis()->SetNdivisions(5);
		framePull->Draw("E1");
		
		TLine* lineZero = new TLine(-1*bias_edges[i], 0, bias_edges[i], 0);
		lineZero->SetLineStyle(kDashed);
		lineZero->SetLineColor(kBlack);
		lineZero->Draw();
		
		TLine* lineTwoSigUp = new TLine(-1*bias_edges[i], 3, bias_edges[i], 3);
		lineTwoSigUp->SetLineColor(kRed);
		lineTwoSigUp->Draw();
		TLine* lineTwoSigDown = new TLine(-1*bias_edges[i], -3, bias_edges[i], -3);
		lineTwoSigDown->SetLineColor(kRed);
		lineTwoSigDown->Draw();


		system("mkdir -p "+toyDir);
		canv->SaveAs(toyDir+"/varBias_"+vars[i]+".pdf");
	}

	return table;
}

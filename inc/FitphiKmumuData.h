#ifndef __FITPHIKMUMUDATA_H
#define __FITPHIKMUMUDATA_H

#include <iostream>
#include "RooStats/ModelConfig.h"
#include "RooStats/ProfileLikelihoodCalculator.h"
#include "RooStats/HypoTestResult.h"
#include <RooDataSet.h>

#include "/mnt/d/lhcb/wangla/RphiK/inc/tools.h"
#include "/mnt/d/lhcb/wangla/RphiK/inc/getPresel.h"
#include "/mnt/d/lhcb/wangla/RphiK/inc/fitMCtoGetShape.h"
#include "/mnt/d/lhcb/wangla/RphiK/inc/RooSemiGaussian.h"
#include "/mnt/d/lhcb/wangla/RphiK/inc/RooIpatia2.h"
#include "/mnt/d/lhcb/wangla/RphiK/inc/RooDoubleCB.h"

using namespace RooFit;
using namespace RooStats;

class FitphiKmumuData
{
	public:
	FitphiKmumuData()
	:
	dataFile(""),
	MCFile(""),
	cutStringMC(""),
	cutStringData(""),
	weightMC(""),
	plotdir(""),
	bias_passTrigCat(false)
	{}

	string dataFile;
	string MCFile;

	string cutStringMC;
	string cutStringData;

	string weightMC;
	string plotdir;

	bool bias_passTrigCat;

	void setDataMCFileName(string dataFileName, string MCFileName);
	vector<ValError> fitBmassDTF(int trigCat, RooRealVar& yield, int nPoisson = -1, string tagString="", bool saveModelHisto=false);
	vector<ValError> fitBmass(int trigCat, RooRealVar& yield, int nPoisson = -1, string tagString="", bool saveModelHisto=false);

	void FillHistoWithYields(string nameHistoFile, string plotdir_, int trigCat, int nBins, double binTab[], string binningVar, string nameHisto);
};


void FitphiKmumuData::setDataMCFileName(string dataFileName, string MCFileName){
	dataFile = dataFileName;
	MCFile = MCFileName;
}

vector<ValError> FitphiKmumuData::fitBmassDTF(int trigCat, RooRealVar& yield, int nPoisson, string tagString, bool saveModelHisto){
	vector<ValError> ret;

	//R__LOAD_LIBRARY(RooDoubleCB_cxx)
	//R__LOAD_LIBRARY(libRooFit)
	//---------Build background PDF------

	vector<string> trigCatS = {"MUTOS", "MUTIS"};

	lhcbStyle(0.0063);
	
	RooRealVar m("B_DTFM_M_0", "#it{m}^{DTF}_{#it{#phi}#it{K}^{+}#it{#mu}^{+}#it{#mu}^{-}}", 5180, 5600.);
	
	RooRealVar tau("tau", "tau", -0.001, -1, 1);
	RooExponential expBkg("expBkg", "background model", m, tau);
	
	int nentries;
	TChain chainData;
	chainData.Add((dataFile+"/DecayTree").c_str());

	if(cutStringData == "") cutStringData = "(1)";

	string datacut = "";
	datacut += "(B_DTFM_M_0>5180 && B_DTFM_M_0<5600)";
	if(!bias_passTrigCat) datacut +=  "&& (passTrigCat" +i2s(trigCat)+ " == 1)";
	datacut +=  "&& " + cutStringData;
	
	TRandom rand;

	string toEraseTreeName = "FitphiKmumuData_fitBmassDTF_DataTmp"+i2s(rand.Integer(1000))+".root";

	vector<string> varsToSwitchOn;
	varsToSwitchOn.push_back("B_DTFM_M_0");
	if(nPoisson>=0) varsToSwitchOn.push_back("poissonWeight");
	varsToSwitchOn.push_back(("passTrigCat"+i2s(trigCat)).c_str());

	system("mkdir -p /mnt/d/lhcb/wangla/RphiK/toErase");

	if(nPoisson>=0) copyTreeWithNewVars("/mnt/d/lhcb/wangla/RphiK/toErase/"+toEraseTreeName, dataFile, datacut, "Sum$(poissonWeight["+i2s(nPoisson)+"])", "weightForFit", varsToSwitchOn, "DecayTree");
	else copyTreeWithNewVars("/mnt/d/lhcb/wangla/RphiK/toErase/"+toEraseTreeName, dataFile, datacut, "(1.)", "weightForFit", varsToSwitchOn, "DecayTree");

	TFile fTmpData(("/mnt/d/lhcb/wangla/RphiK/toErase/"+toEraseTreeName).c_str());
	TTree* treeTmpData = (TTree*)fTmpData.Get("DecayTree");

	RooRealVar weight("weightForFit", "weightForFit", 0, 10);

	RooDataSet *datars = new RooDataSet("dataDataSet", "dataDataSet", RooArgSet(m, weight), Import(*treeTmpData), WeightVar( weight.GetName() ));

	RooRealVar sigma 	("sigma", "width of signal peaks", 5.0, 1.0, 20.0);
	RooRealVar mean  	("mean", "position of B_{u} peak", 5281., 5200, 5300);
	RooRealVar al		("al", "Transition point of CB", 1.444, 0. , 20.);
	RooRealVar nl		("nl", "Exponent of CB", 4.55, 0., 20.);
	RooRealVar ar	   ("ar", "Transition point of CB", 1.444, 0. , 20.);
	RooRealVar nr		("nr", "Exponent of CB", 3.37, 0., 20.);

	if(cutStringMC == "") cutStringMC = "(1)";

	string MCCuts = "";
	MCCuts += "(B_DTFM_M_0>5180 && B_DTFM_M_0<5600)";
	if(!bias_passTrigCat) MCCuts +=  "&&(passTrigCat" +i2s(trigCat)+ " == 1)";
	MCCuts +=  "&&(rdmNumber < 0.1)";
	MCCuts +=  "&& " + cutStringMC;

	TPaveText text(0.15, 0.65, 0.35, 0.95, "NDC");
	text.SetFillColor(0);
	text.SetFillStyle(0);
	text.SetLineColor(0);
	text.SetLineWidth(0);
	text.SetBorderSize(1);
	text.SetTextSize(2*0.05);
	
	string trigText;
	if(trigCat==0) trigText = "#it{#mu}TOS";
	if(trigCat==1) trigText = "#it{#mu}TIS";
	if(bias_passTrigCat) trigText = "biased trigCat";
	
	text.AddText( (trigText).c_str() );

	system(("mkdir -p " + plotdir).c_str());
	fitMCBMassDTFJpsiMuMu(MCFile.c_str(), MCCuts, weightMC, 5180, 5600,
	sigma, mean, al, nl, ar, nr,
	"JpsiMuMu_B_DTFM_M_sPlot", text, plotdir+"/MC_phiKJpsiMuMu_BmassDTF_" +trigCatS[trigCat]+ ".pdf", false);

	sigma.setConstant(true);
	mean.setConstant(true);
	al.setConstant(true);
	nl.setConstant(true);
	ar.setConstant(true);
	nr.setConstant(true);

	
	//prepare the fit function
	RooRealVar shift("shift", "#Delta_{#mu}", 1.15, -5, 5);
	RooRealVar scale("scale", "s_{#sigma}", 1.2, 0.9, 1.3);
	
	RooFormulaVar shiftedMean("shiftedMean", "shiftedMean", "@0+@1", RooArgList(mean, shift) );
	RooFormulaVar scaledSigma("scaledSigma", "scaledSigma", "@0*@1", RooArgList(sigma, scale) );

	RooDoubleCB cbBu("cbBu", "CB for B_{d}", m, shiftedMean, scaledSigma, al, nl, ar, nr);
	
	RooRealVar nsig("nsig", "Number of signal events", 2800, 0., 1e7);
	RooRealVar nbkg("nbkg", "Number of bkg events", 2800, 0., 1e7);
	
	RooAddPdf* totPDF = new RooAddPdf("totPDF", "Total pdf", RooArgList(cbBu, expBkg), RooArgList(nsig, nbkg));


	RooFitResult *result = totPDF->fitTo(*datars, Save(true));

	TColor* col1 = gROOT->GetColor(92);
	col1->SetRGB(0.992, 0.6823, 0.3804);  //orange for comb


	//m.setBins(50);
	//m.SetTitle("m^{DTF}_{J/#psi}({#phi}K^{+}{#mu}^{+}{#mu}^{-})");

	RooPlot *mframe = m.frame();
	datars->plotOn(mframe);
	totPDF->plotOn(mframe, RooFit::LineColor(kRed) );
	cout << "chi^2 = " << mframe->chiSquare() << endl;


	totPDF->plotOn(mframe, Name("background"), Components(expBkg), FillColor(92), LineColor(92), DrawOption("F"));
	totPDF->plotOn(mframe, Name("signal Bu"), Components(cbBu), RooFit::LineColor(kBlack), LineStyle(2));
	totPDF->plotOn(mframe, RooFit::LineColor(kRed));
	//datars->plotOn(mframe, Binning(80));
	datars->plotOn(mframe);

	totPDF->paramOn(mframe, Layout(0.55, 0.85, 0.85));

	int result_status = result->status();
	cout << "result_status = "<< result_status<<endl;
	result->Print("v");


	RooHist *pull = mframe->pullHist();
	pull->GetYaxis()->SetTitle("Pull");
	RooPlot *framePull = m.frame();
	framePull->addPlotable(pull, "P");

	int binWidth = (int)(5600-5180)/80;
	mframe->SetTitle("JpsiMuMu_B_DTFM_M_sPlot");
	mframe->SetXTitle("#it{m}^{DTF}_{#it{#phi}#it{K}^{+}#it{#mu}^{+}#it{#mu}^{-}} [MeV/c^{2}]");
	mframe->SetYTitle(Form("Candidates / (%d MeV/c^{2})", binWidth));
	mframe->GetXaxis()->SetTitleSize(0.2);
	mframe->GetYaxis()->SetTitleSize(0.2);

	framePull->SetTitle("");
	framePull->GetYaxis()->SetTitle("Pull");
	framePull->SetMinimum(-6.);
	framePull->SetMaximum(6.);
	framePull->SetMarkerStyle(2);
	//cout<<"-----------------1"<<endl;

	TCanvas *canv = new TCanvas("phiKJpsiMuMu", "phiKJpsiMuMu", 800, 600);
	canv->SetFillColor(10);
	canv->SetBorderMode(0);
	canv->cd();
	canv->cd(1);

	TPad *p1 = new TPad("pad1", "pad1", 0.02, 0.2, 0.98, 0.98, 0);
	TPad *p2 = new TPad("pad2", "pad2", 0.02, 0.04, 0.98, 0.2, 0);
	p1->Draw();
	p2->Draw();

	p1->cd();
	mframe->GetYaxis()->SetTitleOffset(1.00);
	mframe->GetYaxis()->SetLabelSize(0.05);
	mframe->GetYaxis()->SetTitleSize(0.05);
	mframe->GetYaxis()->SetNoExponent();
	mframe->GetXaxis()->SetTitleOffset(1.00);
	mframe->GetXaxis()->SetTitleSize(0.05);
	mframe->GetXaxis()->SetLabelSize(0.05);
	mframe->Draw("E1");
	p1->SetLogy();

	text.Draw("same");


	p2->cd();
	p2->SetTickx();
	framePull->GetYaxis()->SetTitleOffset(0.17);
	framePull->GetXaxis()->SetTitleSize(0.0);
	framePull->GetXaxis()->SetTickLength(0.09);
	framePull->GetYaxis()->SetTitleSize(0.2);
	framePull->GetXaxis()->SetLabelSize(0.0);
	framePull->GetYaxis()->SetLabelSize(0.2);
	framePull->GetYaxis()->SetNdivisions(5);
	framePull->Draw("E1");

	TLine* lineZero = new TLine(5180, 0, 5600, 0);
	lineZero->SetLineStyle(kDashed);
	lineZero->SetLineColor(kBlack);
	lineZero->Draw();

	TLine* lineTwoSigUp = new TLine(5180, 3, 5600, 3);
	lineTwoSigUp->SetLineColor(kRed);
	lineTwoSigUp->Draw();
	TLine* lineTwoSigDown = new TLine(5180, -3, 5600, -3);
	lineTwoSigDown->SetLineColor(kRed);
	lineTwoSigDown->Draw();

	gPad->Update();
	canv->Update();
	
	canv->SaveAs((plotdir + "/data_phiKJpsiMuMu_BmassDTF_" +trigCatS[trigCat]+ ".pdf").c_str());

	yield.setVal(nsig.getVal());
	yield.setError(nsig.getError());

	ret.push_back(ValError(scale.getVal(), scale.getError()));
	ret.push_back(ValError(shift.getVal(), shift.getError()));
	ret.push_back(ValError(mean.getVal(), mean.getError()));

	return ret;
}

vector<ValError> FitphiKmumuData::fitBmass(int trigCat, RooRealVar& yield, int nPoisson, string tagString, bool saveModelHisto){
	vector<ValError> ret;

	//R__LOAD_LIBRARY(RooDoubleCB_cxx)
	//R__LOAD_LIBRARY(libRooFit)
	//---------Build background PDF------

	vector<string> trigCatS = {"MUTOS", "MUTIS"};

	lhcbStyle(0.0063);
	
	RooRealVar m("B_M", "#it{m}^{DTF}_{#it{#phi}#it{K}^{+}#it{#mu}^{+}#it{#mu}^{-}}", 5180, 5600.);
	
	RooRealVar tau("tau", "tau", -0.001, -1, 1);
	RooExponential expBkg("expBkg", "background model", m, tau);

	RooRealVar argus_p("argus_p","argus shape parameter", 10.0, 1., 100.);
	RooArgusBG argusBkg("argusBkg","argus background model", m, RooConst(5100), argus_p);


	// muon 的左边部分重建本底可以忽略
	RooRealVar fracExp("fracExp", "fracExp", 1.0);
	RooFormulaVar fracArgus("fracArgus", "fracArgus", "1-@0", RooArgList(fracExp));

	RooAddPdf modelBkg("modelBkg", "modelBkg", RooArgList(expBkg, argusBkg), RooArgList(fracExp, fracArgus));
	
	int nentries;
	TChain chainData;
	chainData.Add((dataFile+"/DecayTree").c_str());

	if(cutStringData == "") cutStringData = "(1)";

	string datacut = "";
	datacut += "(B_M>5180 && B_M<5600)";
	if(!bias_passTrigCat) datacut +=  "&& (passTrigCat" +i2s(trigCat)+ " == 1)";
	datacut +=  "&& " + cutStringData;
	
	TRandom rand;

	string toEraseTreeName = "FitphiKmumuData_fitBmassDTF_DataTmp"+i2s(rand.Integer(1000))+".root";

	vector<string> varsToSwitchOn;
	varsToSwitchOn.push_back("B_M");
	if(nPoisson>=0) varsToSwitchOn.push_back("poissonWeight");
	varsToSwitchOn.push_back(("passTrigCat"+i2s(trigCat)).c_str());

	system("mkdir -p /mnt/d/lhcb/wangla/RphiK/toErase");

	if(nPoisson>=0) copyTreeWithNewVars("/mnt/d/lhcb/wangla/RphiK/toErase/"+toEraseTreeName, dataFile, datacut, "Sum$(poissonWeight["+i2s(nPoisson)+"])", "weightForFit", varsToSwitchOn, "DecayTree");
	else copyTreeWithNewVars("/mnt/d/lhcb/wangla/RphiK/toErase/"+toEraseTreeName, dataFile, datacut, "(1.)", "weightForFit", varsToSwitchOn, "DecayTree");

	TFile fTmpData(("/mnt/d/lhcb/wangla/RphiK/toErase/"+toEraseTreeName).c_str());
	TTree* treeTmpData = (TTree*)fTmpData.Get("DecayTree");

	RooRealVar weight("weightForFit", "weightForFit", 0, 10);

	RooDataSet *datars = new RooDataSet("dataDataSet", "dataDataSet", RooArgSet(m, weight), Import(*treeTmpData), WeightVar( weight.GetName() ));

	RooRealVar sigma 	("sigma", "width of signal peaks", 5.0, 1.0, 20.0);
	RooRealVar mean  	("mean", "position of B_{u} peak", 5281., 5200, 5300);
	RooRealVar al		("al", "Transition point of CB", 1.444, 0. , 20.);
	RooRealVar nl		("nl", "Exponent of CB", 4.55, 0., 20.);
	RooRealVar ar	   ("ar", "Transition point of CB", 1.444, 0. , 20.);
	RooRealVar nr		("nr", "Exponent of CB", 3.37, 0., 20.);

	if(cutStringMC == "") cutStringMC = "(1)";

	string MCCuts = "";
	MCCuts += "(B_M>5180 && B_M<5600)";
	if(!bias_passTrigCat) MCCuts +=  "&&(passTrigCat" +i2s(trigCat)+ " == 1)";
	MCCuts +=  "&&(rdmNumber < 0.1)";
	MCCuts +=  "&& " + cutStringMC;

	TPaveText text(0.15, 0.65, 0.35, 0.95, "NDC");
	text.SetFillColor(0);
	text.SetFillStyle(0);
	text.SetLineColor(0);
	text.SetLineWidth(0);
	text.SetBorderSize(1);
	text.SetTextSize(2*0.05);
	
	string trigText;
	if(trigCat==0) trigText = "#it{#mu}TOS";
	if(trigCat==1) trigText = "#it{#mu}TIS";
	if(bias_passTrigCat) trigText = "biased trigCat";
	
	text.AddText( (trigText).c_str() );

	system(("mkdir -p " + plotdir).c_str());
	fitMCBMassJpsiMuMu(MCFile.c_str(), MCCuts, weightMC, 5180, 5600,
	sigma, mean, al, nl, ar, nr,
	"JpsiMuMu_B_DTFM_M_sPlot", text, plotdir+"/MC_phiKJpsiMuMu_BmassDTF_" +trigCatS[trigCat]+ ".pdf", false);

	sigma.setConstant(true);
	mean.setConstant(true);
	al.setConstant(true);
	nl.setConstant(true);
	ar.setConstant(true);
	nr.setConstant(true);

	
	//prepare the fit function
	RooRealVar shift("shift", "#Delta_{#mu}", 1.15, -5, 5);
	RooRealVar scale("scale", "s_{#sigma}", 1.2, 0.9, 1.3);
	
	RooFormulaVar shiftedMean("shiftedMean", "shiftedMean", "@0+@1", RooArgList(mean, shift) );
	RooFormulaVar scaledSigma("scaledSigma", "scaledSigma", "@0*@1", RooArgList(sigma, scale) );

	RooDoubleCB cbBu("cbBu", "CB for B_{d}", m, shiftedMean, scaledSigma, al, nl, ar, nr);
	
	RooRealVar nsig("nsig", "Number of signal events", 2800, 0., 1e7);
	RooRealVar nbkg("nbkg", "Number of bkg events", 2800, 0., 1e7);
	
	RooAddPdf* totPDF = new RooAddPdf("totPDF", "Total pdf", RooArgList(cbBu, modelBkg), RooArgList(nsig, nbkg));


	RooFitResult *result = totPDF->fitTo(*datars, Save(true));

	TColor* col1 = gROOT->GetColor(92);
	col1->SetRGB(0.992, 0.6823, 0.3804);  //orange for comb


	//m.setBins(50);
	//m.SetTitle("m^{DTF}_{J/#psi}({#phi}K^{+}{#mu}^{+}{#mu}^{-})");

	RooPlot *mframe = m.frame();
	datars->plotOn(mframe);
	totPDF->plotOn(mframe, RooFit::LineColor(kRed) );
	cout << "chi^2 = " << mframe->chiSquare() << endl;

	// orange for exp background
	totPDF->plotOn(mframe, Name("exp background"), Components(expBkg), FillColor(92), LineColor(92), DrawOption("F"));
	// pink for argus background
	totPDF->plotOn(mframe, Name("argus background"), Components(argusBkg), FillColor(6), LineColor(6), DrawOption("F"));
	totPDF->plotOn(mframe, Name("signal Bu"), Components(cbBu), RooFit::LineColor(kBlack), LineStyle(2));
	totPDF->plotOn(mframe, RooFit::LineColor(kRed));
	//datars->plotOn(mframe, Binning(80));
	datars->plotOn(mframe);

	totPDF->paramOn(mframe, Layout(0.55, 0.85, 0.85));

	int result_status = result->status();
	cout << "result_status = "<< result_status<<endl;
	result->Print("v");


	RooHist *pull = mframe->pullHist();
	pull->GetYaxis()->SetTitle("Pull");
	RooPlot *framePull = m.frame();
	framePull->addPlotable(pull, "P");

	int binWidth = (int)(5600-5180)/80;
	mframe->SetTitle("JpsiMuMu_B_DTFM_M_sPlot");
	mframe->SetXTitle("#it{m}^{DTF}_{#it{#phi}#it{K}^{+}#it{#mu}^{+}#it{#mu}^{-}} [MeV/c^{2}]");
	mframe->SetYTitle(Form("Candidates / (%d MeV/c^{2})", binWidth));
	mframe->GetXaxis()->SetTitleSize(0.2);
	mframe->GetYaxis()->SetTitleSize(0.2);

	framePull->SetTitle("");
	framePull->GetYaxis()->SetTitle("Pull");
	framePull->SetMinimum(-6.);
	framePull->SetMaximum(6.);
	framePull->SetMarkerStyle(2);
	//cout<<"-----------------1"<<endl;

	TCanvas *canv = new TCanvas("phiKJpsiMuMu", "phiKJpsiMuMu", 800, 600);
	canv->SetFillColor(10);
	canv->SetBorderMode(0);
	canv->cd();
	canv->cd(1);

	TPad *p1 = new TPad("pad1", "pad1", 0.02, 0.2, 0.98, 0.98, 0);
	TPad *p2 = new TPad("pad2", "pad2", 0.02, 0.04, 0.98, 0.2, 0);
	p1->Draw();
	p2->Draw();

	p1->cd();
	mframe->GetYaxis()->SetTitleOffset(1.00);
	mframe->GetYaxis()->SetLabelSize(0.05);
	mframe->GetYaxis()->SetTitleSize(0.05);
	mframe->GetYaxis()->SetNoExponent();
	mframe->GetXaxis()->SetTitleOffset(1.00);
	mframe->GetXaxis()->SetTitleSize(0.05);
	mframe->GetXaxis()->SetLabelSize(0.05);
	mframe->Draw("E1");
	p1->SetLogy();

	text.Draw("same");


	p2->cd();
	p2->SetTickx();
	framePull->GetYaxis()->SetTitleOffset(0.17);
	framePull->GetXaxis()->SetTitleSize(0.0);
	framePull->GetXaxis()->SetTickLength(0.09);
	framePull->GetYaxis()->SetTitleSize(0.2);
	framePull->GetXaxis()->SetLabelSize(0.0);
	framePull->GetYaxis()->SetLabelSize(0.2);
	framePull->GetYaxis()->SetNdivisions(5);
	framePull->Draw("E1");

	TLine* lineZero = new TLine(5180, 0, 5600, 0);
	lineZero->SetLineStyle(kDashed);
	lineZero->SetLineColor(kBlack);
	lineZero->Draw();

	TLine* lineTwoSigUp = new TLine(5180, 3, 5600, 3);
	lineTwoSigUp->SetLineColor(kRed);
	lineTwoSigUp->Draw();
	TLine* lineTwoSigDown = new TLine(5180, -3, 5600, -3);
	lineTwoSigDown->SetLineColor(kRed);
	lineTwoSigDown->Draw();

	gPad->Update();
	canv->Update();
	
	canv->SaveAs((plotdir + "/data_phiKJpsiMuMu_BmassDTF_" +trigCatS[trigCat]+ ".pdf").c_str());

	yield.setVal(nsig.getVal());
	yield.setError(nsig.getError());

	ret.push_back(ValError(scale.getVal(), scale.getError()));
	ret.push_back(ValError(shift.getVal(), shift.getError()));
	ret.push_back(ValError(mean.getVal(), mean.getError()));

	return ret;
}


void FitphiKmumuData::FillHistoWithYields(string nameHistoFile, string plotdir_, int trigCat, int nBins, double binTab[], string binningVar, string nameHisto){
	TFile f(nameHistoFile.c_str(), "UPDATE" );
	
	if(nameHisto == "") nameHisto = "yieldsTrig"+i2s(trigCat);
	TH1D h( nameHisto.c_str(), nameHisto.c_str(), nBins, binTab);
	h.Sumw2();
	
	h.GetXaxis()->SetTitleSize(0.05);
	h.GetYaxis()->SetTitleSize(0.05);
	h.GetXaxis()->SetLabelSize(0.05);
	h.GetYaxis()->SetLabelSize(0.05);
	h.GetYaxis()->SetTitleOffset(1.2);
	
	string saveCutMC(cutStringMC);
	string saveCutData(cutStringData);
	
	string binCut("");
	RooRealVar yield("yield", "yield", 0, 0, 1e6);
	
	system( ("mkdir -p "+plotdir_).c_str() );
	
	for(int i(1); i<=nBins; ++i)
	{
		binCut = binningVar +">"+d2s(binTab[i-1])+" && "+binningVar+"<"+d2s(binTab[i]);
		plotdir = plotdir_+"/bin"+i2s(i)+"/";

		cutStringMC = binCut;
		if(saveCutMC != "") cutStringMC += " && "+saveCutMC;

		cutStringData = binCut;
		if(saveCutData != "") cutStringData += " && "+saveCutData;

		system( ("mkdir -p "+plotdir).c_str() );

		fitBmassDTF(trigCat, yield);

		h.SetBinContent(i, yield.getVal());
		h.SetBinError(i, yield.getError());

		cout<<endl<<endl<<"fit "<<i<<"/"<<nBins<<" done"<<endl<<endl<<endl;
	}
	
	h.GetXaxis()->SetTitle(binningVar.c_str() );
	h.GetYaxis()->SetTitle("signal yield" );
	
	f.cd();
	h.Write("", TObject::kOverwrite );
	
	TCanvas canv("canv", "canv", 800, 600);
	h.Draw("E1");
	canv.Print((plotdir_+"/yieldsTrig"+i2s(trigCat)+".png").c_str());
	
	plotdir = plotdir_;
	cutStringMC = saveCutMC;
	cutStringData = saveCutData;
	
	f.Close();
}
#endif

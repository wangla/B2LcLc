#ifndef __FIT_BMASS_POLYNOMIALBKG_H
#define __FIT_BMASS_POLYNOMIALBKG_H

#include <iostream>
#include "RooStats/ModelConfig.h"
#include "RooStats/ProfileLikelihoodCalculator.h"
#include "RooStats/HypoTestResult.h"
#include <RooDataSet.h>

using namespace RooFit;
using namespace RooStats;

R__LOAD_LIBRARY(RooDoubleCB_cxx)
R__LOAD_LIBRARY(libRooFit)

double fit_B_M_polynomialBkg(TTree *tree, TCut datacut, double fit_dn, double fit_up, double return_dn, double return_up){

//---------Build background PDF------

RooRealVar m("B_M", "#it{m}_{phiKJpsimm}", fit_dn, fit_up);
RooArgSet obs(m);

RooRealVar coeff_a("a", "coefficient #0", -1.428E-4, -4E-4, -1E-5);
RooRealVar coeff_b("b", "coefficient #1", 1.508, 0, 4.8);
RooRealVar coeff_c("c", "coefficient #2", -3927, -10000, -3000);
RooPolynomial bkg("bkg", "background Chebychev polynomial", m, RooArgList(coeff_c, coeff_b, coeff_a));

TTree *fitTree = tree->CopyTree(datacut.GetTitle());

RooDataSet *datars = new RooDataSet("signal shape", "sigshape", fitTree, obs);
RooRealVar sigma_Bu("#sigma_Bu", "width of signal peaks", 5.0, 1.0, 20.0);
RooRealVar muBu("#mu_{B_{u}}", "position of B_{u} peak", 5279., 5250, 5300);

RooRealVar nlBu("nlBu", "Exponent of CB", Jpsimm_RooDCB_B_M_nl);
RooRealVar nrBu("nrBu", "Exponent of CB", Jpsimm_RooDCB_B_M_nr);
RooRealVar alphalBu("#alpha_lBu", "Transition point of CB", Jpsimm_RooDCB_B_M_al);
RooRealVar alpharBu("#alpha_rBu", "Transition point of CB", Jpsimm_RooDCB_B_M_ar);

RooDoubleCB cbBu("cbBu", "CB for B_{d}", m, muBu, sigma_Bu, alphalBu, nlBu, alpharBu, nrBu);

RooRealVar *nBu = new RooRealVar("nBu", "Number of signal events", 2800, 0., 1e7);
RooRealVar *nBkg = new RooRealVar("nBkg", "Number of bkg events", 2800, 0., 1e7);

RooAddPdf* totPDF = new RooAddPdf("totPDF", "Total pdf", RooArgList(cbBu, bkg), RooArgList(*nBu, *nBkg));

RooFitResult *result = totPDF->fitTo(*datars, Save(true), NumCPU(10));
RooPlot *mframe1 = m.frame(Title("mass frame"));

datars->plotOn(mframe1, Name("data"), XErrorSize(1), Binning((int)((fit_up-fit_dn)/9)));

totPDF->plotOn(mframe1, Name("signal Bu"), Components(cbBu), LineColor(2), LineStyle(kDashed));
totPDF->plotOn(mframe1, Name("background"), Components(bkg), LineColor(4), LineStyle(kDashed));
totPDF->plotOn(mframe1);
totPDF->paramOn(mframe1, Layout(0.65, 0.85, 0.95));


int result_status = result->status();

cout << "result_status = "<< result_status<<endl;

result->Print("v");

RooPlot *framePull = m.frame();
cout << "chi^2 = " << mframe1->chiSquare() << endl;

RooHist *pull = mframe1->pullHist();
pull->SetFillColor(39);
pull->SetLineColor(2);
pull->GetYaxis()->SetTitle("Pull");
framePull->addPlotable(pull, "3L");
//cout<<"-----------------1"<<endl;

TCanvas *c1 = new TCanvas("phiKJpsimm", "phiKJpsimm", 1200, 800);

gPad->SetLeftMargin(20);

c1->cd();
TPad *p2 = new TPad("p2", "p2Y", 0.0, 0.0, 1.0, 0.25, 0, 0, 0);
p2->SetTopMargin(0.015);
p2->SetBottomMargin(0.45);
p2->SetFillStyle(0);
p2->Draw();

TPad *p1 = new TPad("c1", "c1", 0.0, 0.25, 1.0, 1.0, 0, 0, 0);
p1->SetTopMargin(0.022);
p1->SetBottomMargin(0.08);
p1->Draw();

p1->cd();
gPad->SetLeftMargin(0.15);
mframe1->GetYaxis()->SetTitleOffset(1.00);
mframe1->GetYaxis()->SetLabelSize(0.07);
mframe1->GetYaxis()->SetTitleSize(0.07);
mframe1->GetYaxis()->SetNoExponent();
mframe1->GetXaxis()->SetTitleSize(0.0);
mframe1->GetXaxis()->SetLabelSize(0.0);
mframe1->Draw();


p2->cd();
gPad->SetLeftMargin(0.15);
framePull->SetFillStyle(0);
framePull->GetYaxis()->SetTitleSize(fabs(fit_dn-fit_up)/2500.0);
framePull->GetXaxis()->SetTitleSize(fabs(fit_dn-fit_up)/2500.0);
framePull->GetYaxis()->SetLabelSize(fabs(fit_dn-fit_up)/2500.0);
framePull->GetXaxis()->SetLabelSize(fabs(fit_dn-fit_up)/2500.0);
framePull->GetYaxis()->SetTitleOffset(1.4);
framePull->GetXaxis()->SetTitleOffset(1.0);
framePull->GetYaxis()->SetNdivisions(205, kTRUE);
framePull->SetMaximum(6.0);
framePull->SetMinimum(-6.0);
framePull->SetTitle("");
framePull->Draw();

m.setRange("SignalRegion", return_dn, return_up);
double S = nBu->getVal()* (cbBu.createIntegral(m, m, "SignalRegion")->getVal());
double B = nBkg->getVal()* (bkg.createIntegral(m, m, "SignalRegion")->getVal());

delete fitTree;
delete datars;

c1->SaveAs( Form("phiKJpsimm_data_%s_unbinned.pdf", "B_M") );

//gROOT->ProcessLine(".q");
return S;
}

double fit_Jpsimm_B_DTFM_M_0_polynomialBkg(TTree *tree, TCut datacut, double fit_dn, double fit_up, double return_dn, double return_up){

//---------Build background PDF------

RooRealVar m("B_DTFM_M_0", "#it{m}_{phiKJpsimm}", fit_dn, fit_up);
RooArgSet obs(m);

RooRealVar coeff_a("a", "coefficient #0", -1.428E-4, -4E-4, -1E-5);
RooRealVar coeff_b("b", "coefficient #1", 1.508, 0, 4.8);
RooRealVar coeff_c("c", "coefficient #2", -3927, -10000, -3000);
RooPolynomial bkg("bkg", "background Chebychev polynomial", m, RooArgList(coeff_c, coeff_b, coeff_a));

TTree *fitTree = tree->CopyTree(datacut.GetTitle());

RooDataSet *datars = new RooDataSet("signal shape", "sigshape", fitTree, obs);
RooRealVar sigma_Bu("#sigma_Bu", "width of signal peaks", 5.0, 1.0, 20.0);
RooRealVar muBu("#mu_{B_{u}}", "position of B_{u} peak", 5279., 5250, 5300);

RooRealVar nlBu("nlBu", "Exponent of CB", Jpsimm_RooDCB_B_DTFM_M_0_nl);
RooRealVar nrBu("nrBu", "Exponent of CB", Jpsimm_RooDCB_B_DTFM_M_0_nr);
RooRealVar alphalBu("#alpha_lBu", "Transition point of CB", Jpsimm_RooDCB_B_DTFM_M_0_al);
RooRealVar alpharBu("#alpha_rBu", "Transition point of CB", Jpsimm_RooDCB_B_DTFM_M_0_ar);

RooDoubleCB cbBu("cbBu", "CB for B_{d}", m, muBu, sigma_Bu, alphalBu, nlBu, alpharBu, nrBu);

RooRealVar *nBu = new RooRealVar("nBu", "Number of signal events", 2800, 0., 1e7);
RooRealVar *nBkg = new RooRealVar("nBkg", "Number of bkg events", 2800, 0., 1e7);

RooAddPdf* totPDF = new RooAddPdf("totPDF", "Total pdf", RooArgList(cbBu, bkg), RooArgList(*nBu, *nBkg));

RooFitResult *result = totPDF->fitTo(*datars, Save(true), NumCPU(10));
RooPlot *mframe1 = m.frame(Title("mass frame"));

datars->plotOn(mframe1, Name("data"), XErrorSize(1), Binning((int)((fit_up-fit_dn)/9)));

totPDF->plotOn(mframe1, Name("signal Bu"), Components(cbBu), LineColor(2), LineStyle(kDashed));
totPDF->plotOn(mframe1, Name("background"), Components(bkg), LineColor(4), LineStyle(kDashed));
totPDF->plotOn(mframe1);
totPDF->paramOn(mframe1, Layout(0.65, 0.85, 0.95));


int result_status = result->status();

cout << "result_status = "<< result_status<<endl;

result->Print("v");

RooPlot *framePull = m.frame();
cout << "chi^2 = " << mframe1->chiSquare() << endl;

RooHist *pull = mframe1->pullHist();
pull->SetFillColor(39);
pull->SetLineColor(2);
pull->GetYaxis()->SetTitle("Pull");
framePull->addPlotable(pull, "3L");
//cout<<"-----------------1"<<endl;

TCanvas *c1 = new TCanvas("phiKJpsimm", "phiKJpsimm", 1200, 800);

gPad->SetLeftMargin(20);

c1->cd();
TPad *p2 = new TPad("p2", "p2Y", 0.0, 0.0, 1.0, 0.25, 0, 0, 0);
p2->SetTopMargin(0.015);
p2->SetBottomMargin(0.45);
p2->SetFillStyle(0);
p2->Draw();

TPad *p1 = new TPad("c1", "c1", 0.0, 0.25, 1.0, 1.0, 0, 0, 0);
p1->SetTopMargin(0.022);
p1->SetBottomMargin(0.08);
p1->Draw();

p1->cd();
gPad->SetLeftMargin(0.15);
mframe1->GetYaxis()->SetTitleOffset(1.00);
mframe1->GetYaxis()->SetLabelSize(0.07);
mframe1->GetYaxis()->SetTitleSize(0.07);
mframe1->GetYaxis()->SetNoExponent();
mframe1->GetXaxis()->SetTitleSize(0.0);
mframe1->GetXaxis()->SetLabelSize(0.0);
mframe1->Draw();


p2->cd();
gPad->SetLeftMargin(0.15);
framePull->SetFillStyle(0);
framePull->GetYaxis()->SetTitleSize(fabs(fit_dn-fit_up)/16800.0);
framePull->GetXaxis()->SetTitleSize(fabs(fit_dn-fit_up)/16800.0);
framePull->GetYaxis()->SetLabelSize(fabs(fit_dn-fit_up)/16800.0);
framePull->GetXaxis()->SetLabelSize(fabs(fit_dn-fit_up)/16800.0);
framePull->GetYaxis()->SetTitleOffset(1.4);
framePull->GetXaxis()->SetTitleOffset(1.0);
framePull->GetYaxis()->SetNdivisions(205, kTRUE);
framePull->SetMaximum(6.0);
framePull->SetMinimum(-6.0);
framePull->SetTitle("");
framePull->Draw();

m.setRange("SignalRegion", return_dn, return_up);
double S = nBu->getVal()* (cbBu.createIntegral(m, m, "SignalRegion")->getVal());
double B = nBkg->getVal()* (bkg.createIntegral(m, m, "SignalRegion")->getVal());

delete fitTree;
delete datars;

c1->SaveAs( Form("phiKJpsimm_data_%s_unbinned.pdf", "B_DTFM_M_0") );

//gROOT->ProcessLine(".q");
return S;
}

double fit_Jpsiee_B_DTFM_M_0_polynomialBkg(TTree *tree, TCut datacut, double fit_dn, double fit_up, double return_dn, double return_up){

//---------Build background PDF------

RooRealVar m("B_DTFM_M_0", "#it{m}_{phiKJpsimm}", fit_dn, fit_up);
RooArgSet obs(m);

RooRealVar coeff_a("a", "coefficient #0", -1.428E-4, -4E-4, -1E-5);
RooRealVar coeff_b("b", "coefficient #1", 1.508, 0, 4.8);
RooRealVar coeff_c("c", "coefficient #2", -3927, -10000, -3000);
RooPolynomial bkg("bkg", "background Chebychev polynomial", m, RooArgList(coeff_c, coeff_b, coeff_a));

TTree *fitTree = tree->CopyTree(datacut.GetTitle());

RooDataSet *datars = new RooDataSet("signal shape", "sigshape", fitTree, obs);
RooRealVar sigma_Bu("#sigma_Bu", "width of signal peaks", 5.0, 1.0, 20.0);
RooRealVar muBu("#mu_{B_{u}}", "position of B_{u} peak", 5279., 5250, 5300);

RooRealVar nlBu("nlBu", "Exponent of CB", Jpsiee_RooDCB_B_DTFM_M_0_nl);
RooRealVar nrBu("nrBu", "Exponent of CB", Jpsiee_RooDCB_B_DTFM_M_0_nr);
RooRealVar alphalBu("#alpha_lBu", "Transition point of CB", Jpsiee_RooDCB_B_DTFM_M_0_al);
RooRealVar alpharBu("#alpha_rBu", "Transition point of CB", Jpsiee_RooDCB_B_DTFM_M_0_ar);

RooDoubleCB cbBu("cbBu", "CB for B_{d}", m, muBu, sigma_Bu, alphalBu, nlBu, alpharBu, nrBu);

RooRealVar *nBu = new RooRealVar("nBu", "Number of signal events", 2800, 0., 1e7);
RooRealVar *nBkg = new RooRealVar("nBkg", "Number of bkg events", 2800, 0., 1e7);

RooAddPdf* totPDF = new RooAddPdf("totPDF", "Total pdf", RooArgList(cbBu, bkg), RooArgList(*nBu, *nBkg));

RooFitResult *result = totPDF->fitTo(*datars, Save(true), NumCPU(10));
RooPlot *mframe1 = m.frame(Title("mass frame"));

datars->plotOn(mframe1, Name("data"), XErrorSize(1), Binning((int)((fit_up-fit_dn)/9)));

totPDF->plotOn(mframe1, Name("signal Bu"), Components(cbBu), LineColor(2), LineStyle(kDashed));
totPDF->plotOn(mframe1, Name("background"), Components(bkg), LineColor(4), LineStyle(kDashed));
totPDF->plotOn(mframe1);
totPDF->paramOn(mframe1, Layout(0.65, 0.85, 0.95));


int result_status = result->status();

cout << "result_status = "<< result_status<<endl;

result->Print("v");

RooPlot *framePull = m.frame();
cout << "chi^2 = " << mframe1->chiSquare() << endl;

RooHist *pull = mframe1->pullHist();
pull->SetFillColor(39);
pull->SetLineColor(2);
pull->GetYaxis()->SetTitle("Pull");
framePull->addPlotable(pull, "3L");
//cout<<"-----------------1"<<endl;

TCanvas *c1 = new TCanvas("phiKJpsimm", "phiKJpsimm", 1200, 800);

gPad->SetLeftMargin(20);

c1->cd();
TPad *p2 = new TPad("p2", "p2Y", 0.0, 0.0, 1.0, 0.25, 0, 0, 0);
p2->SetTopMargin(0.015);
p2->SetBottomMargin(0.45);
p2->SetFillStyle(0);
p2->Draw();

TPad *p1 = new TPad("c1", "c1", 0.0, 0.25, 1.0, 1.0, 0, 0, 0);
p1->SetTopMargin(0.022);
p1->SetBottomMargin(0.08);
p1->Draw();

p1->cd();
gPad->SetLeftMargin(0.15);
mframe1->GetYaxis()->SetTitleOffset(1.00);
mframe1->GetYaxis()->SetLabelSize(0.07);
mframe1->GetYaxis()->SetTitleSize(0.07);
mframe1->GetYaxis()->SetNoExponent();
mframe1->GetXaxis()->SetTitleSize(0.0);
mframe1->GetXaxis()->SetLabelSize(0.0);
mframe1->Draw();


p2->cd();
gPad->SetLeftMargin(0.15);
framePull->SetFillStyle(0);
framePull->GetYaxis()->SetTitleSize(fabs(fit_dn-fit_up)/16800.0);
framePull->GetXaxis()->SetTitleSize(fabs(fit_dn-fit_up)/16800.0);
framePull->GetYaxis()->SetLabelSize(fabs(fit_dn-fit_up)/16800.0);
framePull->GetXaxis()->SetLabelSize(fabs(fit_dn-fit_up)/16800.0);
framePull->GetYaxis()->SetTitleOffset(1.4);
framePull->GetXaxis()->SetTitleOffset(1.0);
framePull->GetYaxis()->SetNdivisions(205, kTRUE);
framePull->SetMaximum(6.0);
framePull->SetMinimum(-6.0);
framePull->SetTitle("");
framePull->Draw();

m.setRange("SignalRegion", return_dn, return_up);
double S = nBu->getVal()* (cbBu.createIntegral(m, m, "SignalRegion")->getVal());
double B = nBkg->getVal()* (bkg.createIntegral(m, m, "SignalRegion")->getVal());

delete fitTree;
delete datars;

c1->SaveAs( Form("phiKJpsimm_data_%s_unbinned.pdf", "B_DTFM_M_0") );

//gROOT->ProcessLine(".q");
return S;
}
#endif

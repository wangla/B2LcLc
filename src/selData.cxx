#include "/mnt/d/lhcb/B2XcXc/inc/tools.h"
#include "/mnt/d/lhcb/B2XcXc/inc/getPresel.h"

//  mdoe == 1, Bd2DsD
//			2, Bd2LcLc
//			3, Bs2DsDs
//			4, Bs2LcLc
//void selData(){
void selData(int mode, bool merge = false){
	//int mode =3;

	vector<TString> channel(4, "");
	channel[0] = "Bd2DsD";
	channel[1] = "Bd2LcLc";
	channel[2] = "Bs2DsDs";
	channel[3] = "Bs2LcLc";

	TString modeStr = channel[mode-1];
	TString Bs_Bd = "Bd";
	if(mode == 3 || mode == 4) Bs_Bd = "Bs";

	//if(merge){
	//	// signal LcLc mode
	//	// 数据只看 Bs2LcLc的一种质量谱就行了
	//	if(mode == 4){
	//		for(int year=2015; year<=2018; year++){
	//			//selTree("/mnt/d/lhcb/B2XcXc/data_stripping/B2XcXc_UNBLIND_RD-CC2DDLine-"+i2s(year)+"-DATA-Down_TriggerSel.root", "/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_SB_"+i2s(year)+"_Down_TriggerSel.root", "B_DTF_LcM_M[0]>5000 && !(B_DTF_LcM_M[0]>5240 && B_DTF_LcM_M[0]<5407)");
	//			//selTree("/mnt/d/lhcb/B2XcXc/data_stripping/B2XcXc_UNBLIND_RD-CC2DDLine-"+i2s(year)+"-DATA-Up_TriggerSel.root", "/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_SB_"+i2s(year)+"_Up_TriggerSel.root", "B_DTF_LcM_M[0]>5000 && !(B_DTF_LcM_M[0]>5240 && B_DTF_LcM_M[0]<5407)");
	//			// add year label
	//			//addBranch("/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_SB_"+i2s(year)+"_Down_TriggerSel.root", "year", i2s(year));
	//			//addBranch("/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_SB_"+i2s(year)+"_Up_TriggerSel.root", "year", i2s(year));
	//			//// mag polarity
	//			//addBranch("/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_SB_"+i2s(year)+"_Down_TriggerSel.root", "magFlag", "-1");
	//			//addBranch("/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_SB_"+i2s(year)+"_Up_TriggerSel.root", "magFlag", "1");


	//			//// for data
	//			//mergeTrees("/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_SB_"+i2s(year)+"_TriggerSel.root", 
	//			//	"/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_SB_"+i2s(year)+"_Down_TriggerSel.root",
	//			//	"/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_SB_"+i2s(year)+"_Up_TriggerSel.root");


	//			selTree("/mnt/d/lhcb/B2XcXc/data_stripping/B2XcXc_UNBLIND_RD-CC2DDLine-"+i2s(year)+"-DATA-Down_TriggerSel.root", "/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_"+i2s(year)+"_Down_TriggerSel.root", "B_DTF_LcM_M[0]>5000");
	//			selTree("/mnt/d/lhcb/B2XcXc/data_stripping/B2XcXc_UNBLIND_RD-CC2DDLine-"+i2s(year)+"-DATA-Up_TriggerSel.root", "/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_"+i2s(year)+"_Up_TriggerSel.root", "B_DTF_LcM_M[0]>5000");
	//			// add year label
	//			addBranch("/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_"+i2s(year)+"_Down_TriggerSel.root", "year", i2s(year));
	//			addBranch("/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_"+i2s(year)+"_Up_TriggerSel.root", "year", i2s(year));
	//			// mag polarity
	//			addBranch("/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_"+i2s(year)+"_Down_TriggerSel.root", "magFlag", "-1");
	//			addBranch("/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_"+i2s(year)+"_Up_TriggerSel.root", "magFlag", "1");


	//			// for data
	//			mergeTrees("/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_"+i2s(year)+"_TriggerSel.root", 
	//				"/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_"+i2s(year)+"_Down_TriggerSel.root",
	//				"/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_"+i2s(year)+"_Up_TriggerSel.root");


	//				//no using sameside
	//			////// add year label
	//			////addBranch("/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_SS_"+i2s(year)+"_Down_TriggerSel.root", "year", i2s(year));
	//			////addBranch("/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_SS_"+i2s(year)+"_Up_TriggerSel.root", "year", i2s(year));
	//			////// mag polarity
	//			////addBranch("/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_SS_"+i2s(year)+"_Down_TriggerSel.root", "magFlag", "-1");
	//			////addBranch("/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_SS_"+i2s(year)+"_Up_TriggerSel.root", "magFlag", "1");


	//			////// for data
	//			////mergeTrees("/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_SS_"+i2s(year)+"_TriggerSel.root", 
	//			////	"/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_SS_"+i2s(year)+"_Down_TriggerSel.root",
	//			////	"/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_SS_"+i2s(year)+"_Up_TriggerSel.root");
	//		}


	//		//// for data
	//		//mergeTrees("/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/Data_"+modeStr+"_SB_Run2_TriggerSel.root", 
	//		//	"/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_SB_2015_TriggerSel.root",
	//		//	"/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_SB_2016_TriggerSel.root",
	//		//	"/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_SB_2017_TriggerSel.root",
	//		//	"/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_SB_2018_TriggerSel.root");

	//		mergeTrees("/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/Data_"+modeStr+"_Run2_TriggerSel.root", 
	//			"/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_2015_TriggerSel.root",
	//			"/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_2016_TriggerSel.root",
	//			"/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_2017_TriggerSel.root",
	//			"/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_2018_TriggerSel.root");

	//		////mergeTrees("/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/Data_"+modeStr+"_SS_Run2_TriggerSel.root", 
	//		////	"/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_SS_2015_TriggerSel.root",
	//		////	"/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_SS_2016_TriggerSel.root",
	//		////	"/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_SS_2017_TriggerSel.root",
	//		////	"/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_SS_2018_TriggerSel.root");
	//	}
	//	// ctrl mode
	//	if(mode == 1 || mode == 3){
	//		for(int year=2015; year<=2018; year++){
	//			// add year label
	//			addBranch("/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_"+i2s(year)+"_Down_TriggerSel.root", "year", i2s(year));
	//			addBranch("/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_"+i2s(year)+"_Up_TriggerSel.root", "year", i2s(year));
	//			// mag polarity
	//			addBranch("/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_"+i2s(year)+"_Down_TriggerSel.root", "magFlag", "-1");
	//			addBranch("/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_"+i2s(year)+"_Up_TriggerSel.root", "magFlag", "1");


	//				// for data
	//			mergeTrees("/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_"+i2s(year)+"_TriggerSel.root", 
	//				"/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_"+i2s(year)+"_Down_TriggerSel.root",
	//				"/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_"+i2s(year)+"_Up_TriggerSel.root");
	//		}

	//		//	// for data
	//		mergeTrees("/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/Data_"+modeStr+"_Run2_TriggerSel.root", 
	//					"/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_2015_TriggerSel.root",
	//					"/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_2016_TriggerSel.root",
	//					"/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_2017_TriggerSel.root",
	//					"/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/TriggerSel/Data_"+modeStr+"_2018_TriggerSel.root");
	//	}
	//}

	vector<TString> newBranchName;
	vector<TString> formulas;

	string probNNxFormula;
	if(mode == 2 || mode == 4){
		probNNxFormula = "pbar_Xc_minus_MC15TuneV1_ProbNNp*Kplus_Xc_minus_MC15TuneV1_ProbNNk*piminus_Xc_minus_MC15TuneV1_ProbNNpi*p_Xc_plus_MC15TuneV1_ProbNNp*Kminus_Xc_plus_MC15TuneV1_ProbNNk*piplus_Xc_plus_MC15TuneV1_ProbNNpi";


		newBranchName.push_back("pbar_Xc_minus_MC15TuneV1_ProbNNp_corr"); formulas.push_back("pbar_Xc_minus_MC15TuneV1_ProbNNp");
		newBranchName.push_back("Kplus_Xc_minus_MC15TuneV1_ProbNNk_corr"); formulas.push_back("Kplus_Xc_minus_MC15TuneV1_ProbNNk");
		newBranchName.push_back("piminus_Xc_minus_MC15TuneV1_ProbNNpi_corr"); formulas.push_back("piminus_Xc_minus_MC15TuneV1_ProbNNpi");
		newBranchName.push_back("p_Xc_plus_MC15TuneV1_ProbNNp_corr"); formulas.push_back("p_Xc_plus_MC15TuneV1_ProbNNp");
		newBranchName.push_back("Kminus_Xc_plus_MC15TuneV1_ProbNNk_corr"); formulas.push_back("Kminus_Xc_plus_MC15TuneV1_ProbNNk");
		newBranchName.push_back("piplus_Xc_plus_MC15TuneV1_ProbNNpi_corr"); formulas.push_back("piplus_Xc_plus_MC15TuneV1_ProbNNpi");
		newBranchName.push_back("pbar_Xc_minus_PIDp_corr"); formulas.push_back("pbar_Xc_minus_PIDp");
		newBranchName.push_back("Kplus_Xc_minus_PIDp_corr"); formulas.push_back("Kplus_Xc_minus_PIDp");
		newBranchName.push_back("piminus_Xc_minus_PIDp_corr"); formulas.push_back("piminus_Xc_minus_PIDp");
		newBranchName.push_back("p_Xc_plus_PIDp_corr"); formulas.push_back("p_Xc_plus_PIDp");
		newBranchName.push_back("Kminus_Xc_plus_PIDp_corr"); formulas.push_back("Kminus_Xc_plus_PIDp");
		newBranchName.push_back("piplus_Xc_plus_PIDp_corr"); formulas.push_back("piplus_Xc_plus_PIDp");
		newBranchName.push_back("pbar_Xc_minus_PIDK_corr"); formulas.push_back("pbar_Xc_minus_PIDK");
		newBranchName.push_back("Kplus_Xc_minus_PIDK_corr"); formulas.push_back("Kplus_Xc_minus_PIDK");
		newBranchName.push_back("piminus_Xc_minus_PIDK_corr"); formulas.push_back("piminus_Xc_minus_PIDK");
		newBranchName.push_back("p_Xc_plus_PIDK_corr"); formulas.push_back("p_Xc_plus_PIDK");
		newBranchName.push_back("Kminus_Xc_plus_PIDK_corr"); formulas.push_back("Kminus_Xc_plus_PIDK");
		newBranchName.push_back("piplus_Xc_plus_PIDK_corr"); formulas.push_back("piplus_Xc_plus_PIDK");

		addAngleList(2, &newBranchName, &formulas);
	}
	
	if(mode == 1){
		probNNxFormula = "Kplus_Dsminus_MC15TuneV1_ProbNNk*Kminus_Dsminus_MC15TuneV1_ProbNNk*piminus_Dsminus_MC15TuneV1_ProbNNpi*Kminus_Dplus_MC15TuneV1_ProbNNk*piplus1_Dplus_MC15TuneV1_ProbNNpi*piplus2_Dplus_MC15TuneV1_ProbNNpi";


		newBranchName.push_back("Kplus_Dsminus_MC15TuneV1_ProbNNk_corr"); formulas.push_back("Kplus_Dsminus_MC15TuneV1_ProbNNk");
		newBranchName.push_back("Kminus_Dsminus_MC15TuneV1_ProbNNk_corr"); formulas.push_back("Kminus_Dsminus_MC15TuneV1_ProbNNk");
		newBranchName.push_back("piminus_Dsminus_MC15TuneV1_ProbNNpi_corr"); formulas.push_back("piminus_Dsminus_MC15TuneV1_ProbNNpi");
		newBranchName.push_back("Kminus_Dplus_MC15TuneV1_ProbNNk_corr"); formulas.push_back("Kminus_Dplus_MC15TuneV1_ProbNNk");
		newBranchName.push_back("piplus1_Dplus_MC15TuneV1_ProbNNpi_corr"); formulas.push_back("piplus1_Dplus_MC15TuneV1_ProbNNpi");
		newBranchName.push_back("piplus2_Dplus_MC15TuneV1_ProbNNpi_corr"); formulas.push_back("piplus2_Dplus_MC15TuneV1_ProbNNpi");
		newBranchName.push_back("Kplus_Dsminus_PIDp_corr"); formulas.push_back("Kplus_Dsminus_PIDp");
		newBranchName.push_back("Kminus_Dsminus_PIDp_corr"); formulas.push_back("Kminus_Dsminus_PIDp");
		newBranchName.push_back("piminus_Dsminus_PIDp_corr"); formulas.push_back("piminus_Dsminus_PIDp");
		newBranchName.push_back("Kminus_Dplus_PIDp_corr"); formulas.push_back("Kminus_Dplus_PIDp");
		newBranchName.push_back("piplus1_Dplus_PIDp_corr"); formulas.push_back("piplus1_Dplus_PIDp");
		newBranchName.push_back("piplus2_Dplus_PIDp_corr"); formulas.push_back("piplus2_Dplus_PIDp");
		newBranchName.push_back("Kplus_Dsminus_PIDK_corr"); formulas.push_back("Kplus_Dsminus_PIDK");
		newBranchName.push_back("Kminus_Dsminus_PIDK_corr"); formulas.push_back("Kminus_Dsminus_PIDK");
		newBranchName.push_back("piminus_Dsminus_PIDK_corr"); formulas.push_back("piminus_Dsminus_PIDK");
		newBranchName.push_back("Kminus_Dplus_PIDK_corr"); formulas.push_back("Kminus_Dplus_PIDK");
		newBranchName.push_back("piplus1_Dplus_PIDK_corr"); formulas.push_back("piplus1_Dplus_PIDK");
		newBranchName.push_back("piplus2_Dplus_PIDK_corr"); formulas.push_back("piplus2_Dplus_PIDK");

		addAngleList(1, &newBranchName, &formulas);
	}
	if(mode == 3){
		probNNxFormula  = "Kplus_Dsminus_MC15TuneV1_ProbNNk*Kminus_Dsminus_MC15TuneV1_ProbNNk*piminus_Dsminus_MC15TuneV1_ProbNNpi*Kminus_Dsplus_MC15TuneV1_ProbNNk*Kplus_Dsplus_MC15TuneV1_ProbNNk*piplus_Dsplus_MC15TuneV1_ProbNNpi";

		
		newBranchName.push_back("Kplus_Dsminus_MC15TuneV1_ProbNNk_corr"); formulas.push_back("Kplus_Dsminus_MC15TuneV1_ProbNNk");
		newBranchName.push_back("Kminus_Dsminus_MC15TuneV1_ProbNNk_corr"); formulas.push_back("Kminus_Dsminus_MC15TuneV1_ProbNNk");
		newBranchName.push_back("piminus_Dsminus_MC15TuneV1_ProbNNpi_corr"); formulas.push_back("piminus_Dsminus_MC15TuneV1_ProbNNpi");
		newBranchName.push_back("Kminus_Dsplus_MC15TuneV1_ProbNNk_corr"); formulas.push_back("Kminus_Dsplus_MC15TuneV1_ProbNNk");
		newBranchName.push_back("Kplus_Dsplus_MC15TuneV1_ProbNNk_corr"); formulas.push_back("Kplus_Dsplus_MC15TuneV1_ProbNNk");
		newBranchName.push_back("piplus_Dsplus_MC15TuneV1_ProbNNpi_corr"); formulas.push_back("piplus_Dsplus_MC15TuneV1_ProbNNpi");
		newBranchName.push_back("Kplus_Dsminus_PIDp_corr"); formulas.push_back("Kplus_Dsminus_PIDp");
		newBranchName.push_back("Kminus_Dsminus_PIDp_corr"); formulas.push_back("Kminus_Dsminus_PIDp");
		newBranchName.push_back("piminus_Dsminus_PIDp_corr"); formulas.push_back("piminus_Dsminus_PIDp");
		newBranchName.push_back("Kminus_Dsplus_PIDp_corr"); formulas.push_back("Kminus_Dsplus_PIDp");
		newBranchName.push_back("Kplus_Dsplus_PIDp_corr"); formulas.push_back("Kplus_Dsplus_PIDp");
		newBranchName.push_back("piplus_Dsplus_PIDp_corr"); formulas.push_back("piplus_Dsplus_PIDp");
		newBranchName.push_back("Kplus_Dsminus_PIDK_corr"); formulas.push_back("Kplus_Dsminus_PIDK");
		newBranchName.push_back("Kminus_Dsminus_PIDK_corr"); formulas.push_back("Kminus_Dsminus_PIDK");
		newBranchName.push_back("piminus_Dsminus_PIDK_corr"); formulas.push_back("piminus_Dsminus_PIDK");
		newBranchName.push_back("Kminus_Dsplus_PIDK_corr"); formulas.push_back("Kminus_Dsplus_PIDK");
		newBranchName.push_back("Kplus_Dsplus_PIDK_corr"); formulas.push_back("Kplus_Dsplus_PIDK");
		newBranchName.push_back("piplus_Dsplus_PIDK_corr"); formulas.push_back("piplus_Dsplus_PIDK");

		addAngleList(3, &newBranchName, &formulas);
	}
	

	newBranchName.push_back("prodProbNNx"); formulas.push_back(probNNxFormula);
	if(mode==1 || mode ==3) {newBranchName.push_back("B_DTFM_M_0"); formulas.push_back("B_DTF_M_M[0]");}
	if(mode==2 || mode ==4) {newBranchName.push_back("B_DTFM_M_0"); formulas.push_back("B_DTF_LcM_M[0]");}

	vector<TString> trees;
	if(mode == 1 || mode == 3){
		trees.push_back("/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/Data_"+modeStr+"_Run2_TriggerSel.root");
	}
	// signal mode
	if(mode == 4){
		//trees.push_back("/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/Data_"+modeStr+"_SB_Run2_TriggerSel.root");
		trees.push_back("/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/Data_"+modeStr+"_Run2_TriggerSel.root");
		//trees.push_back("/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/data/Data_"+modeStr+"_SS_Run2_TriggerSel.root");
	}

	//addBranch(trees, newBranchName, formulas);
	//addRadomNumber(trees);
	//addCombineMassVars(trees,  mode);


	if(mode == 1) {
		//selTree("/mnt/d/lhcb/B2XcXc/root/Bd/data/Data_Bd2DsD_Run2_TriggerSel.root", "/mnt/d/lhcb/B2XcXc/root/Bd/data/Data_Bd2DsD_Run2_TriggerSel_PreSel.root", getPresel("Bd2DsD"));
		selTree("/mnt/d/lhcb/B2XcXc/root/Bd/data/Data_Bd2DsD_Run2_TriggerSel.root", "/mnt/d/lhcb/B2XcXc/root/Bd/data/Data_Bd2DsD_Run2_TriggerSel_DsSB_PreSel.root", getBd2DsDPreselFor_DsSB());
	}
	if(mode == 3) {
		//selTree("/mnt/d/lhcb/B2XcXc/root/Bs/data/Data_Bs2DsDs_Run2_TriggerSel.root", "/mnt/d/lhcb/B2XcXc/root/Bs/data/Data_Bs2DsDs_Run2_TriggerSel_PreSel.root", getPresel("Bs2DsDs"));
		selTree("/mnt/d/lhcb/B2XcXc/root/Bs/data/Data_Bs2DsDs_Run2_TriggerSel.root", "/mnt/d/lhcb/B2XcXc/root/Bs/data/Data_Bs2DsDs_Run2_TriggerSel_DsSB_PreSel.root", getBs2DsDsPreselFor_DsSB());
	}
	if(mode == 4) {
		//selTree("/mnt/d/lhcb/B2XcXc/root/Bs/data/Data_Bs2LcLc_SB_Run2_TriggerSel.root", "/mnt/d/lhcb/B2XcXc/root/Bs/data/Data_Bs2LcLc_SB_Run2_TriggerSel_PreSel.root", getPresel("Bs2LcLc"));
		//selTree("/mnt/d/lhcb/B2XcXc/root/Bs/data/Data_Bs2LcLc_Run2_TriggerSel.root", "/mnt/d/lhcb/B2XcXc/root/Bs/data/Data_Bs2LcLc_Run2_TriggerSel_PreSel.root", getPresel("Bs2LcLc"));
		selTree("/mnt/d/lhcb/B2XcXc/root/Bs/data/Data_Bs2LcLc_Run2_TriggerSel.root", "/mnt/d/lhcb/B2XcXc/root/Bs/data/Data_Bs2LcLc_Run2_TriggerSel_PreSel_Lc60MeV.root", getLcLcPresel_Lc60Mev());
		//selTree("/mnt/d/lhcb/B2XcXc/root/Bs/data/Data_Bs2LcLc_SS_Run2_TriggerSel.root", "/mnt/d/lhcb/B2XcXc/root/Bs/data/Data_Bs2LcLc_SS_Run2_TriggerSel_PreSel.root", getPresel("Bs2LcLc"));
	}
}

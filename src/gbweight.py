'''
To get this to run you need to have the hep_ml package installed.

pip install hep_ml

then you also need to source a stable LGC env:

source /cvmfs/sft.cern.ch/lcg/views/LCG_97/x86_64-centos7-gcc8-opt/setup.sh 

'''
from ROOT import *
import os
import sys

import root_numpy
import numpy as np
import pandas
from hep_ml import reweight
import matplotlib.pyplot as plt
from matplotlib import rcParams, style
from decimal import Decimal
style.use('seaborn-muted')
rcParams.update({'font.size': 12})
rcParams['figure.figsize'] = 18, 6
rcParams['xtick.labelsize'] = 16
rcParams['ytick.labelsize'] = 16

gROOT.ProcessLine( "struct MyStructF{ Float_t afloat; };" )


swFile = str(sys.argv[1])
MCFile = str(sys.argv[2])

MC_mode=""
sw_mode=""

if "Bd2DsD" in MCFile:
	MC_mode = "Bd2DsD"
elif "Bd2LcLc" in MCFile:
	MC_mode = "Bd2LcLc"
elif "Bs2DsDs" in MCFile:
	MC_mode = "Bs2DsDs"
elif "Bs2LcLc" in MCFile:
	MC_mode = "Bs2LcLc"
else:
	print("ERROR: in gbweight.py, error input MC_mode")
	sys.exit(1)



if "Bd2DsD" in swFile:
	sw_mode = "Bd2DsD"
elif "Bs2DsDs" in swFile:
	sw_mode = "Bs2DsDs"
else:
	print("ERROR: in gbweight.py, error input sw_mode")
	sys.exit(1)




#sw_mode = str(sys.argv[1]) # string to tag the mode


## These are the variables that we want to pass to
## the GB reweighting (since these are the ones used in the BDT)
## Note that the DTF_CHI2NDOF variable for the MC target needs
## to be constructed below

Vars_original=[]
Vars_target=[]


#Vars_target = [
#	"nTracks",
#	"B_PT",
#	"B_ETA",
#	"B_IPCHI2_OWNPV",
#]
#Vars_original = Vars_target


### for DsD  K+ K- pi-    K- pi+ pi+
###     LcLc p+    K-pi+    p-     K+pi-
if MC_mode == "Bd2DsD":
	Vars_target = [
		#"Dsminus_PT",
		#"Dsminus_ETA",
		#"Dplus_PT",
		#"Dplus_ETA",
		"nTracks",
		#"B_PT",
		#"B_ETA",
		"B_IPCHI2_OWNPV",
		"Kplus_Dsminus_ETA",
		"Kplus_Dsminus_PT",
		"Kminus_Dsminus_ETA",
		"Kminus_Dsminus_PT",
		"piminus_Dsminus_ETA",
		"piminus_Dsminus_PT",
		"Kminus_Dplus_ETA",
		"Kminus_Dplus_PT",
		"piplus1_Dplus_ETA",
		"piplus1_Dplus_PT",
		"piplus2_Dplus_ETA",
		"piplus2_Dplus_PT",
	]
	
	Vars_original = Vars_target

if MC_mode == "Bs2DsDs":
	Vars_target = [
		"nTracks",
		#"B_PT",
		#"B_ETA",
		"B_IPCHI2_OWNPV",
		"Kplus_Dsminus_ETA",
		"Kplus_Dsminus_PT",
		"Kminus_Dsminus_ETA",
		"Kminus_Dsminus_PT",
		"piminus_Dsminus_ETA",
		"piminus_Dsminus_PT",
		"Kminus_Dsplus_ETA",
		"Kminus_Dsplus_PT",
		"Kplus_Dsplus_ETA",
		"Kplus_Dsplus_PT",
		"piplus_Dsplus_ETA",
		"piplus_Dsplus_PT",
	]
	

	Vars_original = Vars_target


if MC_mode == "Bd2LcLc":
	Vars_target = [
		"nTracks",
		#"B_PT",
		#"B_ETA",
		"B_IPCHI2_OWNPV",
		"Kplus_Dsminus_ETA",
		"Kplus_Dsminus_PT",
		"piminus_Dsminus_ETA",
		"piminus_Dsminus_PT",
		"Kminus_Dplus_ETA",
		"Kminus_Dplus_PT",
		"piplus1_Dplus_ETA",
		"piplus1_Dplus_PT",
	]
	
	Vars_original = [
		"nTracks",
		#"B_PT",
		#"B_ETA",
		"B_IPCHI2_OWNPV",
		"Kplus_Xc_minus_ETA",
		"Kplus_Xc_minus_PT",
		"piminus_Xc_minus_ETA",
		"piminus_Xc_minus_PT",
		"Kminus_Xc_plus_ETA",
		"Kminus_Xc_plus_PT",
		"piplus_Xc_plus_ETA",
		"piplus_Xc_plus_PT",
	]
	
if MC_mode == "Bs2LcLc":
	Vars_target = [
		"nTracks",
		#"B_PT",
		#"B_ETA",
		"B_IPCHI2_OWNPV",
		"Kplus_Dsminus_ETA",
		"Kplus_Dsminus_PT",
		"piminus_Dsminus_ETA",
		"piminus_Dsminus_PT",
		"Kminus_Dsplus_ETA",
		"Kminus_Dsplus_PT",
		"piplus_Dsplus_ETA",
		"piplus_Dsplus_PT",
	]
	
	Vars_original = [
		"nTracks",
		#"B_PT",
		#"B_ETA",
		"B_IPCHI2_OWNPV",
		"Kplus_Xc_minus_ETA",
		"Kplus_Xc_minus_PT",
		"piminus_Xc_minus_ETA",
		"piminus_Xc_minus_PT",
		"Kminus_Xc_plus_ETA",
		"Kminus_Xc_plus_PT",
		"piplus_Xc_plus_ETA",
		"piplus_Xc_plus_PT",
	]
	




columns_original = Vars_original

def draw_distributions(original_weights, gb_weights, year, sw_mode):

	from hep_ml.metrics_utils import ks_2samp_weighted
	hist_settings = {'bins': 50, 'density': True,  'alpha': 0.8, 'histtype': 'step', 'lw': 2}
	
	fig_size = plt.rcParams["figure.figsize"]
	fig_size[0] = 12
	fig_size[0] = 6
	fig_size[1] = 5
	plt.rcParams["figure.figsize"] = fig_size

	os.system('mkdir -p /mnt/d/lhcb/B2XcXc/public_html/gb_reweight/')

	for id, column in enumerate(columns_original, 0):
		name = "/mnt/d/lhcb/B2XcXc/public_html/gb_reweight/gb_reweighting_%s_%s_%s.png"%(year,sw_mode,column)
		plt.figure()

		i = id / 2
		j = id % 2
		xlim = np.percentile(np.hstack([target[column]]), [0.01, 99.9])
		data = original[column]
		if "IPCHI" in column: 
			xlim=[0.,15.]
		if "PT" in column: 
			xlim=[500.,15000.]
		if "B_PT" in column: 
			xlim=[0.,50000.]
		if "ETA" in column: 
			xlim=[1.5,5.5]
		if "nTracks" in column: 
			#xlim=[0.,350.]
			xlim=[0.,500.]

		ks_now = ks_2samp_weighted(data, target[column], weights1=original_weights, weights2=target_sw)
		ks_w   = ks_2samp_weighted(data, target[column], weights1=gb_weights,	   weights2=target_sw)
		
		plt.hist(target[column], weights=target_sw,		range=xlim, label="Data",	   **hist_settings)
		plt.hist(data,		   weights=original_weights, range=xlim, label="MC (KS=%.3f)"%ks_now,		 **hist_settings)
		plt.hist(data,		   weights=gb_weights,	   range=xlim, label="MC weights (KS=%.3f)"%ks_w, **hist_settings)
			
		plt.xlabel(column)
		plt.title("%s (%s)"%(MC_mode,year))
		#if "PT" in column: ax.set_yscale('log')
		legend = plt.legend(loc='upper right')
		print( 'KS std', column, ' = ', ks_now)
		print( 'KS GB ', column, ' = ', ks_w)
		plt.savefig(name)
   	

l_years = ["2015","2016","2017","2018"]
l_mags  = ["Down, Up"]

year = "Run2"



print("MC file: %s"%MCFile)

columns_original = Vars_original
columns_target = Vars_target+['sig_sw']
print("RD file: %s"%swFile)

original = root_numpy.root2array(MCFile, treename = 'DecayTree', branches=columns_original)#
print (original)
target   = root_numpy.root2array(swFile,   treename = 'DecayTree', branches=columns_target)#
print (target)

original = pandas.DataFrame(original)
target   = pandas.DataFrame(target)

original_sw   = np.ones(len(original))
original	  = original[Vars_original]
target_sw = target['sig_sw']
target		= target[Vars_target]

reweighter = reweight.GBReweighter(n_estimators=50, learning_rate=0.1, max_depth=3, min_samples_leaf=100, gb_args={'subsample': 0.6})
#reweighter = reweight.GBReweighter(n_estimators=50, learning_rate=0.1, max_depth=5, min_samples_leaf=1000, gb_args={'subsample': 0.4})

reweighter.fit(original, target, original_weight=original_sw, target_weight=target_sw)
gb_weights = reweighter.predict_weights(original, original_sw)

# pay attention, actually we have very few data
len(original), len(target)

#draw_distributions(original_sw, gb_weights, year, sw_mode)

# Add weights to tuple
weighted_file = MCFile+"_GBWeighted"
print("Creating a new MC file (copying original one) with additional gbWeights variable 'data_MC_w': %s"%weighted_file)
inputFile = TFile("%s"%MCFile)
inputTree = inputFile.Get("DecayTree")

inputTree.SetBranchStatus("data_MC_w", 0)
#inputTree.SetBranchStatus("MC_data_w", 0)

outputFile = TFile("%s"%(weighted_file), "RECREATE")
outputTree = inputTree.CopyTree("")
nEntries = outputTree.GetEntries()

gb_weights_var = MyStructF()
gb_weights_br  = outputTree.Branch( "data_MC_w", addressof(gb_weights_var,'afloat'), "data_MC_w/F" ) #data_MC_w is the name of weight
branches = outputTree.GetListOfBranches()
for entry in range(nEntries):
	outputTree.GetEntry(entry)
	gb_weights_var.afloat = gb_weights[entry]
	gb_weights_br.Fill()

outputTree.Write("",TObject.kOverwrite)
outputFile.Close()

inputFile.Close()

os.system("mv %s %s"%(weighted_file, MCFile))

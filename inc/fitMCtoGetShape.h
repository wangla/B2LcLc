#ifndef __FITMCTOGETSHAPE_H
#define __FITMCTOGETSHAPE_H 

#include <iostream>
#include "RooStats/ModelConfig.h"
#include "RooStats/ProfileLikelihoodCalculator.h"
#include "RooStats/HypoTestResult.h"
#include <RooDataSet.h>

#include "/mnt/d/lhcb/B2XcXc/inc/tools.h"
//#include "/mnt/d/lhcb/B2XcXc/inc/RooSemiGaussian.h"
#include "/mnt/d/lhcb/B2XcXc/inc/RooIpatia2.h"
#include "/mnt/d/lhcb/B2XcXc/inc/RooDoubleCB.h"

using namespace RooFit;
using namespace RooStats;

RooFitResult fitMCBMass(TTree* tree, TString fitVar, TString cuts, double fit_dn, double fit_up, double binWidth,
		RooAbsPdf& model, double plotDn, double plotUp,
		TString title, TPaveText text, TString saveName="", bool setLog = true, bool drawLeg=true){

	//---------Build background PDF------
	RooRealVar m(fitVar, "#it{m}_{B}", fit_dn, fit_up);

	m.setRange("plotRegion", plotDn, plotUp);
	m.setBins((int)((plotUp-plotDn)/binWidth));

    RooRealVar weight("weightForFit", "weightForFit", 0, 10);

    RooDataSet *datars = new RooDataSet("MCDataSet", "MCDataSet", RooArgSet(m, weight), Import(*tree), WeightVar( weight.GetName() ));
	

	RooFitResult *result = model.fitTo(*datars, Save(true), Strategy(2), NumCPU(10), Range("plotRegion"));

	RooPlot *mframe = m.frame(Title("mass frame"), Range("plotRegion"));
	cout << "chi^2 = " << mframe->chiSquare() << endl;

	const double nData = datars->sumEntries("", "plotRegion");

	datars->plotOn(mframe, Name("data"), CutRange("plotRegion"));
	model.plotOn(mframe, Name("model"),  RooFit::LineColor(kBlue), Normalization(nData, RooAbsReal::NumEvent), ProjectionRange("plotRegion"));
	datars->plotOn(mframe, Name("data"), CutRange("plotRegion"));

	if(drawLeg) model.paramOn(mframe, Layout(0.65, 0.95, 0.95));


	int result_status = result->status();
	cout << "result_status = "<< result_status<<endl;
	result->Print("v");
	
	mframe->SetTitle(fitVar);
	mframe->SetXTitle("#it{m}_{B} [MeV/c^{2}]");
	mframe->SetYTitle(Form("Candidates / (%.1lf MeV/c^{2})", binWidth));
	mframe->GetXaxis()->SetTitleSize(0.2);
	mframe->GetYaxis()->SetTitleSize(0.2);
	
	RooHist *pull = mframe->pullHist();
	pull->GetYaxis()->SetTitle("Pull");
	RooPlot *framePull = m.frame(Range("plotRegion"));
	framePull->addPlotable(pull, "P");
	
	framePull->SetTitle("");
	framePull->GetYaxis()->SetTitle("Pull");
	framePull->SetMinimum(-6.);
	framePull->SetMaximum(6.);
	framePull->SetMarkerStyle(2);
	//cout<<"-----------------1"<<endl;
	
	TCanvas *canv = new TCanvas("canv", "canv", 800, 600);
	canv->SetFillColor(10);
	canv->SetBorderMode(0);
	canv->cd();
	canv->cd(1);
	
	TPad *p1 = new TPad("pad1", "pad1", 0.02, 0.2, 0.98, 0.98, 0);
	TPad *p2 = new TPad("pad2", "pad2", 0.02, 0.04, 0.98, 0.2, 0);
	p1->Draw();
	p2->Draw();
	
	p1->cd();
	mframe->GetYaxis()->SetTitleOffset(1.00);
	mframe->GetYaxis()->SetLabelSize(0.05);
	mframe->GetYaxis()->SetTitleSize(0.05);
	mframe->GetYaxis()->SetNoExponent();
	mframe->GetXaxis()->SetTitleOffset(1.00);
	mframe->GetXaxis()->SetTitleSize(0.05);
	mframe->GetXaxis()->SetLabelSize(0.05);
	mframe->Draw("E1");
	text.Draw("same");
	if(setLog) p1->SetLogy();
	
	p2->cd();
	p2->SetTickx();
	framePull->GetYaxis()->SetTitleOffset(0.17);
	framePull->GetXaxis()->SetTitleSize(0.0);
	framePull->GetXaxis()->SetTickLength(0.09);
	framePull->GetYaxis()->SetTitleSize(0.2);
	framePull->GetXaxis()->SetLabelSize(0.0);
	framePull->GetYaxis()->SetLabelSize(0.2);
	framePull->GetYaxis()->SetNdivisions(5);
	framePull->Draw("E1");

    TLine* lineZero = new TLine(plotDn, 0, plotUp, 0);
    lineZero->SetLineStyle(kDashed);
    lineZero->SetLineColor(kBlack);
    lineZero->Draw();

    TLine* lineTwoSigUp = new TLine(plotDn, 3, plotUp, 3);
    lineTwoSigUp->SetLineColor(kRed);
    lineTwoSigUp->Draw();
    TLine* lineTwoSigDown = new TLine(plotDn, -3, plotUp, -3);
    lineTwoSigDown->SetLineColor(kRed);
    lineTwoSigDown->Draw();

    gPad->Update();
    canv->Update();

	if(saveName.Length() != 0) canv->SaveAs(saveName);

	return *result;
}

RooFitResult fitMCBMass(TString fileName, TString fitVar, TString cuts, TString weight, double fit_dn, double fit_up, double binWidth,
		RooAbsPdf& model, double plotDn, double plotUp,
		TString title, TPaveText text, TString saveName="", bool setLog = true, bool drawLeg=true){
	if(cuts == "") cuts = "(1.)";
	if(weight == "") weight = "(1.)";

	cuts += " &&("+fitVar+">"+d2s(fit_dn)+" && "+fitVar+"<"+d2s(fit_up)+")";
	
	TRandom2 rand;

	TString toEraseTreeName = "fitMCBMassTmp"+d2s(rand.Rndm())+".root";
	
	vector<TString> varsToSwitchOn;
	varsToSwitchOn.push_back(fitVar);

    system("mkdir -p /mnt/d/lhcb/B2XcXc/toErase");

    copyTreeWithNewVars("/mnt/d/lhcb/B2XcXc/toErase/"+toEraseTreeName, fileName, cuts, weight, "weightForFit", varsToSwitchOn, "DecayTree");

	TString tmpName = "/mnt/d/lhcb/B2XcXc/toErase/"+toEraseTreeName;
	TFile fMC(tmpName);
	TTree* tree = (TTree*)fMC.Get("DecayTree");

	if(!tree){
		printf("fitMCBMass_Ipatia: no DecayTree in %s\n", fileName.Data());
		return RooFitResult();
	}
	

	return fitMCBMass( tree, fitVar, cuts, fit_dn, fit_up, binWidth,
				model, plotDn, plotUp,
            	title,  text, saveName, setLog, drawLeg);
	fMC.Close();
	system("rm " + tmpName);
}

RooFitResult fitMCBMass_addPdf(TTree* tree, TString fitVar, TString cuts, double fit_dn, double fit_up, double binWidth,
		RooAddPdf& totPDF, const RooArgList& modelSets, double plotDn, double plotUp,
		TString title, TPaveText text, TString saveName="", bool setLog = true, bool drawLeg=true){

	//---------Build background PDF------
	RooRealVar m(fitVar, "#it{m}_{B}", fit_dn, fit_up);

	m.setRange("plotRegion", plotDn, plotUp);
	m.setBins((int)((plotUp-plotDn)/binWidth));

    RooRealVar weight("weightForFit", "weightForFit", 0, 10);

    RooDataSet *datars = new RooDataSet("MCDataSet", "MCDataSet", RooArgSet(m, weight), Import(*tree), WeightVar( weight.GetName() ));
	

	RooFitResult *result = totPDF.fitTo(*datars, Save(true), Strategy(2), NumCPU(10), Range("plotRegion"));

	RooPlot *mframe = m.frame(Title("mass frame"), Range("plotRegion"));
	cout << "chi^2 = " << mframe->chiSquare() << endl;

	const double nData = datars->sumEntries("", "plotRegion");

	// MC 的model最多只有两个
	datars->plotOn(mframe, Name("data"), CutRange("plotRegion"));
	for(int i=0; i<modelSets.getSize(); i++){
		RooAbsPdf* pdf = (RooAbsPdf*)modelSets.at(i);
		totPDF.plotOn(mframe, Name(pdf->GetName()), Components(*pdf), RooFit::LineColor(getBeautifulColour(i+1)), LineStyle(kDashed), Normalization(nData, RooAbsReal::NumEvent),  ProjectionRange("plotRegion"));
	}
	totPDF.plotOn(mframe, Name("totPDF"),  RooFit::LineColor(kBlue), ProjectionRange("plotRegion"));


	datars->plotOn(mframe, Name("data"), CutRange("plotRegion"));

	if(drawLeg) totPDF.paramOn(mframe, Layout(0.65, 0.95, 0.95));


	int result_status = result->status();
	cout << "result_status = "<< result_status<<endl;
	result->Print("v");
	
	mframe->SetTitle(fitVar);
	mframe->SetXTitle("#it{m}_{B} [MeV/c^{2}]");
	mframe->SetYTitle(Form("Candidates / (%.1lf MeV/c^{2})", binWidth));
	mframe->GetXaxis()->SetTitleSize(0.2);
	mframe->GetYaxis()->SetTitleSize(0.2);
	
	RooHist *pull = mframe->pullHist();
	pull->GetYaxis()->SetTitle("Pull");
	RooPlot *framePull = m.frame(Range("plotRegion"));
	framePull->addPlotable(pull, "P");
	
	framePull->SetTitle("");
	framePull->GetYaxis()->SetTitle("Pull");
	framePull->SetMinimum(-6.);
	framePull->SetMaximum(6.);
	framePull->SetMarkerStyle(2);
	//cout<<"-----------------1"<<endl;
	
	TCanvas *canv = new TCanvas("canv", "canv", 800, 600);
	canv->SetFillColor(10);
	canv->SetBorderMode(0);
	canv->cd();
	canv->cd(1);
	
	TPad *p1 = new TPad("pad1", "pad1", 0.02, 0.2, 0.98, 0.98, 0);
	TPad *p2 = new TPad("pad2", "pad2", 0.02, 0.04, 0.98, 0.2, 0);
	p1->Draw();
	p2->Draw();
	
	p1->cd();
	mframe->GetYaxis()->SetTitleOffset(1.00);
	mframe->GetYaxis()->SetLabelSize(0.05);
	mframe->GetYaxis()->SetTitleSize(0.05);
	mframe->GetYaxis()->SetNoExponent();
	mframe->GetXaxis()->SetTitleOffset(1.00);
	mframe->GetXaxis()->SetTitleSize(0.05);
	mframe->GetXaxis()->SetLabelSize(0.05);
	mframe->Draw("E1");
	text.Draw("same");
	if(setLog) p1->SetLogy();
	
	p2->cd();
	p2->SetTickx();
	framePull->GetYaxis()->SetTitleOffset(0.17);
	framePull->GetXaxis()->SetTitleSize(0.0);
	framePull->GetXaxis()->SetTickLength(0.09);
	framePull->GetYaxis()->SetTitleSize(0.2);
	framePull->GetXaxis()->SetLabelSize(0.0);
	framePull->GetYaxis()->SetLabelSize(0.2);
	framePull->GetYaxis()->SetNdivisions(5);
	framePull->Draw("E1");


    TLine* lineZero = new TLine(plotDn, 0, plotUp, 0);
    lineZero->SetLineStyle(kDashed);
    lineZero->SetLineColor(kBlack);
    lineZero->Draw();

    TLine* lineTwoSigUp = new TLine(plotDn, 3, plotUp, 3);
    lineTwoSigUp->SetLineColor(kRed);
    lineTwoSigUp->Draw();
    TLine* lineTwoSigDown = new TLine(plotDn, -3, plotUp, -3);
    lineTwoSigDown->SetLineColor(kRed);
    lineTwoSigDown->Draw();

    gPad->Update();
    canv->Update();

	if(saveName.Length() != 0) canv->SaveAs(saveName);

	return *result;
}

RooFitResult fitMCBMass_addPdf(TString fileName, TString fitVar, TString cuts, TString weight, double fit_dn, double fit_up, double binWidth,
		RooAddPdf& totPDF, const RooArgList& modelSets, double plotDn, double plotUp,
		TString title, TPaveText text, TString saveName="", bool setLog = true, bool drawLeg=true){
	if(cuts == "") cuts = "(1.)";
	if(weight == "") weight = "(1.)";

	cuts += " &&("+fitVar+">"+d2s(fit_dn)+" && "+fitVar+"<"+d2s(fit_up)+")";
	
	TRandom2 rand;

	TString toEraseTreeName = "fitMCBMassTmp"+d2s(rand.Rndm())+".root";
	
	vector<TString> varsToSwitchOn;
	varsToSwitchOn.push_back(fitVar);

    system("mkdir -p /mnt/d/lhcb/B2XcXc/toErase");

    copyTreeWithNewVars("/mnt/d/lhcb/B2XcXc/toErase/"+toEraseTreeName, fileName, cuts, weight, "weightForFit", varsToSwitchOn, "DecayTree");

	TString tmpName = "/mnt/d/lhcb/B2XcXc/toErase/"+toEraseTreeName;
	TFile fMC(tmpName);
	TTree* tree = (TTree*)fMC.Get("DecayTree");

	if(!tree){
		printf("fitMCBMass_Ipatia: no DecayTree in %s\n", fileName.Data());
		return RooFitResult();
	}
	

	return fitMCBMass_addPdf( tree, fitVar, cuts, fit_dn, fit_up, binWidth,
				totPDF, modelSets, plotDn, plotUp,
            	title,  text, saveName, setLog, drawLeg);
	fMC.Close();
	system("rm " + tmpName);
}
#endif

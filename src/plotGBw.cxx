#include "/mnt/d/lhcb/B2XcXc/inc/histoTools.h"
#include "/mnt/d/lhcb/B2XcXc/inc/getPresel.h"

void plotGBw(){

	lhcbStyle(0.0603);

	vector<TString> treeNames;
	vector<TString> drawTitles;
	vector<TString> cuts;
	vector<TString> weights;
	vector<int> colors;
	vector<TString> drawOptions;


	vector<int> years;
	years.push_back(-1);

	for(int i=0; i<years.size(); i++){
		int year = years[i];

		TString cutYear = "(1)";
		if(year != -1)	cutYear = "year==" + i2s(year);


		//vector<double> legPos = {0, 0, 0, 0};
		vector<double> legPos = {0.65, 0.65, 0.90, 0.90};
		//vector<double> legPos = {0.25, 0.50, 0.65, 0.90};


		vector<Histo1DWithInfo> Bd2DsD_HistoInfos_corr;
		Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("data_MC_w", "data_MC_w", "#w^{data}_{MC}", 0, 2, 50, false));

		treeNames.clear();
		drawTitles.clear();
		cuts.clear();
		weights.clear();
		colors.clear();
		drawOptions.clear();


		drawTitles.push_back("Bd2DsD GB1");
		drawOptions.push_back("E1");

		drawTitles.push_back("Bd2DsD GB2");
		drawOptions.push_back("E1");

		drawTitles.push_back("Bd2DsD GB3");
		drawOptions.push_back("E1");

		vector<vector<TH1D>> histosBd;
		histosBd.push_back(getVarsHistos1D("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2DsD_Run2_truth_PIDcor_TrigSel_Presel.root", cutYear, "(1)", kBlue, Bd2DsD_HistoInfos_corr));
		histosBd.push_back(getVarsHistos1D("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2DsD_Run2_truth_PIDcor_TrigSel_PreselCp.root", cutYear, "(1)", kRed, Bd2DsD_HistoInfos_corr));
		histosBd.push_back(getVarsHistos1D("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2DsD_Run2_truth_PIDcor_TrigSel_PreselCp2.root", cutYear, "(1)", kBlack, Bd2DsD_HistoInfos_corr));


		TString plotDir_Bd = "/mnt/d/lhcb/B2XcXc/public_html/plotGBw/Bd";
		if(year != -1) plotDir_Bd += "/every_year/"+i2s(year);

		system("mkdir -p " + plotDir_Bd);
		drawHistos(histosBd,
				Bd2DsD_HistoInfos_corr,
				drawTitles, drawOptions,
				legPos,
				plotDir_Bd);
	}
	gROOT->ProcessLine(".q");
}

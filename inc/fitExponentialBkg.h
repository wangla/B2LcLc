#ifndef __FITPHIKEEBKG_H
#define __FITPHIKEEBKG_H

#include <iostream>
#include "RooStats/ModelConfig.h"
#include "RooStats/ProfileLikelihoodCalculator.h"
#include "RooStats/HypoTestResult.h"
#include <RooDataSet.h>
#include "/mnt/d/lhcb/B2XcXc/inc/tools.h"
using namespace RooFit;
using namespace RooStats;

//fitSB = 0: fit full range in [fit_dn, fit_up]
//fitSB = 1: fit  range in [fit_dn, fit_up] except [empty_dn, empty_up]
ValError fitExponentialBkg(TTree *tree, TString fitVar, TString datacut, double &chisq, double fit_dn,  double fit_up, double  return_dn, double  return_up, TString saveName, double   empty_dn=0, double   empty_up=0, bool fitSB=0, bool limitNeg = false){
//R__LOAD_LIBRARY(libRooFit)

RooRealVar m(fitVar, Form("Fit bkg in range [%.2lf, %.2lf]", fit_dn, fit_up), fit_dn, fit_up);
if(fitSB){
	m.setRange("down", fit_dn, empty_dn);
	m.setRange("up", empty_up, fit_up);
}
m.setRange("B0", return_dn, return_up);
RooArgSet obs(m);

TTree *fitTree = tree->CopyTree(datacut);
RooDataSet *datars = new RooDataSet("signal shape", "sigshape", fitTree, obs);

RooRealVar tau("tau", "tau", -0.0003, -0.01, 0.01);
if(limitNeg) tau.setRange(-0.01, 0);
RooExponential bkg("bkg", "background model", m, tau);
RooRealVar nbkg("nbkg", "Number of bkg events", 120000, 0., 1e6);
RooAddPdf* totPDF = new RooAddPdf("totPDF", "Total pdf", bkg, nbkg);

RooFitResult *result;
if(fitSB){
	//result = totPDF->fitTo(*datars, Range("down,up"), Save(true), NumCPU(10));
	result = totPDF->fitTo(*datars, Range("down,up"), Save(true));
}else{
	//result = totPDF->fitTo(*datars, Save(true), NumCPU(10));
	result = totPDF->fitTo(*datars, Save(true));
}

RooPlot *mframe = m.frame(Title(fitVar));

datars->plotOn(mframe, Name("data"), XErrorSize(1), Binning((int)((fit_up-fit_dn)/9)));

totPDF->plotOn(mframe, Name("signal"), Components(bkg), LineColor(2));
totPDF->plotOn(mframe, LineColor(4), LineStyle(kDashed));
totPDF->paramOn(mframe, Layout(0.65, 0.85, 0.95));
int result_status = result->status();

cout << "result_status = "<< result_status<<endl;

result->Print("v");

RooPlot *framePull = m.frame();
chisq = mframe->chiSquare();
cout << "chi^2 = " << chisq << endl;

RooHist *pull = mframe->pullHist();
pull->SetFillColor(39);
pull->SetLineColor(2);
pull->GetYaxis()->SetTitle("Pull");
framePull->addPlotable(pull, "3L");

TCanvas *canv = new TCanvas("Bkg", "Bkg", 1200, 800);

gPad->SetLeftMargin(20);

canv->cd();
TPad *p2 = new TPad("p2", "p2Y", 0.0, 0.0, 1.0, 0.25, 0, 0, 0);
p2->SetTopMargin(0.015);
p2->SetBottomMargin(0.45);
p2->SetFillStyle(0);
p2->Draw();

TPad *p1 = new TPad("pad1", "pad1", 0.0, 0.25, 1.0, 1.0, 0, 0, 0);
p1->SetTopMargin(0.022);
p1->SetBottomMargin(0.08);
p1->Draw();

p1->cd();
gPad->SetLeftMargin(0.15);
mframe->GetYaxis()->SetTitleOffset(1.00);
mframe->GetYaxis()->SetLabelSize(0.07);
mframe->GetYaxis()->SetTitleSize(0.07);
mframe->GetYaxis()->SetNoExponent();
mframe->GetXaxis()->SetTitleSize(0.0);
mframe->GetXaxis()->SetLabelSize(0.0);
mframe->Draw();


p2->cd();
gPad->SetLeftMargin(0.15);
framePull->SetFillStyle(0);
framePull->GetYaxis()->SetTitleSize(fabs(fit_dn-fit_up)/16800.0);
framePull->GetXaxis()->SetTitleSize(fabs(fit_dn-fit_up)/16800.0);
framePull->GetYaxis()->SetLabelSize(fabs(fit_dn-fit_up)/16800.0);
framePull->GetXaxis()->SetLabelSize(fabs(fit_dn-fit_up)/16800.0);
framePull->GetYaxis()->SetTitleOffset(1.4);
framePull->GetXaxis()->SetTitleOffset(1.0);
framePull->GetYaxis()->SetNdivisions(205, kTRUE);
framePull->SetMaximum(6.0);
framePull->SetMinimum(-6.0);
framePull->SetTitle("");
framePull->Draw();



ValError bkga(nbkg.getVal()*(bkg.createIntegral(m, m, "B0")->getVal()),  nbkg.getError()*(bkg.createIntegral(m, m, "B0")->getVal()));

cout << "Bkg number in signal region: " << bkga << endl;
                     
if(saveName != "") canv->SaveAs(saveName);

delete fitTree;
delete datars;
//delete totPDF;
//delete result;
//delete mframe;
//delete framePull;
//delete pull;
//delete canv;
//delete p2;
//delete p1;

return bkga;
}

ValError fitExponentialBkg(TString file, TString fitVar, TString datacut, double &chisq, double  fit_dn, double  fit_up, double return_dn, double  return_up, TString saveName, double   empty_dn=0, double   empty_up=0, bool fitSB=0, bool limitNeg = false){
	TFile f(file);
	TTree* tree = (TTree*)f.Get("DecayTree");
	if(!tree){
		cerr << "ERROR in fitExponentialBkg, no DecayTree in '"+ file+"'" <<endl;
		return ValError(0, 0);
	}

	return fitExponentialBkg(tree, fitVar, datacut, chisq, fit_dn,  fit_up,  return_dn,  return_up,  empty_dn,  empty_up,  fitSB);
}

#endif

#!/bin/bash
## Run1/Run2 use different option! Check your option before submit!!!

##-----------------here is whose account
homeDir=$(env | grep ^HOME= | cut -c 6-)
account="${homeDir##*/}"

MC_or_DATA="DATA"
Line="CC2DDLine"

for Year in 2011 2012 #2015 2016 #2017 2018
do
    for MagType in Up Down
    do
        cp template/*py .
        sed -i "s#YEAR#${Year}#g" *py
        sed -i "s#MAG_TYPE#${MagType}#g" *py
        sed -i "s#MC_OR_DATA#${MC_or_DATA}#g" *py
        sed -i "s#STRIPPING_LINE#${Line}#g" *py
        sed -i "s#ACCOUNT#${account}#g" *py
        ganga ganga.py
        echo "submitted ${Year}-${MagType}"
    done
done

rm *py 

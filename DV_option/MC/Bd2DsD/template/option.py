from os import environ
from GaudiKernel.SystemOfUnits import *
from Gaudi.Configuration import *
from Configurables import GaudiSequencer, CombineParticles, OfflineVertexFitter
from Configurables import DecayTreeTuple, EventTuple, TupleToolTrigger, TupleToolTISTOS,FilterDesktop
from Configurables import BackgroundCategory, TupleToolDecay, TupleToolVtxIsoln,TupleToolPid,EventCountHisto,TupleToolRecoStats,TupleToolKinematic, SubstitutePID
from Configurables import TupleToolVeto, TupleToolTagging, TupleToolL0Data, TupleToolL0Calo
from Configurables import TupleToolMCTruth, TupleToolMCBackgroundInfo
from Configurables import MCTupleToolAngles, MCTupleToolHierarchy, MCTupleToolKinematic, MCTupleToolPrimaries, MCTupleToolReconstructed, MCTupleToolInteractions, PrintMCTree, PrintMCDecayTreeTool  
from GaudiConfUtils.ConfigurableGenerators import  CombineParticles
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand, AutomaticData
from GaudiConfUtils.ConfigurableGenerators import DaVinci__N3BodyDecays

year = 'YEAR'

doMC = 1
doLocalTest = 0

stream = 'CharmCompleteEvent'
if doMC: stream = 'AllStreams'
rootInTES = '/Event/%s'%stream

from Configurables import DecayTreeTuple
from Configurables import TupleToolTrigger, TupleToolDecay, TupleToolTISTOS

# Trigger lines
myTriggerList = [
             #L0
              'L0HadronDecision'
             ,'L0ElectronDecision'
             ,'L0ElectronHiDecision'
             ,'L0MuonDecision'
             ,'L0DiMuonDecision'
             ,'L0PhotonDecision'
             ,'L0MuonHighDecision'
             #Hlt1
             ,'Hlt1TrackMVADecision'
             ,'Hlt1TwoTrackMVADecision'
             ,'Hlt1GlobalDecision'
             ,'Hlt1TrackAllL0Decision'
             #Hlt2
             ,'Hlt2Topo2BodyBBDTDecision'
             ,'Hlt2Topo3BodyBBDTDecision'
             ,'Hlt2Topo4BodyBBDTDecision'
             ,'Hlt2BHadB02PpPpPmPmDecision'
             ,'Hlt2Topo2BodyDecision'
             ,'Hlt2Topo3BodyDecision'
             ,'Hlt2Topo4BodyDecision'
             ,'Hlt2GlobalDecision'
]

if year == '2011':
    from StrippingArchive.Stripping21r0p1.StrippingBandQ.StrippingCC2DD import CC2DDConf as builder
    from StrippingArchive.Stripping21r0p1.StrippingBandQ.StrippingCC2DD import default_config as config
if year == '2012':
    from StrippingArchive.Stripping21r0p1.StrippingBandQ.StrippingCC2DD import CC2DDConf as builder
    from StrippingArchive.Stripping21r0p1.StrippingBandQ.StrippingCC2DD import default_config as config
if year == '2015':
    from StrippingArchive.Stripping24r2.StrippingBandQ.StrippingCC2DD import CC2DDConf as builder
    from StrippingArchive.Stripping24r2.StrippingBandQ.StrippingCC2DD import default_config as config
if year == '2016':
    from StrippingArchive.Stripping28r2.StrippingBandQ.StrippingCC2DD import CC2DDConf as builder
    from StrippingArchive.Stripping28r2.StrippingBandQ.StrippingCC2DD import default_config as config
if year == '2017':
    from StrippingArchive.Stripping29r2.StrippingBandQ.StrippingCC2DD import CC2DDConf as builder
    from StrippingArchive.Stripping29r2.StrippingBandQ.StrippingCC2DD import default_config as config
if year == '2018':
    from StrippingArchive.Stripping34.StrippingBandQ.StrippingCC2DD import CC2DDConf as builder
    from StrippingArchive.Stripping34.StrippingBandQ.StrippingCC2DD import default_config as config

from StrippingConf.Configuration import StrippingConf, StrippingStream

print config

if (year=='2011') or (year=='2012'):
    config1 = config['CONFIG']
else:
    config1 = config['CC2DD']['CONFIG']

config1['DpmdaughterKaonProbNNk'] = -10000.
config1['DpmdaughterPionProbNNpi'] = -10000.

config1['LcdaughterKaonProbNNk'] = -10000.
config1['LcdaughterPionProbNNpi'] = -10000.
config1['LcdaughterProtonProbNNp'] = -10000.

config1['XcdaughterKaonProbNNk'] = -10000.
config1['XcdaughterPionProbNNpi'] = -10000.
config1['XcdaughterProtonProbNNp'] = -10000.

lb = builder('CC2DD', config1)

MyStream = StrippingStream("MyStream")

for line in lb.lines():
   if line.name() == 'StrippingCC2DDLine':
      MyStream.appendLines([line])


# Configure Stripping
from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()

sc = StrippingConf( Streams = [ MyStream ],
      MaxCandidates = 2000,
      AcceptBadEvents = False,
      BadEventSelection = filterBadEvents )




############
from DecayTreeTuple.Configuration import *
from Configurables import LoKi__Hybrid__TupleTool

#######--------------------B + -> XcXc
B2DsDTuple = DecayTreeTuple("B2DsDTuple")
#B2DsDTuple.Inputs = [rootInTES+"/Phys/CC2DDLine/Particles"]
B2DsDTuple.Inputs = ["Phys/CC2DDLine/Particles"]
B2DsDTuple.Decay = "[psi(3770) -> ^(D_s- -> ^K+ ^K- ^pi-) ^(D+ -> ^K- ^pi+ ^pi+)]CC"
B2DsDTuple.addBranches({
    "B"                 :    "[psi(3770) ->  (D_s- ->  K+  K-  pi-)  (D+ ->  K-  pi+  pi+)]CC",
    "Dsminus"           :    "[psi(3770) -> ^(D_s- ->  K+  K-  pi-)  (D+ ->  K-  pi+  pi+)]CC",
    "Kplus_Dsminus"     :    "[psi(3770) ->  (D_s- -> ^K+  K-  pi-)  (D+ ->  K-  pi+  pi+)]CC",
    "Kminus_Dsminus"    :    "[psi(3770) ->  (D_s- ->  K+ ^K-  pi-)  (D+ ->  K-  pi+  pi+)]CC",
    "piminus_Dsminus"   :    "[psi(3770) ->  (D_s- ->  K+  K- ^pi-)  (D+ ->  K-  pi+  pi+)]CC",
    "Dplus"             :    "[psi(3770) ->  (D_s- ->  K+  K-  pi-) ^(D+ ->  K-  pi+  pi+)]CC",
    "Kminus_Dplus"      :    "[psi(3770) ->  (D_s- ->  K+  K-  pi-)  (D+ -> ^K-  pi+  pi+)]CC",
    "piplus1_Dplus"     :    "[psi(3770) ->  (D_s- ->  K+  K-  pi-)  (D+ ->  K- ^pi+  pi+)]CC",
    "piplus2_Dplus"     :    "[psi(3770) ->  (D_s- ->  K+  K-  pi-)  (D+ ->  K-  pi+ ^pi+)]CC",
})

## Reference point
B2DsDTuple.ToolList += [ "TupleToolKinematic" ]
B2DsDTuple.addTool(TupleToolKinematic, name = "TupleToolKinematic")
B2DsDTuple.TupleToolKinematic.Verbose = True

###################################
# Constraints:
###################################

## B D_s- D+ (mass + PV)constraint Tool
B2DsDTuple.addTool(TupleToolDecay, name = "DTFM_PVC")
B2DsDTuple_DTFM_PVC = B2DsDTuple.B.addTupleTool('TupleToolDecayTreeFitter/DTFM_PVC')
B2DsDTuple_DTFM_PVC.UpdateDaughters = True
B2DsDTuple_DTFM_PVC.Verbose = True
B2DsDTuple_DTFM_PVC.constrainToOriginVertex = True
B2DsDTuple_DTFM_PVC.daughtersToConstrain += [ 'D+']
B2DsDTuple_DTFM_PVC.daughtersToConstrain += [ 'D_s-']

## B D_s- D+ (mass)constraint Tool
B2DsDTuple.addTool(TupleToolDecay, name = "DTFM")
B2DsDTuple_DTFM = B2DsDTuple.B.addTupleTool('TupleToolDecayTreeFitter/DTFM')
B2DsDTuple_DTFM.UpdateDaughters = True
B2DsDTuple_DTFM.Verbose = True
B2DsDTuple_DTFM.constrainToOriginVertex = False
B2DsDTuple_DTFM.daughtersToConstrain += [ 'D+']
B2DsDTuple_DTFM.daughtersToConstrain += [ 'D_s-']

## B (PV)constraint Tool
B2DsDTuple.addTool(TupleToolDecay, name = "PVC")
B2DsDTuple_PVC = B2DsDTuple.B.addTupleTool('TupleToolDecayTreeFitter/PVC')
B2DsDTuple_PVC.UpdateDaughters = True
B2DsDTuple_PVC.Verbose = True
B2DsDTuple_PVC.constrainToOriginVertex = True

B2DsDTuple.ToolList += [ "TupleToolAngles"
                    , "TupleToolEventInfo"
                    , "TupleToolGeometry"
                    , "TupleToolKinematic"
                    , "TupleToolPid"
                    , "TupleToolPrimaries"
                    , "TupleToolPropertime"
                    , "TupleToolTrackInfo"
                    # , "TupleToolCovariances"
                    , "TupleToolBremInfo"
                    , "TupleToolMuonPid"
                    , "TupleToolRecoStats"
                    , "TupleToolDira"
                    , "TupleToolTrackPosition"
                    , "TupleToolL0Data"
                    , "TupleToolMCTruth"
                    , "TupleToolMCBackgroundInfo"
                    ]


B2DsDTuple.ToolList+= [ "TupleToolTISTOS" ]
B2DsDTuple.addTool(TupleToolTISTOS, name = "TupleToolTISTOS")
B2DsDTuple.TupleToolTISTOS.Verbose = True
B2DsDTuple.TupleToolTISTOS.VerboseL0 = True
B2DsDTuple.TupleToolTISTOS.VerboseHlt1 = True
B2DsDTuple.TupleToolTISTOS.VerboseHlt2 = True
B2DsDTuple.TupleToolTISTOS.TriggerList = myTriggerList #trigger_list.trigger_list() 


from Configurables import TupleToolRecoStats
B2DsDTuple.addTool(TupleToolRecoStats, name = "TupleToolRecoStats")
B2DsDTuple.TupleToolRecoStats.Verbose = True


# Add eta                                                            
LoKi_All = B2DsDTuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_All")
LoKi_All.Variables = {
    'ETA' : 'ETA',
    'MinIPCHI2' : 'MIPCHI2DV(PRIMARY)'
    }


## MCTruth info
MCTruth = TupleToolMCTruth()
MCTruth.ToolList = [
    "MCTupleToolAngles"
        ,"MCTupleToolHierarchy"
            ,"MCTupleToolKinematic" ]


if doMC:
    B2DsDTuple.addTool(MCTruth, name = "TupleToolMCTruth")


###########################
##DTFDict B2DsD
###########################
from Configurables import LoKi__Hybrid__DictOfFunctors
from Configurables import LoKi__Hybrid__Dict2Tuple
from Configurables import LoKi__Hybrid__DTFDict as DTFDict
DictTuple = B2DsDTuple.B.addTupleTool(LoKi__Hybrid__Dict2Tuple, "DTFTuple")
DictTuple.addTool(DTFDict,"DTF")
DictTuple.Source = "LoKi::Hybrid::DTFDict/DTF"
DictTuple.NumVar = 46     # reserve a suitable size for the dictionaire
DictTuple.DTF.constrainToOriginVertex = True
DictTuple.DTF.daughtersToConstrain = ['D+', 'D_s-']
DictTuple.DTF.addTool(LoKi__Hybrid__DictOfFunctors,"dict")
DictTuple.DTF.Source = "LoKi::Hybrid::DictOfFunctors/dict"
DictTuple.DTF.dict.Variables = {
    "DTFDict_B_PT"      : "PT",
    "DTFDict_B_M"       : "M",
    'DTFDict_B_PX'                :   "PX",
    'DTFDict_B_PY'                :   "PY",
    'DTFDict_B_PZ'                :   "PZ",
    'DTFDict_B_PT'                :   "PT",
    'DTFDict_B_E'                 :   "E" ,
    "DTFDict_B_MINIPCHI2"         :   "MIPCHI2DV()" ,
    "DTFDict_B_DIRA_OWNPV"        :   "BPVDIRA" ,
    "DTFDict_B_ENDVERTEX_CHI2"    :   "VFASPF(VCHI2)" ,
    "DTFDict_B_ENDVERTEX_NDOF"    :   "VFASPF(VDOF)" ,
    "DTFDict_B_FDCHI2_OWNPV"      :   "BPVVDCHI2" ,
    # D+
    "DTFDict_Dplus_MM"              :   "CHILD(MM              ,ABSID=='D+')",
    "DTFDict_Dplus_M"               :   "CHILD(M               ,ABSID=='D+')",
    "DTFDict_Dplus_PX"              :   "CHILD(PX              ,ABSID=='D+')",
    "DTFDict_Dplus_PY"              :   "CHILD(PY              ,ABSID=='D+')",
    "DTFDict_Dplus_PZ"              :   "CHILD(PZ              ,ABSID=='D+')",
    "DTFDict_Dplus_PT"              :   "CHILD(PT              ,ABSID=='D+')",
    "DTFDict_Dplus_E"               :   "CHILD(E               ,ABSID=='D+')",
    "DTFDict_Dplus_MINIPCHI2"       :   "CHILD(MIPCHI2DV()     ,ABSID=='D+')",
    "DTFDict_Dplus_DIRA_OWNPV"      :   "CHILD(BPVDIRA         ,ABSID=='D+')",
    "DTFDict_Dplus_ENDVERTEX_CHI2"  :   "CHILD(VFASPF(VCHI2)   ,ABSID=='D+')",
    "DTFDict_Dplus_ENDVERTEX_NDOF"  :   "CHILD(VFASPF(VDOF)    ,ABSID=='D+')",
    "DTFDict_Dplus_FDCHI2_OWNPV"    :   "CHILD(BPVVDCHI2       ,ABSID=='D+')",
    # Ds-
    "DTFDict_Dsminus_MM"              :   "CHILD(MM              ,ABSID=='D_s-')",
    "DTFDict_Dsminus_M"               :   "CHILD(M               ,ABSID=='D_s-')",
    "DTFDict_Dsminus_PX"              :   "CHILD(PX              ,ABSID=='D_s-')",
    "DTFDict_Dsminus_PY"              :   "CHILD(PY              ,ABSID=='D_s-')",
    "DTFDict_Dsminus_PZ"              :   "CHILD(PZ              ,ABSID=='D_s-')",
    "DTFDict_Dsminus_PT"              :   "CHILD(PT              ,ABSID=='D_s-')",
    "DTFDict_Dsminus_E"               :   "CHILD(E               ,ABSID=='D_s-')",
    "DTFDict_Dsminus_MINIPCHI2"       :   "CHILD(MIPCHI2DV()     ,ABSID=='D_s-')",
    "DTFDict_Dsminus_DIRA_OWNPV"      :   "CHILD(BPVDIRA         ,ABSID=='D_s-')",
    "DTFDict_Dsminus_ENDVERTEX_CHI2"  :   "CHILD(VFASPF(VCHI2)   ,ABSID=='D_s-')",
    "DTFDict_Dsminus_ENDVERTEX_NDOF"  :   "CHILD(VFASPF(VDOF)    ,ABSID=='D_s-')",
    "DTFDict_Dsminus_FDCHI2_OWNPV"    :   "CHILD(BPVVDCHI2       ,ABSID=='D_s-')",
   }


from Configurables import DaVinci, CheckPV, GaudiSequencer, LoKi__HDRFilter

#from Configurables import CondDB
#CondDB ( LatestGlobalTagByDataType = year )

######## kill old stripping

from Configurables import EventNodeKiller
eventNodeKiller = EventNodeKiller('Stripkiller')
eventNodeKiller.Nodes = [ '/Event/AllStreams', '/Event/Strip' ]


#######################################################################
from Configurables import DaVinci
DaVinci().EvtMax = -1                      # Number of events
DaVinci().PrintFreq = 5000
DaVinci().SkipEvents = 0                       # Events to skip
DaVinci().TupleFile = "Tuple.root"             # NB2DsDTuple
DaVinci().Simulation = doMC
DaVinci().Lumi = not DaVinci().Simulation

DaVinci().appendToMainSequence( [ eventNodeKiller ] )
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().UserAlgorithms = [ B2DsDTuple ]
DaVinci().DataType = year


if doMC:
    local_dict = {
        '2011' : ["/eos/lhcb/grid/prod/lhcb/MC/2011/ALLSTREAMS.DST/00174100/0000/00174100_00000062_5.AllStreams.dst"],
        '2012' : ["/eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00174082/0000/00174082_00000192_5.AllStreams.dst"],
        '2015' : ["/eos/lhcb/grid/prod/lhcb/MC/2015/ALLSTREAMS.DST/00174056/0000/00174056_00000105_6.AllStreams.dst"],
        '2016' : ["/eos/lhcb/grid/prod/lhcb/MC/2016/ALLSTREAMS.DST/00173956/0000/00173956_00000195_7.AllStreams.dst"],
        '2017' : ["/eos/lhcb/grid/prod/lhcb/MC/2017/ALLSTREAMS.DST/00173872/0000/00173872_00000198_7.AllStreams.dst"],
        '2018' : ["/eos/lhcb/grid/prod/lhcb/MC/2018/ALLSTREAMS.DST/00173796/0000/00173796_00000195_7.AllStreams.dst"],
    }
    DaVinci().InputType = 'DST'
    #DaVinci().RootInTES = rootInTES
else:
    local_dict = {
        '2011' : ["/eos/lhcb/grid/prod/lhcb/LHCb/Collision11/CHARMCOMPLETEEVENT.DST/00041840/0000/00041840_00009954_1.charmcompleteevent.dst"],
        '2012' : ["/eos/lhcb/grid/prod/lhcb/LHCb/Collision12/CHARMCOMPLETEEVENT.DST/00050929/0000/00050929_00009148_1.charmcompleteevent.dst"],
        '2015' : ["/eos/lhcb/grid/prod/lhcb/LHCb/Collision15/CHARMCOMPLETEEVENT.DST/00069078/0000/00069078_00009785_1.charmcompleteevent.dst"],
        '2016' : ["/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/CHARMCOMPLETEEVENT.DST/00069603/0000/00069603_00009880_1.charmcompleteevent.dst"],
        '2017' : ["/eos/lhcb/grid/prod/lhcb/LHCb/Collision17/CHARMCOMPLETEEVENT.DST/00071671/0000/00071671_00009449_1.charmcompleteevent.dst"],
        '2018' : ["/eos/lhcb/grid/prod/lhcb/LHCb/Collision18/CHARMCOMPLETEEVENT.DST/00077434/0000/00077434_00005721_1.charmcompleteevent.dst"],
    }
    DaVinci().InputType = 'DST'

###------------------Local Test
if doLocalTest:
    DaVinci().Input = local_dict[year]



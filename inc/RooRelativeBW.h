#ifndef ROORELATIVEBW
#define ROORELATIVEBW

#include "RooAbsPdf.h"
#include "RooRealProxy.h"
#include "RooAbsReal.h"
 
class RooRelativeBW : public RooAbsPdf {
public:
  RooRelativeBW(); 
  RooRelativeBW(const char *name, const char *title,
	      RooAbsReal& _x,
	      RooAbsReal& _mean,
	      RooAbsReal& _width,
	      RooAbsReal& _spin,
	      RooAbsReal& _radius,
	      RooAbsReal& _massa,
	      RooAbsReal& _massb);

  RooRelativeBW(const RooRelativeBW& other, const char* name=0);

  virtual TObject* clone(const char* newname) const { return new RooRelativeBW(*this,newname); }
  inline virtual ~RooRelativeBW() { }

  Double_t Gamma() const;
  Double_t FFunction(Double_t X) const;
  Double_t KFunction(Double_t X) const;

protected:

  RooRealProxy x ;
  RooRealProxy mean ;
  RooRealProxy width ;
  RooRealProxy spin ;
  RooRealProxy radius ;
  RooRealProxy massa ;
  RooRealProxy massb ;
  
  Double_t evaluate() const ;

private:

  ClassDef(RooRelativeBW,1); // Your description goes here...
};
#endif

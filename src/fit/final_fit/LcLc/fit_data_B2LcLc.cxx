#include <iostream>
#include "RooStats/ModelConfig.h"
#include "RooStats/ProfileLikelihoodCalculator.h"
#include "RooStats/HypoTestResult.h"
#include <RooDataSet.h>

#include "/mnt/d/lhcb/B2XcXc/inc/tools.h"
#include "/mnt/d/lhcb/B2XcXc/inc/RooDoubleCB.h"
#include "/mnt/d/lhcb/B2XcXc/inc/fitMCtoGetShape.h"
#include "/mnt/d/lhcb/B2XcXc/inc/getPresel.h"

using namespace RooFit;
using namespace RooStats;

void toyStudy(TString toyDir, TString yearStr);
vector<vector<ValError>> fitVarsPull(TString nameFile, TString toyDir);
void fit(TString cutString )
{
	 //---------Build background PDF------
	lhcbStyle(0.063);
	 
	cout<< "define the expBkg"<<endl;
	 
	 
	vector<int> years;
	years.push_back(2015);
	years.push_back(2016);
	years.push_back(1516);
	years.push_back(2017);
	years.push_back(2018);
	years.push_back(2);

	int w(7);
	system("mkdir -p /mnt/d/lhcb/B2XcXc/public_html/fitParamTables/final_fit/");

	TString fTableData = "/mnt/d/lhcb/B2XcXc/public_html/fitParamTables/final_fit/data_B2LcLc.dat";

	TString fTableData1 = "/mnt/d/lhcb/B2XcXc/public_html/fitParamTables/final_fit/data_B2LcLc_yields.dat";
	TString fTableData2 = "/mnt/d/lhcb/B2XcXc/public_html/fitParamTables/final_fit/data_B2LcLc_meanSigma.dat";
	TString fTableData3 = "/mnt/d/lhcb/B2XcXc/public_html/fitParamTables/final_fit/data_B2LcLc_bkgParams.dat";
	ofstream out_data_yields(fTableData1.Data());
	ofstream out_data_meanSigma(fTableData2.Data());
	ofstream out_data_bkgParams(fTableData3.Data());

	TString fTableMCBd = "/mnt/d/lhcb/B2XcXc/public_html/fitParamTables/final_fit/MC_Bd2LcLc.dat";
	TString fTableMCBs = "/mnt/d/lhcb/B2XcXc/public_html/fitParamTables/final_fit/MC_Bs2LcLc.dat";
	ofstream out_MCBd(fTableMCBd.Data());
	ofstream out_MCBs(fTableMCBs.Data());

	system("mkdir -p /mnt/d/lhcb/B2XcXc/Documents/yields/final_fit/");
	TString sYields = "/mnt/d/lhcb/B2XcXc/Documents/yields/final_fit/yields_B2LcLc.root";

    ////********* 11 12, 3.0fb-1
    ////15, 16, 17, 18,  5.8fb-1
    double lumi1[2] = {1.0, 2.0};
    double lumi2[4] = {0.3, 1.6, 1.7, 2.2};
	for(int i=0; i<years.size(); i++){
		TString yearStr = "Run2";
		if(years[i] ==2) yearStr = "Run"+i2s(years[i]);
		else yearStr = i2s(years[i]);
		
		
		TString yearCut = "(1)";
		if(years[i] == 1112) yearCut = "(year == 2011 || year==2012)";
		else if(years[i] == 2) yearCut = "(year == 2015 || year==2016 || year == 2017 || year==2018)";
		else if(years[i] == 1516) yearCut = "(year == 2015 || year==2016)";
		else yearCut = "(year == " +i2s(years[i])+")";


	 	TString fitVar = "B_DTFM_M_0";
	 	//TString fitVar = "B_M";

	 	RooRealVar m(fitVar, "#it{m}_{LcLc}", 5150, 5550);
		//double binWidth = 8;
		double binWidth = 3.0;
		int nBins = (m.getMax()-m.getMin())/binWidth;
		m.setBins(nBins);
		m.setRange("SB_Left", 5150, 5240);
		m.setRange("SB_Right", 5407, 5550);
	 	

	 	RooRealVar tau("tau", "tau", -0.002, -0.1, 0.); 
	 	RooExponential expBkg("expBkg","background model", m, tau);
	 	
	 	int nentries;

	 	cout<<"read data file"<<endl;
	 	
	 	TString datacut = fitVar+">"+d2s(m.getMin())+" && "+fitVar+"<"+d2s(m.getMax())+" && "+cutString;
		datacut += "&&" + yearCut;


	 	//TChain chData;
		//chData.Add("/mnt/d/lhcb/B2XcXc/root/Bs/data/Data_Bs2LcLc_SB_Run2_TriggerSel_PreSel.root/DecayTree");
	 	//TTree* datatree = chData.CopyTree(datacut);
	 	//datatree->SetBranchStatus("*", 0);
	 	//datatree->SetBranchStatus(fitVar, 1);
		//setBranchStatusTTF(datatree, datacut);
	 	//RooDataSet *datars = new RooDataSet("dataset", "dataset", datatree, m);



    	TPaveText text(0.15, 0.85, 0.35, 0.95, "NDC");
	 	text.SetFillColor(0);
	 	text.SetFillStyle(0);
	 	text.SetLineColor(0);
	 	text.SetLineWidth(0);
	 	text.SetBorderSize(1);
	 	text.SetTextSize(2*0.05);

		text.AddText(yearStr);

		system("mkdir -p /mnt/d/lhcb/B2XcXc/public_html/fit/final_fit/B2LcLc/");
		system("mkdir -p /mnt/d/lhcb/B2XcXc/public_html/fit/final_fit/B2LcLc/syst_2CB");

		//RooRealVar alphalBs("alphalBs","#alpha_{1}", 3.0, 0.01, 5.);
		//RooRealVar alpharBs("alpharBs","#alpha_{2}", 3.0, 0.1, 5.);
		//RooRealVar nlBs("nlBs","n_{1}", 1.5, 0., 10);
		//RooRealVar nrBs("nrBs","n_{2}", 3, 0.01, 10);
		//RooRealVar sigmaBs("sigmaBs","#sigmaBs", 6.3, 5, 10);
		//RooRealVar meanBs("meanBs","#mu", 5366.6);
		//RooRealVar beta_Bs("beta_Bs","#beta_Bs", 0);
		//RooRealVar zeta_Bs("zeta_Bs","#zeta_Bs", 1e-5);
		//RooRealVar l_Bs("l_Bs", "l_Bs", -4, -10, 0.);
		//RooIpatia2 modelBs("modelBs", "modelBs", m, l_Bs, zeta_Bs, beta_Bs, sigmaBs, meanBs, alphalBs, nlBs, alpharBs, nrBs);


		// B_DTFM_M with right tail, try 2CB
		RooRealVar alphalBs("alphalBs","alphalBs", 2, 0.01, 5.);
		RooRealVar nlBs("nlBs","nlBs", 2.0, 1, 20);
		//RooRealVar nlBs("nlBs","nlBs", 10);
		RooRealVar alpharBs("alpharBs", "alpharBs", -2, -5, -0.01);
		//RooRealVar nrBs("nrBs","nrBs", 2.0, 1, 20);
		RooRealVar nrBs("nrBs","nrBs", 10);
		RooRealVar meanBs("meanBs","#mu", 5368, 5363,5373);
		//RooRealVar sigma1Bs("sigma1Bs","#sigma1Bs", 7, 5, 15);
		//RooRealVar sigma2Bs("sigma2Bs","#sigma2Bs", 7, 5, 15);
		RooRealVar sigma1Bs("sigma1Bs","#sigma1Bs", 7, 3, 15);
		RooRealVar sigma2Bs("sigma2Bs","#sigma2Bs", 7, 3, 15);
        
        
		RooCBShape cb1Bs("cb1Bs", "cb1Bs", m, meanBs, sigma1Bs, alphalBs, nlBs);
		RooCBShape cb2Bs("cb2Bs", "cb2Bs", m, meanBs, sigma2Bs, alpharBs, nrBs);

		RooRealVar fracCB1_Bs("fracCB1_Bs", "fracCB1_Bs", 0.5);
		//RooRealVar fracCB1_Bs("fracCB1_Bs", "fracCB1_Bs", 0.5, 0.01, 0.9999);
		RooAddPdf modelBs("modelBs", "modelBs", RooArgList(cb1Bs, cb2Bs), RooArgList(fracCB1_Bs));





		fitMCBMass_addPdf("/mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bs2LcLc_Run2_truth_PIDcor_TrigSel_Presel.root", fitVar, datacut+" && rdmNumber<0.5", "1", m.getMin(), m.getMax(), binWidth,
			modelBs, RooArgList(cb1Bs, cb2Bs), 5300, 5430,
			fitVar, text, "/mnt/d/lhcb/B2XcXc/public_html/fit/final_fit/B2LcLc/syst_2CB/MC_Bs2LcLc_"+yearStr+".pdf", false, false);
		//fitMCBMass("/mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bs2LcLc_Run2_truth_PIDcor_TrigSel_Presel.root", fitVar, datacut, "1", m.getMin(), m.getMax(), binWidth,
		//	modelBs, 5300, 5450,
		//	fitVar, text, "/mnt/d/lhcb/B2XcXc/public_html/fit/final_fit/B2LcLc/MC_Bs2LcLc_"+yearStr+".pdf", false, false);

		alphalBs.setConstant(true);
		alpharBs.setConstant(true);
		nlBs.setConstant(true);
		nrBs.setConstant(true);
		//sigmaBs.setConstant(true);
		sigma1Bs.setConstant(true);
		sigma2Bs.setConstant(true);
			//meanBs.setConstant(true);
			//beta_Bs.setConstant(true);
			//zeta_Bs.setConstant(true);
		//l_Bs.setConstant(true);
		fracCB1_Bs.setConstant(true);

			//fSigmaBs.setConstant(true);
			//fCB_Bs.setConstant(true);

 
		out_MCBs<<setw(w)<<yearStr;
		out_MCBs<<" & "<<setw(w)<<"$"<<dbl2str(meanBs.getVal(),2)<<"\\pm"<<dbl2str(meanBs.getError(),2)<<"$";
		//out_MCBs<<" & "<<setw(w)<<"$"<<dbl2str(sigmaBs.getVal(),2)<<"\\pm"<<dbl2str(sigmaBs.getError(),2)<<"$";
		out_MCBs<<" & "<<setw(w)<<"$"<<dbl2str(sigma1Bs.getVal(),2)<<"\\pm"<<dbl2str(sigma1Bs.getError(),2)<<"$";
		out_MCBs<<" & "<<setw(w)<<"$"<<dbl2str(sigma2Bs.getVal(),2)<<"\\pm"<<dbl2str(sigma2Bs.getError(),2)<<"$";
		out_MCBs<<" & "<<setw(w)<<"$"<<dbl2str(nlBs.getVal(),2)<<"\\pm"<<dbl2str(nlBs.getError(),2)<<"$";
		out_MCBs<<" & "<<setw(w)<<"$"<<dbl2str(nrBs.getVal(),2)<<"\\pm"<<dbl2str(nrBs.getError(),2)<<"$";
		out_MCBs<<" & "<<setw(w)<<"$"<<dbl2str(alphalBs.getVal(),2)<<"\\pm"<<dbl2str(alphalBs.getError(),2)<<"$";
		out_MCBs<<" & "<<setw(w)<<"$"<<dbl2str(alpharBs.getVal(),2)<<"\\pm"<<dbl2str(alpharBs.getError(),2)<<"$";
			//out_MCBs<<" & "<<setw(w)<<"$"<<dbl2str(fSigmaBs.getVal(),2)<<"\\pm"<<dbl2str(fSigmaBs.getError(),2)<<"$";
			//out_MCBs<<" & "<<setw(w)<<"$"<<dbl2str(fCB_Bs.getVal(),2)<<"\\pm"<<dbl2str(fCB_Bs.getError(),2)<<"$";

		//out_MCBs<<" & "<<setw(w)<<"$"<<dbl2str(l_Bs.getVal(),2)<<"\\pm"<<dbl2str(l_Bs.getError(),2)<<"$";
		out_MCBs<<" & "<<setw(w)<<"$"<<dbl2str(fracCB1_Bs.getVal(),2)<<"\\pm"<<dbl2str(fracCB1_Bs.getError(),2)<<"$";
			//out_MCBs<<" & "<<setw(w)<<"$"<<dbl2str(zeta_Bs.getVal(),2)<<"\\pm"<<dbl2str(zeta_Bs.getError(),2)<<"$";
			//out_MCBs<<" & "<<setw(w)<<"$"<<dbl2str(beta_Bs.getVal(),2)<<"\\pm"<<dbl2str(beta_Bs.getError(),2)<<"$";
		out_MCBs<<"\\\\"<<endl;
	


		//RooRealVar alphalBd("alphalBd","#alpha_{1}", 2.7, 0.01, 5.);
		//RooRealVar alpharBd("alpharBd","#alpha_{2}", 2.8, 0, 10);
		//RooRealVar nlBd("nlBd","n_{1}", 2, 0., 10);
		////RooRealVar nrBd("nrBd","n_{2}", 4, 0.01, 10);
		//RooRealVar nrBd("nrBd","n_{2}", 4, 0.01, 20);
		//RooRealVar sigmaBd("sigmaBd","#sigmaBd", 7, 5, 10);
		////RooRealVar deltaM("deltaM", "deltaM", 87.38, 77, 97);
		//RooRealVar deltaM("deltaM", "deltaM", 87.2);
    	//RooFormulaVar meanBd("meanBd","meanBd", "@0-@1", RooArgList(meanBs, deltaM));
		//RooRealVar beta_Bd("beta_Bd","#beta_Bd", 0);
		//RooRealVar zeta_Bd("zeta_Bd","#zeta_Bd", 1e-5);
		//RooRealVar l_Bd("l_Bd", "l_Bd", -4, -10, 0.);

        ////if(years[i] == 2016) {
		////	alphalBd.setMax(20);
		////	alpharBd.setMax(20);
        ////}
		////if(years[i] == 2017) {
		////	alpharBd.setMax(10);
		////}
		//RooIpatia2 modelBd("modelBd", "modelBd", m, l_Bd, zeta_Bd, beta_Bd, sigmaBd, meanBd, alphalBd, nlBd, alpharBd, nrBd);


		// B_DTFM_M with right tail, try 2CB
		RooRealVar alphalBd("alphalBd","alphalBd", 2, 0.01, 5.);
		RooRealVar nlBd("nlBd","nlBd", 2.0, 1, 20);
		//RooRealVar nlBd("nlBd","nlBd", 10);
		RooRealVar alpharBd("alpharBd", "alpharBd", -2, -5, -0.01);
		//RooRealVar nrBd("nrBd","nrBd", 2.0, 1, 10);
		RooRealVar nrBd("nrBd","nrBd", 10);
		RooRealVar deltaM("deltaM", "deltaM", 87.2);
    	RooFormulaVar meanBd("meanBd","meanBd", "@0-@1", RooArgList(meanBs, deltaM));
		//RooRealVar sigma1Bd("sigma1Bd","#sigma1Bd", 7, 5, 15);
		//RooRealVar sigma2Bd("sigma2Bd","#sigma2Bd", 7, 5, 15);
		RooRealVar sigma1Bd("sigma1Bd","#sigma1Bd", 7, 3, 15);
		RooRealVar sigma2Bd("sigma2Bd","#sigma2Bd", 7, 3, 15);
        
        
		RooCBShape cb1Bd("cb1Bd", "cb1Bd", m, meanBd, sigma1Bd, alphalBd, nlBd);
		RooCBShape cb2Bd("cb2Bd", "cb2Bd", m, meanBd, sigma2Bd, alpharBd, nrBd);

		RooRealVar fracCB1_Bd("fracCB1_Bd", "fracCB1_Bd", 0.5);
		//RooRealVar fracCB1_Bd("fracCB1_Bd", "fracCB1_Bd", 0.5, 0.01, 0.9999);
		RooAddPdf modelBd("modelBd", "modelBd", RooArgList(cb1Bd, cb2Bd), RooArgList(fracCB1_Bd));



		RooFitResult result_fitBdMC = fitMCBMass_addPdf("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2LcLc_Run2_truth_PIDcor_TrigSel_Presel.root", fitVar, datacut+" && rdmNumber<0.5", "1", m.getMin(), m.getMax(), binWidth,
			modelBd, RooArgList(cb1Bd, cb2Bd), 5230, 5350,
			fitVar, text, "/mnt/d/lhcb/B2XcXc/public_html/fit/final_fit/B2LcLc/syst_2CB/MC_Bd2LcLc_"+yearStr+".pdf", false, false);
		//RooFitResult result_fitBdMC = fitMCBMass("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2LcLc_Run2_truth_PIDcor_TrigSel_Presel.root", fitVar, datacut+" && rdmNumber<0.5", "1", m.getMin(), m.getMax(), binWidth,
		//	modelBd, 5230, 5350,
		//	fitVar, text, "/mnt/d/lhcb/B2XcXc/public_html/fit/final_fit/B2LcLc/MC_Bd2LcLc_"+yearStr+".pdf", false, false);

		alphalBd.setConstant(true);
		alpharBd.setConstant(true);
		nlBd.setConstant(true);
		nrBd.setConstant(true);
		//sigmaBd.setConstant(true);
		sigma1Bd.setConstant(true);
		sigma2Bd.setConstant(true);
			//meanBd.setConstant(true);
			//beta_Bd.setConstant(true);
			//zeta_Bd.setConstant(true);
		//l_Bd.setConstant(true);
		fracCB1_Bd.setConstant(true);
 
		////sigmaBd.setConstant(true);
		//fSigmaBd.setConstant(true);
		//fCB_Bd.setConstant(true);

		out_MCBd<<setw(w)<<yearStr;
		out_MCBd<<" & "<<setw(w)<<"$"<<dbl2str(meanBd.getVal(),2)<<"\\pm"<<dbl2str(meanBs.getError(),2)<<"$";
		//out_MCBd<<" & "<<setw(w)<<"$"<<dbl2str(sigmaBd.getVal(),2)<<"\\pm"<<dbl2str(sigmaBd.getError(),2)<<"$";
		out_MCBd<<" & "<<setw(w)<<"$"<<dbl2str(sigma1Bd.getVal(),2)<<"\\pm"<<dbl2str(sigma1Bd.getError(),2)<<"$";
		out_MCBd<<" & "<<setw(w)<<"$"<<dbl2str(sigma2Bd.getVal(),2)<<"\\pm"<<dbl2str(sigma2Bd.getError(),2)<<"$";
		out_MCBd<<" & "<<setw(w)<<"$"<<dbl2str(nlBd.getVal(),2)<<"\\pm"<<dbl2str(nlBd.getError(),2)<<"$";
		out_MCBd<<" & "<<setw(w)<<"$"<<dbl2str(nrBd.getVal(),2)<<"\\pm"<<dbl2str(nrBd.getError(),2)<<"$";
		out_MCBd<<" & "<<setw(w)<<"$"<<dbl2str(alphalBd.getVal(),2)<<"\\pm"<<dbl2str(alphalBd.getError(),2)<<"$";
		out_MCBd<<" & "<<setw(w)<<"$"<<dbl2str(alpharBd.getVal(),2)<<"\\pm"<<dbl2str(alpharBd.getError(),2)<<"$";
			//out_MCBd<<" & "<<setw(w)<<"$"<<dbl2str(fSigmaBd.getVal(),2)<<"\\pm"<<dbl2str(fSigmaBd.getError(),2)<<"$";
			//out_MCBd<<" & "<<setw(w)<<"$"<<dbl2str(fCB_Bd.getVal(),2)<<"\\pm"<<dbl2str(fCB_Bd.getError(),2)<<"$";

		//out_MCBd<<" & "<<setw(w)<<"$"<<dbl2str(l_Bd.getVal(),2)<<"\\pm"<<dbl2str(l_Bd.getError(),2)<<"$";
		out_MCBd<<" & "<<setw(w)<<"$"<<dbl2str(fracCB1_Bd.getVal(),2)<<"\\pm"<<dbl2str(fracCB1_Bd.getError(),2)<<"$";
			//out_MCBd<<" & "<<setw(w)<<"$"<<dbl2str(zeta_Bd.getVal(),2)<<"\\pm"<<dbl2str(zeta_Bd.getError(),2)<<"$";
			//out_MCBd<<" & "<<setw(w)<<"$"<<dbl2str(beta_Bd.getVal(),2)<<"\\pm"<<dbl2str(beta_Bd.getError(),2)<<"$";
		out_MCBd<<"\\\\"<<endl;
		



	 	//RooRealVar *nExpBkg = new RooRealVar("nExpBkg","Number of expBkg events", 398, 0., 1e7);
	 	//RooRealVar *nBd = new RooRealVar("nBd","Number of signal Bd", 100, 0., 5000);
	 	//RooRealVar *nBs = new RooRealVar("nBs","Number of signal Bs", 100, 0., 5000);
	 	////RooRealVar *nArgusBkg = new RooRealVar("nArgusBkg","Number of Rec expBkg events",2800, 0., 1e7);


		//int nBin;
        //if(years[i] == 2011) nBin = 1;
        //if(years[i] == 2012) nBin = 2;
        //if(years[i] == 1112) nBin = 3;
        //if(years[i] == 2015) nBin = 4;
        //if(years[i] == 2016) nBin = 5;
        //if(years[i] == 1516) nBin = 6;
        //if(years[i] == 2017) nBin = 7;
        //if(years[i] == 2018) nBin = 8;
        //if(years[i] == 2) nBin = 9;

		//double nBd2DsD = readVEFromFile("/mnt/d/lhcb/B2XcXc/Documents/yields/final_fit/yields_Bd2DsD.root", "yields")[nBin-1].val;
		//double nBs2DsDs = readVEFromFile("/mnt/d/lhcb/B2XcXc/Documents/yields/final_fit/yields_Bs2DsDs.root", "yields")[nBin-1].val;

		//double effBd2LcLc = readVEFromFile("/mnt/d/lhcb/B2XcXc/Documents/effHists/Bd2LcLc_eff_Tot.root", "effs")[nBin-1].val;
		//double effBs2LcLc = readVEFromFile("/mnt/d/lhcb/B2XcXc/Documents/effHists/Bs2LcLc_eff_Tot.root", "effs")[nBin-1].val;
		//double effBd2DsD = readVEFromFile("/mnt/d/lhcb/B2XcXc/Documents/effHists/Bd2DsD_eff_Tot.root", "effs")[nBin-1].val;
		//double effBs2DsDs = readVEFromFile("/mnt/d/lhcb/B2XcXc/Documents/effHists/Bs2DsDs_eff_Tot.root", "effs")[nBin-1].val;

		//nBd->setVal(nBd2DsD*(1.6e-5*6.28e-2*6.28e-2/3.6e-5)*(effBd2LcLc/effBd2DsD));
		//nBs->setVal(nBs2DsDs*(8e-5*6.28e-2*6.28e-2/1.27e-5)*(effBs2LcLc/effBs2DsDs));
		//if(years[i]>=2015 && years[i]<=2018){
		//	nBd->setVal(nBd->getVal() * lumi2[nBin-1] /(0.3+1.6+1.7+2.2));
		//	nBs->setVal(nBs->getVal() * lumi2[nBin-1] /(0.3+1.6+1.7+2.2));
		//}
		//if(years[i]>=2011 && years[i]<=2012){
		//	nBd->setVal(nBd->getVal() * lumi1[nBin-1] /3.0);
		//	nBs->setVal(nBs->getVal() * lumi1[nBin-1] /3.0);
		//}

		//TFile fWSBd2DsD("/mnt/d/lhcb/B2XcXc/Documents/toy/final_fit/Bd2DsD/fitVarsWorkspace.root");
		//RooWorkspace* wsBd2DsD = (RooWorkspace*)fWSBd2DsD.Get("workspace"+yearStr);
		//ValError fSigmaDataMCBdVE = Roo2VE(*((RooRealVar*) wsBd2DsD->var("fSigmaDataMC")));
		//fWSBd2DsD.Close();




		//TFile fWSBs2DsDs("/mnt/d/lhcb/B2XcXc/Documents/toy/final_fit/Bs2DsDs/fitVarsWorkspace.root");
		//RooWorkspace* wsBs2DsDs = (RooWorkspace*)fWSBs2DsDs.Get("workspace"+yearStr);
		//ValError fSigmaDataMCBsVE = Roo2VE(*((RooRealVar*) wsBs2DsDs->var("fSigmaDataMC")));
		//fWSBs2DsDs.Close();


		//RooRealVar fSigmaDataMCBd("fSigmaDataMCBd", "fSigmaDataMCBd", fSigmaDataMCBsVE.val);
		//RooRealVar fSigmaDataMCBs("fSigmaDataMCBs", "fSigmaDataMCBs", fSigmaDataMCBsVE.val);

		//RooFormulaVar sigmaBdData("sigmaBdData", "sigmaBdData", "@0*@1", RooArgList(sigmaBd, fSigmaDataMCBd));
		//RooFormulaVar sigmaBsData("sigmaBsData", "sigmaBsData", "@0*@1", RooArgList(sigmaBs, fSigmaDataMCBs));

		//RooIpatia2 modelBdData("modelBdData", "modelBdData", m, l_Bd, zeta_Bd, beta_Bd, sigmaBdData, meanBd, alphalBd, nlBd, alpharBd, nrBd);
		//RooIpatia2 modelBsData("modelBsData", "modelBsData", m, l_Bs, zeta_Bs, beta_Bs, sigmaBsData, meanBs, alphalBs, nlBs, alpharBs, nrBs);




		//// 现在还没解盲
	 	//RooAddPdf* totPDF = new RooAddPdf("totPDF","Total pdf", RooArgList(modelBdData, modelBsData, expBkg), RooArgList(*nBd, *nBs, *nExpBkg));
	 	////RooAddPdf* totPDF = new RooAddPdf("totPDF","Total pdf", RooArgList(expBkg), RooArgList(*nExpBkg));


		//RooDataSet *toy_Bd = modelBdData.generate(m, nBd->getVal());
		//RooDataSet *toy_Bs = modelBsData.generate(m, nBs->getVal());
		////RooDataSet *toy_Bd = modelBdData.generate(m, 1000);
		////RooDataSet *toy_Bs = modelBsData.generate(m, 2000);
		//RooDataSet *toy_ExpBkg = expBkg.generate(m, nExpBkg->getVal());
		////RooDataSet *toy_ArgusBkg = argusBkg->generate(m, nArgusBkg_D);
		//
		//RooDataSet* datars = new RooDataSet("datars", "datars", m);
		//datars->append(*toy_Bd);
		//datars->append(*toy_Bs);
		//datars->append(*toy_ExpBkg);
		//////datars->append(*toy_ArgusBkg);



	 	//cout<<"all ready"<<endl;
	 	////////////////////// Fit the data/////////////
	 	//RooFitResult *result = totPDF->fitTo(*datars, Save(true), NumCPU(10));
	 	////RooFitResult *result = totPDF->fitTo(*datars, Save(true), NumCPU(10), Range("SB_Left,SB_Right"));



		////if(years[i] == 0){
		//	// save vars into workspace for toy study
		//	system("mkdir -p /mnt/d/lhcb/B2XcXc/Documents/toy/final_fit/B2LcLc");
		//	TFile fWS("/mnt/d/lhcb/B2XcXc/Documents/toy/final_fit/B2LcLc/fitVarsWorkspace.root", "UPDATE");
		//	RooWorkspace wsToy("workspace"+yearStr, "workspace"+yearStr);

		//	wsToy.import(fSigmaDataMCBd);
		//	wsToy.import(fSigmaDataMCBs);

		//	wsToy.import(m);

		//	wsToy.import(tau);


		//	wsToy.import(alphalBd);
		//	wsToy.import(alpharBd);
		//	wsToy.import(nlBd);
		//	wsToy.import(nrBd);
		//	wsToy.import(sigmaBd);
		//	wsToy.import(deltaM);
		//	//wsToy.import(meanBd);
		//	//wsToy.import(fSigmaBd);
		//	//wsToy.import(fCB_Bd);
		//	wsToy.import(beta_Bd);
		//	wsToy.import(zeta_Bd);
		//	wsToy.import(l_Bd);


		//	wsToy.import(alphalBs);
		//	wsToy.import(alpharBs);
		//	wsToy.import(nlBs);
		//	wsToy.import(nrBs);
		//	wsToy.import(sigmaBs);
		//	wsToy.import(meanBs);
		//	//wsToy.import(fSigmaBs);
		//	//wsToy.import(fCB_Bs);
		//	wsToy.import(beta_Bs);
		//	wsToy.import(zeta_Bs);
		//	wsToy.import(l_Bs);


		//	wsToy.import(*nBd);
		//	wsToy.import(*nBs);
		//	wsToy.import(*nExpBkg);
		//	//wsToy.import(*nArgusBkg);

		//	fWS.cd();
		//	wsToy.Write("", TObject::kOverwrite);
		//	fWS.Close();
		////}


		//// save yields
		//TFile fYields(sYields, "UPDATE");

		//TH1D* hYields = (TH1D*)fYields.Get("yields");
		//if(!hYields) hYields = new TH1D ("yields", "yields", 9, 0, 9);
		//
		//hYields->SetBinContent(nBin, nBd->getVal());
		//hYields->SetBinError(nBin, nBd->getError());
		//
		//fYields.cd();
		//hYields->Write("yields", TObject::kOverwrite);

		//fYields.Close();

	 	//

	 	//RooPlot *mframe = m.frame(Title("Bs2LcLc Run2 data"));
	 	//
	 	//cout<<"Fit the data"<<endl;
	 	//
	 	//datars->plotOn(mframe, Name("data"), Bins(nBins));
	 	//totPDF->plotOn(mframe, Name("modelBdData"), Components(modelBdData), LineColor(3), LineStyle(kDashed));
	 	//totPDF->plotOn(mframe, Name("modelBsData"), Components(modelBsData), LineColor(6), LineStyle(kDashed));
	 	//totPDF->plotOn(mframe, Name("exp bkg"), Components(expBkg), LineColor(4), LineStyle(kDashed));
	 	////totPDF->plotOn(mframe, Name("prc bkg"), Components(argusBkg), LineColor(kOrange), LineStyle(kDashed));
	 	//totPDF->plotOn(mframe, Name("totPDF"), LineColor(kRed));
	 	//datars->plotOn(mframe, Name("data"), Bins(nBins));
	 	//totPDF->paramOn(mframe, Layout(0.65, 0.95, 0.95));

	 	////datars->plotOn(mframe, CutRange("SB_Left,SB_Right"), Name("data"), Bins(nBins));
	 	////totPDF->plotOn(mframe, Name("exp bkg"), Components(expBkg), Range(m.getMin(), m.getMax()), LineColor(kRed));
	 	////datars->plotOn(mframe, CutRange("SB_Left,SB_Right"), Name("data"));
	 	////totPDF->paramOn(mframe, Layout(0.65, 0.95, 0.95), Bins(nBins));

		//int result_status = result->status();
		//cout << "result_status = "<< result_status<<endl;
		//result->Print("v");
		//
		//
		//RooHist *pull = mframe->pullHist();
		//pull->GetYaxis()->SetTitle("Pull");
		//RooPlot *framePull = m.frame();
		//framePull->addPlotable(pull, "P");
		//
		//mframe->SetTitle(fitVar);
		//mframe->SetXTitle("#it{m}_{B} [MeV/c^{2}]");
		//mframe->SetYTitle(Form("Candidates / (%.1lf MeV/c^{2})", binWidth));
		//mframe->GetXaxis()->SetTitleSize(0.2);
		//mframe->GetYaxis()->SetTitleSize(0.2);
		//
		//framePull->SetTitle("");
		//framePull->GetYaxis()->SetTitle("Pull");
		//framePull->SetMinimum(-6.);
		//framePull->SetMaximum(6.);
		//framePull->SetMarkerStyle(2);
		////cout<<"-----------------1"<<endl;
		//
		//TCanvas *canv = new TCanvas("canv", "canv", 800, 600);
		//canv->SetFillColor(10);
		//canv->SetBorderMode(0);
		//canv->cd();
		//canv->cd(1);
		//
		//TPad *p1 = new TPad("pad1", "pad1", 0.02, 0.2, 0.98, 0.98, 0);
		//TPad *p2 = new TPad("pad2", "pad2", 0.02, 0.04, 0.98, 0.2, 0);
		//p1->Draw();
		//p2->Draw();
		//
		//p1->cd();
		//mframe->GetYaxis()->SetTitleOffset(1.00);
		//mframe->GetYaxis()->SetLabelSize(0.05);
		//mframe->GetYaxis()->SetTitleSize(0.05);
		//mframe->GetYaxis()->SetNoExponent();
		//mframe->GetXaxis()->SetTitleOffset(1.00);
		//mframe->GetXaxis()->SetTitleSize(0.05);
		//mframe->GetXaxis()->SetLabelSize(0.05);
		//mframe->Draw("E1");
		//text.Draw("same");
		//
		//p2->cd();
		//p2->SetTickx();
		//framePull->GetYaxis()->SetTitleOffset(0.17);
		//framePull->GetXaxis()->SetTitleSize(0.0);
		//framePull->GetXaxis()->SetTickLength(0.09);
		//framePull->GetYaxis()->SetTitleSize(0.2);
		//framePull->GetXaxis()->SetLabelSize(0.0);
		//framePull->GetYaxis()->SetLabelSize(0.2);
		//framePull->GetYaxis()->SetNdivisions(5);
		//framePull->Draw("E1");
		//
		//TLine* lineZero = new TLine(m.getMin(), 0, m.getMax(), 0);
		//lineZero->SetLineStyle(kDashed);
		//lineZero->SetLineColor(kBlack);
		//lineZero->Draw();
		//
		//TLine* lineTwoSigUp = new TLine(m.getMin(), 3, m.getMax(), 3);
		//lineTwoSigUp->SetLineColor(kRed);
		//lineTwoSigUp->Draw();
		//TLine* lineTwoSigDown = new TLine(m.getMin(), -3, m.getMax(), -3);
		//lineTwoSigDown->SetLineColor(kRed);
		//lineTwoSigDown->Draw();


	 	//canv->SaveAs("/mnt/d/lhcb/B2XcXc/public_html/fit/final_fit/B2LcLc/"+yearStr+"_data_B2LcLc_final_fit.pdf");
		////p1->SetLogy();
	 	////canv->SaveAs("/mnt/d/lhcb/B2XcXc/public_html/fit/final_fit/B2LcLc/"+yearStr+"_data_B2LcLc_final_fit_log.pdf");

		//out_data_yields<<setw(w)<<"\\NBdLcLc"<<setw(w)<<" & "<<setw(w)<<"$"<<dbl2str(nBd->getVal(),0)<<"\\pm"<<dbl2str(nBd->getError(),0)<<"$"<<"\\\\"<<endl;
		//out_data_yields<<setw(w)<<"\\NBsLcLc"<<setw(w)<<" & "<<setw(w)<<"$"<<dbl2str(nBs->getVal(),0)<<"\\pm"<<dbl2str(nBs->getError(),0)<<"$"<<"\\\\"<<endl;
		//out_data_yields<<setw(w)<<"\\NExpRunII"<<setw(w)<<" & "<<setw(w)<<"$"<<dbl2str(nExpBkg->getVal(),0)<<"\\pm"<<dbl2str(nExpBkg->getError(),0)<<"$"<<"\\\\"<<endl;
		////out_data_yields<<" & "<<setw(w)<<"$"<<dbl2str(nArgusBkg->getVal(),0)<<"\\pm"<<dbl2str(nArgusBkg->getError(),0)<<"$"<<"\\\\"<<endl;
		//out_data_meanSigma<<setw(w)<<"\\meanBsRunII"<<setw(w)<<" & "<<setw(w)<<"$"<<dbl2str(meanBd.getVal(),1)<<"\\pm"<<dbl2str(meanBs.getError(),1)<<"$"<<"\\\\"<<endl;
		////out_data_meanSigma<<" & "<<setw(w)<<"$"<<dbl2str(sigmaDataBd.val,1)<<"\\pm"<<dbl2str(sigmaDataBd.err,1)<<"$"<<"\\\\"<<endl;
		////out_data_meanSigma<<" & "<<setw(w)<<"$"<<dbl2str(sigmaDataBs.val,1)<<"\\pm"<<dbl2str(sigmaDataBs.err,1)<<"$"<<"\\\\"<<endl;
		//out_data_bkgParams<<setw(w)<<"\\kExpRunII"<<setw(w)<<" & "<<setw(w)<<"$"<<dbl2str(tau.getVal()*1e2,1)<<"\\pm"<<dbl2str(tau.getError()*1e2,1)<<"$"<<"\\\\"<<endl;


		//cout << "Estimated nBd: " << nBd->getVal() << endl;
		//cout << "Estimated nBs: " << nBs->getVal() << endl;
	}

	out_MCBd.close();
	out_data_yields.close();
	out_data_meanSigma.close();
	out_data_bkgParams.close();


	cout << endl << "params print to "+fTableMCBd+" "<< endl;
	system("cat "+fTableMCBd);
	cout << endl;
	system("cat "+fTableMCBs);

	cout << endl << "params print to "+fTableData+" "<< endl;
	system("rm " + fTableData);

	system("cat "+fTableData1+">>"+fTableData);
	system("cat "+fTableData2+">>"+fTableData);
	system("cat "+fTableData3+">>"+fTableData);

	system("rm /mnt/d/lhcb/B2XcXc/public_html/fitParamTables/final_fit/data_B2LcLc_*.dat");
	cout << endl;
	system("cat "+fTableData);

	cout << "yields saved into " + sYields << endl;
}


void fit_data_B2LcLc()
{
	fit(getFinalSel());
	//toyStudy("/mnt/d/lhcb/B2XcXc/Documents/toy/final_fit/B2LcLc", "Run2");
	gROOT->ProcessLine(".q");
}

void toyStudy(TString toyDir, TString yearStr)
{
	lhcbStyle(0.063);
	
	TFile fWS(toyDir+"/fitVarsWorkspace.root");
	RooWorkspace* ws = (RooWorkspace*)fWS.Get("workspace"+yearStr);
	if(!ws){
		cerr << "No workspace"+yearStr + " in " + toyDir+"/fitVarsWorkspace.root" << endl;
		return;
	}
	
	int nBins = 50;
	
	TString namefPoisson = toyDir+"/toyVarsPoisson.root";
	TString fitVar = "B_DTFM_M_0";


	//cout << "save possion resonanced vars..." << endl;
	//// making possion resonance
	//cout << "nPoisson: ";
	//TRandom3 rand;

	//bool savefPoisson = true;

	//if(savefPoisson){
	//	TFile fPoisson(namefPoisson, "RECREATE");
	//	TTree* t = new TTree("DecayTree", "DecayTree");
	//	
	//	// 先把泊松分布的变量存到root里面	
	//	double nBd_D, nBs_D, c_D, tau_D, nExpBkg_D, nArgusBkg_D;
	//	t->Branch("nBd", &nBd_D, "nBd/D");
	//	t->Branch("nBs", &nBs_D, "nBs/D");
	//	//t->Branch("c", &c_D, "c/D");
	//	t->Branch("tau", &tau_D, "tau/D");
	//	t->Branch("nExpBkg", &nExpBkg_D, "nExpBkg/D");
	//	//t->Branch("nArgusBkg", &nArgusBkg_D, "nArgusBkg/D");

	//	RooRealVar* nBd = (RooRealVar*) ws->var("nBd");
	//	RooRealVar* nBs = (RooRealVar*) ws->var("nBs");
	//	//RooRealVar* c = (RooRealVar*) ws->var("c");
	//	RooRealVar* tau = (RooRealVar*) ws->var("tau");
	//	RooRealVar* nExpBkg = (RooRealVar*) ws->var("nExpBkg");
	//	//RooRealVar* nArgusBkg = (RooRealVar*) ws->var("nArgusBkg");

	//	for(int nPoisson=0; nPoisson<1000; nPoisson++){
	//		nBd_D = rand.Poisson(nBd->getVal());
	//		nBs_D = rand.Poisson(nBs->getVal());
	//		//c_D = rand.Poisson(c->getVal()*1.0e4)/1.0e4;
	//		tau_D = -1.0*rand.Poisson(fabs(tau->getVal())*1.0e6)/1.0e6;
	//		nExpBkg_D = rand.Poisson(nExpBkg->getVal());
	//		//nArgusBkg_D = rand.Poisson(nArgusBkg->getVal());

	//		t->Fill();
	//	}
	//	fPoisson.cd();
	//	t->Write();
	//	fPoisson.Close();
	//}
	//cout << "Save toy Poisson vars into: " << namefPoisson << endl;


	//TFile fPoisson(namefPoisson);
	//TTree* t = (TTree*)fPoisson.Get("DecayTree");

	//TString namePoissonTmp = namefPoisson+"Tmp";
	//TFile fPoissonTmp(namePoissonTmp, "RECREATE");
	//fPoissonTmp.cd();
	//TTree* tNew = t->CloneTree(0);

	//// 读取需要震荡的参数
	////double nBd_D, nBs_D, c_D, tau_D, nExpBkg_D, nArgusBkg_D;
	//double nBd_D, nBs_D, tau_D, nExpBkg_D;
	//t->SetBranchAddress("nBd", &nBd_D);
	//t->SetBranchAddress("nBs", &nBs_D);
	////t->SetBranchAddress("c", &c_D);
	//t->SetBranchAddress("tau", &tau_D);
	//t->SetBranchAddress("nExpBkg", &nExpBkg_D);
	////t->SetBranchAddress("nArgusBkg", &nArgusBkg_D);


	//// 震荡后产生的参数
	////double nBd_fitVal, nBs_fitVal, nExpBkg_fitVal, nArgusBkg_fitVal, c_fitVal, tau_fitVal;
	//double nBd_fitVal, nBs_fitVal, nExpBkg_fitVal, tau_fitVal;
	//t->SetBranchStatus("nBd_fitVal", 0);
	//t->SetBranchStatus("nBs_fitVal", 0);
	//t->SetBranchStatus("nExpBkg_fitVal", 0);
	////t->SetBranchStatus("nArgusBkg_fitVal", 0);
	////t->SetBranchStatus("c_fitVal", 0);
	//t->SetBranchStatus("tau_fitVal", 0);
	//tNew->Branch("nBd_fitVal", &nBd_fitVal, "nBd_fitVal/D");
	//tNew->Branch("nBs_fitVal", &nBs_fitVal, "nBs_fitVal/D");
	//tNew->Branch("nExpBkg_fitVal", &nExpBkg_fitVal, "nExpBkg_fitVal/D");
	////tNew->Branch("nArgusBkg_fitVal", &nArgusBkg_fitVal, "nArgusBkg_fitVal/D");
	////tNew->Branch("c_fitVal", &c_fitVal, "c_fitVal/D");
	//tNew->Branch("tau_fitVal", &tau_fitVal, "tau_fitVal/D");
	//
	////double nBd_fitErr, nBs_fitErr, nExpBkg_fitErr, nArgusBkg_fitErr, c_fitErr, tau_fitErr;
	//double nBd_fitErr, nBs_fitErr, nExpBkg_fitErr, tau_fitErr;
	//t->SetBranchStatus("nBd_fitErr", 0);
	//t->SetBranchStatus("nBs_fitErr", 0);
	//t->SetBranchStatus("nExpBkg_fitErr", 0);
	////t->SetBranchStatus("nArgusBkg_fitErr", 0);
	////t->SetBranchStatus("c_fitErr", 0);
	//t->SetBranchStatus("tau_fitErr", 0);
	//tNew->Branch("nBd_fitErr", &nBd_fitErr, "nBd_fitErr/D");
	//tNew->Branch("nBs_fitErr", &nBs_fitErr, "nBs_fitErr/D");
	//tNew->Branch("nExpBkg_fitErr", &nExpBkg_fitErr, "nExpBkg_fitErr/D");
	////tNew->Branch("nArgusBkg_fitErr", &nArgusBkg_fitErr, "nArgusBkg_fitErr/D");
	////tNew->Branch("c_fitErr", &c_fitErr, "c_fitErr/D");
	//tNew->Branch("tau_fitErr", &tau_fitErr, "tau_fitErr/D");
	//
	//double nBd_bias, nBs_bias, nExpBkg_bias, tau_bias;
	//t->SetBranchStatus("nBd_bias", 0);
	//t->SetBranchStatus("nBs_bias", 0);
	//t->SetBranchStatus("nExpBkg_bias", 0);
	////t->SetBranchStatus("nArgusBkg_bias", 0);
	////t->SetBranchStatus("c_bias", 0);
	//t->SetBranchStatus("tau_bias", 0);
	//tNew->Branch("nBd_bias", &nBd_bias, "nBd_bias/D");
	//tNew->Branch("nBs_bias", &nBs_bias, "nBs_bias/D");
	//tNew->Branch("nExpBkg_bias", &nExpBkg_bias, "nExpBkg_bias/D");
	////tNew->Branch("nArgusBkg_bias", &nArgusBkg_bias, "nArgusBkg_bias/D");
	////tNew->Branch("c_bias", &c_bias, "c_bias/D");
	//tNew->Branch("tau_bias", &tau_bias, "tau_bias/D");
	//
	//double nBd_pull, nBs_pull, nExpBkg_pull, tau_pull;
	//t->SetBranchStatus("nBd_pull", 0);
	//t->SetBranchStatus("nBs_pull", 0);
	//t->SetBranchStatus("nExpBkg_pull", 0);
	////t->SetBranchStatus("nArgusBkg_pull", 0);
	////t->SetBranchStatus("c_pull", 0);
	//t->SetBranchStatus("tau_pull", 0);
	//tNew->Branch("nBd_pull", &nBd_pull, "nBd_pull/D");
	//tNew->Branch("nBs_pull", &nBs_pull, "nBs_pull/D");
	//tNew->Branch("nExpBkg_pull", &nExpBkg_pull, "nExpBkg_pull/D");
	////tNew->Branch("nArgusBkg_pull", &nArgusBkg_pull, "nArgusBkg_pull/D");
	////tNew->Branch("c_pull", &c_pull, "c_pull/D");
	//tNew->Branch("tau_pull", &tau_pull, "tau_pull/D");
	//
	//for(int nPoisson=0; nPoisson<1000; nPoisson++){
	//	t->GetEntry(nPoisson);

	//	//TPaveText text(0.15, 0.85, 0.35, 0.95, "NDC");
	//	TPaveText text(0.15, 0.75, 0.35, 0.90, "NDC");
	//	text.SetFillColor(0);
	//	text.SetFillStyle(0);
	//	text.SetLineColor(0);
	//	text.SetLineWidth(0);
	//	text.SetBorderSize(1);
	//	text.SetTextSize(0.05);
	//	
	//	text.AddText(yearStr);
	//	text.AddText("nPoisson: "+i2s(nPoisson));
	//	
	//	////---------Build background PDF------
	//	RooRealVar* m = (RooRealVar*) ws->var(fitVar);
	//	
	//	//RooRealVar* m0 = (RooRealVar*) ws->var("m0");
	//	//RooRealVar* c = (RooRealVar*) ws->var("c");
	//	//m0->setConstant(true);
	//	//c->setVal(c_D);
	//	////c->setConstant(true);

    //	//RooArgusBG* argus_noSmearing = new RooArgusBG("argus_noSmearing","rec background model", *m, *m0, *c);

	//	////// Detector response function
	//	//RooRealVar* m0_g = (RooRealVar*) ws->var("m0_g");
	//	//RooRealVar* sigma_g = (RooRealVar*) ws->var("sigma_g");
	//	//m0_g->setConstant(true);
	//	//sigma_g->setConstant(true);
	//	//
	//	//RooGaussian* gauss = new RooGaussian("gauss", "gauss", *m, *m0_g, *sigma_g);
	//	//RooFFTConvPdf* argusBkg = new RooFFTConvPdf("smearedArgus","argusBkg (X) gauss", *m, *argus_noSmearing, *gauss);
	//	
	//	RooRealVar* tau = (RooRealVar*) ws->var("tau");
	//	tau->setVal(tau_D);
	//	//tau->setConstant(true);
	//	
	//	RooExponential* expBkg = new RooExponential("expBkg","background model", *m, *tau) ;
	//	
	//	
	//	RooRealVar* alphalBs = (RooRealVar*) ws->var("alphalBs");
	//	RooRealVar* alpharBs = (RooRealVar*) ws->var("alpharBs");
	//	RooRealVar* nlBs = (RooRealVar*) ws->var("nlBs");
	//	RooRealVar* nrBs = (RooRealVar*) ws->var("nrBs");
	//	RooRealVar* sigmaBs = (RooRealVar*) ws->var("sigmaBs");
	//	RooRealVar* meanBs = (RooRealVar*) ws->var("meanBs");;
	//	//RooRealVar* fSigmaBs = (RooRealVar*) ws->var("fSigmaBs");
	//	//RooRealVar* fCB_Bs = (RooRealVar*) ws->var("fCB_Bs");
	//	RooRealVar* beta_Bs = (RooRealVar*) ws->var("beta_Bs");
	//	RooRealVar* zeta_Bs = (RooRealVar*) ws->var("zeta_Bs");
	//	RooRealVar* l_Bs = (RooRealVar*) ws->var("l_Bs");

	//	alphalBs->setConstant(true);
	//	alpharBs->setConstant(true);
	//	nlBs->setConstant(true);
	//	nrBs->setConstant(true);
	//	sigmaBs->setConstant(true);
	//	meanBs->setConstant(true);
	//	//fSigmaBs->setConstant(true);
	//	//fCB_Bs->setConstant(true);
	//	beta_Bs->setConstant(true);
	//	zeta_Bs->setConstant(true);
	//	l_Bs->setConstant(true);

	//	//RooFormulaVar* sigma_gausCB_Bs = new RooFormulaVar("sigma_gausCB_Bs", "sigma_gausCB_Bs", "@0*@1", RooArgList(*sigmaBs, *fSigmaBs));
	//	//RooGaussian* gausCB_Bs = new RooGaussian("gausCB_Bs", "gausCB_Bs", *m, *meanBs, *sigma_gausCB_Bs);

	//	////RooDoubleCB* cbBs = new RooDoubleCB("cbBs", "CB for B_{s}", *m, *meanBs, *sigmaBs, *alphalBs, *nlBs, *alpharBs, *nrBs);
	//	//RooCBShape* cbBs = new RooCBShape("cbBs", "cbBs", *m, *meanBs, *sigmaBs, *alphalBs, *nlBs);
	//	//RooAddPdf* modelBs = new RooAddPdf("modelBs", "modelBs", RooArgList(*cbBs, *gausCB_Bs), RooArgList(*fCB_Bs));
	//	
	//	RooIpatia2* modelBs = new RooIpatia2("modelBs", "modelBs", *m, *l_Bs, *zeta_Bs, *beta_Bs, *sigmaBs, *meanBs, *alphalBs, *nlBs, *alpharBs, *nrBs);
	//	
	//	RooRealVar* alphalBd = (RooRealVar*) ws->var("alphalBd");
	//	RooRealVar* alpharBd = (RooRealVar*) ws->var("alpharBd");
	//	RooRealVar* nlBd = (RooRealVar*) ws->var("nlBd");
	//	RooRealVar* nrBd = (RooRealVar*) ws->var("nrBd");
	//	RooRealVar* sigmaBd = (RooRealVar*) ws->var("sigmaBd");
    //	RooFormulaVar* meanBd = new RooFormulaVar("meanBd","meanBd", "@0-87.38", RooArgList(*meanBs));
	//	//RooRealVar* fSigmaBd = (RooRealVar*) ws->var("fSigmaBd");
	//	//RooRealVar* fCB_Bd = (RooRealVar*) ws->var("fCB_Bd");
	//	RooRealVar* beta_Bd = (RooRealVar*) ws->var("beta_Bd");
	//	RooRealVar* zeta_Bd = (RooRealVar*) ws->var("zeta_Bd");
	//	RooRealVar* l_Bd = (RooRealVar*) ws->var("l_Bd");

	//	alphalBd->setConstant(true);
	//	alpharBd->setConstant(true);
	//	nlBd->setConstant(true);
	//	nrBd->setConstant(true);
	//	sigmaBd->setConstant(true);
	//	//meanBd->setConstant(true);
	//	//fSigmaBd->setConstant(true);
	//	//fCB_Bd->setConstant(true);
	//	beta_Bd->setConstant(true);
	//	zeta_Bd->setConstant(true);
	//	l_Bd->setConstant(true);




	//	//RooFormulaVar* sigma_gausCB_Bd = new RooFormulaVar("sigma_gausCB_Bd", "sigma_gausCB_Bd", "@0*@1", RooArgList(*sigmaBd, *fSigmaBd));
	//	//RooGaussian* gausCB_Bd = new RooGaussian("gausCB_Bd", "gausCB_Bd", *m, *meanBd, *sigma_gausCB_Bd);

	//	//RooCBShape* cbBd = new RooCBShape("cbBd", "cbBd", *m, *meanBd, *sigmaBd, *alphalBd, *nlBd);
	//	//RooAddPdf* modelBd = new RooAddPdf("modelBd", "modelBd", RooArgList(*cbBd, *gausCB_Bd), RooArgList(*fCB_Bd));
	//	
	//	RooIpatia2* modelBd = new RooIpatia2("modelBd", "modelBd", *m, *l_Bd, *zeta_Bd, *beta_Bd, *sigmaBd, *meanBd, *alphalBd, *nlBd, *alpharBd, *nrBd);
	//	
	//	RooRealVar* nBd = (RooRealVar*) ws->var("nBd");
	//	RooRealVar* nBs = (RooRealVar*) ws->var("nBs");
	//	RooRealVar* nExpBkg = (RooRealVar*) ws->var("nExpBkg");
	//	//RooRealVar* nArgusBkg = (RooRealVar*) ws->var("nArgusBkg");

	//	double raw_nBd, raw_nBs, raw_nExpBkg;
	//	raw_nBd = nBd->getVal();
	//	raw_nBs = nBs->getVal();
	//	raw_nExpBkg = nExpBkg->getVal();

	//	// 产额也设置下，也许会快一些
	//	nBd->setVal(nBd_D);
	//	nBs->setVal(nBs_D);
	//	nExpBkg->setVal(nExpBkg_D);
	//	//nArgusBkg->setVal(nArgusBkg_D);
	//	
	//	tau->setRange(-0.2, 0.2);

	//	nBd->setRange(0, 1e5);
	//	nBs->setRange(0, 1e5);
	//	nExpBkg->setRange(0, 1e5);

	//	RooDataSet *toy_Bd = modelBd->generate(*m, nBd_D);
	//	RooDataSet *toy_Bs = modelBs->generate(*m, nBs_D);
	//	RooDataSet *toy_ExpBkg = expBkg->generate(*m, nExpBkg_D);
	//	//RooDataSet *toy_ArgusBkg = argusBkg->generate(*m, nArgusBkg_D);
	//	
	//	RooDataSet* datars = new RooDataSet("datars", "datars", *m);
	//	datars->append(*toy_Bd);
	//	datars->append(*toy_Bs);
	//	datars->append(*toy_ExpBkg);
	//	//datars->append(*toy_ArgusBkg);
	//	
	//	//cout << nBd_D << ", "<< nBs_D <<  ", "<< nExpBkg_D <<  ", "<< nArgusBkg_D << endl;
	//	cout << nBd_D << ", "<< nBs_D <<  ", "<< nExpBkg_D  << endl;
	//	
	//	//double nTotEvents = nBd_D + nBs_D + nExpBkg_D + nArgusBkg_D;
	//	double nTotEvents = nBd_D + nBs_D + nExpBkg_D;
	//	
	//	//RooAddPdf* totPDF = new RooAddPdf("totPDF","Total pdf", RooArgList(*modelBd, *modelBs, *expBkg, *argusBkg), RooArgList(*nBd, *nBs, *nExpBkg, *nArgusBkg));
	//	RooAddPdf* totPDF = new RooAddPdf("totPDF","Total pdf", RooArgList(*modelBd, *modelBs, *expBkg), RooArgList(*nBd, *nBs, *nExpBkg));



	//	//////////////////// Fit the data/////////////
	//	//RooFitResult *result = totPDF->fitTo(*datars, Save(true), NumCPU(14));
	//	RooFitResult *result = totPDF->fitTo(*datars, Save(true));

	//	nBd_fitVal = nBd->getVal();
	//	nBs_fitVal = nBs->getVal();
	//	nExpBkg_fitVal = nExpBkg->getVal();
	//	//nArgusBkg_fitVal = nArgusBkg->getVal();
	//	//c_fitVal = c->getVal();
	//	tau_fitVal = tau->getVal();
	//	
	//	nBd_fitErr = nBd->getError();
	//	nBs_fitErr = nBs->getError();
	//	nExpBkg_fitErr = nExpBkg->getError();
	//	//nArgusBkg_fitErr = nArgusBkg->getError();
	//	//c_fitErr = c->getError();
	//	tau_fitErr = tau->getError();
	//	
	//	nBd_bias = (nBd_fitVal - raw_nBd);
	//	nBs_bias = (nBs_fitVal - raw_nBs);
	//	nExpBkg_bias = (nExpBkg_fitVal - raw_nExpBkg);
	//	tau_bias = (tau_fitVal - tau_D);
	//	
	//	nBd_pull = nBd_bias/nBd_fitErr;
	//	nBs_pull = nBs_bias /nBs_fitErr;
	//	nExpBkg_pull = nExpBkg_bias /nExpBkg_fitErr;
	//	tau_pull = tau_bias /tau_fitErr;
	//	
	//	
	//	RooPlot *mframe = m->frame(Title("B2LcLc Run2"));
	//	
	//	cout<<"Fit the data"<<endl;
	//	
	//	datars->plotOn(mframe, Name("data"));
	//	totPDF->plotOn(mframe, Name("model Bd"), Components(*modelBd), LineColor(3), LineStyle(kDashed));
	//	totPDF->plotOn(mframe, Name("model Bs"), Components(*modelBs), LineColor(6), LineStyle(kDashed));
	//	totPDF->plotOn(mframe, Name("exp bkg"), Components(*expBkg), LineColor(4), LineStyle(kDashed));
	//	//totPDF->plotOn(mframe, Name("prc bkg"), Components(*argusBkg), LineColor(kOrange), LineStyle(kDashed));
	//	totPDF->plotOn(mframe, Name("totPDF"), LineColor(kRed));
	//	datars->plotOn(mframe, Name("data"));
	//	totPDF->paramOn(mframe, Layout(0.65, 0.90, 0.85));
	//	
	//	int result_status = result->status();
	//	cout << "result_status = "<< result_status<<endl;
	//	result->Print("v");
	//	
	//	
	//	RooHist *pull = mframe->pullHist();
	//	pull->GetYaxis()->SetTitle("Pull");
	//	RooPlot *framePull = m->frame();
	//	framePull->addPlotable(pull, "P");
	//	
	//	int binWidth = (int)(m->getMax() - m->getMin())/nBins;
	//	mframe->SetTitle(fitVar);
	//	mframe->SetXTitle("#it{m}_{B} [MeV/c^{2}]");
	//	mframe->SetYTitle(Form("Candidates / (%d MeV/c^{2})", binWidth));
	//	mframe->GetXaxis()->SetTitleSize(0.2);
	//	mframe->GetYaxis()->SetTitleSize(0.2);
	//	
	//	framePull->SetTitle("");
	//	framePull->GetYaxis()->SetTitle("Pull");
	//	framePull->SetMinimum(-6.);
	//	framePull->SetMaximum(6.);
	//	framePull->SetMarkerStyle(2);
	//	//cout<<"-----------------1"<<endl;
	//	
	//	TCanvas *canv = new TCanvas("canv", "canv", 800, 600);
	//	canv->SetFillColor(10);
	//	canv->SetBorderMode(0);
	//	canv->cd();
	//	canv->cd(1);
	//	
	//	TPad *p1 = new TPad("pad1", "pad1", 0.02, 0.2, 0.98, 0.98, 0);
	//	TPad *p2 = new TPad("pad2", "pad2", 0.02, 0.04, 0.98, 0.2, 0);
	//	p1->Draw();
	//	p2->Draw();
	//	
	//	p1->cd();
	//	mframe->GetYaxis()->SetTitleOffset(1.00);
	//	mframe->GetYaxis()->SetLabelSize(0.05);
	//	mframe->GetYaxis()->SetTitleSize(0.05);
	//	mframe->GetYaxis()->SetNoExponent();
	//	mframe->GetXaxis()->SetTitleOffset(1.00);
	//	mframe->GetXaxis()->SetTitleSize(0.05);
	//	mframe->GetXaxis()->SetLabelSize(0.05);
	//	mframe->Draw("E1");
	//	text.Draw("same");
	//	
	//	p2->cd();
	//	p2->SetTickx();
	//	framePull->GetYaxis()->SetTitleOffset(0.17);
	//	framePull->GetXaxis()->SetTitleSize(0.0);
	//	framePull->GetXaxis()->SetTickLength(0.09);
	//	framePull->GetYaxis()->SetTitleSize(0.2);
	//	framePull->GetXaxis()->SetLabelSize(0.0);
	//	framePull->GetYaxis()->SetLabelSize(0.2);
	//	framePull->GetYaxis()->SetNdivisions(5);
	//	framePull->Draw("E1");
	//	
	//	TLine* lineZero = new TLine(m->getMin(), 0, m->getMax(), 0);
	//	lineZero->SetLineStyle(kDashed);
	//	lineZero->SetLineColor(kBlack);
	//	lineZero->Draw();
	//	
	//	TLine* lineTwoSigUp = new TLine(m->getMin(), 3, m->getMax(), 3);
	//	lineTwoSigUp->SetLineColor(kRed);
	//	lineTwoSigUp->Draw();
	//	TLine* lineTwoSigDown = new TLine(m->getMin(), -3, m->getMax(), -3);
	//	lineTwoSigDown->SetLineColor(kRed);
	//	lineTwoSigDown->Draw();
	//	
	//	system("mkdir -p "+toyDir+"/plots");
	//	canv->SaveAs(toyDir+"/plots/"+yearStr+"_data_B2LcLc_final_fit_"+i2s(nPoisson)+".pdf");
	//	
	//	delete datars;
	//	
	//	tNew->Fill();
	//}
	//cout<< endl;
	//
	//fPoissonTmp.cd();
	//tNew->Write();
	//fPoissonTmp.Close();
	//fPoisson.Close();

	//system("mv " + namePoissonTmp + " " + namefPoisson);



	
	
	// 读取泊松中心值
	RooRealVar* nBd = (RooRealVar*) ws->var("nBd");
	RooRealVar* nBs = (RooRealVar*) ws->var("nBs");
	RooRealVar* nExpBkg = (RooRealVar*) ws->var("nExpBkg");
	//RooRealVar* nArgusBkg = (RooRealVar*) ws->var("nArgusBkg");
	//RooRealVar* c = (RooRealVar*) ws->var("c");
	RooRealVar* tau = (RooRealVar*) ws->var("tau");
	
	cout << "Mean: " << nBd->getVal() << ", "<< nBs->getVal() <<  ", "<< nExpBkg->getVal() <<  ", "<< tau->getVal() << endl;
	cout << "Poisson vars saved in " + namefPoisson << endl;
	



	vector<TString> varNames;
	varNames.push_back("\\texttt{nBd}");
	varNames.push_back("\\texttt{nBs}");
	varNames.push_back("\\texttt{nExpBkg}");
	//varNames.push_back("\\texttt{nArgusBkg}");
	//varNames.push_back("\\texttt{c}");
	varNames.push_back("\\texttt{tau}");
	//varNames.push_back("\\texttt{meanBs}");

	vector<double> varSetVals;
	varSetVals.push_back(nBd->getVal());
	varSetVals.push_back(nBs->getVal());
	varSetVals.push_back(nExpBkg->getVal());
	//varSetVals.push_back(nArgusBkg->getVal());
	//varSetVals.push_back(c->getVal());
	varSetVals.push_back(tau->getVal());
	//varSetVals.push_back(meanBs->getVal());
	

	vector<vector<ValError>> tableInfo  = fitVarsPull(namefPoisson, toyDir);
	for(int i=0; i<tableInfo.size(); i++){
		int nfixed = 1;
		if(varNames[i].Contains("tau")) nfixed=5;

		cout  <<setw(12)<< varNames[i];
		cout  <<setw(12)<< "& "<< dbl2str(varSetVals[i], nfixed);

		for(int j=0; j<tableInfo[0].size(); j++){
			if(j>=2) nfixed = 2;

			if(j!=2 && j!=3)
				cout  <<setw(12)<< "& "<< dbl2str(tableInfo[i][j].val, nfixed);
			else
				cout  <<setw(12)<< "& "<< tableInfo[i][j].roundToError(1, nfixed);
		}
		cout << " \\\\" << endl;
	}
	

	fWS.Close();
}

vector<vector<ValError>> fitVarsPull(TString nameFile, TString toyDir)
{
	cout << "Fit vars pull using Gaus founction." << endl;
	vector<TString> vars;
	vars.push_back("nBd");
	vars.push_back("nBs");
	vars.push_back("nExpBkg");
	vars.push_back("tau");
	//vars.push_back("meanBs");

	vector<double> pull_edges;
	pull_edges.push_back(5);
	pull_edges.push_back(5);
	pull_edges.push_back(5);
	pull_edges.push_back(5);
	//pull_edges.push_back(5);

	vector<double> bias_edges;
	bias_edges.push_back(35.);
	bias_edges.push_back(35.);
	bias_edges.push_back(60.);
	bias_edges.push_back(0.004);
	//bias_edges.push_back(10.);

	vector<DoubleDouble> fitVal_edges;
	fitVal_edges.push_back(DoubleDouble(10, 70));
	fitVal_edges.push_back(DoubleDouble(15, 90));
	fitVal_edges.push_back(DoubleDouble(300, 440));
	fitVal_edges.push_back(DoubleDouble(-0.004, 0.0005));
	//fitVal_edges.push_back(DoubleDouble(5360, 5369));

	vector<DoubleDouble> fitErr_edges;
	fitErr_edges.push_back(DoubleDouble(5.5, 12));
	fitErr_edges.push_back(DoubleDouble(6, 12));
	fitErr_edges.push_back(DoubleDouble(18, 23));
	fitErr_edges.push_back(DoubleDouble(0.42e-3, 0.51e-3));
	//fitErr_edges.push_back(DoubleDouble(0.5, 1.25));

	vector<vector<ValError>> table(vars.size(), vector<ValError>(5, ValError(0, 0)));
	//var-0: mean of FitVal
	//var-1: mean of FitErr
	//var-2: mean of Pull Gaus
	//var-3: sigma of Pull Gaus
	//var-4: chi2/ndf of Pull fit

	TFile f(nameFile);
	TTree* t = (TTree*)f.Get("DecayTree");
	if(!t) {
		cerr << "No DecayTree in file: " + nameFile << endl;
		return table;
	}

	// fitVal, fitErr histos
	for(int i=0; i<vars.size(); i++){
    	TPaveText text(0.65, 0.85, 0.95, 0.95, "NDC");
	 	text.SetFillColor(0);
	 	text.SetFillStyle(0);
	 	text.SetLineColor(0);
	 	text.SetLineWidth(0);
	 	text.SetBorderSize(1);
	 	//text.SetTextSize(0.08);

	 	RooRealVar observable(vars[i]+"_fitVal", vars[i]+"_fitVal", fitVal_edges[i].one, fitVal_edges[i].two);
		observable.setBins(50);

		TH1D varHist(vars[i]+"_HistoFitVal", vars[i]+"_HistoFitVal", 50, fitVal_edges[i].one, fitVal_edges[i].two);
		t->Draw(vars[i]+"_fitVal>>" + vars[i]+"_HistoFitVal");

		RooDataHist *datars = new RooDataHist("dataset", "dataset", observable, &varHist);
		
	 	RooPlot *mframe = observable.frame(Title(vars[i]+"_fitVal"));
	 	
	 	cout<<"Fit the data"<<endl;
	 	
	 	datars->plotOn(mframe, Name("data"));
		
		double binWidth = 2*(fitVal_edges[i].two - fitVal_edges[i].one)/50;
		mframe->SetTitle(vars[i]+"_fitVal");
		mframe->SetXTitle(vars[i]+"_fitVal");
		mframe->SetYTitle("Candidates / "+d2s(binWidth));
		mframe->GetXaxis()->SetTitleSize(0.2);
		mframe->GetYaxis()->SetTitleSize(0.2);
		
		TCanvas *canv = new TCanvas("canv", "canv", 800, 600);
		canv->SetFillColor(10);
		canv->SetBorderMode(0);
		canv->cd();
		canv->cd(1);
		
		TPad *p1 = new TPad("pad1", "pad1", 0.02, 0.2, 0.98, 0.98, 0);
		p1->Draw();
		
		p1->cd();
		mframe->GetYaxis()->SetTitleOffset(1.00);
		mframe->GetYaxis()->SetLabelSize(0.05);
		mframe->GetYaxis()->SetTitleSize(0.05);
		mframe->GetYaxis()->SetNoExponent();
		mframe->GetXaxis()->SetTitleOffset(1.00);
		mframe->GetXaxis()->SetTitleSize(0.05);
		mframe->GetXaxis()->SetLabelSize(0.05);
		mframe->Draw("E1");
		text.AddText("Mean: "+d2s(varHist.GetMean()));
		text.AddText("Std Dev: "+d2s(varHist.GetStdDev()));
		text.Draw("same");

		table[i][0] = ValError(varHist.GetMean(), 0);
		
		system("mkdir -p "+toyDir);
		canv->SaveAs(toyDir+"/varFitVal_"+vars[i]+".pdf");
	}
	for(int i=0; i<vars.size(); i++){
    	TPaveText text(0.65, 0.85, 0.95, 0.95, "NDC");
	 	text.SetFillColor(0);
	 	text.SetFillStyle(0);
	 	text.SetLineColor(0);
	 	text.SetLineWidth(0);
	 	text.SetBorderSize(1);
	 	//text.SetTextSize(0.08);

	 	RooRealVar observable(vars[i]+"_fitErr", vars[i]+"_fitErr", fitErr_edges[i].one, fitErr_edges[i].two);
		observable.setBins(50);

		TH1D varHist(vars[i]+"_HistoFitErr", vars[i]+"_HistoFitErr", 50, fitErr_edges[i].one, fitErr_edges[i].two);
		t->Draw(vars[i]+"_fitErr>>" + vars[i]+"_HistoFitErr");

		RooDataHist *datars = new RooDataHist("dataset", "dataset", observable, &varHist);
		
	 	RooPlot *mframe = observable.frame(Title(vars[i]+"_fitErr"));
	 	
	 	cout<<"Fit the data"<<endl;
	 	
	 	datars->plotOn(mframe, Name("data"));
		
		double binWidth = 2*(fitErr_edges[i].two - fitErr_edges[i].one)/50;
		mframe->SetTitle(vars[i]+"_fitErr");
		mframe->SetXTitle(vars[i]+"_fitErr");
		mframe->SetYTitle("Candidates / "+d2s(binWidth));
		mframe->GetXaxis()->SetTitleSize(0.2);
		mframe->GetYaxis()->SetTitleSize(0.2);
		
		TCanvas *canv = new TCanvas("canv", "canv", 800, 600);
		canv->SetFillColor(10);
		canv->SetBorderMode(0);
		canv->cd();
		canv->cd(1);
		
		TPad *p1 = new TPad("pad1", "pad1", 0.02, 0.2, 0.98, 0.98, 0);
		p1->Draw();
		
		p1->cd();
		mframe->GetYaxis()->SetTitleOffset(1.00);
		mframe->GetYaxis()->SetLabelSize(0.05);
		mframe->GetYaxis()->SetTitleSize(0.05);
		mframe->GetYaxis()->SetNoExponent();
		mframe->GetXaxis()->SetTitleOffset(1.00);
		mframe->GetXaxis()->SetTitleSize(0.05);
		mframe->GetXaxis()->SetLabelSize(0.05);
		mframe->Draw("E1");
		text.AddText("Mean: "+d2s(varHist.GetMean()));
		text.AddText("Std Dev: "+d2s(varHist.GetStdDev()));
		text.Draw("same");
		
		table[i][1] = ValError(varHist.GetMean(), 0);

		system("mkdir -p "+toyDir);
		canv->SaveAs(toyDir+"/varFitErr_"+vars[i]+".pdf");
	}

	// fit pull
	for(int i=0; i<vars.size(); i++){
    	TPaveText text(0.25, 0.85, 0.45, 0.95, "NDC");
	 	text.SetFillColor(0);
	 	text.SetFillStyle(0);
	 	text.SetLineColor(0);
	 	text.SetLineWidth(0);
	 	text.SetBorderSize(1);
	 	text.SetTextSize(0.08);


	 	RooRealVar observable(vars[i]+"_pull", vars[i]+"_pull", -1.*pull_edges[i], pull_edges[i]);
		observable.setBins(50);

	 	RooDataSet *datars = new RooDataSet("dataset", "dataset", t, observable);
		
		// Detector response function
		RooRealVar mean("#it{#mu}", "m0", 0, -2, 2) ;
		RooRealVar sigma("#it{#sigma}", "sigma", 2, 0.1, 5);
		RooGaussian gauss("gauss", "gauss", observable, mean, sigma);

		
	 	RooFitResult *result = gauss.fitTo(*datars, Save(true), NumCPU(10));


	 	RooPlot *mframe = observable.frame(Title(vars[i]+"_pull"));
	 	
	 	cout<<"Fit the data"<<endl;
	 	
	 	datars->plotOn(mframe, Name("data"));
	 	gauss.plotOn(mframe, Name("gauss"), LineColor(kBlue));
	 	datars->plotOn(mframe, Name("data"));
	 	gauss.paramOn(mframe, Layout(0.70, 0.95, 0.95));

		int result_status = result->status();
		cout << "result_status = "<< result_status<<endl;
		result->Print("v");
		
		
		RooHist *pull = mframe->pullHist();
		pull->GetYaxis()->SetTitle("Pull");
		RooPlot *framePull = observable.frame();
		framePull->addPlotable(pull, "P");
		
		double binWidth = 2*pull_edges[i]/50;
		mframe->SetTitle(vars[i]+"_pull");
		mframe->SetXTitle(vars[i]+"_pull");
		mframe->SetYTitle("Candidates / "+d2s(binWidth));
		mframe->GetXaxis()->SetTitleSize(0.2);
		mframe->GetYaxis()->SetTitleSize(0.2);
		
		framePull->SetTitle("");
		framePull->GetYaxis()->SetTitle("Pull");
		framePull->SetMinimum(-6.);
		framePull->SetMaximum(6.);
		framePull->SetMarkerStyle(2);
		//cout<<"-----------------1"<<endl;
		
		TCanvas *canv = new TCanvas("canv", "canv", 800, 600);
		canv->SetFillColor(10);
		canv->SetBorderMode(0);
		canv->cd();
		canv->cd(1);
		
		TPad *p1 = new TPad("pad1", "pad1", 0.02, 0.2, 0.98, 0.98, 0);
		TPad *p2 = new TPad("pad2", "pad2", 0.02, 0.04, 0.98, 0.2, 0);
		p1->Draw();
		p2->Draw();
		
		p1->cd();
		mframe->GetYaxis()->SetTitleOffset(1.00);
		mframe->GetYaxis()->SetLabelSize(0.05);
		mframe->GetYaxis()->SetTitleSize(0.05);
		mframe->GetYaxis()->SetNoExponent();
		mframe->GetXaxis()->SetTitleOffset(1.00);
		mframe->GetXaxis()->SetTitleSize(0.05);
		mframe->GetXaxis()->SetLabelSize(0.05);
		mframe->Draw("E1");
		text.AddText("#chi^{2}/ndf: "+d2s(mframe->chiSquare()));
		text.Draw("same");
		
		p2->cd();
		p2->SetTickx();
		framePull->GetYaxis()->SetTitleOffset(0.17);
		framePull->GetXaxis()->SetTitleSize(0.0);
		framePull->GetXaxis()->SetTickLength(0.09);
		framePull->GetYaxis()->SetTitleSize(0.2);
		framePull->GetXaxis()->SetLabelSize(0.0);
		framePull->GetYaxis()->SetLabelSize(0.2);
		framePull->GetYaxis()->SetNdivisions(5);
		framePull->Draw("E1");
		
		TLine* lineZero = new TLine(-1*pull_edges[i], 0, pull_edges[i], 0);
		lineZero->SetLineStyle(kDashed);
		lineZero->SetLineColor(kBlack);
		lineZero->Draw();
		
		TLine* lineTwoSigUp = new TLine(-1*pull_edges[i], 3, pull_edges[i], 3);
		lineTwoSigUp->SetLineColor(kRed);
		lineTwoSigUp->Draw();
		TLine* lineTwoSigDown = new TLine(-1*pull_edges[i], -3, pull_edges[i], -3);
		lineTwoSigDown->SetLineColor(kRed);
		lineTwoSigDown->Draw();


		table[i][2] = ValError(mean.getVal(), mean.getError());
		table[i][3] = ValError(sigma.getVal(), sigma.getError());
		table[i][4] = ValError(mframe->chiSquare(), 0);

		system("mkdir -p "+toyDir);
		canv->SaveAs(toyDir+"/varPull_"+vars[i]+".pdf");
	}


	// fit bias, bias = fitVal - setVal
	for(int i=0; i<vars.size(); i++){
    	TPaveText text(0.25, 0.85, 0.45, 0.95, "NDC");
	 	text.SetFillColor(0);
	 	text.SetFillStyle(0);
	 	text.SetLineColor(0);
	 	text.SetLineWidth(0);
	 	text.SetBorderSize(1);
	 	text.SetTextSize(0.08);

		TH1D varHist("varBiasHisto_"+vars[i], "varBiasHisto_"+vars[i], 50, -1*bias_edges[i], bias_edges[i]);
		t->Draw(vars[i]+"-"+vars[i]+"_fitVal>>" + "varBiasHisto_"+vars[i]);

	 	RooRealVar observable(vars[i]+"_bias", vars[i]+"_bias", -1.*bias_edges[i], bias_edges[i]);
		observable.setBins(50);

		RooDataHist *datars = new RooDataHist("dataset", "dataset", observable, &varHist);
		
		// Detector response function
		RooRealVar mean("#it{#mu}", "m0", 0, -5, 5) ;
		RooRealVar sigma("#it{#sigma}", "sigma", 10, 0, 200);
		if(vars[i].Contains("nArgus") || vars[i].Contains("nExp")) sigma.setVal(100);
		//if(vars[i].Contains("tau")) sigma.setVal(0.1);

		RooGaussian gauss("gauss", "gauss", observable, mean, sigma);

		
	 	RooFitResult *result = gauss.fitTo(*datars, Save(true), NumCPU(10));


	 	RooPlot *mframe = observable.frame(Title(vars[i]+"_bias"));
	 	
	 	cout<<"Fit the data"<<endl;
	 	
	 	datars->plotOn(mframe, Name("data"));
	 	gauss.plotOn(mframe, Name("gauss"), LineColor(kBlue));
	 	datars->plotOn(mframe, Name("data"));
	 	gauss.paramOn(mframe, Layout(0.70, 0.95, 0.95));

		int result_status = result->status();
		cout << "result_status = "<< result_status<<endl;
		result->Print("v");
		
		
		RooHist *pull = mframe->pullHist();
		pull->GetYaxis()->SetTitle("Pull");
		RooPlot *framePull = observable.frame();
		framePull->addPlotable(pull, "P");
		
		double binWidth = 2*bias_edges[i]/50;
		mframe->SetTitle(vars[i]+"_bias");
		mframe->SetXTitle(vars[i]+"_bias");
		mframe->SetYTitle("Candidates / "+d2s(binWidth));
		mframe->GetXaxis()->SetTitleSize(0.2);
		mframe->GetYaxis()->SetTitleSize(0.2);
		
		framePull->SetTitle("");
		framePull->GetYaxis()->SetTitle("Pull");
		framePull->SetMinimum(-6.);
		framePull->SetMaximum(6.);
		framePull->SetMarkerStyle(2);
		//cout<<"-----------------1"<<endl;
		
		TCanvas *canv = new TCanvas("canv", "canv", 800, 600);
		canv->SetFillColor(10);
		canv->SetBorderMode(0);
		canv->cd();
		canv->cd(1);
		
		TPad *p1 = new TPad("pad1", "pad1", 0.02, 0.2, 0.98, 0.98, 0);
		TPad *p2 = new TPad("pad2", "pad2", 0.02, 0.04, 0.98, 0.2, 0);
		p1->Draw();
		p2->Draw();
		
		p1->cd();
		mframe->GetYaxis()->SetTitleOffset(1.00);
		mframe->GetYaxis()->SetLabelSize(0.05);
		mframe->GetYaxis()->SetTitleSize(0.05);
		mframe->GetYaxis()->SetNoExponent();
		mframe->GetXaxis()->SetTitleOffset(1.00);
		mframe->GetXaxis()->SetTitleSize(0.05);
		mframe->GetXaxis()->SetLabelSize(0.05);
		mframe->Draw("E1");
		text.AddText("#chi^{2}/ndf: "+d2s(mframe->chiSquare()));
		text.Draw("same");
		
		p2->cd();
		p2->SetTickx();
		framePull->GetYaxis()->SetTitleOffset(0.17);
		framePull->GetXaxis()->SetTitleSize(0.0);
		framePull->GetXaxis()->SetTickLength(0.09);
		framePull->GetYaxis()->SetTitleSize(0.2);
		framePull->GetXaxis()->SetLabelSize(0.0);
		framePull->GetYaxis()->SetLabelSize(0.2);
		framePull->GetYaxis()->SetNdivisions(5);
		framePull->Draw("E1");
		
		TLine* lineZero = new TLine(-1*bias_edges[i], 0, bias_edges[i], 0);
		lineZero->SetLineStyle(kDashed);
		lineZero->SetLineColor(kBlack);
		lineZero->Draw();
		
		TLine* lineTwoSigUp = new TLine(-1*bias_edges[i], 3, bias_edges[i], 3);
		lineTwoSigUp->SetLineColor(kRed);
		lineTwoSigUp->Draw();
		TLine* lineTwoSigDown = new TLine(-1*bias_edges[i], -3, bias_edges[i], -3);
		lineTwoSigDown->SetLineColor(kRed);
		lineTwoSigDown->Draw();


		system("mkdir -p "+toyDir);
		canv->SaveAs(toyDir+"/varBias_"+vars[i]+".pdf");
	}

	return table;
}

#include "/mnt/d/lhcb/B2XcXc/inc/histoTools.h"
#include "/mnt/d/lhcb/B2XcXc/inc/getPresel.h"

void nTracks(){

	lhcbStyle(0.0603);

	vector<TString> treeNames;
	vector<TString> drawTitles;
	vector<TString> cuts;
	vector<TString> weights;
	vector<int> colors;
	vector<TString> drawOptions;


	vector<int> years;
	years.push_back(-1);
	//years.push_back(2015);
	//years.push_back(2016);
	//years.push_back(2017);
	//years.push_back(2018);

	for(int i=0; i<years.size(); i++){
		int year = years[i];

		TString cutYear = "(1)";
		if(year != -1)	cutYear = "year==" + i2s(year);


		//vector<double> legPos = {0, 0, 0, 0};
		vector<double> legPos = {0.5, 0.5, 0.95, 0.95};
		//vector<double> legPos = {0.25, 0.50, 0.65, 0.90};


		vector<Histo1DWithInfo> Bd2DsD_HistoInfos;
		Bd2DsD_HistoInfos.push_back(Histo1DWithInfo("nTracks", "nTracks", "nTracks", 0, 500, 50, false));

		vector<Histo1DWithInfo> Bd2DsD_HistoInfos110;
		Bd2DsD_HistoInfos110.push_back(Histo1DWithInfo("nTracks", "nTracks*1.10", "nTracks", 0, 500, 50, false));

		vector<Histo1DWithInfo> Bd2DsD_HistoInfos111;
		Bd2DsD_HistoInfos111.push_back(Histo1DWithInfo("nTracks", "nTracks*1.11", "nTracks", 0, 500, 50, false));

		vector<Histo1DWithInfo> Bd2DsD_HistoInfos112;
		Bd2DsD_HistoInfos112.push_back(Histo1DWithInfo("nTracks", "nTracks*1.12", "nTracks", 0, 500, 50, false));

		treeNames.clear();
		drawTitles.clear();
		cuts.clear();
		weights.clear();
		colors.clear();
		drawOptions.clear();


		drawTitles.push_back("sWeighted DATA");
		drawOptions.push_back("E1");

		drawTitles.push_back("MC 1.10");
		drawOptions.push_back("E1");
		drawTitles.push_back("MC 1.11");
		drawOptions.push_back("E1");
		drawTitles.push_back("MC 1.12");
		drawOptions.push_back("E1");


		vector<vector<TH1D>> histosBd2DsD;
		histosBd2DsD.push_back(getVarsHistos1D("/mnt/d/lhcb/B2XcXc/root/Bd/data/Data_Bd2DsD_Run2_TriggerSel_PreSel_sWeighted.root", cutYear, "(sig_sw)", getBeautifulColour(0), Bd2DsD_HistoInfos));
		histosBd2DsD.push_back(getVarsHistos1D("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2DsD_Run2_truth_PIDcor_TrigSel_Presel.root", cutYear, "(1)", getBeautifulColour(1), Bd2DsD_HistoInfos110));
		histosBd2DsD.push_back(getVarsHistos1D("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2DsD_Run2_truth_PIDcor_TrigSel_Presel.root", cutYear, "(1)", getBeautifulColour(2), Bd2DsD_HistoInfos111));
		histosBd2DsD.push_back(getVarsHistos1D("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2DsD_Run2_truth_PIDcor_TrigSel_Presel.root", cutYear, "(1)", getBeautifulColour(3), Bd2DsD_HistoInfos112));



		TString plotDir_Bd2DsD = "/mnt/d/lhcb/B2XcXc/public_html/compareDataMC/Bd2DsD";
		if(year != -1) plotDir_Bd2DsD += "/every_year/"+i2s(year);

		system("mkdir -p " + plotDir_Bd2DsD);
		drawHistos(histosBd2DsD,
				Bd2DsD_HistoInfos110,
				drawTitles, drawOptions,
				legPos,
				plotDir_Bd2DsD);


		vector<Histo1DWithInfo> Bs2DsDs_HistoInfos;
		Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("nTracks", "nTracks", "nTracks", 0, 500, 50, false));

		vector<Histo1DWithInfo> Bs2DsDs_HistoInfos110;
		Bs2DsDs_HistoInfos110.push_back(Histo1DWithInfo("nTracks", "nTracks*1.10", "nTracks", 0, 500, 50, false));

		vector<Histo1DWithInfo> Bs2DsDs_HistoInfos111;
		Bs2DsDs_HistoInfos111.push_back(Histo1DWithInfo("nTracks", "nTracks*1.11", "nTracks", 0, 500, 50, false));

		vector<Histo1DWithInfo> Bs2DsDs_HistoInfos112;
		Bs2DsDs_HistoInfos112.push_back(Histo1DWithInfo("nTracks", "nTracks*1.12", "nTracks", 0, 500, 50, false));

		treeNames.clear();
		drawTitles.clear();
		cuts.clear();
		weights.clear();
		colors.clear();
		drawOptions.clear();


		drawTitles.push_back("sWeighted DATA");
		drawOptions.push_back("E1");

		drawTitles.push_back("MC 1.10");
		drawOptions.push_back("E1");
		drawTitles.push_back("MC 1.11");
		drawOptions.push_back("E1");
		drawTitles.push_back("MC 1.12");
		drawOptions.push_back("E1");


		vector<vector<TH1D>> histosBs2DsDs;
		histosBs2DsDs.push_back(getVarsHistos1D("/mnt/d/lhcb/B2XcXc/root/Bs/data/Data_Bs2DsDs_Run2_TriggerSel_PreSel_sWeighted.root", cutYear, "(sig_sw)", getBeautifulColour(0), Bs2DsDs_HistoInfos));
		histosBs2DsDs.push_back(getVarsHistos1D("/mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bs2DsDs_Run2_truth_PIDcor_TrigSel.root", cutYear, "(1)", getBeautifulColour(1), Bs2DsDs_HistoInfos110));
		histosBs2DsDs.push_back(getVarsHistos1D("/mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bs2DsDs_Run2_truth_PIDcor_TrigSel.root", cutYear, "(1)", getBeautifulColour(2), Bs2DsDs_HistoInfos111));
		histosBs2DsDs.push_back(getVarsHistos1D("/mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bs2DsDs_Run2_truth_PIDcor_TrigSel.root", cutYear, "(1)", getBeautifulColour(3), Bs2DsDs_HistoInfos112));



		TString plotDir_Bs2DsDs = "/mnt/d/lhcb/B2XcXc/public_html/compareDataMC/Bs2DsDs";
		if(year != -1) plotDir_Bs2DsDs += "/every_year/"+i2s(year);

		system("mkdir -p " + plotDir_Bs2DsDs);
		drawHistos(histosBs2DsDs,
				Bs2DsDs_HistoInfos110,
				drawTitles, drawOptions,
				legPos,
				plotDir_Bs2DsDs);


	}
	gROOT->ProcessLine(".q");
}

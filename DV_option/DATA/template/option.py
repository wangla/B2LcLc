from os import environ
from GaudiKernel.SystemOfUnits import *
from Gaudi.Configuration import *
from Configurables import GaudiSequencer, CombineParticles, OfflineVertexFitter
from Configurables import DecayTreeTuple, EventTuple, TupleToolTrigger, TupleToolTISTOS,FilterDesktop
from Configurables import BackgroundCategory, TupleToolDecay, TupleToolVtxIsoln,TupleToolPid,EventCountHisto,TupleToolRecoStats,TupleToolKinematic, SubstitutePID
from Configurables import TupleToolVeto, TupleToolTagging, TupleToolL0Data, TupleToolL0Calo
from Configurables import TupleToolMCTruth, TupleToolMCBackgroundInfo
from Configurables import MCTupleToolAngles, MCTupleToolHierarchy, MCTupleToolKinematic, MCTupleToolPrimaries, MCTupleToolReconstructed, MCTupleToolInteractions, PrintMCTree, PrintMCDecayTreeTool  
from GaudiConfUtils.ConfigurableGenerators import  CombineParticles
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand, AutomaticData
from GaudiConfUtils.ConfigurableGenerators import DaVinci__N3BodyDecays

year = 'YEAR'

doMC = 0
doLocalTest = 0

stream = 'CharmCompleteEvent'
if doMC: stream = 'AllStreams'
rootInTES = '/Event/%s'%stream

from Configurables import DecayTreeTuple
from Configurables import TupleToolTrigger, TupleToolDecay, TupleToolTISTOS

# Trigger lines
myTriggerList = [
             #L0
              'L0HadronDecision'
             ,'L0ElectronDecision'
             ,'L0ElectronHiDecision'
             ,'L0MuonDecision'
             ,'L0DiMuonDecision'
             ,'L0PhotonDecision'
             ,'L0MuonHighDecision'
             #Hlt1
             ,'Hlt1TrackMVADecision'
             ,'Hlt1TwoTrackMVADecision'
             ,'Hlt1GlobalDecision'
             ,'Hlt1TrackAllL0Decision'
             #Hlt2
             ,'Hlt2Topo2BodyBBDTDecision'
             ,'Hlt2Topo3BodyBBDTDecision'
             ,'Hlt2Topo4BodyBBDTDecision'
             ,'Hlt2BHadB02PpPpPmPmDecision'
             ,'Hlt2Topo2BodyDecision'
             ,'Hlt2Topo3BodyDecision'
             ,'Hlt2Topo4BodyDecision'
             ,'Hlt2GlobalDecision'
]

############
from DecayTreeTuple.Configuration import *
from Configurables import LoKi__Hybrid__TupleTool

#######--------------------B + -> XcXc
B2DsDTuple = DecayTreeTuple("B2DsDTuple")
B2DsDTuple.Inputs = [rootInTES+"/Phys/CC2DDLine/Particles"]
B2DsDTuple.Decay = "[psi(3770) -> ^(D_s- -> ^K+ ^K- ^pi-) ^(D+ -> ^K- ^pi+ ^pi+)]CC"
B2DsDTuple.addBranches({
    "B"                 :    "[psi(3770) ->  (D_s- ->  K+  K-  pi-)  (D+ ->  K-  pi+  pi+)]CC",
    "Dsminus"           :    "[psi(3770) -> ^(D_s- ->  K+  K-  pi-)  (D+ ->  K-  pi+  pi+)]CC",
    "Kplus_Dsminus"     :    "[psi(3770) ->  (D_s- -> ^K+  K-  pi-)  (D+ ->  K-  pi+  pi+)]CC",
    "Kminus_Dsminus"    :    "[psi(3770) ->  (D_s- ->  K+ ^K-  pi-)  (D+ ->  K-  pi+  pi+)]CC",
    "piminus_Dsminus"   :    "[psi(3770) ->  (D_s- ->  K+  K- ^pi-)  (D+ ->  K-  pi+  pi+)]CC",
    "Dplus"             :    "[psi(3770) ->  (D_s- ->  K+  K-  pi-) ^(D+ ->  K-  pi+  pi+)]CC",
    "Kminus_Dplus"      :    "[psi(3770) ->  (D_s- ->  K+  K-  pi-)  (D+ -> ^K-  pi+  pi+)]CC",
    "piplus1_Dplus"     :    "[psi(3770) ->  (D_s- ->  K+  K-  pi-)  (D+ ->  K- ^pi+  pi+)]CC",
    "piplus2_Dplus"     :    "[psi(3770) ->  (D_s- ->  K+  K-  pi-)  (D+ ->  K-  pi+ ^pi+)]CC",
})

## Reference point
B2DsDTuple.ToolList += [ "TupleToolKinematic" ]
B2DsDTuple.addTool(TupleToolKinematic, name = "TupleToolKinematic")
B2DsDTuple.TupleToolKinematic.Verbose = True

###################################
# Constraints:
###################################

## B D_s- D+ (mass + PV)constraint Tool
B2DsDTuple.addTool(TupleToolDecay, name = "DTFM_PVC")
B2DsDTuple_DTFM_PVC = B2DsDTuple.B.addTupleTool('TupleToolDecayTreeFitter/DTFM_PVC')
B2DsDTuple_DTFM_PVC.UpdateDaughters = True
B2DsDTuple_DTFM_PVC.Verbose = True
B2DsDTuple_DTFM_PVC.constrainToOriginVertex = True
B2DsDTuple_DTFM_PVC.daughtersToConstrain += [ 'D+']
B2DsDTuple_DTFM_PVC.daughtersToConstrain += [ 'D_s-']

## B D_s- D+ (mass)constraint Tool
B2DsDTuple.addTool(TupleToolDecay, name = "DTFM")
B2DsDTuple_DTFM = B2DsDTuple.B.addTupleTool('TupleToolDecayTreeFitter/DTFM')
B2DsDTuple_DTFM.UpdateDaughters = True
B2DsDTuple_DTFM.Verbose = True
B2DsDTuple_DTFM.constrainToOriginVertex = False
B2DsDTuple_DTFM.daughtersToConstrain += [ 'D+']
B2DsDTuple_DTFM.daughtersToConstrain += [ 'D_s-']

## B (PV)constraint Tool
B2DsDTuple.addTool(TupleToolDecay, name = "PVC")
B2DsDTuple_PVC = B2DsDTuple.B.addTupleTool('TupleToolDecayTreeFitter/PVC')
B2DsDTuple_PVC.UpdateDaughters = True
B2DsDTuple_PVC.Verbose = True
B2DsDTuple_PVC.constrainToOriginVertex = True

###########################
##DTFDict B2DsD
###########################
from Configurables import LoKi__Hybrid__DictOfFunctors
from Configurables import LoKi__Hybrid__Dict2Tuple
from Configurables import LoKi__Hybrid__DTFDict as DTFDict
DictTuple = B2DsDTuple.B.addTupleTool(LoKi__Hybrid__Dict2Tuple, "DTFTuple")
DictTuple.addTool(DTFDict,"DTF")
DictTuple.Source = "LoKi::Hybrid::DTFDict/DTF"
DictTuple.NumVar = 46     # reserve a suitable size for the dictionaire
DictTuple.DTF.constrainToOriginVertex = True
DictTuple.DTF.daughtersToConstrain = ['D+','D_s-']
#DictTuple.DTF.daughtersToConstrain = ['D_s-']
DictTuple.DTF.addTool(LoKi__Hybrid__DictOfFunctors,"dict")
DictTuple.DTF.Source = "LoKi::Hybrid::DictOfFunctors/dict"
DictTuple.DTF.dict.Variables = {
    "DTFDict_B_PT"      : "PT",
    "DTFDict_B_M"       : "M",
    'DTFDict_B_PX'                :   "PX",
    'DTFDict_B_PY'                :   "PY",
    'DTFDict_B_PZ'                :   "PZ",
    'DTFDict_B_PT'                :   "PT",            
    'DTFDict_B_E'                 :   "E" ,
    "DTFDict_B_MINIPCHI2"         :   "MIPCHI2DV()" ,
    "DTFDict_B_DIRA_OWNPV"        :   "BPVDIRA" ,
    "DTFDict_B_ENDVERTEX_CHI2"    :   "VFASPF(VCHI2)" ,
    "DTFDict_B_ENDVERTEX_NDOF"    :   "VFASPF(VDOF)" ,
    "DTFDict_B_FDCHI2_OWNPV"      :   "BPVVDCHI2" ,
    # D+
    "DTFDict_Dplus_MM"              :   "CHILD(MM              ,ABSID=='D+')",
    "DTFDict_Dplus_M"               :   "CHILD(M               ,ABSID=='D+')",
    "DTFDict_Dplus_PX"              :   "CHILD(PX              ,ABSID=='D+')",
    "DTFDict_Dplus_PY"              :   "CHILD(PY              ,ABSID=='D+')",
    "DTFDict_Dplus_PZ"              :   "CHILD(PZ              ,ABSID=='D+')",
    "DTFDict_Dplus_PT"              :   "CHILD(PT              ,ABSID=='D+')",
    "DTFDict_Dplus_E"               :   "CHILD(E               ,ABSID=='D+')", 
    "DTFDict_Dplus_MINIPCHI2"       :   "CHILD(MIPCHI2DV()     ,ABSID=='D+')",
    "DTFDict_Dplus_DIRA_OWNPV"      :   "CHILD(BPVDIRA         ,ABSID=='D+')",
    "DTFDict_Dplus_ENDVERTEX_CHI2"  :   "CHILD(VFASPF(VCHI2)   ,ABSID=='D+')",
    "DTFDict_Dplus_ENDVERTEX_NDOF"  :   "CHILD(VFASPF(VDOF)    ,ABSID=='D+')",
    "DTFDict_Dplus_FDCHI2_OWNPV"    :   "CHILD(BPVVDCHI2       ,ABSID=='D+')",
    # Ds-
    "DTFDict_Dsminus_MM"              :   "CHILD(MM              ,ABSID=='D_s-')",
    "DTFDict_Dsminus_M"               :   "CHILD(M               ,ABSID=='D_s-')",
    "DTFDict_Dsminus_PX"              :   "CHILD(PX              ,ABSID=='D_s-')",
    "DTFDict_Dsminus_PY"              :   "CHILD(PY              ,ABSID=='D_s-')",
    "DTFDict_Dsminus_PZ"              :   "CHILD(PZ              ,ABSID=='D_s-')",
    "DTFDict_Dsminus_PT"              :   "CHILD(PT              ,ABSID=='D_s-')",
    "DTFDict_Dsminus_E"               :   "CHILD(E               ,ABSID=='D_s-')",
    "DTFDict_Dsminus_MINIPCHI2"       :   "CHILD(MIPCHI2DV()     ,ABSID=='D_s-')",
    "DTFDict_Dsminus_DIRA_OWNPV"      :   "CHILD(BPVDIRA         ,ABSID=='D_s-')",
    "DTFDict_Dsminus_ENDVERTEX_CHI2"  :   "CHILD(VFASPF(VCHI2)   ,ABSID=='D_s-')",
    "DTFDict_Dsminus_ENDVERTEX_NDOF"  :   "CHILD(VFASPF(VDOF)    ,ABSID=='D_s-')",
    "DTFDict_Dsminus_FDCHI2_OWNPV"    :   "CHILD(BPVVDCHI2       ,ABSID=='D_s-')",
   }


B2DsDTuple.ToolList += [ "TupleToolAngles"
                    , "TupleToolEventInfo"
                    , "TupleToolGeometry"
                    , "TupleToolKinematic"
                    , "TupleToolPid"
                    , "TupleToolPrimaries"
                    , "TupleToolPropertime"
                    , "TupleToolTrackInfo"
                    # , "TupleToolCovariances"
                    , "TupleToolBremInfo"
                    , "TupleToolMuonPid"
                    , "TupleToolRecoStats"
                    , "TupleToolDira"
                    , "TupleToolTrackPosition"
                    , "TupleToolL0Data"
                    , "TupleToolMCTruth"
                    , "TupleToolMCBackgroundInfo"
                    , "TupleToolDecayTreeFitter"
                    ]


B2DsDTuple.ToolList+= [ "TupleToolTISTOS" ]
B2DsDTuple.addTool(TupleToolTISTOS, name = "TupleToolTISTOS")
B2DsDTuple.TupleToolTISTOS.Verbose = True
B2DsDTuple.TupleToolTISTOS.VerboseL0 = True
B2DsDTuple.TupleToolTISTOS.VerboseHlt1 = True
B2DsDTuple.TupleToolTISTOS.VerboseHlt2 = True
B2DsDTuple.TupleToolTISTOS.TriggerList = myTriggerList #trigger_list.trigger_list() 


from Configurables import TupleToolRecoStats
B2DsDTuple.addTool(TupleToolRecoStats, name = "TupleToolRecoStats")
B2DsDTuple.TupleToolRecoStats.Verbose = True


# Add eta                                                            
LoKi_All = B2DsDTuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_All")
LoKi_All.Variables = {
    'ETA' : 'ETA',
    'MinIPCHI2' : 'MIPCHI2DV(PRIMARY)'
    }


## MCTruth info
MCTruth = TupleToolMCTruth()
MCTruth.ToolList = [
    "MCTupleToolAngles"
        ,"MCTupleToolHierarchy"
            ,"MCTupleToolKinematic" ]


if doMC:
    B2DsDTuple.addTool(MCTruth, name = "TupleToolMCTruth")






############
from DecayTreeTuple.Configuration import *
from Configurables import LoKi__Hybrid__TupleTool

#######--------------------B + -> Lcp
B2DsDsTuple = DecayTreeTuple("B2DsDsTuple")
B2DsDsTuple.Inputs = [rootInTES+"/Phys/CC2DDLine/Particles"]
B2DsDsTuple.Decay = "[psi(3770) -> ^(D_s- -> ^K+ ^K- ^pi-) ^(D_s+ -> ^K- ^K+ ^pi+)]CC"
B2DsDsTuple.addBranches({
    "B"                 :    "[psi(3770) ->  (D_s- ->  K+  K-  pi-)  (D_s+ ->  K-  K+  pi+)]CC",
    "Dsminus"           :    "[psi(3770) -> ^(D_s- ->  K+  K-  pi-)  (D_s+ ->  K-  K+  pi+)]CC",
    "Kplus_Dsminus"     :    "[psi(3770) ->  (D_s- -> ^K+  K-  pi-)  (D_s+ ->  K-  K+  pi+)]CC",
    "Kminus_Dsminus"    :    "[psi(3770) ->  (D_s- ->  K+ ^K-  pi-)  (D_s+ ->  K-  K+  pi+)]CC",
    "piminus_Dsminus"   :    "[psi(3770) ->  (D_s- ->  K+  K- ^pi-)  (D_s+ ->  K-  K+  pi+)]CC",
    "Dsplus"            :    "[psi(3770) ->  (D_s- ->  K+  K-  pi-) ^(D_s+ ->  K-  K+  pi+)]CC",
    "Kminus_Dsplus"     :    "[psi(3770) ->  (D_s- ->  K+  K-  pi-)  (D_s+ -> ^K-  K+  pi+)]CC",
    "Kplus_Dsplus"      :    "[psi(3770) ->  (D_s- ->  K+  K-  pi-)  (D_s+ ->  K- ^K+  pi+)]CC",
    "piplus_Dsplus"     :    "[psi(3770) ->  (D_s- ->  K+  K-  pi-)  (D_s+ ->  K-  K+ ^pi+)]CC",
})

## Reference point
B2DsDsTuple.ToolList += [ "TupleToolKinematic" ]
B2DsDsTuple.addTool(TupleToolKinematic, name = "TupleToolKinematic")
B2DsDsTuple.TupleToolKinematic.Verbose = True

###################################
# Lc Constraints:
###################################
### B Lambda_c~- Lambda_c+ (mass + PV)constraint Tool
B2DsDsTuple.addTool(TupleToolDecay, name = "DTFM_PVC")
B2DsDsTuple_DTFM_PVC = B2DsDsTuple.B.addTupleTool('TupleToolDecayTreeFitter/DTFM_PVC')
B2DsDsTuple_DTFM_PVC.UpdateDaughters = True
B2DsDsTuple_DTFM_PVC.Verbose = True
B2DsDsTuple_DTFM_PVC.constrainToOriginVertex = True
B2DsDsTuple_DTFM_PVC.daughtersToConstrain += [ 'D_s+']
B2DsDsTuple_DTFM_PVC.daughtersToConstrain += [ 'D_s-']

## B Lambda_c+ (mass)constraint Tool
B2DsDsTuple.addTool(TupleToolDecay, name = "DTFM")
B2DsDsTuple_DTFM = B2DsDsTuple.B.addTupleTool('TupleToolDecayTreeFitter/DTFM')
B2DsDsTuple_DTFM.UpdateDaughters = True
B2DsDsTuple_DTFM.Verbose = True
B2DsDsTuple_DTFM.constrainToOriginVertex = False
B2DsDsTuple_DTFM.daughtersToConstrain += [ 'D_s+']
B2DsDsTuple_DTFM.daughtersToConstrain += [ 'D_s-']

## B (PV)constraint Tool
B2DsDsTuple.addTool(TupleToolDecay, name = "PVC")
B2DsDsTuple_PVC = B2DsDsTuple.B.addTupleTool('TupleToolDecayTreeFitter/PVC')
B2DsDsTuple_PVC.UpdateDaughters = True
B2DsDsTuple_PVC.Verbose = True
B2DsDsTuple_PVC.constrainToOriginVertex = True
#

###########################
##DTFDict Bs2DsDsbar
###########################
from Configurables import LoKi__Hybrid__DictOfFunctors
from Configurables import LoKi__Hybrid__Dict2Tuple
from Configurables import LoKi__Hybrid__DTFDict as DTFDict
DictTuple = B2DsDsTuple.B.addTupleTool(LoKi__Hybrid__Dict2Tuple, "DTFTuple")
DictTuple.addTool(DTFDict,"DTF")
DictTuple.Source = "LoKi::Hybrid::DTFDict/DTF"
DictTuple.NumVar = 46     # reserve a suitable size for the dictionaire
DictTuple.DTF.constrainToOriginVertex = True
DictTuple.DTF.daughtersToConstrain = ['D_s+', 'D_s-']
DictTuple.DTF.addTool(LoKi__Hybrid__DictOfFunctors,"dict")
DictTuple.DTF.Source = "LoKi::Hybrid::DictOfFunctors/dict"
DictTuple.DTF.dict.Variables = {
    "DTFDict_B_PT"      : "PT",
    "DTFDict_B_M"       : "M",
    'DTFDict_B_PX'                :   "PX",
    'DTFDict_B_PY'                :   "PY",
    'DTFDict_B_PZ'                :   "PZ",
    'DTFDict_B_PT'                :   "PT",
    'DTFDict_B_E'                 :   "E" ,
    "DTFDict_B_MINIPCHI2"         :   "MIPCHI2DV()" ,
    "DTFDict_B_DIRA_OWNPV"        :   "BPVDIRA" ,
    "DTFDict_B_ENDVERTEX_CHI2"    :   "VFASPF(VCHI2)" ,
    "DTFDict_B_ENDVERTEX_NDOF"    :   "VFASPF(VDOF)" ,
    "DTFDict_B_FDCHI2_OWNPV"      :   "BPVVDCHI2" ,
#    # Lc
#    "DTFDict_Lc_MM"              :   "CHILD(MM              ,ABSID=='Lambda_c+')",
#    "DTFDict_Lc_M"               :   "CHILD(M               ,ABSID=='Lambda_c+')",
#    "DTFDict_Lc_PX"              :   "CHILD(PX              ,ABSID=='Lambda_c+')",
#    "DTFDict_Lc_PY"              :   "CHILD(PY              ,ABSID=='Lambda_c+')",
#    "DTFDict_Lc_PZ"              :   "CHILD(PZ              ,ABSID=='Lambda_c+')",
#    "DTFDict_Lc_PT"              :   "CHILD(PT              ,ABSID=='Lambda_c+')",
#    "DTFDict_Lc_E"               :   "CHILD(E               ,ABSID=='Lambda_c+')",
#    "DTFDict_Lc_MINIPCHI2"       :   "CHILD(MIPCHI2DV()     ,ABSID=='Lambda_c+')",
#    "DTFDict_Lc_DIRA_OWNPV"      :   "CHILD(BPVDIRA         ,ABSID=='Lambda_c+')",
#    "DTFDict_Lc_ENDVERTEX_CHI2"  :   "CHILD(VFASPF(VCHI2)   ,ABSID=='Lambda_c+')",
#    "DTFDict_Lc_ENDVERTEX_NDOF"  :   "CHILD(VFASPF(VDOF)    ,ABSID=='Lambda_c+')",
#    "DTFDict_Lc_FDCHI2_OWNPV"    :   "CHILD(BPVVDCHI2       ,ABSID=='Lambda_c+')",
    # D_s+
    "DTFDict_Dplus_MM"              :   "CHILD(MM              ,ABSID=='D_s+')",
    "DTFDict_Dplus_M"               :   "CHILD(M               ,ABSID=='D_s+')",
    "DTFDict_Dplus_PX"              :   "CHILD(PX              ,ABSID=='D_s+')",
    "DTFDict_Dplus_PY"              :   "CHILD(PY              ,ABSID=='D_s+')",
    "DTFDict_Dplus_PZ"              :   "CHILD(PZ              ,ABSID=='D_s+')",
    "DTFDict_Dplus_PT"              :   "CHILD(PT              ,ABSID=='D_s+')",
    "DTFDict_Dplus_E"               :   "CHILD(E               ,ABSID=='D_s+')",
    "DTFDict_Dplus_MINIPCHI2"       :   "CHILD(MIPCHI2DV()     ,ABSID=='D_s+')",
    "DTFDict_Dplus_DIRA_OWNPV"      :   "CHILD(BPVDIRA         ,ABSID=='D_s+')",
    "DTFDict_Dplus_ENDVERTEX_CHI2"  :   "CHILD(VFASPF(VCHI2)   ,ABSID=='D_s+')",
    "DTFDict_Dplus_ENDVERTEX_NDOF"  :   "CHILD(VFASPF(VDOF)    ,ABSID=='D_s+')",
    "DTFDict_Dplus_FDCHI2_OWNPV"    :   "CHILD(BPVVDCHI2       ,ABSID=='D_s+')",
    # Ds-
    "DTFDict_Dsminus_MM"              :   "CHILD(MM              ,ABSID=='D_s-')",
    "DTFDict_Dsminus_M"               :   "CHILD(M               ,ABSID=='D_s-')",
    "DTFDict_Dsminus_PX"              :   "CHILD(PX              ,ABSID=='D_s-')",
    "DTFDict_Dsminus_PY"              :   "CHILD(PY              ,ABSID=='D_s-')",
    "DTFDict_Dsminus_PZ"              :   "CHILD(PZ              ,ABSID=='D_s-')",
    "DTFDict_Dsminus_PT"              :   "CHILD(PT              ,ABSID=='D_s-')",
    "DTFDict_Dsminus_E"               :   "CHILD(E               ,ABSID=='D_s-')",
    "DTFDict_Dsminus_MINIPCHI2"       :   "CHILD(MIPCHI2DV()     ,ABSID=='D_s-')",
    "DTFDict_Dsminus_DIRA_OWNPV"      :   "CHILD(BPVDIRA         ,ABSID=='D_s-')",
    "DTFDict_Dsminus_ENDVERTEX_CHI2"  :   "CHILD(VFASPF(VCHI2)   ,ABSID=='D_s-')",
    "DTFDict_Dsminus_ENDVERTEX_NDOF"  :   "CHILD(VFASPF(VDOF)    ,ABSID=='D_s-')",
    "DTFDict_Dsminus_FDCHI2_OWNPV"    :   "CHILD(BPVVDCHI2       ,ABSID=='D_s-')",

   }

#

B2DsDsTuple.ToolList += [ "TupleToolAngles"
                    , "TupleToolEventInfo"
                    , "TupleToolGeometry"
                    , "TupleToolKinematic"
                    , "TupleToolPid"
                    , "TupleToolPrimaries"
                    , "TupleToolPropertime"
                    , "TupleToolTrackInfo"
                    # , "TupleToolCovariances"
                    , "TupleToolBremInfo"
                    , "TupleToolMuonPid"
                    , "TupleToolRecoStats"
                    , "TupleToolDira"
                    , "TupleToolTrackPosition"
                    , "TupleToolL0Data"
                    , "TupleToolMCTruth"
                    , "TupleToolMCBackgroundInfo"
                    ]


B2DsDsTuple.ToolList+= [ "TupleToolTISTOS" ]
B2DsDsTuple.addTool(TupleToolTISTOS, name = "TupleToolTISTOS")
B2DsDsTuple.TupleToolTISTOS.Verbose = True
B2DsDsTuple.TupleToolTISTOS.VerboseL0 = True
B2DsDsTuple.TupleToolTISTOS.VerboseHlt1 = True
B2DsDsTuple.TupleToolTISTOS.VerboseHlt2 = True
B2DsDsTuple.TupleToolTISTOS.TriggerList = myTriggerList #trigger_list.trigger_list() 


from Configurables import TupleToolRecoStats
B2DsDsTuple.addTool(TupleToolRecoStats, name = "TupleToolRecoStats")
B2DsDsTuple.TupleToolRecoStats.Verbose = True


# Add eta                                                            
LoKi_All = B2DsDsTuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_All")
LoKi_All.Variables = {
    'ETA' : 'ETA',
    'MinIPCHI2' : 'MIPCHI2DV(PRIMARY)'
    }


## MCTruth info
MCTruth = TupleToolMCTruth()
MCTruth.ToolList = [
    "MCTupleToolAngles"
        ,"MCTupleToolHierarchy"
            ,"MCTupleToolKinematic" ]


if doMC:
    B2DsDsTuple.addTool(MCTruth, name = "TupleToolMCTruth")





############
from DecayTreeTuple.Configuration import *
from Configurables import LoKi__Hybrid__TupleTool

#######--------------------B + -> XcXc
B2XcXcTuple = DecayTreeTuple("B2XcXcTuple")
B2XcXcTuple.Inputs = [rootInTES+"/Phys/CC2DDLine/Particles"]
B2XcXcTuple.Decay = "psi(3770) -> ^(Lambda_c+ -> ^p+ ^K- ^pi+) ^(Lambda_c~- -> ^p~- ^K+ ^pi-)"
B2XcXcTuple.addBranches({
    "B"                 :    "psi(3770) -> (Lambda_c+ -> p+ K- pi+) (Lambda_c~- -> p~- K+ pi-)",
    "Xc_plus"           :    "psi(3770) -> ^(Lambda_c+ -> p+ K- pi+) (Lambda_c~- -> p~- K+ pi-)",
    "p_Xc_plus"         :    "psi(3770) -> (Lambda_c+ -> ^p+ K- pi+) (Lambda_c~- -> p~- K+ pi-)",
    "Kminus_Xc_plus"    :    "psi(3770) -> (Lambda_c+ -> p+ ^K- pi+) (Lambda_c~- -> p~- K+ pi-)",
    "piplus_Xc_plus"    :    "psi(3770) -> (Lambda_c+ -> p+ K- ^pi+) (Lambda_c~- -> p~- K+ pi-)",
    "Xc_minus"          :    "psi(3770) -> (Lambda_c+ -> p+ K- pi+) ^(Lambda_c~- -> p~- K+ pi-)",
    "pbar_Xc_minus"     :    "psi(3770) -> (Lambda_c+ -> p+ K- pi+) (Lambda_c~- -> ^p~- K+ pi-)",
    "Kplus_Xc_minus"    :    "psi(3770) -> (Lambda_c+ -> p+ K- pi+) (Lambda_c~- -> p~- ^K+ pi-)",
    "piminus_Xc_minus"  :    "psi(3770) -> (Lambda_c+ -> p+ K- pi+) (Lambda_c~- -> p~- K+ ^pi-)",
})

## Reference point
B2XcXcTuple.ToolList += [ "TupleToolKinematic" ]
B2XcXcTuple.addTool(TupleToolKinematic, name = "TupleToolKinematic")
B2XcXcTuple.TupleToolKinematic.Verbose = True

###################################
# Lc Constraints:
###################################
## B Lambda_c~- Lambda_c+ (mass + PV)constraint Tool
B2XcXcTuple.addTool(TupleToolDecay, name = "DTFM_PVC")
B2XcXcTuple_DTFM_PVC = B2XcXcTuple.B.addTupleTool('TupleToolDecayTreeFitter/DTFM_PVC')
B2XcXcTuple_DTFM_PVC.UpdateDaughters = True
B2XcXcTuple_DTFM_PVC.Verbose = True
B2XcXcTuple_DTFM_PVC.constrainToOriginVertex = True
B2XcXcTuple_DTFM_PVC.daughtersToConstrain += [ 'Lambda_c+']
B2XcXcTuple_DTFM_PVC.daughtersToConstrain += [ 'Lambda_c~-']

## B Lambda_c~- Lambda_c+ (mass)constraint Tool
B2XcXcTuple.addTool(TupleToolDecay, name = "DTFM")
B2XcXcTuple_DTFM = B2XcXcTuple.B.addTupleTool('TupleToolDecayTreeFitter/DTFM')
B2XcXcTuple_DTFM.UpdateDaughters = True
B2XcXcTuple_DTFM.Verbose = True
B2XcXcTuple_DTFM.constrainToOriginVertex = False
B2XcXcTuple_DTFM.daughtersToConstrain += [ 'Lambda_c+']
B2XcXcTuple_DTFM.daughtersToConstrain += [ 'Lambda_c~-']
#
## B (PV)constraint Tool
B2XcXcTuple.addTool(TupleToolDecay, name = "PVC")
B2XcXcTuple_PVC = B2XcXcTuple.B.addTupleTool('TupleToolDecayTreeFitter/PVC')
B2XcXcTuple_PVC.UpdateDaughters = True
B2XcXcTuple_PVC.Verbose = True
B2XcXcTuple_PVC.constrainToOriginVertex = True

###########################
##DTFDict B2DsD
###########################
from Configurables import LoKi__Hybrid__DictOfFunctors
from Configurables import LoKi__Hybrid__Dict2Tuple
from Configurables import LoKi__Hybrid__DTFDict as DTFDict
DictTuple = B2XcXcTuple.B.addTupleTool(LoKi__Hybrid__Dict2Tuple, "DTFTuple")
DictTuple.addTool(DTFDict,"DTF")
DictTuple.Source = "LoKi::Hybrid::DTFDict/DTF"
DictTuple.NumVar = 46     # reserve a suitable size for the dictionaire
DictTuple.DTF.constrainToOriginVertex = True
DictTuple.DTF.daughtersToConstrain = ['Lambda_c+','Lambda_c~-']
DictTuple.DTF.addTool(LoKi__Hybrid__DictOfFunctors,"dict")
DictTuple.DTF.Source = "LoKi::Hybrid::DictOfFunctors/dict"
DictTuple.DTF.dict.Variables = {
    "DTFDict_B_PT"      : "PT",
    "DTFDict_B_M"       : "M",
    'DTFDict_B_PX'                :   "PX",
    'DTFDict_B_PY'                :   "PY",
    'DTFDict_B_PZ'                :   "PZ",
    'DTFDict_B_PT'                :   "PT",            
    'DTFDict_B_E'                 :   "E" ,
    "DTFDict_B_MINIPCHI2"         :   "MIPCHI2DV()" ,
    "DTFDict_B_DIRA_OWNPV"        :   "BPVDIRA" ,
    "DTFDict_B_ENDVERTEX_CHI2"    :   "VFASPF(VCHI2)" ,
    "DTFDict_B_ENDVERTEX_NDOF"    :   "VFASPF(VDOF)" ,
    "DTFDict_B_FDCHI2_OWNPV"      :   "BPVVDCHI2" ,
    # D+
    "DTFDict_Xc_plus_MM"              :   "CHILD(MM              ,ABSID=='Lambda_c+')",
    "DTFDict_Xc_plus_M"               :   "CHILD(M               ,ABSID=='Lambda_c+')",
    "DTFDict_Xc_plus_PX"              :   "CHILD(PX              ,ABSID=='Lambda_c+')",
    "DTFDict_Xc_plus_PY"              :   "CHILD(PY              ,ABSID=='Lambda_c+')",
    "DTFDict_Xc_plus_PZ"              :   "CHILD(PZ              ,ABSID=='Lambda_c+')",
    "DTFDict_Xc_plus_PT"              :   "CHILD(PT              ,ABSID=='Lambda_c+')",
    "DTFDict_Xc_plus_E"               :   "CHILD(E               ,ABSID=='Lambda_c+')", 
    "DTFDict_Xc_plus_MINIPCHI2"       :   "CHILD(MIPCHI2DV()     ,ABSID=='Lambda_c+')",
    "DTFDict_Xc_plus_DIRA_OWNPV"      :   "CHILD(BPVDIRA         ,ABSID=='Lambda_c+')",
    "DTFDict_Xc_plus_ENDVERTEX_CHI2"  :   "CHILD(VFASPF(VCHI2)   ,ABSID=='Lambda_c+')",
    "DTFDict_Xc_plus_ENDVERTEX_NDOF"  :   "CHILD(VFASPF(VDOF)    ,ABSID=='Lambda_c+')",
    "DTFDict_Xc_plus_FDCHI2_OWNPV"    :   "CHILD(BPVVDCHI2       ,ABSID=='Lambda_c+')",
    # Ds-
    "DTFDict_Xc_minus_MM"              :   "CHILD(MM              ,ABSID=='Lambda_c~-')",
    "DTFDict_Xc_minus_M"               :   "CHILD(M               ,ABSID=='Lambda_c~-')",
    "DTFDict_Xc_minus_PX"              :   "CHILD(PX              ,ABSID=='Lambda_c~-')",
    "DTFDict_Xc_minus_PY"              :   "CHILD(PY              ,ABSID=='Lambda_c~-')",
    "DTFDict_Xc_minus_PZ"              :   "CHILD(PZ              ,ABSID=='Lambda_c~-')",
    "DTFDict_Xc_minus_PT"              :   "CHILD(PT              ,ABSID=='Lambda_c~-')",
    "DTFDict_Xc_minus_E"               :   "CHILD(E               ,ABSID=='Lambda_c~-')",
    "DTFDict_Xc_minus_MINIPCHI2"       :   "CHILD(MIPCHI2DV()     ,ABSID=='Lambda_c~-')",
    "DTFDict_Xc_minus_DIRA_OWNPV"      :   "CHILD(BPVDIRA         ,ABSID=='Lambda_c~-')",
    "DTFDict_Xc_minus_ENDVERTEX_CHI2"  :   "CHILD(VFASPF(VCHI2)   ,ABSID=='Lambda_c~-')",
    "DTFDict_Xc_minus_ENDVERTEX_NDOF"  :   "CHILD(VFASPF(VDOF)    ,ABSID=='Lambda_c~-')",
    "DTFDict_Xc_minus_FDCHI2_OWNPV"    :   "CHILD(BPVVDCHI2       ,ABSID=='Lambda_c~-')",
   }

B2XcXcTuple.ToolList += [ "TupleToolAngles"
                    , "TupleToolEventInfo"
                    , "TupleToolGeometry"
                    , "TupleToolKinematic"
                    , "TupleToolPid"
                    , "TupleToolPrimaries"
                    , "TupleToolPropertime"
                    , "TupleToolTrackInfo"
                    # , "TupleToolCovariances"
                    , "TupleToolBremInfo"
                    , "TupleToolMuonPid"
                    , "TupleToolRecoStats"
                    , "TupleToolDira"
                    , "TupleToolTrackPosition"
                    , "TupleToolL0Data"
                    , "TupleToolMCTruth"
                    , "TupleToolMCBackgroundInfo"
                    ]


B2XcXcTuple.ToolList+= [ "TupleToolTISTOS" ]
B2XcXcTuple.addTool(TupleToolTISTOS, name = "TupleToolTISTOS")
B2XcXcTuple.TupleToolTISTOS.Verbose = True
B2XcXcTuple.TupleToolTISTOS.VerboseL0 = True
B2XcXcTuple.TupleToolTISTOS.VerboseHlt1 = True
B2XcXcTuple.TupleToolTISTOS.VerboseHlt2 = True
B2XcXcTuple.TupleToolTISTOS.TriggerList = myTriggerList #trigger_list.trigger_list() 


from Configurables import TupleToolRecoStats
B2XcXcTuple.addTool(TupleToolRecoStats, name = "TupleToolRecoStats")
B2XcXcTuple.TupleToolRecoStats.Verbose = True


# Add eta                                                            
LoKi_All = B2XcXcTuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_All")
LoKi_All.Variables = {
    'ETA' : 'ETA',
    'MinIPCHI2' : 'MIPCHI2DV(PRIMARY)'
    }


## MCTruth info
MCTruth = TupleToolMCTruth()
MCTruth.ToolList = [
    "MCTupleToolAngles"
        ,"MCTupleToolHierarchy"
            ,"MCTupleToolKinematic" ]


if doMC:
    B2XcXcTuple.addTool(MCTruth, name = "TupleToolMCTruth")



############
from DecayTreeTuple.Configuration import *
from Configurables import LoKi__Hybrid__TupleTool

#######--------------------B + -> XcXc
B2XcXc_SSTuple = DecayTreeTuple("B2XcXc_SSTuple")
B2XcXc_SSTuple.Inputs = [rootInTES+"/Phys/CC2DDLine/Particles"]
B2XcXc_SSTuple.Decay = "psi(3770) -> ^(Lambda_c+ -> ^p+ ^K- ^pi+) ^(Lambda_c+ -> ^p+ ^K- ^pi+)"
B2XcXc_SSTuple.addBranches({
    "B"                 :    "psi(3770) ->  (Lambda_c+ -> p+ K- pi+)   (Lambda_c+ ->  p+  K-  pi+)",
    "Xc_plus"           :    "psi(3770) -> ^(Lambda_c+ -> p+ K- pi+)   (Lambda_c+ ->  p+  K-  pi+)",
    "p_Xc_plus"         :    "psi(3770) ->  (Lambda_c+ -> ^p+ K- pi+)  (Lambda_c+ ->  p+  K-  pi+)",
    "Kminus_Xc_plus"    :    "psi(3770) ->  (Lambda_c+ -> p+ ^K- pi+)  (Lambda_c+ ->  p+  K-  pi+)",
    "piplus_Xc_plus"    :    "psi(3770) ->  (Lambda_c+ -> p+ K- ^pi+)  (Lambda_c+ ->  p+  K-  pi+)",
    "Xc_minus"          :    "psi(3770) ->  (Lambda_c+ -> p+ K- pi+)  ^(Lambda_c+ ->  p+  K-  pi+)",
    "pbar_Xc_minus"     :    "psi(3770) ->  (Lambda_c+ -> p+ K- pi+)   (Lambda_c+ -> ^p+  K-  pi+)",
    "Kplus_Xc_minus"    :    "psi(3770) ->  (Lambda_c+ -> p+ K- pi+)   (Lambda_c+ ->  p+ ^K-  pi+)",
    "piminus_Xc_minus"  :    "psi(3770) ->  (Lambda_c+ -> p+ K- pi+)   (Lambda_c+ ->  p+  K- ^pi+)",
})

## Reference point
B2XcXc_SSTuple.ToolList += [ "TupleToolKinematic" ]
B2XcXc_SSTuple.addTool(TupleToolKinematic, name = "TupleToolKinematic")
B2XcXc_SSTuple.TupleToolKinematic.Verbose = True

###################################
# Lc Constraints:
###################################
## B Lambda_c+ Lambda_c+ (mass + PV)constraint Tool
B2XcXc_SSTuple.addTool(TupleToolDecay, name = "DTFM_PVC")
B2XcXc_SSTuple_DTFM_PVC = B2XcXc_SSTuple.B.addTupleTool('TupleToolDecayTreeFitter/DTFM_PVC')
B2XcXc_SSTuple_DTFM_PVC.UpdateDaughters = True
B2XcXc_SSTuple_DTFM_PVC.Verbose = True
B2XcXc_SSTuple_DTFM_PVC.constrainToOriginVertex = True
B2XcXc_SSTuple_DTFM_PVC.daughtersToConstrain += [ 'Lambda_c+']
B2XcXc_SSTuple_DTFM_PVC.daughtersToConstrain += [ 'Lambda_c+']

## B Lambda_c+ Lambda_c+ (mass)constraint Tool
B2XcXc_SSTuple.addTool(TupleToolDecay, name = "DTFM")
B2XcXc_SSTuple_DTFM = B2XcXc_SSTuple.B.addTupleTool('TupleToolDecayTreeFitter/DTFM')
B2XcXc_SSTuple_DTFM.UpdateDaughters = True
B2XcXc_SSTuple_DTFM.Verbose = True
B2XcXc_SSTuple_DTFM.constrainToOriginVertex = False
B2XcXc_SSTuple_DTFM.daughtersToConstrain += [ 'Lambda_c+']
B2XcXc_SSTuple_DTFM.daughtersToConstrain += [ 'Lambda_c+']
#
## B (PV)constraint Tool
B2XcXc_SSTuple.addTool(TupleToolDecay, name = "PVC")
B2XcXc_SSTuple_PVC = B2XcXc_SSTuple.B.addTupleTool('TupleToolDecayTreeFitter/PVC')
B2XcXc_SSTuple_PVC.UpdateDaughters = True
B2XcXc_SSTuple_PVC.Verbose = True
B2XcXc_SSTuple_PVC.constrainToOriginVertex = True

###########################
##DTFDict B2DsD
###########################
from Configurables import LoKi__Hybrid__DictOfFunctors
from Configurables import LoKi__Hybrid__Dict2Tuple
from Configurables import LoKi__Hybrid__DTFDict as DTFDict
DictTuple = B2XcXc_SSTuple.B.addTupleTool(LoKi__Hybrid__Dict2Tuple, "DTFTuple")
DictTuple.addTool(DTFDict,"DTF")
DictTuple.Source = "LoKi::Hybrid::DTFDict/DTF"
DictTuple.NumVar = 46     # reserve a suitable size for the dictionaire
DictTuple.DTF.constrainToOriginVertex = True
DictTuple.DTF.daughtersToConstrain = ['Lambda_c+','Lambda_c+']
DictTuple.DTF.addTool(LoKi__Hybrid__DictOfFunctors,"dict")
DictTuple.DTF.Source = "LoKi::Hybrid::DictOfFunctors/dict"
DictTuple.DTF.dict.Variables = {
    "DTFDict_B_PT"      : "PT",
    "DTFDict_B_M"       : "M",
    'DTFDict_B_PX'                :   "PX",
    'DTFDict_B_PY'                :   "PY",
    'DTFDict_B_PZ'                :   "PZ",
    'DTFDict_B_PT'                :   "PT",            
    'DTFDict_B_E'                 :   "E" ,
    "DTFDict_B_MINIPCHI2"         :   "MIPCHI2DV()" ,
    "DTFDict_B_DIRA_OWNPV"        :   "BPVDIRA" ,
    "DTFDict_B_ENDVERTEX_CHI2"    :   "VFASPF(VCHI2)" ,
    "DTFDict_B_ENDVERTEX_NDOF"    :   "VFASPF(VDOF)" ,
    "DTFDict_B_FDCHI2_OWNPV"      :   "BPVVDCHI2" ,
    # D+
    "DTFDict_Xc_plus_MM"              :   "CHILD(MM              ,ABSID=='Lambda_c+')",
    "DTFDict_Xc_plus_M"               :   "CHILD(M               ,ABSID=='Lambda_c+')",
    "DTFDict_Xc_plus_PX"              :   "CHILD(PX              ,ABSID=='Lambda_c+')",
    "DTFDict_Xc_plus_PY"              :   "CHILD(PY              ,ABSID=='Lambda_c+')",
    "DTFDict_Xc_plus_PZ"              :   "CHILD(PZ              ,ABSID=='Lambda_c+')",
    "DTFDict_Xc_plus_PT"              :   "CHILD(PT              ,ABSID=='Lambda_c+')",
    "DTFDict_Xc_plus_E"               :   "CHILD(E               ,ABSID=='Lambda_c+')", 
    "DTFDict_Xc_plus_MINIPCHI2"       :   "CHILD(MIPCHI2DV()     ,ABSID=='Lambda_c+')",
    "DTFDict_Xc_plus_DIRA_OWNPV"      :   "CHILD(BPVDIRA         ,ABSID=='Lambda_c+')",
    "DTFDict_Xc_plus_ENDVERTEX_CHI2"  :   "CHILD(VFASPF(VCHI2)   ,ABSID=='Lambda_c+')",
    "DTFDict_Xc_plus_ENDVERTEX_NDOF"  :   "CHILD(VFASPF(VDOF)    ,ABSID=='Lambda_c+')",
    "DTFDict_Xc_plus_FDCHI2_OWNPV"    :   "CHILD(BPVVDCHI2       ,ABSID=='Lambda_c+')",
    # Ds-
    "DTFDict_Xc_minus_MM"              :   "CHILD(MM              ,ABSID=='Lambda_c+')",
    "DTFDict_Xc_minus_M"               :   "CHILD(M               ,ABSID=='Lambda_c+')",
    "DTFDict_Xc_minus_PX"              :   "CHILD(PX              ,ABSID=='Lambda_c+')",
    "DTFDict_Xc_minus_PY"              :   "CHILD(PY              ,ABSID=='Lambda_c+')",
    "DTFDict_Xc_minus_PZ"              :   "CHILD(PZ              ,ABSID=='Lambda_c+')",
    "DTFDict_Xc_minus_PT"              :   "CHILD(PT              ,ABSID=='Lambda_c+')",
    "DTFDict_Xc_minus_E"               :   "CHILD(E               ,ABSID=='Lambda_c+')",
    "DTFDict_Xc_minus_MINIPCHI2"       :   "CHILD(MIPCHI2DV()     ,ABSID=='Lambda_c+')",
    "DTFDict_Xc_minus_DIRA_OWNPV"      :   "CHILD(BPVDIRA         ,ABSID=='Lambda_c+')",
    "DTFDict_Xc_minus_ENDVERTEX_CHI2"  :   "CHILD(VFASPF(VCHI2)   ,ABSID=='Lambda_c+')",
    "DTFDict_Xc_minus_ENDVERTEX_NDOF"  :   "CHILD(VFASPF(VDOF)    ,ABSID=='Lambda_c+')",
    "DTFDict_Xc_minus_FDCHI2_OWNPV"    :   "CHILD(BPVVDCHI2       ,ABSID=='Lambda_c+')",
   }

B2XcXc_SSTuple.ToolList += [ "TupleToolAngles"
                    , "TupleToolEventInfo"
                    , "TupleToolGeometry"
                    , "TupleToolKinematic"
                    , "TupleToolPid"
                    , "TupleToolPrimaries"
                    , "TupleToolPropertime"
                    , "TupleToolTrackInfo"
                    # , "TupleToolCovariances"
                    , "TupleToolBremInfo"
                    , "TupleToolMuonPid"
                    , "TupleToolRecoStats"
                    , "TupleToolDira"
                    , "TupleToolTrackPosition"
                    , "TupleToolL0Data"
                    , "TupleToolMCTruth"
                    , "TupleToolMCBackgroundInfo"
                    ]


B2XcXc_SSTuple.ToolList+= [ "TupleToolTISTOS" ]
B2XcXc_SSTuple.addTool(TupleToolTISTOS, name = "TupleToolTISTOS")
B2XcXc_SSTuple.TupleToolTISTOS.Verbose = True
B2XcXc_SSTuple.TupleToolTISTOS.VerboseL0 = True
B2XcXc_SSTuple.TupleToolTISTOS.VerboseHlt1 = True
B2XcXc_SSTuple.TupleToolTISTOS.VerboseHlt2 = True
B2XcXc_SSTuple.TupleToolTISTOS.TriggerList = myTriggerList #trigger_list.trigger_list() 


from Configurables import TupleToolRecoStats
B2XcXc_SSTuple.addTool(TupleToolRecoStats, name = "TupleToolRecoStats")
B2XcXc_SSTuple.TupleToolRecoStats.Verbose = True


# Add eta                                                            
LoKi_All = B2XcXc_SSTuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_All")
LoKi_All.Variables = {
    'ETA' : 'ETA',
    'MinIPCHI2' : 'MIPCHI2DV(PRIMARY)'
    }


## MCTruth info
MCTruth = TupleToolMCTruth()
MCTruth.ToolList = [
    "MCTupleToolAngles"
        ,"MCTupleToolHierarchy"
            ,"MCTupleToolKinematic" ]


if doMC:
    B2XcXc_SSTuple.addTool(MCTruth, name = "TupleToolMCTruth")



from Configurables import DaVinci, CheckPV, GaudiSequencer, LoKi__HDRFilter

#from Configurables import CondDB
#CondDB ( LatestGlobalTagByDataType = year )


#######################################################################
from Configurables import DaVinci
DaVinci().EvtMax = -1                      # Number of events
DaVinci().PrintFreq = 5000
DaVinci().SkipEvents = 0                       # Events to skip
DaVinci().TupleFile = "Tuple.root"             # NB2DsDTuple
DaVinci().Simulation = doMC
DaVinci().Lumi = not DaVinci().Simulation

DaVinci().UserAlgorithms = [ B2DsDTuple, B2DsDsTuple, B2XcXcTuple, B2XcXc_SSTuple]
DaVinci().DataType = year


if doMC:
    local_dict = {
    }
    DaVinci().InputType = 'DST'
else:
    local_dict = {
        '2011' : ["/eos/lhcb/grid/prod/lhcb/LHCb/Collision11/CHARMCOMPLETEEVENT.DST/00041840/0000/00041840_00009954_1.charmcompleteevent.dst"],
        '2012' : ["/eos/lhcb/grid/prod/lhcb/LHCb/Collision12/CHARMCOMPLETEEVENT.DST/00050929/0000/00050929_00009148_1.charmcompleteevent.dst"],
        '2015' : ["/eos/lhcb/grid/prod/lhcb/LHCb/Collision15/CHARMCOMPLETEEVENT.DST/00069078/0000/00069078_00009785_1.charmcompleteevent.dst"],
        '2016' : ["/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/CHARMCOMPLETEEVENT.DST/00069603/0000/00069603_00009880_1.charmcompleteevent.dst"],
        '2017' : ["/eos/lhcb/grid/prod/lhcb/LHCb/Collision17/CHARMCOMPLETEEVENT.DST/00071671/0000/00071671_00009449_1.charmcompleteevent.dst"],
        '2018' : ["/eos/lhcb/grid/prod/lhcb/LHCb/Collision18/CHARMCOMPLETEEVENT.DST/00077434/0000/00077434_00005721_1.charmcompleteevent.dst"],
    }
    DaVinci().InputType = 'DST'

###------------------Local Test
if doLocalTest:
    DaVinci().Input = local_dict[year]

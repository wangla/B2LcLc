#include <iostream>
#include "RooStats/ModelConfig.h"
#include "RooStats/ProfileLikelihoodCalculator.h"
#include "RooStats/HypoTestResult.h"
#include <RooDataSet.h>

#include "/mnt/d/lhcb/B2XcXc/inc/tools.h"
#include "/mnt/d/lhcb/B2XcXc/inc/RooDoubleCB.h"
#include "/mnt/d/lhcb/B2XcXc/inc/fitMCtoGetShape.h"
#include "/mnt/d/lhcb/B2XcXc/inc/getPresel.h"

using namespace RooFit;
using namespace RooStats;

#include <iostream>
#include "RooStats/ModelConfig.h"
#include "RooStats/ProfileLikelihoodCalculator.h"
#include "RooStats/HypoTestResult.h"
#include <RooDataSet.h>

#include "/mnt/d/lhcb/B2XcXc/inc/tools.h"
#include "/mnt/d/lhcb/B2XcXc/inc/RooDoubleCB.h"
#include "/mnt/d/lhcb/B2XcXc/inc/fitMCtoGetShape.h"
#include "/mnt/d/lhcb/B2XcXc/inc/getPresel.h"

using namespace RooFit;
using namespace RooStats;

void fit(TString cutString )
{
	lhcbStyle(0.063);
    //---------Build background PDF------
    
    cout<< "define the expBkg"<<endl;
    
	vector<int> years;
	//years.push_back(2015);
	//years.push_back(2016);
	//years.push_back(1516);
	//years.push_back(2017);
	//years.push_back(2018);
	years.push_back(2);

	int w(7);
	system("mkdir -p /mnt/d/lhcb/B2XcXc/public_html/fitParamTables/sPlot/");

	TString fTableData = "/mnt/d/lhcb/B2XcXc/public_html/fitParamTables/sPlot/data_Bs2DsDs.dat";

	TString fTableData1 = "/mnt/d/lhcb/B2XcXc/public_html/fitParamTables/sPlot/data_Bs2DsDs_yields.dat";
	TString fTableData2 = "/mnt/d/lhcb/B2XcXc/public_html/fitParamTables/sPlot/data_Bs2DsDs_meanSigma.dat";
	TString fTableData3 = "/mnt/d/lhcb/B2XcXc/public_html/fitParamTables/sPlot/data_Bs2DsDs_bkgParams.dat";
	ofstream out_data_yields(fTableData1.Data());
	ofstream out_data_meanSigma(fTableData2.Data());
	ofstream out_data_bkgParams(fTableData3.Data());

	TString fTableMC = "/mnt/d/lhcb/B2XcXc/public_html/fitParamTables/sPlot/MC_Bs2DsDs.dat";
	ofstream out_MC(fTableMC.Data());

	system("mkdir -p /mnt/d/lhcb/B2XcXc/Documents/yields/sPlot/");
	TString sYields = "/mnt/d/lhcb/B2XcXc/Documents/yields/sPlot/yields_Bs2DsDs.root";

    ////********* 11 12, 3.0fb-1
    ////15, 16, 17, 18,  5.8fb-1
    double lumi1[2] = {1.0, 2.0};
    double lumi2[4] = {0.3, 1.6, 1.7, 2.2};
    for(int i=0; i<years.size(); i++){
        TString yearStr = "Run2";
        if(years[i] ==2) yearStr = "Run"+i2s(years[i]);
        else yearStr = i2s(years[i]);


        TString yearCut = "(1)";
        if(years[i] == 1112) yearCut = "(year == 2011 || year==2012)";
        else if(years[i] == 2) yearCut = "(year == 2015 || year==2016 || year == 2017 || year==2018)";
        else if(years[i] == 1516) yearCut = "(year == 2015 || year==2016)";
        else yearCut = "(year == " +i2s(years[i])+")";

    
    
		TString fitVar = "B_DTFM_M_0";
    	RooRealVar m(fitVar, "#it{m}_{DsDs}", 5200, 5550);
		double binWidth = 5.5;
		int nBins = (int)(m.getMax() - m.getMin())/binWidth;
		m.setBins(nBins);

    	RooRealVar tau("tau", "tau", -0.008, -0.2, 1e-5); 
    	RooExponential expBkg("expBkg","background model",m,tau);
    	
    	int nentries;
    	TChain chBs;
    	chBs.Add("/mnt/d/lhcb/B2XcXc/root/Bs/data/Data_Bs2DsDs_Run2_TriggerSel_PreSel.root/DecayTree");
    	
    	cout<<"read data file"<<endl;
    	
    	////////////////////// Get Bd_misID pdf ////////////////////////////
    	
    	TChain chBd_misIDBkg;
    	chBd_misIDBkg.Add("/mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bd2DsD_misID_Run2_TrigSel_Presel.root/DecayTree");
    	
    	TString bkgcut;
    	bkgcut = fitVar+">"+d2s(m.getMin())+" && "+fitVar+"<"+d2s(m.getMax())+" && "+cutString;
    	//bkgcut = fitVar+">"+d2s(m.getMin())+" && "+fitVar+"<"+d2s(m.getMax());
		//bkgcut += "&&" + yearCut;

    	
		TTree* misIDTree = chBd_misIDBkg.CopyTree(bkgcut);
		RooDataSet *misID_DataSet = new RooDataSet("Bd2DsDData", "Bd2DsDData", misIDTree, m);

		delete misIDTree;
		misIDTree = NULL;

		RooKeysPdf Bd_misID_shape( "Bd_misID_shape", "Bd_misID_shape", m, *misID_DataSet, RooKeysPdf::MirrorBoth, 1.0);

    	
    	cout<<"Get Bd_misID pdf"<<endl;
    	
    	TString datacut = fitVar+">"+d2s(m.getMin())+" && "+fitVar+"<"+d2s(m.getMax())+" && "+cutString;
		TString datacutNoYear = datacut;
		datacut += "&&" + yearCut;


    	TTree *datatree = chBs.CopyTree(datacut);

		datatree->SetBranchStatus("*", 0);
		datatree->SetBranchStatus(fitVar, 1);
		setBranchStatusTTF(datatree, datacut);

    	RooDataSet *datars = new RooDataSet("signal shape", "sigshape", datatree, m);

		//delete datatree;
		//datatree=NULL;
    	
    	TPaveText text(0.15, 0.85, 0.35, 0.95, "NDC");
    	text.SetFillColor(0);
    	text.SetFillStyle(0);
    	text.SetLineColor(0);
    	text.SetLineWidth(0);
    	text.SetBorderSize(1);
    	text.SetTextSize(2*0.05);

		text.AddText(yearStr);

		system("mkdir -p /mnt/d/lhcb/B2XcXc/public_html/fit/sPlot/Bs2DsDs/");


        // try Ipatia
        RooRealVar alphalBs("alphalBs","#alpha_{1}", 3.5, 0.01, 5.);
        RooRealVar alpharBs("alpharBs","#alpha_{2}", 2.8, 0.1, 5.);
        RooRealVar nlBs("nlBs","n_{1}", 1.5, 0., 10);
        RooRealVar nrBs("nrBs","n_{2}", 2.3, 0., 10);
        //RooRealVar sigmaBs("sigmaBs","#sigmaBs", 8, 5, 12);
        RooRealVar sigmaBs("sigmaBs","#sigmaBs", 8, 5, 18);
        RooRealVar meanBs("meanBs","#mu", 5368, 5363,5373);
        RooRealVar beta_Bs("beta_Bs","#beta_Bs", 0);
        RooRealVar zeta_Bs("zeta_Bs","#zeta_Bs", 1e-5);
        RooRealVar l_Bs("l_Bs", "l_Bs", -3, -10, 0.);
        RooIpatia2 modelBs("ipatia", "ipatia", m, l_Bs, zeta_Bs, beta_Bs, sigmaBs, meanBs, alphalBs, nlBs, alpharBs, nrBs);


		//// B_M no right tail, try signal CB or with gaus.
		//RooRealVar alphalBs("alphalBs","#alpha_{1}", 1.0, 1, 5.);
		//RooRealVar nlBs("nlBs","n_{1}", 2.5, 0.1, 5);
		//RooRealVar sigmaBs("sigmaBs","#sigmaBs", 11, 8, 15);
		//RooRealVar meanBs("meanBs","#mu", 5368, 5363, 5373);

		//RooRealVar fSigmaBs("fSigmaBs","fSigmaBs", 1.7, 1, 3);
		//RooFormulaVar sigma_gausCB_Bs("sigma_gausCB_Bs", "sigma_gausCB_Bs", "@0*@1", RooArgList(sigmaBs, fSigmaBs));

		//RooGaussian gausCB_Bs("gausCB_Bs", "gausCB_Bs", m, meanBs, sigma_gausCB_Bs);

		//RooCBShape cbBs("cbBs", "cbBs", m, meanBs, sigmaBs, alphalBs, nlBs);

		//RooRealVar fCB_Bs("fCB_Bs", "f_{CB}", 0.9, 0.5, 0.9999);
		//RooAddPdf modelBs("modelBs", "modelBs", RooArgList(cbBs, gausCB_Bs), RooArgList(fCB_Bs));



		//fitMCBMass_addPdf("/mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bs2DsDs_Run2_truth_PIDcor_TrigSel_Presel.root", fitVar, datacut+"&& rdmNumber<0.5", "1", m.getMin(), m.getMax(), binWidth,
		//	modelBs, RooArgList(cbBs, gausCB_Bs),
		fitMCBMass("/mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bs2DsDs_Run2_truth_PIDcor_TrigSel_Presel.root", fitVar,  datacut+"&& rdmNumber<0.5", "1", m.getMin(), m.getMax(), binWidth,
			modelBs, 5250, 5450,
			fitVar, text, "/mnt/d/lhcb/B2XcXc/public_html/fit/sPlot/Bs2DsDs/MC_Bs2DsDs_"+yearStr+".pdf", false, false);

		// IPatia
		alphalBs.setConstant(true);
		alpharBs.setConstant(true);
		nlBs.setConstant(true);
		nrBs.setConstant(true);
		sigmaBs.setConstant(true);
		//meanBs.setConstant(true);
		beta_Bs.setConstant(true);
		zeta_Bs.setConstant(true);
		l_Bs.setConstant(true);


			/*
		// single cb
		alphalBs.setConstant(true);
		nlBs.setConstant(true);
		//sigmaBs.setConstant(true);
		//meanBs.setConstant(true);
		fSigmaBs.setConstant(true);
		fCB_Bs.setConstant(true);
			*/

 
		out_MC<<setw(w)<<yearStr;
		out_MC<<" & "<<setw(w)<<"$"<<dbl2str(meanBs.getVal(),2)<<"\\pm"<<dbl2str(meanBs.getError(),2)<<"$";
		out_MC<<" & "<<setw(w)<<"$"<<dbl2str(sigmaBs.getVal(),2)<<"\\pm"<<dbl2str(sigmaBs.getError(),2)<<"$";
		out_MC<<" & "<<setw(w)<<"$"<<dbl2str(nlBs.getVal(),2)<<"\\pm"<<dbl2str(nlBs.getError(),2)<<"$";
		out_MC<<" & "<<setw(w)<<"$"<<dbl2str(nrBs.getVal(),2)<<"\\pm"<<dbl2str(nrBs.getError(),2)<<"$";
		out_MC<<" & "<<setw(w)<<"$"<<dbl2str(alphalBs.getVal(),2)<<"\\pm"<<dbl2str(alphalBs.getError(),2)<<"$";
		out_MC<<" & "<<setw(w)<<"$"<<dbl2str(alpharBs.getVal(),2)<<"\\pm"<<dbl2str(alpharBs.getError(),2)<<"$";
		//out_MC<<" & "<<setw(w)<<"$"<<dbl2str(fSigmaBs.getVal(),2)<<"\\pm"<<dbl2str(fSigmaBs.getError(),2)<<"$";
		//out_MC<<" & "<<setw(w)<<"$"<<dbl2str(fCB_Bs.getVal(),2)<<"\\pm"<<dbl2str(fCB_Bs.getError(),2)<<"$";

		out_MC<<" & "<<setw(w)<<"$"<<dbl2str(l_Bs.getVal(),2)<<"\\pm"<<dbl2str(l_Bs.getError(),2)<<"$";
		//out_MC<<" & "<<setw(w)<<"$"<<dbl2str(zeta_Bs.getVal(),2)<<"\\pm"<<dbl2str(zeta_Bs.getError(),2)<<"$";
		//out_MC<<" & "<<setw(w)<<"$"<<dbl2str(10*beta_Bs.getVal(),2)<<"\\pm"<<dbl2str(10*beta_Bs.getError(),2)<<"$";
		out_MC<<"\\\\"<<endl;





        RooRealVar fSigmaDataMC("fSigmaDataMC", "fSigmaDataMC", 1.1, 0.7, 2.0);

        ////RooFormulaVar sigma1BdData("sigma1BdData", "sigma1BdData", "@0*@1", RooArgList(sigma1Bd, fSigmaDataMC));
        ////RooFormulaVar sigma2BdData("sigma2BdData", "sigma2BdData", "@0*@1", RooArgList(sigma2Bd, fSigmaDataMC));
        ////RooCBShape cb1BdData("cb1BdData", "cb1BdData", m, meanBd, sigma1BdData, alphalBd, nlBd);
        ////RooCBShape cb2BdData("cb2BdData", "cb2BdData", m, meanBd, sigma2BdData, alpharBd, nrBd);
        ////RooAddPdf modelBdData("modelBdData", "modelBdData", RooArgList(cb1BdData, cb2BdData), RooArgList(fracCB1_Bd));
        
        RooFormulaVar sigmaBsData("sigmaBsData", "sigmaBsData", "@0*@1", RooArgList(sigmaBs, fSigmaDataMC));

        
		RooRealVar sigmaNonDD("sigmaNonDD", "sigmaNonDD", 18.7);
		RooGaussian modelNonDD("modelNonDD", "modelNonDD", m, meanBs, sigmaNonDD);


        RooIpatia2 modelBsData("modelBsData", "modelBsData", m, l_Bs, zeta_Bs, beta_Bs, sigmaBsData, meanBs, alphalBs, nlBs, alpharBs, nrBs);
        //





        RooRealVar m0("m0","m0", 5300, 5280, 5320);
        RooRealVar c("c","c", 10, -20, 50);
        RooArgusBG argus_noSmearing("prcbkg","rec background model", m, m0, c);

        // Detector response function
        RooRealVar m0_g("m0_g", "m0_g", 0);
        //RooFormulaVar sigmaGaus("sigmaGaus", "sigmaGaus", "@0*@2+@1*(1-@2)", RooArgList(sigma1Bs, sigma2Bs, fracCB1_Bs));
        RooFormulaVar sigmaGaus("sigmaGaus", "sigmaGaus", "@0", RooArgList(sigmaBsData));
        RooGaussian gauss("gauss", "gauss", m, m0_g, sigmaGaus);

        RooFFTConvPdf argusBkg("smearedArgus","argusBkg (X) gauss", m, argus_noSmearing, gauss);
        argusBkg.setBufferFraction(0.35);






		ValError  nBd_misID;
		//ValError nmisIDMCRun1 = getEntriesWithError("/mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bd2DsD_misID_Run1_TrigSel_Presel.root", datacutNoYear, "1");
		
		//ValError  nBdDsDRun1MC = getEntriesWithError("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2DsD_Run1_truth_PIDcor_TrigSel_Presel.root", datacutNoYear, "1");
		
		//ValError nYieldBd2DsDRun1 = readVEFromFile("/mnt/d/lhcb/B2XcXc/Documents/yields/final_fit/yields_Bd2DsD.root", "yields")[2];
		
		//if(years[i] == 1112) nBd_misID = nYieldBd2DsDRun1*(nmisIDMCRun1/nBdDsDRun1MC);
		//else if(years[i] == 2011 || years[i] == 2012) nBd_misID = nYieldBd2DsDRun1*(nmisIDMCRun1/nBdDsDRun1MC)*ValError(lumi1[years[i]-2011], 0)/ValError(3.0, 0);
		
		
		
		
		ValError nmisIDMCRun2 = getEntriesWithError("/mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bd2DsD_misID_Run2_TrigSel_Presel.root", datacutNoYear, "1");
		
		ValError  nBdDsDRun2MC = getEntriesWithError("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2DsD_Run2_truth_PIDcor_TrigSel_Presel.root", datacutNoYear, "1");
		
		ValError nYieldBd2DsDRun2 = readVEFromFile("/mnt/d/lhcb/B2XcXc/Documents/yields/final_fit/yields_Bd2DsD.root", "yields")[8];
		
		if(years[i] == 2) nBd_misID = nYieldBd2DsDRun2*((nmisIDMCRun2/10768783.)/(nBdDsDRun2MC/3543927.));
		else if(years[i] == 1516) nBd_misID = nYieldBd2DsDRun2*((nmisIDMCRun2/10768783.)/(nBdDsDRun2MC/3543927.))*ValError(1.9, 0)/ValError(5.8, 0);
		else if(years[i]>=2015 && years[i]<=2018) nBd_misID = nYieldBd2DsDRun2*((nmisIDMCRun2/10768783.)/(nBdDsDRun2MC/3543927.))*ValError(lumi2[years[i]-2015], 0)/ValError(5.8, 0);



	 	RooRealVar *nNonDD = new RooRealVar("nNonDD", "nNonDD", 0);
        if(years[i] == 2) nNonDD->setVal(362.364);
        if(years[i] == 1516) nNonDD->setVal( 362.364 * 1.9/5.8);
        else if(years[i]>= 2015 && years[i]<= 2018) nNonDD->setVal( 362.364 * lumi2[years[i]-2015]/5.8);
        else if(years[i] == 1112) nNonDD->setVal(0);
        else if(years[i]>= 2011 && years[i]<= 2012) nNonDD->setVal( 0 * lumi1[years[i]-2011]/3.0);


 	
    	RooRealVar *nBs = new RooRealVar("nBs","Number of signal Bs events", 2800, 0., 5e5);
		RooRealVar *nBdBkg  = new RooRealVar("nBdBkg","Number of signal events", nBd_misID.val, nBd_misID.val- 1.0*nBd_misID.err, nBd_misID.val+ 1.0*nBd_misID.err);
    	RooRealVar *nExpBkg = new RooRealVar("nExpBkg","Number of expBkg events", 500, 0., 10000);
    	RooRealVar *nArgusBkg = new RooRealVar("nArgusBkg","Number of Rec expBkg events", 2800, 0., 10000);
   
    	RooAddPdf* totPDF = new RooAddPdf("totPDF","Total pdf", RooArgList(modelBsData, modelNonDD, Bd_misID_shape, expBkg, argusBkg), RooArgList(*nBs, *nNonDD, *nBdBkg, *nExpBkg, *nArgusBkg));
    	
    	//////////////////// Fit the data/////////////
    	RooFitResult *result = totPDF->fitTo(*datars, Save(true), NumCPU(10));


    	cout<<"all ready"<<endl;

		out_data_yields<<setw(w)<<yearStr;
		out_data_yields<<" & "<<setw(w)<<"$"<<dbl2str(nBs->getVal(),1)<<"\\pm"<<dbl2str(nBs->getError(),1)<<"$";
		out_data_yields<<" & "<<setw(w)<<"$"<<dbl2str(nNonDD->getVal(),1)<<"\\pm"<<dbl2str(nNonDD->getError(),1)<<"$";
		//out_data_yields<<" & "<<setw(w)<<"$"<<dbl2str(100*fracBd->getVal(),3)<<"\\pm"<<dbl2str(100*fracBd->getError(),3)<<"$";
		out_data_yields<<" & "<<setw(w)<<"$"<<dbl2str(nBdBkg->getVal(),3)<<"\\pm"<<dbl2str(nBdBkg->getError(),3)<<"$";
		out_data_yields<<" & "<<setw(w)<<"$"<<dbl2str(nExpBkg->getVal(),1)<<"\\pm"<<dbl2str(nExpBkg->getError(),1)<<"$";
		out_data_yields<<" & "<<setw(w)<<"$"<<dbl2str(nArgusBkg->getVal(),1)<<"\\pm"<<dbl2str(nArgusBkg->getError(),1)<<"$";
		out_data_yields<<"\\\\"<<endl;
		out_data_meanSigma<<setw(w)<<yearStr;
		out_data_meanSigma<<" & "<<setw(w)<<"$"<<dbl2str(meanBs.getVal(),1)<<"\\pm"<<dbl2str(meanBs.getError(),1)<<"$";
		out_data_meanSigma<<" & "<<setw(w)<<"$"<<dbl2str(sigmaNonDD.getVal(),1)<<"\\pm"<<dbl2str(sigmaNonDD.getError(),1)<<"$";
		out_data_meanSigma<<" & "<<setw(w)<<"$"<<dbl2str(fSigmaDataMC.getVal(),1)<<"\\pm"<<dbl2str(fSigmaDataMC.getError(),1)<<"$";
		out_data_meanSigma<<"\\\\"<<endl;
		out_data_bkgParams<<setw(w)<<yearStr;
		out_data_bkgParams<<" & "<<setw(w)<<"$"<<dbl2str(tau.getVal()*1e2,1)<<"\\pm"<<dbl2str(tau.getError()*1e2,1)<<"$";
		out_data_bkgParams<<" & "<<setw(w)<<"$"<<dbl2str(m0.getVal(),1)<<"\\pm"<<dbl2str(m0.getError(),1)<<"$";
		out_data_bkgParams<<" & "<<setw(w)<<"$"<<dbl2str(c.getVal(),1)<<"\\pm"<<dbl2str(c.getError(),1)<<"$";
		out_data_bkgParams<<"\\\\"<<endl;



		TFile fYields(sYields, "UPDATE");

		TH1D* hYields = (TH1D*)fYields.Get("yields");
		if(!hYields) hYields = new TH1D ("yields", "yields", 9, 0, 9);
		
		int nBin;
        if(years[i] == 2011) nBin = 1;
        if(years[i] == 2012) nBin = 2;
        if(years[i] == 1112) nBin = 3;
        if(years[i] == 2015) nBin = 4;
        if(years[i] == 2016) nBin = 5;
        if(years[i] == 1516) nBin = 6;
        if(years[i] == 2017) nBin = 7;
        if(years[i] == 2018) nBin = 8;
        if(years[i] == 2) nBin = 9;


		hYields->SetBinContent(nBin, nBs->getVal());
		hYields->SetBinError(nBin, nBs->getError());
		
		fYields.cd();
		hYields->Write("yields", TObject::kOverwrite);

		fYields.Close();

	 


    	RooPlot *mframe = m.frame(Title("Bs2DsDs Run2 data"));
    	
    	cout<<"Fit the data"<<endl;
    	
    	
    	
    	datars->plotOn(mframe, Name("data"));
    	totPDF->plotOn(mframe, Name("NonDD"), Components(modelNonDD), LineColor(13), LineStyle(kDashed));
    	totPDF->plotOn(mframe, Name("misIDBd"), Components(Bd_misID_shape), LineColor(2), LineStyle(kDashed));
    	totPDF->plotOn(mframe, Name("prc"), Components(argusBkg), LineColor(kOrange), LineStyle(kDashed));
    	totPDF->plotOn(mframe, Name("comb"), Components(expBkg), LineColor(kBlue), LineStyle(kDashed));
    	totPDF->plotOn(mframe, Name("signalBs"), Components(modelBsData), LineColor(3), LineStyle(kDashed));
	 	totPDF->plotOn(mframe, Name("totPDF"), LineColor(kRed));
	 	datars->plotOn(mframe, Name("data"));
    	//totPDF->paramOn(mframe, Layout(0.65, 0.95, 0.95));

		int result_status = result->status();
		cout << "result_status = "<< result_status<<endl;
		result->Print("v");
		
		
		RooHist *pull = mframe->pullHist();
		pull->GetYaxis()->SetTitle("Pull");
		RooPlot *framePull = m.frame();
		framePull->addPlotable(pull, "P");
		
		mframe->SetTitle(fitVar);
		mframe->SetXTitle("#it{m}_{B} [MeV/c^{2}]");
		mframe->SetYTitle(Form("Candidates / (%.1lf MeV/c^{2})", binWidth));
		mframe->GetXaxis()->SetTitleSize(0.2);
		mframe->GetYaxis()->SetTitleSize(0.2);
		
		framePull->SetTitle("");
		framePull->GetYaxis()->SetTitle("Pull");
		framePull->SetMinimum(-6.);
		framePull->SetMaximum(6.);
		framePull->SetMarkerStyle(2);
		//cout<<"-----------------1"<<endl;
		
		TCanvas *canv = new TCanvas("canv", "canv", 800, 600);
		canv->SetFillColor(10);
		canv->SetBorderMode(0);
		canv->cd();
		canv->cd(1);
		
		TPad *p1 = new TPad("pad1", "pad1", 0.02, 0.2, 0.98, 0.98, 0);
		TPad *p2 = new TPad("pad2", "pad2", 0.02, 0.04, 0.98, 0.2, 0);
		p1->Draw();
		p2->Draw();
		
		p1->cd();
		mframe->GetYaxis()->SetTitleOffset(1.00);
		mframe->GetYaxis()->SetLabelSize(0.05);
		mframe->GetYaxis()->SetTitleSize(0.05);
		mframe->GetYaxis()->SetNoExponent();
		mframe->GetXaxis()->SetTitleOffset(1.00);
		mframe->GetXaxis()->SetTitleSize(0.05);
		mframe->GetXaxis()->SetLabelSize(0.05);
		//mframe->Draw("E1");
		mframe->Draw();
		text.Draw("same");


		TLegend leg(0.68, 0.55, 0.93, 0.93);
		leg.SetFillStyle(0);
        leg.AddEntry("signalBs", "#it{B^{0}_{s}#rightarrow D^{+}_{s}D^{-}_{s}}", "l");
        leg.AddEntry("NonDD", "#it{B^{0}_{s}#rightarrow D^{-}_{s}K^{+}K^{-}#pi^{+}}", "l");
        leg.AddEntry("misIDBd", "mis-ID #it{B^{0}#rightarrow D^{+}_{s}D^{-}}", "l");
        leg.AddEntry("comb", "Comb. bkg", "l");
        leg.AddEntry("prc", "Prc. bkg", "l");


		leg.SetTextSize(0.85*mframe->GetYaxis()->GetTitleSize());
        leg.SetLineColor(0);
        leg.SetFillColor(0);
		leg.Draw("same");
		
		p2->cd();
		p2->SetTickx();
		framePull->GetYaxis()->SetTitleOffset(0.17);
		framePull->GetXaxis()->SetTitleSize(0.0);
		framePull->GetXaxis()->SetTickLength(0.09);
		framePull->GetYaxis()->SetTitleSize(0.2);
		framePull->GetXaxis()->SetLabelSize(0.0);
		framePull->GetYaxis()->SetLabelSize(0.2);
		framePull->GetYaxis()->SetNdivisions(5);
		framePull->Draw("E1");
		
		TLine* lineZero = new TLine(m.getMin(), 0, m.getMax(), 0);
		lineZero->SetLineStyle(kDashed);
		lineZero->SetLineColor(kBlack);
		lineZero->Draw();
		
		TLine* lineTwoSigUp = new TLine(m.getMin(), 3, m.getMax(), 3);
		lineTwoSigUp->SetLineColor(kRed);
		lineTwoSigUp->Draw();
		TLine* lineTwoSigDown = new TLine(m.getMin(), -3, m.getMax(), -3);
		lineTwoSigDown->SetLineColor(kRed);
		lineTwoSigDown->Draw();

    	canv->SaveAs("/mnt/d/lhcb/B2XcXc/public_html/fit/sPlot/Bs2DsDs/"+yearStr+"_data_Bs2DsDs_sPlot.pdf");


		p1->SetLogy();
		canv->SaveAs("/mnt/d/lhcb/B2XcXc/public_html/fit/sPlot/Bs2DsDs/"+yearStr+"_data_Bs2DsDs_sPlot_log.pdf");


		//cout << "ratio: " << ratio << endl;
		//cout << "fracBd: " << fracBd->getVal() << ", Range: "<< fracBd->getMin() << ", "<< fracBd->getMax() <<endl;
		cout << "nBdBkg: " << nBdBkg->getVal() << ", Range: "<< nBdBkg->getMin() << ", "<< nBdBkg->getMax() <<endl;


        //if(years[i] == 2 || years[i] == 1112){
    	//	datatree->SetBranchStatus("*", 1);
    	//	//////------------------------get sweight way 2--------------
    	//	RooAddPdf* modelSPlot = new RooAddPdf("modelSPlot", "modelSPlot", RooArgList(modelBsData, modelNonDD, Bd_misID_shape, expBkg, argusBkg), RooArgList(*nBs, *nNonDD, *nBdBkg, *nExpBkg, *nArgusBkg));
    	//	////------------------------get sweight way 2--------------
    	//	RooStats::SPlot* splot = new RooStats::SPlot("splot","splot",*datars, modelSPlot, RooArgList(*nBs, *nNonDD, *nBdBkg, *nExpBkg, *nArgusBkg));
    	//	datars->Print("v");
    	//	splot->Print();
    	//	double  sig_sw, exp_sw, prc_sw;
    	//	TString rootname = "/mnt/d/lhcb/B2XcXc/root/Bs/data/Data_Bs2DsDs_Run2_TriggerSel_PreSel_sWeighted.root";
		//	if(years[i] == 1112)  rootname = "/mnt/d/lhcb/B2XcXc/root/Bs/data/Data_Bs2DsDs_Run1_TriggerSel_PreSel_sWeighted.root";
    	//	TFile fSW(rootname, "RECREATE");
    	//	TTree *swTree = datatree->CloneTree(0);
    	//	swTree->Branch("sig_sw",&sig_sw, "sig_sw/D");
    	//	//swTree->Branch("exp_sw", &exp_sw, "exp_sw/D");
    	//	//swTree->Branch("prc_sw", &prc_sw, "prc_sw/D");
    	//	int index = 0;
    	//	for(int i=0;i<datatree->GetEntries();i++){
    	//		datatree->GetEntry(i);
    	//		index++;
    	//		const RooArgSet* row = datars->get(index-1);
    	//		RooRealVar *sig_sw_row = (RooRealVar*)row->find("nBs_sw");
    	//		//RooRealVar *exp_sw_row = (RooRealVar*)row->find("nExpBkg_sw");
    	//		//RooRealVar *prc_sw_row = (RooRealVar*)row->find("nArgusBkg_sw");
    	//	
    	//		sig_sw = sig_sw_row->getVal();
    	//		//exp_sw = exp_sw_row->getVal();
    	//		//prc_sw = prc_sw_row->getVal();
    	//		swTree->Fill();
    	//	}
    	//	swTree->Write();
    	//	fSW.Close();
		//	cout << "saved sweighted root: "+ rootname << endl;
		//}
	}

	out_MC.close();
	out_data_yields.close();
	out_data_meanSigma.close();
	out_data_bkgParams.close();

	cout << endl << "params print to "+fTableMC+" "<< endl;
	system("cat "+fTableMC);

	cout << endl << "params print to "+fTableData1+" "<< endl;
	system("rm " + fTableData);

	system("cat "+fTableData1+">>"+fTableData);
	system("cat "+fTableData2+">>"+fTableData);
	system("cat "+fTableData3+">>"+fTableData);

	system("rm /mnt/d/lhcb/B2XcXc/public_html/fitParamTables/sPlot/data_Bs2DsDs_*.dat");
	system("cat "+fTableData);

	cout << "yields saved into " + sYields << endl;
}
void fit_data_Bs2DsDs_sWeight()
{
	fit("(1)");
	gROOT->ProcessLine(".q");
}

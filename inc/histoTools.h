#ifndef __HISTOTOOLS_H
#define __HISTOTOOLS_H
#include "/mnt/d/lhcb/B2XcXc/inc/tools.h"

class Histo1DWithInfo{
	public:

	Histo1DWithInfo(TString _saveName, TString _formula, TString _xTitle,
					double _range_dn, double _range_up, int _nbin, bool _setLog)
		:saveName(_saveName), formula(_formula), xTitle(_xTitle), range_dn(_range_dn), range_up(_range_up), nbin(_nbin), setLog(_setLog)
	{}

	TString saveName;
	TString formula;
	TString xTitle;
	double range_dn;
	double range_up;
	int nbin;
	bool setLog;

	void displayOnScreen()
	{
		cout<<endl<<endl<<"=======================Histo1D Display============================================"<<endl;
		cout<<"Save name: "<< saveName <<endl;
		cout<<"Var: "<< formula <<endl;
		cout<<"xTitle: "<< xTitle <<endl;
		cout<<"range_dn: "<< range_dn <<endl;
		cout<<"range_up: "<< range_up <<endl;
		cout<<"Nbins: "<< nbin <<endl;
		if(setLog) cout<<"Histo draw with log Y-axis" << endl;
		cout<<"tree"<<endl;
		cout<<endl<<endl<<"=======================Histo1D Display end========================================"<<endl;
	}
};

class Histo2DWithInfo{
	public:

	Histo2DWithInfo(TString _saveName, TString _formula_x, TString _formula_y, TString _xTitle, TString _yTitle,
					double _range_dn_x, double _range_up_x, int _nbin_x, double _range_dn_y, double _range_up_y, int _nbin_y, bool _setLog)
		:saveName(_saveName), formula_x(_formula_x), formula_y(_formula_y), xTitle(_xTitle), yTitle(_yTitle), range_dn_x(_range_dn_x), range_up_x(_range_up_x), nbin_x(_nbin_x), range_dn_y(_range_dn_y), range_up_y(_range_up_y), nbin_y(_nbin_y), setLog(_setLog)
	{}

	TString saveName;
	TString formula_x;
	TString formula_y;
	TString xTitle;
	TString yTitle;
	double range_dn_x;
	double range_up_x;
	double range_dn_y;
	double range_up_y;
	int nbin_x;
	int nbin_y;
	bool setLog;

	void displayOnScreen()
	{
		cout<<endl<<endl<<"=======================Graph2D Display============================================"<<endl;
		cout<<"Save name: "<< saveName <<endl;
		cout<<"VarX: "<< formula_x <<endl;
		cout<<"VarY: "<< formula_y <<endl;
		cout<<"xTitle: "<< xTitle <<endl;
		cout<<"yTitle: "<< yTitle <<endl;
		cout<<"range_dn_x: "<< range_dn_x <<endl;
		cout<<"range_up_x: "<< range_up_x <<endl;
		cout<<"range_dn_y: "<< range_dn_y <<endl;
		cout<<"range_up_y: "<< range_up_y <<endl;
		cout<<"NbinsX: "<< nbin_x <<endl;
		cout<<"NbinsY: "<< nbin_y <<endl;
		if(setLog) cout<<"Histo draw with log Z-axis" << endl;
		cout<<"tree"<<endl;
		cout<<endl<<endl<<"=======================Graph2D Display end========================================"<<endl;
	}
};

vector<TH1D> getVarsHistos1D(TTree* tree, TString cuts, TString weights, int color,
				vector<TString> formulas, vector<TString> xTitles, vector<double> range_dn, vector<double> range_up, vector<int> nbins, bool scale = true){

	if(!tree){
		cerr << "ERROR: failed in getVarsHistos1D: empty tree!" << endl;
		return vector<TH1D>();
	}

	int nPlots = formulas.size();
	if(	formulas.size() != nPlots || xTitles.size() != nPlots ||
		range_dn.size() != nPlots || range_up.size() != nPlots || nbins.size() != nPlots ){
		cerr << "ERROR: failed in getVarsHistos1D: check size" << endl;
		return vector<TH1D>();
	}

	if(cuts == "") cuts="1";
	if(weights == "") weights="1";


	TString cutString = cuts;


	tree->SetBranchStatus("*", 0);

	vector<TH1D> histos;

	setBranchStatusTTF(tree, cuts);
	setBranchStatusTTF(tree, weights);

	for(int i=0; i<nPlots; i++){
		TString rangeCut = "((" +formulas[i]+ ">" + d2s(range_dn[i]) +") && ("+ formulas[i]+ "<" + d2s(range_up[i]) +"))";
		cuts = cutString + " &&" + rangeCut;
		//cuts = cutString;

		cout << "histo: " << i << " / " << nPlots << endl;
		//Initialize histos
		histos.push_back(TH1D(("histo"+i2s(i)), xTitles[i], nbins[i], range_dn[i], range_up[i]));

		histos[i].Sumw2();

		histos[i].GetXaxis()->SetTitle(xTitles[i]);
	 	histos[i].GetYaxis()->SetTitle("arbitrary units");
		histos[i].SetLineColor(color);
		histos[i].SetMarkerColor(color);

		//Set Branch and formulas
		setBranchStatusTTF(tree,formulas[i]);

		tree->Draw((formulas[i]+">>"+histos[i].GetName()), ("("+weights+")*("+cuts+")"));

		if(scale) histos[i].Scale(1./histos[i].Integral());
		//histos[i].Scale(1./histos[i].GetMaximum());
		histos[i].GetXaxis()->SetTitleOffset(1);
		histos[i].GetYaxis()->SetTitleOffset(1);
	}

	return histos;
}

vector<TH1D> getVarsHistos1D(TString rootFile, TString cuts, TString weights, int color,
					 vector<TString> formulas, vector<TString> xTitles, vector<double> range_dn, vector<double> range_up, vector<int> nbins, bool scale = true){
	TFile f(rootFile);
	TTree* tree = (TTree*)f.Get("DecayTree");

	printf("\nRead %s and generating variables histos...\n", rootFile.Data());
	return getVarsHistos1D(tree, cuts, weights, color,
					 formulas, xTitles, range_dn, range_up, nbins, scale);
}


vector<vector<TH1D>> getVarsHistos1D(vector<TString> fileNames, vector<TString> cuts, vector<TString> weights, vector<int> colors,
                vector<Histo1DWithInfo> histoInfos, bool scale = true){

	int nFiles = fileNames.size();
	if(cuts.size()!= nFiles || weights.size() != nFiles || colors.size() != nFiles){
		cerr << "ERROR in getVarsHistos1D: fileNames.size() != drawTitles.size()" << endl;
		return vector<vector<TH1D>> {};
	}
	int nPlots = histoInfos.size();

    vector<TString> varFormulas, xTitles;
    vector<double> range_dn, range_up;
    vector<int> nbins;

    for(int i=0; i<histoInfos.size(); i++){
        varFormulas.push_back(histoInfos[i].formula);
        xTitles.push_back(histoInfos[i].xTitle);

        range_dn.push_back(histoInfos[i].range_dn);
        range_up.push_back(histoInfos[i].range_up);
        nbins.push_back(histoInfos[i].nbin);
    }


    vector<vector<TH1D>> histos(nFiles, vector<TH1D>(nPlots));

    for(int j=0; j<nFiles; j++){
        histos[j] = getVarsHistos1D(fileNames[j], cuts[j], weights[j], colors[j],
                                        varFormulas, xTitles,
                                        range_dn, range_up, nbins, scale);
    }

	return histos;
}


vector<TH1D> getVarsHistos1D(TString fileName, TString cut, TString weight, int color,
                vector<Histo1DWithInfo> histoInfos, bool scale = true){
    vector<TH1D> histos;

    histos = getVarsHistos1D(vector<TString> (1, fileName), vector<TString> (1, cut), vector<TString> (1, weight), vector<int> (1, color),
                                    histoInfos, scale)[0];

	return histos;
}


vector<TH2D> getVarsHistos2D(TTree* tree, TString cuts, TString weights,
				vector<TString> formulas_x, vector<TString> formulas_y, vector<TString> xTitles, vector<TString> yTitles, vector<double> range_dn_x, vector<double> range_up_x, vector<int> nbins_x, vector<double> range_dn_y, vector<double> range_up_y, vector<int> nbins_y){

	if(!tree){
		cerr << "ERROR: failed in getHistos2D: empty tree!" << endl;
		return vector<TH2D>();
	}

	int nPlots = formulas_x.size();
	if(	formulas_x.size() != nPlots || formulas_y.size() != nPlots || xTitles.size() != nPlots || yTitles.size() != nPlots || range_dn_x.size() != nPlots || range_up_x.size() != nPlots || nbins_x.size() != nPlots || range_dn_y.size() != nPlots || range_up_y.size() != nPlots || nbins_y.size() != nPlots){
		cerr << "ERROR: failed in getHistos2D: check size" << endl;
		return vector<TH2D>();
	}

	
	if(cuts == "") cuts="1";
	if(weights == "") weights="1";

	tree->SetBranchStatus("*", 0);

	vector<TH2D> histos;

	setBranchStatusTTF(tree, cuts);
	setBranchStatusTTF(tree, weights);

	int nEntries = tree->GetEntries();

	for(int i=0; i<nPlots; i++){
		//Initialize histos
		histos.push_back(TH2D(("TH2D" + i2s(i)), (xTitles[i] + " vs " + yTitles[i]), nbins_x[i], range_dn_x[i], range_up_x[i], nbins_y[i], range_dn_y[i], range_up_y[i]));

		cout << "2D configuration: " << nbins_x[i] << ", " << range_dn_x[i] << ", " << range_up_x[i]  << ", " << nbins_y[i]  << ", " << range_dn_y[i]  << ", " <<  range_up_y[i] << endl;;

		histos[i].Sumw2();
		histos[i].GetXaxis()->SetTitle(xTitles[i]);
		histos[i].GetYaxis()->SetTitle(yTitles[i]);


		//Set Branch and formulas_x
		setBranchStatusTTF(tree, formulas_x[i]);
		setBranchStatusTTF(tree, formulas_y[i]);

		TTreeFormula varFormulasX(("formula_x"+i2s(i)), formulas_x[i], tree);
		TTreeFormula varFormulasY(("formula_y"+i2s(i)), formulas_y[i], tree);
		TTreeFormula weightFormula("weight", ("("+weights+")*("+cuts+")"), tree);

		cout << "Fill 2D histo for Plot-" << i << "..." << endl;
		for(int n=0; n<nEntries; n++){
			double varX, varY, wt;

			tree->GetEntry(n);

			if(n % (nEntries/10) == 0) cout<<100*n/nEntries<<" \% "<<flush;

			wt = weightFormula.EvalInstance();

			varX = varFormulasX.EvalInstance();
			varY = varFormulasY.EvalInstance();

			//cout << wt << ", " << varX << ", " << varY << endl;

			if(varX>range_dn_x[i] && varX<range_up_x[i]  && varY>range_dn_y[i] && varY<range_up_y[i]){
				histos[i].Fill(varX, varY, wt);
			}
		}
	}

	return histos;
}

vector<TH2D> getVarsHistos2D(TString rootFile, TString cuts, TString weights,
					 vector<TString> formulas_x, vector<TString> formulas_y, vector<TString> xTitles, vector<TString> yTitles, vector<double> range_dn_x, vector<double> range_up_x, vector<int> nbins_x, vector<double> range_dn_y, vector<double> range_up_y, vector<int> nbins_y){
	TFile f(rootFile);
	TTree* tree = (TTree*)f.Get("DecayTree");

	printf("\nRead %s and generating variables histos...\n", rootFile.Data());

	return getVarsHistos2D(tree, cuts, weights,
					 formulas_x, formulas_y, xTitles, yTitles, range_dn_x, range_up_x, nbins_x, range_dn_y, range_up_y, nbins_y);
}


//Draw all the root variables in the fileNames list on the same canvas
void drawHistos(vector<vector<TH1D>> histos, vector<TString> drawTitles, vector<TString> drawOptions,
				vector<TString> saveNames, vector<bool> setLog,
				vector<double> legPos,
				TString saveDir, bool savePNG=false, bool scale = true){
	int nFiles = histos.size();
	if(drawTitles.size() != nFiles || drawOptions.size() != nFiles){
		cerr << "ERROR in drawHistos: drawTitles.size() != nFiles" << endl;
		return;
	}
	int nPlots = histos[0].size();
	if(saveNames.size() != nPlots || setLog.size() != nPlots){
		cerr << "ERROR in drawHistos: saveNames.size() != nPlots || setLog.size() != nPlots" << endl;
		return;
	}
	if(legPos.size() != 4){
		cerr << "ERROR in drawHistos: legPos.size() != 4" << endl;
		return;
	}

	for(int i=0; i<nFiles; i++){
		if(histos[i].size() != nPlots){
			cerr << "ERROR in drawHistos: histos[i].size() != nPlots" << endl;
			return;
		}
	}

	if(saveDir != "") system(("mkdir -p " + saveDir));

    TCanvas canv("canv", "canv", 800, 600);
    canv.cd();


    TLegend leg(legPos[0], legPos[1], legPos[2], legPos[3]);
    leg.SetLineColor(0);
    leg.SetFillColor(0);

	//const int lineStyle[10] = {1, 6, 8, 9, 10};

    for(int i=0; i<nPlots; i++){
		canv.Clear();
		leg.Clear();

		double yMax=DBL_MIN;
		for(int j=0; j<nFiles; j++){
    		leg.AddEntry(&histos[j][i], drawTitles[j]);
        	yMax = max(yMax, histos[j][i].GetMaximum());

        	if(!setLog[i]){
        		histos[0][i].GetYaxis()->SetRangeUser(0, yMax*1.1);
			}

			//if(drawOptions[j] == "L" || drawOptions[j] == "l"){
			//	histos[j][i].SetLineWidth(5);
			//	histos[j][i].SetLineStyle(lineStyle[j]);
			//}
			if(j==0) histos[j][i].Draw(drawOptions[j]);
			else histos[j][i].Draw((drawOptions[j] + "same"));

			if(saveNames[i].Contains("B_PT")) cout << "B_PT mean: " << histos[j][i].GetMean()*1e-3 << ", " << histos[j][i].GetMeanError()*1e-3 << endl;
		}
		//if(setLog[i]) canv.SetLogy(true);
		canv.SetLogy(setLog[i]);

        if(i==0) leg.Draw();
        //leg.Draw();
		TString saveFormat = ".pdf";
		if(savePNG) saveFormat = ".png";
        if(saveDir != "") canv.SaveAs((saveDir+"/"+saveNames[i]+ saveFormat));

    }

}
void drawHistos(vector<vector<TH1D>> histos,
				vector<Histo1DWithInfo> histoInfos,
                vector<TString> drawTitles, vector<TString> drawOptions,
				vector<double> legPos,
                TString saveDir, bool savePNG=false, bool scale = true){
    vector<TString> saveNames;
    vector<bool> setLogs;

	for(int i=0; i<histoInfos.size(); i++){
        saveNames.push_back(histoInfos[i].saveName);
        setLogs.push_back(histoInfos[i].setLog);
    }

	return drawHistos( histos,
					drawTitles, drawOptions,
					saveNames, setLogs,
					legPos,
					saveDir, savePNG, scale);
}
void drawHistos(vector<TString> fileNames, vector<TString> cuts, vector<TString> weights, vector<int> colors,
				vector<Histo1DWithInfo> histoInfos,
				vector<TString> drawTitles, vector<TString> drawOptions,
				vector<double> legPos,
				TString saveDir, bool savePNG=false, bool scale = true){

	vector<vector<TH1D>> histos = getVarsHistos1D(fileNames,  cuts,  weights, colors,
													histoInfos, scale);
	return drawHistos( histos,
					histoInfos,
					drawTitles, drawOptions,
					legPos,
					saveDir, savePNG, scale);
}
void drawHistos2D(TString fileNames, TString drawTitles, TString cuts, TString weights, TString drawOption,
				vector<TString> saveNames, vector<TString> formulas_x, vector<TString> formulas_y, vector<TString> xTitles, vector<TString> yTitles,
				vector<double> range_dn_x, vector<double> range_up_x, vector<int> nbins_x, vector<double> range_dn_y, vector<double> range_up_y, vector<int> nbins_y, vector<bool> setLog,
				TString saveDir, bool savePNG=false){
	int nPlots = saveNames.size();
	if(formulas_x.size() != nPlots || formulas_y.size() != nPlots || xTitles.size() != nPlots || yTitles.size() != nPlots || range_dn_x.size() != nPlots || range_up_x.size() != nPlots || nbins_x.size() != nPlots || range_dn_y.size() != nPlots || range_up_y.size() != nPlots || nbins_y.size() != nPlots || setLog.size() != nPlots){
		cerr << "ERROR in drawHistos2D: different size of pictures options" << endl;
		return;
	}
	vector<TH2D> histos(nPlots);
	
	histos = getVarsHistos2D(fileNames, cuts, weights,
									formulas_x, formulas_y, xTitles, yTitles,
									range_dn_x, range_up_x, nbins_x, range_dn_y, range_up_y, nbins_y);

	if(saveDir != "") system(("mkdir -p " + saveDir));

    for(int i=0; i<nPlots; i++){
        TCanvas canv(saveNames[i], saveNames[i], 800, 600);
        canv.cd();

		histos[i].Draw(drawOption);

		if(setLog[i]) canv.SetLogy(true);
		TString saveFormat = ".pdf";
		if(savePNG) saveFormat = ".png";
        if(saveDir != "") canv.SaveAs((saveDir+"/"+saveNames[i]+ saveFormat));
    }
}
void drawHistos2D(TString fileNames, TString drawTitles, TString cuts, TString weights, TString drawOption,
				vector<Histo2DWithInfo> graphInfos,
				TString saveDir, bool savePNG=false){
    vector<TString> saveNames, varFormulas_x, varFormulas_y, xTitles, yTitles;
    vector<double> range_dn_x, range_up_x, range_dn_y, range_up_y;
    vector<int> nbins_x, nbins_y;
    vector<bool> setLogs;

	for(int i=0; i<graphInfos.size(); i++){
        saveNames.push_back(graphInfos[i].saveName);

        varFormulas_x.push_back(graphInfos[i].formula_x);
        varFormulas_y.push_back(graphInfos[i].formula_y);

        xTitles.push_back(graphInfos[i].xTitle);
        yTitles.push_back(graphInfos[i].yTitle);

        range_dn_x.push_back(graphInfos[i].range_dn_x);
        range_up_x.push_back(graphInfos[i].range_up_x);
        range_dn_y.push_back(graphInfos[i].range_dn_y);
        range_up_y.push_back(graphInfos[i].range_up_y);

        nbins_x.push_back(graphInfos[i].nbin_x);
        nbins_y.push_back(graphInfos[i].nbin_y);

        setLogs.push_back(graphInfos[i].setLog);
    }
	return drawHistos2D( fileNames, drawTitles, cuts, weights, drawOption,
					saveNames, varFormulas_x, varFormulas_y, xTitles, yTitles, range_dn_x, range_up_x, nbins_x, range_dn_y, range_up_y, nbins_y, setLogs,
					saveDir, savePNG);
}
#endif

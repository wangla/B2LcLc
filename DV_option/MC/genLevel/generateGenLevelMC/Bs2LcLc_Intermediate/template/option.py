from os import environ
from GaudiKernel.SystemOfUnits import *
from Gaudi.Configuration import *
from Configurables import GaudiSequencer, CombineParticles, OfflineVertexFitter
from Configurables import DecayTreeTuple, EventTuple, TupleToolTrigger, TupleToolTISTOS,FilterDesktop
from Configurables import BackgroundCategory, TupleToolDecay, TupleToolVtxIsoln,TupleToolPid,EventCountHisto,TupleToolRecoStats,TupleToolKinematic, SubstitutePID
from Configurables import TupleToolVeto, TupleToolTagging, TupleToolL0Data, TupleToolL0Calo
from Configurables import TupleToolMCTruth, TupleToolMCBackgroundInfo
from Configurables import MCTupleToolAngles, MCTupleToolHierarchy, MCTupleToolKinematic, MCTupleToolPrimaries, MCTupleToolReconstructed, MCTupleToolInteractions, PrintMCTree, PrintMCDecayTreeTool  
from GaudiConfUtils.ConfigurableGenerators import  CombineParticles
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand, AutomaticData
from GaudiConfUtils.ConfigurableGenerators import DaVinci__N3BodyDecays

year = 'YEAR'

doMC = 1
doLocalTest = 0

stream = 'CharmCompleteEvent'
if doMC: stream = 'AllStreams'
rootInTES = '/Event/%s'%stream

from Configurables import DecayTreeTuple
from Configurables import TupleToolTrigger, TupleToolDecay, TupleToolTISTOS


# Trigger lines
myTriggerList = [
             #L0
              'L0HadronDecision'
             ,'L0ElectronDecision'
             ,'L0ElectronHiDecision'
             ,'L0MuonDecision'
             ,'L0DiMuonDecision'
             ,'L0MuonHighDecision'
             #Hlt1
             ,'Hlt1TrackMVADecision'
             ,'Hlt1TwoTrackMVADecision'
             ,'Hlt1GlobalDecision'
             ,'Hlt1TrackAllL0Decision'
             #Hlt2
             ,'Hlt2Topo2BodyBBDTDecision'
             ,'Hlt2Topo3BodyBBDTDecision'
             ,'Hlt2Topo4BodyBBDTDecision'
             ,'Hlt2BHadB02PpPpPmPmDecision'
             ,'Hlt2Topo2BodyDecision'
             ,'Hlt2Topo3BodyDecision'
             ,'Hlt2Topo4BodyDecision'
             ,'Hlt2GlobalDecision'
]


############
from DecayTreeTuple.Configuration import *
from Configurables import LoKi__Hybrid__TupleTool

from Configurables import MCDecayTreeTuple
MCTuple = MCDecayTreeTuple("Bs2LcLc_MCTuple")
MCTuple.Decay = "B_s0 -> ^(Lambda_c+ -> ^p+ ^K- ^pi+) ^(Lambda_c~- -> ^p~- ^K+ ^pi-)"
MCTuple.Branches={
    "B"                 :    "B_s0 -> (Lambda_c+ -> p+ K- pi+) (Lambda_c~- -> p~- K+ pi-)",
    "Xc_plus"           :    "B_s0 -> ^(Lambda_c+ -> p+ K- pi+) (Lambda_c~- -> p~- K+ pi-)",
    "p_Xc_plus"         :    "B_s0 -> (Lambda_c+ -> ^p+ K- pi+) (Lambda_c~- -> p~- K+ pi-)",
    "Kminus_Xc_plus"    :    "B_s0 -> (Lambda_c+ -> p+ ^K- pi+) (Lambda_c~- -> p~- K+ pi-)",
    "piplus_Xc_plus"    :    "B_s0 -> (Lambda_c+ -> p+ K- ^pi+) (Lambda_c~- -> p~- K+ pi-)",
    "Xc_minus"          :    "B_s0 -> (Lambda_c+ -> p+ K- pi+) ^(Lambda_c~- -> p~- K+ pi-)",
    "pbar_Xc_minus"     :    "B_s0 -> (Lambda_c+ -> p+ K- pi+) (Lambda_c~- -> ^p~- K+ pi-)",
    "Kplus_Xc_minus"    :    "B_s0 -> (Lambda_c+ -> p+ K- pi+) (Lambda_c~- -> p~- ^K+ pi-)",
    "piminus_Xc_minus"  :    "B_s0 -> (Lambda_c+ -> p+ K- pi+) (Lambda_c~- -> p~- K+ ^pi-)",
    }

MCTupleToolList=[
    'MCTupleToolAngles',
    'MCTupleToolHierarchy',
    'MCTupleToolKinematic',
    'MCTupleToolPrimaries',
    #'MCTupleToolReconstructed',
    #'MCTupleToolInteractions'
    ]
MCTuple.ToolList=MCTupleToolList



from Configurables import DaVinci, CheckPV, GaudiSequencer, LoKi__HDRFilter


#######################################################################
from Configurables import DaVinci
#DaVinci().CondDBtag = 'sim-20161124-2-vc-md100'
#DaVinci().DDDBtag = 'dddb-20150724'
DaVinci().EvtMax = -1                      # Number of events
DaVinci().PrintFreq = 5000
DaVinci().SkipEvents = 0                       # Events to skip
DaVinci().TupleFile = "Tuple.root"             # NB2DsDTuple
DaVinci().Simulation = doMC
DaVinci().Lumi = not DaVinci().Simulation

DaVinci().UserAlgorithms = [ MCTuple]
DaVinci().DataType = year


if doMC:
    local_dict = {
        '2016' : ["/eos/lhcb/grid/prod/lhcb/MC/2016/ALLSTREAMS.DST/00173952/0000/00173952_00000201_7.AllStreams.dst"],
    }
    DaVinci().InputType = 'DST'
    #DaVinci().RootInTES = rootInTES
else:
    local_dict = {
        '2011' : ["/eos/lhcb/grid/prod/lhcb/LHCb/Collision11/CHARMCOMPLETEEVENT.DST/00041840/0000/00041840_00009954_1.charmcompleteevent.dst"],
        '2012' : ["/eos/lhcb/grid/prod/lhcb/LHCb/Collision12/CHARMCOMPLETEEVENT.DST/00050929/0000/00050929_00009148_1.charmcompleteevent.dst"],
        '2015' : ["/eos/lhcb/grid/prod/lhcb/LHCb/Collision15/CHARMCOMPLETEEVENT.DST/00069078/0000/00069078_00009785_1.charmcompleteevent.dst"],
        '2016' : ["/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/CHARMCOMPLETEEVENT.DST/00069603/0000/00069603_00009880_1.charmcompleteevent.dst"],
        '2017' : ["/eos/lhcb/grid/prod/lhcb/LHCb/Collision17/CHARMCOMPLETEEVENT.DST/00071671/0000/00071671_00009449_1.charmcompleteevent.dst"],
        '2018' : ["/eos/lhcb/grid/prod/lhcb/LHCb/Collision18/CHARMCOMPLETEEVENT.DST/00077434/0000/00077434_00005721_1.charmcompleteevent.dst"],
    }
    DaVinci().InputType = 'DST'

###------------------Local Test
if doLocalTest:
    DaVinci().Input = local_dict[year]



#!/bin/bash
###--------- configuration params-------------#####
### root_file@format: xxx.root
### branch_prefix@format: will be head of the new branch, for example, "2016 will get new branch ""2016BDTout"
### tmva_dir@format: path of trainning mva

root_file="$1"
branch_prefix="$2"
tmva_dir="$3"

echo "root_file: '${root_file}'"
echo "branch_prefix: '${branch_prefix}'"
echo "tmva_dir: '${tmva_dir}'"

root_name=${root_file##*/}
root_dir=${root_file%/*}

TmpDir=${root_dir}/${root_name}Tmp

mkdir -p ${TmpDir}
cd ${TmpDir}

cat /mnt/d/lhcb/B2XcXc/inc/applyTMVA.C | sed "s#_ROOT_DIR#${root_dir}#g" | sed "s#_ROOT_NAME#${root_name}#g" | sed "s#_BRANCH_PREFIX#${branch_prefix}#g" | sed "s#_TMVA_DIR#${tmva_dir}#g" > applyTMVA.C

root -l applyTMVA.C

rm applyTMVA.C

cd ${root_dir}
rm -rf ${TmpDir}

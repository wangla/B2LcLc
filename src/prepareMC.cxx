#include "/mnt/d/lhcb/B2XcXc/inc/tools.h"
#include "/mnt/d/lhcb/B2XcXc/inc/getPresel.h"

//  mdoe == 1, Bd2DsD
//			2, Bd2LcLc
//			3, Bs2DsDs
//			4, Bs2LcLc
//void prepareMC(){
void prepareMC(int mode, bool merge = false){
	//int mode =3;

	vector<TString> channel(4, "");
	channel[0] = "Bd2DsD";
	channel[1] = "Bd2LcLc";
	channel[2] = "Bs2DsDs";
	channel[3] = "Bs2LcLc";

	TString modeStr = channel[mode-1];
	TString Bs_Bd = "Bd";
	if(mode == 3 || mode == 4) Bs_Bd = "Bs";

	TString stat = "_stat_2";
	//TString stat = "";
	// "_stat_0" "_stat_1" "_stat_2" "_stat_3" "_stat_4" "_syst_1"

	if(merge){
		vector<TString> mergeTrees;
		vector<TString> mergeBdBkgTrees;

		for(int year=2015; year<=2018; year++){

			// Bd2DsD
			if(mode==1){
				// add year label
				addBranch("/mnt/d/lhcb/B2XcXc/root/Bd/MC/truthed/MC_Bd2DsD_"+i2s(year)+"_Down_truth_PIDcor"+stat+".root", "year", i2s(year));
				addBranch("/mnt/d/lhcb/B2XcXc/root/Bd/MC/truthed/MC_Bd2DsD_"+i2s(year)+"_Up_truth_PIDcor"+stat+".root", "year", i2s(year));
				// mag polarity
				addBranch("/mnt/d/lhcb/B2XcXc/root/Bd/MC/truthed/MC_Bd2DsD_"+i2s(year)+"_Down_truth_PIDcor"+stat+".root", "magFlag", "-1");
				addBranch("/mnt/d/lhcb/B2XcXc/root/Bd/MC/truthed/MC_Bd2DsD_"+i2s(year)+"_Up_truth_PIDcor"+stat+".root", "magFlag", "1");

				mergeTrees.push_back("/mnt/d/lhcb/B2XcXc/root/Bd/MC/truthed/MC_Bd2DsD_"+i2s(year)+"_Down_truth_PIDcor"+stat+".root");
				mergeTrees.push_back("/mnt/d/lhcb/B2XcXc/root/Bd/MC/truthed/MC_Bd2DsD_"+i2s(year)+"_Up_truth_PIDcor"+stat+".root");


				////// add year label
				//addBranch("/mnt/d/lhcb/B2XcXc/root/Bd/MC/truthed/MC_Bs2DsD_"+i2s(year)+"_Down_truth_PIDcor.root", "year", i2s(year));
				//addBranch("/mnt/d/lhcb/B2XcXc/root/Bd/MC/truthed/MC_Bs2DsD_"+i2s(year)+"_Up_truth_PIDcor.root", "year", i2s(year));
				//// mag polarity
				//addBranch("/mnt/d/lhcb/B2XcXc/root/Bd/MC/truthed/MC_Bs2DsD_"+i2s(year)+"_Down_truth_PIDcor.root", "magFlag", "-1");
				//addBranch("/mnt/d/lhcb/B2XcXc/root/Bd/MC/truthed/MC_Bs2DsD_"+i2s(year)+"_Up_truth_PIDcor.root", "magFlag", "1");

				//// Bs2DsD 本底
				//mergeBdBkgTrees.push_back("/mnt/d/lhcb/B2XcXc/root/Bd/MC/truthed/MC_Bs2DsD_"+i2s(year)+"_Down_truth_PIDcor.root");
				//mergeBdBkgTrees.push_back("/mnt/d/lhcb/B2XcXc/root/Bd/MC/truthed/MC_Bs2DsD_"+i2s(year)+"_Up_truth_PIDcor.root");
			}

			// Bd2LcLc
			if(mode==2){
				// add year label
				addBranch("/mnt/d/lhcb/B2XcXc/root/Bd/MC/truthed/MC_Bd2LcLc_"+i2s(year)+"_Down_truth_PIDcor"+stat+".root", "year", i2s(year));
				addBranch("/mnt/d/lhcb/B2XcXc/root/Bd/MC/truthed/MC_Bd2LcLc_"+i2s(year)+"_Up_truth_PIDcor"+stat+".root", "year", i2s(year));
				// mag polarity
				addBranch("/mnt/d/lhcb/B2XcXc/root/Bd/MC/truthed/MC_Bd2LcLc_"+i2s(year)+"_Down_truth_PIDcor"+stat+".root", "magFlag", "-1");
				addBranch("/mnt/d/lhcb/B2XcXc/root/Bd/MC/truthed/MC_Bd2LcLc_"+i2s(year)+"_Up_truth_PIDcor"+stat+".root", "magFlag", "1");

				mergeTrees.push_back("/mnt/d/lhcb/B2XcXc/root/Bd/MC/truthed/MC_Bd2LcLc_"+i2s(year)+"_Down_truth_PIDcor"+stat+".root");
				mergeTrees.push_back("/mnt/d/lhcb/B2XcXc/root/Bd/MC/truthed/MC_Bd2LcLc_"+i2s(year)+"_Up_truth_PIDcor"+stat+".root");
			}

			// Bs2DsDs misID bkg from Bd2DsD
			if(mode == 3){
				// add year label
				addBranch("/mnt/d/lhcb/B2XcXc/root/Bs/MC/truthed/MC_Bs2DsDs_"+i2s(year)+"_Down_truth_PIDcor"+stat+".root", "year", i2s(year));
				addBranch("/mnt/d/lhcb/B2XcXc/root/Bs/MC/truthed/MC_Bs2DsDs_"+i2s(year)+"_Up_truth_PIDcor"+stat+".root", "year", i2s(year));
				// mag polarity
				addBranch("/mnt/d/lhcb/B2XcXc/root/Bs/MC/truthed/MC_Bs2DsDs_"+i2s(year)+"_Down_truth_PIDcor"+stat+".root", "magFlag", "-1");
				addBranch("/mnt/d/lhcb/B2XcXc/root/Bs/MC/truthed/MC_Bs2DsDs_"+i2s(year)+"_Up_truth_PIDcor"+stat+".root", "magFlag", "1");

				mergeTrees.push_back("/mnt/d/lhcb/B2XcXc/root/Bs/MC/truthed/MC_Bs2DsDs_"+i2s(year)+"_Down_truth_PIDcor"+stat+".root");
				mergeTrees.push_back("/mnt/d/lhcb/B2XcXc/root/Bs/MC/truthed/MC_Bs2DsDs_"+i2s(year)+"_Up_truth_PIDcor"+stat+".root");


				//// add year label
				//addBranch("/mnt/d/lhcb/B2XcXc/root/Bs/MC/truthed/MC_Bd2DsD_misID_"+i2s(year)+"_Down.root", "year", i2s(year));
				//addBranch("/mnt/d/lhcb/B2XcXc/root/Bs/MC/truthed/MC_Bd2DsD_misID_"+i2s(year)+"_Up.root", "year", i2s(year));
				// // mag polarity
				//addBranch("/mnt/d/lhcb/B2XcXc/root/Bs/MC/truthed/MC_Bd2DsD_misID_"+i2s(year)+"_Down.root", "magFlag", "-1");
				//addBranch("/mnt/d/lhcb/B2XcXc/root/Bs/MC/truthed/MC_Bd2DsD_misID_"+i2s(year)+"_Up.root", "magFlag", "1");
                //
				//addBranch("/mnt/d/lhcb/B2XcXc/root/Bs/MC/truthed/MC_Bd2DsD_misID_mdst_"+i2s(year)+"_Down.root", "year", i2s(year));
				//addBranch("/mnt/d/lhcb/B2XcXc/root/Bs/MC/truthed/MC_Bd2DsD_misID_mdst_"+i2s(year)+"_Up.root", "year", i2s(year));
				// //mag polarity
				//addBranch("/mnt/d/lhcb/B2XcXc/root/Bs/MC/truthed/MC_Bd2DsD_misID_mdst_"+i2s(year)+"_Down.root", "magFlag", "-1");
				//addBranch("/mnt/d/lhcb/B2XcXc/root/Bs/MC/truthed/MC_Bd2DsD_misID_mdst_"+i2s(year)+"_Up.root", "magFlag", "1");

				//mergeBdBkgTrees.push_back("/mnt/d/lhcb/B2XcXc/root/Bs/MC/truthed/MC_Bd2DsD_misID_"+i2s(year)+"_Down.root");
				//mergeBdBkgTrees.push_back("/mnt/d/lhcb/B2XcXc/root/Bs/MC/truthed/MC_Bd2DsD_misID_"+i2s(year)+"_Up.root");
				//mergeBdBkgTrees.push_back("/mnt/d/lhcb/B2XcXc/root/Bs/MC/truthed/MC_Bd2DsD_misID_mdst_"+i2s(year)+"_Down.root");
				//mergeBdBkgTrees.push_back("/mnt/d/lhcb/B2XcXc/root/Bs/MC/truthed/MC_Bd2DsD_misID_mdst_"+i2s(year)+"_Up.root");
			}

			// Bs2LcLc
			if(mode==4){
				// add year label
				addBranch("/mnt/d/lhcb/B2XcXc/root/Bs/MC/truthed/MC_Bs2LcLc_"+i2s(year)+"_Down_truth_PIDcor"+stat+".root", "year", i2s(year));
				addBranch("/mnt/d/lhcb/B2XcXc/root/Bs/MC/truthed/MC_Bs2LcLc_"+i2s(year)+"_Up_truth_PIDcor"+stat+".root", "year", i2s(year));
				// mag polarity
				addBranch("/mnt/d/lhcb/B2XcXc/root/Bs/MC/truthed/MC_Bs2LcLc_"+i2s(year)+"_Down_truth_PIDcor"+stat+".root", "magFlag", "-1");
				addBranch("/mnt/d/lhcb/B2XcXc/root/Bs/MC/truthed/MC_Bs2LcLc_"+i2s(year)+"_Up_truth_PIDcor"+stat+".root", "magFlag", "1");

				mergeTrees.push_back("/mnt/d/lhcb/B2XcXc/root/Bs/MC/truthed/MC_Bs2LcLc_"+i2s(year)+"_Down_truth_PIDcor"+stat+".root");
				mergeTrees.push_back("/mnt/d/lhcb/B2XcXc/root/Bs/MC/truthed/MC_Bs2LcLc_"+i2s(year)+"_Up_truth_PIDcor"+stat+".root");
			}
		}




		// merge into Run2
		vector<double> weights;
		vector<double> entries_Bd2DsD = {503609, 504188, 1002371, 1197869, 1001545, 1000627, 1009837, 1005389};
		vector<double> entries_Bd2LcLc = {503029, 500467, 1004381, 1093601, 1009037, 1009518, 1005341, 1007072};
		vector<double> entries_Bs2DsDs = {501000, 504129, 1000997, 1002543, 1003434, 1000996, 1001632, 1003754};
		vector<double> entries_Bs2LcLc = {503245, 500254, 1045302, 1082947, 1000471, 1004623, 1005302, 1000129};
		vector<vector<double>> entries;
		entries.push_back(entries_Bd2DsD);
		entries.push_back(entries_Bd2LcLc);
		entries.push_back(entries_Bs2DsDs);
		entries.push_back(entries_Bs2LcLc);

		weights = getDataCondWeights(entries[mode-1]);

		if(mode == 1) {
			MergeTreesWithWeights(mergeTrees, "", weights, "dataCondWeight", "/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2DsD_Run2_truth_PIDcor"+stat+".root");

			//// Bs2DsD 本底
			//vector<double> weightsBkg(mergeBdBkgTrees.size(), 1.0);
			//MergeTreesWithWeights(mergeBdBkgTrees, "", weightsBkg, "dataCondWeight", "/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bs2DsD_Run2_truth_PIDcor.root");
		}
		if(mode == 2) MergeTreesWithWeights(mergeTrees, "", weights, "dataCondWeight", "/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2LcLc_Run2_truth_PIDcor"+stat+".root");
		if(mode == 3){
			MergeTreesWithWeights(mergeTrees, "", weights, "dataCondWeight", "/mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bs2DsDs_Run2_truth_PIDcor"+stat+".root");


			//vector<double> weightsBkg(mergeBdBkgTrees.size(), 1.0);
			//MergeTreesWithWeights(mergeBdBkgTrees, "", weightsBkg, "dataCondWeight", "/mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bd2DsD_misID_Run2.root");
		}
		if(mode == 4) MergeTreesWithWeights(mergeTrees, "", weights, "dataCondWeight", "/mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bs2LcLc_Run2_truth_PIDcor"+stat+".root");
	}

	vector<TString> newBranchName;
	vector<TString> formulas;

	TString probNNxFormula;
	if(mode == 2 || mode == 4){
		//probNNxFormula = "pbar_Xc_minus_MC15TuneV1_ProbNNp_corr*Kplus_Xc_minus_MC15TuneV1_ProbNNk_corr*piminus_Xc_minus_MC15TuneV1_ProbNNpi_corr*p_Xc_plus_MC15TuneV1_ProbNNp_corr*Kminus_Xc_plus_MC15TuneV1_ProbNNk_corr*piplus_Xc_plus_MC15TuneV1_ProbNNpi_corr";
		probNNxFormula = "pbar_Xc_minus_MC15TuneV1_ProbNNp_corr*Kplus_Xc_minus_MC15TuneV1_ProbNNk_corr*piminus_Xc_minus_MC15TuneV1_ProbNNpi*p_Xc_plus_MC15TuneV1_ProbNNp_corr*Kminus_Xc_plus_MC15TuneV1_ProbNNk_corr*piplus_Xc_plus_MC15TuneV1_ProbNNpi";

	}
	// Bd2DsD
	if(mode == 1){
		//probNNxFormula = "Kplus_Dsminus_MC15TuneV1_ProbNNk_corr*Kminus_Dsminus_MC15TuneV1_ProbNNk_corr*piminus_Dsminus_MC15TuneV1_ProbNNpi_corr*Kminus_Dplus_MC15TuneV1_ProbNNk_corr*piplus1_Dplus_MC15TuneV1_ProbNNpi_corr*piplus2_Dplus_MC15TuneV1_ProbNNpi_corr";
		probNNxFormula = "Kplus_Dsminus_MC15TuneV1_ProbNNk_corr*Kminus_Dsminus_MC15TuneV1_ProbNNk_corr*piminus_Dsminus_MC15TuneV1_ProbNNpi*Kminus_Dplus_MC15TuneV1_ProbNNk_corr*piplus1_Dplus_MC15TuneV1_ProbNNpi*piplus2_Dplus_MC15TuneV1_ProbNNpi";

	}
	// Bs2DsDs
	if(mode == 3){
		//probNNxFormula  = "Kplus_Dsminus_MC15TuneV1_ProbNNk_corr*Kminus_Dsminus_MC15TuneV1_ProbNNk_corr*piminus_Dsminus_MC15TuneV1_ProbNNpi_corr*Kminus_Dsplus_MC15TuneV1_ProbNNk_corr*Kplus_Dsplus_MC15TuneV1_ProbNNk_corr*piplus_Dsplus_MC15TuneV1_ProbNNpi_corr";
		probNNxFormula  = "Kplus_Dsminus_MC15TuneV1_ProbNNk_corr*Kminus_Dsminus_MC15TuneV1_ProbNNk_corr*piminus_Dsminus_MC15TuneV1_ProbNNpi*Kminus_Dsplus_MC15TuneV1_ProbNNk_corr*Kplus_Dsplus_MC15TuneV1_ProbNNk_corr*piplus_Dsplus_MC15TuneV1_ProbNNpi";

	}

	newBranchName.push_back("prodProbNNx"); formulas.push_back(probNNxFormula);
	newBranchName.push_back("B_DTFM_M_0"); formulas.push_back("B_DTFM_M[0]");
	addAngleList(mode, &newBranchName, &formulas);

	vector<TString> trees;
	trees.push_back("/mnt/d/lhcb/B2XcXc/root/"+Bs_Bd+"/MC/MC_"+modeStr+"_Run2_truth_PIDcor"+stat+".root");

	////if(mode == 1){
	////	// Bs2DsD 本底
	////	trees.push_back("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bs2DsD_Run2_truth_PIDcor.root");
	////}
	////if(mode == 3){
    ////	trees.push_back("/mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bd2DsD_misID_Run2.root");
	////}

	addBranch(trees, newBranchName, formulas);
	addRadomNumber(trees);
	addCombineMassVars(trees, mode);

	////// Bs2DsDs 的 misID 本底, PID 不需要cor的
	////if(mode == 3){
	////	vector<TString> newBranchBkg, formulasBkg;
	////	newBranchBkg.push_back("prodProbNNx");  formulasBkg.push_back("Kplus_Dsminus_MC15TuneV1_ProbNNk*Kminus_Dsminus_MC15TuneV1_ProbNNk*piminus_Dsminus_MC15TuneV1_ProbNNpi*Kminus_Dsplus_MC15TuneV1_ProbNNk*Kplus_Dsplus_MC15TuneV1_ProbNNk*piplus_Dsplus_MC15TuneV1_ProbNNpi");
	////	newBranchBkg.push_back("Kplus_Dsminus_PIDp_corr");  formulasBkg.push_back("Kplus_Dsminus_PIDp");
	////	newBranchBkg.push_back("Kplus_Dsminus_PIDK_corr");  formulasBkg.push_back("Kplus_Dsminus_PIDK");
	////	newBranchBkg.push_back("Kminus_Dsminus_PIDp_corr");  formulasBkg.push_back("Kminus_Dsminus_PIDp");
	////	newBranchBkg.push_back("Kminus_Dsminus_PIDK_corr");  formulasBkg.push_back("Kminus_Dsminus_PIDK");
	////	newBranchBkg.push_back("piminus_Dsminus_PIDp_corr");  formulasBkg.push_back("piminus_Dsminus_PIDp");
	////	newBranchBkg.push_back("piminus_Dsminus_PIDK_corr");  formulasBkg.push_back("piminus_Dsminus_PIDK");
	////	newBranchBkg.push_back("Kminus_Dsplus_PIDp_corr");  formulasBkg.push_back("Kminus_Dsplus_PIDp");
	////	newBranchBkg.push_back("Kminus_Dsplus_PIDK_corr");  formulasBkg.push_back("Kminus_Dsplus_PIDK");
	////	newBranchBkg.push_back("Kplus_Dsplus_PIDp_corr");  formulasBkg.push_back("Kplus_Dsplus_PIDp");
	////	newBranchBkg.push_back("Kplus_Dsplus_PIDK_corr");  formulasBkg.push_back("Kplus_Dsplus_PIDK");
	////	newBranchBkg.push_back("piplus_Dsplus_PIDp_corr");  formulasBkg.push_back("piplus_Dsplus_PIDp");
	////	newBranchBkg.push_back("piplus_Dsplus_PIDK_corr");  formulasBkg.push_back("piplus_Dsplus_PIDK");
	////	addBranch("/mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bd2DsD_misID_Run2.root", newBranchBkg, formulasBkg);
	////}

	//if(mode == 1){
	//	selTree("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2DsD_Run2_truth_PIDcor.root", "/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2DsD_Run2_truth_PIDcor_TrigSel.root", getTriggerCut());
	//	selTree("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2DsD_Run2_truth_PIDcor_TrigSel.root", "/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2DsD_Run2_truth_PIDcor_TrigSel_Presel.root", getPresel(modeStr));

	//	//selTree("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2DsD_Run2_truth_PIDcor_TrigSel_Presel.root", "/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2DsD_truth_PIDcor_TrigSel_Presel_1718.root", "(year == 2017 || year == 2018)");
	//	//selTree("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2DsD_Run2_truth_PIDcor_TrigSel_Presel.root", "/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2DsD_truth_PIDcor_TrigSel_Presel_1516.root", "(year == 2015 || year == 2016)");

	//	//selTree("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2DsD_Run2_truth_PIDcor_TrigSel_Presel.root", "/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2DsD_truth_PIDcor_TrigSel_Presel_2017.root", "(year == 2017)");
	//	//selTree("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2DsD_Run2_truth_PIDcor_TrigSel_Presel.root", "/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2DsD_truth_PIDcor_TrigSel_Presel_2018.root", "(year == 2018)");
	//	//selTree("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2DsD_Run2_truth_PIDcor_TrigSel_Presel.root", "/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2DsD_truth_PIDcor_TrigSel_Presel_2015.root", "(year == 2015)");
	//	//selTree("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2DsD_Run2_truth_PIDcor_TrigSel_Presel.root", "/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2DsD_truth_PIDcor_TrigSel_Presel_2016.root", "(year == 2016)");

	//	// Bs2DsD 本底
	//	selTree("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bs2DsD_Run2_truth_PIDcor.root", "/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bs2DsD_Run2_truth_PIDcor_TrigSel.root", getTriggerCut());
	//	selTree("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bs2DsD_Run2_truth_PIDcor_TrigSel.root", "/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bs2DsD_Run2_truth_PIDcor_TrigSel_Presel.root", getPresel(modeStr));
	//}
	//if(mode == 2){
	//	selTree("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2LcLc_Run2_truth_PIDcor.root", "/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2LcLc_Run2_truth_PIDcor_TrigSel.root", getTriggerCut());
	//	selTree("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2LcLc_Run2_truth_PIDcor_TrigSel.root", "/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2LcLc_Run2_truth_PIDcor_TrigSel_Presel.root", getPresel(modeStr));
	//}
	//if(mode == 3){
	//	selTree("/mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bs2DsDs_Run2_truth_PIDcor.root", "/mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bs2DsDs_Run2_truth_PIDcor_TrigSel.root", getTriggerCut());
	//	selTree("/mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bs2DsDs_Run2_truth_PIDcor_TrigSel.root", "/mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bs2DsDs_Run2_truth_PIDcor_TrigSel_Presel.root", getPresel(modeStr));

	//	//// misID
	//	//selTree("/mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bd2DsD_misID_Run2.root", "/mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bd2DsD_misID_Run2_TrigSel.root", getTriggerCut());
	//	//selTree("/mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bd2DsD_misID_Run2_TrigSel.root", "/mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bd2DsD_misID_Run2_TrigSel_Presel.root", getPresel(modeStr));
	//}
	//if(mode == 4){
	//	selTree("/mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bs2LcLc_Run2_truth_PIDcor.root", "/mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bs2LcLc_Run2_truth_PIDcor_TrigSel.root", getTriggerCut());
	//	selTree("/mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bs2LcLc_Run2_truth_PIDcor_TrigSel.root", "/mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bs2LcLc_Run2_truth_PIDcor_TrigSel_Presel.root", getPresel(modeStr));
	//}
}

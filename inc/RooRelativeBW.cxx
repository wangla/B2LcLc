#include "RooAbsPdf.h"
#include "RooRealProxy.h"
#include "RooAbsReal.h"
#include "TMath.h"
#include "RooRelativeBW.h" 

RooRelativeBW::RooRelativeBW(const char *name, const char *title, 
        RooAbsReal& _x,
        RooAbsReal& _mean,
        RooAbsReal& _width,
        RooAbsReal& _spin,
        RooAbsReal& _radius,
        RooAbsReal& _massa,
        RooAbsReal& _massb):
  RooAbsPdf(name,title), 
  x("x","x",this,_x),
  mean("mean","mean",this,_mean),
  width("width","width",this,_width),
  spin("spin","spin",this,_spin),
  radius("radius","radius",this,_radius),
  massa("massa","massa",this,_massa),
  massb("massb","massb",this,_massb)
{ 
} 


RooRelativeBW::RooRelativeBW(const RooRelativeBW& other, const char* name) :  
  RooAbsPdf(other,name), 
  x("x",this,other.x),
  mean("mean",this,other.mean),
  width("width",this,other.width),
  spin("spin",this,other.spin),
  radius("radius",this,other.radius),
  massa("massa",this,other.massa),
  massb("massb",this,other.massb)
{ 
}




// Version found in AFit Package: 
// http://pprc.qmul.ac.uk/~bevan/afit/afit.pdf

Double_t RooRelativeBW::evaluate() const
{
  Double_t arg= x*x - mean*mean;  
  Double_t gammaf = Gamma();

  if(x < massa+massb) return 0;
  else return x*x / (arg*arg + x*x*gammaf*gammaf);
}


Double_t RooRelativeBW::Gamma() const
{
  Double_t kx = KFunction((double)x);
  Double_t km = KFunction(mean);
  Double_t fx = FFunction(radius*kx);
  Double_t fm = FFunction(radius*km);
  Double_t rk = 0.0;
  Double_t rf = 0.0;
  if(km!=0){
    rk = (kx/km);
    if(spin ==1){
      rk = rk*rk*rk;
    }
    if(spin ==2){
      rk = rk*rk*rk*rk*rk;
    }    
  }
  if(fm!=0){
    rf = (fx/fm);
  }
  return width*(mean/x)*rk*rf;
}

Double_t RooRelativeBW::FFunction(Double_t X) const
{
  if(spin==0){
      return 1.0;
  }
  if(spin==1){
    return 1.0/(1 + X*X);
  }
  if(spin==2){
    return 1.0/(9 + 3*X*X + X*X*X*X); 
  }
  return 1.0;
}

Double_t RooRelativeBW::KFunction(Double_t X) const
{
  if(X==0) return 0;
  Double_t massone = sqrt(1 - (massa + massb)*(massa + massb)/(X*X));
  Double_t masstwo = sqrt(1 - (massa - massb)*(massa - massb)/(X*X));
  return X/2.0 * massone * masstwo;
}


#include "/mnt/d/lhcb/B2XcXc/inc/efficiencyComputer.h"

void calEfficiencies(){
	//ROOT::v5::TFormula::SetMaxima(10000);

	EfficiencyComputer EffComp;

	TString IPCutBd;
	TString IPCutBs;
	IPCutBd = getIPCut();
	IPCutBs = getIPCut();

	ValError massCutEff;

	TString presel;

	vector<TString> mode = {
		"Bd2DsD",
		"Bd2LcLc",
		"Bs2DsDs",
		"Bs2LcLc"
	};

	vector<vector<vector<ValError>>> effs(4, vector<vector<ValError>>(9));

	// dim1:		dim2:		dim3:
	// 0 Bd2DsD		0 2011		0~2, two individual L0 eff
	// 1 Bd2LcLc	1 2012		3~5, two individual HLT1 eff
	// 2 Bs2DsDs	2 1112		6~9, two individual HLT2 eff
	// 3 Bs2LcLc	3 2015
	//              4 2016
    //              5 1516
    //              6 2017
    //              7 2018
	//				8 Run2





	//TString stat = "_stat_1";
	TString stat = "";
	// "_stat_0" "_stat_1" "_stat_2" "_stat_3" "_stat_4" "_syst_1"

	// here do not push_back 1112 or 1516 !!!! calculated by combine individual years
    vector<int> years;
    years.push_back(2015);
    years.push_back(2016);
    years.push_back(2017);
    years.push_back(2018);


	//for(int year = 2015; year<=2018; year++){
	for(int i=0; i<years.size(); i++){
		int year = years[i];
		int yearPos;
		if(year>=2011 && year<=2012) yearPos = year-2011;
		else if(year>=2015 && year<=2016) yearPos = year-2015 + 3;
		else if(year>=2017 && year<=2018) yearPos = year-2017 + 6;
		

		cout << "get eff of " << year << " year:" << endl;

		for(int m=0; m<4; m++){
			TString BStr = "Bs";
			TString extraCut = IPCutBs;

			if(m==0 || m==1){
				BStr = "Bd";
				extraCut = IPCutBd;
			}

			// ALL corrections
			effs[m][yearPos] = EffComp.getEfficiency(
					year,
					mode[m],
					"/mnt/d/lhcb/B2XcXc/root/"+BStr+"/MC/MC_"+mode[m]+"_Run2_truth_PIDcor"+stat+".root", "DecayTree",
					//"1",
					"data_MC_w",
					//"data_MC_w*wLc_plus_m2pK_m2Kpi*wLc_minus_m2pK_m2Kpi",
					"1",
					"1",
					"1",
					vector<TString>(1, extraCut)
			);

			TString presel = getPresel(mode[m]);
			TString preselTree = "/mnt/d/lhcb/B2XcXc/root/"+BStr+"/MC/MC_"+mode[m]+"_Run2_truth_PIDcor_TrigSel_Presel.root";
			massCutEff = getEntriesWithError(preselTree, 
						//presel +" && "+getMassCut(mode[m])+" && "+extraCut+" && "+getPIDCut(), "1") / getEntriesWithError(preselTree,
						//presel +"&& "+extraCut+" && "+getPIDCut(), "1"); 
						presel +" && "+getMassCut(mode[m])+" && "+extraCut+" && "+getPIDCut(), "data_MC_w") / getEntriesWithError(preselTree,
						presel +"&& "+extraCut+" && "+getPIDCut(), "data_MC_w"); 
						//presel +" && "+getMassCut(mode[m])+" && "+extraCut+" && "+getPIDCut(), "data_MC_w*wLc_plus_m2pK_m2Kpi*wLc_minus_m2pK_m2Kpi") / getEntriesWithError(preselTree,
						//presel +"&& "+extraCut+" && "+getPIDCut(), "data_MC_w*wLc_plus_m2pK_m2Kpi*wLc_minus_m2pK_m2Kpi"); 
			effs[m][yearPos].push_back(massCutEff);


			//exchange mass and tot
			//cout<<"exchange mass and tot"<<endl;

			ValError dumb1;
			ValError dumb2;
			int n = effs[m][yearPos].size();

			dumb1 = effs[m][yearPos].at(n-2);
			dumb2 = effs[m][yearPos].at(n-1);
			effs[m][yearPos].at(n-2) = dumb2;
			effs[m][yearPos].at(n-1) = dumb1*dumb2;
		}
	}


	// 打印同一个衰变的不同年份效率

	//************************************************
	//PRINT THE TABLE
	//************************************************
	cout<<"PRINT THE TABLE"<<endl;

	int w(17);

	system("mkdir -p /mnt/d/lhcb/B2XcXc/public_html/effciencyTables/");
	system("mkdir -p /mnt/d/lhcb/B2XcXc/public_html/effciencyTables/triggers/");

	vector<TString> effNameVec;
	effNameVec.push_back("L0      &  B L0Global TIS");					//0
	effNameVec.push_back("        &  B L0HadronDecision TOS");			//1
	effNameVec.push_back("\\multicolumn{2}{r}{OR of both L0 lines}");	//2
	effNameVec.push_back("HLt1    &  B Hlt1TrackMVADecision TOS");		//3
	effNameVec.push_back("        &  B Hlt1TwoTrackMVADecision TOS");	//4
	effNameVec.push_back("\\multicolumn{2}{r}{OR of both HLt1 lines}");	//5
	effNameVec.push_back("Hlt2    &  B Hlt2Topo2BodyDecision TOS");		//6
	effNameVec.push_back("        &  B Hlt2Topo3BodyDecision TOS");		//7
	effNameVec.push_back("        &  B Hlt2Topo4BodyDecision TOS");		//8
	effNameVec.push_back("\\multicolumn{2}{r}{OR of both Hlt2 lines}");	//9

	effNameVec.push_back("\\effGeom{}");		//10 
	effNameVec.push_back("\\effRecStrip{}");   	//11
	effNameVec.push_back("\\effTrig{}");	    //12
	effNameVec.push_back("\\effPresel{}");	 	//13
	effNameVec.push_back("\\effPID{}");		    //14
	effNameVec.push_back("\\effIP{}");		    //15
	effNameVec.push_back("\\effFit{}");			//16
	effNameVec.push_back("\\effTot{}");		 	//17


	for(int m=0; m<4; m++){
		ofstream out("/mnt/d/lhcb/B2XcXc/public_html/effciencyTables/"+mode[m]+".dat");
		ofstream outTrig("/mnt/d/lhcb/B2XcXc/public_html/effciencyTables/triggers/"+mode[m]+".dat");

		out<<setw(w)<<" & "<<setw(w)<<"2015  & "<<setw(w)<<"2016  & "<<setw(w)<<"2017  & "<<setw(w)<<"2018 \\\\"<<endl;
		outTrig<<setw(w)<<" & "<<setw(w)<<"2015  & "<<setw(w)<<"2016  & "<<setw(w)<<"2017  & "<<setw(w)<<"2018 \\\\"<<endl;

		int a(2);
		//0: for screen printing,
		//1: for latex document printing
		int printOption = 1;


		//out<<"ALL CORRECTIONS:\\"<<endl;
		//outTrig<<"ALL CORRECTIONS:\\"<<endl;
		for(int i(0); i<10; ++i)
		{
			a = 2;
			outTrig<<setw(50)<<effNameVec.at(i)<<" & ";
			//outTrig<<setw(w)<<(100*effs[m][0].at(i)).roundToError(printOption, a)<<" & ";
			//outTrig<<setw(w)<<(100*effs[m][1].at(i)).roundToError(printOption, a)<<" & ";
			outTrig<<setw(w)<<(100*effs[m][3].at(i)).roundToError(printOption, a)<<" & ";
			outTrig<<setw(w)<<(100*effs[m][4].at(i)).roundToError(printOption, a)<<" & ";
			outTrig<<setw(w)<<(100*effs[m][6].at(i)).roundToError(printOption, a)<<" & ";
			outTrig<<setw(w)<<(100*effs[m][7].at(i)).roundToError(printOption, a)<<"\\\\";
			outTrig<<endl;

			if(i==2) outTrig<<setw(50)<<"\\midrule"<<endl;
			// total triggereff
			if(i==9){
				outTrig<<setw(50)<<"\\midrule"<<endl;
				outTrig<<setw(50)<<"\\multicolumn{2}{l}{Total}"<<" & ";
				//outTrig<<setw(w)<<(100*effs[m][0].at(12)).roundToError(printOption, a)<<" & ";
				//outTrig<<setw(w)<<(100*effs[m][1].at(12)).roundToError(printOption, a)<<" & ";
				outTrig<<setw(w)<<(100*effs[m][3].at(12)).roundToError(printOption, a)<<" & ";
				outTrig<<setw(w)<<(100*effs[m][4].at(12)).roundToError(printOption, a)<<" & ";
				outTrig<<setw(w)<<(100*effs[m][6].at(12)).roundToError(printOption, a)<<" & ";
				outTrig<<setw(w)<<(100*effs[m][7].at(12)).roundToError(printOption, a)<<"\\\\";
				outTrig<<endl;
			}
		}

		// 前面几项都是分开的 trigger 效率
		for(int i(10); i<effs[m][3].size(); ++i)
		{
			//if(i!=14){ //PID
				a = 2;
				if(i == 17) a = 3;
				out<<setw(w)<<effNameVec.at(i)<<" & ";
				//out<<setw(w)<<(100*effs[m][0].at(i)).roundToError(printOption, a)<<" & ";
				//out<<setw(w)<<(100*effs[m][1].at(i)).roundToError(printOption, a)<<" & ";
				out<<setw(w)<<(100*effs[m][3].at(i)).roundToError(printOption, a)<<" & ";
				out<<setw(w)<<(100*effs[m][4].at(i)).roundToError(printOption, a)<<" & ";
				out<<setw(w)<<(100*effs[m][6].at(i)).roundToError(printOption, a)<<" & ";
				out<<setw(w)<<(100*effs[m][7].at(i)).roundToError(printOption, a)<<"\\\\";
				out<<endl;
			//}
		}

		out.close();
		outTrig.close();
		cout << "output efficiencies into /mnt/d/lhcb/B2XcXc/public_html/effciencyTables/"+mode[m]+".dat " << endl;
		//cout << "output trigger effs into /mnt/d/lhcb/B2XcXc/public_html/effciencyTables/triggers/"+mode[m]+".dat " << endl;



		vector<TH1D> effHistosRun1;
		vector<TH1D> effHistosRun2;
		vector<TH1D> effHistos1516;
		// save eff into histos
		//for(int year=2015; year<=2018; year++){
		for(int i=0; i<years.size(); i++){
			int year = years[i];
			int yearPos;
			if(year>=2011 && year<=2012) yearPos = year-2011;
			else if(year>=2015 && year<=2016) yearPos = year-2015 + 3;
			else if(year>=2017 && year<=2018) yearPos = year-2017 + 6;
			else continue;

			system("mkdir -p /mnt/d/lhcb/B2XcXc/Documents/effHists/");
			TFile fEff("/mnt/d/lhcb/B2XcXc/Documents/effHists/"+mode[m]+"_eff_"+i2s(year)+".root", "RECREATE");
			TH1D histEff("effs", "effs", 8, 0, 8);
			for(int i=0; i<8; i++){
				histEff.SetBinContent(i+1, effs[m][yearPos].at(i+10).val);
				histEff.SetBinError(i+1, effs[m][yearPos].at(i+10).err);
			}
			fEff.cd();
			histEff.Write("effs", TObject::kOverwrite);

			if(year>=2011 && year<=2012) effHistosRun1.push_back(histEff);
			if(year>=2015 && year<=2018) effHistosRun2.push_back(histEff);
			if(year>=2015 && year<=2016) effHistos1516.push_back(histEff);

			fEff.Close();
		}

		// merge Run2 Eff
		TFile fEffRun2("/mnt/d/lhcb/B2XcXc/Documents/effHists/"+mode[m]+"_eff_Run2.root", "RECREATE");
		TH1D histEffRun2 = mergeRun2Eff(effHistosRun2);
		histEffRun2.Write("effs", TObject::kOverwrite);
		fEffRun2.Close();
		
		//// merge Run1 Eff
		//TFile fEffRun1("/mnt/d/lhcb/B2XcXc/Documents/effHists/"+mode[m]+"_eff_1112.root", "RECREATE");
		//TH1D histEffRun1 = mergeRun1Eff(effHistosRun1);
		//histEffRun1.Write("effs", TObject::kOverwrite);
		//fEffRun1.Close();
		
		// merge 1516 Eff
		TFile fEff1516("/mnt/d/lhcb/B2XcXc/Documents/effHists/"+mode[m]+"_eff_1516.root", "RECREATE");
		TH1D histEff1516 = merge1516Eff(effHistos1516);
		histEff1516.Write("effs", TObject::kOverwrite);
		fEff1516.Close();
		


		// 总效率存下来，计算分支比
		//for(int year=2015; year<=2018; year++){
		for(int i=0; i<years.size(); i++){
			int year = years[i];

			system("mkdir -p /mnt/d/lhcb/B2XcXc/Documents/effHists/");
			TFile fEff("/mnt/d/lhcb/B2XcXc/Documents/effHists/"+mode[m]+"_eff_Tot.root", "UPDATE");
			TH1D* histEff = (TH1D*)fEff.Get("effs");
			if(!histEff) histEff = new TH1D("effs", "effs", 9, 0, 9);

			int nBin;
			if(year == 2011) nBin = 1;
			if(year == 2012) nBin = 2;
			//if(year == 1112) nBin = 3;
			if(year == 2015) nBin = 4;
			if(year == 2016) nBin = 5;
			//if(year == 1516) nBin = 6;
			if(year == 2017) nBin = 7;
			if(year == 2018) nBin = 8;
			//if(year == 2) nBin = 9;

			ValError effTot = readVEFromFile("/mnt/d/lhcb/B2XcXc/Documents/effHists/"+mode[m]+"_eff_"+i2s(year)+".root", "effs")[7];
			histEff->SetBinContent(nBin, effTot.val);
			histEff->SetBinError(nBin, effTot.err);

			fEff.cd();
			histEff->Write("effs", TObject::kOverwrite);
			fEff.Close();
		}


		//前面只给了分年份的效率，这里补充合起来的
		TFile fEff("/mnt/d/lhcb/B2XcXc/Documents/effHists/"+mode[m]+"_eff_Tot.root", "UPDATE");
		TH1D* histEff = (TH1D*)fEff.Get("effs");
		if(!histEff) histEff = new TH1D("effs", "effs", 9, 0, 9);

		//ValError effTot1112 = readVEFromFile("/mnt/d/lhcb/B2XcXc/Documents/effHists/"+mode[m]+"_eff_1112.root", "effs")[7];
		//histEff->SetBinContent(3, effTot1112.val);
		//histEff->SetBinError(3, effTot1112.err);

		ValError effTot1516 = readVEFromFile("/mnt/d/lhcb/B2XcXc/Documents/effHists/"+mode[m]+"_eff_1516.root", "effs")[7];
		histEff->SetBinContent(6, effTot1516.val);
		histEff->SetBinError(6, effTot1516.err);

		ValError effTotRun2 = readVEFromFile("/mnt/d/lhcb/B2XcXc/Documents/effHists/"+mode[m]+"_eff_Run2.root", "effs")[7];
		histEff->SetBinContent(9, effTotRun2.val);
		histEff->SetBinError(9, effTotRun2.err);

		fEff.cd();
		histEff->Write("effs", TObject::kOverwrite);
		fEff.Close();
	}


	//vector<ValError> totEff1112(4, ValError(0, 0));
	//for(int m=0; m<4; m++){
	//	totEff1112[m] = readVEFromFile("/mnt/d/lhcb/B2XcXc/Documents/effHists/"+mode[m]+"_eff_1112.root", "effs")[7];
	//}

	vector<ValError> totEffRun2(4, ValError(0, 0));
	for(int m=0; m<4; m++){
		totEffRun2[m] = readVEFromFile("/mnt/d/lhcb/B2XcXc/Documents/effHists/"+mode[m]+"_eff_Run2.root", "effs")[7];
	}



	// print ratio of rare/ctrl
	system("mkdir -p /mnt/d/lhcb/B2XcXc/public_html/effciencyTables/ratio/");
	ofstream out_ratio("/mnt/d/lhcb/B2XcXc/public_html/effciencyTables/ratio/ratio.dat");


	int a(2);
	//0: for screen printing,
	//1: for latex document printing
	int printOption = 1;

	//// Bd2LcLc / Bd2DsD
	//out_ratio<<"1112 rarios:"<<endl<<endl;
	//out_ratio<<setw(80)<<"\\multirow{2}*{$\\frac{\\varepsilon(\\BdLcLcbar)}{\\varepsilon(\\BdDsD)}$} & ";
	//out_ratio<<setw(w)<<"\\multirow{2}*{"<<(effs[1][0].at(17)/effs[0][0].at(17)).roundToError(printOption, a)<<"} & ";
	//out_ratio<<setw(w)<<"\\multirow{2}*{"<<(effs[1][1].at(17)/effs[0][1].at(17)).roundToError(printOption, a)<<"} & ";
	//out_ratio<<setw(w)<<"\\multirow{2}*{"<<(totEff1112[1]/totEff1112[0]).roundToError(printOption, a)<<"}\\\\";
	//out_ratio<<endl;
	//out_ratio<<"& & & & &\\\\"<<endl;

	//out_ratio<<"\\midrule"<<endl;


	//// Bs2LcLc / Bs2DsDs
	//out_ratio<<setw(80)<<"\\multirow{2}*{$\\frac{\\varepsilon(\\BsLcLcbar)}{\\varepsilon(\\BsDsDs)}$} & ";
	//out_ratio<<setw(w)<<"\\multirow{2}*{"<<(effs[3][0].at(17)/effs[2][0].at(17)).roundToError(printOption, a)<<"} & ";
	//out_ratio<<setw(w)<<"\\multirow{2}*{"<<(effs[3][1].at(17)/effs[2][1].at(17)).roundToError(printOption, a)<<"} & ";
	//out_ratio<<setw(w)<<"\\multirow{2}*{"<<(totEff1112[3]/totEff1112[2]).roundToError(printOption, a)<<"}\\\\";
	//out_ratio<<endl;
	//out_ratio<<"& & & & &\\\\"<<endl;




	// Bd2LcLc / Bd2DsD
	out_ratio<<"Run2 rarios:"<<endl<<endl;
	out_ratio<<setw(80)<<"\\multirow{2}*{$\\frac{\\varepsilon(\\BdLcLcbar)}{\\varepsilon(\\BdDsD)}$} & ";
	out_ratio<<setw(w)<<"\\multirow{2}*{"<<(effs[1][3].at(17)/effs[0][3].at(17)).roundToError(printOption, a)<<"} & ";
	out_ratio<<setw(w)<<"\\multirow{2}*{"<<(effs[1][4].at(17)/effs[0][4].at(17)).roundToError(printOption, a)<<"} & ";
	out_ratio<<setw(w)<<"\\multirow{2}*{"<<(effs[1][6].at(17)/effs[0][6].at(17)).roundToError(printOption, a)<<"} & ";
	out_ratio<<setw(w)<<"\\multirow{2}*{"<<(effs[1][7].at(17)/effs[0][7].at(17)).roundToError(printOption, a)<<"} & ";
	out_ratio<<setw(w)<<"\\multirow{2}*{"<<(totEffRun2[1]/totEffRun2[0]).roundToError(printOption, a)<<"}\\\\";
	out_ratio<<endl;
	out_ratio<<"& & & & &\\\\"<<endl;

	out_ratio<<"\\midrule"<<endl;


	// Bs2LcLc / Bs2DsDs
	out_ratio<<setw(80)<<"\\multirow{2}*{$\\frac{\\varepsilon(\\BsLcLcbar)}{\\varepsilon(\\BsDsDs)}$} & ";
	out_ratio<<setw(w)<<"\\multirow{2}*{"<<(effs[3][3].at(17)/effs[2][3].at(17)).roundToError(printOption, a)<<"} & ";
	out_ratio<<setw(w)<<"\\multirow{2}*{"<<(effs[3][4].at(17)/effs[2][4].at(17)).roundToError(printOption, a)<<"} & ";
	out_ratio<<setw(w)<<"\\multirow{2}*{"<<(effs[3][6].at(17)/effs[2][6].at(17)).roundToError(printOption, a)<<"} & ";
	out_ratio<<setw(w)<<"\\multirow{2}*{"<<(effs[3][7].at(17)/effs[2][7].at(17)).roundToError(printOption, a)<<"} & ";
	out_ratio<<setw(w)<<"\\multirow{2}*{"<<(totEffRun2[3]/totEffRun2[2]).roundToError(printOption, a)<<"}\\\\";
	out_ratio<<endl;
	out_ratio<<"& & & & &\\\\"<<endl;

	out_ratio.close();
	cout << endl << "output ratio into /mnt/d/lhcb/B2XcXc/public_html/effciencyTables/ratio/ratio.dat " << endl;

	gROOT->ProcessLine(".q");
}

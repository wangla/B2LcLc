#include "/mnt/d/lhcb/B2XcXc/inc/histoTools.h"
#include "/mnt/d/lhcb/B2XcXc/inc/getPresel.h"

void plotBkg(){

	lhcbStyle(0.0603);

	vector<TString> treeNames;
	vector<TString> drawTitles;
	vector<TString> cuts;
	vector<TString> weights;
	vector<int> colors;
	vector<TString> drawOptions;

	vector<double> legPos = {0, 0, 0, 0};
	//vector<double> legPos = {0.5, 0.5, 0.98, 0.95};
	//vector<double> legPos = {0.15, 0.15, 0.35, 0.35};


	vector<Histo1DWithInfo> Bs2DsDs_HistoInfos;

	Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("B_PT", "B_PT", "p_{T}(#it{B}) [MeV]", 0, 5e4, 50, false));
	Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("B_IPCHI2_OWNPV", "B_IPCHI2_OWNPV", "#chi^{2}_{IP}(#it{B}) [MeV]", 0, 15, 50, false));
	Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("B_FDCHI2_OWNPV", "B_FDCHI2_OWNPV", "#chi^{2}_{FD}(#if{B})", 0, 5e3, 50, false));
	Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("m_DsDs_Dsminus_pi2K", "m_DsDs_Dsminus_pi2K", "m(DsDs)_{K#leftarrow#pi}", 5200, 5550, 50, false));
	Bs2DsDs_HistoInfos.push_back(Histo1DWithInfo("m_Dsminus_pi2K", "m_Dsminus_pi2K", "m(Ds^{-})_{K#leftarrow#pi}", 1500, 2100, 50, false));


	treeNames.clear();
	drawTitles.clear();
	cuts.clear();
	weights.clear();
	colors.clear();
	drawOptions.clear();


	// ambigous Ds
	treeNames.push_back("/mnt/d/lhcb/B2XcXc/root/Bs/data/Data_Bs2DsDs_Run2_TriggerSel.root");
	drawTitles.push_back("Bd2DsD bkg");
	cuts.push_back("B_M>5200 && B_M<5550 && "+getPreselNoVeto("Bs2DsDs")+"");
	weights.push_back("(1)");
	colors.push_back(kRed);
	drawOptions.push_back("h");


	treeNames.push_back("/mnt/d/lhcb/B2XcXc/root/Bs/data/Data_Bs2DsDs_Run2_TriggerSel_PreSel.root");
	drawTitles.push_back("Bs2DsDs");
	cuts.push_back("B_M>5200 && B_M<5550 && fabs(Dsplus_M-1968)<25 && fabs(Dsminus_M-1968)<25");
	weights.push_back("(1)");
	colors.push_back(kGreen);
	drawOptions.push_back("h");

	system("mkdir -p /mnt/d/lhcb/B2XcXc/public_html/plotBkg/Bs2DsDs");


	drawHistos(treeNames, cuts, weights, colors,
			Bs2DsDs_HistoInfos,
			drawTitles, drawOptions,
			legPos,
			"/mnt/d/lhcb/B2XcXc/public_html/plotBkg/Bs2DsDs", true);



	//vector<Histo1DWithInfo> B2LcLc_HistoInfos;

	//B2LcLc_HistoInfos.push_back(Histo1DWithInfo("B_M", "B_M", "#it{m(#Lambda_{c}^{+}#overline{#Lambda}_{c}^{-})} [MeV]", 5100, 5550, 50, false));


	//treeNames.clear();
	//drawTitles.clear();
	//cuts.clear();
	//weights.clear();
	//colors.clear();
	//drawOptions.clear();


	//// ambigous Ds
	//treeNames.push_back("/mnt/d/lhcb/B2XcXc/root/Bs/data/Data_Bs2LcLc_SB_Run2_TriggerSel_PreSel.root");
	//drawTitles.push_back("LcLc SB");
	//cuts.push_back(getFinalSel());
	//weights.push_back("(1)");
	//colors.push_back(kBlack);
	//drawOptions.push_back("h");

	//system("mkdir -p /mnt/d/lhcb/B2XcXc/public_html/plotBkg/B2LcLc");


	//drawHistos(treeNames, cuts, weights, colors,
	//		B2LcLc_HistoInfos,
	//		drawTitles, drawOptions,
	//		legPos,
	//		"/mnt/d/lhcb/B2XcXc/public_html/plotBkg/B2LcLc", true);

	gROOT->ProcessLine(".q");
}

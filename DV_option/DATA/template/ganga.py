Year = 'YEAR'
DataOrMC = 'MC_OR_DATA'
MagType = 'MAG_TYPE'
Line="STRIPPING_LINE"
Account= "ACCOUNT"

Input_dict = {
    '2011'   : '/LHCb/Collision11/Beam3500GeV-VeloClosed-Mag{}/Real Data/Reco14/Stripping21r1p1a/90000000/CHARMCOMPLETEEVENT.DST'.format(MagType),
    '2012'   : '/LHCb/Collision12/Beam4000GeV-VeloClosed-Mag{}/Real Data/Reco14/Stripping21r0p1a/90000000/CHARMCOMPLETEEVENT.DST'.format(MagType),
    '2015'   : '/LHCb/Collision15/Beam6500GeV-VeloClosed-Mag{}/Real Data/Reco15a/Stripping24r2/90000000/CHARMCOMPLETEEVENT.DST'.format(MagType),
    '2016'   : '/LHCb/Collision16/Beam6500GeV-VeloClosed-Mag{}/Real Data/Reco16/Stripping28r2/90000000/CHARMCOMPLETEEVENT.DST'.format(MagType),
    '2017'   : '/LHCb/Collision17/Beam6500GeV-VeloClosed-Mag{}/Real Data/Reco17/Stripping29r2/90000000/CHARMCOMPLETEEVENT.DST'.format(MagType),
    '2018'   : '/LHCb/Collision18/Beam6500GeV-VeloClosed-Mag{}/Real Data/Reco18/Stripping34/90000000/CHARMCOMPLETEEVENT.DST'.format(MagType),
}
myJobName = 'B2XcXc-{0}-{1}-{2}-{3}'.format(Line,Year,DataOrMC,MagType)

myApplication = GaudiExec()
myApplication.directory = "/afs/cern.ch/work/l/lai/DaVinciDev_v44r10p5"
myApplication.platform = 'x86_64-centos7-gcc62-opt'
myApplication.options   = [ 'option.py']

data = BKQuery(Input_dict[Year]).getDataset()

mySplitter = SplitByFiles( filesPerJob = 10, maxFiles = -1, ignoremissing = True, bulksubmit=False )
myBackend = Dirac()
j = Job (
        name         = myJobName,
        application  = myApplication,
        splitter     = mySplitter,
        outputfiles  = [ LocalFile('Tuple.root')],
        backend      = myBackend,
#        inputdata    = data[0:100],
        inputdata    = data,
        do_auto_resubmit = True
        )
j.submit(keep_going=True, keep_on_fail=True)


#ifndef __GETPRESEL_H
#define __GETPRESEL_H

#include "/mnt/d/lhcb/B2XcXc/inc/tools.h"

// define the trigger cut
TString getL0Cut(){
	return "( B_L0Global_TIS || B_L0HadronDecision_TOS)";
}

TString getHlt1Cut(){
	return "( B_Hlt1TrackMVADecision_TOS || B_Hlt1TwoTrackMVADecision_TOS)";
}

TString getStripPIDCut(TString mode){
	TString cuts = "(1)";
	if(mode == "Bd2LcLc" || mode == "Bs2LcLc"){
		cuts += " &&(p_Xc_plus_MC15TuneV1_ProbNNp_corr>0.15 && Kminus_Xc_plus_MC15TuneV1_ProbNNk_corr>0.1 && piplus_Xc_plus_MC15TuneV1_ProbNNpi>0.1 && pbar_Xc_minus_MC15TuneV1_ProbNNp_corr>0.15 && Kplus_Xc_minus_MC15TuneV1_ProbNNk_corr>0.1 && piminus_Xc_minus_MC15TuneV1_ProbNNpi>0.1)";
	}else if(mode == "Bd2DsD"){
		cuts += " &&(Kplus_Dsminus_MC15TuneV1_ProbNNk_corr>0.1 && Kminus_Dsminus_MC15TuneV1_ProbNNk_corr>0.1 && piminus_Dsminus_MC15TuneV1_ProbNNpi>0.1 && Kminus_Dplus_MC15TuneV1_ProbNNk_corr>0.1 && piplus1_Dplus_MC15TuneV1_ProbNNpi>0.1 && piplus2_Dplus_MC15TuneV1_ProbNNpi>0.1)";
	}else if(mode == "Bs2DsDs"){
		cuts+= " &&(Kplus_Dsminus_MC15TuneV1_ProbNNk_corr>0.1 && Kminus_Dsminus_MC15TuneV1_ProbNNk_corr>0.1 && piminus_Dsminus_MC15TuneV1_ProbNNpi>0.1 && Kminus_Dsplus_MC15TuneV1_ProbNNk_corr>0.1 && Kplus_Dsplus_MC15TuneV1_ProbNNk_corr>0.1 && piplus_Dsplus_MC15TuneV1_ProbNNpi>0.1)";
	}else{
		cerr << "Bad input in getStripPIDCut(TString mode)!" << endl;
		return "(1)";
	}

	return cuts;
}

TString getHlt2Cut(){
	return "( B_Hlt2Topo2BodyDecision_TOS || B_Hlt2Topo3BodyDecision_TOS || B_Hlt2Topo4BodyDecision_TOS)";
}

TString getTriggerCut(){
	TString cuts;
	cuts = getL0Cut() +" && "+ getHlt1Cut() +" && "+ getHlt2Cut();
	return cuts;
}

TString getCommonPresel(){
	return "((B_IPCHI2_OWNPV + B_ENDVERTEX_CHI2<14) && (B_FDCHI2_OWNPV>64) && (B_ENDVERTEX_CHI2/B_ENDVERTEX_NDOF<10) && (B_DIRA_OWNPV>0.999))  &&(B_DTFM_M_0>4800)";
}

// define the pre-selection
TString getPreselNoVeto(TString mode){
	TString cuts = getCommonPresel();
	if(mode == "Bd2LcLc" || mode == "Bs2LcLc"){
		// for proton
		cuts += " &&((p_Xc_plus_P>10e3 && p_Xc_plus_P<110e3) && (pbar_Xc_minus_P>10e3 && pbar_Xc_minus_P<110e3) )";
		// for Lc
		cuts += " &&((Xc_plus_ENDVERTEX_Z-B_ENDVERTEX_Z>0) && (Xc_minus_ENDVERTEX_Z-B_ENDVERTEX_Z>0))";
		cuts += " &&((fabs(Xc_plus_M-2286.46)<25) && (fabs(Xc_minus_M-2286.46)<25))";
		cuts += " &&(angle_p_Xc_plus_Kminus_Xc_plus >0.0005 && angle_p_Xc_plus_piplus_Xc_plus >0.0005 && angle_piplus_Xc_plus_Kminus_Xc_plus >0.0005 && angle_pbar_Xc_minus_Kplus_Xc_minus >0.0005 && angle_pbar_Xc_minus_piminus_Xc_minus >0.0005 && angle_piminus_Xc_minus_Kplus_Xc_minus >0.0005 && angle_p_Xc_plus_pbar_Xc_minus >0.0005 && angle_p_Xc_plus_Kplus_Xc_minus >0.0005 && angle_p_Xc_plus_piminus_Xc_minus >0.0005 && angle_Kminus_Xc_plus_pbar_Xc_minus >0.0005 && angle_Kminus_Xc_plus_Kplus_Xc_minus >0.0005 && angle_Kminus_Xc_plus_piminus_Xc_minus >0.0005 && angle_piplus_Xc_plus_pbar_Xc_minus >0.0005 && angle_piplus_Xc_plus_Kplus_Xc_minus >0.0005 && angle_piplus_Xc_plus_piminus_Xc_minus >0.0005)";
		// cuts for non Resonnance
		cuts += " &&(Xc_plus_FDCHI2_OWNPV>2 && Xc_minus_FDCHI2_OWNPV>2)";
	}else if(mode == "Bd2DsD"){
		// for D/Ds
		cuts += " &&((Dplus_ENDVERTEX_Z-B_ENDVERTEX_Z>0) && (Dsminus_ENDVERTEX_Z-B_ENDVERTEX_Z>0))";
		cuts += " &&((fabs(Dplus_M-1869.66)<25) && (fabs(Dsminus_M-1968.35)<25))";
		cuts += " &&(angle_Kminus_Dplus_piplus1_Dplus >0.0005 && angle_Kminus_Dplus_piplus2_Dplus >0.0005 && angle_piplus2_Dplus_piplus1_Dplus >0.0005 && angle_Kplus_Dsminus_Kminus_Dsminus >0.0005 && angle_Kplus_Dsminus_piminus_Dsminus >0.0005 && angle_piminus_Dsminus_Kminus_Dsminus >0.0005 && angle_Kminus_Dplus_Kplus_Dsminus >0.0005 && angle_Kminus_Dplus_Kminus_Dsminus >0.0005 && angle_Kminus_Dplus_piminus_Dsminus >0.0005 && angle_piplus1_Dplus_Kplus_Dsminus >0.0005 && angle_piplus1_Dplus_Kminus_Dsminus >0.0005 && angle_piplus1_Dplus_piminus_Dsminus >0.0005 && angle_piplus2_Dplus_Kplus_Dsminus >0.0005 && angle_piplus2_Dplus_Kminus_Dsminus >0.0005 && angle_piplus2_Dplus_piminus_Dsminus >0.0005)";
		// cuts for non Resonnance
		cuts += " &&(Dplus_FDCHI2_OWNPV>2 && Dsminus_FDCHI2_OWNPV>2)";
	}else if(mode == "Bs2DsDs"){
		// for Ds
		cuts += " &&((Dsplus_ENDVERTEX_Z-B_ENDVERTEX_Z>0) && (Dsminus_ENDVERTEX_Z-B_ENDVERTEX_Z>0))";
		cuts += " &&((fabs(Dsplus_M-1968.35)<25) && (fabs(Dsminus_M-1968.35)<25))";
		cuts += " &&(angle_Kminus_Dsplus_Kplus_Dsplus >0.0005 && angle_Kminus_Dsplus_piplus_Dsplus >0.0005 && angle_piplus_Dsplus_Kplus_Dsplus >0.0005 && angle_Kplus_Dsminus_Kminus_Dsminus >0.0005 && angle_Kplus_Dsminus_piminus_Dsminus >0.0005 && angle_piminus_Dsminus_Kminus_Dsminus >0.0005 && angle_Kminus_Dsplus_Kplus_Dsminus >0.0005 && angle_Kminus_Dsplus_Kminus_Dsminus >0.0005 && angle_Kminus_Dsplus_piminus_Dsminus >0.0005 && angle_Kplus_Dsplus_Kplus_Dsminus >0.0005 && angle_Kplus_Dsplus_Kminus_Dsminus >0.0005 && angle_Kplus_Dsplus_piminus_Dsminus >0.0005 && angle_piplus_Dsplus_Kplus_Dsminus >0.0005 && angle_piplus_Dsplus_Kminus_Dsminus >0.0005 && angle_piplus_Dsplus_piminus_Dsminus >0.0005)";
		// cuts for non Resonnance
		cuts += " &&(Dsplus_FDCHI2_OWNPV>2 && Dsminus_FDCHI2_OWNPV>2)";
	}else{
		cerr << "Bad input in getPresel(TString mode)!" << endl;
		return "(1)";
	}

	return cuts;
}

TString getVetoCut(TString mode){
	TString cuts = "(1)";
	if(mode == "Bd2LcLc" || mode == "Bs2LcLc"){
		// veto Ds+
		cuts += " &&((fabs(m_Lcplus_K2p-1968.35)<25 && (fabs(m_KK_Lcplus_K2p-1019.461)>10 && p_Xc_plus_PIDp_corr-p_Xc_plus_PIDK_corr>10)) || (fabs(m_Lcplus_K2p-1968.35)>25))";
		cuts += " &&((fabs(m_Lcminus_K2p-1968.35)<25 && (fabs(m_KK_Lcminus_K2p-1019.461)>10 && pbar_Xc_minus_PIDp_corr-pbar_Xc_minus_PIDK_corr>10)) || (fabs(m_Lcminus_K2p-1968.35)>25))";

		// veto D+
		cuts += " &&((fabs(m_Lcplus_pi2p-1869.66)<25 && p_Xc_plus_PIDp_corr>10) || (fabs(m_Lcplus_pi2p-1869.66)>25))";
		cuts += " &&((fabs(m_Lcminus_pi2p-1869.66)<25 && pbar_Xc_minus_PIDp_corr>10) || (fabs(m_Lcminus_pi2p-1869.66)>25))";
	}else if(mode == "Bd2DsD"){
		// veto Ds+  for D+
		cuts += " &&((fabs(m_Dplus_K2pi1-1968.35)<25 && (fabs(m_KK_Dplus_K2pi1-1019.461)>10 && piplus1_Dplus_PIDK_corr<-10)) || (fabs(m_Dplus_K2pi1-1968.35)>25))";
		cuts += " &&((fabs(m_Dplus_K2pi2-1968.35)<25 && (fabs(m_KK_Dplus_K2pi2-1019.461)>10 && piplus2_Dplus_PIDK_corr<-10)) || (fabs(m_Dplus_K2pi2-1968.35)>25))";
		// veto Lc+  for D+
		cuts += " &&((fabs(m_Dplus_p2pi1-2286.46)<25 && piplus1_Dplus_PIDp_corr<0) || (fabs(m_Dplus_p2pi1-2286.46)>25))";
		cuts += " &&((fabs(m_Dplus_p2pi2-2286.46)<25 && piplus2_Dplus_PIDp_corr<0) || (fabs(m_Dplus_p2pi2-2286.46)>25))";

		// veto D+ for Ds+
		cuts += " &&((fabs(m_Dsminus_pi2K-1869.66)<25 && (fabs(m_KK_Dsminus-1019.461)<10 || Kminus_Dsminus_PIDK_corr>10)) || (fabs(m_Dsminus_pi2K-1869.66)>25))";
		// veto Lc+ for Ds+
		cuts += " &&((fabs(m_Dsminus_p2K-2286.46)<25 && (fabs(m_KK_Dsminus-1019.461)<10 || Kminus_Dsminus_PIDK_corr-Kminus_Dsminus_PIDp_corr>0)) || (fabs(m_Dsminus_p2K-2286.46)>25))";
	}else if(mode == "Bs2DsDs"){
		// veto D+ for Ds+
		cuts += " &&((fabs(m_Dsplus_pi2K-1869.66)<25 && (fabs(m_KK_Dsplus-1019.461)<10 || Kplus_Dsplus_PIDK_corr>10)) || (fabs(m_Dsplus_pi2K-1869.66)>25))";
		cuts += " &&((fabs(m_Dsminus_pi2K-1869.66)<25 && (fabs(m_KK_Dsminus-1019.461)<10 || Kminus_Dsminus_PIDK_corr>10)) || (fabs(m_Dsminus_pi2K-1869.66)>25))";

		// veto Lc+ for Ds+
		cuts += " &&((fabs(m_Dsplus_p2K-2286.46)<25 && (fabs(m_KK_Dsplus-1019.461)<10 || Kplus_Dsplus_PIDK_corr-Kplus_Dsplus_PIDp_corr>0)) || (fabs(m_Dsplus_p2K-2286.46)>25))";
		cuts += " &&((fabs(m_Dsminus_p2K-2286.46)<25 && (fabs(m_KK_Dsminus-1019.461)<10 || Kminus_Dsminus_PIDK_corr-Kminus_Dsminus_PIDp_corr>0)) || (fabs(m_Dsminus_p2K-2286.46)>25))";
	}else{
		cerr << "Bad input in getPresel(TString mode)!" << endl;
		return "(1)";
	}

	return cuts;
}

// define the pre-selection
TString getPresel(TString mode){
	TString cuts = getPreselNoVeto(mode);
	cuts += " &&"+getVetoCut(mode);

	return cuts;
}


TString getMassCut(TString mode){
	if(mode == "Bd2DsD") return "(B_DTFM_M_0>5150 && B_DTFM_M_0<5450)";
	else if(mode == "Bs2DsDs") return "(B_DTFM_M_0>5200 && B_DTFM_M_0<5550)";
	else if(mode == "Bd2LcLc" || mode == "Bs2LcLc") return "(B_DTFM_M_0>5150 && B_DTFM_M_0<5550)";
	else{
		cerr << "Bad input in getProdProbNNx(TString mode)!" << endl;
		return "(1)";
	}
}

TString getProdProbNNx(TString mode){
	if(mode == "Bd2DsD") return "Kplus_Dsminus_MC15TuneV1_ProbNNk*Kminus_Dsminus_MC15TuneV1_ProbNNk*piminus_Dsminus_MC15TuneV1_ProbNNpi*Kminus_Dplus_MC15TuneV1_ProbNNk*piplus1_Dplus_MC15TuneV1_ProbNNpi*piplus2_Dplus_MC15TuneV1_ProbNNpi";
	else if(mode == "Bs2DsDs") return "Kplus_Dsminus_MC15TuneV1_ProbNNk*Kminus_Dsminus_MC15TuneV1_ProbNNk*piminus_Dsminus_MC15TuneV1_ProbNNpi*Kminus_Dsplus_MC15TuneV1_ProbNNk*Kplus_Dsplus_MC15TuneV1_ProbNNk*piplus_Dsplus_MC15TuneV1_ProbNNpi";
	else if(mode == "Bd2LcLc" || mode == "Bs2LcLc") return "pbar_Xc_minus_MC15TuneV1_ProbNNp*Kplus_Xc_minus_MC15TuneV1_ProbNNk*piminus_Xc_minus_MC15TuneV1_ProbNNpi*p_Xc_plus_MC15TuneV1_ProbNNp*Kminus_Xc_plus_MC15TuneV1_ProbNNk*piplus_Xc_plus_MC15TuneV1_ProbNNpi";
	else{
		cerr << "Bad input in getProdProbNNx(TString mode)!" << endl;
		return "(1)";
	}
}


TString getPIDCut(){
	return readSingleLineFile("/mnt/d/lhcb/B2XcXc/Documents/OptimizCutString/Bs/PIDCut.dat");
}

TString getIPCut(){
    return readSingleLineFile("/mnt/d/lhcb/B2XcXc/Documents/OptimizCutString/Bs/IPCut.dat");
}
TString getFinalSel(){
	return getPIDCut() + " && " + getIPCut();
}

//TString getBDTCutBd(){
//	return readSingleLineFile("/mnt/d/lhcb/B2XcXc/Documents/OptimizCutString/Bd/PID_IPout.dat");
//}
//
//TString getBDTCutBs(){
//	return readSingleLineFile("/mnt/d/lhcb/B2XcXc/Documents/OptimizCutString/Bs/PID_IPout.dat");
//}


TString getBd2DsDPreselFor_DsSB(){
	// removed (fabs(Dsminus_M-1968.35)<25) cuts, estimate how much Bd->DKKpi in peak region
	return "((B_IPCHI2_OWNPV + B_ENDVERTEX_CHI2<14) && (B_FDCHI2_OWNPV>64) && (B_ENDVERTEX_CHI2/B_ENDVERTEX_NDOF<10) && (B_DIRA_OWNPV>0.999))  &&(B_DTFM_M_0>4800) &&((Dplus_ENDVERTEX_Z-B_ENDVERTEX_Z>0) && (Dsminus_ENDVERTEX_Z-B_ENDVERTEX_Z>0)) &&((fabs(Dplus_M-1869.66)<25)) &&(angle_Kminus_Dplus_piplus1_Dplus >0.0005 && angle_Kminus_Dplus_piplus2_Dplus >0.0005 && angle_piplus2_Dplus_piplus1_Dplus >0.0005 && angle_Kplus_Dsminus_Kminus_Dsminus >0.0005 && angle_Kplus_Dsminus_piminus_Dsminus >0.0005 && angle_piminus_Dsminus_Kminus_Dsminus >0.0005 && angle_Kminus_Dplus_Kplus_Dsminus >0.0005 && angle_Kminus_Dplus_Kminus_Dsminus >0.0005 && angle_Kminus_Dplus_piminus_Dsminus >0.0005 && angle_piplus1_Dplus_Kplus_Dsminus >0.0005 && angle_piplus1_Dplus_Kminus_Dsminus >0.0005 && angle_piplus1_Dplus_piminus_Dsminus >0.0005 && angle_piplus2_Dplus_Kplus_Dsminus >0.0005 && angle_piplus2_Dplus_Kminus_Dsminus >0.0005 && angle_piplus2_Dplus_piminus_Dsminus >0.0005) &&(Dplus_FDCHI2_OWNPV>2 && Dsminus_FDCHI2_OWNPV>2) &&(1) &&((fabs(m_Dplus_K2pi1-1968.35)<25 && (fabs(m_KK_Dplus_K2pi1-1019.461)>10 && piplus1_Dplus_PIDK_corr<-10)) || (fabs(m_Dplus_K2pi1-1968.35)>25)) &&((fabs(m_Dplus_K2pi2-1968.35)<25 && (fabs(m_KK_Dplus_K2pi2-1019.461)>10 && piplus2_Dplus_PIDK_corr<-10)) || (fabs(m_Dplus_K2pi2-1968.35)>25)) &&((fabs(m_Dplus_p2pi1-2286.46)<25 && piplus1_Dplus_PIDp_corr<0) || (fabs(m_Dplus_p2pi1-2286.46)>25)) &&((fabs(m_Dplus_p2pi2-2286.46)<25 && piplus2_Dplus_PIDp_corr<0) || (fabs(m_Dplus_p2pi2-2286.46)>25)) &&((fabs(m_Dsminus_pi2K-1869.66)<25 && (fabs(m_KK_Dsminus-1019.461)<10 || Kminus_Dsminus_PIDK_corr>10)) || (fabs(m_Dsminus_pi2K-1869.66)>25)) &&((fabs(m_Dsminus_p2K-2286.46)<25 && (fabs(m_KK_Dsminus-1019.461)<10 || Kminus_Dsminus_PIDK_corr-Kminus_Dsminus_PIDp_corr>0)) || (fabs(m_Dsminus_p2K-2286.46)>25))";
}
TString getBs2DsDsPreselFor_DsSB(){
	// removed ((fabs(Dsplus_M-1968.35)<25) && (fabs(Dsminus_M-1968.35)<25)) cuts, estimate how much Bs->DsKKpi in peak region
	return "((B_IPCHI2_OWNPV + B_ENDVERTEX_CHI2<14) && (B_FDCHI2_OWNPV>64) && (B_ENDVERTEX_CHI2/B_ENDVERTEX_NDOF<10) && (B_DIRA_OWNPV>0.999))  &&(B_DTFM_M_0>4800) &&((Dsplus_ENDVERTEX_Z-B_ENDVERTEX_Z>0) && (Dsminus_ENDVERTEX_Z-B_ENDVERTEX_Z>0)) &&(angle_Kminus_Dsplus_Kplus_Dsplus >0.0005 && angle_Kminus_Dsplus_piplus_Dsplus >0.0005 && angle_piplus_Dsplus_Kplus_Dsplus >0.0005 && angle_Kplus_Dsminus_Kminus_Dsminus >0.0005 && angle_Kplus_Dsminus_piminus_Dsminus >0.0005 && angle_piminus_Dsminus_Kminus_Dsminus >0.0005 && angle_Kminus_Dsplus_Kplus_Dsminus >0.0005 && angle_Kminus_Dsplus_Kminus_Dsminus >0.0005 && angle_Kminus_Dsplus_piminus_Dsminus >0.0005 && angle_Kplus_Dsplus_Kplus_Dsminus >0.0005 && angle_Kplus_Dsplus_Kminus_Dsminus >0.0005 && angle_Kplus_Dsplus_piminus_Dsminus >0.0005 && angle_piplus_Dsplus_Kplus_Dsminus >0.0005 && angle_piplus_Dsplus_Kminus_Dsminus >0.0005 && angle_piplus_Dsplus_piminus_Dsminus >0.0005) &&(Dsplus_FDCHI2_OWNPV>2 && Dsminus_FDCHI2_OWNPV>2) &&(1) &&((fabs(m_Dsplus_pi2K-1869.66)<25 && (fabs(m_KK_Dsplus-1019.461)<10 || Kplus_Dsplus_PIDK_corr>10)) || (fabs(m_Dsplus_pi2K-1869.66)>25)) &&((fabs(m_Dsminus_pi2K-1869.66)<25 && (fabs(m_KK_Dsminus-1019.461)<10 || Kminus_Dsminus_PIDK_corr>10)) || (fabs(m_Dsminus_pi2K-1869.66)>25)) &&((fabs(m_Dsplus_p2K-2286.46)<25 && (fabs(m_KK_Dsplus-1019.461)<10 || Kplus_Dsplus_PIDK_corr-Kplus_Dsplus_PIDp_corr>0)) || (fabs(m_Dsplus_p2K-2286.46)>25)) &&((fabs(m_Dsminus_p2K-2286.46)<25 && (fabs(m_KK_Dsminus-1019.461)<10 || Kminus_Dsminus_PIDK_corr-Kminus_Dsminus_PIDp_corr>0)) || (fabs(m_Dsminus_p2K-2286.46)>25))";
}


TString getLcLcPresel_Lc60Mev(){
	return "((B_IPCHI2_OWNPV + B_ENDVERTEX_CHI2<14) && (B_FDCHI2_OWNPV>64) && (B_ENDVERTEX_CHI2/B_ENDVERTEX_NDOF<10) && (B_DIRA_OWNPV>0.999))  &&(B_DTFM_M_0>4800) &&((p_Xc_plus_P>10e3 && p_Xc_plus_P<110e3) && (pbar_Xc_minus_P>10e3 && pbar_Xc_minus_P<110e3) ) &&((Xc_plus_ENDVERTEX_Z-B_ENDVERTEX_Z>0) && (Xc_minus_ENDVERTEX_Z-B_ENDVERTEX_Z>0)) &&((fabs(Xc_plus_M-2286.46)<60) && (fabs(Xc_minus_M-2286.46)<60)) &&(angle_p_Xc_plus_Kminus_Xc_plus >0.0005 && angle_p_Xc_plus_piplus_Xc_plus >0.0005 && angle_piplus_Xc_plus_Kminus_Xc_plus >0.0005 && angle_pbar_Xc_minus_Kplus_Xc_minus >0.0005 && angle_pbar_Xc_minus_piminus_Xc_minus >0.0005 && angle_piminus_Xc_minus_Kplus_Xc_minus >0.0005 && angle_p_Xc_plus_pbar_Xc_minus >0.0005 && angle_p_Xc_plus_Kplus_Xc_minus >0.0005 && angle_p_Xc_plus_piminus_Xc_minus >0.0005 && angle_Kminus_Xc_plus_pbar_Xc_minus >0.0005 && angle_Kminus_Xc_plus_Kplus_Xc_minus >0.0005 && angle_Kminus_Xc_plus_piminus_Xc_minus >0.0005 && angle_piplus_Xc_plus_pbar_Xc_minus >0.0005 && angle_piplus_Xc_plus_Kplus_Xc_minus >0.0005 && angle_piplus_Xc_plus_piminus_Xc_minus >0.0005) &&(Xc_plus_FDCHI2_OWNPV>2 && Xc_minus_FDCHI2_OWNPV>2) &&(1) &&((fabs(m_Lcplus_K2p-1968.35)<25 && (fabs(m_KK_Lcplus_K2p-1019.461)>10 && p_Xc_plus_PIDp_corr-p_Xc_plus_PIDK_corr>10)) || (fabs(m_Lcplus_K2p-1968.35)>25)) &&((fabs(m_Lcminus_K2p-1968.35)<25 && (fabs(m_KK_Lcminus_K2p-1019.461)>10 && pbar_Xc_minus_PIDp_corr-pbar_Xc_minus_PIDK_corr>10)) || (fabs(m_Lcminus_K2p-1968.35)>25)) &&((fabs(m_Lcplus_pi2p-1869.66)<25 && p_Xc_plus_PIDp_corr>10) || (fabs(m_Lcplus_pi2p-1869.66)>25)) &&((fabs(m_Lcminus_pi2p-1869.66)<25 && pbar_Xc_minus_PIDp_corr>10) || (fabs(m_Lcminus_pi2p-1869.66)>25))";
}
#endif

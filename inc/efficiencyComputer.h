#ifndef _EFFICIENCYCOMPUTER_H_
#define _EFFICIENCYCOMPUTER_H_
#include "/mnt/d/lhcb/B2XcXc/inc/tools.h"
#include "/mnt/d/lhcb/B2XcXc/inc/getPresel.h"

class EfficiencyComputer{
	public:

		ValError getEfficiency(
			int year,
			TString mode,
			TString treeRecFileName, TString treeRecName,
			TString nameRecWeight,
			TString namePIDWeight,
			TString nameL0Weight,
			TString nameHltWeight,

			TString extraCut,
			bool wantEfficiencyBreakDown = true,
			TString binVarName = "(1.)",
			TH1D* histoEff = NULL,
			bool debug = false,
			TString overRiddenPreselection = "",
			TString varUniquing = ""
		);


		vector<ValError> getEfficiency(
			int year,
			TString mode,
			TString treeRecFileName, TString treeRecName,
			TString nameRecWeight,
			TString namePIDWeight,
			TString nameL0Weight,
			TString nameHltWeight,

			vector<TString> extraCuts,
			bool wantEfficiencyBreakDown = true,
			TString binVarName = "(1.)",
			TH1D* histoEff = NULL,
			bool debug = false,
			TString overRiddenPreselection = "",
			TString varUniquing = ""
		);

		ValError getDecProdCutEff(int year, TString mode);
		ValError getGenLevelNumber(int year, TString mode);
};

vector<ValError> EfficiencyComputer::getEfficiency(
			int year,
			TString mode,
			TString treeRecFileName, TString treeRecName,
			TString nameRecWeight,
			TString namePIDWeight,
			TString nameL0Weight,
			TString nameHltWeight,

			vector<TString> extraCuts,
			bool wantEfficiencyBreakDown,
			TString binVarName,
			TH1D* histoEff,
			bool debug,
			TString overRiddenPreselection,
			TString varUniquing
		){
	int cutSize(extraCuts.size());
	vector<ValError> ret(cutSize, ValError(0, 0));

	if(extraCuts.size() == 0){
		cerr<<"ERROR: EfficiencyComputer::getEfficiency, extraCuts has size 0"<<endl;
		return ret;
	}

	if(varUniquing == "") varUniquing = "eventNumber";

	//get the efficiency corrector and compute GenLevel eff

	if(!(mode == "Bd2LcLc" || mode == "Bs2LcLc" || mode == "Bd2DsD" || mode == "Bs2DsDs")){
		cerr<<"ERROR: in getEfficiency, invalid mode: "<<mode<<endl;
		return ret;
	}
	if(!((year>=2015 && year<=2018) || year==-1)){
		cerr<<"ERROR: in getEfficiency, invalid year "<<year<<endl;
	}

	ValError geomEff(0, 0);

	geomEff = getDecProdCutEff(year, mode);

	//prepare ValErrors to stock number of events

	ValError nGen(0, 0);
	ValError nStrip(0, 0);
	ValError nStripKin(0, 0);
	ValError nTrig(0, 0);
	ValError nSel(0, 0);
	ValError nPID(0, 0);

	// for individual trigger effs
	ValError nGlbTIS(0, 0);
	ValError nHTOS(0, 0);
	ValError nL0(0, 0);

	ValError nTrkMVA(0, 0);
	ValError nTwoTrkMVA(0, 0);
	ValError nHLT1(0, 0);

	ValError nTopo2(0, 0);
	ValError nTopo3(0, 0);
	ValError nTopo4(0, 0);
	ValError nHLT2(0, 0);

	vector<ValError> nExtraCuts(extraCuts.size(), ValError(0, 0));

	//get number of gen level events

	//printf("Getting nGen from %s...\n", treeGenFileName);
	nGen = getGenLevelNumber(year, mode);


	TFile f(treeRecFileName);
	TTree* t = (TTree*)f.Get(treeRecName);

	printf("Calculate efficiencies for %s\n", treeRecFileName.Data());

	if(!t){
		cerr<<"In getEfficiency, no tree found"<<endl;
		return ret;
	}


	for(int i(0); i<cutSize; ++i){
		if(extraCuts.at(i) == "") extraCuts.at(i) = "(1)";
	}

	TString preselCutString;
	preselCutString = getPresel(mode);
	if(overRiddenPreselection != "") preselCutString = overRiddenPreselection;

	TString pidCutString;
	pidCutString = getPIDCut();


	TString GlbTISCutString = "(B_L0Global_TIS)";
	TString HTOSCutString = "(B_L0HadronDecision_TOS)";
	TString L0CutString = getL0Cut();

	TString TrkMVACutString = "(B_Hlt1TrackMVADecision_TOS)";
	TString TwoTrkMVACutString = "(B_Hlt1TwoTrackMVADecision_TOS)";
	TString HLT1CutString = getHlt1Cut();

	TString Topo2CutString = "(B_Hlt2Topo2BodyDecision_TOS)";
	TString Topo3CutString = "(B_Hlt2Topo3BodyDecision_TOS)";
	TString Topo4CutString = "(B_Hlt2Topo4BodyDecision_TOS)";
	TString HLT2CutString = getHlt2Cut();

	TString trigCutString;
	trigCutString = getTriggerCut(); 

	TString totCutString("1");
	if(year != -1) totCutString = "(year == " +i2s(year)+")";
	totCutString += " &&"+ getStripPIDCut(mode);

	TString passCutString(trigCutString+" && "+preselCutString);

	if(nameRecWeight == "") nameRecWeight = "(1)";
	if(namePIDWeight == "") namePIDWeight = "(1)";
	if(nameL0Weight == "") nameL0Weight = "(1)";

	t->SetBranchStatus("*", 0);	
	t->SetBranchStatus("Polarity", 1);
	t->SetBranchStatus("runNumber", 1);
	t->SetBranchStatus("eventNumber", 1);

	setBranchStatusTTF(t, nameRecWeight);

	setBranchStatusTTF(t, namePIDWeight);
	setBranchStatusTTF(t, nameL0Weight);

	setBranchStatusTTF(t, passCutString);
	setBranchStatusTTF(t, totCutString);
	setBranchStatusTTF(t, preselCutString);
	setBranchStatusTTF(t, pidCutString);

	setBranchStatusTTF(t, GlbTISCutString);
	setBranchStatusTTF(t, HTOSCutString);
	setBranchStatusTTF(t, L0CutString);

	setBranchStatusTTF(t, TrkMVACutString);
	setBranchStatusTTF(t, TwoTrkMVACutString);
	setBranchStatusTTF(t, HLT1CutString);

	setBranchStatusTTF(t, Topo2CutString);
	setBranchStatusTTF(t, Topo3CutString);
	setBranchStatusTTF(t, Topo4CutString);
	setBranchStatusTTF(t, HLT2CutString);

	setBranchStatusTTF(t, trigCutString);
	setBranchStatusTTF(t, binVarName);
	setBranchStatusTTF(t, varUniquing);
	for(int i(0); i<cutSize; ++i) setBranchStatusTTF(t, extraCuts.at(i)); 

	short Polarity;
	unsigned int runNumber;
	unsigned long long int eventNumber;
	t->SetBranchAddress("Polarity", &Polarity);
	t->SetBranchAddress("runNumber", &runNumber);
	t->SetBranchAddress("eventNumber", &eventNumber);


	TTreeFormula ttfKinWeight("ttfKinWeight", nameRecWeight, t);
	TTreeFormula ttfPIDWeight("ttfPIDWeight", namePIDWeight, t);
	TTreeFormula ttfTrigWeight("ttfTrigWeight", nameL0Weight, t);


	TTreeFormula ttfGlbTISCut("ttfGlbTISCut", 		GlbTISCutString, 	t);
	TTreeFormula ttfHTOSCut("ttfHTOSCut", 			HTOSCutString, 		t);
	TTreeFormula ttfL0Cut("ttfL0Cut", 				L0CutString, 		t);

	TTreeFormula ttfTrkMVACut("ttfTrkMVACut", 		TrkMVACutString, 	t);
	TTreeFormula ttfTwoTrkMVACut("ttfTwoTrkMVACut", TwoTrkMVACutString, t);
	TTreeFormula ttfHLT1Cut("ttfHLT1Cut", 			HLT1CutString,		t);

	TTreeFormula ttfTopo2Cut("ttfTopo2Cut", 		Topo2CutString, 	t);
	TTreeFormula ttfTopo3Cut("ttfTopo3Cut", 		Topo3CutString, 	t);
	TTreeFormula ttfTopo4Cut("ttfTopo4Cut", 		Topo4CutString, 	t);
	TTreeFormula ttfHLT2Cut("ttfHLT2Cut", 			HLT2CutString,		t);

	TTreeFormula ttfTrigCut("ttfTrigCut", trigCutString, t);
	TTreeFormula ttfPreselCut("ttfPreselCut", preselCutString, t);
	TTreeFormula ttfPIDCut("ttfPIDCut", pidCutString, t);
	TTreeFormula ttfPassCut("ttfPassCut", passCutString, t);
	TTreeFormula ttfTotCut("ttfTotCut", totCutString, t);
	TTreeFormula ttfBinVar("ttfBinVar", binVarName, t);
	TTreeFormula ttfVarUniquing("ttfVarUniquing", varUniquing, t);

	//cout<<"MEUH PASSCUTSTRING:"<<endl<<passCutString<<endl;

	vector<TTreeFormula*> ttfExtraCuts(cutSize, NULL);
	for(int i(0); i<cutSize; ++i) ttfExtraCuts.at(i) = new TTreeFormula( ("ttfExtraCuts"+i2s(i)), (extraCuts.at(i)), t);

	//loop over the tree to compute efficiency
	double wKin;
	double wL0;
	double wPID(0);
	double wHlt(0);
	double wTot(0);
	double binVar(0);

	int nEntries(t->GetEntries());
	//cout << "nEntries: " << nEntries << endl;
	bool passTotCut(false);
	bool passPassCut(false);
	bool passExtraCut(false);

	for(int i(0); i<nEntries; ++i){
		t->GetEntry(i);
		
		passTotCut = ttfTotCut.EvalInstance();

		if(passTotCut){
			//first get nStrip
			
			// 不对genlevl做修正，所以weight直接赋值1
			nStrip.val += 1;
			nStrip.err += 1;

			//set all the weights

			//set nStripKin
			wKin = ttfKinWeight.EvalInstance();
			nStripKin.val += wKin;
			nStripKin.err += (wKin*wKin);


			wL0 = ttfTrigWeight.EvalInstance();
			wHlt = 1.0;


			wPID = ttfPIDWeight.EvalInstance();

			wTot = wKin * wPID * wL0 * wHlt;

			if(wTot != wTot) wTot = 0; //NaN check

			//get all the N's

			// get individual trigger counters
			if(ttfL0Cut.EvalInstance()){
				// L0
				nL0.val += wKin*wL0;   nL0.err += wKin*wL0;

				if(ttfGlbTISCut.EvalInstance()){
					nGlbTIS.val += wKin*wL0; nGlbTIS.err += wKin*wL0;
				}
				if(ttfHTOSCut.EvalInstance()){
					nHTOS.val += wKin*wL0; nHTOS.err += wKin*wL0;
				}


				// HLT1
				if(ttfHLT1Cut.EvalInstance()){
					nHLT1.val += wKin*wL0*wHlt;   nHLT1.err += wKin*wL0*wHlt;

					if(ttfTrkMVACut.EvalInstance()){
						nTrkMVA.val += wKin*wL0*wHlt;   nTrkMVA.err += wKin*wL0*wHlt;
					}
					if(ttfTwoTrkMVACut.EvalInstance()){
						nTwoTrkMVA.val += wKin*wL0*wHlt;   nTwoTrkMVA.err += wKin*wL0*wHlt;
					}

					// HLT2
					if(ttfHLT2Cut.EvalInstance()){
						nHLT2.val += wKin*wL0*wHlt;   nHLT2.err += wKin*wL0*wHlt;

						if(ttfTopo2Cut.EvalInstance()){
							nTopo2.val += wKin*wL0*wHlt;   nTopo2.err += wKin*wL0*wHlt;
						}
						if(ttfTopo3Cut.EvalInstance()){
							nTopo3.val += wKin*wL0*wHlt;   nTopo3.err += wKin*wL0*wHlt;
						}
						if(ttfTopo4Cut.EvalInstance()){
							nTopo4.val += wKin*wL0*wHlt;   nTopo4.err += wKin*wL0*wHlt;
						}
					}
				}
			}
			

			if(ttfTrigCut.EvalInstance()){
				nTrig.val += wKin*wL0*wHlt;
				nTrig.err += (wKin*wL0*wHlt)*(wKin*wL0*wHlt);

				if(ttfPreselCut.EvalInstance()){
					nSel.val += wKin*wL0*wHlt;
					nSel.err += (wKin*wL0*wHlt)*(wKin*wL0*wHlt);

					if(ttfPIDCut.EvalInstance()){
						nPID.val += wTot;
						nPID.err += wTot*wTot;


						for(unsigned int i(0); i<extraCuts.size(); ++i){
							passExtraCut = ttfExtraCuts.at(i)->EvalInstance();

							if(passExtraCut){
								nExtraCuts.at(i).val += wTot;
								nExtraCuts.at(i).err += wTot*wTot;
							}
						}

						if(histoEff != NULL && ttfExtraCuts.at(0)->EvalInstance()) {
							binVar = ttfBinVar.EvalInstance();
							histoEff->Fill(binVar, wTot);
						}
					}
				}
			}

			//passPassCut = ttfPassCut.EvalInstance();

			//if(passPassCut){
			//	for(unsigned int i(0); i<extraCuts.size(); ++i){
			//		passExtraCut = ttfExtraCuts.at(i)->EvalInstance();

			//		if(passExtraCut){
			//			nExtraCuts.at(i).val += wTot;
			//			nExtraCuts.at(i).err += wTot*wTot;
			//		}
			//	}

			//	if(histoEff != NULL && ttfExtraCuts.at(0)->EvalInstance()) {
			//		binVar = ttfBinVar.EvalInstance();
			//		histoEff->Fill(binVar, wTot);
			//	}
			//}
		}
	}

	nExtraCuts.at(0).err = sqrt(nExtraCuts.at(0).err);

	for(int i(1); i<cutSize; ++i) nExtraCuts.at(i).err = sqrt(nExtraCuts.at(i).err);
	nStrip.err = sqrt(nStrip.err);
	nStripKin.err = sqrt(nStripKin.err);
	nTrig.err = sqrt(nTrig.err);
	nSel.err = sqrt(nSel.err);
	nPID.err = sqrt(nPID.err);



	nGlbTIS.err = sqrt(nGlbTIS.err);
	nHTOS.err = sqrt(nHTOS.err);
	nL0.err = sqrt(nL0.err);

	nTrkMVA.err = sqrt(nTrkMVA.err);
	nTwoTrkMVA.err = sqrt(nTwoTrkMVA.err);
	nHLT1.err = sqrt(nHLT1.err);

	nTopo2.err = sqrt(nTopo2.err);
	nTopo3.err = sqrt(nTopo3.err);
	nTopo4.err = sqrt(nTopo4.err);
	nHLT2.err = sqrt(nHLT2.err);

	//compute the total efficiency

	ValError stripRecEff(getRatioWeightedBinomial(nStrip, nGen));
	//cout << "mode: "<< mode << ", nStrip/nGen: " << nStrip.roundToError(0,1)<<"/"<<nGen.roundToError(0,1) << endl;


	if(!wantEfficiencyBreakDown){
		for(int i(0); i<cutSize; ++i) ret.at(i) = geomEff * stripRecEff * getRatioWeightedBinomial(nExtraCuts.at(i), nStripKin);
	}

	if(wantEfficiencyBreakDown){
		//cout<< mode <<" trig"<<trigCat<<endl;
		//cout<<"geom: "<<geomEff<<endl;
		//cout<<"strip: "<<stripRecEff<<endl;
		//cout<<"trig: "<< getRatioWeightedBinomial(nTrig, nStripKin) <<endl;
		//cout<<"presel: "<< getRatioWeightedBinomial(nSel, nTrig) <<endl;
		//cout<<"PID: "<< getRatioWeightedBinomial(nPID, nSel) <<endl;
		//cout<<"range & BDT: "<< getRatioWeightedBinomial(nExtraCuts.at(0), nPID) <<endl;
		//cout<<"TOTAL: "<< ret.at(0)<<endl;

		ret.clear();
		ret.push_back( getRatioWeightedBinomial(nGlbTIS, nStripKin) );									//0
		ret.push_back( getRatioWeightedBinomial(nHTOS  , nStripKin) );									//1
		ret.push_back( getRatioWeightedBinomial(nL0    , nStripKin) );									//2
		ret.push_back( getRatioWeightedBinomial(nTrkMVA   , nL0) );	  								  	//3
		ret.push_back( getRatioWeightedBinomial(nTwoTrkMVA, nL0) );	  								  	//4
		ret.push_back( getRatioWeightedBinomial(nHLT1     , nL0) );	  								  	//5
		ret.push_back( getRatioWeightedBinomial(nTopo2 , nHLT1) );	  								  	//6
		ret.push_back( getRatioWeightedBinomial(nTopo3 , nHLT1) );	  								  	//7
		ret.push_back( getRatioWeightedBinomial(nTopo4 , nHLT1) );	  								  	//8
		ret.push_back( getRatioWeightedBinomial(nHLT2  , nHLT1) );	  								  	//9


		ret.push_back(geomEff);										  								  	//10
		ret.push_back( stripRecEff);								  								  	//11
		//cout << "nTrig/nStrip: "<< nTrig << " / "<< nStrip << endl;
		//cout << "nTrig/nStripKin: "<< nTrig << " / "<< nStripKin << endl;
		ret.push_back( getRatioWeightedBinomial(nTrig, nStripKin) );  								  	//12
		ret.push_back( getRatioWeightedBinomial(nSel, nTrig) );		  								  	//13
		ret.push_back( getRatioWeightedBinomial(nPID, nSel) );		  								  	//14
		ret.push_back( getRatioWeightedBinomial(nExtraCuts.at(0), nPID) );								//15
		ret.push_back( geomEff * stripRecEff * getRatioWeightedBinomial(nExtraCuts.at(0), nStripKin) );	//16

		//printf("nGen:           %-15lf +-   %-15lf\n", nGen.val, nGen.err);
		//printf("nStrip:   %-15lf +-   %-15lf\n", nStrip.val, nStrip.err);
		//printf("nStripKin:     %-15lf +-   %-15lf\n", nStripKin.val, nStripKin.err);
		//printf("nTrig:          %-15lf +-   %-15lf\n", nTrig.val, nTrig.err);
		//printf("nSel:           %-15lf +-   %-15lf\n", nSel.val, nSel.err);
		//printf("nPID:           %-15lf +-   %-15lf\n", nPID.val, nPID.err);
		//printf("nBDT:           %-15lf +-   %-15lf\n", nExtraCuts.at(0).val, nExtraCuts.at(0).err);
	}

	//cout<<"Print effciencies done!"<<endl;

	//if the histoEff is not NULL, set the bins to the correct eff

	if(histoEff != NULL){
		ValError restEffBin(0, 0);
		ValError totEffBin(0, 0);
		ValError veBin(0, 0);
		for(int i(1); i<=histoEff->GetNbinsX(); ++i){
			veBin.val = histoEff->GetBinContent(i);
			veBin.err = histoEff->GetBinError(i);
			restEffBin = getRatioWeightedBinomial(veBin, nStripKin);
			totEffBin = geomEff * stripRecEff * restEffBin; 

			histoEff->SetBinContent(i, totEffBin.val);
			histoEff->SetBinError(i, totEffBin.err);
		}
	}

	//cout<<"Fill histos done!"<<endl;

	for(int i(0); i<cutSize; ++i) delete ttfExtraCuts.at(i);

	return ret;
}



ValError EfficiencyComputer::getEfficiency(
		int year,
		TString mode,
		TString treeRecFileName, TString treeRecName,
		TString nameRecWeight,
		TString namePIDWeight,
		TString nameL0Weight,
		TString nameHltWeight,

		TString extraCut,
		bool wantEfficiencyBreakDown,
		TString binVarName,
		TH1D* histoEff,
		bool debug,
		TString overRiddenPreselection,
		TString varUniquing
		){
	vector<ValError> ret;
	vector<TString> extraCuts(1, extraCut);

	ret =  getEfficiency(
			year,
			mode, 
			treeRecFileName, treeRecName, 
			nameRecWeight, 
			namePIDWeight,
			nameL0Weight,
			nameHltWeight,

			extraCuts, 
			wantEfficiencyBreakDown, 
			binVarName, 
			histoEff, 
			debug, 
			overRiddenPreselection, 
			varUniquing);

	return ret.at(0);
}


//ValError EfficiencyComputer::getDecProdCutEff(TString nameFile, TString preCut, TString nameWeight, TString nameTree){
ValError EfficiencyComputer::getDecProdCutEff(int year, TString mode){
	ValError eff(1, 0);
	vector<vector<ValError>> effs(4);

	//if(!((year>=2015 && year<=2018) || year==-1)) {
	//不计算组合年份的效率，所有组合效率都是分年份然后合起来的
	if(!((year>=2015 && year<=2018) || (year>=2011 && year<=2012))) {
		cerr << "Bad year input in EfficiencyComputer::getDecProdCutEff" << endl;
		return eff;
	}
	if(!(mode=="Bd2DsD" || mode == "Bd2LcLc" || mode == "Bs2DsDs" || mode == "Bs2LcLc")){
		cerr << "Bad mode input in EfficiencyComputer::getDecProdCutEff" << endl;
	}

	vector<vector<double>> num_up(4);
	vector<vector<double>> num_down(4);
	vector<vector<double>> eff_up(4);
	vector<vector<double>> eff_err_up(4);
	vector<vector<double>> eff_down(4);
	vector<vector<double>> eff_err_down(4);

	int modeNumber;
	if(mode == "Bd2DsD") modeNumber = 0;
	if(mode == "Bd2LcLc") modeNumber = 1;
	if(mode == "Bs2DsDs") modeNumber = 2;
	if(mode == "Bs2LcLc") modeNumber = 3;

	// 0 Bd2DsD
	// 1 Bd2LcLc
	// 2 Bs2DsDs
	// 3 Bs2LcLc

	//Bd2DsD 
	num_down[0].push_back(254044);  num_up[0].push_back(257291);
	num_down[0].push_back(529750);  num_up[0].push_back(506609);
    num_down[0].push_back(257178); 	num_up[0].push_back(256193);
    num_down[0].push_back(503273);	num_up[0].push_back(503702);
    num_down[0].push_back(502869);	num_up[0].push_back(510030);
    num_down[0].push_back(502392);	num_up[0].push_back(508290);

	eff_down[0].push_back(0); eff_err_down[0].push_back(0);
	eff_down[0].push_back(0); eff_err_down[0].push_back(0);
    eff_down[0].push_back(0.12555);	eff_err_down[0].push_back(0.00028);
    eff_down[0].push_back(0.12548);	eff_err_down[0].push_back(0.00031);
    eff_down[0].push_back(0.12517);	eff_err_down[0].push_back(0.00030);
    eff_down[0].push_back(0.12566);	eff_err_down[0].push_back(0.00030);

	eff_up[0].push_back(0);   eff_err_up[0].push_back(0);
	eff_up[0].push_back(0);   eff_err_up[0].push_back(0);
    eff_up[0].push_back(0.12523);	eff_err_up[0].push_back(0.00029);
    eff_up[0].push_back(0.12563);	eff_err_up[0].push_back(0.00029);
    eff_up[0].push_back(0.12587);	eff_err_up[0].push_back(0.00029);
    eff_up[0].push_back(0.12505);	eff_err_up[0].push_back(0.00029);

	//Bd2LcLc
	num_down[1].push_back(256704);  num_up[1].push_back(255290);
	num_down[1].push_back(504818);  num_up[1].push_back(501987);
    num_down[1].push_back(252924); 	num_up[1].push_back(256041); 
    num_down[1].push_back(502880);	num_up[1].push_back(505958);
    num_down[1].push_back(507736);	num_up[1].push_back(503672);
    num_down[1].push_back(506413);	num_up[1].push_back(506605);

	eff_down[1].push_back(0); eff_err_down[1].push_back(0);
    eff_down[1].push_back(0); eff_err_down[1].push_back(0);
    eff_down[1].push_back(0.13430);	eff_err_down[1].push_back(0.00030);
    eff_down[1].push_back(0.13376);	eff_err_down[1].push_back(0.00032);
    eff_down[1].push_back(0.13432);	eff_err_down[1].push_back(0.00032);
    eff_down[1].push_back(0.13383);	eff_err_down[1].push_back(0.00032);

    eff_up[1].push_back(0);	eff_err_up[1].push_back(0);
    eff_up[1].push_back(0);	eff_err_up[1].push_back(0);
    eff_up[1].push_back(0.13393);	eff_err_up[1].push_back(0.00030);
    eff_up[1].push_back(0.13455);	eff_err_up[1].push_back(0.00031);
    eff_up[1].push_back(0.13450);	eff_err_up[1].push_back(0.00031);
    eff_up[1].push_back(0.13435);	eff_err_up[1].push_back(0.00031);

	//Bs2DsDs
    num_down[2].push_back(257219); 	num_up[2].push_back(337981);
    num_down[2].push_back(504606); 	num_up[2].push_back(502274);
    num_down[2].push_back(255341); 	num_up[2].push_back(255341);
    num_down[2].push_back(505813);	num_up[2].push_back(504799);
    num_down[2].push_back(505675);	num_up[2].push_back(504420);
    num_down[2].push_back(505044);	num_up[2].push_back(503209);

    eff_down[2].push_back(0);	eff_err_down[2].push_back(0);
    eff_down[2].push_back(0);	eff_err_down[2].push_back(0);
    eff_down[2].push_back(0.1427);	eff_err_down[2].push_back(0.0026);
    eff_down[2].push_back(0.1359);	eff_err_down[2].push_back(0.0018);
    eff_down[2].push_back(0.1385);	eff_err_down[2].push_back(0.0018);
    eff_down[2].push_back(0.1350);	eff_err_down[2].push_back(0.0018);

    eff_up[2].push_back(0);	eff_err_up[2].push_back(0);
    eff_up[2].push_back(0);	eff_err_up[2].push_back(0);
    eff_up[2].push_back(0.1376);	eff_err_up[2].push_back(0.0025);
    eff_up[2].push_back(0.1359);	eff_err_up[2].push_back(0.0018);
    eff_up[2].push_back(0.1355);	eff_err_up[2].push_back(0.0018);
    eff_up[2].push_back(0.1357);	eff_err_up[2].push_back(0.0018);

	//Bs2LcLc
    num_down[3].push_back(354224); 	num_up[3].push_back(250856);
    num_down[3].push_back(505713); 	num_up[3].push_back(500487);
    num_down[3].push_back(252506); 	num_up[3].push_back(251054);
    num_down[3].push_back(505474);	num_up[3].push_back(509839);
    num_down[3].push_back(500246);	num_up[3].push_back(504982);
    num_down[3].push_back(528815);	num_up[3].push_back(506123);

    eff_down[3].push_back(0);	eff_err_down[3].push_back(0);
    eff_down[3].push_back(0);	eff_err_down[3].push_back(0);
    eff_down[3].push_back(0.13378);	eff_err_down[3].push_back(0.00030);
    eff_down[3].push_back(0.13400);	eff_err_down[3].push_back(0.00033);
    eff_down[3].push_back(0.13427);	eff_err_down[3].push_back(0.00031);
    eff_down[3].push_back(0.13435);	eff_err_down[3].push_back(0.00031);

    eff_up[3].push_back(0);	eff_err_up[3].push_back(0);
    eff_up[3].push_back(0);	eff_err_up[3].push_back(0);
    eff_up[3].push_back(0.13453);	eff_err_up[3].push_back(0.00031);
    eff_up[3].push_back(0.13418);	eff_err_up[3].push_back(0.00032);
    eff_up[3].push_back(0.13455);	eff_err_up[3].push_back(0.00032);
    eff_up[3].push_back(0.13428);	eff_err_up[3].push_back(0.00031);

	for(int m=0; m<4; m++){
		for(int y=2011; y<=2012; y++){
			vector <double> tmp = get_gen_eff(
							num_up[m][y-2011], num_down[m][y-2011],
							eff_up[m][y-2011], eff_err_up[m][y-2011],
							eff_down[m][y-2011], eff_err_down[m][y-2011]);
			effs[m].push_back(ValError(tmp[0], tmp[1]));
		}
		for(int y=2015; y<=2018; y++){
			vector <double> tmp = get_gen_eff(
							num_up[m][y-2015+2], num_down[m][y-2015+2],
							eff_up[m][y-2015+2], eff_err_up[m][y-2015+2],
							eff_down[m][y-2015+2], eff_err_down[m][y-2015+2]);
			effs[m].push_back(ValError(tmp[0], tmp[1]));
		}
	}

	if(year>=2015 && year<=2018) eff = effs[modeNumber][year-2015+2];
	else if(year>=2011 && year<=2012) eff = effs[modeNumber][year-2011]; 

	return eff;
}

ValError EfficiencyComputer::getGenLevelNumber(int year, TString mode){
	ValError nGen(0, 0);

	vector<double> entries_Bd2DsD = {511335, 1036359, 513371, 1006975, 1012899, 1010682};
	vector<double> entries_Bd2LcLc = {511994, 1006805, 508965, 1008838, 1011408, 1013018};
	vector<double> entries_Bs2DsDs = {595200, 1006880, 510682, 1010612, 1010095, 1008253};
	vector<double> entries_Bs2LcLc = {605080, 1006200, 503560, 1015313, 1005228, 1034938};
	vector<double> entries;

	//if(!((year>=2015 && year<=2018) || year==-1)) {
	if(!((year>=2015 && year<=2018) || (year>=2011 && year<=2012))) {
		cerr << "Bad input in EfficiencyComputer::getGenLevelNumber" << endl;
		return nGen;
	}
	if(!(mode=="Bd2DsD" || mode == "Bd2LcLc" || mode == "Bs2DsDs" || mode == "Bs2LcLc")){
		cerr << "Bad mode input in EfficiencyComputer::getDecProdCutEff" << endl;
	}


	if(mode == "Bd2DsD") entries = entries_Bd2DsD;
	if(mode == "Bd2LcLc") entries = entries_Bd2LcLc;
	if(mode == "Bs2DsDs") entries = entries_Bs2DsDs;
	if(mode == "Bs2LcLc") entries = entries_Bs2LcLc;


	if(year>=2015 && year<=2018) nGen.val = entries[year-2015 + 2];
	else if(year>=2011 && year<=2012) nGen.val = entries[year-2011];

	return nGen;
}
#endif

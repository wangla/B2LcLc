Year = 'YEAR'
DataOrMC = 'MC_OR_DATA'
MagType = 'MAG_TYPE'
Line="STRIPPING_LINE"
Account= "ACCOUNT"

Input_dict = {
    '2015'   : '/MC/2015/Beam6500GeV-2015-Mag{}-Nu1.6-25ns-Pythia8/Sim09l/Trig0x411400a2/Reco15a/Turbo02/Stripping24r2NoPrescalingFlagged/11296012/ALLSTREAMS.DST'.format(MagType),
    '2016'   : '/MC/2016/Beam6500GeV-2016-Mag{}-Nu1.6-25ns-Pythia8/Sim09l/Trig0x6139160F/Reco16/Turbo03a/Stripping28r2NoPrescalingFlagged/11296012/ALLSTREAMS.DST'.format(MagType),
    '2017'   : '/MC/2017/Beam6500GeV-2017-Mag{}-Nu1.6-25ns-Pythia8/Sim09l/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/11296012/ALLSTREAMS.DST'.format(MagType),
    '2018'   : '/MC/2018/Beam6500GeV-2018-Mag{}-Nu1.6-25ns-Pythia8/Sim09l/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/11296012/ALLSTREAMS.DST'.format(MagType),
}
myJobName = 'Bd2DsD-misID-{0}-{1}-{2}-{3}'.format(Line,Year,DataOrMC,MagType)

myApplication = GaudiExec()
myApplication.directory = "/afs/cern.ch/work/l/lai/DaVinciDev_v44r10p5"
myApplication.platform = 'x86_64-centos7-gcc62-opt'
myApplication.options   = [ 'option.py']

data = BKQuery(Input_dict[Year]).getDataset()

mySplitter = SplitByFiles( filesPerJob = 10, maxFiles = -1, ignoremissing = True, bulksubmit=False )
myBackend = Dirac()
j = Job (
        name         = myJobName,
        application  = myApplication,
        splitter     = mySplitter,
        outputfiles  = [ LocalFile('Tuple.root')],
        backend      = myBackend,
#        inputdata    = data[0:100],
        inputdata    = data,
        do_auto_resubmit = True
        )
j.submit(keep_going=True, keep_on_fail=True)


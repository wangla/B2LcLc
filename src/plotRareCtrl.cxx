#include "/mnt/d/lhcb/B2XcXc/inc/histoTools.h"
#include "/mnt/d/lhcb/B2XcXc/inc/getPresel.h"

void plotRareCtrl(){

	lhcbStyle(0.0603);

	vector<TString> treeNames;
	vector<TString> drawTitles;
	vector<TString> cuts;
	vector<TString> weights;
	vector<int> colors;
	vector<TString> drawOptions;


	vector<int> years;
	years.push_back(-1);

	for(int i=0; i<years.size(); i++){
		int year = years[i];

		TString cutYear = "(1)";
		if(year != -1)	cutYear = "year==" + i2s(year);


		//vector<double> legPos = {0, 0, 0, 0};
		vector<double> legPos = {0.65, 0.65, 0.90, 0.90};
		//vector<double> legPos = {0.25, 0.50, 0.65, 0.90};


		vector<Histo1DWithInfo> Bd2DsD_HistoInfos_corr;
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kminus_Dplus_piplus1_Dplus", "angle_Kminus_Dplus_piplus1_Dplus", "angle_Kminus_Dplus_piplus1_Dplus", 0, 0.004, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kminus_Dplus_piplus2_Dplus", "angle_Kminus_Dplus_piplus2_Dplus", "angle_Kminus_Dplus_piplus2_Dplus", 0, 0.004, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piplus2_Dplus_piplus1_Dplus", "angle_piplus2_Dplus_piplus1_Dplus", "angle_piplus2_Dplus_piplus1_Dplus", 0, 0.004, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kplus_Dsminus_Kminus_Dsminus", "angle_Kplus_Dsminus_Kminus_Dsminus", "angle_Kplus_Dsminus_Kminus_Dsminus", 0, 0.004, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kplus_Dsminus_piminus_Dsminus", "angle_Kplus_Dsminus_piminus_Dsminus", "angle_Kplus_Dsminus_piminus_Dsminus", 0, 0.004, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piminus_Dsminus_Kminus_Dsminus", "angle_piminus_Dsminus_Kminus_Dsminus", "angle_piminus_Dsminus_Kminus_Dsminus", 0, 0.004, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kminus_Dplus_Kplus_Dsminus", "angle_Kminus_Dplus_Kplus_Dsminus", "angle_Kminus_Dplus_Kplus_Dsminus", 0, 0.004, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kminus_Dplus_Kminus_Dsminus", "angle_Kminus_Dplus_Kminus_Dsminus", "angle_Kminus_Dplus_Kminus_Dsminus", 0, 0.004, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kminus_Dplus_piminus_Dsminus", "angle_Kminus_Dplus_piminus_Dsminus", "angle_Kminus_Dplus_piminus_Dsminus", 0, 0.004, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piplus1_Dplus_Kplus_Dsminus", "angle_piplus1_Dplus_Kplus_Dsminus", "angle_piplus1_Dplus_Kplus_Dsminus", 0, 0.004, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piplus1_Dplus_Kminus_Dsminus", "angle_piplus1_Dplus_Kminus_Dsminus", "angle_piplus1_Dplus_Kminus_Dsminus", 0, 0.004, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piplus1_Dplus_piminus_Dsminus", "angle_piplus1_Dplus_piminus_Dsminus", "angle_piplus1_Dplus_piminus_Dsminus", 0, 0.004, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piplus2_Dplus_Kplus_Dsminus", "angle_piplus2_Dplus_Kplus_Dsminus", "angle_piplus2_Dplus_Kplus_Dsminus", 0, 0.004, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piplus2_Dplus_Kminus_Dsminus", "angle_piplus2_Dplus_Kminus_Dsminus", "angle_piplus2_Dplus_Kminus_Dsminus", 0, 0.004, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piplus2_Dplus_piminus_Dsminus", "angle_piplus2_Dplus_piminus_Dsminus", "angle_piplus2_Dplus_piminus_Dsminus", 0, 0.004, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("Kplus_Dsminus_MC15TuneV1_ProbNNk_corr", "Kplus_Dsminus_MC15TuneV1_ProbNNk_corr", "Kplus_Dsminus_MC15TuneV1_ProbNNk_corr", 0.4, 1, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Dsminus_MC15TuneV1_ProbNNk_corr", "Kminus_Dsminus_MC15TuneV1_ProbNNk_corr", "Kminus_Dsminus_MC15TuneV1_ProbNNk_corr", 0.4, 1, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("piminus_Dsminus_MC15TuneV1_ProbNNpi_corr", "piminus_Dsminus_MC15TuneV1_ProbNNpi_corr", "piminus_Dsminus_MC15TuneV1_ProbNNpi_corr", 0.4, 1, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Dplus_MC15TuneV1_ProbNNk_corr", "Kminus_Dplus_MC15TuneV1_ProbNNk_corr", "Kminus_Dplus_MC15TuneV1_ProbNNk_corr", 0.4, 1, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("piplus1_Dplus_MC15TuneV1_ProbNNpi_corr", "piplus1_Dplus_MC15TuneV1_ProbNNpi_corr", "piplus1_Dplus_MC15TuneV1_ProbNNpi_corr", 0.4, 1, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("piplus2_Dplus_MC15TuneV1_ProbNNpi_corr", "piplus2_Dplus_MC15TuneV1_ProbNNpi_corr", "piplus2_Dplus_MC15TuneV1_ProbNNpi_corr", 0.4, 1, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("prodProbNNx_corr", "prodProbNNx", "prodProbNNx", 0.4, 1, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("log_B_IPCHI2_OWNPV", "log(B_IPCHI2_OWNPV)", "log(B_IPCHI2_OWNPV)", -6, 4, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("B_PT", "B_PT", "B_PT", 0, 5e4, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("B_ETA", "B_ETA", "B_ETA", 2, 5, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("nTracks", "nTracks", "nTracks", 0, 500, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("Kplus_Dsminus_PT", "Kplus_Dsminus_PT", "Kplus_Dsminus_PT", 0, 13000, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Dsminus_PT", "Kminus_Dsminus_PT", "Kminus_Dsminus_PT", 0, 13000, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("piminus_Dsminus_PT", "piminus_Dsminus_PT", "piminus_Dsminus_PT", 0, 13000, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Dplus_PT", "Kminus_Dplus_PT", "Kminus_Dplus_PT", 0, 13000, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("piplus1_Dplus_PT", "piplus1_Dplus_PT", "piplus1_Dplus_PT", 0, 13000, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("piplus2_Dplus_PT", "piplus2_Dplus_PT", "piplus2_Dplus_PT", 0, 13000, 50, false));
		Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Dsminus_PT", "Kminus_Dsminus_PT", "#it{p_{T}(K^{-}_{D^{-}_{s}})} / #it{p_{T}(#bar{p}_{#Lambda^{-}_{c}})}", 0, 13000, 50, false));
		Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("Kplus_Dsminus_PT", "Kplus_Dsminus_PT", "#it{p_{T}(K^{+}_{D^{-}_{s}})} / #it{p_{T}(K^{+}_{#Lambda^{-}_{c}})}", 0, 13000, 50, false));
		Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("piminus_Dsminus_PT", "piminus_Dsminus_PT", "#it{p_{T}(#pi^{-}_{D^{-}_{s}})} / #it{p_{T}(#pi^{-}_{#Lambda^{-}_{c}})}", 0, 13000, 50, false));
		Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("piplus2_Dplus_PT", "piplus2_Dplus_PT", "#it{p_{T}(2 #pi^{+}_{D^{+}})} / #it{p_{T}(p_{#Lambda^{+}_{c}})}", 0, 13000, 50, false));
		Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Dplus_PT", "Kminus_Dplus_PT", "#it{p_{T}(K^{-}_{D^{+}})} / #it{p_{T}(K^{-}_{#Lambda^{+}_{c}})}", 0, 13000, 50, false));
		Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("piplus1_Dplus_PT", "piplus1_Dplus_PT", "#it{p_{T}(1 #pi^{+}_{D^{+}})} / #it{p_{T}(#pi^{+}_{#Lambda^{+}_{c}})}", 0, 13000, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("Kplus_Dsminus_ETA", "Kplus_Dsminus_ETA", "Kplus_Dsminus_ETA", 1.5, 5.5, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Dsminus_ETA", "Kminus_Dsminus_ETA", "Kminus_Dsminus_ETA", 1.5, 5.5, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("piminus_Dsminus_ETA", "piminus_Dsminus_ETA", "piminus_Dsminus_ETA", 1.5, 5.5, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Dplus_ETA", "Kminus_Dplus_ETA", "Kminus_Dplus_ETA", 1.5, 5.5, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("piplus1_Dplus_ETA", "piplus1_Dplus_ETA", "piplus1_Dplus_ETA", 1.5, 5.5, 50, false));
		//Bd2DsD_HistoInfos_corr.push_back(Histo1DWithInfo("piplus2_Dplus_ETA", "piplus2_Dplus_ETA", "piplus2_Dplus_ETA", 1.5, 5.5, 50, false));

		vector<Histo1DWithInfo> Bd2LcLc_HistoInfos_corr;
		//Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_pbar_Xc_minus_Kplus_Xc_minus", "angle_pbar_Xc_minus_Kplus_Xc_minus", "angle_pbar_Xc_minus_Kplus_Xc_minus", 0, 0.004, 50, false));
		//Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_pbar_Xc_minus_piminus_Xc_minus", "angle_pbar_Xc_minus_piminus_Xc_minus", "angle_pbar_Xc_minus_piminus_Xc_minus", 0, 0.004, 50, false));
		//Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piminus_Xc_minus_Kplus_Xc_minus", "angle_piminus_Xc_minus_Kplus_Xc_minus", "angle_piminus_Xc_minus_Kplus_Xc_minus", 0, 0.004, 50, false));
		//Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_p_Xc_plus_Kminus_Xc_plus", "angle_p_Xc_plus_Kminus_Xc_plus", "angle_p_Xc_plus_Kminus_Xc_plus", 0, 0.004, 50, false));
		//Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_p_Xc_plus_piplus_Xc_plus", "angle_p_Xc_plus_piplus_Xc_plus", "angle_p_Xc_plus_piplus_Xc_plus", 0, 0.004, 50, false));
		//Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piplus_Xc_plus_Kminus_Xc_plus", "angle_piplus_Xc_plus_Kminus_Xc_plus", "angle_piplus_Xc_plus_Kminus_Xc_plus", 0, 0.004, 50, false));
		//Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_pbar_Xc_minus_p_Xc_plus", "angle_pbar_Xc_minus_p_Xc_plus", "angle_pbar_Xc_minus_p_Xc_plus", 0, 0.004, 50, false));
		//Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_pbar_Xc_minus_Kminus_Xc_plus", "angle_pbar_Xc_minus_Kminus_Xc_plus", "angle_pbar_Xc_minus_Kminus_Xc_plus", 0, 0.004, 50, false));
		//Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_pbar_Xc_minus_piplus_Xc_plus", "angle_pbar_Xc_minus_piplus_Xc_plus", "angle_pbar_Xc_minus_piplus_Xc_plus", 0, 0.004, 50, false));
		//Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kplus_Xc_minus_p_Xc_plus", "angle_Kplus_Xc_minus_p_Xc_plus", "angle_Kplus_Xc_minus_p_Xc_plus", 0, 0.004, 50, false));
		//Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kplus_Xc_minus_Kminus_Xc_plus", "angle_Kplus_Xc_minus_Kminus_Xc_plus", "angle_Kplus_Xc_minus_Kminus_Xc_plus", 0, 0.004, 50, false));
		//Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kplus_Xc_minus_piplus_Xc_plus", "angle_Kplus_Xc_minus_piplus_Xc_plus", "angle_Kplus_Xc_minus_piplus_Xc_plus", 0, 0.004, 50, false));
		//Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piminus_Xc_minus_p_Xc_plus", "angle_piminus_Xc_minus_p_Xc_plus", "angle_piminus_Xc_minus_p_Xc_plus", 0, 0.004, 50, false));
		//Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piminus_Xc_minus_Kminus_Xc_plus", "angle_piminus_Xc_minus_Kminus_Xc_plus", "angle_piminus_Xc_minus_Kminus_Xc_plus", 0, 0.004, 50, false));
		//Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piminus_Xc_minus_piplus_Xc_plus", "angle_piminus_Xc_minus_piplus_Xc_plus", "angle_piminus_Xc_minus_piplus_Xc_plus", 0, 0.004, 50, false));
		//Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("p_Xc_plus_MC15TuneV1_ProbNNk_corr", "p_Xc_plus_MC15TuneV1_ProbNNk_corr", "p_Xc_plus_MC15TuneV1_ProbNNk_corr", 0.4, 1, 50, false));
		//Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Xc_plus_MC15TuneV1_ProbNNk_corr", "Kminus_Xc_plus_MC15TuneV1_ProbNNk_corr", "Kminus_Xc_plus_MC15TuneV1_ProbNNk_corr", 0.4, 1, 50, false));
		//Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("piplus_Xc_plus_MC15TuneV1_ProbNNpi_corr", "piplus_Xc_plus_MC15TuneV1_ProbNNpi_corr", "piplus_Xc_plus_MC15TuneV1_ProbNNpi_corr", 0.4, 1, 50, false));
		//Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("pbar_Xc_minus_MC15TuneV1_ProbNNk_corr", "pbar_Xc_minus_MC15TuneV1_ProbNNk_corr", "pbar_Xc_minus_MC15TuneV1_ProbNNk_corr", 0.4, 1, 50, false));
		//Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("Kplus_Xc_minus_MC15TuneV1_ProbNNpi_corr", "Kplus_Xc_minus_MC15TuneV1_ProbNNpi_corr", "Kplus_Xc_minus_MC15TuneV1_ProbNNpi_corr", 0.4, 1, 50, false));
		//Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("piminus_Xc_minus_MC15TuneV1_ProbNNpi_corr", "piminus_Xc_minus_MC15TuneV1_ProbNNpi_corr", "piminus_Xc_minus_MC15TuneV1_ProbNNpi_corr", 0.4, 1, 50, false));
		//Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("prodProbNNx_corr", "prodProbNNx", "prodProbNNx", 0.4, 1, 50, false));
		//Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("log_B_IPCHI2_OWNPV", "log(B_IPCHI2_OWNPV)", "log(B_IPCHI2_OWNPV)", -6, 4, 50, false));
		//Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("B_PT", "B_PT", "B_PT", 0, 5e4, 50, false));
		//Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("B_ETA", "B_ETA", "B_ETA", 2, 5, 50, false));
		//Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("nTracks", "nTracks", "nTracks", 0, 500, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("pbar_Xc_minus_PT", "pbar_Xc_minus_PT", "pbar_Xc_minus_PT", 0, 13000, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("Kplus_Xc_minus_PT", "Kplus_Xc_minus_PT", "Kplus_Xc_minus_PT", 0, 13000, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("piminus_Xc_minus_PT", "piminus_Xc_minus_PT", "piminus_Xc_minus_PT", 0, 13000, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("p_Xc_plus_PT", "p_Xc_plus_PT", "p_Xc_plus_PT", 0, 13000, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Xc_plus_PT", "Kminus_Xc_plus_PT", "Kminus_Xc_plus_PT", 0, 13000, 50, false));
		Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("piplus_Xc_plus_PT", "piplus_Xc_plus_PT", "piplus_Xc_plus_PT", 0, 13000, 50, false));
		//Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("p_Xc_plus_ETA", "p_Xc_plus_ETA", "p_Xc_plus_ETA", 1.5, 5.5, 50, false));
		//Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Xc_plus_ETA", "Kminus_Xc_plus_ETA", "Kminus_Xc_plus_ETA", 1.5, 5.5, 50, false));
		//Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("piplus_Xc_plus_ETA", "piplus_Xc_plus_ETA", "piplus_Xc_plus_ETA", 1.5, 5.5, 50, false));
		//Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("pbar_Xc_minus_ETA", "pbar_Xc_minus_ETA", "pbar_Xc_minus_ETA", 1.5, 5.5, 50, false));
		//Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("Kplus_Xc_minus_ETA", "Kplus_Xc_minus_ETA", "Kplus_Xc_minus_ETA", 1.5, 5.5, 50, false));
		//Bd2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("piminus_Xc_minus_ETA", "piminus_Xc_minus_ETA", "piminus_Xc_minus_ETA", 1.5, 5.5, 50, false));

		treeNames.clear();
		drawTitles.clear();
		cuts.clear();
		weights.clear();
		colors.clear();
		drawOptions.clear();


		drawTitles.push_back("Bd2DsD MC");
		drawOptions.push_back("E1");

		drawTitles.push_back("Bd2LcLc MC");
		drawOptions.push_back("E1");


		vector<vector<TH1D>> histosBd;
		histosBd.push_back(getVarsHistos1D("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2DsD_Run2_truth_PIDcor_TrigSel_Presel.root", cutYear, "(data_MC_w)", kBlue, Bd2DsD_HistoInfos_corr));
		histosBd.push_back(getVarsHistos1D("/mnt/d/lhcb/B2XcXc/root/Bd/MC/MC_Bd2LcLc_Run2_truth_PIDcor_TrigSel_Presel.root", cutYear, "(data_MC_w)", kGreen, Bd2LcLc_HistoInfos_corr));


		TString plotDir_Bd = "/mnt/d/lhcb/B2XcXc/public_html/plotRareCtrl/Bd";
		if(year != -1) plotDir_Bd += "/every_year/"+i2s(year);

		system("mkdir -p " + plotDir_Bd);
		drawHistos(histosBd,
				Bd2DsD_HistoInfos_corr,
				drawTitles, drawOptions,
				legPos,
				plotDir_Bd);

		//if(year == -1){
			// log plots
			plotDir_Bd = "/mnt/d/lhcb/B2XcXc/public_html/plotRareCtrl/Bd/log_plots";
			if(year != -1) plotDir_Bd += "/every_year/"+i2s(year);

			for(int i=0; i<Bd2DsD_HistoInfos_corr.size(); i++){
				Bd2DsD_HistoInfos_corr[i].setLog = true;
			}
			drawHistos(histosBd,
				Bd2DsD_HistoInfos_corr,
				drawTitles, drawOptions,
				legPos,
				plotDir_Bd);
		//}



		vector<Histo1DWithInfo> Bs2DsDs_HistoInfos_corr;
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kminus_Dsplus_piplus_Dsplus", "angle_Kminus_Dsplus_piplus_Dsplus", "angle_Kminus_Dsplus_piplus_Dsplus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kminus_Dsplus_Kplus_Dsplus", "angle_Kminus_Dsplus_Kplus_Dsplus", "angle_Kminus_Dsplus_Kplus_Dsplus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kplus_Dsplus_piplus_Dsplus", "angle_Kplus_Dsplus_piplus_Dsplus", "angle_Kplus_Dsplus_piplus_Dsplus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kplus_Dsminus_Kminus_Dsminus", "angle_Kplus_Dsminus_Kminus_Dsminus", "angle_Kplus_Dsminus_Kminus_Dsminus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kplus_Dsminus_piminus_Dsminus", "angle_Kplus_Dsminus_piminus_Dsminus", "angle_Kplus_Dsminus_piminus_Dsminus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piminus_Dsminus_Kminus_Dsminus", "angle_piminus_Dsminus_Kminus_Dsminus", "angle_piminus_Dsminus_Kminus_Dsminus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kminus_Dsplus_Kplus_Dsminus", "angle_Kminus_Dsplus_Kplus_Dsminus", "angle_Kminus_Dsplus_Kplus_Dsminus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kminus_Dsplus_Kminus_Dsminus", "angle_Kminus_Dsplus_Kminus_Dsminus", "angle_Kminus_Dsplus_Kminus_Dsminus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kminus_Dsplus_piminus_Dsminus", "angle_Kminus_Dsplus_piminus_Dsminus", "angle_Kminus_Dsplus_piminus_Dsminus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piplus_Dsplus_Kplus_Dsminus", "angle_piplus_Dsplus_Kplus_Dsminus", "angle_piplus_Dsplus_Kplus_Dsminus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piplus_Dsplus_Kminus_Dsminus", "angle_piplus_Dsplus_Kminus_Dsminus", "angle_piplus_Dsplus_Kminus_Dsminus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piplus_Dsplus_piminus_Dsminus", "angle_piplus_Dsplus_piminus_Dsminus", "angle_piplus_Dsplus_piminus_Dsminus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kplus_Dsplus_Kplus_Dsminus", "angle_Kplus_Dsplus_Kplus_Dsminus", "angle_Kplus_Dsplus_Kplus_Dsminus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kplus_Dsplus_Kminus_Dsminus", "angle_Kplus_Dsplus_Kminus_Dsminus", "angle_Kplus_Dsplus_Kminus_Dsminus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kplus_Dsplus_piminus_Dsminus", "angle_Kplus_Dsplus_piminus_Dsminus", "angle_Kplus_Dsplus_piminus_Dsminus", 0, 0.004, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("Kplus_Dsminus_MC15TuneV1_ProbNNk_corr", "Kplus_Dsminus_MC15TuneV1_ProbNNk_corr", "Kplus_Dsminus_MC15TuneV1_ProbNNk_corr", 0.4, 1, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Dsminus_MC15TuneV1_ProbNNk_corr", "Kminus_Dsminus_MC15TuneV1_ProbNNk_corr", "Kminus_Dsminus_MC15TuneV1_ProbNNk_corr", 0.4, 1, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("piminus_Dsminus_MC15TuneV1_ProbNNpi_corr", "piminus_Dsminus_MC15TuneV1_ProbNNpi_corr", "piminus_Dsminus_MC15TuneV1_ProbNNpi_corr", 0.4, 1, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Dsplus_MC15TuneV1_ProbNNk_corr", "Kminus_Dsplus_MC15TuneV1_ProbNNk_corr", "Kminus_Dsplus_MC15TuneV1_ProbNNk_corr", 0.4, 1, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("piplus_Dsplus_MC15TuneV1_ProbNNpi_corr", "piplus_Dsplus_MC15TuneV1_ProbNNpi_corr", "piplus_Dsplus_MC15TuneV1_ProbNNpi_corr", 0.4, 1, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("Kplus_Dsplus_MC15TuneV1_ProbNNpi_corr", "Kplus_Dsplus_MC15TuneV1_ProbNNpi_corr", "Kplus_Dsplus_MC15TuneV1_ProbNNpi_corr", 0.4, 1, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("prodProbNNx_corr", "prodProbNNx", "prodProbNNx", 0.4, 1, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("log_B_IPCHI2_OWNPV", "log(B_IPCHI2_OWNPV)", "log(B_IPCHI2_OWNPV)", -6, 4, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("B_PT", "B_PT", "B_PT", 0, 5e4, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("B_ETA", "B_ETA", "B_ETA", 2, 5, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("nTracks", "nTracks", "nTracks", 0, 500, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("Kplus_Dsminus_PT", "Kplus_Dsminus_PT", "Kplus_Dsminus_PT", 0, 13000, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Dsminus_PT", "Kminus_Dsminus_PT", "Kminus_Dsminus_PT", 0, 13000, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("piminus_Dsminus_PT", "piminus_Dsminus_PT", "piminus_Dsminus_PT", 0, 13000, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Dsplus_PT", "Kminus_Dsplus_PT", "Kminus_Dsplus_PT", 0, 13000, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("piplus_Dsplus_PT", "piplus_Dsplus_PT", "piplus_Dsplus_PT", 0, 13000, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("Kplus_Dsplus_PT", "Kplus_Dsplus_PT", "Kplus_Dsplus_PT", 0, 13000, 50, false));
		Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Dsminus_PT", "Kminus_Dsminus_PT", "#it{p_{T}(K^{-}_{D^{-}_{s}})} / #it{p_{T}(#bar{p}_{#Lambda^{-}_{c}})}", 0, 13000, 50, false));
		Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("Kplus_Dsminus_PT", "Kplus_Dsminus_PT", "#it{p_{T}(K^{+}_{D^{-}_{s}})} / #it{p_{T}(K^{+}_{#Lambda^{-}_{c}})}", 0, 13000, 50, false));
		Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("piminus_Dsminus_PT", "piminus_Dsminus_PT", "#it{p_{T}(#pi^{-}_{D^{-}_{s}})} / #it{p_{T}(#pi^{-}_{#Lambda^{-}_{c}})}", 0, 13000, 50, false));
		Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("Kplus_Dsplus_PT", "Kplus_Dsplus_PT", "#it{p_{T}(2 #pi^{+}_{D^{+}})} / #it{p_{T}(p_{#Lambda^{+}_{c}})}", 0, 13000, 50, false));
		Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Dsplus_PT", "Kminus_Dsplus_PT", "#it{p_{T}(K^{-}_{D^{+}})} / #it{p_{T}(K^{-}_{#Lambda^{+}_{c}})}", 0, 13000, 50, false));
		Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("piplus_Dsplus_PT", "piplus_Dsplus_PT", "#it{p_{T}(1 #pi^{+}_{D^{+}})} / #it{p_{T}(#pi^{+}_{#Lambda^{+}_{c}})}", 0, 13000, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("Kplus_Dsminus_ETA", "Kplus_Dsminus_ETA", "Kplus_Dsminus_ETA", 1.5, 5.5, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Dsminus_ETA", "Kminus_Dsminus_ETA", "Kminus_Dsminus_ETA", 1.5, 5.5, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("piminus_Dsminus_ETA", "piminus_Dsminus_ETA", "piminus_Dsminus_ETA", 1.5, 5.5, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Dsplus_ETA", "Kminus_Dsplus_ETA", "Kminus_Dsplus_ETA", 1.5, 5.5, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("piplus_Dsplus_ETA", "piplus_Dsplus_ETA", "piplus_Dsplus_ETA", 1.5, 5.5, 50, false));
		//Bs2DsDs_HistoInfos_corr.push_back(Histo1DWithInfo("Kplus_Dsplus_ETA", "Kplus_Dsplus_ETA", "Kplus_Dsplus_ETA", 1.5, 5.5, 50, false));

		vector<Histo1DWithInfo> Bs2LcLc_HistoInfos_corr;
		//Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_pbar_Xc_minus_Kplus_Xc_minus", "angle_pbar_Xc_minus_Kplus_Xc_minus", "angle_pbar_Xc_minus_Kplus_Xc_minus", 0, 0.004, 50, false));
		//Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_pbar_Xc_minus_piminus_Xc_minus", "angle_pbar_Xc_minus_piminus_Xc_minus", "angle_pbar_Xc_minus_piminus_Xc_minus", 0, 0.004, 50, false));
		//Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piminus_Xc_minus_Kplus_Xc_minus", "angle_piminus_Xc_minus_Kplus_Xc_minus", "angle_piminus_Xc_minus_Kplus_Xc_minus", 0, 0.004, 50, false));
		//Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_p_Xc_plus_Kminus_Xc_plus", "angle_p_Xc_plus_Kminus_Xc_plus", "angle_p_Xc_plus_Kminus_Xc_plus", 0, 0.004, 50, false));
		//Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_p_Xc_plus_piplus_Xc_plus", "angle_p_Xc_plus_piplus_Xc_plus", "angle_p_Xc_plus_piplus_Xc_plus", 0, 0.004, 50, false));
		//Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piplus_Xc_plus_Kminus_Xc_plus", "angle_piplus_Xc_plus_Kminus_Xc_plus", "angle_piplus_Xc_plus_Kminus_Xc_plus", 0, 0.004, 50, false));
		//Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_pbar_Xc_minus_p_Xc_plus", "angle_pbar_Xc_minus_p_Xc_plus", "angle_pbar_Xc_minus_p_Xc_plus", 0, 0.004, 50, false));
		//Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_pbar_Xc_minus_Kminus_Xc_plus", "angle_pbar_Xc_minus_Kminus_Xc_plus", "angle_pbar_Xc_minus_Kminus_Xc_plus", 0, 0.004, 50, false));
		//Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_pbar_Xc_minus_piplus_Xc_plus", "angle_pbar_Xc_minus_piplus_Xc_plus", "angle_pbar_Xc_minus_piplus_Xc_plus", 0, 0.004, 50, false));
		//Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kplus_Xc_minus_p_Xc_plus", "angle_Kplus_Xc_minus_p_Xc_plus", "angle_Kplus_Xc_minus_p_Xc_plus", 0, 0.004, 50, false));
		//Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kplus_Xc_minus_Kminus_Xc_plus", "angle_Kplus_Xc_minus_Kminus_Xc_plus", "angle_Kplus_Xc_minus_Kminus_Xc_plus", 0, 0.004, 50, false));
		//Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_Kplus_Xc_minus_piplus_Xc_plus", "angle_Kplus_Xc_minus_piplus_Xc_plus", "angle_Kplus_Xc_minus_piplus_Xc_plus", 0, 0.004, 50, false));
		//Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piminus_Xc_minus_p_Xc_plus", "angle_piminus_Xc_minus_p_Xc_plus", "angle_piminus_Xc_minus_p_Xc_plus", 0, 0.004, 50, false));
		//Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piminus_Xc_minus_Kminus_Xc_plus", "angle_piminus_Xc_minus_Kminus_Xc_plus", "angle_piminus_Xc_minus_Kminus_Xc_plus", 0, 0.004, 50, false));
		//Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("angle_piminus_Xc_minus_piplus_Xc_plus", "angle_piminus_Xc_minus_piplus_Xc_plus", "angle_piminus_Xc_minus_piplus_Xc_plus", 0, 0.004, 50, false));
		//Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("p_Xc_plus_MC15TuneV1_ProbNNk_corr", "p_Xc_plus_MC15TuneV1_ProbNNk_corr", "p_Xc_plus_MC15TuneV1_ProbNNk_corr", 0.4, 1, 50, false));
		//Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Xc_plus_MC15TuneV1_ProbNNk_corr", "Kminus_Xc_plus_MC15TuneV1_ProbNNk_corr", "Kminus_Xc_plus_MC15TuneV1_ProbNNk_corr", 0.4, 1, 50, false));
		//Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("piplus_Xc_plus_MC15TuneV1_ProbNNpi_corr", "piplus_Xc_plus_MC15TuneV1_ProbNNpi_corr", "piplus_Xc_plus_MC15TuneV1_ProbNNpi_corr", 0.4, 1, 50, false));
		//Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("pbar_Xc_minus_MC15TuneV1_ProbNNk_corr", "pbar_Xc_minus_MC15TuneV1_ProbNNk_corr", "pbar_Xc_minus_MC15TuneV1_ProbNNk_corr", 0.4, 1, 50, false));
		//Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("Kplus_Xc_minus_MC15TuneV1_ProbNNpi_corr", "Kplus_Xc_minus_MC15TuneV1_ProbNNpi_corr", "Kplus_Xc_minus_MC15TuneV1_ProbNNpi_corr", 0.4, 1, 50, false));
		//Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("piminus_Xc_minus_MC15TuneV1_ProbNNpi_corr", "piminus_Xc_minus_MC15TuneV1_ProbNNpi_corr", "piminus_Xc_minus_MC15TuneV1_ProbNNpi_corr", 0.4, 1, 50, false));
		//Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("prodProbNNx_corr", "prodProbNNx", "prodProbNNx", 0.4, 1, 50, false));
		//Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("log_B_IPCHI2_OWNPV", "log(B_IPCHI2_OWNPV)", "log(B_IPCHI2_OWNPV)", -6, 4, 50, false));
		//Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("B_PT", "B_PT", "B_PT", 0, 5e4, 50, false));
		//Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("B_ETA", "B_ETA", "B_ETA", 2, 5, 50, false));
		//Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("nTracks", "nTracks", "nTracks", 0, 500, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("pbar_Xc_minus_PT", "pbar_Xc_minus_PT", "pbar_Xc_minus_PT", 0, 13000, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("Kplus_Xc_minus_PT", "Kplus_Xc_minus_PT", "Kplus_Xc_minus_PT", 0, 13000, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("piminus_Xc_minus_PT", "piminus_Xc_minus_PT", "piminus_Xc_minus_PT", 0, 13000, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("p_Xc_plus_PT", "p_Xc_plus_PT", "p_Xc_plus_PT", 0, 13000, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Xc_plus_PT", "Kminus_Xc_plus_PT", "Kminus_Xc_plus_PT", 0, 13000, 50, false));
		Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("piplus_Xc_plus_PT", "piplus_Xc_plus_PT", "piplus_Xc_plus_PT", 0, 13000, 50, false));
		//Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("p_Xc_plus_ETA", "p_Xc_plus_ETA", "p_Xc_plus_ETA", 1.5, 5.5, 50, false));
		//Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("Kminus_Xc_plus_ETA", "Kminus_Xc_plus_ETA", "Kminus_Xc_plus_ETA", 1.5, 5.5, 50, false));
		//Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("piplus_Xc_plus_ETA", "piplus_Xc_plus_ETA", "piplus_Xc_plus_ETA", 1.5, 5.5, 50, false));
		//Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("pbar_Xc_minus_ETA", "pbar_Xc_minus_ETA", "pbar_Xc_minus_ETA", 1.5, 5.5, 50, false));
		//Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("Kplus_Xc_minus_ETA", "Kplus_Xc_minus_ETA", "Kplus_Xc_minus_ETA", 1.5, 5.5, 50, false));
		//Bs2LcLc_HistoInfos_corr.push_back(Histo1DWithInfo("piminus_Xc_minus_ETA", "piminus_Xc_minus_ETA", "piminus_Xc_minus_ETA", 1.5, 5.5, 50, false));

		treeNames.clear();
		drawTitles.clear();
		cuts.clear();
		weights.clear();
		colors.clear();
		drawOptions.clear();


		drawTitles.push_back("Bs2DsDs MC");
		drawOptions.push_back("E1");

		drawTitles.push_back("Bs2LcLc MC");
		drawOptions.push_back("E1");


		vector<vector<TH1D>> histosBs;
		histosBs.push_back(getVarsHistos1D("/mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bs2DsDs_Run2_truth_PIDcor_TrigSel_Presel.root", cutYear, "(data_MC_w)", kBlue, Bs2DsDs_HistoInfos_corr));
		histosBs.push_back(getVarsHistos1D("/mnt/d/lhcb/B2XcXc/root/Bs/MC/MC_Bs2LcLc_Run2_truth_PIDcor_TrigSel_Presel.root", cutYear, "(data_MC_w)", kGreen, Bs2LcLc_HistoInfos_corr));


		TString plotDir_Bs = "/mnt/d/lhcb/B2XcXc/public_html/plotRareCtrl/Bs";
		if(year != -1) plotDir_Bs += "/every_year/"+i2s(year);

		system("mkdir -p " + plotDir_Bs);
		drawHistos(histosBs,
				Bs2DsDs_HistoInfos_corr,
				drawTitles, drawOptions,
				legPos,
				plotDir_Bs);

		//if(year == -1){
			// log plots
			plotDir_Bs = "/mnt/d/lhcb/B2XcXc/public_html/plotRareCtrl/Bs/log_plots";
			if(year != -1) plotDir_Bs += "/every_year/"+i2s(year);

			for(int i=0; i<Bs2DsDs_HistoInfos_corr.size(); i++){
				Bs2DsDs_HistoInfos_corr[i].setLog = true;
			}
			drawHistos(histosBs,
				Bs2DsDs_HistoInfos_corr,
				drawTitles, drawOptions,
				legPos,
				plotDir_Bs);
		//}
	}
	gROOT->ProcessLine(".q");
}

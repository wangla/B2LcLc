//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Mar  2 12:32:03 2023 by ROOT version 6.22/08
// from TTree DecayTree/DecayTree
// found on file: /mnt/d/lhcb/B2XcXc/root/Bd/data/Data_Bd2DsD_Run2_TriggerSel.root
//////////////////////////////////////////////////////////

#ifndef DecayTree_h
#define DecayTree_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class DecayTree {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.
   static constexpr Int_t kMaxB_ENDVERTEX_COV = 1;
   static constexpr Int_t kMaxB_OWNPV_COV = 1;
   static constexpr Int_t kMaxDsminus_ENDVERTEX_COV = 1;
   static constexpr Int_t kMaxDsminus_OWNPV_COV = 1;
   static constexpr Int_t kMaxDsminus_ORIVX_COV = 1;
   static constexpr Int_t kMaxKplus_Dsminus_OWNPV_COV = 1;
   static constexpr Int_t kMaxKplus_Dsminus_ORIVX_COV = 1;
   static constexpr Int_t kMaxKminus_Dsminus_OWNPV_COV = 1;
   static constexpr Int_t kMaxKminus_Dsminus_ORIVX_COV = 1;
   static constexpr Int_t kMaxpiminus_Dsminus_OWNPV_COV = 1;
   static constexpr Int_t kMaxpiminus_Dsminus_ORIVX_COV = 1;
   static constexpr Int_t kMaxDplus_ENDVERTEX_COV = 1;
   static constexpr Int_t kMaxDplus_OWNPV_COV = 1;
   static constexpr Int_t kMaxDplus_ORIVX_COV = 1;
   static constexpr Int_t kMaxKminus_Dplus_OWNPV_COV = 1;
   static constexpr Int_t kMaxKminus_Dplus_ORIVX_COV = 1;
   static constexpr Int_t kMaxpiplus1_Dplus_OWNPV_COV = 1;
   static constexpr Int_t kMaxpiplus1_Dplus_ORIVX_COV = 1;
   static constexpr Int_t kMaxpiplus2_Dplus_OWNPV_COV = 1;
   static constexpr Int_t kMaxpiplus2_Dplus_ORIVX_COV = 1;

   // Declaration of leaf types
   Double_t        B_ETA;
   Double_t        B_MinIPCHI2;
   Double_t        B_DiraAngleError;
   Double_t        B_DiraCosError;
   Double_t        B_DiraAngle;
   Double_t        B_DiraCos;
   Double_t        B_ENDVERTEX_X;
   Double_t        B_ENDVERTEX_Y;
   Double_t        B_ENDVERTEX_Z;
   Double_t        B_ENDVERTEX_XERR;
   Double_t        B_ENDVERTEX_YERR;
   Double_t        B_ENDVERTEX_ZERR;
   Double_t        B_ENDVERTEX_CHI2;
   Int_t           B_ENDVERTEX_NDOF;
   Float_t         B_ENDVERTEX_COV_[3][3];
   Double_t        B_OWNPV_X;
   Double_t        B_OWNPV_Y;
   Double_t        B_OWNPV_Z;
   Double_t        B_OWNPV_XERR;
   Double_t        B_OWNPV_YERR;
   Double_t        B_OWNPV_ZERR;
   Double_t        B_OWNPV_CHI2;
   Int_t           B_OWNPV_NDOF;
   Float_t         B_OWNPV_COV_[3][3];
   Double_t        B_IP_OWNPV;
   Double_t        B_IPCHI2_OWNPV;
   Double_t        B_FD_OWNPV;
   Double_t        B_FDCHI2_OWNPV;
   Double_t        B_DIRA_OWNPV;
   Double_t        B_P;
   Double_t        B_PT;
   Double_t        B_PE;
   Double_t        B_PX;
   Double_t        B_PY;
   Double_t        B_PZ;
   Double_t        B_REFPX;
   Double_t        B_REFPY;
   Double_t        B_REFPZ;
   Double_t        B_MM;
   Double_t        B_MMERR;
   Double_t        B_M;
   Int_t           B_BKGCAT;
   Int_t           B_TRUEID;
   Double_t        B_TRUEP_E;
   Double_t        B_TRUEP_X;
   Double_t        B_TRUEP_Y;
   Double_t        B_TRUEP_Z;
   Double_t        B_TRUEPT;
   Double_t        B_TRUEORIGINVERTEX_X;
   Double_t        B_TRUEORIGINVERTEX_Y;
   Double_t        B_TRUEORIGINVERTEX_Z;
   Double_t        B_TRUEENDVERTEX_X;
   Double_t        B_TRUEENDVERTEX_Y;
   Double_t        B_TRUEENDVERTEX_Z;
   Bool_t          B_TRUEISSTABLE;
   Double_t        B_TRUETAU;
   Int_t           B_ID;
   Double_t        B_TAU;
   Double_t        B_TAUERR;
   Double_t        B_TAUCHI2;
   Bool_t          B_L0Global_Dec;
   Bool_t          B_L0Global_TIS;
   Bool_t          B_L0Global_TOS;
   Bool_t          B_Hlt1Global_Dec;
   Bool_t          B_Hlt1Global_TIS;
   Bool_t          B_Hlt1Global_TOS;
   Bool_t          B_Hlt1Phys_Dec;
   Bool_t          B_Hlt1Phys_TIS;
   Bool_t          B_Hlt1Phys_TOS;
   Bool_t          B_Hlt2Global_Dec;
   Bool_t          B_Hlt2Global_TIS;
   Bool_t          B_Hlt2Global_TOS;
   Bool_t          B_Hlt2Phys_Dec;
   Bool_t          B_Hlt2Phys_TIS;
   Bool_t          B_Hlt2Phys_TOS;
   Bool_t          B_L0HadronDecision_Dec;
   Bool_t          B_L0HadronDecision_TIS;
   Bool_t          B_L0HadronDecision_TOS;
   Bool_t          B_L0ElectronDecision_Dec;
   Bool_t          B_L0ElectronDecision_TIS;
   Bool_t          B_L0ElectronDecision_TOS;
   Bool_t          B_L0ElectronHiDecision_Dec;
   Bool_t          B_L0ElectronHiDecision_TIS;
   Bool_t          B_L0ElectronHiDecision_TOS;
   Bool_t          B_L0MuonDecision_Dec;
   Bool_t          B_L0MuonDecision_TIS;
   Bool_t          B_L0MuonDecision_TOS;
   Bool_t          B_L0DiMuonDecision_Dec;
   Bool_t          B_L0DiMuonDecision_TIS;
   Bool_t          B_L0DiMuonDecision_TOS;
   Bool_t          B_L0MuonHighDecision_Dec;
   Bool_t          B_L0MuonHighDecision_TIS;
   Bool_t          B_L0MuonHighDecision_TOS;
   Bool_t          B_Hlt1TrackMVADecision_Dec;
   Bool_t          B_Hlt1TrackMVADecision_TIS;
   Bool_t          B_Hlt1TrackMVADecision_TOS;
   Bool_t          B_Hlt1TwoTrackMVADecision_Dec;
   Bool_t          B_Hlt1TwoTrackMVADecision_TIS;
   Bool_t          B_Hlt1TwoTrackMVADecision_TOS;
   Bool_t          B_Hlt1GlobalDecision_Dec;
   Bool_t          B_Hlt1GlobalDecision_TIS;
   Bool_t          B_Hlt1GlobalDecision_TOS;
   Bool_t          B_Hlt1TrackAllL0Decision_Dec;
   Bool_t          B_Hlt1TrackAllL0Decision_TIS;
   Bool_t          B_Hlt1TrackAllL0Decision_TOS;
   Bool_t          B_Hlt2Topo2BodyBBDTDecision_Dec;
   Bool_t          B_Hlt2Topo2BodyBBDTDecision_TIS;
   Bool_t          B_Hlt2Topo2BodyBBDTDecision_TOS;
   Bool_t          B_Hlt2Topo3BodyBBDTDecision_Dec;
   Bool_t          B_Hlt2Topo3BodyBBDTDecision_TIS;
   Bool_t          B_Hlt2Topo3BodyBBDTDecision_TOS;
   Bool_t          B_Hlt2Topo4BodyBBDTDecision_Dec;
   Bool_t          B_Hlt2Topo4BodyBBDTDecision_TIS;
   Bool_t          B_Hlt2Topo4BodyBBDTDecision_TOS;
   Bool_t          B_Hlt2BHadB02PpPpPmPmDecision_Dec;
   Bool_t          B_Hlt2BHadB02PpPpPmPmDecision_TIS;
   Bool_t          B_Hlt2BHadB02PpPpPmPmDecision_TOS;
   Bool_t          B_Hlt2Topo2BodyDecision_Dec;
   Bool_t          B_Hlt2Topo2BodyDecision_TIS;
   Bool_t          B_Hlt2Topo2BodyDecision_TOS;
   Bool_t          B_Hlt2Topo3BodyDecision_Dec;
   Bool_t          B_Hlt2Topo3BodyDecision_TIS;
   Bool_t          B_Hlt2Topo3BodyDecision_TOS;
   Bool_t          B_Hlt2Topo4BodyDecision_Dec;
   Bool_t          B_Hlt2Topo4BodyDecision_TIS;
   Bool_t          B_Hlt2Topo4BodyDecision_TOS;
   Bool_t          B_Hlt2GlobalDecision_Dec;
   Bool_t          B_Hlt2GlobalDecision_TIS;
   Bool_t          B_Hlt2GlobalDecision_TOS;
   Double_t        B_X;
   Double_t        B_Y;
   Double_t        B_DTFDict_B_DIRA_OWNPV;
   Double_t        B_DTFDict_B_E;
   Double_t        B_DTFDict_B_ENDVERTEX_CHI2;
   Double_t        B_DTFDict_B_ENDVERTEX_NDOF;
   Double_t        B_DTFDict_B_FDCHI2_OWNPV;
   Double_t        B_DTFDict_B_M;
   Double_t        B_DTFDict_B_MINIPCHI2;
   Double_t        B_DTFDict_B_PT;
   Double_t        B_DTFDict_B_PX;
   Double_t        B_DTFDict_B_PY;
   Double_t        B_DTFDict_B_PZ;
   Double_t        B_DTFDict_Dplus_DIRA_OWNPV;
   Double_t        B_DTFDict_Dplus_E;
   Double_t        B_DTFDict_Dplus_ENDVERTEX_CHI2;
   Double_t        B_DTFDict_Dplus_ENDVERTEX_NDOF;
   Double_t        B_DTFDict_Dplus_FDCHI2_OWNPV;
   Double_t        B_DTFDict_Dplus_M;
   Double_t        B_DTFDict_Dplus_MINIPCHI2;
   Double_t        B_DTFDict_Dplus_MM;
   Double_t        B_DTFDict_Dplus_PT;
   Double_t        B_DTFDict_Dplus_PX;
   Double_t        B_DTFDict_Dplus_PY;
   Double_t        B_DTFDict_Dplus_PZ;
   Double_t        B_DTFDict_Dsminus_DIRA_OWNPV;
   Double_t        B_DTFDict_Dsminus_E;
   Double_t        B_DTFDict_Dsminus_ENDVERTEX_CHI2;
   Double_t        B_DTFDict_Dsminus_ENDVERTEX_NDOF;
   Double_t        B_DTFDict_Dsminus_FDCHI2_OWNPV;
   Double_t        B_DTFDict_Dsminus_M;
   Double_t        B_DTFDict_Dsminus_MINIPCHI2;
   Double_t        B_DTFDict_Dsminus_MM;
   Double_t        B_DTFDict_Dsminus_PT;
   Double_t        B_DTFDict_Dsminus_PX;
   Double_t        B_DTFDict_Dsminus_PY;
   Double_t        B_DTFDict_Dsminus_PZ;
   Double_t        B_DTF_CHI2;
   Double_t        B_DTF_NDOF;
   Int_t           B_DTF_M_nPV;
   Float_t         B_DTF_M_D_splus_Kplus_0_ID[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_D_splus_Kplus_0_PE[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_D_splus_Kplus_0_PX[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_D_splus_Kplus_0_PY[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_D_splus_Kplus_0_PZ[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_D_splus_Kplus_ID[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_D_splus_Kplus_PE[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_D_splus_Kplus_PX[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_D_splus_Kplus_PY[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_D_splus_Kplus_PZ[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_D_splus_M[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_D_splus_MERR[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_D_splus_P[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_D_splus_PERR[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_D_splus_ctau[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_D_splus_ctauErr[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_D_splus_decayLength[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_D_splus_decayLengthErr[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_D_splus_piplus_ID[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_D_splus_piplus_PE[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_D_splus_piplus_PX[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_D_splus_piplus_PY[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_D_splus_piplus_PZ[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_Dplus_Kplus_ID[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_Dplus_Kplus_PE[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_Dplus_Kplus_PX[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_Dplus_Kplus_PY[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_Dplus_Kplus_PZ[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_Dplus_M[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_Dplus_MERR[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_Dplus_P[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_Dplus_PERR[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_Dplus_ctau[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_Dplus_ctauErr[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_Dplus_decayLength[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_Dplus_decayLengthErr[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_Dplus_piplus_0_ID[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_Dplus_piplus_0_PE[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_Dplus_piplus_0_PX[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_Dplus_piplus_0_PY[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_Dplus_piplus_0_PZ[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_Dplus_piplus_ID[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_Dplus_piplus_PE[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_Dplus_piplus_PX[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_Dplus_piplus_PY[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_Dplus_piplus_PZ[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_M[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_MERR[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_P[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_PERR[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_chi2[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_nDOF[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_nIter[100];   //[B_DTF_M_nPV]
   Float_t         B_DTF_M_status[100];   //[B_DTF_M_nPV]
   Int_t           B_DTF_M_PVC_nPV;
   Float_t         B_DTF_M_PVC_D_splus_Kplus_0_ID[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_D_splus_Kplus_0_PE[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_D_splus_Kplus_0_PX[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_D_splus_Kplus_0_PY[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_D_splus_Kplus_0_PZ[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_D_splus_Kplus_ID[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_D_splus_Kplus_PE[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_D_splus_Kplus_PX[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_D_splus_Kplus_PY[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_D_splus_Kplus_PZ[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_D_splus_M[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_D_splus_MERR[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_D_splus_P[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_D_splus_PERR[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_D_splus_ctau[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_D_splus_ctauErr[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_D_splus_decayLength[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_D_splus_decayLengthErr[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_D_splus_piplus_ID[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_D_splus_piplus_PE[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_D_splus_piplus_PX[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_D_splus_piplus_PY[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_D_splus_piplus_PZ[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_Dplus_Kplus_ID[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_Dplus_Kplus_PE[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_Dplus_Kplus_PX[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_Dplus_Kplus_PY[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_Dplus_Kplus_PZ[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_Dplus_M[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_Dplus_MERR[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_Dplus_P[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_Dplus_PERR[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_Dplus_ctau[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_Dplus_ctauErr[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_Dplus_decayLength[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_Dplus_decayLengthErr[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_Dplus_piplus_0_ID[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_Dplus_piplus_0_PE[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_Dplus_piplus_0_PX[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_Dplus_piplus_0_PY[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_Dplus_piplus_0_PZ[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_Dplus_piplus_ID[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_Dplus_piplus_PE[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_Dplus_piplus_PX[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_Dplus_piplus_PY[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_Dplus_piplus_PZ[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_M[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_MERR[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_P[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_PERR[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_PV_X[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_PV_Y[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_PV_Z[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_PV_key[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_chi2[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_ctau[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_ctauErr[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_decayLength[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_decayLengthErr[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_nDOF[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_nIter[100];   //[B_DTF_M_PVC_nPV]
   Float_t         B_DTF_M_PVC_status[100];   //[B_DTF_M_PVC_nPV]
   Int_t           B_PVC_nPV;
   Float_t         B_PVC_D_splus_Kplus_0_ID[100];   //[B_PVC_nPV]
   Float_t         B_PVC_D_splus_Kplus_0_PE[100];   //[B_PVC_nPV]
   Float_t         B_PVC_D_splus_Kplus_0_PX[100];   //[B_PVC_nPV]
   Float_t         B_PVC_D_splus_Kplus_0_PY[100];   //[B_PVC_nPV]
   Float_t         B_PVC_D_splus_Kplus_0_PZ[100];   //[B_PVC_nPV]
   Float_t         B_PVC_D_splus_Kplus_ID[100];   //[B_PVC_nPV]
   Float_t         B_PVC_D_splus_Kplus_PE[100];   //[B_PVC_nPV]
   Float_t         B_PVC_D_splus_Kplus_PX[100];   //[B_PVC_nPV]
   Float_t         B_PVC_D_splus_Kplus_PY[100];   //[B_PVC_nPV]
   Float_t         B_PVC_D_splus_Kplus_PZ[100];   //[B_PVC_nPV]
   Float_t         B_PVC_D_splus_M[100];   //[B_PVC_nPV]
   Float_t         B_PVC_D_splus_MERR[100];   //[B_PVC_nPV]
   Float_t         B_PVC_D_splus_P[100];   //[B_PVC_nPV]
   Float_t         B_PVC_D_splus_PERR[100];   //[B_PVC_nPV]
   Float_t         B_PVC_D_splus_ctau[100];   //[B_PVC_nPV]
   Float_t         B_PVC_D_splus_ctauErr[100];   //[B_PVC_nPV]
   Float_t         B_PVC_D_splus_decayLength[100];   //[B_PVC_nPV]
   Float_t         B_PVC_D_splus_decayLengthErr[100];   //[B_PVC_nPV]
   Float_t         B_PVC_D_splus_piplus_ID[100];   //[B_PVC_nPV]
   Float_t         B_PVC_D_splus_piplus_PE[100];   //[B_PVC_nPV]
   Float_t         B_PVC_D_splus_piplus_PX[100];   //[B_PVC_nPV]
   Float_t         B_PVC_D_splus_piplus_PY[100];   //[B_PVC_nPV]
   Float_t         B_PVC_D_splus_piplus_PZ[100];   //[B_PVC_nPV]
   Float_t         B_PVC_Dplus_Kplus_ID[100];   //[B_PVC_nPV]
   Float_t         B_PVC_Dplus_Kplus_PE[100];   //[B_PVC_nPV]
   Float_t         B_PVC_Dplus_Kplus_PX[100];   //[B_PVC_nPV]
   Float_t         B_PVC_Dplus_Kplus_PY[100];   //[B_PVC_nPV]
   Float_t         B_PVC_Dplus_Kplus_PZ[100];   //[B_PVC_nPV]
   Float_t         B_PVC_Dplus_M[100];   //[B_PVC_nPV]
   Float_t         B_PVC_Dplus_MERR[100];   //[B_PVC_nPV]
   Float_t         B_PVC_Dplus_P[100];   //[B_PVC_nPV]
   Float_t         B_PVC_Dplus_PERR[100];   //[B_PVC_nPV]
   Float_t         B_PVC_Dplus_ctau[100];   //[B_PVC_nPV]
   Float_t         B_PVC_Dplus_ctauErr[100];   //[B_PVC_nPV]
   Float_t         B_PVC_Dplus_decayLength[100];   //[B_PVC_nPV]
   Float_t         B_PVC_Dplus_decayLengthErr[100];   //[B_PVC_nPV]
   Float_t         B_PVC_Dplus_piplus_0_ID[100];   //[B_PVC_nPV]
   Float_t         B_PVC_Dplus_piplus_0_PE[100];   //[B_PVC_nPV]
   Float_t         B_PVC_Dplus_piplus_0_PX[100];   //[B_PVC_nPV]
   Float_t         B_PVC_Dplus_piplus_0_PY[100];   //[B_PVC_nPV]
   Float_t         B_PVC_Dplus_piplus_0_PZ[100];   //[B_PVC_nPV]
   Float_t         B_PVC_Dplus_piplus_ID[100];   //[B_PVC_nPV]
   Float_t         B_PVC_Dplus_piplus_PE[100];   //[B_PVC_nPV]
   Float_t         B_PVC_Dplus_piplus_PX[100];   //[B_PVC_nPV]
   Float_t         B_PVC_Dplus_piplus_PY[100];   //[B_PVC_nPV]
   Float_t         B_PVC_Dplus_piplus_PZ[100];   //[B_PVC_nPV]
   Float_t         B_PVC_M[100];   //[B_PVC_nPV]
   Float_t         B_PVC_MERR[100];   //[B_PVC_nPV]
   Float_t         B_PVC_P[100];   //[B_PVC_nPV]
   Float_t         B_PVC_PERR[100];   //[B_PVC_nPV]
   Float_t         B_PVC_PV_X[100];   //[B_PVC_nPV]
   Float_t         B_PVC_PV_Y[100];   //[B_PVC_nPV]
   Float_t         B_PVC_PV_Z[100];   //[B_PVC_nPV]
   Float_t         B_PVC_PV_key[100];   //[B_PVC_nPV]
   Float_t         B_PVC_chi2[100];   //[B_PVC_nPV]
   Float_t         B_PVC_ctau[100];   //[B_PVC_nPV]
   Float_t         B_PVC_ctauErr[100];   //[B_PVC_nPV]
   Float_t         B_PVC_decayLength[100];   //[B_PVC_nPV]
   Float_t         B_PVC_decayLengthErr[100];   //[B_PVC_nPV]
   Float_t         B_PVC_nDOF[100];   //[B_PVC_nPV]
   Float_t         B_PVC_nIter[100];   //[B_PVC_nPV]
   Float_t         B_PVC_status[100];   //[B_PVC_nPV]
   Double_t        Dsminus_ETA;
   Double_t        Dsminus_MinIPCHI2;
   Double_t        Dsminus_CosTheta;
   Double_t        Dsminus_ENDVERTEX_X;
   Double_t        Dsminus_ENDVERTEX_Y;
   Double_t        Dsminus_ENDVERTEX_Z;
   Double_t        Dsminus_ENDVERTEX_XERR;
   Double_t        Dsminus_ENDVERTEX_YERR;
   Double_t        Dsminus_ENDVERTEX_ZERR;
   Double_t        Dsminus_ENDVERTEX_CHI2;
   Int_t           Dsminus_ENDVERTEX_NDOF;
   Float_t         Dsminus_ENDVERTEX_COV_[3][3];
   Double_t        Dsminus_OWNPV_X;
   Double_t        Dsminus_OWNPV_Y;
   Double_t        Dsminus_OWNPV_Z;
   Double_t        Dsminus_OWNPV_XERR;
   Double_t        Dsminus_OWNPV_YERR;
   Double_t        Dsminus_OWNPV_ZERR;
   Double_t        Dsminus_OWNPV_CHI2;
   Int_t           Dsminus_OWNPV_NDOF;
   Float_t         Dsminus_OWNPV_COV_[3][3];
   Double_t        Dsminus_IP_OWNPV;
   Double_t        Dsminus_IPCHI2_OWNPV;
   Double_t        Dsminus_FD_OWNPV;
   Double_t        Dsminus_FDCHI2_OWNPV;
   Double_t        Dsminus_DIRA_OWNPV;
   Double_t        Dsminus_ORIVX_X;
   Double_t        Dsminus_ORIVX_Y;
   Double_t        Dsminus_ORIVX_Z;
   Double_t        Dsminus_ORIVX_XERR;
   Double_t        Dsminus_ORIVX_YERR;
   Double_t        Dsminus_ORIVX_ZERR;
   Double_t        Dsminus_ORIVX_CHI2;
   Int_t           Dsminus_ORIVX_NDOF;
   Float_t         Dsminus_ORIVX_COV_[3][3];
   Double_t        Dsminus_FD_ORIVX;
   Double_t        Dsminus_FDCHI2_ORIVX;
   Double_t        Dsminus_DIRA_ORIVX;
   Double_t        Dsminus_P;
   Double_t        Dsminus_PT;
   Double_t        Dsminus_PE;
   Double_t        Dsminus_PX;
   Double_t        Dsminus_PY;
   Double_t        Dsminus_PZ;
   Double_t        Dsminus_REFPX;
   Double_t        Dsminus_REFPY;
   Double_t        Dsminus_REFPZ;
   Double_t        Dsminus_MM;
   Double_t        Dsminus_MMERR;
   Double_t        Dsminus_M;
   Int_t           Dsminus_BKGCAT;
   Int_t           Dsminus_TRUEID;
   Double_t        Dsminus_TRUEP_E;
   Double_t        Dsminus_TRUEP_X;
   Double_t        Dsminus_TRUEP_Y;
   Double_t        Dsminus_TRUEP_Z;
   Double_t        Dsminus_TRUEPT;
   Double_t        Dsminus_TRUEORIGINVERTEX_X;
   Double_t        Dsminus_TRUEORIGINVERTEX_Y;
   Double_t        Dsminus_TRUEORIGINVERTEX_Z;
   Double_t        Dsminus_TRUEENDVERTEX_X;
   Double_t        Dsminus_TRUEENDVERTEX_Y;
   Double_t        Dsminus_TRUEENDVERTEX_Z;
   Bool_t          Dsminus_TRUEISSTABLE;
   Double_t        Dsminus_TRUETAU;
   Int_t           Dsminus_ID;
   Double_t        Dsminus_TAU;
   Double_t        Dsminus_TAUERR;
   Double_t        Dsminus_TAUCHI2;
   Bool_t          Dsminus_L0Global_Dec;
   Bool_t          Dsminus_L0Global_TIS;
   Bool_t          Dsminus_L0Global_TOS;
   Bool_t          Dsminus_Hlt1Global_Dec;
   Bool_t          Dsminus_Hlt1Global_TIS;
   Bool_t          Dsminus_Hlt1Global_TOS;
   Bool_t          Dsminus_Hlt1Phys_Dec;
   Bool_t          Dsminus_Hlt1Phys_TIS;
   Bool_t          Dsminus_Hlt1Phys_TOS;
   Bool_t          Dsminus_Hlt2Global_Dec;
   Bool_t          Dsminus_Hlt2Global_TIS;
   Bool_t          Dsminus_Hlt2Global_TOS;
   Bool_t          Dsminus_Hlt2Phys_Dec;
   Bool_t          Dsminus_Hlt2Phys_TIS;
   Bool_t          Dsminus_Hlt2Phys_TOS;
   Bool_t          Dsminus_L0HadronDecision_Dec;
   Bool_t          Dsminus_L0HadronDecision_TIS;
   Bool_t          Dsminus_L0HadronDecision_TOS;
   Bool_t          Dsminus_L0ElectronDecision_Dec;
   Bool_t          Dsminus_L0ElectronDecision_TIS;
   Bool_t          Dsminus_L0ElectronDecision_TOS;
   Bool_t          Dsminus_L0ElectronHiDecision_Dec;
   Bool_t          Dsminus_L0ElectronHiDecision_TIS;
   Bool_t          Dsminus_L0ElectronHiDecision_TOS;
   Bool_t          Dsminus_L0MuonDecision_Dec;
   Bool_t          Dsminus_L0MuonDecision_TIS;
   Bool_t          Dsminus_L0MuonDecision_TOS;
   Bool_t          Dsminus_L0DiMuonDecision_Dec;
   Bool_t          Dsminus_L0DiMuonDecision_TIS;
   Bool_t          Dsminus_L0DiMuonDecision_TOS;
   Bool_t          Dsminus_L0MuonHighDecision_Dec;
   Bool_t          Dsminus_L0MuonHighDecision_TIS;
   Bool_t          Dsminus_L0MuonHighDecision_TOS;
   Bool_t          Dsminus_Hlt1TrackMVADecision_Dec;
   Bool_t          Dsminus_Hlt1TrackMVADecision_TIS;
   Bool_t          Dsminus_Hlt1TrackMVADecision_TOS;
   Bool_t          Dsminus_Hlt1TwoTrackMVADecision_Dec;
   Bool_t          Dsminus_Hlt1TwoTrackMVADecision_TIS;
   Bool_t          Dsminus_Hlt1TwoTrackMVADecision_TOS;
   Bool_t          Dsminus_Hlt1GlobalDecision_Dec;
   Bool_t          Dsminus_Hlt1GlobalDecision_TIS;
   Bool_t          Dsminus_Hlt1GlobalDecision_TOS;
   Bool_t          Dsminus_Hlt1TrackAllL0Decision_Dec;
   Bool_t          Dsminus_Hlt1TrackAllL0Decision_TIS;
   Bool_t          Dsminus_Hlt1TrackAllL0Decision_TOS;
   Bool_t          Dsminus_Hlt2Topo2BodyBBDTDecision_Dec;
   Bool_t          Dsminus_Hlt2Topo2BodyBBDTDecision_TIS;
   Bool_t          Dsminus_Hlt2Topo2BodyBBDTDecision_TOS;
   Bool_t          Dsminus_Hlt2Topo3BodyBBDTDecision_Dec;
   Bool_t          Dsminus_Hlt2Topo3BodyBBDTDecision_TIS;
   Bool_t          Dsminus_Hlt2Topo3BodyBBDTDecision_TOS;
   Bool_t          Dsminus_Hlt2Topo4BodyBBDTDecision_Dec;
   Bool_t          Dsminus_Hlt2Topo4BodyBBDTDecision_TIS;
   Bool_t          Dsminus_Hlt2Topo4BodyBBDTDecision_TOS;
   Bool_t          Dsminus_Hlt2BHadB02PpPpPmPmDecision_Dec;
   Bool_t          Dsminus_Hlt2BHadB02PpPpPmPmDecision_TIS;
   Bool_t          Dsminus_Hlt2BHadB02PpPpPmPmDecision_TOS;
   Bool_t          Dsminus_Hlt2Topo2BodyDecision_Dec;
   Bool_t          Dsminus_Hlt2Topo2BodyDecision_TIS;
   Bool_t          Dsminus_Hlt2Topo2BodyDecision_TOS;
   Bool_t          Dsminus_Hlt2Topo3BodyDecision_Dec;
   Bool_t          Dsminus_Hlt2Topo3BodyDecision_TIS;
   Bool_t          Dsminus_Hlt2Topo3BodyDecision_TOS;
   Bool_t          Dsminus_Hlt2Topo4BodyDecision_Dec;
   Bool_t          Dsminus_Hlt2Topo4BodyDecision_TIS;
   Bool_t          Dsminus_Hlt2Topo4BodyDecision_TOS;
   Bool_t          Dsminus_Hlt2GlobalDecision_Dec;
   Bool_t          Dsminus_Hlt2GlobalDecision_TIS;
   Bool_t          Dsminus_Hlt2GlobalDecision_TOS;
   Double_t        Dsminus_X;
   Double_t        Dsminus_Y;
   Double_t        Kplus_Dsminus_ETA;
   Double_t        Kplus_Dsminus_MinIPCHI2;
   Double_t        Kplus_Dsminus_MC12TuneV2_ProbNNe;
   Double_t        Kplus_Dsminus_MC12TuneV2_ProbNNmu;
   Double_t        Kplus_Dsminus_MC12TuneV2_ProbNNpi;
   Double_t        Kplus_Dsminus_MC12TuneV2_ProbNNk;
   Double_t        Kplus_Dsminus_MC12TuneV2_ProbNNp;
   Double_t        Kplus_Dsminus_MC12TuneV2_ProbNNghost;
   Double_t        Kplus_Dsminus_MC12TuneV3_ProbNNe;
   Double_t        Kplus_Dsminus_MC12TuneV3_ProbNNmu;
   Double_t        Kplus_Dsminus_MC12TuneV3_ProbNNpi;
   Double_t        Kplus_Dsminus_MC12TuneV3_ProbNNk;
   Double_t        Kplus_Dsminus_MC12TuneV3_ProbNNp;
   Double_t        Kplus_Dsminus_MC12TuneV3_ProbNNghost;
   Double_t        Kplus_Dsminus_MC12TuneV4_ProbNNe;
   Double_t        Kplus_Dsminus_MC12TuneV4_ProbNNmu;
   Double_t        Kplus_Dsminus_MC12TuneV4_ProbNNpi;
   Double_t        Kplus_Dsminus_MC12TuneV4_ProbNNk;
   Double_t        Kplus_Dsminus_MC12TuneV4_ProbNNp;
   Double_t        Kplus_Dsminus_MC12TuneV4_ProbNNghost;
   Double_t        Kplus_Dsminus_MC15TuneV1_ProbNNe;
   Double_t        Kplus_Dsminus_MC15TuneV1_ProbNNmu;
   Double_t        Kplus_Dsminus_MC15TuneV1_ProbNNpi;
   Double_t        Kplus_Dsminus_MC15TuneV1_ProbNNk;
   Double_t        Kplus_Dsminus_MC15TuneV1_ProbNNp;
   Double_t        Kplus_Dsminus_MC15TuneV1_ProbNNghost;
   Double_t        Kplus_Dsminus_CosTheta;
   Double_t        Kplus_Dsminus_OWNPV_X;
   Double_t        Kplus_Dsminus_OWNPV_Y;
   Double_t        Kplus_Dsminus_OWNPV_Z;
   Double_t        Kplus_Dsminus_OWNPV_XERR;
   Double_t        Kplus_Dsminus_OWNPV_YERR;
   Double_t        Kplus_Dsminus_OWNPV_ZERR;
   Double_t        Kplus_Dsminus_OWNPV_CHI2;
   Int_t           Kplus_Dsminus_OWNPV_NDOF;
   Float_t         Kplus_Dsminus_OWNPV_COV_[3][3];
   Double_t        Kplus_Dsminus_IP_OWNPV;
   Double_t        Kplus_Dsminus_IPCHI2_OWNPV;
   Double_t        Kplus_Dsminus_ORIVX_X;
   Double_t        Kplus_Dsminus_ORIVX_Y;
   Double_t        Kplus_Dsminus_ORIVX_Z;
   Double_t        Kplus_Dsminus_ORIVX_XERR;
   Double_t        Kplus_Dsminus_ORIVX_YERR;
   Double_t        Kplus_Dsminus_ORIVX_ZERR;
   Double_t        Kplus_Dsminus_ORIVX_CHI2;
   Int_t           Kplus_Dsminus_ORIVX_NDOF;
   Float_t         Kplus_Dsminus_ORIVX_COV_[3][3];
   Double_t        Kplus_Dsminus_P;
   Double_t        Kplus_Dsminus_PT;
   Double_t        Kplus_Dsminus_PE;
   Double_t        Kplus_Dsminus_PX;
   Double_t        Kplus_Dsminus_PY;
   Double_t        Kplus_Dsminus_PZ;
   Double_t        Kplus_Dsminus_REFPX;
   Double_t        Kplus_Dsminus_REFPY;
   Double_t        Kplus_Dsminus_REFPZ;
   Double_t        Kplus_Dsminus_M;
   Double_t        Kplus_Dsminus_AtVtx_PE;
   Double_t        Kplus_Dsminus_AtVtx_PX;
   Double_t        Kplus_Dsminus_AtVtx_PY;
   Double_t        Kplus_Dsminus_AtVtx_PZ;
   Int_t           Kplus_Dsminus_TRUEID;
   Double_t        Kplus_Dsminus_TRUEP_E;
   Double_t        Kplus_Dsminus_TRUEP_X;
   Double_t        Kplus_Dsminus_TRUEP_Y;
   Double_t        Kplus_Dsminus_TRUEP_Z;
   Double_t        Kplus_Dsminus_TRUEPT;
   Double_t        Kplus_Dsminus_TRUEORIGINVERTEX_X;
   Double_t        Kplus_Dsminus_TRUEORIGINVERTEX_Y;
   Double_t        Kplus_Dsminus_TRUEORIGINVERTEX_Z;
   Double_t        Kplus_Dsminus_TRUEENDVERTEX_X;
   Double_t        Kplus_Dsminus_TRUEENDVERTEX_Y;
   Double_t        Kplus_Dsminus_TRUEENDVERTEX_Z;
   Bool_t          Kplus_Dsminus_TRUEISSTABLE;
   Double_t        Kplus_Dsminus_TRUETAU;
   Int_t           Kplus_Dsminus_ID;
   Double_t        Kplus_Dsminus_CombDLLMu;
   Double_t        Kplus_Dsminus_ProbNNmu;
   Double_t        Kplus_Dsminus_ProbNNghost;
   Double_t        Kplus_Dsminus_InMuonAcc;
   Double_t        Kplus_Dsminus_MuonDist2;
   Int_t           Kplus_Dsminus_regionInM2;
   Bool_t          Kplus_Dsminus_hasMuon;
   Bool_t          Kplus_Dsminus_isMuon;
   Bool_t          Kplus_Dsminus_isMuonLoose;
   Int_t           Kplus_Dsminus_NShared;
   Double_t        Kplus_Dsminus_MuonLLmu;
   Double_t        Kplus_Dsminus_MuonLLbg;
   Int_t           Kplus_Dsminus_isMuonFromProto;
   Double_t        Kplus_Dsminus_PIDe;
   Double_t        Kplus_Dsminus_PIDmu;
   Double_t        Kplus_Dsminus_PIDK;
   Double_t        Kplus_Dsminus_PIDp;
   Double_t        Kplus_Dsminus_PIDd;
   Double_t        Kplus_Dsminus_ProbNNe;
   Double_t        Kplus_Dsminus_ProbNNk;
   Double_t        Kplus_Dsminus_ProbNNp;
   Double_t        Kplus_Dsminus_ProbNNpi;
   Double_t        Kplus_Dsminus_ProbNNd;
   Bool_t          Kplus_Dsminus_hasRich;
   Bool_t          Kplus_Dsminus_UsedRichAerogel;
   Bool_t          Kplus_Dsminus_UsedRich1Gas;
   Bool_t          Kplus_Dsminus_UsedRich2Gas;
   Bool_t          Kplus_Dsminus_RichAboveElThres;
   Bool_t          Kplus_Dsminus_RichAboveMuThres;
   Bool_t          Kplus_Dsminus_RichAbovePiThres;
   Bool_t          Kplus_Dsminus_RichAboveKaThres;
   Bool_t          Kplus_Dsminus_RichAbovePrThres;
   Bool_t          Kplus_Dsminus_hasCalo;
   Bool_t          Kplus_Dsminus_L0Global_Dec;
   Bool_t          Kplus_Dsminus_L0Global_TIS;
   Bool_t          Kplus_Dsminus_L0Global_TOS;
   Bool_t          Kplus_Dsminus_Hlt1Global_Dec;
   Bool_t          Kplus_Dsminus_Hlt1Global_TIS;
   Bool_t          Kplus_Dsminus_Hlt1Global_TOS;
   Bool_t          Kplus_Dsminus_Hlt1Phys_Dec;
   Bool_t          Kplus_Dsminus_Hlt1Phys_TIS;
   Bool_t          Kplus_Dsminus_Hlt1Phys_TOS;
   Bool_t          Kplus_Dsminus_Hlt2Global_Dec;
   Bool_t          Kplus_Dsminus_Hlt2Global_TIS;
   Bool_t          Kplus_Dsminus_Hlt2Global_TOS;
   Bool_t          Kplus_Dsminus_Hlt2Phys_Dec;
   Bool_t          Kplus_Dsminus_Hlt2Phys_TIS;
   Bool_t          Kplus_Dsminus_Hlt2Phys_TOS;
   Bool_t          Kplus_Dsminus_L0HadronDecision_Dec;
   Bool_t          Kplus_Dsminus_L0HadronDecision_TIS;
   Bool_t          Kplus_Dsminus_L0HadronDecision_TOS;
   Bool_t          Kplus_Dsminus_L0ElectronDecision_Dec;
   Bool_t          Kplus_Dsminus_L0ElectronDecision_TIS;
   Bool_t          Kplus_Dsminus_L0ElectronDecision_TOS;
   Bool_t          Kplus_Dsminus_L0ElectronHiDecision_Dec;
   Bool_t          Kplus_Dsminus_L0ElectronHiDecision_TIS;
   Bool_t          Kplus_Dsminus_L0ElectronHiDecision_TOS;
   Bool_t          Kplus_Dsminus_L0MuonDecision_Dec;
   Bool_t          Kplus_Dsminus_L0MuonDecision_TIS;
   Bool_t          Kplus_Dsminus_L0MuonDecision_TOS;
   Bool_t          Kplus_Dsminus_L0DiMuonDecision_Dec;
   Bool_t          Kplus_Dsminus_L0DiMuonDecision_TIS;
   Bool_t          Kplus_Dsminus_L0DiMuonDecision_TOS;
   Bool_t          Kplus_Dsminus_L0MuonHighDecision_Dec;
   Bool_t          Kplus_Dsminus_L0MuonHighDecision_TIS;
   Bool_t          Kplus_Dsminus_L0MuonHighDecision_TOS;
   Bool_t          Kplus_Dsminus_Hlt1TrackMVADecision_Dec;
   Bool_t          Kplus_Dsminus_Hlt1TrackMVADecision_TIS;
   Bool_t          Kplus_Dsminus_Hlt1TrackMVADecision_TOS;
   Bool_t          Kplus_Dsminus_Hlt1TwoTrackMVADecision_Dec;
   Bool_t          Kplus_Dsminus_Hlt1TwoTrackMVADecision_TIS;
   Bool_t          Kplus_Dsminus_Hlt1TwoTrackMVADecision_TOS;
   Bool_t          Kplus_Dsminus_Hlt1GlobalDecision_Dec;
   Bool_t          Kplus_Dsminus_Hlt1GlobalDecision_TIS;
   Bool_t          Kplus_Dsminus_Hlt1GlobalDecision_TOS;
   Bool_t          Kplus_Dsminus_Hlt1TrackAllL0Decision_Dec;
   Bool_t          Kplus_Dsminus_Hlt1TrackAllL0Decision_TIS;
   Bool_t          Kplus_Dsminus_Hlt1TrackAllL0Decision_TOS;
   Bool_t          Kplus_Dsminus_Hlt2Topo2BodyBBDTDecision_Dec;
   Bool_t          Kplus_Dsminus_Hlt2Topo2BodyBBDTDecision_TIS;
   Bool_t          Kplus_Dsminus_Hlt2Topo2BodyBBDTDecision_TOS;
   Bool_t          Kplus_Dsminus_Hlt2Topo3BodyBBDTDecision_Dec;
   Bool_t          Kplus_Dsminus_Hlt2Topo3BodyBBDTDecision_TIS;
   Bool_t          Kplus_Dsminus_Hlt2Topo3BodyBBDTDecision_TOS;
   Bool_t          Kplus_Dsminus_Hlt2Topo4BodyBBDTDecision_Dec;
   Bool_t          Kplus_Dsminus_Hlt2Topo4BodyBBDTDecision_TIS;
   Bool_t          Kplus_Dsminus_Hlt2Topo4BodyBBDTDecision_TOS;
   Bool_t          Kplus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_Dec;
   Bool_t          Kplus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_TIS;
   Bool_t          Kplus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_TOS;
   Bool_t          Kplus_Dsminus_Hlt2Topo2BodyDecision_Dec;
   Bool_t          Kplus_Dsminus_Hlt2Topo2BodyDecision_TIS;
   Bool_t          Kplus_Dsminus_Hlt2Topo2BodyDecision_TOS;
   Bool_t          Kplus_Dsminus_Hlt2Topo3BodyDecision_Dec;
   Bool_t          Kplus_Dsminus_Hlt2Topo3BodyDecision_TIS;
   Bool_t          Kplus_Dsminus_Hlt2Topo3BodyDecision_TOS;
   Bool_t          Kplus_Dsminus_Hlt2Topo4BodyDecision_Dec;
   Bool_t          Kplus_Dsminus_Hlt2Topo4BodyDecision_TIS;
   Bool_t          Kplus_Dsminus_Hlt2Topo4BodyDecision_TOS;
   Bool_t          Kplus_Dsminus_Hlt2GlobalDecision_Dec;
   Bool_t          Kplus_Dsminus_Hlt2GlobalDecision_TIS;
   Bool_t          Kplus_Dsminus_Hlt2GlobalDecision_TOS;
   Int_t           Kplus_Dsminus_TRACK_Type;
   Int_t           Kplus_Dsminus_TRACK_Key;
   Double_t        Kplus_Dsminus_TRACK_CHI2NDOF;
   Double_t        Kplus_Dsminus_TRACK_PCHI2;
   Double_t        Kplus_Dsminus_TRACK_MatchCHI2;
   Double_t        Kplus_Dsminus_TRACK_GhostProb;
   Double_t        Kplus_Dsminus_TRACK_CloneDist;
   Double_t        Kplus_Dsminus_TRACK_Likelihood;
   Double_t        Kplus_Dsminus_X;
   Double_t        Kplus_Dsminus_Y;
   Double_t        Kminus_Dsminus_ETA;
   Double_t        Kminus_Dsminus_MinIPCHI2;
   Double_t        Kminus_Dsminus_MC12TuneV2_ProbNNe;
   Double_t        Kminus_Dsminus_MC12TuneV2_ProbNNmu;
   Double_t        Kminus_Dsminus_MC12TuneV2_ProbNNpi;
   Double_t        Kminus_Dsminus_MC12TuneV2_ProbNNk;
   Double_t        Kminus_Dsminus_MC12TuneV2_ProbNNp;
   Double_t        Kminus_Dsminus_MC12TuneV2_ProbNNghost;
   Double_t        Kminus_Dsminus_MC12TuneV3_ProbNNe;
   Double_t        Kminus_Dsminus_MC12TuneV3_ProbNNmu;
   Double_t        Kminus_Dsminus_MC12TuneV3_ProbNNpi;
   Double_t        Kminus_Dsminus_MC12TuneV3_ProbNNk;
   Double_t        Kminus_Dsminus_MC12TuneV3_ProbNNp;
   Double_t        Kminus_Dsminus_MC12TuneV3_ProbNNghost;
   Double_t        Kminus_Dsminus_MC12TuneV4_ProbNNe;
   Double_t        Kminus_Dsminus_MC12TuneV4_ProbNNmu;
   Double_t        Kminus_Dsminus_MC12TuneV4_ProbNNpi;
   Double_t        Kminus_Dsminus_MC12TuneV4_ProbNNk;
   Double_t        Kminus_Dsminus_MC12TuneV4_ProbNNp;
   Double_t        Kminus_Dsminus_MC12TuneV4_ProbNNghost;
   Double_t        Kminus_Dsminus_MC15TuneV1_ProbNNe;
   Double_t        Kminus_Dsminus_MC15TuneV1_ProbNNmu;
   Double_t        Kminus_Dsminus_MC15TuneV1_ProbNNpi;
   Double_t        Kminus_Dsminus_MC15TuneV1_ProbNNk;
   Double_t        Kminus_Dsminus_MC15TuneV1_ProbNNp;
   Double_t        Kminus_Dsminus_MC15TuneV1_ProbNNghost;
   Double_t        Kminus_Dsminus_CosTheta;
   Double_t        Kminus_Dsminus_OWNPV_X;
   Double_t        Kminus_Dsminus_OWNPV_Y;
   Double_t        Kminus_Dsminus_OWNPV_Z;
   Double_t        Kminus_Dsminus_OWNPV_XERR;
   Double_t        Kminus_Dsminus_OWNPV_YERR;
   Double_t        Kminus_Dsminus_OWNPV_ZERR;
   Double_t        Kminus_Dsminus_OWNPV_CHI2;
   Int_t           Kminus_Dsminus_OWNPV_NDOF;
   Float_t         Kminus_Dsminus_OWNPV_COV_[3][3];
   Double_t        Kminus_Dsminus_IP_OWNPV;
   Double_t        Kminus_Dsminus_IPCHI2_OWNPV;
   Double_t        Kminus_Dsminus_ORIVX_X;
   Double_t        Kminus_Dsminus_ORIVX_Y;
   Double_t        Kminus_Dsminus_ORIVX_Z;
   Double_t        Kminus_Dsminus_ORIVX_XERR;
   Double_t        Kminus_Dsminus_ORIVX_YERR;
   Double_t        Kminus_Dsminus_ORIVX_ZERR;
   Double_t        Kminus_Dsminus_ORIVX_CHI2;
   Int_t           Kminus_Dsminus_ORIVX_NDOF;
   Float_t         Kminus_Dsminus_ORIVX_COV_[3][3];
   Double_t        Kminus_Dsminus_P;
   Double_t        Kminus_Dsminus_PT;
   Double_t        Kminus_Dsminus_PE;
   Double_t        Kminus_Dsminus_PX;
   Double_t        Kminus_Dsminus_PY;
   Double_t        Kminus_Dsminus_PZ;
   Double_t        Kminus_Dsminus_REFPX;
   Double_t        Kminus_Dsminus_REFPY;
   Double_t        Kminus_Dsminus_REFPZ;
   Double_t        Kminus_Dsminus_M;
   Double_t        Kminus_Dsminus_AtVtx_PE;
   Double_t        Kminus_Dsminus_AtVtx_PX;
   Double_t        Kminus_Dsminus_AtVtx_PY;
   Double_t        Kminus_Dsminus_AtVtx_PZ;
   Int_t           Kminus_Dsminus_TRUEID;
   Double_t        Kminus_Dsminus_TRUEP_E;
   Double_t        Kminus_Dsminus_TRUEP_X;
   Double_t        Kminus_Dsminus_TRUEP_Y;
   Double_t        Kminus_Dsminus_TRUEP_Z;
   Double_t        Kminus_Dsminus_TRUEPT;
   Double_t        Kminus_Dsminus_TRUEORIGINVERTEX_X;
   Double_t        Kminus_Dsminus_TRUEORIGINVERTEX_Y;
   Double_t        Kminus_Dsminus_TRUEORIGINVERTEX_Z;
   Double_t        Kminus_Dsminus_TRUEENDVERTEX_X;
   Double_t        Kminus_Dsminus_TRUEENDVERTEX_Y;
   Double_t        Kminus_Dsminus_TRUEENDVERTEX_Z;
   Bool_t          Kminus_Dsminus_TRUEISSTABLE;
   Double_t        Kminus_Dsminus_TRUETAU;
   Int_t           Kminus_Dsminus_ID;
   Double_t        Kminus_Dsminus_CombDLLMu;
   Double_t        Kminus_Dsminus_ProbNNmu;
   Double_t        Kminus_Dsminus_ProbNNghost;
   Double_t        Kminus_Dsminus_InMuonAcc;
   Double_t        Kminus_Dsminus_MuonDist2;
   Int_t           Kminus_Dsminus_regionInM2;
   Bool_t          Kminus_Dsminus_hasMuon;
   Bool_t          Kminus_Dsminus_isMuon;
   Bool_t          Kminus_Dsminus_isMuonLoose;
   Int_t           Kminus_Dsminus_NShared;
   Double_t        Kminus_Dsminus_MuonLLmu;
   Double_t        Kminus_Dsminus_MuonLLbg;
   Int_t           Kminus_Dsminus_isMuonFromProto;
   Double_t        Kminus_Dsminus_PIDe;
   Double_t        Kminus_Dsminus_PIDmu;
   Double_t        Kminus_Dsminus_PIDK;
   Double_t        Kminus_Dsminus_PIDp;
   Double_t        Kminus_Dsminus_PIDd;
   Double_t        Kminus_Dsminus_ProbNNe;
   Double_t        Kminus_Dsminus_ProbNNk;
   Double_t        Kminus_Dsminus_ProbNNp;
   Double_t        Kminus_Dsminus_ProbNNpi;
   Double_t        Kminus_Dsminus_ProbNNd;
   Bool_t          Kminus_Dsminus_hasRich;
   Bool_t          Kminus_Dsminus_UsedRichAerogel;
   Bool_t          Kminus_Dsminus_UsedRich1Gas;
   Bool_t          Kminus_Dsminus_UsedRich2Gas;
   Bool_t          Kminus_Dsminus_RichAboveElThres;
   Bool_t          Kminus_Dsminus_RichAboveMuThres;
   Bool_t          Kminus_Dsminus_RichAbovePiThres;
   Bool_t          Kminus_Dsminus_RichAboveKaThres;
   Bool_t          Kminus_Dsminus_RichAbovePrThres;
   Bool_t          Kminus_Dsminus_hasCalo;
   Bool_t          Kminus_Dsminus_L0Global_Dec;
   Bool_t          Kminus_Dsminus_L0Global_TIS;
   Bool_t          Kminus_Dsminus_L0Global_TOS;
   Bool_t          Kminus_Dsminus_Hlt1Global_Dec;
   Bool_t          Kminus_Dsminus_Hlt1Global_TIS;
   Bool_t          Kminus_Dsminus_Hlt1Global_TOS;
   Bool_t          Kminus_Dsminus_Hlt1Phys_Dec;
   Bool_t          Kminus_Dsminus_Hlt1Phys_TIS;
   Bool_t          Kminus_Dsminus_Hlt1Phys_TOS;
   Bool_t          Kminus_Dsminus_Hlt2Global_Dec;
   Bool_t          Kminus_Dsminus_Hlt2Global_TIS;
   Bool_t          Kminus_Dsminus_Hlt2Global_TOS;
   Bool_t          Kminus_Dsminus_Hlt2Phys_Dec;
   Bool_t          Kminus_Dsminus_Hlt2Phys_TIS;
   Bool_t          Kminus_Dsminus_Hlt2Phys_TOS;
   Bool_t          Kminus_Dsminus_L0HadronDecision_Dec;
   Bool_t          Kminus_Dsminus_L0HadronDecision_TIS;
   Bool_t          Kminus_Dsminus_L0HadronDecision_TOS;
   Bool_t          Kminus_Dsminus_L0ElectronDecision_Dec;
   Bool_t          Kminus_Dsminus_L0ElectronDecision_TIS;
   Bool_t          Kminus_Dsminus_L0ElectronDecision_TOS;
   Bool_t          Kminus_Dsminus_L0ElectronHiDecision_Dec;
   Bool_t          Kminus_Dsminus_L0ElectronHiDecision_TIS;
   Bool_t          Kminus_Dsminus_L0ElectronHiDecision_TOS;
   Bool_t          Kminus_Dsminus_L0MuonDecision_Dec;
   Bool_t          Kminus_Dsminus_L0MuonDecision_TIS;
   Bool_t          Kminus_Dsminus_L0MuonDecision_TOS;
   Bool_t          Kminus_Dsminus_L0DiMuonDecision_Dec;
   Bool_t          Kminus_Dsminus_L0DiMuonDecision_TIS;
   Bool_t          Kminus_Dsminus_L0DiMuonDecision_TOS;
   Bool_t          Kminus_Dsminus_L0MuonHighDecision_Dec;
   Bool_t          Kminus_Dsminus_L0MuonHighDecision_TIS;
   Bool_t          Kminus_Dsminus_L0MuonHighDecision_TOS;
   Bool_t          Kminus_Dsminus_Hlt1TrackMVADecision_Dec;
   Bool_t          Kminus_Dsminus_Hlt1TrackMVADecision_TIS;
   Bool_t          Kminus_Dsminus_Hlt1TrackMVADecision_TOS;
   Bool_t          Kminus_Dsminus_Hlt1TwoTrackMVADecision_Dec;
   Bool_t          Kminus_Dsminus_Hlt1TwoTrackMVADecision_TIS;
   Bool_t          Kminus_Dsminus_Hlt1TwoTrackMVADecision_TOS;
   Bool_t          Kminus_Dsminus_Hlt1GlobalDecision_Dec;
   Bool_t          Kminus_Dsminus_Hlt1GlobalDecision_TIS;
   Bool_t          Kminus_Dsminus_Hlt1GlobalDecision_TOS;
   Bool_t          Kminus_Dsminus_Hlt1TrackAllL0Decision_Dec;
   Bool_t          Kminus_Dsminus_Hlt1TrackAllL0Decision_TIS;
   Bool_t          Kminus_Dsminus_Hlt1TrackAllL0Decision_TOS;
   Bool_t          Kminus_Dsminus_Hlt2Topo2BodyBBDTDecision_Dec;
   Bool_t          Kminus_Dsminus_Hlt2Topo2BodyBBDTDecision_TIS;
   Bool_t          Kminus_Dsminus_Hlt2Topo2BodyBBDTDecision_TOS;
   Bool_t          Kminus_Dsminus_Hlt2Topo3BodyBBDTDecision_Dec;
   Bool_t          Kminus_Dsminus_Hlt2Topo3BodyBBDTDecision_TIS;
   Bool_t          Kminus_Dsminus_Hlt2Topo3BodyBBDTDecision_TOS;
   Bool_t          Kminus_Dsminus_Hlt2Topo4BodyBBDTDecision_Dec;
   Bool_t          Kminus_Dsminus_Hlt2Topo4BodyBBDTDecision_TIS;
   Bool_t          Kminus_Dsminus_Hlt2Topo4BodyBBDTDecision_TOS;
   Bool_t          Kminus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_Dec;
   Bool_t          Kminus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_TIS;
   Bool_t          Kminus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_TOS;
   Bool_t          Kminus_Dsminus_Hlt2Topo2BodyDecision_Dec;
   Bool_t          Kminus_Dsminus_Hlt2Topo2BodyDecision_TIS;
   Bool_t          Kminus_Dsminus_Hlt2Topo2BodyDecision_TOS;
   Bool_t          Kminus_Dsminus_Hlt2Topo3BodyDecision_Dec;
   Bool_t          Kminus_Dsminus_Hlt2Topo3BodyDecision_TIS;
   Bool_t          Kminus_Dsminus_Hlt2Topo3BodyDecision_TOS;
   Bool_t          Kminus_Dsminus_Hlt2Topo4BodyDecision_Dec;
   Bool_t          Kminus_Dsminus_Hlt2Topo4BodyDecision_TIS;
   Bool_t          Kminus_Dsminus_Hlt2Topo4BodyDecision_TOS;
   Bool_t          Kminus_Dsminus_Hlt2GlobalDecision_Dec;
   Bool_t          Kminus_Dsminus_Hlt2GlobalDecision_TIS;
   Bool_t          Kminus_Dsminus_Hlt2GlobalDecision_TOS;
   Int_t           Kminus_Dsminus_TRACK_Type;
   Int_t           Kminus_Dsminus_TRACK_Key;
   Double_t        Kminus_Dsminus_TRACK_CHI2NDOF;
   Double_t        Kminus_Dsminus_TRACK_PCHI2;
   Double_t        Kminus_Dsminus_TRACK_MatchCHI2;
   Double_t        Kminus_Dsminus_TRACK_GhostProb;
   Double_t        Kminus_Dsminus_TRACK_CloneDist;
   Double_t        Kminus_Dsminus_TRACK_Likelihood;
   Double_t        Kminus_Dsminus_X;
   Double_t        Kminus_Dsminus_Y;
   Double_t        piminus_Dsminus_ETA;
   Double_t        piminus_Dsminus_MinIPCHI2;
   Double_t        piminus_Dsminus_MC12TuneV2_ProbNNe;
   Double_t        piminus_Dsminus_MC12TuneV2_ProbNNmu;
   Double_t        piminus_Dsminus_MC12TuneV2_ProbNNpi;
   Double_t        piminus_Dsminus_MC12TuneV2_ProbNNk;
   Double_t        piminus_Dsminus_MC12TuneV2_ProbNNp;
   Double_t        piminus_Dsminus_MC12TuneV2_ProbNNghost;
   Double_t        piminus_Dsminus_MC12TuneV3_ProbNNe;
   Double_t        piminus_Dsminus_MC12TuneV3_ProbNNmu;
   Double_t        piminus_Dsminus_MC12TuneV3_ProbNNpi;
   Double_t        piminus_Dsminus_MC12TuneV3_ProbNNk;
   Double_t        piminus_Dsminus_MC12TuneV3_ProbNNp;
   Double_t        piminus_Dsminus_MC12TuneV3_ProbNNghost;
   Double_t        piminus_Dsminus_MC12TuneV4_ProbNNe;
   Double_t        piminus_Dsminus_MC12TuneV4_ProbNNmu;
   Double_t        piminus_Dsminus_MC12TuneV4_ProbNNpi;
   Double_t        piminus_Dsminus_MC12TuneV4_ProbNNk;
   Double_t        piminus_Dsminus_MC12TuneV4_ProbNNp;
   Double_t        piminus_Dsminus_MC12TuneV4_ProbNNghost;
   Double_t        piminus_Dsminus_MC15TuneV1_ProbNNe;
   Double_t        piminus_Dsminus_MC15TuneV1_ProbNNmu;
   Double_t        piminus_Dsminus_MC15TuneV1_ProbNNpi;
   Double_t        piminus_Dsminus_MC15TuneV1_ProbNNk;
   Double_t        piminus_Dsminus_MC15TuneV1_ProbNNp;
   Double_t        piminus_Dsminus_MC15TuneV1_ProbNNghost;
   Double_t        piminus_Dsminus_CosTheta;
   Double_t        piminus_Dsminus_OWNPV_X;
   Double_t        piminus_Dsminus_OWNPV_Y;
   Double_t        piminus_Dsminus_OWNPV_Z;
   Double_t        piminus_Dsminus_OWNPV_XERR;
   Double_t        piminus_Dsminus_OWNPV_YERR;
   Double_t        piminus_Dsminus_OWNPV_ZERR;
   Double_t        piminus_Dsminus_OWNPV_CHI2;
   Int_t           piminus_Dsminus_OWNPV_NDOF;
   Float_t         piminus_Dsminus_OWNPV_COV_[3][3];
   Double_t        piminus_Dsminus_IP_OWNPV;
   Double_t        piminus_Dsminus_IPCHI2_OWNPV;
   Double_t        piminus_Dsminus_ORIVX_X;
   Double_t        piminus_Dsminus_ORIVX_Y;
   Double_t        piminus_Dsminus_ORIVX_Z;
   Double_t        piminus_Dsminus_ORIVX_XERR;
   Double_t        piminus_Dsminus_ORIVX_YERR;
   Double_t        piminus_Dsminus_ORIVX_ZERR;
   Double_t        piminus_Dsminus_ORIVX_CHI2;
   Int_t           piminus_Dsminus_ORIVX_NDOF;
   Float_t         piminus_Dsminus_ORIVX_COV_[3][3];
   Double_t        piminus_Dsminus_P;
   Double_t        piminus_Dsminus_PT;
   Double_t        piminus_Dsminus_PE;
   Double_t        piminus_Dsminus_PX;
   Double_t        piminus_Dsminus_PY;
   Double_t        piminus_Dsminus_PZ;
   Double_t        piminus_Dsminus_REFPX;
   Double_t        piminus_Dsminus_REFPY;
   Double_t        piminus_Dsminus_REFPZ;
   Double_t        piminus_Dsminus_M;
   Double_t        piminus_Dsminus_AtVtx_PE;
   Double_t        piminus_Dsminus_AtVtx_PX;
   Double_t        piminus_Dsminus_AtVtx_PY;
   Double_t        piminus_Dsminus_AtVtx_PZ;
   Int_t           piminus_Dsminus_TRUEID;
   Double_t        piminus_Dsminus_TRUEP_E;
   Double_t        piminus_Dsminus_TRUEP_X;
   Double_t        piminus_Dsminus_TRUEP_Y;
   Double_t        piminus_Dsminus_TRUEP_Z;
   Double_t        piminus_Dsminus_TRUEPT;
   Double_t        piminus_Dsminus_TRUEORIGINVERTEX_X;
   Double_t        piminus_Dsminus_TRUEORIGINVERTEX_Y;
   Double_t        piminus_Dsminus_TRUEORIGINVERTEX_Z;
   Double_t        piminus_Dsminus_TRUEENDVERTEX_X;
   Double_t        piminus_Dsminus_TRUEENDVERTEX_Y;
   Double_t        piminus_Dsminus_TRUEENDVERTEX_Z;
   Bool_t          piminus_Dsminus_TRUEISSTABLE;
   Double_t        piminus_Dsminus_TRUETAU;
   Int_t           piminus_Dsminus_ID;
   Double_t        piminus_Dsminus_CombDLLMu;
   Double_t        piminus_Dsminus_ProbNNmu;
   Double_t        piminus_Dsminus_ProbNNghost;
   Double_t        piminus_Dsminus_InMuonAcc;
   Double_t        piminus_Dsminus_MuonDist2;
   Int_t           piminus_Dsminus_regionInM2;
   Bool_t          piminus_Dsminus_hasMuon;
   Bool_t          piminus_Dsminus_isMuon;
   Bool_t          piminus_Dsminus_isMuonLoose;
   Int_t           piminus_Dsminus_NShared;
   Double_t        piminus_Dsminus_MuonLLmu;
   Double_t        piminus_Dsminus_MuonLLbg;
   Int_t           piminus_Dsminus_isMuonFromProto;
   Double_t        piminus_Dsminus_PIDe;
   Double_t        piminus_Dsminus_PIDmu;
   Double_t        piminus_Dsminus_PIDK;
   Double_t        piminus_Dsminus_PIDp;
   Double_t        piminus_Dsminus_PIDd;
   Double_t        piminus_Dsminus_ProbNNe;
   Double_t        piminus_Dsminus_ProbNNk;
   Double_t        piminus_Dsminus_ProbNNp;
   Double_t        piminus_Dsminus_ProbNNpi;
   Double_t        piminus_Dsminus_ProbNNd;
   Bool_t          piminus_Dsminus_hasRich;
   Bool_t          piminus_Dsminus_UsedRichAerogel;
   Bool_t          piminus_Dsminus_UsedRich1Gas;
   Bool_t          piminus_Dsminus_UsedRich2Gas;
   Bool_t          piminus_Dsminus_RichAboveElThres;
   Bool_t          piminus_Dsminus_RichAboveMuThres;
   Bool_t          piminus_Dsminus_RichAbovePiThres;
   Bool_t          piminus_Dsminus_RichAboveKaThres;
   Bool_t          piminus_Dsminus_RichAbovePrThres;
   Bool_t          piminus_Dsminus_hasCalo;
   Bool_t          piminus_Dsminus_L0Global_Dec;
   Bool_t          piminus_Dsminus_L0Global_TIS;
   Bool_t          piminus_Dsminus_L0Global_TOS;
   Bool_t          piminus_Dsminus_Hlt1Global_Dec;
   Bool_t          piminus_Dsminus_Hlt1Global_TIS;
   Bool_t          piminus_Dsminus_Hlt1Global_TOS;
   Bool_t          piminus_Dsminus_Hlt1Phys_Dec;
   Bool_t          piminus_Dsminus_Hlt1Phys_TIS;
   Bool_t          piminus_Dsminus_Hlt1Phys_TOS;
   Bool_t          piminus_Dsminus_Hlt2Global_Dec;
   Bool_t          piminus_Dsminus_Hlt2Global_TIS;
   Bool_t          piminus_Dsminus_Hlt2Global_TOS;
   Bool_t          piminus_Dsminus_Hlt2Phys_Dec;
   Bool_t          piminus_Dsminus_Hlt2Phys_TIS;
   Bool_t          piminus_Dsminus_Hlt2Phys_TOS;
   Bool_t          piminus_Dsminus_L0HadronDecision_Dec;
   Bool_t          piminus_Dsminus_L0HadronDecision_TIS;
   Bool_t          piminus_Dsminus_L0HadronDecision_TOS;
   Bool_t          piminus_Dsminus_L0ElectronDecision_Dec;
   Bool_t          piminus_Dsminus_L0ElectronDecision_TIS;
   Bool_t          piminus_Dsminus_L0ElectronDecision_TOS;
   Bool_t          piminus_Dsminus_L0ElectronHiDecision_Dec;
   Bool_t          piminus_Dsminus_L0ElectronHiDecision_TIS;
   Bool_t          piminus_Dsminus_L0ElectronHiDecision_TOS;
   Bool_t          piminus_Dsminus_L0MuonDecision_Dec;
   Bool_t          piminus_Dsminus_L0MuonDecision_TIS;
   Bool_t          piminus_Dsminus_L0MuonDecision_TOS;
   Bool_t          piminus_Dsminus_L0DiMuonDecision_Dec;
   Bool_t          piminus_Dsminus_L0DiMuonDecision_TIS;
   Bool_t          piminus_Dsminus_L0DiMuonDecision_TOS;
   Bool_t          piminus_Dsminus_L0MuonHighDecision_Dec;
   Bool_t          piminus_Dsminus_L0MuonHighDecision_TIS;
   Bool_t          piminus_Dsminus_L0MuonHighDecision_TOS;
   Bool_t          piminus_Dsminus_Hlt1TrackMVADecision_Dec;
   Bool_t          piminus_Dsminus_Hlt1TrackMVADecision_TIS;
   Bool_t          piminus_Dsminus_Hlt1TrackMVADecision_TOS;
   Bool_t          piminus_Dsminus_Hlt1TwoTrackMVADecision_Dec;
   Bool_t          piminus_Dsminus_Hlt1TwoTrackMVADecision_TIS;
   Bool_t          piminus_Dsminus_Hlt1TwoTrackMVADecision_TOS;
   Bool_t          piminus_Dsminus_Hlt1GlobalDecision_Dec;
   Bool_t          piminus_Dsminus_Hlt1GlobalDecision_TIS;
   Bool_t          piminus_Dsminus_Hlt1GlobalDecision_TOS;
   Bool_t          piminus_Dsminus_Hlt1TrackAllL0Decision_Dec;
   Bool_t          piminus_Dsminus_Hlt1TrackAllL0Decision_TIS;
   Bool_t          piminus_Dsminus_Hlt1TrackAllL0Decision_TOS;
   Bool_t          piminus_Dsminus_Hlt2Topo2BodyBBDTDecision_Dec;
   Bool_t          piminus_Dsminus_Hlt2Topo2BodyBBDTDecision_TIS;
   Bool_t          piminus_Dsminus_Hlt2Topo2BodyBBDTDecision_TOS;
   Bool_t          piminus_Dsminus_Hlt2Topo3BodyBBDTDecision_Dec;
   Bool_t          piminus_Dsminus_Hlt2Topo3BodyBBDTDecision_TIS;
   Bool_t          piminus_Dsminus_Hlt2Topo3BodyBBDTDecision_TOS;
   Bool_t          piminus_Dsminus_Hlt2Topo4BodyBBDTDecision_Dec;
   Bool_t          piminus_Dsminus_Hlt2Topo4BodyBBDTDecision_TIS;
   Bool_t          piminus_Dsminus_Hlt2Topo4BodyBBDTDecision_TOS;
   Bool_t          piminus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_Dec;
   Bool_t          piminus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_TIS;
   Bool_t          piminus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_TOS;
   Bool_t          piminus_Dsminus_Hlt2Topo2BodyDecision_Dec;
   Bool_t          piminus_Dsminus_Hlt2Topo2BodyDecision_TIS;
   Bool_t          piminus_Dsminus_Hlt2Topo2BodyDecision_TOS;
   Bool_t          piminus_Dsminus_Hlt2Topo3BodyDecision_Dec;
   Bool_t          piminus_Dsminus_Hlt2Topo3BodyDecision_TIS;
   Bool_t          piminus_Dsminus_Hlt2Topo3BodyDecision_TOS;
   Bool_t          piminus_Dsminus_Hlt2Topo4BodyDecision_Dec;
   Bool_t          piminus_Dsminus_Hlt2Topo4BodyDecision_TIS;
   Bool_t          piminus_Dsminus_Hlt2Topo4BodyDecision_TOS;
   Bool_t          piminus_Dsminus_Hlt2GlobalDecision_Dec;
   Bool_t          piminus_Dsminus_Hlt2GlobalDecision_TIS;
   Bool_t          piminus_Dsminus_Hlt2GlobalDecision_TOS;
   Int_t           piminus_Dsminus_TRACK_Type;
   Int_t           piminus_Dsminus_TRACK_Key;
   Double_t        piminus_Dsminus_TRACK_CHI2NDOF;
   Double_t        piminus_Dsminus_TRACK_PCHI2;
   Double_t        piminus_Dsminus_TRACK_MatchCHI2;
   Double_t        piminus_Dsminus_TRACK_GhostProb;
   Double_t        piminus_Dsminus_TRACK_CloneDist;
   Double_t        piminus_Dsminus_TRACK_Likelihood;
   Double_t        piminus_Dsminus_X;
   Double_t        piminus_Dsminus_Y;
   Double_t        Dplus_ETA;
   Double_t        Dplus_MinIPCHI2;
   Double_t        Dplus_CosTheta;
   Double_t        Dplus_ENDVERTEX_X;
   Double_t        Dplus_ENDVERTEX_Y;
   Double_t        Dplus_ENDVERTEX_Z;
   Double_t        Dplus_ENDVERTEX_XERR;
   Double_t        Dplus_ENDVERTEX_YERR;
   Double_t        Dplus_ENDVERTEX_ZERR;
   Double_t        Dplus_ENDVERTEX_CHI2;
   Int_t           Dplus_ENDVERTEX_NDOF;
   Float_t         Dplus_ENDVERTEX_COV_[3][3];
   Double_t        Dplus_OWNPV_X;
   Double_t        Dplus_OWNPV_Y;
   Double_t        Dplus_OWNPV_Z;
   Double_t        Dplus_OWNPV_XERR;
   Double_t        Dplus_OWNPV_YERR;
   Double_t        Dplus_OWNPV_ZERR;
   Double_t        Dplus_OWNPV_CHI2;
   Int_t           Dplus_OWNPV_NDOF;
   Float_t         Dplus_OWNPV_COV_[3][3];
   Double_t        Dplus_IP_OWNPV;
   Double_t        Dplus_IPCHI2_OWNPV;
   Double_t        Dplus_FD_OWNPV;
   Double_t        Dplus_FDCHI2_OWNPV;
   Double_t        Dplus_DIRA_OWNPV;
   Double_t        Dplus_ORIVX_X;
   Double_t        Dplus_ORIVX_Y;
   Double_t        Dplus_ORIVX_Z;
   Double_t        Dplus_ORIVX_XERR;
   Double_t        Dplus_ORIVX_YERR;
   Double_t        Dplus_ORIVX_ZERR;
   Double_t        Dplus_ORIVX_CHI2;
   Int_t           Dplus_ORIVX_NDOF;
   Float_t         Dplus_ORIVX_COV_[3][3];
   Double_t        Dplus_FD_ORIVX;
   Double_t        Dplus_FDCHI2_ORIVX;
   Double_t        Dplus_DIRA_ORIVX;
   Double_t        Dplus_P;
   Double_t        Dplus_PT;
   Double_t        Dplus_PE;
   Double_t        Dplus_PX;
   Double_t        Dplus_PY;
   Double_t        Dplus_PZ;
   Double_t        Dplus_REFPX;
   Double_t        Dplus_REFPY;
   Double_t        Dplus_REFPZ;
   Double_t        Dplus_MM;
   Double_t        Dplus_MMERR;
   Double_t        Dplus_M;
   Int_t           Dplus_BKGCAT;
   Int_t           Dplus_TRUEID;
   Double_t        Dplus_TRUEP_E;
   Double_t        Dplus_TRUEP_X;
   Double_t        Dplus_TRUEP_Y;
   Double_t        Dplus_TRUEP_Z;
   Double_t        Dplus_TRUEPT;
   Double_t        Dplus_TRUEORIGINVERTEX_X;
   Double_t        Dplus_TRUEORIGINVERTEX_Y;
   Double_t        Dplus_TRUEORIGINVERTEX_Z;
   Double_t        Dplus_TRUEENDVERTEX_X;
   Double_t        Dplus_TRUEENDVERTEX_Y;
   Double_t        Dplus_TRUEENDVERTEX_Z;
   Bool_t          Dplus_TRUEISSTABLE;
   Double_t        Dplus_TRUETAU;
   Int_t           Dplus_ID;
   Double_t        Dplus_TAU;
   Double_t        Dplus_TAUERR;
   Double_t        Dplus_TAUCHI2;
   Bool_t          Dplus_L0Global_Dec;
   Bool_t          Dplus_L0Global_TIS;
   Bool_t          Dplus_L0Global_TOS;
   Bool_t          Dplus_Hlt1Global_Dec;
   Bool_t          Dplus_Hlt1Global_TIS;
   Bool_t          Dplus_Hlt1Global_TOS;
   Bool_t          Dplus_Hlt1Phys_Dec;
   Bool_t          Dplus_Hlt1Phys_TIS;
   Bool_t          Dplus_Hlt1Phys_TOS;
   Bool_t          Dplus_Hlt2Global_Dec;
   Bool_t          Dplus_Hlt2Global_TIS;
   Bool_t          Dplus_Hlt2Global_TOS;
   Bool_t          Dplus_Hlt2Phys_Dec;
   Bool_t          Dplus_Hlt2Phys_TIS;
   Bool_t          Dplus_Hlt2Phys_TOS;
   Bool_t          Dplus_L0HadronDecision_Dec;
   Bool_t          Dplus_L0HadronDecision_TIS;
   Bool_t          Dplus_L0HadronDecision_TOS;
   Bool_t          Dplus_L0ElectronDecision_Dec;
   Bool_t          Dplus_L0ElectronDecision_TIS;
   Bool_t          Dplus_L0ElectronDecision_TOS;
   Bool_t          Dplus_L0ElectronHiDecision_Dec;
   Bool_t          Dplus_L0ElectronHiDecision_TIS;
   Bool_t          Dplus_L0ElectronHiDecision_TOS;
   Bool_t          Dplus_L0MuonDecision_Dec;
   Bool_t          Dplus_L0MuonDecision_TIS;
   Bool_t          Dplus_L0MuonDecision_TOS;
   Bool_t          Dplus_L0DiMuonDecision_Dec;
   Bool_t          Dplus_L0DiMuonDecision_TIS;
   Bool_t          Dplus_L0DiMuonDecision_TOS;
   Bool_t          Dplus_L0MuonHighDecision_Dec;
   Bool_t          Dplus_L0MuonHighDecision_TIS;
   Bool_t          Dplus_L0MuonHighDecision_TOS;
   Bool_t          Dplus_Hlt1TrackMVADecision_Dec;
   Bool_t          Dplus_Hlt1TrackMVADecision_TIS;
   Bool_t          Dplus_Hlt1TrackMVADecision_TOS;
   Bool_t          Dplus_Hlt1TwoTrackMVADecision_Dec;
   Bool_t          Dplus_Hlt1TwoTrackMVADecision_TIS;
   Bool_t          Dplus_Hlt1TwoTrackMVADecision_TOS;
   Bool_t          Dplus_Hlt1GlobalDecision_Dec;
   Bool_t          Dplus_Hlt1GlobalDecision_TIS;
   Bool_t          Dplus_Hlt1GlobalDecision_TOS;
   Bool_t          Dplus_Hlt1TrackAllL0Decision_Dec;
   Bool_t          Dplus_Hlt1TrackAllL0Decision_TIS;
   Bool_t          Dplus_Hlt1TrackAllL0Decision_TOS;
   Bool_t          Dplus_Hlt2Topo2BodyBBDTDecision_Dec;
   Bool_t          Dplus_Hlt2Topo2BodyBBDTDecision_TIS;
   Bool_t          Dplus_Hlt2Topo2BodyBBDTDecision_TOS;
   Bool_t          Dplus_Hlt2Topo3BodyBBDTDecision_Dec;
   Bool_t          Dplus_Hlt2Topo3BodyBBDTDecision_TIS;
   Bool_t          Dplus_Hlt2Topo3BodyBBDTDecision_TOS;
   Bool_t          Dplus_Hlt2Topo4BodyBBDTDecision_Dec;
   Bool_t          Dplus_Hlt2Topo4BodyBBDTDecision_TIS;
   Bool_t          Dplus_Hlt2Topo4BodyBBDTDecision_TOS;
   Bool_t          Dplus_Hlt2BHadB02PpPpPmPmDecision_Dec;
   Bool_t          Dplus_Hlt2BHadB02PpPpPmPmDecision_TIS;
   Bool_t          Dplus_Hlt2BHadB02PpPpPmPmDecision_TOS;
   Bool_t          Dplus_Hlt2Topo2BodyDecision_Dec;
   Bool_t          Dplus_Hlt2Topo2BodyDecision_TIS;
   Bool_t          Dplus_Hlt2Topo2BodyDecision_TOS;
   Bool_t          Dplus_Hlt2Topo3BodyDecision_Dec;
   Bool_t          Dplus_Hlt2Topo3BodyDecision_TIS;
   Bool_t          Dplus_Hlt2Topo3BodyDecision_TOS;
   Bool_t          Dplus_Hlt2Topo4BodyDecision_Dec;
   Bool_t          Dplus_Hlt2Topo4BodyDecision_TIS;
   Bool_t          Dplus_Hlt2Topo4BodyDecision_TOS;
   Bool_t          Dplus_Hlt2GlobalDecision_Dec;
   Bool_t          Dplus_Hlt2GlobalDecision_TIS;
   Bool_t          Dplus_Hlt2GlobalDecision_TOS;
   Double_t        Dplus_X;
   Double_t        Dplus_Y;
   Double_t        Kminus_Dplus_ETA;
   Double_t        Kminus_Dplus_MinIPCHI2;
   Double_t        Kminus_Dplus_MC12TuneV2_ProbNNe;
   Double_t        Kminus_Dplus_MC12TuneV2_ProbNNmu;
   Double_t        Kminus_Dplus_MC12TuneV2_ProbNNpi;
   Double_t        Kminus_Dplus_MC12TuneV2_ProbNNk;
   Double_t        Kminus_Dplus_MC12TuneV2_ProbNNp;
   Double_t        Kminus_Dplus_MC12TuneV2_ProbNNghost;
   Double_t        Kminus_Dplus_MC12TuneV3_ProbNNe;
   Double_t        Kminus_Dplus_MC12TuneV3_ProbNNmu;
   Double_t        Kminus_Dplus_MC12TuneV3_ProbNNpi;
   Double_t        Kminus_Dplus_MC12TuneV3_ProbNNk;
   Double_t        Kminus_Dplus_MC12TuneV3_ProbNNp;
   Double_t        Kminus_Dplus_MC12TuneV3_ProbNNghost;
   Double_t        Kminus_Dplus_MC12TuneV4_ProbNNe;
   Double_t        Kminus_Dplus_MC12TuneV4_ProbNNmu;
   Double_t        Kminus_Dplus_MC12TuneV4_ProbNNpi;
   Double_t        Kminus_Dplus_MC12TuneV4_ProbNNk;
   Double_t        Kminus_Dplus_MC12TuneV4_ProbNNp;
   Double_t        Kminus_Dplus_MC12TuneV4_ProbNNghost;
   Double_t        Kminus_Dplus_MC15TuneV1_ProbNNe;
   Double_t        Kminus_Dplus_MC15TuneV1_ProbNNmu;
   Double_t        Kminus_Dplus_MC15TuneV1_ProbNNpi;
   Double_t        Kminus_Dplus_MC15TuneV1_ProbNNk;
   Double_t        Kminus_Dplus_MC15TuneV1_ProbNNp;
   Double_t        Kminus_Dplus_MC15TuneV1_ProbNNghost;
   Double_t        Kminus_Dplus_CosTheta;
   Double_t        Kminus_Dplus_OWNPV_X;
   Double_t        Kminus_Dplus_OWNPV_Y;
   Double_t        Kminus_Dplus_OWNPV_Z;
   Double_t        Kminus_Dplus_OWNPV_XERR;
   Double_t        Kminus_Dplus_OWNPV_YERR;
   Double_t        Kminus_Dplus_OWNPV_ZERR;
   Double_t        Kminus_Dplus_OWNPV_CHI2;
   Int_t           Kminus_Dplus_OWNPV_NDOF;
   Float_t         Kminus_Dplus_OWNPV_COV_[3][3];
   Double_t        Kminus_Dplus_IP_OWNPV;
   Double_t        Kminus_Dplus_IPCHI2_OWNPV;
   Double_t        Kminus_Dplus_ORIVX_X;
   Double_t        Kminus_Dplus_ORIVX_Y;
   Double_t        Kminus_Dplus_ORIVX_Z;
   Double_t        Kminus_Dplus_ORIVX_XERR;
   Double_t        Kminus_Dplus_ORIVX_YERR;
   Double_t        Kminus_Dplus_ORIVX_ZERR;
   Double_t        Kminus_Dplus_ORIVX_CHI2;
   Int_t           Kminus_Dplus_ORIVX_NDOF;
   Float_t         Kminus_Dplus_ORIVX_COV_[3][3];
   Double_t        Kminus_Dplus_P;
   Double_t        Kminus_Dplus_PT;
   Double_t        Kminus_Dplus_PE;
   Double_t        Kminus_Dplus_PX;
   Double_t        Kminus_Dplus_PY;
   Double_t        Kminus_Dplus_PZ;
   Double_t        Kminus_Dplus_REFPX;
   Double_t        Kminus_Dplus_REFPY;
   Double_t        Kminus_Dplus_REFPZ;
   Double_t        Kminus_Dplus_M;
   Double_t        Kminus_Dplus_AtVtx_PE;
   Double_t        Kminus_Dplus_AtVtx_PX;
   Double_t        Kminus_Dplus_AtVtx_PY;
   Double_t        Kminus_Dplus_AtVtx_PZ;
   Int_t           Kminus_Dplus_TRUEID;
   Double_t        Kminus_Dplus_TRUEP_E;
   Double_t        Kminus_Dplus_TRUEP_X;
   Double_t        Kminus_Dplus_TRUEP_Y;
   Double_t        Kminus_Dplus_TRUEP_Z;
   Double_t        Kminus_Dplus_TRUEPT;
   Double_t        Kminus_Dplus_TRUEORIGINVERTEX_X;
   Double_t        Kminus_Dplus_TRUEORIGINVERTEX_Y;
   Double_t        Kminus_Dplus_TRUEORIGINVERTEX_Z;
   Double_t        Kminus_Dplus_TRUEENDVERTEX_X;
   Double_t        Kminus_Dplus_TRUEENDVERTEX_Y;
   Double_t        Kminus_Dplus_TRUEENDVERTEX_Z;
   Bool_t          Kminus_Dplus_TRUEISSTABLE;
   Double_t        Kminus_Dplus_TRUETAU;
   Int_t           Kminus_Dplus_ID;
   Double_t        Kminus_Dplus_CombDLLMu;
   Double_t        Kminus_Dplus_ProbNNmu;
   Double_t        Kminus_Dplus_ProbNNghost;
   Double_t        Kminus_Dplus_InMuonAcc;
   Double_t        Kminus_Dplus_MuonDist2;
   Int_t           Kminus_Dplus_regionInM2;
   Bool_t          Kminus_Dplus_hasMuon;
   Bool_t          Kminus_Dplus_isMuon;
   Bool_t          Kminus_Dplus_isMuonLoose;
   Int_t           Kminus_Dplus_NShared;
   Double_t        Kminus_Dplus_MuonLLmu;
   Double_t        Kminus_Dplus_MuonLLbg;
   Int_t           Kminus_Dplus_isMuonFromProto;
   Double_t        Kminus_Dplus_PIDe;
   Double_t        Kminus_Dplus_PIDmu;
   Double_t        Kminus_Dplus_PIDK;
   Double_t        Kminus_Dplus_PIDp;
   Double_t        Kminus_Dplus_PIDd;
   Double_t        Kminus_Dplus_ProbNNe;
   Double_t        Kminus_Dplus_ProbNNk;
   Double_t        Kminus_Dplus_ProbNNp;
   Double_t        Kminus_Dplus_ProbNNpi;
   Double_t        Kminus_Dplus_ProbNNd;
   Bool_t          Kminus_Dplus_hasRich;
   Bool_t          Kminus_Dplus_UsedRichAerogel;
   Bool_t          Kminus_Dplus_UsedRich1Gas;
   Bool_t          Kminus_Dplus_UsedRich2Gas;
   Bool_t          Kminus_Dplus_RichAboveElThres;
   Bool_t          Kminus_Dplus_RichAboveMuThres;
   Bool_t          Kminus_Dplus_RichAbovePiThres;
   Bool_t          Kminus_Dplus_RichAboveKaThres;
   Bool_t          Kminus_Dplus_RichAbovePrThres;
   Bool_t          Kminus_Dplus_hasCalo;
   Bool_t          Kminus_Dplus_L0Global_Dec;
   Bool_t          Kminus_Dplus_L0Global_TIS;
   Bool_t          Kminus_Dplus_L0Global_TOS;
   Bool_t          Kminus_Dplus_Hlt1Global_Dec;
   Bool_t          Kminus_Dplus_Hlt1Global_TIS;
   Bool_t          Kminus_Dplus_Hlt1Global_TOS;
   Bool_t          Kminus_Dplus_Hlt1Phys_Dec;
   Bool_t          Kminus_Dplus_Hlt1Phys_TIS;
   Bool_t          Kminus_Dplus_Hlt1Phys_TOS;
   Bool_t          Kminus_Dplus_Hlt2Global_Dec;
   Bool_t          Kminus_Dplus_Hlt2Global_TIS;
   Bool_t          Kminus_Dplus_Hlt2Global_TOS;
   Bool_t          Kminus_Dplus_Hlt2Phys_Dec;
   Bool_t          Kminus_Dplus_Hlt2Phys_TIS;
   Bool_t          Kminus_Dplus_Hlt2Phys_TOS;
   Bool_t          Kminus_Dplus_L0HadronDecision_Dec;
   Bool_t          Kminus_Dplus_L0HadronDecision_TIS;
   Bool_t          Kminus_Dplus_L0HadronDecision_TOS;
   Bool_t          Kminus_Dplus_L0ElectronDecision_Dec;
   Bool_t          Kminus_Dplus_L0ElectronDecision_TIS;
   Bool_t          Kminus_Dplus_L0ElectronDecision_TOS;
   Bool_t          Kminus_Dplus_L0ElectronHiDecision_Dec;
   Bool_t          Kminus_Dplus_L0ElectronHiDecision_TIS;
   Bool_t          Kminus_Dplus_L0ElectronHiDecision_TOS;
   Bool_t          Kminus_Dplus_L0MuonDecision_Dec;
   Bool_t          Kminus_Dplus_L0MuonDecision_TIS;
   Bool_t          Kminus_Dplus_L0MuonDecision_TOS;
   Bool_t          Kminus_Dplus_L0DiMuonDecision_Dec;
   Bool_t          Kminus_Dplus_L0DiMuonDecision_TIS;
   Bool_t          Kminus_Dplus_L0DiMuonDecision_TOS;
   Bool_t          Kminus_Dplus_L0MuonHighDecision_Dec;
   Bool_t          Kminus_Dplus_L0MuonHighDecision_TIS;
   Bool_t          Kminus_Dplus_L0MuonHighDecision_TOS;
   Bool_t          Kminus_Dplus_Hlt1TrackMVADecision_Dec;
   Bool_t          Kminus_Dplus_Hlt1TrackMVADecision_TIS;
   Bool_t          Kminus_Dplus_Hlt1TrackMVADecision_TOS;
   Bool_t          Kminus_Dplus_Hlt1TwoTrackMVADecision_Dec;
   Bool_t          Kminus_Dplus_Hlt1TwoTrackMVADecision_TIS;
   Bool_t          Kminus_Dplus_Hlt1TwoTrackMVADecision_TOS;
   Bool_t          Kminus_Dplus_Hlt1GlobalDecision_Dec;
   Bool_t          Kminus_Dplus_Hlt1GlobalDecision_TIS;
   Bool_t          Kminus_Dplus_Hlt1GlobalDecision_TOS;
   Bool_t          Kminus_Dplus_Hlt1TrackAllL0Decision_Dec;
   Bool_t          Kminus_Dplus_Hlt1TrackAllL0Decision_TIS;
   Bool_t          Kminus_Dplus_Hlt1TrackAllL0Decision_TOS;
   Bool_t          Kminus_Dplus_Hlt2Topo2BodyBBDTDecision_Dec;
   Bool_t          Kminus_Dplus_Hlt2Topo2BodyBBDTDecision_TIS;
   Bool_t          Kminus_Dplus_Hlt2Topo2BodyBBDTDecision_TOS;
   Bool_t          Kminus_Dplus_Hlt2Topo3BodyBBDTDecision_Dec;
   Bool_t          Kminus_Dplus_Hlt2Topo3BodyBBDTDecision_TIS;
   Bool_t          Kminus_Dplus_Hlt2Topo3BodyBBDTDecision_TOS;
   Bool_t          Kminus_Dplus_Hlt2Topo4BodyBBDTDecision_Dec;
   Bool_t          Kminus_Dplus_Hlt2Topo4BodyBBDTDecision_TIS;
   Bool_t          Kminus_Dplus_Hlt2Topo4BodyBBDTDecision_TOS;
   Bool_t          Kminus_Dplus_Hlt2BHadB02PpPpPmPmDecision_Dec;
   Bool_t          Kminus_Dplus_Hlt2BHadB02PpPpPmPmDecision_TIS;
   Bool_t          Kminus_Dplus_Hlt2BHadB02PpPpPmPmDecision_TOS;
   Bool_t          Kminus_Dplus_Hlt2Topo2BodyDecision_Dec;
   Bool_t          Kminus_Dplus_Hlt2Topo2BodyDecision_TIS;
   Bool_t          Kminus_Dplus_Hlt2Topo2BodyDecision_TOS;
   Bool_t          Kminus_Dplus_Hlt2Topo3BodyDecision_Dec;
   Bool_t          Kminus_Dplus_Hlt2Topo3BodyDecision_TIS;
   Bool_t          Kminus_Dplus_Hlt2Topo3BodyDecision_TOS;
   Bool_t          Kminus_Dplus_Hlt2Topo4BodyDecision_Dec;
   Bool_t          Kminus_Dplus_Hlt2Topo4BodyDecision_TIS;
   Bool_t          Kminus_Dplus_Hlt2Topo4BodyDecision_TOS;
   Bool_t          Kminus_Dplus_Hlt2GlobalDecision_Dec;
   Bool_t          Kminus_Dplus_Hlt2GlobalDecision_TIS;
   Bool_t          Kminus_Dplus_Hlt2GlobalDecision_TOS;
   Int_t           Kminus_Dplus_TRACK_Type;
   Int_t           Kminus_Dplus_TRACK_Key;
   Double_t        Kminus_Dplus_TRACK_CHI2NDOF;
   Double_t        Kminus_Dplus_TRACK_PCHI2;
   Double_t        Kminus_Dplus_TRACK_MatchCHI2;
   Double_t        Kminus_Dplus_TRACK_GhostProb;
   Double_t        Kminus_Dplus_TRACK_CloneDist;
   Double_t        Kminus_Dplus_TRACK_Likelihood;
   Double_t        Kminus_Dplus_X;
   Double_t        Kminus_Dplus_Y;
   Double_t        piplus1_Dplus_ETA;
   Double_t        piplus1_Dplus_MinIPCHI2;
   Double_t        piplus1_Dplus_MC12TuneV2_ProbNNe;
   Double_t        piplus1_Dplus_MC12TuneV2_ProbNNmu;
   Double_t        piplus1_Dplus_MC12TuneV2_ProbNNpi;
   Double_t        piplus1_Dplus_MC12TuneV2_ProbNNk;
   Double_t        piplus1_Dplus_MC12TuneV2_ProbNNp;
   Double_t        piplus1_Dplus_MC12TuneV2_ProbNNghost;
   Double_t        piplus1_Dplus_MC12TuneV3_ProbNNe;
   Double_t        piplus1_Dplus_MC12TuneV3_ProbNNmu;
   Double_t        piplus1_Dplus_MC12TuneV3_ProbNNpi;
   Double_t        piplus1_Dplus_MC12TuneV3_ProbNNk;
   Double_t        piplus1_Dplus_MC12TuneV3_ProbNNp;
   Double_t        piplus1_Dplus_MC12TuneV3_ProbNNghost;
   Double_t        piplus1_Dplus_MC12TuneV4_ProbNNe;
   Double_t        piplus1_Dplus_MC12TuneV4_ProbNNmu;
   Double_t        piplus1_Dplus_MC12TuneV4_ProbNNpi;
   Double_t        piplus1_Dplus_MC12TuneV4_ProbNNk;
   Double_t        piplus1_Dplus_MC12TuneV4_ProbNNp;
   Double_t        piplus1_Dplus_MC12TuneV4_ProbNNghost;
   Double_t        piplus1_Dplus_MC15TuneV1_ProbNNe;
   Double_t        piplus1_Dplus_MC15TuneV1_ProbNNmu;
   Double_t        piplus1_Dplus_MC15TuneV1_ProbNNpi;
   Double_t        piplus1_Dplus_MC15TuneV1_ProbNNk;
   Double_t        piplus1_Dplus_MC15TuneV1_ProbNNp;
   Double_t        piplus1_Dplus_MC15TuneV1_ProbNNghost;
   Double_t        piplus1_Dplus_CosTheta;
   Double_t        piplus1_Dplus_OWNPV_X;
   Double_t        piplus1_Dplus_OWNPV_Y;
   Double_t        piplus1_Dplus_OWNPV_Z;
   Double_t        piplus1_Dplus_OWNPV_XERR;
   Double_t        piplus1_Dplus_OWNPV_YERR;
   Double_t        piplus1_Dplus_OWNPV_ZERR;
   Double_t        piplus1_Dplus_OWNPV_CHI2;
   Int_t           piplus1_Dplus_OWNPV_NDOF;
   Float_t         piplus1_Dplus_OWNPV_COV_[3][3];
   Double_t        piplus1_Dplus_IP_OWNPV;
   Double_t        piplus1_Dplus_IPCHI2_OWNPV;
   Double_t        piplus1_Dplus_ORIVX_X;
   Double_t        piplus1_Dplus_ORIVX_Y;
   Double_t        piplus1_Dplus_ORIVX_Z;
   Double_t        piplus1_Dplus_ORIVX_XERR;
   Double_t        piplus1_Dplus_ORIVX_YERR;
   Double_t        piplus1_Dplus_ORIVX_ZERR;
   Double_t        piplus1_Dplus_ORIVX_CHI2;
   Int_t           piplus1_Dplus_ORIVX_NDOF;
   Float_t         piplus1_Dplus_ORIVX_COV_[3][3];
   Double_t        piplus1_Dplus_P;
   Double_t        piplus1_Dplus_PT;
   Double_t        piplus1_Dplus_PE;
   Double_t        piplus1_Dplus_PX;
   Double_t        piplus1_Dplus_PY;
   Double_t        piplus1_Dplus_PZ;
   Double_t        piplus1_Dplus_REFPX;
   Double_t        piplus1_Dplus_REFPY;
   Double_t        piplus1_Dplus_REFPZ;
   Double_t        piplus1_Dplus_M;
   Double_t        piplus1_Dplus_AtVtx_PE;
   Double_t        piplus1_Dplus_AtVtx_PX;
   Double_t        piplus1_Dplus_AtVtx_PY;
   Double_t        piplus1_Dplus_AtVtx_PZ;
   Int_t           piplus1_Dplus_TRUEID;
   Double_t        piplus1_Dplus_TRUEP_E;
   Double_t        piplus1_Dplus_TRUEP_X;
   Double_t        piplus1_Dplus_TRUEP_Y;
   Double_t        piplus1_Dplus_TRUEP_Z;
   Double_t        piplus1_Dplus_TRUEPT;
   Double_t        piplus1_Dplus_TRUEORIGINVERTEX_X;
   Double_t        piplus1_Dplus_TRUEORIGINVERTEX_Y;
   Double_t        piplus1_Dplus_TRUEORIGINVERTEX_Z;
   Double_t        piplus1_Dplus_TRUEENDVERTEX_X;
   Double_t        piplus1_Dplus_TRUEENDVERTEX_Y;
   Double_t        piplus1_Dplus_TRUEENDVERTEX_Z;
   Bool_t          piplus1_Dplus_TRUEISSTABLE;
   Double_t        piplus1_Dplus_TRUETAU;
   Int_t           piplus1_Dplus_ID;
   Double_t        piplus1_Dplus_CombDLLMu;
   Double_t        piplus1_Dplus_ProbNNmu;
   Double_t        piplus1_Dplus_ProbNNghost;
   Double_t        piplus1_Dplus_InMuonAcc;
   Double_t        piplus1_Dplus_MuonDist2;
   Int_t           piplus1_Dplus_regionInM2;
   Bool_t          piplus1_Dplus_hasMuon;
   Bool_t          piplus1_Dplus_isMuon;
   Bool_t          piplus1_Dplus_isMuonLoose;
   Int_t           piplus1_Dplus_NShared;
   Double_t        piplus1_Dplus_MuonLLmu;
   Double_t        piplus1_Dplus_MuonLLbg;
   Int_t           piplus1_Dplus_isMuonFromProto;
   Double_t        piplus1_Dplus_PIDe;
   Double_t        piplus1_Dplus_PIDmu;
   Double_t        piplus1_Dplus_PIDK;
   Double_t        piplus1_Dplus_PIDp;
   Double_t        piplus1_Dplus_PIDd;
   Double_t        piplus1_Dplus_ProbNNe;
   Double_t        piplus1_Dplus_ProbNNk;
   Double_t        piplus1_Dplus_ProbNNp;
   Double_t        piplus1_Dplus_ProbNNpi;
   Double_t        piplus1_Dplus_ProbNNd;
   Bool_t          piplus1_Dplus_hasRich;
   Bool_t          piplus1_Dplus_UsedRichAerogel;
   Bool_t          piplus1_Dplus_UsedRich1Gas;
   Bool_t          piplus1_Dplus_UsedRich2Gas;
   Bool_t          piplus1_Dplus_RichAboveElThres;
   Bool_t          piplus1_Dplus_RichAboveMuThres;
   Bool_t          piplus1_Dplus_RichAbovePiThres;
   Bool_t          piplus1_Dplus_RichAboveKaThres;
   Bool_t          piplus1_Dplus_RichAbovePrThres;
   Bool_t          piplus1_Dplus_hasCalo;
   Bool_t          piplus1_Dplus_L0Global_Dec;
   Bool_t          piplus1_Dplus_L0Global_TIS;
   Bool_t          piplus1_Dplus_L0Global_TOS;
   Bool_t          piplus1_Dplus_Hlt1Global_Dec;
   Bool_t          piplus1_Dplus_Hlt1Global_TIS;
   Bool_t          piplus1_Dplus_Hlt1Global_TOS;
   Bool_t          piplus1_Dplus_Hlt1Phys_Dec;
   Bool_t          piplus1_Dplus_Hlt1Phys_TIS;
   Bool_t          piplus1_Dplus_Hlt1Phys_TOS;
   Bool_t          piplus1_Dplus_Hlt2Global_Dec;
   Bool_t          piplus1_Dplus_Hlt2Global_TIS;
   Bool_t          piplus1_Dplus_Hlt2Global_TOS;
   Bool_t          piplus1_Dplus_Hlt2Phys_Dec;
   Bool_t          piplus1_Dplus_Hlt2Phys_TIS;
   Bool_t          piplus1_Dplus_Hlt2Phys_TOS;
   Bool_t          piplus1_Dplus_L0HadronDecision_Dec;
   Bool_t          piplus1_Dplus_L0HadronDecision_TIS;
   Bool_t          piplus1_Dplus_L0HadronDecision_TOS;
   Bool_t          piplus1_Dplus_L0ElectronDecision_Dec;
   Bool_t          piplus1_Dplus_L0ElectronDecision_TIS;
   Bool_t          piplus1_Dplus_L0ElectronDecision_TOS;
   Bool_t          piplus1_Dplus_L0ElectronHiDecision_Dec;
   Bool_t          piplus1_Dplus_L0ElectronHiDecision_TIS;
   Bool_t          piplus1_Dplus_L0ElectronHiDecision_TOS;
   Bool_t          piplus1_Dplus_L0MuonDecision_Dec;
   Bool_t          piplus1_Dplus_L0MuonDecision_TIS;
   Bool_t          piplus1_Dplus_L0MuonDecision_TOS;
   Bool_t          piplus1_Dplus_L0DiMuonDecision_Dec;
   Bool_t          piplus1_Dplus_L0DiMuonDecision_TIS;
   Bool_t          piplus1_Dplus_L0DiMuonDecision_TOS;
   Bool_t          piplus1_Dplus_L0MuonHighDecision_Dec;
   Bool_t          piplus1_Dplus_L0MuonHighDecision_TIS;
   Bool_t          piplus1_Dplus_L0MuonHighDecision_TOS;
   Bool_t          piplus1_Dplus_Hlt1TrackMVADecision_Dec;
   Bool_t          piplus1_Dplus_Hlt1TrackMVADecision_TIS;
   Bool_t          piplus1_Dplus_Hlt1TrackMVADecision_TOS;
   Bool_t          piplus1_Dplus_Hlt1TwoTrackMVADecision_Dec;
   Bool_t          piplus1_Dplus_Hlt1TwoTrackMVADecision_TIS;
   Bool_t          piplus1_Dplus_Hlt1TwoTrackMVADecision_TOS;
   Bool_t          piplus1_Dplus_Hlt1GlobalDecision_Dec;
   Bool_t          piplus1_Dplus_Hlt1GlobalDecision_TIS;
   Bool_t          piplus1_Dplus_Hlt1GlobalDecision_TOS;
   Bool_t          piplus1_Dplus_Hlt1TrackAllL0Decision_Dec;
   Bool_t          piplus1_Dplus_Hlt1TrackAllL0Decision_TIS;
   Bool_t          piplus1_Dplus_Hlt1TrackAllL0Decision_TOS;
   Bool_t          piplus1_Dplus_Hlt2Topo2BodyBBDTDecision_Dec;
   Bool_t          piplus1_Dplus_Hlt2Topo2BodyBBDTDecision_TIS;
   Bool_t          piplus1_Dplus_Hlt2Topo2BodyBBDTDecision_TOS;
   Bool_t          piplus1_Dplus_Hlt2Topo3BodyBBDTDecision_Dec;
   Bool_t          piplus1_Dplus_Hlt2Topo3BodyBBDTDecision_TIS;
   Bool_t          piplus1_Dplus_Hlt2Topo3BodyBBDTDecision_TOS;
   Bool_t          piplus1_Dplus_Hlt2Topo4BodyBBDTDecision_Dec;
   Bool_t          piplus1_Dplus_Hlt2Topo4BodyBBDTDecision_TIS;
   Bool_t          piplus1_Dplus_Hlt2Topo4BodyBBDTDecision_TOS;
   Bool_t          piplus1_Dplus_Hlt2BHadB02PpPpPmPmDecision_Dec;
   Bool_t          piplus1_Dplus_Hlt2BHadB02PpPpPmPmDecision_TIS;
   Bool_t          piplus1_Dplus_Hlt2BHadB02PpPpPmPmDecision_TOS;
   Bool_t          piplus1_Dplus_Hlt2Topo2BodyDecision_Dec;
   Bool_t          piplus1_Dplus_Hlt2Topo2BodyDecision_TIS;
   Bool_t          piplus1_Dplus_Hlt2Topo2BodyDecision_TOS;
   Bool_t          piplus1_Dplus_Hlt2Topo3BodyDecision_Dec;
   Bool_t          piplus1_Dplus_Hlt2Topo3BodyDecision_TIS;
   Bool_t          piplus1_Dplus_Hlt2Topo3BodyDecision_TOS;
   Bool_t          piplus1_Dplus_Hlt2Topo4BodyDecision_Dec;
   Bool_t          piplus1_Dplus_Hlt2Topo4BodyDecision_TIS;
   Bool_t          piplus1_Dplus_Hlt2Topo4BodyDecision_TOS;
   Bool_t          piplus1_Dplus_Hlt2GlobalDecision_Dec;
   Bool_t          piplus1_Dplus_Hlt2GlobalDecision_TIS;
   Bool_t          piplus1_Dplus_Hlt2GlobalDecision_TOS;
   Int_t           piplus1_Dplus_TRACK_Type;
   Int_t           piplus1_Dplus_TRACK_Key;
   Double_t        piplus1_Dplus_TRACK_CHI2NDOF;
   Double_t        piplus1_Dplus_TRACK_PCHI2;
   Double_t        piplus1_Dplus_TRACK_MatchCHI2;
   Double_t        piplus1_Dplus_TRACK_GhostProb;
   Double_t        piplus1_Dplus_TRACK_CloneDist;
   Double_t        piplus1_Dplus_TRACK_Likelihood;
   Double_t        piplus1_Dplus_X;
   Double_t        piplus1_Dplus_Y;
   Double_t        piplus2_Dplus_ETA;
   Double_t        piplus2_Dplus_MinIPCHI2;
   Double_t        piplus2_Dplus_MC12TuneV2_ProbNNe;
   Double_t        piplus2_Dplus_MC12TuneV2_ProbNNmu;
   Double_t        piplus2_Dplus_MC12TuneV2_ProbNNpi;
   Double_t        piplus2_Dplus_MC12TuneV2_ProbNNk;
   Double_t        piplus2_Dplus_MC12TuneV2_ProbNNp;
   Double_t        piplus2_Dplus_MC12TuneV2_ProbNNghost;
   Double_t        piplus2_Dplus_MC12TuneV3_ProbNNe;
   Double_t        piplus2_Dplus_MC12TuneV3_ProbNNmu;
   Double_t        piplus2_Dplus_MC12TuneV3_ProbNNpi;
   Double_t        piplus2_Dplus_MC12TuneV3_ProbNNk;
   Double_t        piplus2_Dplus_MC12TuneV3_ProbNNp;
   Double_t        piplus2_Dplus_MC12TuneV3_ProbNNghost;
   Double_t        piplus2_Dplus_MC12TuneV4_ProbNNe;
   Double_t        piplus2_Dplus_MC12TuneV4_ProbNNmu;
   Double_t        piplus2_Dplus_MC12TuneV4_ProbNNpi;
   Double_t        piplus2_Dplus_MC12TuneV4_ProbNNk;
   Double_t        piplus2_Dplus_MC12TuneV4_ProbNNp;
   Double_t        piplus2_Dplus_MC12TuneV4_ProbNNghost;
   Double_t        piplus2_Dplus_MC15TuneV1_ProbNNe;
   Double_t        piplus2_Dplus_MC15TuneV1_ProbNNmu;
   Double_t        piplus2_Dplus_MC15TuneV1_ProbNNpi;
   Double_t        piplus2_Dplus_MC15TuneV1_ProbNNk;
   Double_t        piplus2_Dplus_MC15TuneV1_ProbNNp;
   Double_t        piplus2_Dplus_MC15TuneV1_ProbNNghost;
   Double_t        piplus2_Dplus_CosTheta;
   Double_t        piplus2_Dplus_OWNPV_X;
   Double_t        piplus2_Dplus_OWNPV_Y;
   Double_t        piplus2_Dplus_OWNPV_Z;
   Double_t        piplus2_Dplus_OWNPV_XERR;
   Double_t        piplus2_Dplus_OWNPV_YERR;
   Double_t        piplus2_Dplus_OWNPV_ZERR;
   Double_t        piplus2_Dplus_OWNPV_CHI2;
   Int_t           piplus2_Dplus_OWNPV_NDOF;
   Float_t         piplus2_Dplus_OWNPV_COV_[3][3];
   Double_t        piplus2_Dplus_IP_OWNPV;
   Double_t        piplus2_Dplus_IPCHI2_OWNPV;
   Double_t        piplus2_Dplus_ORIVX_X;
   Double_t        piplus2_Dplus_ORIVX_Y;
   Double_t        piplus2_Dplus_ORIVX_Z;
   Double_t        piplus2_Dplus_ORIVX_XERR;
   Double_t        piplus2_Dplus_ORIVX_YERR;
   Double_t        piplus2_Dplus_ORIVX_ZERR;
   Double_t        piplus2_Dplus_ORIVX_CHI2;
   Int_t           piplus2_Dplus_ORIVX_NDOF;
   Float_t         piplus2_Dplus_ORIVX_COV_[3][3];
   Double_t        piplus2_Dplus_P;
   Double_t        piplus2_Dplus_PT;
   Double_t        piplus2_Dplus_PE;
   Double_t        piplus2_Dplus_PX;
   Double_t        piplus2_Dplus_PY;
   Double_t        piplus2_Dplus_PZ;
   Double_t        piplus2_Dplus_REFPX;
   Double_t        piplus2_Dplus_REFPY;
   Double_t        piplus2_Dplus_REFPZ;
   Double_t        piplus2_Dplus_M;
   Double_t        piplus2_Dplus_AtVtx_PE;
   Double_t        piplus2_Dplus_AtVtx_PX;
   Double_t        piplus2_Dplus_AtVtx_PY;
   Double_t        piplus2_Dplus_AtVtx_PZ;
   Int_t           piplus2_Dplus_TRUEID;
   Double_t        piplus2_Dplus_TRUEP_E;
   Double_t        piplus2_Dplus_TRUEP_X;
   Double_t        piplus2_Dplus_TRUEP_Y;
   Double_t        piplus2_Dplus_TRUEP_Z;
   Double_t        piplus2_Dplus_TRUEPT;
   Double_t        piplus2_Dplus_TRUEORIGINVERTEX_X;
   Double_t        piplus2_Dplus_TRUEORIGINVERTEX_Y;
   Double_t        piplus2_Dplus_TRUEORIGINVERTEX_Z;
   Double_t        piplus2_Dplus_TRUEENDVERTEX_X;
   Double_t        piplus2_Dplus_TRUEENDVERTEX_Y;
   Double_t        piplus2_Dplus_TRUEENDVERTEX_Z;
   Bool_t          piplus2_Dplus_TRUEISSTABLE;
   Double_t        piplus2_Dplus_TRUETAU;
   Int_t           piplus2_Dplus_ID;
   Double_t        piplus2_Dplus_CombDLLMu;
   Double_t        piplus2_Dplus_ProbNNmu;
   Double_t        piplus2_Dplus_ProbNNghost;
   Double_t        piplus2_Dplus_InMuonAcc;
   Double_t        piplus2_Dplus_MuonDist2;
   Int_t           piplus2_Dplus_regionInM2;
   Bool_t          piplus2_Dplus_hasMuon;
   Bool_t          piplus2_Dplus_isMuon;
   Bool_t          piplus2_Dplus_isMuonLoose;
   Int_t           piplus2_Dplus_NShared;
   Double_t        piplus2_Dplus_MuonLLmu;
   Double_t        piplus2_Dplus_MuonLLbg;
   Int_t           piplus2_Dplus_isMuonFromProto;
   Double_t        piplus2_Dplus_PIDe;
   Double_t        piplus2_Dplus_PIDmu;
   Double_t        piplus2_Dplus_PIDK;
   Double_t        piplus2_Dplus_PIDp;
   Double_t        piplus2_Dplus_PIDd;
   Double_t        piplus2_Dplus_ProbNNe;
   Double_t        piplus2_Dplus_ProbNNk;
   Double_t        piplus2_Dplus_ProbNNp;
   Double_t        piplus2_Dplus_ProbNNpi;
   Double_t        piplus2_Dplus_ProbNNd;
   Bool_t          piplus2_Dplus_hasRich;
   Bool_t          piplus2_Dplus_UsedRichAerogel;
   Bool_t          piplus2_Dplus_UsedRich1Gas;
   Bool_t          piplus2_Dplus_UsedRich2Gas;
   Bool_t          piplus2_Dplus_RichAboveElThres;
   Bool_t          piplus2_Dplus_RichAboveMuThres;
   Bool_t          piplus2_Dplus_RichAbovePiThres;
   Bool_t          piplus2_Dplus_RichAboveKaThres;
   Bool_t          piplus2_Dplus_RichAbovePrThres;
   Bool_t          piplus2_Dplus_hasCalo;
   Bool_t          piplus2_Dplus_L0Global_Dec;
   Bool_t          piplus2_Dplus_L0Global_TIS;
   Bool_t          piplus2_Dplus_L0Global_TOS;
   Bool_t          piplus2_Dplus_Hlt1Global_Dec;
   Bool_t          piplus2_Dplus_Hlt1Global_TIS;
   Bool_t          piplus2_Dplus_Hlt1Global_TOS;
   Bool_t          piplus2_Dplus_Hlt1Phys_Dec;
   Bool_t          piplus2_Dplus_Hlt1Phys_TIS;
   Bool_t          piplus2_Dplus_Hlt1Phys_TOS;
   Bool_t          piplus2_Dplus_Hlt2Global_Dec;
   Bool_t          piplus2_Dplus_Hlt2Global_TIS;
   Bool_t          piplus2_Dplus_Hlt2Global_TOS;
   Bool_t          piplus2_Dplus_Hlt2Phys_Dec;
   Bool_t          piplus2_Dplus_Hlt2Phys_TIS;
   Bool_t          piplus2_Dplus_Hlt2Phys_TOS;
   Bool_t          piplus2_Dplus_L0HadronDecision_Dec;
   Bool_t          piplus2_Dplus_L0HadronDecision_TIS;
   Bool_t          piplus2_Dplus_L0HadronDecision_TOS;
   Bool_t          piplus2_Dplus_L0ElectronDecision_Dec;
   Bool_t          piplus2_Dplus_L0ElectronDecision_TIS;
   Bool_t          piplus2_Dplus_L0ElectronDecision_TOS;
   Bool_t          piplus2_Dplus_L0ElectronHiDecision_Dec;
   Bool_t          piplus2_Dplus_L0ElectronHiDecision_TIS;
   Bool_t          piplus2_Dplus_L0ElectronHiDecision_TOS;
   Bool_t          piplus2_Dplus_L0MuonDecision_Dec;
   Bool_t          piplus2_Dplus_L0MuonDecision_TIS;
   Bool_t          piplus2_Dplus_L0MuonDecision_TOS;
   Bool_t          piplus2_Dplus_L0DiMuonDecision_Dec;
   Bool_t          piplus2_Dplus_L0DiMuonDecision_TIS;
   Bool_t          piplus2_Dplus_L0DiMuonDecision_TOS;
   Bool_t          piplus2_Dplus_L0MuonHighDecision_Dec;
   Bool_t          piplus2_Dplus_L0MuonHighDecision_TIS;
   Bool_t          piplus2_Dplus_L0MuonHighDecision_TOS;
   Bool_t          piplus2_Dplus_Hlt1TrackMVADecision_Dec;
   Bool_t          piplus2_Dplus_Hlt1TrackMVADecision_TIS;
   Bool_t          piplus2_Dplus_Hlt1TrackMVADecision_TOS;
   Bool_t          piplus2_Dplus_Hlt1TwoTrackMVADecision_Dec;
   Bool_t          piplus2_Dplus_Hlt1TwoTrackMVADecision_TIS;
   Bool_t          piplus2_Dplus_Hlt1TwoTrackMVADecision_TOS;
   Bool_t          piplus2_Dplus_Hlt1GlobalDecision_Dec;
   Bool_t          piplus2_Dplus_Hlt1GlobalDecision_TIS;
   Bool_t          piplus2_Dplus_Hlt1GlobalDecision_TOS;
   Bool_t          piplus2_Dplus_Hlt1TrackAllL0Decision_Dec;
   Bool_t          piplus2_Dplus_Hlt1TrackAllL0Decision_TIS;
   Bool_t          piplus2_Dplus_Hlt1TrackAllL0Decision_TOS;
   Bool_t          piplus2_Dplus_Hlt2Topo2BodyBBDTDecision_Dec;
   Bool_t          piplus2_Dplus_Hlt2Topo2BodyBBDTDecision_TIS;
   Bool_t          piplus2_Dplus_Hlt2Topo2BodyBBDTDecision_TOS;
   Bool_t          piplus2_Dplus_Hlt2Topo3BodyBBDTDecision_Dec;
   Bool_t          piplus2_Dplus_Hlt2Topo3BodyBBDTDecision_TIS;
   Bool_t          piplus2_Dplus_Hlt2Topo3BodyBBDTDecision_TOS;
   Bool_t          piplus2_Dplus_Hlt2Topo4BodyBBDTDecision_Dec;
   Bool_t          piplus2_Dplus_Hlt2Topo4BodyBBDTDecision_TIS;
   Bool_t          piplus2_Dplus_Hlt2Topo4BodyBBDTDecision_TOS;
   Bool_t          piplus2_Dplus_Hlt2BHadB02PpPpPmPmDecision_Dec;
   Bool_t          piplus2_Dplus_Hlt2BHadB02PpPpPmPmDecision_TIS;
   Bool_t          piplus2_Dplus_Hlt2BHadB02PpPpPmPmDecision_TOS;
   Bool_t          piplus2_Dplus_Hlt2Topo2BodyDecision_Dec;
   Bool_t          piplus2_Dplus_Hlt2Topo2BodyDecision_TIS;
   Bool_t          piplus2_Dplus_Hlt2Topo2BodyDecision_TOS;
   Bool_t          piplus2_Dplus_Hlt2Topo3BodyDecision_Dec;
   Bool_t          piplus2_Dplus_Hlt2Topo3BodyDecision_TIS;
   Bool_t          piplus2_Dplus_Hlt2Topo3BodyDecision_TOS;
   Bool_t          piplus2_Dplus_Hlt2Topo4BodyDecision_Dec;
   Bool_t          piplus2_Dplus_Hlt2Topo4BodyDecision_TIS;
   Bool_t          piplus2_Dplus_Hlt2Topo4BodyDecision_TOS;
   Bool_t          piplus2_Dplus_Hlt2GlobalDecision_Dec;
   Bool_t          piplus2_Dplus_Hlt2GlobalDecision_TIS;
   Bool_t          piplus2_Dplus_Hlt2GlobalDecision_TOS;
   Int_t           piplus2_Dplus_TRACK_Type;
   Int_t           piplus2_Dplus_TRACK_Key;
   Double_t        piplus2_Dplus_TRACK_CHI2NDOF;
   Double_t        piplus2_Dplus_TRACK_PCHI2;
   Double_t        piplus2_Dplus_TRACK_MatchCHI2;
   Double_t        piplus2_Dplus_TRACK_GhostProb;
   Double_t        piplus2_Dplus_TRACK_CloneDist;
   Double_t        piplus2_Dplus_TRACK_Likelihood;
   Double_t        piplus2_Dplus_X;
   Double_t        piplus2_Dplus_Y;
   UInt_t          nCandidate;
   ULong64_t       totCandidates;
   ULong64_t       EventInSequence;
   UInt_t          runNumber;
   ULong64_t       eventNumber;
   UInt_t          BCID;
   Int_t           BCType;
   UInt_t          OdinTCK;
   UInt_t          L0DUTCK;
   UInt_t          HLT1TCK;
   UInt_t          HLT2TCK;
   ULong64_t       GpsTime;
   Short_t         Polarity;
   Int_t           L0Data_DiMuon_Pt;
   Int_t           L0Data_DiMuonProd_Pt1Pt2;
   Int_t           L0Data_Electron_Et;
   Int_t           L0Data_GlobalPi0_Et;
   Int_t           L0Data_Hadron_Et;
   Int_t           L0Data_LocalPi0_Et;
   Int_t           L0Data_Muon1_Pt;
   Int_t           L0Data_Muon1_Sgn;
   Int_t           L0Data_Muon2_Pt;
   Int_t           L0Data_Muon2_Sgn;
   Int_t           L0Data_Muon3_Pt;
   Int_t           L0Data_Muon3_Sgn;
   Int_t           L0Data_PUHits_Mult;
   Int_t           L0Data_PUPeak1_Cont;
   Int_t           L0Data_PUPeak1_Pos;
   Int_t           L0Data_PUPeak2_Cont;
   Int_t           L0Data_PUPeak2_Pos;
   Int_t           L0Data_Photon_Et;
   Int_t           L0Data_Spd_Mult;
   Int_t           L0Data_Sum_Et;
   Int_t           L0Data_Sum_Et_Next1;
   Int_t           L0Data_Sum_Et_Next2;
   Int_t           L0Data_Sum_Et_Prev1;
   Int_t           L0Data_Sum_Et_Prev2;
   Int_t           nPV;
   Float_t         PVX[100];   //[nPV]
   Float_t         PVY[100];   //[nPV]
   Float_t         PVZ[100];   //[nPV]
   Float_t         PVXERR[100];   //[nPV]
   Float_t         PVYERR[100];   //[nPV]
   Float_t         PVZERR[100];   //[nPV]
   Float_t         PVCHI2[100];   //[nPV]
   Float_t         PVNDOF[100];   //[nPV]
   Float_t         PVNTRACKS[100];   //[nPV]
   Int_t           nPVs;
   Int_t           nTracks;
   Int_t           nLongTracks;
   Int_t           nDownstreamTracks;
   Int_t           nUpstreamTracks;
   Int_t           nVeloTracks;
   Int_t           nTTracks;
   Int_t           nBackTracks;
   Int_t           nRich1Hits;
   Int_t           nRich2Hits;
   Int_t           nVeloClusters;
   Int_t           nITClusters;
   Int_t           nTTClusters;
   Int_t           nOTClusters;
   Int_t           nSPDHits;
   Int_t           nMuonCoordsS0;
   Int_t           nMuonCoordsS1;
   Int_t           nMuonCoordsS2;
   Int_t           nMuonCoordsS3;
   Int_t           nMuonCoordsS4;
   Int_t           nMuonTracks;
   Double_t        year;
   Double_t        magFlag;
   Double_t        rdmNumber;
   Double_t        m_Dsminus_pi2K;
   Double_t        m_Dsminus_p2K;
   Double_t        m_KK_Dsminus;
   Double_t        m_Dplus_K2pi1;
   Double_t        m_Dplus_K2pi2;
   Double_t        m_Dplus_p2pi1;
   Double_t        m_Dplus_p2pi2;
   Double_t        m_KK_Dplus_K2pi1;
   Double_t        m_KK_Dplus_K2pi2;
   Double_t        Kplus_Dsminus_MC15TuneV1_ProbNNk_corr;
   Double_t        Kminus_Dsminus_MC15TuneV1_ProbNNk_corr;
   Double_t        piminus_Dsminus_MC15TuneV1_ProbNNpi_corr;
   Double_t        Kminus_Dplus_MC15TuneV1_ProbNNk_corr;
   Double_t        piplus1_Dplus_MC15TuneV1_ProbNNpi_corr;
   Double_t        piplus2_Dplus_MC15TuneV1_ProbNNpi_corr;
   Double_t        Kplus_Dsminus_PIDK_corr;
   Double_t        Kminus_Dsminus_PIDK_corr;
   Double_t        Kminus_Dplus_PIDK_corr;
   Double_t        angle_Kminus_Dplus_piplus1_Dplus;
   Double_t        angle_Kminus_Dplus_piplus2_Dplus;
   Double_t        angle_piplus2_Dplus_piplus1_Dplus;
   Double_t        angle_Kplus_Dsminus_Kminus_Dsminus;
   Double_t        angle_Kplus_Dsminus_piminus_Dsminus;
   Double_t        angle_piminus_Dsminus_Kminus_Dsminus;
   Double_t        angle_Kminus_Dplus_Kplus_Dsminus;
   Double_t        angle_Kminus_Dplus_Kminus_Dsminus;
   Double_t        angle_Kminus_Dplus_piminus_Dsminus;
   Double_t        angle_piplus1_Dplus_Kplus_Dsminus;
   Double_t        angle_piplus1_Dplus_Kminus_Dsminus;
   Double_t        angle_piplus1_Dplus_piminus_Dsminus;
   Double_t        angle_piplus2_Dplus_Kplus_Dsminus;
   Double_t        angle_piplus2_Dplus_Kminus_Dsminus;
   Double_t        angle_piplus2_Dplus_piminus_Dsminus;
   Double_t        prodProbNNx;
   Double_t        B_DTFM_M_0;

   // List of branches
   TBranch        *b_B_ETA;   //!
   TBranch        *b_B_MinIPCHI2;   //!
   TBranch        *b_B_DiraAngleError;   //!
   TBranch        *b_B_DiraCosError;   //!
   TBranch        *b_B_DiraAngle;   //!
   TBranch        *b_B_DiraCos;   //!
   TBranch        *b_B_ENDVERTEX_X;   //!
   TBranch        *b_B_ENDVERTEX_Y;   //!
   TBranch        *b_B_ENDVERTEX_Z;   //!
   TBranch        *b_B_ENDVERTEX_XERR;   //!
   TBranch        *b_B_ENDVERTEX_YERR;   //!
   TBranch        *b_B_ENDVERTEX_ZERR;   //!
   TBranch        *b_B_ENDVERTEX_CHI2;   //!
   TBranch        *b_B_ENDVERTEX_NDOF;   //!
   TBranch        *b_B_ENDVERTEX_COV_;   //!
   TBranch        *b_B_OWNPV_X;   //!
   TBranch        *b_B_OWNPV_Y;   //!
   TBranch        *b_B_OWNPV_Z;   //!
   TBranch        *b_B_OWNPV_XERR;   //!
   TBranch        *b_B_OWNPV_YERR;   //!
   TBranch        *b_B_OWNPV_ZERR;   //!
   TBranch        *b_B_OWNPV_CHI2;   //!
   TBranch        *b_B_OWNPV_NDOF;   //!
   TBranch        *b_B_OWNPV_COV_;   //!
   TBranch        *b_B_IP_OWNPV;   //!
   TBranch        *b_B_IPCHI2_OWNPV;   //!
   TBranch        *b_B_FD_OWNPV;   //!
   TBranch        *b_B_FDCHI2_OWNPV;   //!
   TBranch        *b_B_DIRA_OWNPV;   //!
   TBranch        *b_B_P;   //!
   TBranch        *b_B_PT;   //!
   TBranch        *b_B_PE;   //!
   TBranch        *b_B_PX;   //!
   TBranch        *b_B_PY;   //!
   TBranch        *b_B_PZ;   //!
   TBranch        *b_B_REFPX;   //!
   TBranch        *b_B_REFPY;   //!
   TBranch        *b_B_REFPZ;   //!
   TBranch        *b_B_MM;   //!
   TBranch        *b_B_MMERR;   //!
   TBranch        *b_B_M;   //!
   TBranch        *b_B_BKGCAT;   //!
   TBranch        *b_B_TRUEID;   //!
   TBranch        *b_B_TRUEP_E;   //!
   TBranch        *b_B_TRUEP_X;   //!
   TBranch        *b_B_TRUEP_Y;   //!
   TBranch        *b_B_TRUEP_Z;   //!
   TBranch        *b_B_TRUEPT;   //!
   TBranch        *b_B_TRUEORIGINVERTEX_X;   //!
   TBranch        *b_B_TRUEORIGINVERTEX_Y;   //!
   TBranch        *b_B_TRUEORIGINVERTEX_Z;   //!
   TBranch        *b_B_TRUEENDVERTEX_X;   //!
   TBranch        *b_B_TRUEENDVERTEX_Y;   //!
   TBranch        *b_B_TRUEENDVERTEX_Z;   //!
   TBranch        *b_B_TRUEISSTABLE;   //!
   TBranch        *b_B_TRUETAU;   //!
   TBranch        *b_B_ID;   //!
   TBranch        *b_B_TAU;   //!
   TBranch        *b_B_TAUERR;   //!
   TBranch        *b_B_TAUCHI2;   //!
   TBranch        *b_B_L0Global_Dec;   //!
   TBranch        *b_B_L0Global_TIS;   //!
   TBranch        *b_B_L0Global_TOS;   //!
   TBranch        *b_B_Hlt1Global_Dec;   //!
   TBranch        *b_B_Hlt1Global_TIS;   //!
   TBranch        *b_B_Hlt1Global_TOS;   //!
   TBranch        *b_B_Hlt1Phys_Dec;   //!
   TBranch        *b_B_Hlt1Phys_TIS;   //!
   TBranch        *b_B_Hlt1Phys_TOS;   //!
   TBranch        *b_B_Hlt2Global_Dec;   //!
   TBranch        *b_B_Hlt2Global_TIS;   //!
   TBranch        *b_B_Hlt2Global_TOS;   //!
   TBranch        *b_B_Hlt2Phys_Dec;   //!
   TBranch        *b_B_Hlt2Phys_TIS;   //!
   TBranch        *b_B_Hlt2Phys_TOS;   //!
   TBranch        *b_B_L0HadronDecision_Dec;   //!
   TBranch        *b_B_L0HadronDecision_TIS;   //!
   TBranch        *b_B_L0HadronDecision_TOS;   //!
   TBranch        *b_B_L0ElectronDecision_Dec;   //!
   TBranch        *b_B_L0ElectronDecision_TIS;   //!
   TBranch        *b_B_L0ElectronDecision_TOS;   //!
   TBranch        *b_B_L0ElectronHiDecision_Dec;   //!
   TBranch        *b_B_L0ElectronHiDecision_TIS;   //!
   TBranch        *b_B_L0ElectronHiDecision_TOS;   //!
   TBranch        *b_B_L0MuonDecision_Dec;   //!
   TBranch        *b_B_L0MuonDecision_TIS;   //!
   TBranch        *b_B_L0MuonDecision_TOS;   //!
   TBranch        *b_B_L0DiMuonDecision_Dec;   //!
   TBranch        *b_B_L0DiMuonDecision_TIS;   //!
   TBranch        *b_B_L0DiMuonDecision_TOS;   //!
   TBranch        *b_B_L0MuonHighDecision_Dec;   //!
   TBranch        *b_B_L0MuonHighDecision_TIS;   //!
   TBranch        *b_B_L0MuonHighDecision_TOS;   //!
   TBranch        *b_B_Hlt1TrackMVADecision_Dec;   //!
   TBranch        *b_B_Hlt1TrackMVADecision_TIS;   //!
   TBranch        *b_B_Hlt1TrackMVADecision_TOS;   //!
   TBranch        *b_B_Hlt1TwoTrackMVADecision_Dec;   //!
   TBranch        *b_B_Hlt1TwoTrackMVADecision_TIS;   //!
   TBranch        *b_B_Hlt1TwoTrackMVADecision_TOS;   //!
   TBranch        *b_B_Hlt1GlobalDecision_Dec;   //!
   TBranch        *b_B_Hlt1GlobalDecision_TIS;   //!
   TBranch        *b_B_Hlt1GlobalDecision_TOS;   //!
   TBranch        *b_B_Hlt1TrackAllL0Decision_Dec;   //!
   TBranch        *b_B_Hlt1TrackAllL0Decision_TIS;   //!
   TBranch        *b_B_Hlt1TrackAllL0Decision_TOS;   //!
   TBranch        *b_B_Hlt2Topo2BodyBBDTDecision_Dec;   //!
   TBranch        *b_B_Hlt2Topo2BodyBBDTDecision_TIS;   //!
   TBranch        *b_B_Hlt2Topo2BodyBBDTDecision_TOS;   //!
   TBranch        *b_B_Hlt2Topo3BodyBBDTDecision_Dec;   //!
   TBranch        *b_B_Hlt2Topo3BodyBBDTDecision_TIS;   //!
   TBranch        *b_B_Hlt2Topo3BodyBBDTDecision_TOS;   //!
   TBranch        *b_B_Hlt2Topo4BodyBBDTDecision_Dec;   //!
   TBranch        *b_B_Hlt2Topo4BodyBBDTDecision_TIS;   //!
   TBranch        *b_B_Hlt2Topo4BodyBBDTDecision_TOS;   //!
   TBranch        *b_B_Hlt2BHadB02PpPpPmPmDecision_Dec;   //!
   TBranch        *b_B_Hlt2BHadB02PpPpPmPmDecision_TIS;   //!
   TBranch        *b_B_Hlt2BHadB02PpPpPmPmDecision_TOS;   //!
   TBranch        *b_B_Hlt2Topo2BodyDecision_Dec;   //!
   TBranch        *b_B_Hlt2Topo2BodyDecision_TIS;   //!
   TBranch        *b_B_Hlt2Topo2BodyDecision_TOS;   //!
   TBranch        *b_B_Hlt2Topo3BodyDecision_Dec;   //!
   TBranch        *b_B_Hlt2Topo3BodyDecision_TIS;   //!
   TBranch        *b_B_Hlt2Topo3BodyDecision_TOS;   //!
   TBranch        *b_B_Hlt2Topo4BodyDecision_Dec;   //!
   TBranch        *b_B_Hlt2Topo4BodyDecision_TIS;   //!
   TBranch        *b_B_Hlt2Topo4BodyDecision_TOS;   //!
   TBranch        *b_B_Hlt2GlobalDecision_Dec;   //!
   TBranch        *b_B_Hlt2GlobalDecision_TIS;   //!
   TBranch        *b_B_Hlt2GlobalDecision_TOS;   //!
   TBranch        *b_B_X;   //!
   TBranch        *b_B_Y;   //!
   TBranch        *b_B_DTFDict_B_DIRA_OWNPV;   //!
   TBranch        *b_B_DTFDict_B_E;   //!
   TBranch        *b_B_DTFDict_B_ENDVERTEX_CHI2;   //!
   TBranch        *b_B_DTFDict_B_ENDVERTEX_NDOF;   //!
   TBranch        *b_B_DTFDict_B_FDCHI2_OWNPV;   //!
   TBranch        *b_B_DTFDict_B_M;   //!
   TBranch        *b_B_DTFDict_B_MINIPCHI2;   //!
   TBranch        *b_B_DTFDict_B_PT;   //!
   TBranch        *b_B_DTFDict_B_PX;   //!
   TBranch        *b_B_DTFDict_B_PY;   //!
   TBranch        *b_B_DTFDict_B_PZ;   //!
   TBranch        *b_B_DTFDict_Dplus_DIRA_OWNPV;   //!
   TBranch        *b_B_DTFDict_Dplus_E;   //!
   TBranch        *b_B_DTFDict_Dplus_ENDVERTEX_CHI2;   //!
   TBranch        *b_B_DTFDict_Dplus_ENDVERTEX_NDOF;   //!
   TBranch        *b_B_DTFDict_Dplus_FDCHI2_OWNPV;   //!
   TBranch        *b_B_DTFDict_Dplus_M;   //!
   TBranch        *b_B_DTFDict_Dplus_MINIPCHI2;   //!
   TBranch        *b_B_DTFDict_Dplus_MM;   //!
   TBranch        *b_B_DTFDict_Dplus_PT;   //!
   TBranch        *b_B_DTFDict_Dplus_PX;   //!
   TBranch        *b_B_DTFDict_Dplus_PY;   //!
   TBranch        *b_B_DTFDict_Dplus_PZ;   //!
   TBranch        *b_B_DTFDict_Dsminus_DIRA_OWNPV;   //!
   TBranch        *b_B_DTFDict_Dsminus_E;   //!
   TBranch        *b_B_DTFDict_Dsminus_ENDVERTEX_CHI2;   //!
   TBranch        *b_B_DTFDict_Dsminus_ENDVERTEX_NDOF;   //!
   TBranch        *b_B_DTFDict_Dsminus_FDCHI2_OWNPV;   //!
   TBranch        *b_B_DTFDict_Dsminus_M;   //!
   TBranch        *b_B_DTFDict_Dsminus_MINIPCHI2;   //!
   TBranch        *b_B_DTFDict_Dsminus_MM;   //!
   TBranch        *b_B_DTFDict_Dsminus_PT;   //!
   TBranch        *b_B_DTFDict_Dsminus_PX;   //!
   TBranch        *b_B_DTFDict_Dsminus_PY;   //!
   TBranch        *b_B_DTFDict_Dsminus_PZ;   //!
   TBranch        *b_B_DTF_CHI2;   //!
   TBranch        *b_B_DTF_NDOF;   //!
   TBranch        *b_B_DTF_M_nPV;   //!
   TBranch        *b_B_DTF_M_D_splus_Kplus_0_ID;   //!
   TBranch        *b_B_DTF_M_D_splus_Kplus_0_PE;   //!
   TBranch        *b_B_DTF_M_D_splus_Kplus_0_PX;   //!
   TBranch        *b_B_DTF_M_D_splus_Kplus_0_PY;   //!
   TBranch        *b_B_DTF_M_D_splus_Kplus_0_PZ;   //!
   TBranch        *b_B_DTF_M_D_splus_Kplus_ID;   //!
   TBranch        *b_B_DTF_M_D_splus_Kplus_PE;   //!
   TBranch        *b_B_DTF_M_D_splus_Kplus_PX;   //!
   TBranch        *b_B_DTF_M_D_splus_Kplus_PY;   //!
   TBranch        *b_B_DTF_M_D_splus_Kplus_PZ;   //!
   TBranch        *b_B_DTF_M_D_splus_M;   //!
   TBranch        *b_B_DTF_M_D_splus_MERR;   //!
   TBranch        *b_B_DTF_M_D_splus_P;   //!
   TBranch        *b_B_DTF_M_D_splus_PERR;   //!
   TBranch        *b_B_DTF_M_D_splus_ctau;   //!
   TBranch        *b_B_DTF_M_D_splus_ctauErr;   //!
   TBranch        *b_B_DTF_M_D_splus_decayLength;   //!
   TBranch        *b_B_DTF_M_D_splus_decayLengthErr;   //!
   TBranch        *b_B_DTF_M_D_splus_piplus_ID;   //!
   TBranch        *b_B_DTF_M_D_splus_piplus_PE;   //!
   TBranch        *b_B_DTF_M_D_splus_piplus_PX;   //!
   TBranch        *b_B_DTF_M_D_splus_piplus_PY;   //!
   TBranch        *b_B_DTF_M_D_splus_piplus_PZ;   //!
   TBranch        *b_B_DTF_M_Dplus_Kplus_ID;   //!
   TBranch        *b_B_DTF_M_Dplus_Kplus_PE;   //!
   TBranch        *b_B_DTF_M_Dplus_Kplus_PX;   //!
   TBranch        *b_B_DTF_M_Dplus_Kplus_PY;   //!
   TBranch        *b_B_DTF_M_Dplus_Kplus_PZ;   //!
   TBranch        *b_B_DTF_M_Dplus_M;   //!
   TBranch        *b_B_DTF_M_Dplus_MERR;   //!
   TBranch        *b_B_DTF_M_Dplus_P;   //!
   TBranch        *b_B_DTF_M_Dplus_PERR;   //!
   TBranch        *b_B_DTF_M_Dplus_ctau;   //!
   TBranch        *b_B_DTF_M_Dplus_ctauErr;   //!
   TBranch        *b_B_DTF_M_Dplus_decayLength;   //!
   TBranch        *b_B_DTF_M_Dplus_decayLengthErr;   //!
   TBranch        *b_B_DTF_M_Dplus_piplus_0_ID;   //!
   TBranch        *b_B_DTF_M_Dplus_piplus_0_PE;   //!
   TBranch        *b_B_DTF_M_Dplus_piplus_0_PX;   //!
   TBranch        *b_B_DTF_M_Dplus_piplus_0_PY;   //!
   TBranch        *b_B_DTF_M_Dplus_piplus_0_PZ;   //!
   TBranch        *b_B_DTF_M_Dplus_piplus_ID;   //!
   TBranch        *b_B_DTF_M_Dplus_piplus_PE;   //!
   TBranch        *b_B_DTF_M_Dplus_piplus_PX;   //!
   TBranch        *b_B_DTF_M_Dplus_piplus_PY;   //!
   TBranch        *b_B_DTF_M_Dplus_piplus_PZ;   //!
   TBranch        *b_B_DTF_M_M;   //!
   TBranch        *b_B_DTF_M_MERR;   //!
   TBranch        *b_B_DTF_M_P;   //!
   TBranch        *b_B_DTF_M_PERR;   //!
   TBranch        *b_B_DTF_M_chi2;   //!
   TBranch        *b_B_DTF_M_nDOF;   //!
   TBranch        *b_B_DTF_M_nIter;   //!
   TBranch        *b_B_DTF_M_status;   //!
   TBranch        *b_B_DTF_M_PVC_nPV;   //!
   TBranch        *b_B_DTF_M_PVC_D_splus_Kplus_0_ID;   //!
   TBranch        *b_B_DTF_M_PVC_D_splus_Kplus_0_PE;   //!
   TBranch        *b_B_DTF_M_PVC_D_splus_Kplus_0_PX;   //!
   TBranch        *b_B_DTF_M_PVC_D_splus_Kplus_0_PY;   //!
   TBranch        *b_B_DTF_M_PVC_D_splus_Kplus_0_PZ;   //!
   TBranch        *b_B_DTF_M_PVC_D_splus_Kplus_ID;   //!
   TBranch        *b_B_DTF_M_PVC_D_splus_Kplus_PE;   //!
   TBranch        *b_B_DTF_M_PVC_D_splus_Kplus_PX;   //!
   TBranch        *b_B_DTF_M_PVC_D_splus_Kplus_PY;   //!
   TBranch        *b_B_DTF_M_PVC_D_splus_Kplus_PZ;   //!
   TBranch        *b_B_DTF_M_PVC_D_splus_M;   //!
   TBranch        *b_B_DTF_M_PVC_D_splus_MERR;   //!
   TBranch        *b_B_DTF_M_PVC_D_splus_P;   //!
   TBranch        *b_B_DTF_M_PVC_D_splus_PERR;   //!
   TBranch        *b_B_DTF_M_PVC_D_splus_ctau;   //!
   TBranch        *b_B_DTF_M_PVC_D_splus_ctauErr;   //!
   TBranch        *b_B_DTF_M_PVC_D_splus_decayLength;   //!
   TBranch        *b_B_DTF_M_PVC_D_splus_decayLengthErr;   //!
   TBranch        *b_B_DTF_M_PVC_D_splus_piplus_ID;   //!
   TBranch        *b_B_DTF_M_PVC_D_splus_piplus_PE;   //!
   TBranch        *b_B_DTF_M_PVC_D_splus_piplus_PX;   //!
   TBranch        *b_B_DTF_M_PVC_D_splus_piplus_PY;   //!
   TBranch        *b_B_DTF_M_PVC_D_splus_piplus_PZ;   //!
   TBranch        *b_B_DTF_M_PVC_Dplus_Kplus_ID;   //!
   TBranch        *b_B_DTF_M_PVC_Dplus_Kplus_PE;   //!
   TBranch        *b_B_DTF_M_PVC_Dplus_Kplus_PX;   //!
   TBranch        *b_B_DTF_M_PVC_Dplus_Kplus_PY;   //!
   TBranch        *b_B_DTF_M_PVC_Dplus_Kplus_PZ;   //!
   TBranch        *b_B_DTF_M_PVC_Dplus_M;   //!
   TBranch        *b_B_DTF_M_PVC_Dplus_MERR;   //!
   TBranch        *b_B_DTF_M_PVC_Dplus_P;   //!
   TBranch        *b_B_DTF_M_PVC_Dplus_PERR;   //!
   TBranch        *b_B_DTF_M_PVC_Dplus_ctau;   //!
   TBranch        *b_B_DTF_M_PVC_Dplus_ctauErr;   //!
   TBranch        *b_B_DTF_M_PVC_Dplus_decayLength;   //!
   TBranch        *b_B_DTF_M_PVC_Dplus_decayLengthErr;   //!
   TBranch        *b_B_DTF_M_PVC_Dplus_piplus_0_ID;   //!
   TBranch        *b_B_DTF_M_PVC_Dplus_piplus_0_PE;   //!
   TBranch        *b_B_DTF_M_PVC_Dplus_piplus_0_PX;   //!
   TBranch        *b_B_DTF_M_PVC_Dplus_piplus_0_PY;   //!
   TBranch        *b_B_DTF_M_PVC_Dplus_piplus_0_PZ;   //!
   TBranch        *b_B_DTF_M_PVC_Dplus_piplus_ID;   //!
   TBranch        *b_B_DTF_M_PVC_Dplus_piplus_PE;   //!
   TBranch        *b_B_DTF_M_PVC_Dplus_piplus_PX;   //!
   TBranch        *b_B_DTF_M_PVC_Dplus_piplus_PY;   //!
   TBranch        *b_B_DTF_M_PVC_Dplus_piplus_PZ;   //!
   TBranch        *b_B_DTF_M_PVC_M;   //!
   TBranch        *b_B_DTF_M_PVC_MERR;   //!
   TBranch        *b_B_DTF_M_PVC_P;   //!
   TBranch        *b_B_DTF_M_PVC_PERR;   //!
   TBranch        *b_B_DTF_M_PVC_PV_X;   //!
   TBranch        *b_B_DTF_M_PVC_PV_Y;   //!
   TBranch        *b_B_DTF_M_PVC_PV_Z;   //!
   TBranch        *b_B_DTF_M_PVC_PV_key;   //!
   TBranch        *b_B_DTF_M_PVC_chi2;   //!
   TBranch        *b_B_DTF_M_PVC_ctau;   //!
   TBranch        *b_B_DTF_M_PVC_ctauErr;   //!
   TBranch        *b_B_DTF_M_PVC_decayLength;   //!
   TBranch        *b_B_DTF_M_PVC_decayLengthErr;   //!
   TBranch        *b_B_DTF_M_PVC_nDOF;   //!
   TBranch        *b_B_DTF_M_PVC_nIter;   //!
   TBranch        *b_B_DTF_M_PVC_status;   //!
   TBranch        *b_B_PVC_nPV;   //!
   TBranch        *b_B_PVC_D_splus_Kplus_0_ID;   //!
   TBranch        *b_B_PVC_D_splus_Kplus_0_PE;   //!
   TBranch        *b_B_PVC_D_splus_Kplus_0_PX;   //!
   TBranch        *b_B_PVC_D_splus_Kplus_0_PY;   //!
   TBranch        *b_B_PVC_D_splus_Kplus_0_PZ;   //!
   TBranch        *b_B_PVC_D_splus_Kplus_ID;   //!
   TBranch        *b_B_PVC_D_splus_Kplus_PE;   //!
   TBranch        *b_B_PVC_D_splus_Kplus_PX;   //!
   TBranch        *b_B_PVC_D_splus_Kplus_PY;   //!
   TBranch        *b_B_PVC_D_splus_Kplus_PZ;   //!
   TBranch        *b_B_PVC_D_splus_M;   //!
   TBranch        *b_B_PVC_D_splus_MERR;   //!
   TBranch        *b_B_PVC_D_splus_P;   //!
   TBranch        *b_B_PVC_D_splus_PERR;   //!
   TBranch        *b_B_PVC_D_splus_ctau;   //!
   TBranch        *b_B_PVC_D_splus_ctauErr;   //!
   TBranch        *b_B_PVC_D_splus_decayLength;   //!
   TBranch        *b_B_PVC_D_splus_decayLengthErr;   //!
   TBranch        *b_B_PVC_D_splus_piplus_ID;   //!
   TBranch        *b_B_PVC_D_splus_piplus_PE;   //!
   TBranch        *b_B_PVC_D_splus_piplus_PX;   //!
   TBranch        *b_B_PVC_D_splus_piplus_PY;   //!
   TBranch        *b_B_PVC_D_splus_piplus_PZ;   //!
   TBranch        *b_B_PVC_Dplus_Kplus_ID;   //!
   TBranch        *b_B_PVC_Dplus_Kplus_PE;   //!
   TBranch        *b_B_PVC_Dplus_Kplus_PX;   //!
   TBranch        *b_B_PVC_Dplus_Kplus_PY;   //!
   TBranch        *b_B_PVC_Dplus_Kplus_PZ;   //!
   TBranch        *b_B_PVC_Dplus_M;   //!
   TBranch        *b_B_PVC_Dplus_MERR;   //!
   TBranch        *b_B_PVC_Dplus_P;   //!
   TBranch        *b_B_PVC_Dplus_PERR;   //!
   TBranch        *b_B_PVC_Dplus_ctau;   //!
   TBranch        *b_B_PVC_Dplus_ctauErr;   //!
   TBranch        *b_B_PVC_Dplus_decayLength;   //!
   TBranch        *b_B_PVC_Dplus_decayLengthErr;   //!
   TBranch        *b_B_PVC_Dplus_piplus_0_ID;   //!
   TBranch        *b_B_PVC_Dplus_piplus_0_PE;   //!
   TBranch        *b_B_PVC_Dplus_piplus_0_PX;   //!
   TBranch        *b_B_PVC_Dplus_piplus_0_PY;   //!
   TBranch        *b_B_PVC_Dplus_piplus_0_PZ;   //!
   TBranch        *b_B_PVC_Dplus_piplus_ID;   //!
   TBranch        *b_B_PVC_Dplus_piplus_PE;   //!
   TBranch        *b_B_PVC_Dplus_piplus_PX;   //!
   TBranch        *b_B_PVC_Dplus_piplus_PY;   //!
   TBranch        *b_B_PVC_Dplus_piplus_PZ;   //!
   TBranch        *b_B_PVC_M;   //!
   TBranch        *b_B_PVC_MERR;   //!
   TBranch        *b_B_PVC_P;   //!
   TBranch        *b_B_PVC_PERR;   //!
   TBranch        *b_B_PVC_PV_X;   //!
   TBranch        *b_B_PVC_PV_Y;   //!
   TBranch        *b_B_PVC_PV_Z;   //!
   TBranch        *b_B_PVC_PV_key;   //!
   TBranch        *b_B_PVC_chi2;   //!
   TBranch        *b_B_PVC_ctau;   //!
   TBranch        *b_B_PVC_ctauErr;   //!
   TBranch        *b_B_PVC_decayLength;   //!
   TBranch        *b_B_PVC_decayLengthErr;   //!
   TBranch        *b_B_PVC_nDOF;   //!
   TBranch        *b_B_PVC_nIter;   //!
   TBranch        *b_B_PVC_status;   //!
   TBranch        *b_Dsminus_ETA;   //!
   TBranch        *b_Dsminus_MinIPCHI2;   //!
   TBranch        *b_Dsminus_CosTheta;   //!
   TBranch        *b_Dsminus_ENDVERTEX_X;   //!
   TBranch        *b_Dsminus_ENDVERTEX_Y;   //!
   TBranch        *b_Dsminus_ENDVERTEX_Z;   //!
   TBranch        *b_Dsminus_ENDVERTEX_XERR;   //!
   TBranch        *b_Dsminus_ENDVERTEX_YERR;   //!
   TBranch        *b_Dsminus_ENDVERTEX_ZERR;   //!
   TBranch        *b_Dsminus_ENDVERTEX_CHI2;   //!
   TBranch        *b_Dsminus_ENDVERTEX_NDOF;   //!
   TBranch        *b_Dsminus_ENDVERTEX_COV_;   //!
   TBranch        *b_Dsminus_OWNPV_X;   //!
   TBranch        *b_Dsminus_OWNPV_Y;   //!
   TBranch        *b_Dsminus_OWNPV_Z;   //!
   TBranch        *b_Dsminus_OWNPV_XERR;   //!
   TBranch        *b_Dsminus_OWNPV_YERR;   //!
   TBranch        *b_Dsminus_OWNPV_ZERR;   //!
   TBranch        *b_Dsminus_OWNPV_CHI2;   //!
   TBranch        *b_Dsminus_OWNPV_NDOF;   //!
   TBranch        *b_Dsminus_OWNPV_COV_;   //!
   TBranch        *b_Dsminus_IP_OWNPV;   //!
   TBranch        *b_Dsminus_IPCHI2_OWNPV;   //!
   TBranch        *b_Dsminus_FD_OWNPV;   //!
   TBranch        *b_Dsminus_FDCHI2_OWNPV;   //!
   TBranch        *b_Dsminus_DIRA_OWNPV;   //!
   TBranch        *b_Dsminus_ORIVX_X;   //!
   TBranch        *b_Dsminus_ORIVX_Y;   //!
   TBranch        *b_Dsminus_ORIVX_Z;   //!
   TBranch        *b_Dsminus_ORIVX_XERR;   //!
   TBranch        *b_Dsminus_ORIVX_YERR;   //!
   TBranch        *b_Dsminus_ORIVX_ZERR;   //!
   TBranch        *b_Dsminus_ORIVX_CHI2;   //!
   TBranch        *b_Dsminus_ORIVX_NDOF;   //!
   TBranch        *b_Dsminus_ORIVX_COV_;   //!
   TBranch        *b_Dsminus_FD_ORIVX;   //!
   TBranch        *b_Dsminus_FDCHI2_ORIVX;   //!
   TBranch        *b_Dsminus_DIRA_ORIVX;   //!
   TBranch        *b_Dsminus_P;   //!
   TBranch        *b_Dsminus_PT;   //!
   TBranch        *b_Dsminus_PE;   //!
   TBranch        *b_Dsminus_PX;   //!
   TBranch        *b_Dsminus_PY;   //!
   TBranch        *b_Dsminus_PZ;   //!
   TBranch        *b_Dsminus_REFPX;   //!
   TBranch        *b_Dsminus_REFPY;   //!
   TBranch        *b_Dsminus_REFPZ;   //!
   TBranch        *b_Dsminus_MM;   //!
   TBranch        *b_Dsminus_MMERR;   //!
   TBranch        *b_Dsminus_M;   //!
   TBranch        *b_Dsminus_BKGCAT;   //!
   TBranch        *b_Dsminus_TRUEID;   //!
   TBranch        *b_Dsminus_TRUEP_E;   //!
   TBranch        *b_Dsminus_TRUEP_X;   //!
   TBranch        *b_Dsminus_TRUEP_Y;   //!
   TBranch        *b_Dsminus_TRUEP_Z;   //!
   TBranch        *b_Dsminus_TRUEPT;   //!
   TBranch        *b_Dsminus_TRUEORIGINVERTEX_X;   //!
   TBranch        *b_Dsminus_TRUEORIGINVERTEX_Y;   //!
   TBranch        *b_Dsminus_TRUEORIGINVERTEX_Z;   //!
   TBranch        *b_Dsminus_TRUEENDVERTEX_X;   //!
   TBranch        *b_Dsminus_TRUEENDVERTEX_Y;   //!
   TBranch        *b_Dsminus_TRUEENDVERTEX_Z;   //!
   TBranch        *b_Dsminus_TRUEISSTABLE;   //!
   TBranch        *b_Dsminus_TRUETAU;   //!
   TBranch        *b_Dsminus_ID;   //!
   TBranch        *b_Dsminus_TAU;   //!
   TBranch        *b_Dsminus_TAUERR;   //!
   TBranch        *b_Dsminus_TAUCHI2;   //!
   TBranch        *b_Dsminus_L0Global_Dec;   //!
   TBranch        *b_Dsminus_L0Global_TIS;   //!
   TBranch        *b_Dsminus_L0Global_TOS;   //!
   TBranch        *b_Dsminus_Hlt1Global_Dec;   //!
   TBranch        *b_Dsminus_Hlt1Global_TIS;   //!
   TBranch        *b_Dsminus_Hlt1Global_TOS;   //!
   TBranch        *b_Dsminus_Hlt1Phys_Dec;   //!
   TBranch        *b_Dsminus_Hlt1Phys_TIS;   //!
   TBranch        *b_Dsminus_Hlt1Phys_TOS;   //!
   TBranch        *b_Dsminus_Hlt2Global_Dec;   //!
   TBranch        *b_Dsminus_Hlt2Global_TIS;   //!
   TBranch        *b_Dsminus_Hlt2Global_TOS;   //!
   TBranch        *b_Dsminus_Hlt2Phys_Dec;   //!
   TBranch        *b_Dsminus_Hlt2Phys_TIS;   //!
   TBranch        *b_Dsminus_Hlt2Phys_TOS;   //!
   TBranch        *b_Dsminus_L0HadronDecision_Dec;   //!
   TBranch        *b_Dsminus_L0HadronDecision_TIS;   //!
   TBranch        *b_Dsminus_L0HadronDecision_TOS;   //!
   TBranch        *b_Dsminus_L0ElectronDecision_Dec;   //!
   TBranch        *b_Dsminus_L0ElectronDecision_TIS;   //!
   TBranch        *b_Dsminus_L0ElectronDecision_TOS;   //!
   TBranch        *b_Dsminus_L0ElectronHiDecision_Dec;   //!
   TBranch        *b_Dsminus_L0ElectronHiDecision_TIS;   //!
   TBranch        *b_Dsminus_L0ElectronHiDecision_TOS;   //!
   TBranch        *b_Dsminus_L0MuonDecision_Dec;   //!
   TBranch        *b_Dsminus_L0MuonDecision_TIS;   //!
   TBranch        *b_Dsminus_L0MuonDecision_TOS;   //!
   TBranch        *b_Dsminus_L0DiMuonDecision_Dec;   //!
   TBranch        *b_Dsminus_L0DiMuonDecision_TIS;   //!
   TBranch        *b_Dsminus_L0DiMuonDecision_TOS;   //!
   TBranch        *b_Dsminus_L0MuonHighDecision_Dec;   //!
   TBranch        *b_Dsminus_L0MuonHighDecision_TIS;   //!
   TBranch        *b_Dsminus_L0MuonHighDecision_TOS;   //!
   TBranch        *b_Dsminus_Hlt1TrackMVADecision_Dec;   //!
   TBranch        *b_Dsminus_Hlt1TrackMVADecision_TIS;   //!
   TBranch        *b_Dsminus_Hlt1TrackMVADecision_TOS;   //!
   TBranch        *b_Dsminus_Hlt1TwoTrackMVADecision_Dec;   //!
   TBranch        *b_Dsminus_Hlt1TwoTrackMVADecision_TIS;   //!
   TBranch        *b_Dsminus_Hlt1TwoTrackMVADecision_TOS;   //!
   TBranch        *b_Dsminus_Hlt1GlobalDecision_Dec;   //!
   TBranch        *b_Dsminus_Hlt1GlobalDecision_TIS;   //!
   TBranch        *b_Dsminus_Hlt1GlobalDecision_TOS;   //!
   TBranch        *b_Dsminus_Hlt1TrackAllL0Decision_Dec;   //!
   TBranch        *b_Dsminus_Hlt1TrackAllL0Decision_TIS;   //!
   TBranch        *b_Dsminus_Hlt1TrackAllL0Decision_TOS;   //!
   TBranch        *b_Dsminus_Hlt2Topo2BodyBBDTDecision_Dec;   //!
   TBranch        *b_Dsminus_Hlt2Topo2BodyBBDTDecision_TIS;   //!
   TBranch        *b_Dsminus_Hlt2Topo2BodyBBDTDecision_TOS;   //!
   TBranch        *b_Dsminus_Hlt2Topo3BodyBBDTDecision_Dec;   //!
   TBranch        *b_Dsminus_Hlt2Topo3BodyBBDTDecision_TIS;   //!
   TBranch        *b_Dsminus_Hlt2Topo3BodyBBDTDecision_TOS;   //!
   TBranch        *b_Dsminus_Hlt2Topo4BodyBBDTDecision_Dec;   //!
   TBranch        *b_Dsminus_Hlt2Topo4BodyBBDTDecision_TIS;   //!
   TBranch        *b_Dsminus_Hlt2Topo4BodyBBDTDecision_TOS;   //!
   TBranch        *b_Dsminus_Hlt2BHadB02PpPpPmPmDecision_Dec;   //!
   TBranch        *b_Dsminus_Hlt2BHadB02PpPpPmPmDecision_TIS;   //!
   TBranch        *b_Dsminus_Hlt2BHadB02PpPpPmPmDecision_TOS;   //!
   TBranch        *b_Dsminus_Hlt2Topo2BodyDecision_Dec;   //!
   TBranch        *b_Dsminus_Hlt2Topo2BodyDecision_TIS;   //!
   TBranch        *b_Dsminus_Hlt2Topo2BodyDecision_TOS;   //!
   TBranch        *b_Dsminus_Hlt2Topo3BodyDecision_Dec;   //!
   TBranch        *b_Dsminus_Hlt2Topo3BodyDecision_TIS;   //!
   TBranch        *b_Dsminus_Hlt2Topo3BodyDecision_TOS;   //!
   TBranch        *b_Dsminus_Hlt2Topo4BodyDecision_Dec;   //!
   TBranch        *b_Dsminus_Hlt2Topo4BodyDecision_TIS;   //!
   TBranch        *b_Dsminus_Hlt2Topo4BodyDecision_TOS;   //!
   TBranch        *b_Dsminus_Hlt2GlobalDecision_Dec;   //!
   TBranch        *b_Dsminus_Hlt2GlobalDecision_TIS;   //!
   TBranch        *b_Dsminus_Hlt2GlobalDecision_TOS;   //!
   TBranch        *b_Dsminus_X;   //!
   TBranch        *b_Dsminus_Y;   //!
   TBranch        *b_Kplus_Dsminus_ETA;   //!
   TBranch        *b_Kplus_Dsminus_MinIPCHI2;   //!
   TBranch        *b_Kplus_Dsminus_MC12TuneV2_ProbNNe;   //!
   TBranch        *b_Kplus_Dsminus_MC12TuneV2_ProbNNmu;   //!
   TBranch        *b_Kplus_Dsminus_MC12TuneV2_ProbNNpi;   //!
   TBranch        *b_Kplus_Dsminus_MC12TuneV2_ProbNNk;   //!
   TBranch        *b_Kplus_Dsminus_MC12TuneV2_ProbNNp;   //!
   TBranch        *b_Kplus_Dsminus_MC12TuneV2_ProbNNghost;   //!
   TBranch        *b_Kplus_Dsminus_MC12TuneV3_ProbNNe;   //!
   TBranch        *b_Kplus_Dsminus_MC12TuneV3_ProbNNmu;   //!
   TBranch        *b_Kplus_Dsminus_MC12TuneV3_ProbNNpi;   //!
   TBranch        *b_Kplus_Dsminus_MC12TuneV3_ProbNNk;   //!
   TBranch        *b_Kplus_Dsminus_MC12TuneV3_ProbNNp;   //!
   TBranch        *b_Kplus_Dsminus_MC12TuneV3_ProbNNghost;   //!
   TBranch        *b_Kplus_Dsminus_MC12TuneV4_ProbNNe;   //!
   TBranch        *b_Kplus_Dsminus_MC12TuneV4_ProbNNmu;   //!
   TBranch        *b_Kplus_Dsminus_MC12TuneV4_ProbNNpi;   //!
   TBranch        *b_Kplus_Dsminus_MC12TuneV4_ProbNNk;   //!
   TBranch        *b_Kplus_Dsminus_MC12TuneV4_ProbNNp;   //!
   TBranch        *b_Kplus_Dsminus_MC12TuneV4_ProbNNghost;   //!
   TBranch        *b_Kplus_Dsminus_MC15TuneV1_ProbNNe;   //!
   TBranch        *b_Kplus_Dsminus_MC15TuneV1_ProbNNmu;   //!
   TBranch        *b_Kplus_Dsminus_MC15TuneV1_ProbNNpi;   //!
   TBranch        *b_Kplus_Dsminus_MC15TuneV1_ProbNNk;   //!
   TBranch        *b_Kplus_Dsminus_MC15TuneV1_ProbNNp;   //!
   TBranch        *b_Kplus_Dsminus_MC15TuneV1_ProbNNghost;   //!
   TBranch        *b_Kplus_Dsminus_CosTheta;   //!
   TBranch        *b_Kplus_Dsminus_OWNPV_X;   //!
   TBranch        *b_Kplus_Dsminus_OWNPV_Y;   //!
   TBranch        *b_Kplus_Dsminus_OWNPV_Z;   //!
   TBranch        *b_Kplus_Dsminus_OWNPV_XERR;   //!
   TBranch        *b_Kplus_Dsminus_OWNPV_YERR;   //!
   TBranch        *b_Kplus_Dsminus_OWNPV_ZERR;   //!
   TBranch        *b_Kplus_Dsminus_OWNPV_CHI2;   //!
   TBranch        *b_Kplus_Dsminus_OWNPV_NDOF;   //!
   TBranch        *b_Kplus_Dsminus_OWNPV_COV_;   //!
   TBranch        *b_Kplus_Dsminus_IP_OWNPV;   //!
   TBranch        *b_Kplus_Dsminus_IPCHI2_OWNPV;   //!
   TBranch        *b_Kplus_Dsminus_ORIVX_X;   //!
   TBranch        *b_Kplus_Dsminus_ORIVX_Y;   //!
   TBranch        *b_Kplus_Dsminus_ORIVX_Z;   //!
   TBranch        *b_Kplus_Dsminus_ORIVX_XERR;   //!
   TBranch        *b_Kplus_Dsminus_ORIVX_YERR;   //!
   TBranch        *b_Kplus_Dsminus_ORIVX_ZERR;   //!
   TBranch        *b_Kplus_Dsminus_ORIVX_CHI2;   //!
   TBranch        *b_Kplus_Dsminus_ORIVX_NDOF;   //!
   TBranch        *b_Kplus_Dsminus_ORIVX_COV_;   //!
   TBranch        *b_Kplus_Dsminus_P;   //!
   TBranch        *b_Kplus_Dsminus_PT;   //!
   TBranch        *b_Kplus_Dsminus_PE;   //!
   TBranch        *b_Kplus_Dsminus_PX;   //!
   TBranch        *b_Kplus_Dsminus_PY;   //!
   TBranch        *b_Kplus_Dsminus_PZ;   //!
   TBranch        *b_Kplus_Dsminus_REFPX;   //!
   TBranch        *b_Kplus_Dsminus_REFPY;   //!
   TBranch        *b_Kplus_Dsminus_REFPZ;   //!
   TBranch        *b_Kplus_Dsminus_M;   //!
   TBranch        *b_Kplus_Dsminus_AtVtx_PE;   //!
   TBranch        *b_Kplus_Dsminus_AtVtx_PX;   //!
   TBranch        *b_Kplus_Dsminus_AtVtx_PY;   //!
   TBranch        *b_Kplus_Dsminus_AtVtx_PZ;   //!
   TBranch        *b_Kplus_Dsminus_TRUEID;   //!
   TBranch        *b_Kplus_Dsminus_TRUEP_E;   //!
   TBranch        *b_Kplus_Dsminus_TRUEP_X;   //!
   TBranch        *b_Kplus_Dsminus_TRUEP_Y;   //!
   TBranch        *b_Kplus_Dsminus_TRUEP_Z;   //!
   TBranch        *b_Kplus_Dsminus_TRUEPT;   //!
   TBranch        *b_Kplus_Dsminus_TRUEORIGINVERTEX_X;   //!
   TBranch        *b_Kplus_Dsminus_TRUEORIGINVERTEX_Y;   //!
   TBranch        *b_Kplus_Dsminus_TRUEORIGINVERTEX_Z;   //!
   TBranch        *b_Kplus_Dsminus_TRUEENDVERTEX_X;   //!
   TBranch        *b_Kplus_Dsminus_TRUEENDVERTEX_Y;   //!
   TBranch        *b_Kplus_Dsminus_TRUEENDVERTEX_Z;   //!
   TBranch        *b_Kplus_Dsminus_TRUEISSTABLE;   //!
   TBranch        *b_Kplus_Dsminus_TRUETAU;   //!
   TBranch        *b_Kplus_Dsminus_ID;   //!
   TBranch        *b_Kplus_Dsminus_CombDLLMu;   //!
   TBranch        *b_Kplus_Dsminus_ProbNNmu;   //!
   TBranch        *b_Kplus_Dsminus_ProbNNghost;   //!
   TBranch        *b_Kplus_Dsminus_InMuonAcc;   //!
   TBranch        *b_Kplus_Dsminus_MuonDist2;   //!
   TBranch        *b_Kplus_Dsminus_regionInM2;   //!
   TBranch        *b_Kplus_Dsminus_hasMuon;   //!
   TBranch        *b_Kplus_Dsminus_isMuon;   //!
   TBranch        *b_Kplus_Dsminus_isMuonLoose;   //!
   TBranch        *b_Kplus_Dsminus_NShared;   //!
   TBranch        *b_Kplus_Dsminus_MuonLLmu;   //!
   TBranch        *b_Kplus_Dsminus_MuonLLbg;   //!
   TBranch        *b_Kplus_Dsminus_isMuonFromProto;   //!
   TBranch        *b_Kplus_Dsminus_PIDe;   //!
   TBranch        *b_Kplus_Dsminus_PIDmu;   //!
   TBranch        *b_Kplus_Dsminus_PIDK;   //!
   TBranch        *b_Kplus_Dsminus_PIDp;   //!
   TBranch        *b_Kplus_Dsminus_PIDd;   //!
   TBranch        *b_Kplus_Dsminus_ProbNNe;   //!
   TBranch        *b_Kplus_Dsminus_ProbNNk;   //!
   TBranch        *b_Kplus_Dsminus_ProbNNp;   //!
   TBranch        *b_Kplus_Dsminus_ProbNNpi;   //!
   TBranch        *b_Kplus_Dsminus_ProbNNd;   //!
   TBranch        *b_Kplus_Dsminus_hasRich;   //!
   TBranch        *b_Kplus_Dsminus_UsedRichAerogel;   //!
   TBranch        *b_Kplus_Dsminus_UsedRich1Gas;   //!
   TBranch        *b_Kplus_Dsminus_UsedRich2Gas;   //!
   TBranch        *b_Kplus_Dsminus_RichAboveElThres;   //!
   TBranch        *b_Kplus_Dsminus_RichAboveMuThres;   //!
   TBranch        *b_Kplus_Dsminus_RichAbovePiThres;   //!
   TBranch        *b_Kplus_Dsminus_RichAboveKaThres;   //!
   TBranch        *b_Kplus_Dsminus_RichAbovePrThres;   //!
   TBranch        *b_Kplus_Dsminus_hasCalo;   //!
   TBranch        *b_Kplus_Dsminus_L0Global_Dec;   //!
   TBranch        *b_Kplus_Dsminus_L0Global_TIS;   //!
   TBranch        *b_Kplus_Dsminus_L0Global_TOS;   //!
   TBranch        *b_Kplus_Dsminus_Hlt1Global_Dec;   //!
   TBranch        *b_Kplus_Dsminus_Hlt1Global_TIS;   //!
   TBranch        *b_Kplus_Dsminus_Hlt1Global_TOS;   //!
   TBranch        *b_Kplus_Dsminus_Hlt1Phys_Dec;   //!
   TBranch        *b_Kplus_Dsminus_Hlt1Phys_TIS;   //!
   TBranch        *b_Kplus_Dsminus_Hlt1Phys_TOS;   //!
   TBranch        *b_Kplus_Dsminus_Hlt2Global_Dec;   //!
   TBranch        *b_Kplus_Dsminus_Hlt2Global_TIS;   //!
   TBranch        *b_Kplus_Dsminus_Hlt2Global_TOS;   //!
   TBranch        *b_Kplus_Dsminus_Hlt2Phys_Dec;   //!
   TBranch        *b_Kplus_Dsminus_Hlt2Phys_TIS;   //!
   TBranch        *b_Kplus_Dsminus_Hlt2Phys_TOS;   //!
   TBranch        *b_Kplus_Dsminus_L0HadronDecision_Dec;   //!
   TBranch        *b_Kplus_Dsminus_L0HadronDecision_TIS;   //!
   TBranch        *b_Kplus_Dsminus_L0HadronDecision_TOS;   //!
   TBranch        *b_Kplus_Dsminus_L0ElectronDecision_Dec;   //!
   TBranch        *b_Kplus_Dsminus_L0ElectronDecision_TIS;   //!
   TBranch        *b_Kplus_Dsminus_L0ElectronDecision_TOS;   //!
   TBranch        *b_Kplus_Dsminus_L0ElectronHiDecision_Dec;   //!
   TBranch        *b_Kplus_Dsminus_L0ElectronHiDecision_TIS;   //!
   TBranch        *b_Kplus_Dsminus_L0ElectronHiDecision_TOS;   //!
   TBranch        *b_Kplus_Dsminus_L0MuonDecision_Dec;   //!
   TBranch        *b_Kplus_Dsminus_L0MuonDecision_TIS;   //!
   TBranch        *b_Kplus_Dsminus_L0MuonDecision_TOS;   //!
   TBranch        *b_Kplus_Dsminus_L0DiMuonDecision_Dec;   //!
   TBranch        *b_Kplus_Dsminus_L0DiMuonDecision_TIS;   //!
   TBranch        *b_Kplus_Dsminus_L0DiMuonDecision_TOS;   //!
   TBranch        *b_Kplus_Dsminus_L0MuonHighDecision_Dec;   //!
   TBranch        *b_Kplus_Dsminus_L0MuonHighDecision_TIS;   //!
   TBranch        *b_Kplus_Dsminus_L0MuonHighDecision_TOS;   //!
   TBranch        *b_Kplus_Dsminus_Hlt1TrackMVADecision_Dec;   //!
   TBranch        *b_Kplus_Dsminus_Hlt1TrackMVADecision_TIS;   //!
   TBranch        *b_Kplus_Dsminus_Hlt1TrackMVADecision_TOS;   //!
   TBranch        *b_Kplus_Dsminus_Hlt1TwoTrackMVADecision_Dec;   //!
   TBranch        *b_Kplus_Dsminus_Hlt1TwoTrackMVADecision_TIS;   //!
   TBranch        *b_Kplus_Dsminus_Hlt1TwoTrackMVADecision_TOS;   //!
   TBranch        *b_Kplus_Dsminus_Hlt1GlobalDecision_Dec;   //!
   TBranch        *b_Kplus_Dsminus_Hlt1GlobalDecision_TIS;   //!
   TBranch        *b_Kplus_Dsminus_Hlt1GlobalDecision_TOS;   //!
   TBranch        *b_Kplus_Dsminus_Hlt1TrackAllL0Decision_Dec;   //!
   TBranch        *b_Kplus_Dsminus_Hlt1TrackAllL0Decision_TIS;   //!
   TBranch        *b_Kplus_Dsminus_Hlt1TrackAllL0Decision_TOS;   //!
   TBranch        *b_Kplus_Dsminus_Hlt2Topo2BodyBBDTDecision_Dec;   //!
   TBranch        *b_Kplus_Dsminus_Hlt2Topo2BodyBBDTDecision_TIS;   //!
   TBranch        *b_Kplus_Dsminus_Hlt2Topo2BodyBBDTDecision_TOS;   //!
   TBranch        *b_Kplus_Dsminus_Hlt2Topo3BodyBBDTDecision_Dec;   //!
   TBranch        *b_Kplus_Dsminus_Hlt2Topo3BodyBBDTDecision_TIS;   //!
   TBranch        *b_Kplus_Dsminus_Hlt2Topo3BodyBBDTDecision_TOS;   //!
   TBranch        *b_Kplus_Dsminus_Hlt2Topo4BodyBBDTDecision_Dec;   //!
   TBranch        *b_Kplus_Dsminus_Hlt2Topo4BodyBBDTDecision_TIS;   //!
   TBranch        *b_Kplus_Dsminus_Hlt2Topo4BodyBBDTDecision_TOS;   //!
   TBranch        *b_Kplus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_Dec;   //!
   TBranch        *b_Kplus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_TIS;   //!
   TBranch        *b_Kplus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_TOS;   //!
   TBranch        *b_Kplus_Dsminus_Hlt2Topo2BodyDecision_Dec;   //!
   TBranch        *b_Kplus_Dsminus_Hlt2Topo2BodyDecision_TIS;   //!
   TBranch        *b_Kplus_Dsminus_Hlt2Topo2BodyDecision_TOS;   //!
   TBranch        *b_Kplus_Dsminus_Hlt2Topo3BodyDecision_Dec;   //!
   TBranch        *b_Kplus_Dsminus_Hlt2Topo3BodyDecision_TIS;   //!
   TBranch        *b_Kplus_Dsminus_Hlt2Topo3BodyDecision_TOS;   //!
   TBranch        *b_Kplus_Dsminus_Hlt2Topo4BodyDecision_Dec;   //!
   TBranch        *b_Kplus_Dsminus_Hlt2Topo4BodyDecision_TIS;   //!
   TBranch        *b_Kplus_Dsminus_Hlt2Topo4BodyDecision_TOS;   //!
   TBranch        *b_Kplus_Dsminus_Hlt2GlobalDecision_Dec;   //!
   TBranch        *b_Kplus_Dsminus_Hlt2GlobalDecision_TIS;   //!
   TBranch        *b_Kplus_Dsminus_Hlt2GlobalDecision_TOS;   //!
   TBranch        *b_Kplus_Dsminus_TRACK_Type;   //!
   TBranch        *b_Kplus_Dsminus_TRACK_Key;   //!
   TBranch        *b_Kplus_Dsminus_TRACK_CHI2NDOF;   //!
   TBranch        *b_Kplus_Dsminus_TRACK_PCHI2;   //!
   TBranch        *b_Kplus_Dsminus_TRACK_MatchCHI2;   //!
   TBranch        *b_Kplus_Dsminus_TRACK_GhostProb;   //!
   TBranch        *b_Kplus_Dsminus_TRACK_CloneDist;   //!
   TBranch        *b_Kplus_Dsminus_TRACK_Likelihood;   //!
   TBranch        *b_Kplus_Dsminus_X;   //!
   TBranch        *b_Kplus_Dsminus_Y;   //!
   TBranch        *b_Kminus_Dsminus_ETA;   //!
   TBranch        *b_Kminus_Dsminus_MinIPCHI2;   //!
   TBranch        *b_Kminus_Dsminus_MC12TuneV2_ProbNNe;   //!
   TBranch        *b_Kminus_Dsminus_MC12TuneV2_ProbNNmu;   //!
   TBranch        *b_Kminus_Dsminus_MC12TuneV2_ProbNNpi;   //!
   TBranch        *b_Kminus_Dsminus_MC12TuneV2_ProbNNk;   //!
   TBranch        *b_Kminus_Dsminus_MC12TuneV2_ProbNNp;   //!
   TBranch        *b_Kminus_Dsminus_MC12TuneV2_ProbNNghost;   //!
   TBranch        *b_Kminus_Dsminus_MC12TuneV3_ProbNNe;   //!
   TBranch        *b_Kminus_Dsminus_MC12TuneV3_ProbNNmu;   //!
   TBranch        *b_Kminus_Dsminus_MC12TuneV3_ProbNNpi;   //!
   TBranch        *b_Kminus_Dsminus_MC12TuneV3_ProbNNk;   //!
   TBranch        *b_Kminus_Dsminus_MC12TuneV3_ProbNNp;   //!
   TBranch        *b_Kminus_Dsminus_MC12TuneV3_ProbNNghost;   //!
   TBranch        *b_Kminus_Dsminus_MC12TuneV4_ProbNNe;   //!
   TBranch        *b_Kminus_Dsminus_MC12TuneV4_ProbNNmu;   //!
   TBranch        *b_Kminus_Dsminus_MC12TuneV4_ProbNNpi;   //!
   TBranch        *b_Kminus_Dsminus_MC12TuneV4_ProbNNk;   //!
   TBranch        *b_Kminus_Dsminus_MC12TuneV4_ProbNNp;   //!
   TBranch        *b_Kminus_Dsminus_MC12TuneV4_ProbNNghost;   //!
   TBranch        *b_Kminus_Dsminus_MC15TuneV1_ProbNNe;   //!
   TBranch        *b_Kminus_Dsminus_MC15TuneV1_ProbNNmu;   //!
   TBranch        *b_Kminus_Dsminus_MC15TuneV1_ProbNNpi;   //!
   TBranch        *b_Kminus_Dsminus_MC15TuneV1_ProbNNk;   //!
   TBranch        *b_Kminus_Dsminus_MC15TuneV1_ProbNNp;   //!
   TBranch        *b_Kminus_Dsminus_MC15TuneV1_ProbNNghost;   //!
   TBranch        *b_Kminus_Dsminus_CosTheta;   //!
   TBranch        *b_Kminus_Dsminus_OWNPV_X;   //!
   TBranch        *b_Kminus_Dsminus_OWNPV_Y;   //!
   TBranch        *b_Kminus_Dsminus_OWNPV_Z;   //!
   TBranch        *b_Kminus_Dsminus_OWNPV_XERR;   //!
   TBranch        *b_Kminus_Dsminus_OWNPV_YERR;   //!
   TBranch        *b_Kminus_Dsminus_OWNPV_ZERR;   //!
   TBranch        *b_Kminus_Dsminus_OWNPV_CHI2;   //!
   TBranch        *b_Kminus_Dsminus_OWNPV_NDOF;   //!
   TBranch        *b_Kminus_Dsminus_OWNPV_COV_;   //!
   TBranch        *b_Kminus_Dsminus_IP_OWNPV;   //!
   TBranch        *b_Kminus_Dsminus_IPCHI2_OWNPV;   //!
   TBranch        *b_Kminus_Dsminus_ORIVX_X;   //!
   TBranch        *b_Kminus_Dsminus_ORIVX_Y;   //!
   TBranch        *b_Kminus_Dsminus_ORIVX_Z;   //!
   TBranch        *b_Kminus_Dsminus_ORIVX_XERR;   //!
   TBranch        *b_Kminus_Dsminus_ORIVX_YERR;   //!
   TBranch        *b_Kminus_Dsminus_ORIVX_ZERR;   //!
   TBranch        *b_Kminus_Dsminus_ORIVX_CHI2;   //!
   TBranch        *b_Kminus_Dsminus_ORIVX_NDOF;   //!
   TBranch        *b_Kminus_Dsminus_ORIVX_COV_;   //!
   TBranch        *b_Kminus_Dsminus_P;   //!
   TBranch        *b_Kminus_Dsminus_PT;   //!
   TBranch        *b_Kminus_Dsminus_PE;   //!
   TBranch        *b_Kminus_Dsminus_PX;   //!
   TBranch        *b_Kminus_Dsminus_PY;   //!
   TBranch        *b_Kminus_Dsminus_PZ;   //!
   TBranch        *b_Kminus_Dsminus_REFPX;   //!
   TBranch        *b_Kminus_Dsminus_REFPY;   //!
   TBranch        *b_Kminus_Dsminus_REFPZ;   //!
   TBranch        *b_Kminus_Dsminus_M;   //!
   TBranch        *b_Kminus_Dsminus_AtVtx_PE;   //!
   TBranch        *b_Kminus_Dsminus_AtVtx_PX;   //!
   TBranch        *b_Kminus_Dsminus_AtVtx_PY;   //!
   TBranch        *b_Kminus_Dsminus_AtVtx_PZ;   //!
   TBranch        *b_Kminus_Dsminus_TRUEID;   //!
   TBranch        *b_Kminus_Dsminus_TRUEP_E;   //!
   TBranch        *b_Kminus_Dsminus_TRUEP_X;   //!
   TBranch        *b_Kminus_Dsminus_TRUEP_Y;   //!
   TBranch        *b_Kminus_Dsminus_TRUEP_Z;   //!
   TBranch        *b_Kminus_Dsminus_TRUEPT;   //!
   TBranch        *b_Kminus_Dsminus_TRUEORIGINVERTEX_X;   //!
   TBranch        *b_Kminus_Dsminus_TRUEORIGINVERTEX_Y;   //!
   TBranch        *b_Kminus_Dsminus_TRUEORIGINVERTEX_Z;   //!
   TBranch        *b_Kminus_Dsminus_TRUEENDVERTEX_X;   //!
   TBranch        *b_Kminus_Dsminus_TRUEENDVERTEX_Y;   //!
   TBranch        *b_Kminus_Dsminus_TRUEENDVERTEX_Z;   //!
   TBranch        *b_Kminus_Dsminus_TRUEISSTABLE;   //!
   TBranch        *b_Kminus_Dsminus_TRUETAU;   //!
   TBranch        *b_Kminus_Dsminus_ID;   //!
   TBranch        *b_Kminus_Dsminus_CombDLLMu;   //!
   TBranch        *b_Kminus_Dsminus_ProbNNmu;   //!
   TBranch        *b_Kminus_Dsminus_ProbNNghost;   //!
   TBranch        *b_Kminus_Dsminus_InMuonAcc;   //!
   TBranch        *b_Kminus_Dsminus_MuonDist2;   //!
   TBranch        *b_Kminus_Dsminus_regionInM2;   //!
   TBranch        *b_Kminus_Dsminus_hasMuon;   //!
   TBranch        *b_Kminus_Dsminus_isMuon;   //!
   TBranch        *b_Kminus_Dsminus_isMuonLoose;   //!
   TBranch        *b_Kminus_Dsminus_NShared;   //!
   TBranch        *b_Kminus_Dsminus_MuonLLmu;   //!
   TBranch        *b_Kminus_Dsminus_MuonLLbg;   //!
   TBranch        *b_Kminus_Dsminus_isMuonFromProto;   //!
   TBranch        *b_Kminus_Dsminus_PIDe;   //!
   TBranch        *b_Kminus_Dsminus_PIDmu;   //!
   TBranch        *b_Kminus_Dsminus_PIDK;   //!
   TBranch        *b_Kminus_Dsminus_PIDp;   //!
   TBranch        *b_Kminus_Dsminus_PIDd;   //!
   TBranch        *b_Kminus_Dsminus_ProbNNe;   //!
   TBranch        *b_Kminus_Dsminus_ProbNNk;   //!
   TBranch        *b_Kminus_Dsminus_ProbNNp;   //!
   TBranch        *b_Kminus_Dsminus_ProbNNpi;   //!
   TBranch        *b_Kminus_Dsminus_ProbNNd;   //!
   TBranch        *b_Kminus_Dsminus_hasRich;   //!
   TBranch        *b_Kminus_Dsminus_UsedRichAerogel;   //!
   TBranch        *b_Kminus_Dsminus_UsedRich1Gas;   //!
   TBranch        *b_Kminus_Dsminus_UsedRich2Gas;   //!
   TBranch        *b_Kminus_Dsminus_RichAboveElThres;   //!
   TBranch        *b_Kminus_Dsminus_RichAboveMuThres;   //!
   TBranch        *b_Kminus_Dsminus_RichAbovePiThres;   //!
   TBranch        *b_Kminus_Dsminus_RichAboveKaThres;   //!
   TBranch        *b_Kminus_Dsminus_RichAbovePrThres;   //!
   TBranch        *b_Kminus_Dsminus_hasCalo;   //!
   TBranch        *b_Kminus_Dsminus_L0Global_Dec;   //!
   TBranch        *b_Kminus_Dsminus_L0Global_TIS;   //!
   TBranch        *b_Kminus_Dsminus_L0Global_TOS;   //!
   TBranch        *b_Kminus_Dsminus_Hlt1Global_Dec;   //!
   TBranch        *b_Kminus_Dsminus_Hlt1Global_TIS;   //!
   TBranch        *b_Kminus_Dsminus_Hlt1Global_TOS;   //!
   TBranch        *b_Kminus_Dsminus_Hlt1Phys_Dec;   //!
   TBranch        *b_Kminus_Dsminus_Hlt1Phys_TIS;   //!
   TBranch        *b_Kminus_Dsminus_Hlt1Phys_TOS;   //!
   TBranch        *b_Kminus_Dsminus_Hlt2Global_Dec;   //!
   TBranch        *b_Kminus_Dsminus_Hlt2Global_TIS;   //!
   TBranch        *b_Kminus_Dsminus_Hlt2Global_TOS;   //!
   TBranch        *b_Kminus_Dsminus_Hlt2Phys_Dec;   //!
   TBranch        *b_Kminus_Dsminus_Hlt2Phys_TIS;   //!
   TBranch        *b_Kminus_Dsminus_Hlt2Phys_TOS;   //!
   TBranch        *b_Kminus_Dsminus_L0HadronDecision_Dec;   //!
   TBranch        *b_Kminus_Dsminus_L0HadronDecision_TIS;   //!
   TBranch        *b_Kminus_Dsminus_L0HadronDecision_TOS;   //!
   TBranch        *b_Kminus_Dsminus_L0ElectronDecision_Dec;   //!
   TBranch        *b_Kminus_Dsminus_L0ElectronDecision_TIS;   //!
   TBranch        *b_Kminus_Dsminus_L0ElectronDecision_TOS;   //!
   TBranch        *b_Kminus_Dsminus_L0ElectronHiDecision_Dec;   //!
   TBranch        *b_Kminus_Dsminus_L0ElectronHiDecision_TIS;   //!
   TBranch        *b_Kminus_Dsminus_L0ElectronHiDecision_TOS;   //!
   TBranch        *b_Kminus_Dsminus_L0MuonDecision_Dec;   //!
   TBranch        *b_Kminus_Dsminus_L0MuonDecision_TIS;   //!
   TBranch        *b_Kminus_Dsminus_L0MuonDecision_TOS;   //!
   TBranch        *b_Kminus_Dsminus_L0DiMuonDecision_Dec;   //!
   TBranch        *b_Kminus_Dsminus_L0DiMuonDecision_TIS;   //!
   TBranch        *b_Kminus_Dsminus_L0DiMuonDecision_TOS;   //!
   TBranch        *b_Kminus_Dsminus_L0MuonHighDecision_Dec;   //!
   TBranch        *b_Kminus_Dsminus_L0MuonHighDecision_TIS;   //!
   TBranch        *b_Kminus_Dsminus_L0MuonHighDecision_TOS;   //!
   TBranch        *b_Kminus_Dsminus_Hlt1TrackMVADecision_Dec;   //!
   TBranch        *b_Kminus_Dsminus_Hlt1TrackMVADecision_TIS;   //!
   TBranch        *b_Kminus_Dsminus_Hlt1TrackMVADecision_TOS;   //!
   TBranch        *b_Kminus_Dsminus_Hlt1TwoTrackMVADecision_Dec;   //!
   TBranch        *b_Kminus_Dsminus_Hlt1TwoTrackMVADecision_TIS;   //!
   TBranch        *b_Kminus_Dsminus_Hlt1TwoTrackMVADecision_TOS;   //!
   TBranch        *b_Kminus_Dsminus_Hlt1GlobalDecision_Dec;   //!
   TBranch        *b_Kminus_Dsminus_Hlt1GlobalDecision_TIS;   //!
   TBranch        *b_Kminus_Dsminus_Hlt1GlobalDecision_TOS;   //!
   TBranch        *b_Kminus_Dsminus_Hlt1TrackAllL0Decision_Dec;   //!
   TBranch        *b_Kminus_Dsminus_Hlt1TrackAllL0Decision_TIS;   //!
   TBranch        *b_Kminus_Dsminus_Hlt1TrackAllL0Decision_TOS;   //!
   TBranch        *b_Kminus_Dsminus_Hlt2Topo2BodyBBDTDecision_Dec;   //!
   TBranch        *b_Kminus_Dsminus_Hlt2Topo2BodyBBDTDecision_TIS;   //!
   TBranch        *b_Kminus_Dsminus_Hlt2Topo2BodyBBDTDecision_TOS;   //!
   TBranch        *b_Kminus_Dsminus_Hlt2Topo3BodyBBDTDecision_Dec;   //!
   TBranch        *b_Kminus_Dsminus_Hlt2Topo3BodyBBDTDecision_TIS;   //!
   TBranch        *b_Kminus_Dsminus_Hlt2Topo3BodyBBDTDecision_TOS;   //!
   TBranch        *b_Kminus_Dsminus_Hlt2Topo4BodyBBDTDecision_Dec;   //!
   TBranch        *b_Kminus_Dsminus_Hlt2Topo4BodyBBDTDecision_TIS;   //!
   TBranch        *b_Kminus_Dsminus_Hlt2Topo4BodyBBDTDecision_TOS;   //!
   TBranch        *b_Kminus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_Dec;   //!
   TBranch        *b_Kminus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_TIS;   //!
   TBranch        *b_Kminus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_TOS;   //!
   TBranch        *b_Kminus_Dsminus_Hlt2Topo2BodyDecision_Dec;   //!
   TBranch        *b_Kminus_Dsminus_Hlt2Topo2BodyDecision_TIS;   //!
   TBranch        *b_Kminus_Dsminus_Hlt2Topo2BodyDecision_TOS;   //!
   TBranch        *b_Kminus_Dsminus_Hlt2Topo3BodyDecision_Dec;   //!
   TBranch        *b_Kminus_Dsminus_Hlt2Topo3BodyDecision_TIS;   //!
   TBranch        *b_Kminus_Dsminus_Hlt2Topo3BodyDecision_TOS;   //!
   TBranch        *b_Kminus_Dsminus_Hlt2Topo4BodyDecision_Dec;   //!
   TBranch        *b_Kminus_Dsminus_Hlt2Topo4BodyDecision_TIS;   //!
   TBranch        *b_Kminus_Dsminus_Hlt2Topo4BodyDecision_TOS;   //!
   TBranch        *b_Kminus_Dsminus_Hlt2GlobalDecision_Dec;   //!
   TBranch        *b_Kminus_Dsminus_Hlt2GlobalDecision_TIS;   //!
   TBranch        *b_Kminus_Dsminus_Hlt2GlobalDecision_TOS;   //!
   TBranch        *b_Kminus_Dsminus_TRACK_Type;   //!
   TBranch        *b_Kminus_Dsminus_TRACK_Key;   //!
   TBranch        *b_Kminus_Dsminus_TRACK_CHI2NDOF;   //!
   TBranch        *b_Kminus_Dsminus_TRACK_PCHI2;   //!
   TBranch        *b_Kminus_Dsminus_TRACK_MatchCHI2;   //!
   TBranch        *b_Kminus_Dsminus_TRACK_GhostProb;   //!
   TBranch        *b_Kminus_Dsminus_TRACK_CloneDist;   //!
   TBranch        *b_Kminus_Dsminus_TRACK_Likelihood;   //!
   TBranch        *b_Kminus_Dsminus_X;   //!
   TBranch        *b_Kminus_Dsminus_Y;   //!
   TBranch        *b_piminus_Dsminus_ETA;   //!
   TBranch        *b_piminus_Dsminus_MinIPCHI2;   //!
   TBranch        *b_piminus_Dsminus_MC12TuneV2_ProbNNe;   //!
   TBranch        *b_piminus_Dsminus_MC12TuneV2_ProbNNmu;   //!
   TBranch        *b_piminus_Dsminus_MC12TuneV2_ProbNNpi;   //!
   TBranch        *b_piminus_Dsminus_MC12TuneV2_ProbNNk;   //!
   TBranch        *b_piminus_Dsminus_MC12TuneV2_ProbNNp;   //!
   TBranch        *b_piminus_Dsminus_MC12TuneV2_ProbNNghost;   //!
   TBranch        *b_piminus_Dsminus_MC12TuneV3_ProbNNe;   //!
   TBranch        *b_piminus_Dsminus_MC12TuneV3_ProbNNmu;   //!
   TBranch        *b_piminus_Dsminus_MC12TuneV3_ProbNNpi;   //!
   TBranch        *b_piminus_Dsminus_MC12TuneV3_ProbNNk;   //!
   TBranch        *b_piminus_Dsminus_MC12TuneV3_ProbNNp;   //!
   TBranch        *b_piminus_Dsminus_MC12TuneV3_ProbNNghost;   //!
   TBranch        *b_piminus_Dsminus_MC12TuneV4_ProbNNe;   //!
   TBranch        *b_piminus_Dsminus_MC12TuneV4_ProbNNmu;   //!
   TBranch        *b_piminus_Dsminus_MC12TuneV4_ProbNNpi;   //!
   TBranch        *b_piminus_Dsminus_MC12TuneV4_ProbNNk;   //!
   TBranch        *b_piminus_Dsminus_MC12TuneV4_ProbNNp;   //!
   TBranch        *b_piminus_Dsminus_MC12TuneV4_ProbNNghost;   //!
   TBranch        *b_piminus_Dsminus_MC15TuneV1_ProbNNe;   //!
   TBranch        *b_piminus_Dsminus_MC15TuneV1_ProbNNmu;   //!
   TBranch        *b_piminus_Dsminus_MC15TuneV1_ProbNNpi;   //!
   TBranch        *b_piminus_Dsminus_MC15TuneV1_ProbNNk;   //!
   TBranch        *b_piminus_Dsminus_MC15TuneV1_ProbNNp;   //!
   TBranch        *b_piminus_Dsminus_MC15TuneV1_ProbNNghost;   //!
   TBranch        *b_piminus_Dsminus_CosTheta;   //!
   TBranch        *b_piminus_Dsminus_OWNPV_X;   //!
   TBranch        *b_piminus_Dsminus_OWNPV_Y;   //!
   TBranch        *b_piminus_Dsminus_OWNPV_Z;   //!
   TBranch        *b_piminus_Dsminus_OWNPV_XERR;   //!
   TBranch        *b_piminus_Dsminus_OWNPV_YERR;   //!
   TBranch        *b_piminus_Dsminus_OWNPV_ZERR;   //!
   TBranch        *b_piminus_Dsminus_OWNPV_CHI2;   //!
   TBranch        *b_piminus_Dsminus_OWNPV_NDOF;   //!
   TBranch        *b_piminus_Dsminus_OWNPV_COV_;   //!
   TBranch        *b_piminus_Dsminus_IP_OWNPV;   //!
   TBranch        *b_piminus_Dsminus_IPCHI2_OWNPV;   //!
   TBranch        *b_piminus_Dsminus_ORIVX_X;   //!
   TBranch        *b_piminus_Dsminus_ORIVX_Y;   //!
   TBranch        *b_piminus_Dsminus_ORIVX_Z;   //!
   TBranch        *b_piminus_Dsminus_ORIVX_XERR;   //!
   TBranch        *b_piminus_Dsminus_ORIVX_YERR;   //!
   TBranch        *b_piminus_Dsminus_ORIVX_ZERR;   //!
   TBranch        *b_piminus_Dsminus_ORIVX_CHI2;   //!
   TBranch        *b_piminus_Dsminus_ORIVX_NDOF;   //!
   TBranch        *b_piminus_Dsminus_ORIVX_COV_;   //!
   TBranch        *b_piminus_Dsminus_P;   //!
   TBranch        *b_piminus_Dsminus_PT;   //!
   TBranch        *b_piminus_Dsminus_PE;   //!
   TBranch        *b_piminus_Dsminus_PX;   //!
   TBranch        *b_piminus_Dsminus_PY;   //!
   TBranch        *b_piminus_Dsminus_PZ;   //!
   TBranch        *b_piminus_Dsminus_REFPX;   //!
   TBranch        *b_piminus_Dsminus_REFPY;   //!
   TBranch        *b_piminus_Dsminus_REFPZ;   //!
   TBranch        *b_piminus_Dsminus_M;   //!
   TBranch        *b_piminus_Dsminus_AtVtx_PE;   //!
   TBranch        *b_piminus_Dsminus_AtVtx_PX;   //!
   TBranch        *b_piminus_Dsminus_AtVtx_PY;   //!
   TBranch        *b_piminus_Dsminus_AtVtx_PZ;   //!
   TBranch        *b_piminus_Dsminus_TRUEID;   //!
   TBranch        *b_piminus_Dsminus_TRUEP_E;   //!
   TBranch        *b_piminus_Dsminus_TRUEP_X;   //!
   TBranch        *b_piminus_Dsminus_TRUEP_Y;   //!
   TBranch        *b_piminus_Dsminus_TRUEP_Z;   //!
   TBranch        *b_piminus_Dsminus_TRUEPT;   //!
   TBranch        *b_piminus_Dsminus_TRUEORIGINVERTEX_X;   //!
   TBranch        *b_piminus_Dsminus_TRUEORIGINVERTEX_Y;   //!
   TBranch        *b_piminus_Dsminus_TRUEORIGINVERTEX_Z;   //!
   TBranch        *b_piminus_Dsminus_TRUEENDVERTEX_X;   //!
   TBranch        *b_piminus_Dsminus_TRUEENDVERTEX_Y;   //!
   TBranch        *b_piminus_Dsminus_TRUEENDVERTEX_Z;   //!
   TBranch        *b_piminus_Dsminus_TRUEISSTABLE;   //!
   TBranch        *b_piminus_Dsminus_TRUETAU;   //!
   TBranch        *b_piminus_Dsminus_ID;   //!
   TBranch        *b_piminus_Dsminus_CombDLLMu;   //!
   TBranch        *b_piminus_Dsminus_ProbNNmu;   //!
   TBranch        *b_piminus_Dsminus_ProbNNghost;   //!
   TBranch        *b_piminus_Dsminus_InMuonAcc;   //!
   TBranch        *b_piminus_Dsminus_MuonDist2;   //!
   TBranch        *b_piminus_Dsminus_regionInM2;   //!
   TBranch        *b_piminus_Dsminus_hasMuon;   //!
   TBranch        *b_piminus_Dsminus_isMuon;   //!
   TBranch        *b_piminus_Dsminus_isMuonLoose;   //!
   TBranch        *b_piminus_Dsminus_NShared;   //!
   TBranch        *b_piminus_Dsminus_MuonLLmu;   //!
   TBranch        *b_piminus_Dsminus_MuonLLbg;   //!
   TBranch        *b_piminus_Dsminus_isMuonFromProto;   //!
   TBranch        *b_piminus_Dsminus_PIDe;   //!
   TBranch        *b_piminus_Dsminus_PIDmu;   //!
   TBranch        *b_piminus_Dsminus_PIDK;   //!
   TBranch        *b_piminus_Dsminus_PIDp;   //!
   TBranch        *b_piminus_Dsminus_PIDd;   //!
   TBranch        *b_piminus_Dsminus_ProbNNe;   //!
   TBranch        *b_piminus_Dsminus_ProbNNk;   //!
   TBranch        *b_piminus_Dsminus_ProbNNp;   //!
   TBranch        *b_piminus_Dsminus_ProbNNpi;   //!
   TBranch        *b_piminus_Dsminus_ProbNNd;   //!
   TBranch        *b_piminus_Dsminus_hasRich;   //!
   TBranch        *b_piminus_Dsminus_UsedRichAerogel;   //!
   TBranch        *b_piminus_Dsminus_UsedRich1Gas;   //!
   TBranch        *b_piminus_Dsminus_UsedRich2Gas;   //!
   TBranch        *b_piminus_Dsminus_RichAboveElThres;   //!
   TBranch        *b_piminus_Dsminus_RichAboveMuThres;   //!
   TBranch        *b_piminus_Dsminus_RichAbovePiThres;   //!
   TBranch        *b_piminus_Dsminus_RichAboveKaThres;   //!
   TBranch        *b_piminus_Dsminus_RichAbovePrThres;   //!
   TBranch        *b_piminus_Dsminus_hasCalo;   //!
   TBranch        *b_piminus_Dsminus_L0Global_Dec;   //!
   TBranch        *b_piminus_Dsminus_L0Global_TIS;   //!
   TBranch        *b_piminus_Dsminus_L0Global_TOS;   //!
   TBranch        *b_piminus_Dsminus_Hlt1Global_Dec;   //!
   TBranch        *b_piminus_Dsminus_Hlt1Global_TIS;   //!
   TBranch        *b_piminus_Dsminus_Hlt1Global_TOS;   //!
   TBranch        *b_piminus_Dsminus_Hlt1Phys_Dec;   //!
   TBranch        *b_piminus_Dsminus_Hlt1Phys_TIS;   //!
   TBranch        *b_piminus_Dsminus_Hlt1Phys_TOS;   //!
   TBranch        *b_piminus_Dsminus_Hlt2Global_Dec;   //!
   TBranch        *b_piminus_Dsminus_Hlt2Global_TIS;   //!
   TBranch        *b_piminus_Dsminus_Hlt2Global_TOS;   //!
   TBranch        *b_piminus_Dsminus_Hlt2Phys_Dec;   //!
   TBranch        *b_piminus_Dsminus_Hlt2Phys_TIS;   //!
   TBranch        *b_piminus_Dsminus_Hlt2Phys_TOS;   //!
   TBranch        *b_piminus_Dsminus_L0HadronDecision_Dec;   //!
   TBranch        *b_piminus_Dsminus_L0HadronDecision_TIS;   //!
   TBranch        *b_piminus_Dsminus_L0HadronDecision_TOS;   //!
   TBranch        *b_piminus_Dsminus_L0ElectronDecision_Dec;   //!
   TBranch        *b_piminus_Dsminus_L0ElectronDecision_TIS;   //!
   TBranch        *b_piminus_Dsminus_L0ElectronDecision_TOS;   //!
   TBranch        *b_piminus_Dsminus_L0ElectronHiDecision_Dec;   //!
   TBranch        *b_piminus_Dsminus_L0ElectronHiDecision_TIS;   //!
   TBranch        *b_piminus_Dsminus_L0ElectronHiDecision_TOS;   //!
   TBranch        *b_piminus_Dsminus_L0MuonDecision_Dec;   //!
   TBranch        *b_piminus_Dsminus_L0MuonDecision_TIS;   //!
   TBranch        *b_piminus_Dsminus_L0MuonDecision_TOS;   //!
   TBranch        *b_piminus_Dsminus_L0DiMuonDecision_Dec;   //!
   TBranch        *b_piminus_Dsminus_L0DiMuonDecision_TIS;   //!
   TBranch        *b_piminus_Dsminus_L0DiMuonDecision_TOS;   //!
   TBranch        *b_piminus_Dsminus_L0MuonHighDecision_Dec;   //!
   TBranch        *b_piminus_Dsminus_L0MuonHighDecision_TIS;   //!
   TBranch        *b_piminus_Dsminus_L0MuonHighDecision_TOS;   //!
   TBranch        *b_piminus_Dsminus_Hlt1TrackMVADecision_Dec;   //!
   TBranch        *b_piminus_Dsminus_Hlt1TrackMVADecision_TIS;   //!
   TBranch        *b_piminus_Dsminus_Hlt1TrackMVADecision_TOS;   //!
   TBranch        *b_piminus_Dsminus_Hlt1TwoTrackMVADecision_Dec;   //!
   TBranch        *b_piminus_Dsminus_Hlt1TwoTrackMVADecision_TIS;   //!
   TBranch        *b_piminus_Dsminus_Hlt1TwoTrackMVADecision_TOS;   //!
   TBranch        *b_piminus_Dsminus_Hlt1GlobalDecision_Dec;   //!
   TBranch        *b_piminus_Dsminus_Hlt1GlobalDecision_TIS;   //!
   TBranch        *b_piminus_Dsminus_Hlt1GlobalDecision_TOS;   //!
   TBranch        *b_piminus_Dsminus_Hlt1TrackAllL0Decision_Dec;   //!
   TBranch        *b_piminus_Dsminus_Hlt1TrackAllL0Decision_TIS;   //!
   TBranch        *b_piminus_Dsminus_Hlt1TrackAllL0Decision_TOS;   //!
   TBranch        *b_piminus_Dsminus_Hlt2Topo2BodyBBDTDecision_Dec;   //!
   TBranch        *b_piminus_Dsminus_Hlt2Topo2BodyBBDTDecision_TIS;   //!
   TBranch        *b_piminus_Dsminus_Hlt2Topo2BodyBBDTDecision_TOS;   //!
   TBranch        *b_piminus_Dsminus_Hlt2Topo3BodyBBDTDecision_Dec;   //!
   TBranch        *b_piminus_Dsminus_Hlt2Topo3BodyBBDTDecision_TIS;   //!
   TBranch        *b_piminus_Dsminus_Hlt2Topo3BodyBBDTDecision_TOS;   //!
   TBranch        *b_piminus_Dsminus_Hlt2Topo4BodyBBDTDecision_Dec;   //!
   TBranch        *b_piminus_Dsminus_Hlt2Topo4BodyBBDTDecision_TIS;   //!
   TBranch        *b_piminus_Dsminus_Hlt2Topo4BodyBBDTDecision_TOS;   //!
   TBranch        *b_piminus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_Dec;   //!
   TBranch        *b_piminus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_TIS;   //!
   TBranch        *b_piminus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_TOS;   //!
   TBranch        *b_piminus_Dsminus_Hlt2Topo2BodyDecision_Dec;   //!
   TBranch        *b_piminus_Dsminus_Hlt2Topo2BodyDecision_TIS;   //!
   TBranch        *b_piminus_Dsminus_Hlt2Topo2BodyDecision_TOS;   //!
   TBranch        *b_piminus_Dsminus_Hlt2Topo3BodyDecision_Dec;   //!
   TBranch        *b_piminus_Dsminus_Hlt2Topo3BodyDecision_TIS;   //!
   TBranch        *b_piminus_Dsminus_Hlt2Topo3BodyDecision_TOS;   //!
   TBranch        *b_piminus_Dsminus_Hlt2Topo4BodyDecision_Dec;   //!
   TBranch        *b_piminus_Dsminus_Hlt2Topo4BodyDecision_TIS;   //!
   TBranch        *b_piminus_Dsminus_Hlt2Topo4BodyDecision_TOS;   //!
   TBranch        *b_piminus_Dsminus_Hlt2GlobalDecision_Dec;   //!
   TBranch        *b_piminus_Dsminus_Hlt2GlobalDecision_TIS;   //!
   TBranch        *b_piminus_Dsminus_Hlt2GlobalDecision_TOS;   //!
   TBranch        *b_piminus_Dsminus_TRACK_Type;   //!
   TBranch        *b_piminus_Dsminus_TRACK_Key;   //!
   TBranch        *b_piminus_Dsminus_TRACK_CHI2NDOF;   //!
   TBranch        *b_piminus_Dsminus_TRACK_PCHI2;   //!
   TBranch        *b_piminus_Dsminus_TRACK_MatchCHI2;   //!
   TBranch        *b_piminus_Dsminus_TRACK_GhostProb;   //!
   TBranch        *b_piminus_Dsminus_TRACK_CloneDist;   //!
   TBranch        *b_piminus_Dsminus_TRACK_Likelihood;   //!
   TBranch        *b_piminus_Dsminus_X;   //!
   TBranch        *b_piminus_Dsminus_Y;   //!
   TBranch        *b_Dplus_ETA;   //!
   TBranch        *b_Dplus_MinIPCHI2;   //!
   TBranch        *b_Dplus_CosTheta;   //!
   TBranch        *b_Dplus_ENDVERTEX_X;   //!
   TBranch        *b_Dplus_ENDVERTEX_Y;   //!
   TBranch        *b_Dplus_ENDVERTEX_Z;   //!
   TBranch        *b_Dplus_ENDVERTEX_XERR;   //!
   TBranch        *b_Dplus_ENDVERTEX_YERR;   //!
   TBranch        *b_Dplus_ENDVERTEX_ZERR;   //!
   TBranch        *b_Dplus_ENDVERTEX_CHI2;   //!
   TBranch        *b_Dplus_ENDVERTEX_NDOF;   //!
   TBranch        *b_Dplus_ENDVERTEX_COV_;   //!
   TBranch        *b_Dplus_OWNPV_X;   //!
   TBranch        *b_Dplus_OWNPV_Y;   //!
   TBranch        *b_Dplus_OWNPV_Z;   //!
   TBranch        *b_Dplus_OWNPV_XERR;   //!
   TBranch        *b_Dplus_OWNPV_YERR;   //!
   TBranch        *b_Dplus_OWNPV_ZERR;   //!
   TBranch        *b_Dplus_OWNPV_CHI2;   //!
   TBranch        *b_Dplus_OWNPV_NDOF;   //!
   TBranch        *b_Dplus_OWNPV_COV_;   //!
   TBranch        *b_Dplus_IP_OWNPV;   //!
   TBranch        *b_Dplus_IPCHI2_OWNPV;   //!
   TBranch        *b_Dplus_FD_OWNPV;   //!
   TBranch        *b_Dplus_FDCHI2_OWNPV;   //!
   TBranch        *b_Dplus_DIRA_OWNPV;   //!
   TBranch        *b_Dplus_ORIVX_X;   //!
   TBranch        *b_Dplus_ORIVX_Y;   //!
   TBranch        *b_Dplus_ORIVX_Z;   //!
   TBranch        *b_Dplus_ORIVX_XERR;   //!
   TBranch        *b_Dplus_ORIVX_YERR;   //!
   TBranch        *b_Dplus_ORIVX_ZERR;   //!
   TBranch        *b_Dplus_ORIVX_CHI2;   //!
   TBranch        *b_Dplus_ORIVX_NDOF;   //!
   TBranch        *b_Dplus_ORIVX_COV_;   //!
   TBranch        *b_Dplus_FD_ORIVX;   //!
   TBranch        *b_Dplus_FDCHI2_ORIVX;   //!
   TBranch        *b_Dplus_DIRA_ORIVX;   //!
   TBranch        *b_Dplus_P;   //!
   TBranch        *b_Dplus_PT;   //!
   TBranch        *b_Dplus_PE;   //!
   TBranch        *b_Dplus_PX;   //!
   TBranch        *b_Dplus_PY;   //!
   TBranch        *b_Dplus_PZ;   //!
   TBranch        *b_Dplus_REFPX;   //!
   TBranch        *b_Dplus_REFPY;   //!
   TBranch        *b_Dplus_REFPZ;   //!
   TBranch        *b_Dplus_MM;   //!
   TBranch        *b_Dplus_MMERR;   //!
   TBranch        *b_Dplus_M;   //!
   TBranch        *b_Dplus_BKGCAT;   //!
   TBranch        *b_Dplus_TRUEID;   //!
   TBranch        *b_Dplus_TRUEP_E;   //!
   TBranch        *b_Dplus_TRUEP_X;   //!
   TBranch        *b_Dplus_TRUEP_Y;   //!
   TBranch        *b_Dplus_TRUEP_Z;   //!
   TBranch        *b_Dplus_TRUEPT;   //!
   TBranch        *b_Dplus_TRUEORIGINVERTEX_X;   //!
   TBranch        *b_Dplus_TRUEORIGINVERTEX_Y;   //!
   TBranch        *b_Dplus_TRUEORIGINVERTEX_Z;   //!
   TBranch        *b_Dplus_TRUEENDVERTEX_X;   //!
   TBranch        *b_Dplus_TRUEENDVERTEX_Y;   //!
   TBranch        *b_Dplus_TRUEENDVERTEX_Z;   //!
   TBranch        *b_Dplus_TRUEISSTABLE;   //!
   TBranch        *b_Dplus_TRUETAU;   //!
   TBranch        *b_Dplus_ID;   //!
   TBranch        *b_Dplus_TAU;   //!
   TBranch        *b_Dplus_TAUERR;   //!
   TBranch        *b_Dplus_TAUCHI2;   //!
   TBranch        *b_Dplus_L0Global_Dec;   //!
   TBranch        *b_Dplus_L0Global_TIS;   //!
   TBranch        *b_Dplus_L0Global_TOS;   //!
   TBranch        *b_Dplus_Hlt1Global_Dec;   //!
   TBranch        *b_Dplus_Hlt1Global_TIS;   //!
   TBranch        *b_Dplus_Hlt1Global_TOS;   //!
   TBranch        *b_Dplus_Hlt1Phys_Dec;   //!
   TBranch        *b_Dplus_Hlt1Phys_TIS;   //!
   TBranch        *b_Dplus_Hlt1Phys_TOS;   //!
   TBranch        *b_Dplus_Hlt2Global_Dec;   //!
   TBranch        *b_Dplus_Hlt2Global_TIS;   //!
   TBranch        *b_Dplus_Hlt2Global_TOS;   //!
   TBranch        *b_Dplus_Hlt2Phys_Dec;   //!
   TBranch        *b_Dplus_Hlt2Phys_TIS;   //!
   TBranch        *b_Dplus_Hlt2Phys_TOS;   //!
   TBranch        *b_Dplus_L0HadronDecision_Dec;   //!
   TBranch        *b_Dplus_L0HadronDecision_TIS;   //!
   TBranch        *b_Dplus_L0HadronDecision_TOS;   //!
   TBranch        *b_Dplus_L0ElectronDecision_Dec;   //!
   TBranch        *b_Dplus_L0ElectronDecision_TIS;   //!
   TBranch        *b_Dplus_L0ElectronDecision_TOS;   //!
   TBranch        *b_Dplus_L0ElectronHiDecision_Dec;   //!
   TBranch        *b_Dplus_L0ElectronHiDecision_TIS;   //!
   TBranch        *b_Dplus_L0ElectronHiDecision_TOS;   //!
   TBranch        *b_Dplus_L0MuonDecision_Dec;   //!
   TBranch        *b_Dplus_L0MuonDecision_TIS;   //!
   TBranch        *b_Dplus_L0MuonDecision_TOS;   //!
   TBranch        *b_Dplus_L0DiMuonDecision_Dec;   //!
   TBranch        *b_Dplus_L0DiMuonDecision_TIS;   //!
   TBranch        *b_Dplus_L0DiMuonDecision_TOS;   //!
   TBranch        *b_Dplus_L0MuonHighDecision_Dec;   //!
   TBranch        *b_Dplus_L0MuonHighDecision_TIS;   //!
   TBranch        *b_Dplus_L0MuonHighDecision_TOS;   //!
   TBranch        *b_Dplus_Hlt1TrackMVADecision_Dec;   //!
   TBranch        *b_Dplus_Hlt1TrackMVADecision_TIS;   //!
   TBranch        *b_Dplus_Hlt1TrackMVADecision_TOS;   //!
   TBranch        *b_Dplus_Hlt1TwoTrackMVADecision_Dec;   //!
   TBranch        *b_Dplus_Hlt1TwoTrackMVADecision_TIS;   //!
   TBranch        *b_Dplus_Hlt1TwoTrackMVADecision_TOS;   //!
   TBranch        *b_Dplus_Hlt1GlobalDecision_Dec;   //!
   TBranch        *b_Dplus_Hlt1GlobalDecision_TIS;   //!
   TBranch        *b_Dplus_Hlt1GlobalDecision_TOS;   //!
   TBranch        *b_Dplus_Hlt1TrackAllL0Decision_Dec;   //!
   TBranch        *b_Dplus_Hlt1TrackAllL0Decision_TIS;   //!
   TBranch        *b_Dplus_Hlt1TrackAllL0Decision_TOS;   //!
   TBranch        *b_Dplus_Hlt2Topo2BodyBBDTDecision_Dec;   //!
   TBranch        *b_Dplus_Hlt2Topo2BodyBBDTDecision_TIS;   //!
   TBranch        *b_Dplus_Hlt2Topo2BodyBBDTDecision_TOS;   //!
   TBranch        *b_Dplus_Hlt2Topo3BodyBBDTDecision_Dec;   //!
   TBranch        *b_Dplus_Hlt2Topo3BodyBBDTDecision_TIS;   //!
   TBranch        *b_Dplus_Hlt2Topo3BodyBBDTDecision_TOS;   //!
   TBranch        *b_Dplus_Hlt2Topo4BodyBBDTDecision_Dec;   //!
   TBranch        *b_Dplus_Hlt2Topo4BodyBBDTDecision_TIS;   //!
   TBranch        *b_Dplus_Hlt2Topo4BodyBBDTDecision_TOS;   //!
   TBranch        *b_Dplus_Hlt2BHadB02PpPpPmPmDecision_Dec;   //!
   TBranch        *b_Dplus_Hlt2BHadB02PpPpPmPmDecision_TIS;   //!
   TBranch        *b_Dplus_Hlt2BHadB02PpPpPmPmDecision_TOS;   //!
   TBranch        *b_Dplus_Hlt2Topo2BodyDecision_Dec;   //!
   TBranch        *b_Dplus_Hlt2Topo2BodyDecision_TIS;   //!
   TBranch        *b_Dplus_Hlt2Topo2BodyDecision_TOS;   //!
   TBranch        *b_Dplus_Hlt2Topo3BodyDecision_Dec;   //!
   TBranch        *b_Dplus_Hlt2Topo3BodyDecision_TIS;   //!
   TBranch        *b_Dplus_Hlt2Topo3BodyDecision_TOS;   //!
   TBranch        *b_Dplus_Hlt2Topo4BodyDecision_Dec;   //!
   TBranch        *b_Dplus_Hlt2Topo4BodyDecision_TIS;   //!
   TBranch        *b_Dplus_Hlt2Topo4BodyDecision_TOS;   //!
   TBranch        *b_Dplus_Hlt2GlobalDecision_Dec;   //!
   TBranch        *b_Dplus_Hlt2GlobalDecision_TIS;   //!
   TBranch        *b_Dplus_Hlt2GlobalDecision_TOS;   //!
   TBranch        *b_Dplus_X;   //!
   TBranch        *b_Dplus_Y;   //!
   TBranch        *b_Kminus_Dplus_ETA;   //!
   TBranch        *b_Kminus_Dplus_MinIPCHI2;   //!
   TBranch        *b_Kminus_Dplus_MC12TuneV2_ProbNNe;   //!
   TBranch        *b_Kminus_Dplus_MC12TuneV2_ProbNNmu;   //!
   TBranch        *b_Kminus_Dplus_MC12TuneV2_ProbNNpi;   //!
   TBranch        *b_Kminus_Dplus_MC12TuneV2_ProbNNk;   //!
   TBranch        *b_Kminus_Dplus_MC12TuneV2_ProbNNp;   //!
   TBranch        *b_Kminus_Dplus_MC12TuneV2_ProbNNghost;   //!
   TBranch        *b_Kminus_Dplus_MC12TuneV3_ProbNNe;   //!
   TBranch        *b_Kminus_Dplus_MC12TuneV3_ProbNNmu;   //!
   TBranch        *b_Kminus_Dplus_MC12TuneV3_ProbNNpi;   //!
   TBranch        *b_Kminus_Dplus_MC12TuneV3_ProbNNk;   //!
   TBranch        *b_Kminus_Dplus_MC12TuneV3_ProbNNp;   //!
   TBranch        *b_Kminus_Dplus_MC12TuneV3_ProbNNghost;   //!
   TBranch        *b_Kminus_Dplus_MC12TuneV4_ProbNNe;   //!
   TBranch        *b_Kminus_Dplus_MC12TuneV4_ProbNNmu;   //!
   TBranch        *b_Kminus_Dplus_MC12TuneV4_ProbNNpi;   //!
   TBranch        *b_Kminus_Dplus_MC12TuneV4_ProbNNk;   //!
   TBranch        *b_Kminus_Dplus_MC12TuneV4_ProbNNp;   //!
   TBranch        *b_Kminus_Dplus_MC12TuneV4_ProbNNghost;   //!
   TBranch        *b_Kminus_Dplus_MC15TuneV1_ProbNNe;   //!
   TBranch        *b_Kminus_Dplus_MC15TuneV1_ProbNNmu;   //!
   TBranch        *b_Kminus_Dplus_MC15TuneV1_ProbNNpi;   //!
   TBranch        *b_Kminus_Dplus_MC15TuneV1_ProbNNk;   //!
   TBranch        *b_Kminus_Dplus_MC15TuneV1_ProbNNp;   //!
   TBranch        *b_Kminus_Dplus_MC15TuneV1_ProbNNghost;   //!
   TBranch        *b_Kminus_Dplus_CosTheta;   //!
   TBranch        *b_Kminus_Dplus_OWNPV_X;   //!
   TBranch        *b_Kminus_Dplus_OWNPV_Y;   //!
   TBranch        *b_Kminus_Dplus_OWNPV_Z;   //!
   TBranch        *b_Kminus_Dplus_OWNPV_XERR;   //!
   TBranch        *b_Kminus_Dplus_OWNPV_YERR;   //!
   TBranch        *b_Kminus_Dplus_OWNPV_ZERR;   //!
   TBranch        *b_Kminus_Dplus_OWNPV_CHI2;   //!
   TBranch        *b_Kminus_Dplus_OWNPV_NDOF;   //!
   TBranch        *b_Kminus_Dplus_OWNPV_COV_;   //!
   TBranch        *b_Kminus_Dplus_IP_OWNPV;   //!
   TBranch        *b_Kminus_Dplus_IPCHI2_OWNPV;   //!
   TBranch        *b_Kminus_Dplus_ORIVX_X;   //!
   TBranch        *b_Kminus_Dplus_ORIVX_Y;   //!
   TBranch        *b_Kminus_Dplus_ORIVX_Z;   //!
   TBranch        *b_Kminus_Dplus_ORIVX_XERR;   //!
   TBranch        *b_Kminus_Dplus_ORIVX_YERR;   //!
   TBranch        *b_Kminus_Dplus_ORIVX_ZERR;   //!
   TBranch        *b_Kminus_Dplus_ORIVX_CHI2;   //!
   TBranch        *b_Kminus_Dplus_ORIVX_NDOF;   //!
   TBranch        *b_Kminus_Dplus_ORIVX_COV_;   //!
   TBranch        *b_Kminus_Dplus_P;   //!
   TBranch        *b_Kminus_Dplus_PT;   //!
   TBranch        *b_Kminus_Dplus_PE;   //!
   TBranch        *b_Kminus_Dplus_PX;   //!
   TBranch        *b_Kminus_Dplus_PY;   //!
   TBranch        *b_Kminus_Dplus_PZ;   //!
   TBranch        *b_Kminus_Dplus_REFPX;   //!
   TBranch        *b_Kminus_Dplus_REFPY;   //!
   TBranch        *b_Kminus_Dplus_REFPZ;   //!
   TBranch        *b_Kminus_Dplus_M;   //!
   TBranch        *b_Kminus_Dplus_AtVtx_PE;   //!
   TBranch        *b_Kminus_Dplus_AtVtx_PX;   //!
   TBranch        *b_Kminus_Dplus_AtVtx_PY;   //!
   TBranch        *b_Kminus_Dplus_AtVtx_PZ;   //!
   TBranch        *b_Kminus_Dplus_TRUEID;   //!
   TBranch        *b_Kminus_Dplus_TRUEP_E;   //!
   TBranch        *b_Kminus_Dplus_TRUEP_X;   //!
   TBranch        *b_Kminus_Dplus_TRUEP_Y;   //!
   TBranch        *b_Kminus_Dplus_TRUEP_Z;   //!
   TBranch        *b_Kminus_Dplus_TRUEPT;   //!
   TBranch        *b_Kminus_Dplus_TRUEORIGINVERTEX_X;   //!
   TBranch        *b_Kminus_Dplus_TRUEORIGINVERTEX_Y;   //!
   TBranch        *b_Kminus_Dplus_TRUEORIGINVERTEX_Z;   //!
   TBranch        *b_Kminus_Dplus_TRUEENDVERTEX_X;   //!
   TBranch        *b_Kminus_Dplus_TRUEENDVERTEX_Y;   //!
   TBranch        *b_Kminus_Dplus_TRUEENDVERTEX_Z;   //!
   TBranch        *b_Kminus_Dplus_TRUEISSTABLE;   //!
   TBranch        *b_Kminus_Dplus_TRUETAU;   //!
   TBranch        *b_Kminus_Dplus_ID;   //!
   TBranch        *b_Kminus_Dplus_CombDLLMu;   //!
   TBranch        *b_Kminus_Dplus_ProbNNmu;   //!
   TBranch        *b_Kminus_Dplus_ProbNNghost;   //!
   TBranch        *b_Kminus_Dplus_InMuonAcc;   //!
   TBranch        *b_Kminus_Dplus_MuonDist2;   //!
   TBranch        *b_Kminus_Dplus_regionInM2;   //!
   TBranch        *b_Kminus_Dplus_hasMuon;   //!
   TBranch        *b_Kminus_Dplus_isMuon;   //!
   TBranch        *b_Kminus_Dplus_isMuonLoose;   //!
   TBranch        *b_Kminus_Dplus_NShared;   //!
   TBranch        *b_Kminus_Dplus_MuonLLmu;   //!
   TBranch        *b_Kminus_Dplus_MuonLLbg;   //!
   TBranch        *b_Kminus_Dplus_isMuonFromProto;   //!
   TBranch        *b_Kminus_Dplus_PIDe;   //!
   TBranch        *b_Kminus_Dplus_PIDmu;   //!
   TBranch        *b_Kminus_Dplus_PIDK;   //!
   TBranch        *b_Kminus_Dplus_PIDp;   //!
   TBranch        *b_Kminus_Dplus_PIDd;   //!
   TBranch        *b_Kminus_Dplus_ProbNNe;   //!
   TBranch        *b_Kminus_Dplus_ProbNNk;   //!
   TBranch        *b_Kminus_Dplus_ProbNNp;   //!
   TBranch        *b_Kminus_Dplus_ProbNNpi;   //!
   TBranch        *b_Kminus_Dplus_ProbNNd;   //!
   TBranch        *b_Kminus_Dplus_hasRich;   //!
   TBranch        *b_Kminus_Dplus_UsedRichAerogel;   //!
   TBranch        *b_Kminus_Dplus_UsedRich1Gas;   //!
   TBranch        *b_Kminus_Dplus_UsedRich2Gas;   //!
   TBranch        *b_Kminus_Dplus_RichAboveElThres;   //!
   TBranch        *b_Kminus_Dplus_RichAboveMuThres;   //!
   TBranch        *b_Kminus_Dplus_RichAbovePiThres;   //!
   TBranch        *b_Kminus_Dplus_RichAboveKaThres;   //!
   TBranch        *b_Kminus_Dplus_RichAbovePrThres;   //!
   TBranch        *b_Kminus_Dplus_hasCalo;   //!
   TBranch        *b_Kminus_Dplus_L0Global_Dec;   //!
   TBranch        *b_Kminus_Dplus_L0Global_TIS;   //!
   TBranch        *b_Kminus_Dplus_L0Global_TOS;   //!
   TBranch        *b_Kminus_Dplus_Hlt1Global_Dec;   //!
   TBranch        *b_Kminus_Dplus_Hlt1Global_TIS;   //!
   TBranch        *b_Kminus_Dplus_Hlt1Global_TOS;   //!
   TBranch        *b_Kminus_Dplus_Hlt1Phys_Dec;   //!
   TBranch        *b_Kminus_Dplus_Hlt1Phys_TIS;   //!
   TBranch        *b_Kminus_Dplus_Hlt1Phys_TOS;   //!
   TBranch        *b_Kminus_Dplus_Hlt2Global_Dec;   //!
   TBranch        *b_Kminus_Dplus_Hlt2Global_TIS;   //!
   TBranch        *b_Kminus_Dplus_Hlt2Global_TOS;   //!
   TBranch        *b_Kminus_Dplus_Hlt2Phys_Dec;   //!
   TBranch        *b_Kminus_Dplus_Hlt2Phys_TIS;   //!
   TBranch        *b_Kminus_Dplus_Hlt2Phys_TOS;   //!
   TBranch        *b_Kminus_Dplus_L0HadronDecision_Dec;   //!
   TBranch        *b_Kminus_Dplus_L0HadronDecision_TIS;   //!
   TBranch        *b_Kminus_Dplus_L0HadronDecision_TOS;   //!
   TBranch        *b_Kminus_Dplus_L0ElectronDecision_Dec;   //!
   TBranch        *b_Kminus_Dplus_L0ElectronDecision_TIS;   //!
   TBranch        *b_Kminus_Dplus_L0ElectronDecision_TOS;   //!
   TBranch        *b_Kminus_Dplus_L0ElectronHiDecision_Dec;   //!
   TBranch        *b_Kminus_Dplus_L0ElectronHiDecision_TIS;   //!
   TBranch        *b_Kminus_Dplus_L0ElectronHiDecision_TOS;   //!
   TBranch        *b_Kminus_Dplus_L0MuonDecision_Dec;   //!
   TBranch        *b_Kminus_Dplus_L0MuonDecision_TIS;   //!
   TBranch        *b_Kminus_Dplus_L0MuonDecision_TOS;   //!
   TBranch        *b_Kminus_Dplus_L0DiMuonDecision_Dec;   //!
   TBranch        *b_Kminus_Dplus_L0DiMuonDecision_TIS;   //!
   TBranch        *b_Kminus_Dplus_L0DiMuonDecision_TOS;   //!
   TBranch        *b_Kminus_Dplus_L0MuonHighDecision_Dec;   //!
   TBranch        *b_Kminus_Dplus_L0MuonHighDecision_TIS;   //!
   TBranch        *b_Kminus_Dplus_L0MuonHighDecision_TOS;   //!
   TBranch        *b_Kminus_Dplus_Hlt1TrackMVADecision_Dec;   //!
   TBranch        *b_Kminus_Dplus_Hlt1TrackMVADecision_TIS;   //!
   TBranch        *b_Kminus_Dplus_Hlt1TrackMVADecision_TOS;   //!
   TBranch        *b_Kminus_Dplus_Hlt1TwoTrackMVADecision_Dec;   //!
   TBranch        *b_Kminus_Dplus_Hlt1TwoTrackMVADecision_TIS;   //!
   TBranch        *b_Kminus_Dplus_Hlt1TwoTrackMVADecision_TOS;   //!
   TBranch        *b_Kminus_Dplus_Hlt1GlobalDecision_Dec;   //!
   TBranch        *b_Kminus_Dplus_Hlt1GlobalDecision_TIS;   //!
   TBranch        *b_Kminus_Dplus_Hlt1GlobalDecision_TOS;   //!
   TBranch        *b_Kminus_Dplus_Hlt1TrackAllL0Decision_Dec;   //!
   TBranch        *b_Kminus_Dplus_Hlt1TrackAllL0Decision_TIS;   //!
   TBranch        *b_Kminus_Dplus_Hlt1TrackAllL0Decision_TOS;   //!
   TBranch        *b_Kminus_Dplus_Hlt2Topo2BodyBBDTDecision_Dec;   //!
   TBranch        *b_Kminus_Dplus_Hlt2Topo2BodyBBDTDecision_TIS;   //!
   TBranch        *b_Kminus_Dplus_Hlt2Topo2BodyBBDTDecision_TOS;   //!
   TBranch        *b_Kminus_Dplus_Hlt2Topo3BodyBBDTDecision_Dec;   //!
   TBranch        *b_Kminus_Dplus_Hlt2Topo3BodyBBDTDecision_TIS;   //!
   TBranch        *b_Kminus_Dplus_Hlt2Topo3BodyBBDTDecision_TOS;   //!
   TBranch        *b_Kminus_Dplus_Hlt2Topo4BodyBBDTDecision_Dec;   //!
   TBranch        *b_Kminus_Dplus_Hlt2Topo4BodyBBDTDecision_TIS;   //!
   TBranch        *b_Kminus_Dplus_Hlt2Topo4BodyBBDTDecision_TOS;   //!
   TBranch        *b_Kminus_Dplus_Hlt2BHadB02PpPpPmPmDecision_Dec;   //!
   TBranch        *b_Kminus_Dplus_Hlt2BHadB02PpPpPmPmDecision_TIS;   //!
   TBranch        *b_Kminus_Dplus_Hlt2BHadB02PpPpPmPmDecision_TOS;   //!
   TBranch        *b_Kminus_Dplus_Hlt2Topo2BodyDecision_Dec;   //!
   TBranch        *b_Kminus_Dplus_Hlt2Topo2BodyDecision_TIS;   //!
   TBranch        *b_Kminus_Dplus_Hlt2Topo2BodyDecision_TOS;   //!
   TBranch        *b_Kminus_Dplus_Hlt2Topo3BodyDecision_Dec;   //!
   TBranch        *b_Kminus_Dplus_Hlt2Topo3BodyDecision_TIS;   //!
   TBranch        *b_Kminus_Dplus_Hlt2Topo3BodyDecision_TOS;   //!
   TBranch        *b_Kminus_Dplus_Hlt2Topo4BodyDecision_Dec;   //!
   TBranch        *b_Kminus_Dplus_Hlt2Topo4BodyDecision_TIS;   //!
   TBranch        *b_Kminus_Dplus_Hlt2Topo4BodyDecision_TOS;   //!
   TBranch        *b_Kminus_Dplus_Hlt2GlobalDecision_Dec;   //!
   TBranch        *b_Kminus_Dplus_Hlt2GlobalDecision_TIS;   //!
   TBranch        *b_Kminus_Dplus_Hlt2GlobalDecision_TOS;   //!
   TBranch        *b_Kminus_Dplus_TRACK_Type;   //!
   TBranch        *b_Kminus_Dplus_TRACK_Key;   //!
   TBranch        *b_Kminus_Dplus_TRACK_CHI2NDOF;   //!
   TBranch        *b_Kminus_Dplus_TRACK_PCHI2;   //!
   TBranch        *b_Kminus_Dplus_TRACK_MatchCHI2;   //!
   TBranch        *b_Kminus_Dplus_TRACK_GhostProb;   //!
   TBranch        *b_Kminus_Dplus_TRACK_CloneDist;   //!
   TBranch        *b_Kminus_Dplus_TRACK_Likelihood;   //!
   TBranch        *b_Kminus_Dplus_X;   //!
   TBranch        *b_Kminus_Dplus_Y;   //!
   TBranch        *b_piplus1_Dplus_ETA;   //!
   TBranch        *b_piplus1_Dplus_MinIPCHI2;   //!
   TBranch        *b_piplus1_Dplus_MC12TuneV2_ProbNNe;   //!
   TBranch        *b_piplus1_Dplus_MC12TuneV2_ProbNNmu;   //!
   TBranch        *b_piplus1_Dplus_MC12TuneV2_ProbNNpi;   //!
   TBranch        *b_piplus1_Dplus_MC12TuneV2_ProbNNk;   //!
   TBranch        *b_piplus1_Dplus_MC12TuneV2_ProbNNp;   //!
   TBranch        *b_piplus1_Dplus_MC12TuneV2_ProbNNghost;   //!
   TBranch        *b_piplus1_Dplus_MC12TuneV3_ProbNNe;   //!
   TBranch        *b_piplus1_Dplus_MC12TuneV3_ProbNNmu;   //!
   TBranch        *b_piplus1_Dplus_MC12TuneV3_ProbNNpi;   //!
   TBranch        *b_piplus1_Dplus_MC12TuneV3_ProbNNk;   //!
   TBranch        *b_piplus1_Dplus_MC12TuneV3_ProbNNp;   //!
   TBranch        *b_piplus1_Dplus_MC12TuneV3_ProbNNghost;   //!
   TBranch        *b_piplus1_Dplus_MC12TuneV4_ProbNNe;   //!
   TBranch        *b_piplus1_Dplus_MC12TuneV4_ProbNNmu;   //!
   TBranch        *b_piplus1_Dplus_MC12TuneV4_ProbNNpi;   //!
   TBranch        *b_piplus1_Dplus_MC12TuneV4_ProbNNk;   //!
   TBranch        *b_piplus1_Dplus_MC12TuneV4_ProbNNp;   //!
   TBranch        *b_piplus1_Dplus_MC12TuneV4_ProbNNghost;   //!
   TBranch        *b_piplus1_Dplus_MC15TuneV1_ProbNNe;   //!
   TBranch        *b_piplus1_Dplus_MC15TuneV1_ProbNNmu;   //!
   TBranch        *b_piplus1_Dplus_MC15TuneV1_ProbNNpi;   //!
   TBranch        *b_piplus1_Dplus_MC15TuneV1_ProbNNk;   //!
   TBranch        *b_piplus1_Dplus_MC15TuneV1_ProbNNp;   //!
   TBranch        *b_piplus1_Dplus_MC15TuneV1_ProbNNghost;   //!
   TBranch        *b_piplus1_Dplus_CosTheta;   //!
   TBranch        *b_piplus1_Dplus_OWNPV_X;   //!
   TBranch        *b_piplus1_Dplus_OWNPV_Y;   //!
   TBranch        *b_piplus1_Dplus_OWNPV_Z;   //!
   TBranch        *b_piplus1_Dplus_OWNPV_XERR;   //!
   TBranch        *b_piplus1_Dplus_OWNPV_YERR;   //!
   TBranch        *b_piplus1_Dplus_OWNPV_ZERR;   //!
   TBranch        *b_piplus1_Dplus_OWNPV_CHI2;   //!
   TBranch        *b_piplus1_Dplus_OWNPV_NDOF;   //!
   TBranch        *b_piplus1_Dplus_OWNPV_COV_;   //!
   TBranch        *b_piplus1_Dplus_IP_OWNPV;   //!
   TBranch        *b_piplus1_Dplus_IPCHI2_OWNPV;   //!
   TBranch        *b_piplus1_Dplus_ORIVX_X;   //!
   TBranch        *b_piplus1_Dplus_ORIVX_Y;   //!
   TBranch        *b_piplus1_Dplus_ORIVX_Z;   //!
   TBranch        *b_piplus1_Dplus_ORIVX_XERR;   //!
   TBranch        *b_piplus1_Dplus_ORIVX_YERR;   //!
   TBranch        *b_piplus1_Dplus_ORIVX_ZERR;   //!
   TBranch        *b_piplus1_Dplus_ORIVX_CHI2;   //!
   TBranch        *b_piplus1_Dplus_ORIVX_NDOF;   //!
   TBranch        *b_piplus1_Dplus_ORIVX_COV_;   //!
   TBranch        *b_piplus1_Dplus_P;   //!
   TBranch        *b_piplus1_Dplus_PT;   //!
   TBranch        *b_piplus1_Dplus_PE;   //!
   TBranch        *b_piplus1_Dplus_PX;   //!
   TBranch        *b_piplus1_Dplus_PY;   //!
   TBranch        *b_piplus1_Dplus_PZ;   //!
   TBranch        *b_piplus1_Dplus_REFPX;   //!
   TBranch        *b_piplus1_Dplus_REFPY;   //!
   TBranch        *b_piplus1_Dplus_REFPZ;   //!
   TBranch        *b_piplus1_Dplus_M;   //!
   TBranch        *b_piplus1_Dplus_AtVtx_PE;   //!
   TBranch        *b_piplus1_Dplus_AtVtx_PX;   //!
   TBranch        *b_piplus1_Dplus_AtVtx_PY;   //!
   TBranch        *b_piplus1_Dplus_AtVtx_PZ;   //!
   TBranch        *b_piplus1_Dplus_TRUEID;   //!
   TBranch        *b_piplus1_Dplus_TRUEP_E;   //!
   TBranch        *b_piplus1_Dplus_TRUEP_X;   //!
   TBranch        *b_piplus1_Dplus_TRUEP_Y;   //!
   TBranch        *b_piplus1_Dplus_TRUEP_Z;   //!
   TBranch        *b_piplus1_Dplus_TRUEPT;   //!
   TBranch        *b_piplus1_Dplus_TRUEORIGINVERTEX_X;   //!
   TBranch        *b_piplus1_Dplus_TRUEORIGINVERTEX_Y;   //!
   TBranch        *b_piplus1_Dplus_TRUEORIGINVERTEX_Z;   //!
   TBranch        *b_piplus1_Dplus_TRUEENDVERTEX_X;   //!
   TBranch        *b_piplus1_Dplus_TRUEENDVERTEX_Y;   //!
   TBranch        *b_piplus1_Dplus_TRUEENDVERTEX_Z;   //!
   TBranch        *b_piplus1_Dplus_TRUEISSTABLE;   //!
   TBranch        *b_piplus1_Dplus_TRUETAU;   //!
   TBranch        *b_piplus1_Dplus_ID;   //!
   TBranch        *b_piplus1_Dplus_CombDLLMu;   //!
   TBranch        *b_piplus1_Dplus_ProbNNmu;   //!
   TBranch        *b_piplus1_Dplus_ProbNNghost;   //!
   TBranch        *b_piplus1_Dplus_InMuonAcc;   //!
   TBranch        *b_piplus1_Dplus_MuonDist2;   //!
   TBranch        *b_piplus1_Dplus_regionInM2;   //!
   TBranch        *b_piplus1_Dplus_hasMuon;   //!
   TBranch        *b_piplus1_Dplus_isMuon;   //!
   TBranch        *b_piplus1_Dplus_isMuonLoose;   //!
   TBranch        *b_piplus1_Dplus_NShared;   //!
   TBranch        *b_piplus1_Dplus_MuonLLmu;   //!
   TBranch        *b_piplus1_Dplus_MuonLLbg;   //!
   TBranch        *b_piplus1_Dplus_isMuonFromProto;   //!
   TBranch        *b_piplus1_Dplus_PIDe;   //!
   TBranch        *b_piplus1_Dplus_PIDmu;   //!
   TBranch        *b_piplus1_Dplus_PIDK;   //!
   TBranch        *b_piplus1_Dplus_PIDp;   //!
   TBranch        *b_piplus1_Dplus_PIDd;   //!
   TBranch        *b_piplus1_Dplus_ProbNNe;   //!
   TBranch        *b_piplus1_Dplus_ProbNNk;   //!
   TBranch        *b_piplus1_Dplus_ProbNNp;   //!
   TBranch        *b_piplus1_Dplus_ProbNNpi;   //!
   TBranch        *b_piplus1_Dplus_ProbNNd;   //!
   TBranch        *b_piplus1_Dplus_hasRich;   //!
   TBranch        *b_piplus1_Dplus_UsedRichAerogel;   //!
   TBranch        *b_piplus1_Dplus_UsedRich1Gas;   //!
   TBranch        *b_piplus1_Dplus_UsedRich2Gas;   //!
   TBranch        *b_piplus1_Dplus_RichAboveElThres;   //!
   TBranch        *b_piplus1_Dplus_RichAboveMuThres;   //!
   TBranch        *b_piplus1_Dplus_RichAbovePiThres;   //!
   TBranch        *b_piplus1_Dplus_RichAboveKaThres;   //!
   TBranch        *b_piplus1_Dplus_RichAbovePrThres;   //!
   TBranch        *b_piplus1_Dplus_hasCalo;   //!
   TBranch        *b_piplus1_Dplus_L0Global_Dec;   //!
   TBranch        *b_piplus1_Dplus_L0Global_TIS;   //!
   TBranch        *b_piplus1_Dplus_L0Global_TOS;   //!
   TBranch        *b_piplus1_Dplus_Hlt1Global_Dec;   //!
   TBranch        *b_piplus1_Dplus_Hlt1Global_TIS;   //!
   TBranch        *b_piplus1_Dplus_Hlt1Global_TOS;   //!
   TBranch        *b_piplus1_Dplus_Hlt1Phys_Dec;   //!
   TBranch        *b_piplus1_Dplus_Hlt1Phys_TIS;   //!
   TBranch        *b_piplus1_Dplus_Hlt1Phys_TOS;   //!
   TBranch        *b_piplus1_Dplus_Hlt2Global_Dec;   //!
   TBranch        *b_piplus1_Dplus_Hlt2Global_TIS;   //!
   TBranch        *b_piplus1_Dplus_Hlt2Global_TOS;   //!
   TBranch        *b_piplus1_Dplus_Hlt2Phys_Dec;   //!
   TBranch        *b_piplus1_Dplus_Hlt2Phys_TIS;   //!
   TBranch        *b_piplus1_Dplus_Hlt2Phys_TOS;   //!
   TBranch        *b_piplus1_Dplus_L0HadronDecision_Dec;   //!
   TBranch        *b_piplus1_Dplus_L0HadronDecision_TIS;   //!
   TBranch        *b_piplus1_Dplus_L0HadronDecision_TOS;   //!
   TBranch        *b_piplus1_Dplus_L0ElectronDecision_Dec;   //!
   TBranch        *b_piplus1_Dplus_L0ElectronDecision_TIS;   //!
   TBranch        *b_piplus1_Dplus_L0ElectronDecision_TOS;   //!
   TBranch        *b_piplus1_Dplus_L0ElectronHiDecision_Dec;   //!
   TBranch        *b_piplus1_Dplus_L0ElectronHiDecision_TIS;   //!
   TBranch        *b_piplus1_Dplus_L0ElectronHiDecision_TOS;   //!
   TBranch        *b_piplus1_Dplus_L0MuonDecision_Dec;   //!
   TBranch        *b_piplus1_Dplus_L0MuonDecision_TIS;   //!
   TBranch        *b_piplus1_Dplus_L0MuonDecision_TOS;   //!
   TBranch        *b_piplus1_Dplus_L0DiMuonDecision_Dec;   //!
   TBranch        *b_piplus1_Dplus_L0DiMuonDecision_TIS;   //!
   TBranch        *b_piplus1_Dplus_L0DiMuonDecision_TOS;   //!
   TBranch        *b_piplus1_Dplus_L0MuonHighDecision_Dec;   //!
   TBranch        *b_piplus1_Dplus_L0MuonHighDecision_TIS;   //!
   TBranch        *b_piplus1_Dplus_L0MuonHighDecision_TOS;   //!
   TBranch        *b_piplus1_Dplus_Hlt1TrackMVADecision_Dec;   //!
   TBranch        *b_piplus1_Dplus_Hlt1TrackMVADecision_TIS;   //!
   TBranch        *b_piplus1_Dplus_Hlt1TrackMVADecision_TOS;   //!
   TBranch        *b_piplus1_Dplus_Hlt1TwoTrackMVADecision_Dec;   //!
   TBranch        *b_piplus1_Dplus_Hlt1TwoTrackMVADecision_TIS;   //!
   TBranch        *b_piplus1_Dplus_Hlt1TwoTrackMVADecision_TOS;   //!
   TBranch        *b_piplus1_Dplus_Hlt1GlobalDecision_Dec;   //!
   TBranch        *b_piplus1_Dplus_Hlt1GlobalDecision_TIS;   //!
   TBranch        *b_piplus1_Dplus_Hlt1GlobalDecision_TOS;   //!
   TBranch        *b_piplus1_Dplus_Hlt1TrackAllL0Decision_Dec;   //!
   TBranch        *b_piplus1_Dplus_Hlt1TrackAllL0Decision_TIS;   //!
   TBranch        *b_piplus1_Dplus_Hlt1TrackAllL0Decision_TOS;   //!
   TBranch        *b_piplus1_Dplus_Hlt2Topo2BodyBBDTDecision_Dec;   //!
   TBranch        *b_piplus1_Dplus_Hlt2Topo2BodyBBDTDecision_TIS;   //!
   TBranch        *b_piplus1_Dplus_Hlt2Topo2BodyBBDTDecision_TOS;   //!
   TBranch        *b_piplus1_Dplus_Hlt2Topo3BodyBBDTDecision_Dec;   //!
   TBranch        *b_piplus1_Dplus_Hlt2Topo3BodyBBDTDecision_TIS;   //!
   TBranch        *b_piplus1_Dplus_Hlt2Topo3BodyBBDTDecision_TOS;   //!
   TBranch        *b_piplus1_Dplus_Hlt2Topo4BodyBBDTDecision_Dec;   //!
   TBranch        *b_piplus1_Dplus_Hlt2Topo4BodyBBDTDecision_TIS;   //!
   TBranch        *b_piplus1_Dplus_Hlt2Topo4BodyBBDTDecision_TOS;   //!
   TBranch        *b_piplus1_Dplus_Hlt2BHadB02PpPpPmPmDecision_Dec;   //!
   TBranch        *b_piplus1_Dplus_Hlt2BHadB02PpPpPmPmDecision_TIS;   //!
   TBranch        *b_piplus1_Dplus_Hlt2BHadB02PpPpPmPmDecision_TOS;   //!
   TBranch        *b_piplus1_Dplus_Hlt2Topo2BodyDecision_Dec;   //!
   TBranch        *b_piplus1_Dplus_Hlt2Topo2BodyDecision_TIS;   //!
   TBranch        *b_piplus1_Dplus_Hlt2Topo2BodyDecision_TOS;   //!
   TBranch        *b_piplus1_Dplus_Hlt2Topo3BodyDecision_Dec;   //!
   TBranch        *b_piplus1_Dplus_Hlt2Topo3BodyDecision_TIS;   //!
   TBranch        *b_piplus1_Dplus_Hlt2Topo3BodyDecision_TOS;   //!
   TBranch        *b_piplus1_Dplus_Hlt2Topo4BodyDecision_Dec;   //!
   TBranch        *b_piplus1_Dplus_Hlt2Topo4BodyDecision_TIS;   //!
   TBranch        *b_piplus1_Dplus_Hlt2Topo4BodyDecision_TOS;   //!
   TBranch        *b_piplus1_Dplus_Hlt2GlobalDecision_Dec;   //!
   TBranch        *b_piplus1_Dplus_Hlt2GlobalDecision_TIS;   //!
   TBranch        *b_piplus1_Dplus_Hlt2GlobalDecision_TOS;   //!
   TBranch        *b_piplus1_Dplus_TRACK_Type;   //!
   TBranch        *b_piplus1_Dplus_TRACK_Key;   //!
   TBranch        *b_piplus1_Dplus_TRACK_CHI2NDOF;   //!
   TBranch        *b_piplus1_Dplus_TRACK_PCHI2;   //!
   TBranch        *b_piplus1_Dplus_TRACK_MatchCHI2;   //!
   TBranch        *b_piplus1_Dplus_TRACK_GhostProb;   //!
   TBranch        *b_piplus1_Dplus_TRACK_CloneDist;   //!
   TBranch        *b_piplus1_Dplus_TRACK_Likelihood;   //!
   TBranch        *b_piplus1_Dplus_X;   //!
   TBranch        *b_piplus1_Dplus_Y;   //!
   TBranch        *b_piplus2_Dplus_ETA;   //!
   TBranch        *b_piplus2_Dplus_MinIPCHI2;   //!
   TBranch        *b_piplus2_Dplus_MC12TuneV2_ProbNNe;   //!
   TBranch        *b_piplus2_Dplus_MC12TuneV2_ProbNNmu;   //!
   TBranch        *b_piplus2_Dplus_MC12TuneV2_ProbNNpi;   //!
   TBranch        *b_piplus2_Dplus_MC12TuneV2_ProbNNk;   //!
   TBranch        *b_piplus2_Dplus_MC12TuneV2_ProbNNp;   //!
   TBranch        *b_piplus2_Dplus_MC12TuneV2_ProbNNghost;   //!
   TBranch        *b_piplus2_Dplus_MC12TuneV3_ProbNNe;   //!
   TBranch        *b_piplus2_Dplus_MC12TuneV3_ProbNNmu;   //!
   TBranch        *b_piplus2_Dplus_MC12TuneV3_ProbNNpi;   //!
   TBranch        *b_piplus2_Dplus_MC12TuneV3_ProbNNk;   //!
   TBranch        *b_piplus2_Dplus_MC12TuneV3_ProbNNp;   //!
   TBranch        *b_piplus2_Dplus_MC12TuneV3_ProbNNghost;   //!
   TBranch        *b_piplus2_Dplus_MC12TuneV4_ProbNNe;   //!
   TBranch        *b_piplus2_Dplus_MC12TuneV4_ProbNNmu;   //!
   TBranch        *b_piplus2_Dplus_MC12TuneV4_ProbNNpi;   //!
   TBranch        *b_piplus2_Dplus_MC12TuneV4_ProbNNk;   //!
   TBranch        *b_piplus2_Dplus_MC12TuneV4_ProbNNp;   //!
   TBranch        *b_piplus2_Dplus_MC12TuneV4_ProbNNghost;   //!
   TBranch        *b_piplus2_Dplus_MC15TuneV1_ProbNNe;   //!
   TBranch        *b_piplus2_Dplus_MC15TuneV1_ProbNNmu;   //!
   TBranch        *b_piplus2_Dplus_MC15TuneV1_ProbNNpi;   //!
   TBranch        *b_piplus2_Dplus_MC15TuneV1_ProbNNk;   //!
   TBranch        *b_piplus2_Dplus_MC15TuneV1_ProbNNp;   //!
   TBranch        *b_piplus2_Dplus_MC15TuneV1_ProbNNghost;   //!
   TBranch        *b_piplus2_Dplus_CosTheta;   //!
   TBranch        *b_piplus2_Dplus_OWNPV_X;   //!
   TBranch        *b_piplus2_Dplus_OWNPV_Y;   //!
   TBranch        *b_piplus2_Dplus_OWNPV_Z;   //!
   TBranch        *b_piplus2_Dplus_OWNPV_XERR;   //!
   TBranch        *b_piplus2_Dplus_OWNPV_YERR;   //!
   TBranch        *b_piplus2_Dplus_OWNPV_ZERR;   //!
   TBranch        *b_piplus2_Dplus_OWNPV_CHI2;   //!
   TBranch        *b_piplus2_Dplus_OWNPV_NDOF;   //!
   TBranch        *b_piplus2_Dplus_OWNPV_COV_;   //!
   TBranch        *b_piplus2_Dplus_IP_OWNPV;   //!
   TBranch        *b_piplus2_Dplus_IPCHI2_OWNPV;   //!
   TBranch        *b_piplus2_Dplus_ORIVX_X;   //!
   TBranch        *b_piplus2_Dplus_ORIVX_Y;   //!
   TBranch        *b_piplus2_Dplus_ORIVX_Z;   //!
   TBranch        *b_piplus2_Dplus_ORIVX_XERR;   //!
   TBranch        *b_piplus2_Dplus_ORIVX_YERR;   //!
   TBranch        *b_piplus2_Dplus_ORIVX_ZERR;   //!
   TBranch        *b_piplus2_Dplus_ORIVX_CHI2;   //!
   TBranch        *b_piplus2_Dplus_ORIVX_NDOF;   //!
   TBranch        *b_piplus2_Dplus_ORIVX_COV_;   //!
   TBranch        *b_piplus2_Dplus_P;   //!
   TBranch        *b_piplus2_Dplus_PT;   //!
   TBranch        *b_piplus2_Dplus_PE;   //!
   TBranch        *b_piplus2_Dplus_PX;   //!
   TBranch        *b_piplus2_Dplus_PY;   //!
   TBranch        *b_piplus2_Dplus_PZ;   //!
   TBranch        *b_piplus2_Dplus_REFPX;   //!
   TBranch        *b_piplus2_Dplus_REFPY;   //!
   TBranch        *b_piplus2_Dplus_REFPZ;   //!
   TBranch        *b_piplus2_Dplus_M;   //!
   TBranch        *b_piplus2_Dplus_AtVtx_PE;   //!
   TBranch        *b_piplus2_Dplus_AtVtx_PX;   //!
   TBranch        *b_piplus2_Dplus_AtVtx_PY;   //!
   TBranch        *b_piplus2_Dplus_AtVtx_PZ;   //!
   TBranch        *b_piplus2_Dplus_TRUEID;   //!
   TBranch        *b_piplus2_Dplus_TRUEP_E;   //!
   TBranch        *b_piplus2_Dplus_TRUEP_X;   //!
   TBranch        *b_piplus2_Dplus_TRUEP_Y;   //!
   TBranch        *b_piplus2_Dplus_TRUEP_Z;   //!
   TBranch        *b_piplus2_Dplus_TRUEPT;   //!
   TBranch        *b_piplus2_Dplus_TRUEORIGINVERTEX_X;   //!
   TBranch        *b_piplus2_Dplus_TRUEORIGINVERTEX_Y;   //!
   TBranch        *b_piplus2_Dplus_TRUEORIGINVERTEX_Z;   //!
   TBranch        *b_piplus2_Dplus_TRUEENDVERTEX_X;   //!
   TBranch        *b_piplus2_Dplus_TRUEENDVERTEX_Y;   //!
   TBranch        *b_piplus2_Dplus_TRUEENDVERTEX_Z;   //!
   TBranch        *b_piplus2_Dplus_TRUEISSTABLE;   //!
   TBranch        *b_piplus2_Dplus_TRUETAU;   //!
   TBranch        *b_piplus2_Dplus_ID;   //!
   TBranch        *b_piplus2_Dplus_CombDLLMu;   //!
   TBranch        *b_piplus2_Dplus_ProbNNmu;   //!
   TBranch        *b_piplus2_Dplus_ProbNNghost;   //!
   TBranch        *b_piplus2_Dplus_InMuonAcc;   //!
   TBranch        *b_piplus2_Dplus_MuonDist2;   //!
   TBranch        *b_piplus2_Dplus_regionInM2;   //!
   TBranch        *b_piplus2_Dplus_hasMuon;   //!
   TBranch        *b_piplus2_Dplus_isMuon;   //!
   TBranch        *b_piplus2_Dplus_isMuonLoose;   //!
   TBranch        *b_piplus2_Dplus_NShared;   //!
   TBranch        *b_piplus2_Dplus_MuonLLmu;   //!
   TBranch        *b_piplus2_Dplus_MuonLLbg;   //!
   TBranch        *b_piplus2_Dplus_isMuonFromProto;   //!
   TBranch        *b_piplus2_Dplus_PIDe;   //!
   TBranch        *b_piplus2_Dplus_PIDmu;   //!
   TBranch        *b_piplus2_Dplus_PIDK;   //!
   TBranch        *b_piplus2_Dplus_PIDp;   //!
   TBranch        *b_piplus2_Dplus_PIDd;   //!
   TBranch        *b_piplus2_Dplus_ProbNNe;   //!
   TBranch        *b_piplus2_Dplus_ProbNNk;   //!
   TBranch        *b_piplus2_Dplus_ProbNNp;   //!
   TBranch        *b_piplus2_Dplus_ProbNNpi;   //!
   TBranch        *b_piplus2_Dplus_ProbNNd;   //!
   TBranch        *b_piplus2_Dplus_hasRich;   //!
   TBranch        *b_piplus2_Dplus_UsedRichAerogel;   //!
   TBranch        *b_piplus2_Dplus_UsedRich1Gas;   //!
   TBranch        *b_piplus2_Dplus_UsedRich2Gas;   //!
   TBranch        *b_piplus2_Dplus_RichAboveElThres;   //!
   TBranch        *b_piplus2_Dplus_RichAboveMuThres;   //!
   TBranch        *b_piplus2_Dplus_RichAbovePiThres;   //!
   TBranch        *b_piplus2_Dplus_RichAboveKaThres;   //!
   TBranch        *b_piplus2_Dplus_RichAbovePrThres;   //!
   TBranch        *b_piplus2_Dplus_hasCalo;   //!
   TBranch        *b_piplus2_Dplus_L0Global_Dec;   //!
   TBranch        *b_piplus2_Dplus_L0Global_TIS;   //!
   TBranch        *b_piplus2_Dplus_L0Global_TOS;   //!
   TBranch        *b_piplus2_Dplus_Hlt1Global_Dec;   //!
   TBranch        *b_piplus2_Dplus_Hlt1Global_TIS;   //!
   TBranch        *b_piplus2_Dplus_Hlt1Global_TOS;   //!
   TBranch        *b_piplus2_Dplus_Hlt1Phys_Dec;   //!
   TBranch        *b_piplus2_Dplus_Hlt1Phys_TIS;   //!
   TBranch        *b_piplus2_Dplus_Hlt1Phys_TOS;   //!
   TBranch        *b_piplus2_Dplus_Hlt2Global_Dec;   //!
   TBranch        *b_piplus2_Dplus_Hlt2Global_TIS;   //!
   TBranch        *b_piplus2_Dplus_Hlt2Global_TOS;   //!
   TBranch        *b_piplus2_Dplus_Hlt2Phys_Dec;   //!
   TBranch        *b_piplus2_Dplus_Hlt2Phys_TIS;   //!
   TBranch        *b_piplus2_Dplus_Hlt2Phys_TOS;   //!
   TBranch        *b_piplus2_Dplus_L0HadronDecision_Dec;   //!
   TBranch        *b_piplus2_Dplus_L0HadronDecision_TIS;   //!
   TBranch        *b_piplus2_Dplus_L0HadronDecision_TOS;   //!
   TBranch        *b_piplus2_Dplus_L0ElectronDecision_Dec;   //!
   TBranch        *b_piplus2_Dplus_L0ElectronDecision_TIS;   //!
   TBranch        *b_piplus2_Dplus_L0ElectronDecision_TOS;   //!
   TBranch        *b_piplus2_Dplus_L0ElectronHiDecision_Dec;   //!
   TBranch        *b_piplus2_Dplus_L0ElectronHiDecision_TIS;   //!
   TBranch        *b_piplus2_Dplus_L0ElectronHiDecision_TOS;   //!
   TBranch        *b_piplus2_Dplus_L0MuonDecision_Dec;   //!
   TBranch        *b_piplus2_Dplus_L0MuonDecision_TIS;   //!
   TBranch        *b_piplus2_Dplus_L0MuonDecision_TOS;   //!
   TBranch        *b_piplus2_Dplus_L0DiMuonDecision_Dec;   //!
   TBranch        *b_piplus2_Dplus_L0DiMuonDecision_TIS;   //!
   TBranch        *b_piplus2_Dplus_L0DiMuonDecision_TOS;   //!
   TBranch        *b_piplus2_Dplus_L0MuonHighDecision_Dec;   //!
   TBranch        *b_piplus2_Dplus_L0MuonHighDecision_TIS;   //!
   TBranch        *b_piplus2_Dplus_L0MuonHighDecision_TOS;   //!
   TBranch        *b_piplus2_Dplus_Hlt1TrackMVADecision_Dec;   //!
   TBranch        *b_piplus2_Dplus_Hlt1TrackMVADecision_TIS;   //!
   TBranch        *b_piplus2_Dplus_Hlt1TrackMVADecision_TOS;   //!
   TBranch        *b_piplus2_Dplus_Hlt1TwoTrackMVADecision_Dec;   //!
   TBranch        *b_piplus2_Dplus_Hlt1TwoTrackMVADecision_TIS;   //!
   TBranch        *b_piplus2_Dplus_Hlt1TwoTrackMVADecision_TOS;   //!
   TBranch        *b_piplus2_Dplus_Hlt1GlobalDecision_Dec;   //!
   TBranch        *b_piplus2_Dplus_Hlt1GlobalDecision_TIS;   //!
   TBranch        *b_piplus2_Dplus_Hlt1GlobalDecision_TOS;   //!
   TBranch        *b_piplus2_Dplus_Hlt1TrackAllL0Decision_Dec;   //!
   TBranch        *b_piplus2_Dplus_Hlt1TrackAllL0Decision_TIS;   //!
   TBranch        *b_piplus2_Dplus_Hlt1TrackAllL0Decision_TOS;   //!
   TBranch        *b_piplus2_Dplus_Hlt2Topo2BodyBBDTDecision_Dec;   //!
   TBranch        *b_piplus2_Dplus_Hlt2Topo2BodyBBDTDecision_TIS;   //!
   TBranch        *b_piplus2_Dplus_Hlt2Topo2BodyBBDTDecision_TOS;   //!
   TBranch        *b_piplus2_Dplus_Hlt2Topo3BodyBBDTDecision_Dec;   //!
   TBranch        *b_piplus2_Dplus_Hlt2Topo3BodyBBDTDecision_TIS;   //!
   TBranch        *b_piplus2_Dplus_Hlt2Topo3BodyBBDTDecision_TOS;   //!
   TBranch        *b_piplus2_Dplus_Hlt2Topo4BodyBBDTDecision_Dec;   //!
   TBranch        *b_piplus2_Dplus_Hlt2Topo4BodyBBDTDecision_TIS;   //!
   TBranch        *b_piplus2_Dplus_Hlt2Topo4BodyBBDTDecision_TOS;   //!
   TBranch        *b_piplus2_Dplus_Hlt2BHadB02PpPpPmPmDecision_Dec;   //!
   TBranch        *b_piplus2_Dplus_Hlt2BHadB02PpPpPmPmDecision_TIS;   //!
   TBranch        *b_piplus2_Dplus_Hlt2BHadB02PpPpPmPmDecision_TOS;   //!
   TBranch        *b_piplus2_Dplus_Hlt2Topo2BodyDecision_Dec;   //!
   TBranch        *b_piplus2_Dplus_Hlt2Topo2BodyDecision_TIS;   //!
   TBranch        *b_piplus2_Dplus_Hlt2Topo2BodyDecision_TOS;   //!
   TBranch        *b_piplus2_Dplus_Hlt2Topo3BodyDecision_Dec;   //!
   TBranch        *b_piplus2_Dplus_Hlt2Topo3BodyDecision_TIS;   //!
   TBranch        *b_piplus2_Dplus_Hlt2Topo3BodyDecision_TOS;   //!
   TBranch        *b_piplus2_Dplus_Hlt2Topo4BodyDecision_Dec;   //!
   TBranch        *b_piplus2_Dplus_Hlt2Topo4BodyDecision_TIS;   //!
   TBranch        *b_piplus2_Dplus_Hlt2Topo4BodyDecision_TOS;   //!
   TBranch        *b_piplus2_Dplus_Hlt2GlobalDecision_Dec;   //!
   TBranch        *b_piplus2_Dplus_Hlt2GlobalDecision_TIS;   //!
   TBranch        *b_piplus2_Dplus_Hlt2GlobalDecision_TOS;   //!
   TBranch        *b_piplus2_Dplus_TRACK_Type;   //!
   TBranch        *b_piplus2_Dplus_TRACK_Key;   //!
   TBranch        *b_piplus2_Dplus_TRACK_CHI2NDOF;   //!
   TBranch        *b_piplus2_Dplus_TRACK_PCHI2;   //!
   TBranch        *b_piplus2_Dplus_TRACK_MatchCHI2;   //!
   TBranch        *b_piplus2_Dplus_TRACK_GhostProb;   //!
   TBranch        *b_piplus2_Dplus_TRACK_CloneDist;   //!
   TBranch        *b_piplus2_Dplus_TRACK_Likelihood;   //!
   TBranch        *b_piplus2_Dplus_X;   //!
   TBranch        *b_piplus2_Dplus_Y;   //!
   TBranch        *b_nCandidate;   //!
   TBranch        *b_totCandidates;   //!
   TBranch        *b_EventInSequence;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_BCID;   //!
   TBranch        *b_BCType;   //!
   TBranch        *b_OdinTCK;   //!
   TBranch        *b_L0DUTCK;   //!
   TBranch        *b_HLT1TCK;   //!
   TBranch        *b_HLT2TCK;   //!
   TBranch        *b_GpsTime;   //!
   TBranch        *b_Polarity;   //!
   TBranch        *b_L0Data_DiMuon_Pt;   //!
   TBranch        *b_L0Data_DiMuonProd_Pt1Pt2;   //!
   TBranch        *b_L0Data_Electron_Et;   //!
   TBranch        *b_L0Data_GlobalPi0_Et;   //!
   TBranch        *b_L0Data_Hadron_Et;   //!
   TBranch        *b_L0Data_LocalPi0_Et;   //!
   TBranch        *b_L0Data_Muon1_Pt;   //!
   TBranch        *b_L0Data_Muon1_Sgn;   //!
   TBranch        *b_L0Data_Muon2_Pt;   //!
   TBranch        *b_L0Data_Muon2_Sgn;   //!
   TBranch        *b_L0Data_Muon3_Pt;   //!
   TBranch        *b_L0Data_Muon3_Sgn;   //!
   TBranch        *b_L0Data_PUHits_Mult;   //!
   TBranch        *b_L0Data_PUPeak1_Cont;   //!
   TBranch        *b_L0Data_PUPeak1_Pos;   //!
   TBranch        *b_L0Data_PUPeak2_Cont;   //!
   TBranch        *b_L0Data_PUPeak2_Pos;   //!
   TBranch        *b_L0Data_Photon_Et;   //!
   TBranch        *b_L0Data_Spd_Mult;   //!
   TBranch        *b_L0Data_Sum_Et;   //!
   TBranch        *b_L0Data_Sum_Et_Next1;   //!
   TBranch        *b_L0Data_Sum_Et_Next2;   //!
   TBranch        *b_L0Data_Sum_Et_Prev1;   //!
   TBranch        *b_L0Data_Sum_Et_Prev2;   //!
   TBranch        *b_nPV;   //!
   TBranch        *b_PVX;   //!
   TBranch        *b_PVY;   //!
   TBranch        *b_PVZ;   //!
   TBranch        *b_PVXERR;   //!
   TBranch        *b_PVYERR;   //!
   TBranch        *b_PVZERR;   //!
   TBranch        *b_PVCHI2;   //!
   TBranch        *b_PVNDOF;   //!
   TBranch        *b_PVNTRACKS;   //!
   TBranch        *b_nPVs;   //!
   TBranch        *b_nTracks;   //!
   TBranch        *b_nLongTracks;   //!
   TBranch        *b_nDownstreamTracks;   //!
   TBranch        *b_nUpstreamTracks;   //!
   TBranch        *b_nVeloTracks;   //!
   TBranch        *b_nTTracks;   //!
   TBranch        *b_nBackTracks;   //!
   TBranch        *b_nRich1Hits;   //!
   TBranch        *b_nRich2Hits;   //!
   TBranch        *b_nVeloClusters;   //!
   TBranch        *b_nITClusters;   //!
   TBranch        *b_nTTClusters;   //!
   TBranch        *b_nOTClusters;   //!
   TBranch        *b_nSPDHits;   //!
   TBranch        *b_nMuonCoordsS0;   //!
   TBranch        *b_nMuonCoordsS1;   //!
   TBranch        *b_nMuonCoordsS2;   //!
   TBranch        *b_nMuonCoordsS3;   //!
   TBranch        *b_nMuonCoordsS4;   //!
   TBranch        *b_nMuonTracks;   //!
   TBranch        *b_year;   //!
   TBranch        *b_magFlag;   //!
   TBranch        *b_rdmNumber;   //!
   TBranch        *b_m_Dsminus_pi2K;   //!
   TBranch        *b_m_Dsminus_p2K;   //!
   TBranch        *b_m_KK_Dsminus;   //!
   TBranch        *b_m_Dplus_K2pi1;   //!
   TBranch        *b_m_Dplus_K2pi2;   //!
   TBranch        *b_m_Dplus_p2pi1;   //!
   TBranch        *b_m_Dplus_p2pi2;   //!
   TBranch        *b_m_KK_Dplus_K2pi1;   //!
   TBranch        *b_m_KK_Dplus_K2pi2;   //!
   TBranch        *b_Kplus_Dsminus_MC15TuneV1_ProbNNk_corr;   //!
   TBranch        *b_Kminus_Dsminus_MC15TuneV1_ProbNNk_corr;   //!
   TBranch        *b_piminus_Dsminus_MC15TuneV1_ProbNNpi_corr;   //!
   TBranch        *b_Kminus_Dplus_MC15TuneV1_ProbNNk_corr;   //!
   TBranch        *b_piplus1_Dplus_MC15TuneV1_ProbNNpi_corr;   //!
   TBranch        *b_piplus2_Dplus_MC15TuneV1_ProbNNpi_corr;   //!
   TBranch        *b_Kplus_Dsminus_PIDK_corr;   //!
   TBranch        *b_Kminus_Dsminus_PIDK_corr;   //!
   TBranch        *b_Kminus_Dplus_PIDK_corr;   //!
   TBranch        *b_angle_Kminus_Dplus_piplus1_Dplus;   //!
   TBranch        *b_angle_Kminus_Dplus_piplus2_Dplus;   //!
   TBranch        *b_angle_piplus2_Dplus_piplus1_Dplus;   //!
   TBranch        *b_angle_Kplus_Dsminus_Kminus_Dsminus;   //!
   TBranch        *b_angle_Kplus_Dsminus_piminus_Dsminus;   //!
   TBranch        *b_angle_piminus_Dsminus_Kminus_Dsminus;   //!
   TBranch        *b_angle_Kminus_Dplus_Kplus_Dsminus;   //!
   TBranch        *b_angle_Kminus_Dplus_Kminus_Dsminus;   //!
   TBranch        *b_angle_Kminus_Dplus_piminus_Dsminus;   //!
   TBranch        *b_angle_piplus1_Dplus_Kplus_Dsminus;   //!
   TBranch        *b_angle_piplus1_Dplus_Kminus_Dsminus;   //!
   TBranch        *b_angle_piplus1_Dplus_piminus_Dsminus;   //!
   TBranch        *b_angle_piplus2_Dplus_Kplus_Dsminus;   //!
   TBranch        *b_angle_piplus2_Dplus_Kminus_Dsminus;   //!
   TBranch        *b_angle_piplus2_Dplus_piminus_Dsminus;   //!
   TBranch        *b_prodProbNNx;   //!
   TBranch        *b_B_DTFM_M_0;   //!

   DecayTree(TTree *tree=0);
   virtual ~DecayTree();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef DecayTree_cxx
DecayTree::DecayTree(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("/mnt/d/lhcb/B2XcXc/root/Bd/data/Data_Bd2DsD_Run2_TriggerSel.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("/mnt/d/lhcb/B2XcXc/root/Bd/data/Data_Bd2DsD_Run2_TriggerSel.root");
      }
      f->GetObject("DecayTree",tree);

   }
   Init(tree);
}

DecayTree::~DecayTree()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t DecayTree::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t DecayTree::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void DecayTree::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("B_ETA", &B_ETA, &b_B_ETA);
   fChain->SetBranchAddress("B_MinIPCHI2", &B_MinIPCHI2, &b_B_MinIPCHI2);
   fChain->SetBranchAddress("B_DiraAngleError", &B_DiraAngleError, &b_B_DiraAngleError);
   fChain->SetBranchAddress("B_DiraCosError", &B_DiraCosError, &b_B_DiraCosError);
   fChain->SetBranchAddress("B_DiraAngle", &B_DiraAngle, &b_B_DiraAngle);
   fChain->SetBranchAddress("B_DiraCos", &B_DiraCos, &b_B_DiraCos);
   fChain->SetBranchAddress("B_ENDVERTEX_X", &B_ENDVERTEX_X, &b_B_ENDVERTEX_X);
   fChain->SetBranchAddress("B_ENDVERTEX_Y", &B_ENDVERTEX_Y, &b_B_ENDVERTEX_Y);
   fChain->SetBranchAddress("B_ENDVERTEX_Z", &B_ENDVERTEX_Z, &b_B_ENDVERTEX_Z);
   fChain->SetBranchAddress("B_ENDVERTEX_XERR", &B_ENDVERTEX_XERR, &b_B_ENDVERTEX_XERR);
   fChain->SetBranchAddress("B_ENDVERTEX_YERR", &B_ENDVERTEX_YERR, &b_B_ENDVERTEX_YERR);
   fChain->SetBranchAddress("B_ENDVERTEX_ZERR", &B_ENDVERTEX_ZERR, &b_B_ENDVERTEX_ZERR);
   fChain->SetBranchAddress("B_ENDVERTEX_CHI2", &B_ENDVERTEX_CHI2, &b_B_ENDVERTEX_CHI2);
   fChain->SetBranchAddress("B_ENDVERTEX_NDOF", &B_ENDVERTEX_NDOF, &b_B_ENDVERTEX_NDOF);
   fChain->SetBranchAddress("B_ENDVERTEX_COV_", B_ENDVERTEX_COV_, &b_B_ENDVERTEX_COV_);
   fChain->SetBranchAddress("B_OWNPV_X", &B_OWNPV_X, &b_B_OWNPV_X);
   fChain->SetBranchAddress("B_OWNPV_Y", &B_OWNPV_Y, &b_B_OWNPV_Y);
   fChain->SetBranchAddress("B_OWNPV_Z", &B_OWNPV_Z, &b_B_OWNPV_Z);
   fChain->SetBranchAddress("B_OWNPV_XERR", &B_OWNPV_XERR, &b_B_OWNPV_XERR);
   fChain->SetBranchAddress("B_OWNPV_YERR", &B_OWNPV_YERR, &b_B_OWNPV_YERR);
   fChain->SetBranchAddress("B_OWNPV_ZERR", &B_OWNPV_ZERR, &b_B_OWNPV_ZERR);
   fChain->SetBranchAddress("B_OWNPV_CHI2", &B_OWNPV_CHI2, &b_B_OWNPV_CHI2);
   fChain->SetBranchAddress("B_OWNPV_NDOF", &B_OWNPV_NDOF, &b_B_OWNPV_NDOF);
   fChain->SetBranchAddress("B_OWNPV_COV_", B_OWNPV_COV_, &b_B_OWNPV_COV_);
   fChain->SetBranchAddress("B_IP_OWNPV", &B_IP_OWNPV, &b_B_IP_OWNPV);
   fChain->SetBranchAddress("B_IPCHI2_OWNPV", &B_IPCHI2_OWNPV, &b_B_IPCHI2_OWNPV);
   fChain->SetBranchAddress("B_FD_OWNPV", &B_FD_OWNPV, &b_B_FD_OWNPV);
   fChain->SetBranchAddress("B_FDCHI2_OWNPV", &B_FDCHI2_OWNPV, &b_B_FDCHI2_OWNPV);
   fChain->SetBranchAddress("B_DIRA_OWNPV", &B_DIRA_OWNPV, &b_B_DIRA_OWNPV);
   fChain->SetBranchAddress("B_P", &B_P, &b_B_P);
   fChain->SetBranchAddress("B_PT", &B_PT, &b_B_PT);
   fChain->SetBranchAddress("B_PE", &B_PE, &b_B_PE);
   fChain->SetBranchAddress("B_PX", &B_PX, &b_B_PX);
   fChain->SetBranchAddress("B_PY", &B_PY, &b_B_PY);
   fChain->SetBranchAddress("B_PZ", &B_PZ, &b_B_PZ);
   fChain->SetBranchAddress("B_REFPX", &B_REFPX, &b_B_REFPX);
   fChain->SetBranchAddress("B_REFPY", &B_REFPY, &b_B_REFPY);
   fChain->SetBranchAddress("B_REFPZ", &B_REFPZ, &b_B_REFPZ);
   fChain->SetBranchAddress("B_MM", &B_MM, &b_B_MM);
   fChain->SetBranchAddress("B_MMERR", &B_MMERR, &b_B_MMERR);
   fChain->SetBranchAddress("B_M", &B_M, &b_B_M);
   fChain->SetBranchAddress("B_BKGCAT", &B_BKGCAT, &b_B_BKGCAT);
   fChain->SetBranchAddress("B_TRUEID", &B_TRUEID, &b_B_TRUEID);
   fChain->SetBranchAddress("B_TRUEP_E", &B_TRUEP_E, &b_B_TRUEP_E);
   fChain->SetBranchAddress("B_TRUEP_X", &B_TRUEP_X, &b_B_TRUEP_X);
   fChain->SetBranchAddress("B_TRUEP_Y", &B_TRUEP_Y, &b_B_TRUEP_Y);
   fChain->SetBranchAddress("B_TRUEP_Z", &B_TRUEP_Z, &b_B_TRUEP_Z);
   fChain->SetBranchAddress("B_TRUEPT", &B_TRUEPT, &b_B_TRUEPT);
   fChain->SetBranchAddress("B_TRUEORIGINVERTEX_X", &B_TRUEORIGINVERTEX_X, &b_B_TRUEORIGINVERTEX_X);
   fChain->SetBranchAddress("B_TRUEORIGINVERTEX_Y", &B_TRUEORIGINVERTEX_Y, &b_B_TRUEORIGINVERTEX_Y);
   fChain->SetBranchAddress("B_TRUEORIGINVERTEX_Z", &B_TRUEORIGINVERTEX_Z, &b_B_TRUEORIGINVERTEX_Z);
   fChain->SetBranchAddress("B_TRUEENDVERTEX_X", &B_TRUEENDVERTEX_X, &b_B_TRUEENDVERTEX_X);
   fChain->SetBranchAddress("B_TRUEENDVERTEX_Y", &B_TRUEENDVERTEX_Y, &b_B_TRUEENDVERTEX_Y);
   fChain->SetBranchAddress("B_TRUEENDVERTEX_Z", &B_TRUEENDVERTEX_Z, &b_B_TRUEENDVERTEX_Z);
   fChain->SetBranchAddress("B_TRUEISSTABLE", &B_TRUEISSTABLE, &b_B_TRUEISSTABLE);
   fChain->SetBranchAddress("B_TRUETAU", &B_TRUETAU, &b_B_TRUETAU);
   fChain->SetBranchAddress("B_ID", &B_ID, &b_B_ID);
   fChain->SetBranchAddress("B_TAU", &B_TAU, &b_B_TAU);
   fChain->SetBranchAddress("B_TAUERR", &B_TAUERR, &b_B_TAUERR);
   fChain->SetBranchAddress("B_TAUCHI2", &B_TAUCHI2, &b_B_TAUCHI2);
   fChain->SetBranchAddress("B_L0Global_Dec", &B_L0Global_Dec, &b_B_L0Global_Dec);
   fChain->SetBranchAddress("B_L0Global_TIS", &B_L0Global_TIS, &b_B_L0Global_TIS);
   fChain->SetBranchAddress("B_L0Global_TOS", &B_L0Global_TOS, &b_B_L0Global_TOS);
   fChain->SetBranchAddress("B_Hlt1Global_Dec", &B_Hlt1Global_Dec, &b_B_Hlt1Global_Dec);
   fChain->SetBranchAddress("B_Hlt1Global_TIS", &B_Hlt1Global_TIS, &b_B_Hlt1Global_TIS);
   fChain->SetBranchAddress("B_Hlt1Global_TOS", &B_Hlt1Global_TOS, &b_B_Hlt1Global_TOS);
   fChain->SetBranchAddress("B_Hlt1Phys_Dec", &B_Hlt1Phys_Dec, &b_B_Hlt1Phys_Dec);
   fChain->SetBranchAddress("B_Hlt1Phys_TIS", &B_Hlt1Phys_TIS, &b_B_Hlt1Phys_TIS);
   fChain->SetBranchAddress("B_Hlt1Phys_TOS", &B_Hlt1Phys_TOS, &b_B_Hlt1Phys_TOS);
   fChain->SetBranchAddress("B_Hlt2Global_Dec", &B_Hlt2Global_Dec, &b_B_Hlt2Global_Dec);
   fChain->SetBranchAddress("B_Hlt2Global_TIS", &B_Hlt2Global_TIS, &b_B_Hlt2Global_TIS);
   fChain->SetBranchAddress("B_Hlt2Global_TOS", &B_Hlt2Global_TOS, &b_B_Hlt2Global_TOS);
   fChain->SetBranchAddress("B_Hlt2Phys_Dec", &B_Hlt2Phys_Dec, &b_B_Hlt2Phys_Dec);
   fChain->SetBranchAddress("B_Hlt2Phys_TIS", &B_Hlt2Phys_TIS, &b_B_Hlt2Phys_TIS);
   fChain->SetBranchAddress("B_Hlt2Phys_TOS", &B_Hlt2Phys_TOS, &b_B_Hlt2Phys_TOS);
   fChain->SetBranchAddress("B_L0HadronDecision_Dec", &B_L0HadronDecision_Dec, &b_B_L0HadronDecision_Dec);
   fChain->SetBranchAddress("B_L0HadronDecision_TIS", &B_L0HadronDecision_TIS, &b_B_L0HadronDecision_TIS);
   fChain->SetBranchAddress("B_L0HadronDecision_TOS", &B_L0HadronDecision_TOS, &b_B_L0HadronDecision_TOS);
   fChain->SetBranchAddress("B_L0ElectronDecision_Dec", &B_L0ElectronDecision_Dec, &b_B_L0ElectronDecision_Dec);
   fChain->SetBranchAddress("B_L0ElectronDecision_TIS", &B_L0ElectronDecision_TIS, &b_B_L0ElectronDecision_TIS);
   fChain->SetBranchAddress("B_L0ElectronDecision_TOS", &B_L0ElectronDecision_TOS, &b_B_L0ElectronDecision_TOS);
   fChain->SetBranchAddress("B_L0ElectronHiDecision_Dec", &B_L0ElectronHiDecision_Dec, &b_B_L0ElectronHiDecision_Dec);
   fChain->SetBranchAddress("B_L0ElectronHiDecision_TIS", &B_L0ElectronHiDecision_TIS, &b_B_L0ElectronHiDecision_TIS);
   fChain->SetBranchAddress("B_L0ElectronHiDecision_TOS", &B_L0ElectronHiDecision_TOS, &b_B_L0ElectronHiDecision_TOS);
   fChain->SetBranchAddress("B_L0MuonDecision_Dec", &B_L0MuonDecision_Dec, &b_B_L0MuonDecision_Dec);
   fChain->SetBranchAddress("B_L0MuonDecision_TIS", &B_L0MuonDecision_TIS, &b_B_L0MuonDecision_TIS);
   fChain->SetBranchAddress("B_L0MuonDecision_TOS", &B_L0MuonDecision_TOS, &b_B_L0MuonDecision_TOS);
   fChain->SetBranchAddress("B_L0DiMuonDecision_Dec", &B_L0DiMuonDecision_Dec, &b_B_L0DiMuonDecision_Dec);
   fChain->SetBranchAddress("B_L0DiMuonDecision_TIS", &B_L0DiMuonDecision_TIS, &b_B_L0DiMuonDecision_TIS);
   fChain->SetBranchAddress("B_L0DiMuonDecision_TOS", &B_L0DiMuonDecision_TOS, &b_B_L0DiMuonDecision_TOS);
   fChain->SetBranchAddress("B_L0MuonHighDecision_Dec", &B_L0MuonHighDecision_Dec, &b_B_L0MuonHighDecision_Dec);
   fChain->SetBranchAddress("B_L0MuonHighDecision_TIS", &B_L0MuonHighDecision_TIS, &b_B_L0MuonHighDecision_TIS);
   fChain->SetBranchAddress("B_L0MuonHighDecision_TOS", &B_L0MuonHighDecision_TOS, &b_B_L0MuonHighDecision_TOS);
   fChain->SetBranchAddress("B_Hlt1TrackMVADecision_Dec", &B_Hlt1TrackMVADecision_Dec, &b_B_Hlt1TrackMVADecision_Dec);
   fChain->SetBranchAddress("B_Hlt1TrackMVADecision_TIS", &B_Hlt1TrackMVADecision_TIS, &b_B_Hlt1TrackMVADecision_TIS);
   fChain->SetBranchAddress("B_Hlt1TrackMVADecision_TOS", &B_Hlt1TrackMVADecision_TOS, &b_B_Hlt1TrackMVADecision_TOS);
   fChain->SetBranchAddress("B_Hlt1TwoTrackMVADecision_Dec", &B_Hlt1TwoTrackMVADecision_Dec, &b_B_Hlt1TwoTrackMVADecision_Dec);
   fChain->SetBranchAddress("B_Hlt1TwoTrackMVADecision_TIS", &B_Hlt1TwoTrackMVADecision_TIS, &b_B_Hlt1TwoTrackMVADecision_TIS);
   fChain->SetBranchAddress("B_Hlt1TwoTrackMVADecision_TOS", &B_Hlt1TwoTrackMVADecision_TOS, &b_B_Hlt1TwoTrackMVADecision_TOS);
   fChain->SetBranchAddress("B_Hlt1GlobalDecision_Dec", &B_Hlt1GlobalDecision_Dec, &b_B_Hlt1GlobalDecision_Dec);
   fChain->SetBranchAddress("B_Hlt1GlobalDecision_TIS", &B_Hlt1GlobalDecision_TIS, &b_B_Hlt1GlobalDecision_TIS);
   fChain->SetBranchAddress("B_Hlt1GlobalDecision_TOS", &B_Hlt1GlobalDecision_TOS, &b_B_Hlt1GlobalDecision_TOS);
   fChain->SetBranchAddress("B_Hlt1TrackAllL0Decision_Dec", &B_Hlt1TrackAllL0Decision_Dec, &b_B_Hlt1TrackAllL0Decision_Dec);
   fChain->SetBranchAddress("B_Hlt1TrackAllL0Decision_TIS", &B_Hlt1TrackAllL0Decision_TIS, &b_B_Hlt1TrackAllL0Decision_TIS);
   fChain->SetBranchAddress("B_Hlt1TrackAllL0Decision_TOS", &B_Hlt1TrackAllL0Decision_TOS, &b_B_Hlt1TrackAllL0Decision_TOS);
   fChain->SetBranchAddress("B_Hlt2Topo2BodyBBDTDecision_Dec", &B_Hlt2Topo2BodyBBDTDecision_Dec, &b_B_Hlt2Topo2BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("B_Hlt2Topo2BodyBBDTDecision_TIS", &B_Hlt2Topo2BodyBBDTDecision_TIS, &b_B_Hlt2Topo2BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("B_Hlt2Topo2BodyBBDTDecision_TOS", &B_Hlt2Topo2BodyBBDTDecision_TOS, &b_B_Hlt2Topo2BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("B_Hlt2Topo3BodyBBDTDecision_Dec", &B_Hlt2Topo3BodyBBDTDecision_Dec, &b_B_Hlt2Topo3BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("B_Hlt2Topo3BodyBBDTDecision_TIS", &B_Hlt2Topo3BodyBBDTDecision_TIS, &b_B_Hlt2Topo3BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("B_Hlt2Topo3BodyBBDTDecision_TOS", &B_Hlt2Topo3BodyBBDTDecision_TOS, &b_B_Hlt2Topo3BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("B_Hlt2Topo4BodyBBDTDecision_Dec", &B_Hlt2Topo4BodyBBDTDecision_Dec, &b_B_Hlt2Topo4BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("B_Hlt2Topo4BodyBBDTDecision_TIS", &B_Hlt2Topo4BodyBBDTDecision_TIS, &b_B_Hlt2Topo4BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("B_Hlt2Topo4BodyBBDTDecision_TOS", &B_Hlt2Topo4BodyBBDTDecision_TOS, &b_B_Hlt2Topo4BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("B_Hlt2BHadB02PpPpPmPmDecision_Dec", &B_Hlt2BHadB02PpPpPmPmDecision_Dec, &b_B_Hlt2BHadB02PpPpPmPmDecision_Dec);
   fChain->SetBranchAddress("B_Hlt2BHadB02PpPpPmPmDecision_TIS", &B_Hlt2BHadB02PpPpPmPmDecision_TIS, &b_B_Hlt2BHadB02PpPpPmPmDecision_TIS);
   fChain->SetBranchAddress("B_Hlt2BHadB02PpPpPmPmDecision_TOS", &B_Hlt2BHadB02PpPpPmPmDecision_TOS, &b_B_Hlt2BHadB02PpPpPmPmDecision_TOS);
   fChain->SetBranchAddress("B_Hlt2Topo2BodyDecision_Dec", &B_Hlt2Topo2BodyDecision_Dec, &b_B_Hlt2Topo2BodyDecision_Dec);
   fChain->SetBranchAddress("B_Hlt2Topo2BodyDecision_TIS", &B_Hlt2Topo2BodyDecision_TIS, &b_B_Hlt2Topo2BodyDecision_TIS);
   fChain->SetBranchAddress("B_Hlt2Topo2BodyDecision_TOS", &B_Hlt2Topo2BodyDecision_TOS, &b_B_Hlt2Topo2BodyDecision_TOS);
   fChain->SetBranchAddress("B_Hlt2Topo3BodyDecision_Dec", &B_Hlt2Topo3BodyDecision_Dec, &b_B_Hlt2Topo3BodyDecision_Dec);
   fChain->SetBranchAddress("B_Hlt2Topo3BodyDecision_TIS", &B_Hlt2Topo3BodyDecision_TIS, &b_B_Hlt2Topo3BodyDecision_TIS);
   fChain->SetBranchAddress("B_Hlt2Topo3BodyDecision_TOS", &B_Hlt2Topo3BodyDecision_TOS, &b_B_Hlt2Topo3BodyDecision_TOS);
   fChain->SetBranchAddress("B_Hlt2Topo4BodyDecision_Dec", &B_Hlt2Topo4BodyDecision_Dec, &b_B_Hlt2Topo4BodyDecision_Dec);
   fChain->SetBranchAddress("B_Hlt2Topo4BodyDecision_TIS", &B_Hlt2Topo4BodyDecision_TIS, &b_B_Hlt2Topo4BodyDecision_TIS);
   fChain->SetBranchAddress("B_Hlt2Topo4BodyDecision_TOS", &B_Hlt2Topo4BodyDecision_TOS, &b_B_Hlt2Topo4BodyDecision_TOS);
   fChain->SetBranchAddress("B_Hlt2GlobalDecision_Dec", &B_Hlt2GlobalDecision_Dec, &b_B_Hlt2GlobalDecision_Dec);
   fChain->SetBranchAddress("B_Hlt2GlobalDecision_TIS", &B_Hlt2GlobalDecision_TIS, &b_B_Hlt2GlobalDecision_TIS);
   fChain->SetBranchAddress("B_Hlt2GlobalDecision_TOS", &B_Hlt2GlobalDecision_TOS, &b_B_Hlt2GlobalDecision_TOS);
   fChain->SetBranchAddress("B_X", &B_X, &b_B_X);
   fChain->SetBranchAddress("B_Y", &B_Y, &b_B_Y);
   fChain->SetBranchAddress("B_DTFDict_B_DIRA_OWNPV", &B_DTFDict_B_DIRA_OWNPV, &b_B_DTFDict_B_DIRA_OWNPV);
   fChain->SetBranchAddress("B_DTFDict_B_E", &B_DTFDict_B_E, &b_B_DTFDict_B_E);
   fChain->SetBranchAddress("B_DTFDict_B_ENDVERTEX_CHI2", &B_DTFDict_B_ENDVERTEX_CHI2, &b_B_DTFDict_B_ENDVERTEX_CHI2);
   fChain->SetBranchAddress("B_DTFDict_B_ENDVERTEX_NDOF", &B_DTFDict_B_ENDVERTEX_NDOF, &b_B_DTFDict_B_ENDVERTEX_NDOF);
   fChain->SetBranchAddress("B_DTFDict_B_FDCHI2_OWNPV", &B_DTFDict_B_FDCHI2_OWNPV, &b_B_DTFDict_B_FDCHI2_OWNPV);
   fChain->SetBranchAddress("B_DTFDict_B_M", &B_DTFDict_B_M, &b_B_DTFDict_B_M);
   fChain->SetBranchAddress("B_DTFDict_B_MINIPCHI2", &B_DTFDict_B_MINIPCHI2, &b_B_DTFDict_B_MINIPCHI2);
   fChain->SetBranchAddress("B_DTFDict_B_PT", &B_DTFDict_B_PT, &b_B_DTFDict_B_PT);
   fChain->SetBranchAddress("B_DTFDict_B_PX", &B_DTFDict_B_PX, &b_B_DTFDict_B_PX);
   fChain->SetBranchAddress("B_DTFDict_B_PY", &B_DTFDict_B_PY, &b_B_DTFDict_B_PY);
   fChain->SetBranchAddress("B_DTFDict_B_PZ", &B_DTFDict_B_PZ, &b_B_DTFDict_B_PZ);
   fChain->SetBranchAddress("B_DTFDict_Dplus_DIRA_OWNPV", &B_DTFDict_Dplus_DIRA_OWNPV, &b_B_DTFDict_Dplus_DIRA_OWNPV);
   fChain->SetBranchAddress("B_DTFDict_Dplus_E", &B_DTFDict_Dplus_E, &b_B_DTFDict_Dplus_E);
   fChain->SetBranchAddress("B_DTFDict_Dplus_ENDVERTEX_CHI2", &B_DTFDict_Dplus_ENDVERTEX_CHI2, &b_B_DTFDict_Dplus_ENDVERTEX_CHI2);
   fChain->SetBranchAddress("B_DTFDict_Dplus_ENDVERTEX_NDOF", &B_DTFDict_Dplus_ENDVERTEX_NDOF, &b_B_DTFDict_Dplus_ENDVERTEX_NDOF);
   fChain->SetBranchAddress("B_DTFDict_Dplus_FDCHI2_OWNPV", &B_DTFDict_Dplus_FDCHI2_OWNPV, &b_B_DTFDict_Dplus_FDCHI2_OWNPV);
   fChain->SetBranchAddress("B_DTFDict_Dplus_M", &B_DTFDict_Dplus_M, &b_B_DTFDict_Dplus_M);
   fChain->SetBranchAddress("B_DTFDict_Dplus_MINIPCHI2", &B_DTFDict_Dplus_MINIPCHI2, &b_B_DTFDict_Dplus_MINIPCHI2);
   fChain->SetBranchAddress("B_DTFDict_Dplus_MM", &B_DTFDict_Dplus_MM, &b_B_DTFDict_Dplus_MM);
   fChain->SetBranchAddress("B_DTFDict_Dplus_PT", &B_DTFDict_Dplus_PT, &b_B_DTFDict_Dplus_PT);
   fChain->SetBranchAddress("B_DTFDict_Dplus_PX", &B_DTFDict_Dplus_PX, &b_B_DTFDict_Dplus_PX);
   fChain->SetBranchAddress("B_DTFDict_Dplus_PY", &B_DTFDict_Dplus_PY, &b_B_DTFDict_Dplus_PY);
   fChain->SetBranchAddress("B_DTFDict_Dplus_PZ", &B_DTFDict_Dplus_PZ, &b_B_DTFDict_Dplus_PZ);
   fChain->SetBranchAddress("B_DTFDict_Dsminus_DIRA_OWNPV", &B_DTFDict_Dsminus_DIRA_OWNPV, &b_B_DTFDict_Dsminus_DIRA_OWNPV);
   fChain->SetBranchAddress("B_DTFDict_Dsminus_E", &B_DTFDict_Dsminus_E, &b_B_DTFDict_Dsminus_E);
   fChain->SetBranchAddress("B_DTFDict_Dsminus_ENDVERTEX_CHI2", &B_DTFDict_Dsminus_ENDVERTEX_CHI2, &b_B_DTFDict_Dsminus_ENDVERTEX_CHI2);
   fChain->SetBranchAddress("B_DTFDict_Dsminus_ENDVERTEX_NDOF", &B_DTFDict_Dsminus_ENDVERTEX_NDOF, &b_B_DTFDict_Dsminus_ENDVERTEX_NDOF);
   fChain->SetBranchAddress("B_DTFDict_Dsminus_FDCHI2_OWNPV", &B_DTFDict_Dsminus_FDCHI2_OWNPV, &b_B_DTFDict_Dsminus_FDCHI2_OWNPV);
   fChain->SetBranchAddress("B_DTFDict_Dsminus_M", &B_DTFDict_Dsminus_M, &b_B_DTFDict_Dsminus_M);
   fChain->SetBranchAddress("B_DTFDict_Dsminus_MINIPCHI2", &B_DTFDict_Dsminus_MINIPCHI2, &b_B_DTFDict_Dsminus_MINIPCHI2);
   fChain->SetBranchAddress("B_DTFDict_Dsminus_MM", &B_DTFDict_Dsminus_MM, &b_B_DTFDict_Dsminus_MM);
   fChain->SetBranchAddress("B_DTFDict_Dsminus_PT", &B_DTFDict_Dsminus_PT, &b_B_DTFDict_Dsminus_PT);
   fChain->SetBranchAddress("B_DTFDict_Dsminus_PX", &B_DTFDict_Dsminus_PX, &b_B_DTFDict_Dsminus_PX);
   fChain->SetBranchAddress("B_DTFDict_Dsminus_PY", &B_DTFDict_Dsminus_PY, &b_B_DTFDict_Dsminus_PY);
   fChain->SetBranchAddress("B_DTFDict_Dsminus_PZ", &B_DTFDict_Dsminus_PZ, &b_B_DTFDict_Dsminus_PZ);
   fChain->SetBranchAddress("B_DTF_CHI2", &B_DTF_CHI2, &b_B_DTF_CHI2);
   fChain->SetBranchAddress("B_DTF_NDOF", &B_DTF_NDOF, &b_B_DTF_NDOF);
   fChain->SetBranchAddress("B_DTF_M_nPV", &B_DTF_M_nPV, &b_B_DTF_M_nPV);
   fChain->SetBranchAddress("B_DTF_M_D_splus_Kplus_0_ID", B_DTF_M_D_splus_Kplus_0_ID, &b_B_DTF_M_D_splus_Kplus_0_ID);
   fChain->SetBranchAddress("B_DTF_M_D_splus_Kplus_0_PE", B_DTF_M_D_splus_Kplus_0_PE, &b_B_DTF_M_D_splus_Kplus_0_PE);
   fChain->SetBranchAddress("B_DTF_M_D_splus_Kplus_0_PX", B_DTF_M_D_splus_Kplus_0_PX, &b_B_DTF_M_D_splus_Kplus_0_PX);
   fChain->SetBranchAddress("B_DTF_M_D_splus_Kplus_0_PY", B_DTF_M_D_splus_Kplus_0_PY, &b_B_DTF_M_D_splus_Kplus_0_PY);
   fChain->SetBranchAddress("B_DTF_M_D_splus_Kplus_0_PZ", B_DTF_M_D_splus_Kplus_0_PZ, &b_B_DTF_M_D_splus_Kplus_0_PZ);
   fChain->SetBranchAddress("B_DTF_M_D_splus_Kplus_ID", B_DTF_M_D_splus_Kplus_ID, &b_B_DTF_M_D_splus_Kplus_ID);
   fChain->SetBranchAddress("B_DTF_M_D_splus_Kplus_PE", B_DTF_M_D_splus_Kplus_PE, &b_B_DTF_M_D_splus_Kplus_PE);
   fChain->SetBranchAddress("B_DTF_M_D_splus_Kplus_PX", B_DTF_M_D_splus_Kplus_PX, &b_B_DTF_M_D_splus_Kplus_PX);
   fChain->SetBranchAddress("B_DTF_M_D_splus_Kplus_PY", B_DTF_M_D_splus_Kplus_PY, &b_B_DTF_M_D_splus_Kplus_PY);
   fChain->SetBranchAddress("B_DTF_M_D_splus_Kplus_PZ", B_DTF_M_D_splus_Kplus_PZ, &b_B_DTF_M_D_splus_Kplus_PZ);
   fChain->SetBranchAddress("B_DTF_M_D_splus_M", B_DTF_M_D_splus_M, &b_B_DTF_M_D_splus_M);
   fChain->SetBranchAddress("B_DTF_M_D_splus_MERR", B_DTF_M_D_splus_MERR, &b_B_DTF_M_D_splus_MERR);
   fChain->SetBranchAddress("B_DTF_M_D_splus_P", B_DTF_M_D_splus_P, &b_B_DTF_M_D_splus_P);
   fChain->SetBranchAddress("B_DTF_M_D_splus_PERR", B_DTF_M_D_splus_PERR, &b_B_DTF_M_D_splus_PERR);
   fChain->SetBranchAddress("B_DTF_M_D_splus_ctau", B_DTF_M_D_splus_ctau, &b_B_DTF_M_D_splus_ctau);
   fChain->SetBranchAddress("B_DTF_M_D_splus_ctauErr", B_DTF_M_D_splus_ctauErr, &b_B_DTF_M_D_splus_ctauErr);
   fChain->SetBranchAddress("B_DTF_M_D_splus_decayLength", B_DTF_M_D_splus_decayLength, &b_B_DTF_M_D_splus_decayLength);
   fChain->SetBranchAddress("B_DTF_M_D_splus_decayLengthErr", B_DTF_M_D_splus_decayLengthErr, &b_B_DTF_M_D_splus_decayLengthErr);
   fChain->SetBranchAddress("B_DTF_M_D_splus_piplus_ID", B_DTF_M_D_splus_piplus_ID, &b_B_DTF_M_D_splus_piplus_ID);
   fChain->SetBranchAddress("B_DTF_M_D_splus_piplus_PE", B_DTF_M_D_splus_piplus_PE, &b_B_DTF_M_D_splus_piplus_PE);
   fChain->SetBranchAddress("B_DTF_M_D_splus_piplus_PX", B_DTF_M_D_splus_piplus_PX, &b_B_DTF_M_D_splus_piplus_PX);
   fChain->SetBranchAddress("B_DTF_M_D_splus_piplus_PY", B_DTF_M_D_splus_piplus_PY, &b_B_DTF_M_D_splus_piplus_PY);
   fChain->SetBranchAddress("B_DTF_M_D_splus_piplus_PZ", B_DTF_M_D_splus_piplus_PZ, &b_B_DTF_M_D_splus_piplus_PZ);
   fChain->SetBranchAddress("B_DTF_M_Dplus_Kplus_ID", B_DTF_M_Dplus_Kplus_ID, &b_B_DTF_M_Dplus_Kplus_ID);
   fChain->SetBranchAddress("B_DTF_M_Dplus_Kplus_PE", B_DTF_M_Dplus_Kplus_PE, &b_B_DTF_M_Dplus_Kplus_PE);
   fChain->SetBranchAddress("B_DTF_M_Dplus_Kplus_PX", B_DTF_M_Dplus_Kplus_PX, &b_B_DTF_M_Dplus_Kplus_PX);
   fChain->SetBranchAddress("B_DTF_M_Dplus_Kplus_PY", B_DTF_M_Dplus_Kplus_PY, &b_B_DTF_M_Dplus_Kplus_PY);
   fChain->SetBranchAddress("B_DTF_M_Dplus_Kplus_PZ", B_DTF_M_Dplus_Kplus_PZ, &b_B_DTF_M_Dplus_Kplus_PZ);
   fChain->SetBranchAddress("B_DTF_M_Dplus_M", B_DTF_M_Dplus_M, &b_B_DTF_M_Dplus_M);
   fChain->SetBranchAddress("B_DTF_M_Dplus_MERR", B_DTF_M_Dplus_MERR, &b_B_DTF_M_Dplus_MERR);
   fChain->SetBranchAddress("B_DTF_M_Dplus_P", B_DTF_M_Dplus_P, &b_B_DTF_M_Dplus_P);
   fChain->SetBranchAddress("B_DTF_M_Dplus_PERR", B_DTF_M_Dplus_PERR, &b_B_DTF_M_Dplus_PERR);
   fChain->SetBranchAddress("B_DTF_M_Dplus_ctau", B_DTF_M_Dplus_ctau, &b_B_DTF_M_Dplus_ctau);
   fChain->SetBranchAddress("B_DTF_M_Dplus_ctauErr", B_DTF_M_Dplus_ctauErr, &b_B_DTF_M_Dplus_ctauErr);
   fChain->SetBranchAddress("B_DTF_M_Dplus_decayLength", B_DTF_M_Dplus_decayLength, &b_B_DTF_M_Dplus_decayLength);
   fChain->SetBranchAddress("B_DTF_M_Dplus_decayLengthErr", B_DTF_M_Dplus_decayLengthErr, &b_B_DTF_M_Dplus_decayLengthErr);
   fChain->SetBranchAddress("B_DTF_M_Dplus_piplus_0_ID", B_DTF_M_Dplus_piplus_0_ID, &b_B_DTF_M_Dplus_piplus_0_ID);
   fChain->SetBranchAddress("B_DTF_M_Dplus_piplus_0_PE", B_DTF_M_Dplus_piplus_0_PE, &b_B_DTF_M_Dplus_piplus_0_PE);
   fChain->SetBranchAddress("B_DTF_M_Dplus_piplus_0_PX", B_DTF_M_Dplus_piplus_0_PX, &b_B_DTF_M_Dplus_piplus_0_PX);
   fChain->SetBranchAddress("B_DTF_M_Dplus_piplus_0_PY", B_DTF_M_Dplus_piplus_0_PY, &b_B_DTF_M_Dplus_piplus_0_PY);
   fChain->SetBranchAddress("B_DTF_M_Dplus_piplus_0_PZ", B_DTF_M_Dplus_piplus_0_PZ, &b_B_DTF_M_Dplus_piplus_0_PZ);
   fChain->SetBranchAddress("B_DTF_M_Dplus_piplus_ID", B_DTF_M_Dplus_piplus_ID, &b_B_DTF_M_Dplus_piplus_ID);
   fChain->SetBranchAddress("B_DTF_M_Dplus_piplus_PE", B_DTF_M_Dplus_piplus_PE, &b_B_DTF_M_Dplus_piplus_PE);
   fChain->SetBranchAddress("B_DTF_M_Dplus_piplus_PX", B_DTF_M_Dplus_piplus_PX, &b_B_DTF_M_Dplus_piplus_PX);
   fChain->SetBranchAddress("B_DTF_M_Dplus_piplus_PY", B_DTF_M_Dplus_piplus_PY, &b_B_DTF_M_Dplus_piplus_PY);
   fChain->SetBranchAddress("B_DTF_M_Dplus_piplus_PZ", B_DTF_M_Dplus_piplus_PZ, &b_B_DTF_M_Dplus_piplus_PZ);
   fChain->SetBranchAddress("B_DTF_M_M", B_DTF_M_M, &b_B_DTF_M_M);
   fChain->SetBranchAddress("B_DTF_M_MERR", B_DTF_M_MERR, &b_B_DTF_M_MERR);
   fChain->SetBranchAddress("B_DTF_M_P", B_DTF_M_P, &b_B_DTF_M_P);
   fChain->SetBranchAddress("B_DTF_M_PERR", B_DTF_M_PERR, &b_B_DTF_M_PERR);
   fChain->SetBranchAddress("B_DTF_M_chi2", B_DTF_M_chi2, &b_B_DTF_M_chi2);
   fChain->SetBranchAddress("B_DTF_M_nDOF", B_DTF_M_nDOF, &b_B_DTF_M_nDOF);
   fChain->SetBranchAddress("B_DTF_M_nIter", B_DTF_M_nIter, &b_B_DTF_M_nIter);
   fChain->SetBranchAddress("B_DTF_M_status", B_DTF_M_status, &b_B_DTF_M_status);
   fChain->SetBranchAddress("B_DTF_M_PVC_nPV", &B_DTF_M_PVC_nPV, &b_B_DTF_M_PVC_nPV);
   fChain->SetBranchAddress("B_DTF_M_PVC_D_splus_Kplus_0_ID", B_DTF_M_PVC_D_splus_Kplus_0_ID, &b_B_DTF_M_PVC_D_splus_Kplus_0_ID);
   fChain->SetBranchAddress("B_DTF_M_PVC_D_splus_Kplus_0_PE", B_DTF_M_PVC_D_splus_Kplus_0_PE, &b_B_DTF_M_PVC_D_splus_Kplus_0_PE);
   fChain->SetBranchAddress("B_DTF_M_PVC_D_splus_Kplus_0_PX", B_DTF_M_PVC_D_splus_Kplus_0_PX, &b_B_DTF_M_PVC_D_splus_Kplus_0_PX);
   fChain->SetBranchAddress("B_DTF_M_PVC_D_splus_Kplus_0_PY", B_DTF_M_PVC_D_splus_Kplus_0_PY, &b_B_DTF_M_PVC_D_splus_Kplus_0_PY);
   fChain->SetBranchAddress("B_DTF_M_PVC_D_splus_Kplus_0_PZ", B_DTF_M_PVC_D_splus_Kplus_0_PZ, &b_B_DTF_M_PVC_D_splus_Kplus_0_PZ);
   fChain->SetBranchAddress("B_DTF_M_PVC_D_splus_Kplus_ID", B_DTF_M_PVC_D_splus_Kplus_ID, &b_B_DTF_M_PVC_D_splus_Kplus_ID);
   fChain->SetBranchAddress("B_DTF_M_PVC_D_splus_Kplus_PE", B_DTF_M_PVC_D_splus_Kplus_PE, &b_B_DTF_M_PVC_D_splus_Kplus_PE);
   fChain->SetBranchAddress("B_DTF_M_PVC_D_splus_Kplus_PX", B_DTF_M_PVC_D_splus_Kplus_PX, &b_B_DTF_M_PVC_D_splus_Kplus_PX);
   fChain->SetBranchAddress("B_DTF_M_PVC_D_splus_Kplus_PY", B_DTF_M_PVC_D_splus_Kplus_PY, &b_B_DTF_M_PVC_D_splus_Kplus_PY);
   fChain->SetBranchAddress("B_DTF_M_PVC_D_splus_Kplus_PZ", B_DTF_M_PVC_D_splus_Kplus_PZ, &b_B_DTF_M_PVC_D_splus_Kplus_PZ);
   fChain->SetBranchAddress("B_DTF_M_PVC_D_splus_M", B_DTF_M_PVC_D_splus_M, &b_B_DTF_M_PVC_D_splus_M);
   fChain->SetBranchAddress("B_DTF_M_PVC_D_splus_MERR", B_DTF_M_PVC_D_splus_MERR, &b_B_DTF_M_PVC_D_splus_MERR);
   fChain->SetBranchAddress("B_DTF_M_PVC_D_splus_P", B_DTF_M_PVC_D_splus_P, &b_B_DTF_M_PVC_D_splus_P);
   fChain->SetBranchAddress("B_DTF_M_PVC_D_splus_PERR", B_DTF_M_PVC_D_splus_PERR, &b_B_DTF_M_PVC_D_splus_PERR);
   fChain->SetBranchAddress("B_DTF_M_PVC_D_splus_ctau", B_DTF_M_PVC_D_splus_ctau, &b_B_DTF_M_PVC_D_splus_ctau);
   fChain->SetBranchAddress("B_DTF_M_PVC_D_splus_ctauErr", B_DTF_M_PVC_D_splus_ctauErr, &b_B_DTF_M_PVC_D_splus_ctauErr);
   fChain->SetBranchAddress("B_DTF_M_PVC_D_splus_decayLength", B_DTF_M_PVC_D_splus_decayLength, &b_B_DTF_M_PVC_D_splus_decayLength);
   fChain->SetBranchAddress("B_DTF_M_PVC_D_splus_decayLengthErr", B_DTF_M_PVC_D_splus_decayLengthErr, &b_B_DTF_M_PVC_D_splus_decayLengthErr);
   fChain->SetBranchAddress("B_DTF_M_PVC_D_splus_piplus_ID", B_DTF_M_PVC_D_splus_piplus_ID, &b_B_DTF_M_PVC_D_splus_piplus_ID);
   fChain->SetBranchAddress("B_DTF_M_PVC_D_splus_piplus_PE", B_DTF_M_PVC_D_splus_piplus_PE, &b_B_DTF_M_PVC_D_splus_piplus_PE);
   fChain->SetBranchAddress("B_DTF_M_PVC_D_splus_piplus_PX", B_DTF_M_PVC_D_splus_piplus_PX, &b_B_DTF_M_PVC_D_splus_piplus_PX);
   fChain->SetBranchAddress("B_DTF_M_PVC_D_splus_piplus_PY", B_DTF_M_PVC_D_splus_piplus_PY, &b_B_DTF_M_PVC_D_splus_piplus_PY);
   fChain->SetBranchAddress("B_DTF_M_PVC_D_splus_piplus_PZ", B_DTF_M_PVC_D_splus_piplus_PZ, &b_B_DTF_M_PVC_D_splus_piplus_PZ);
   fChain->SetBranchAddress("B_DTF_M_PVC_Dplus_Kplus_ID", B_DTF_M_PVC_Dplus_Kplus_ID, &b_B_DTF_M_PVC_Dplus_Kplus_ID);
   fChain->SetBranchAddress("B_DTF_M_PVC_Dplus_Kplus_PE", B_DTF_M_PVC_Dplus_Kplus_PE, &b_B_DTF_M_PVC_Dplus_Kplus_PE);
   fChain->SetBranchAddress("B_DTF_M_PVC_Dplus_Kplus_PX", B_DTF_M_PVC_Dplus_Kplus_PX, &b_B_DTF_M_PVC_Dplus_Kplus_PX);
   fChain->SetBranchAddress("B_DTF_M_PVC_Dplus_Kplus_PY", B_DTF_M_PVC_Dplus_Kplus_PY, &b_B_DTF_M_PVC_Dplus_Kplus_PY);
   fChain->SetBranchAddress("B_DTF_M_PVC_Dplus_Kplus_PZ", B_DTF_M_PVC_Dplus_Kplus_PZ, &b_B_DTF_M_PVC_Dplus_Kplus_PZ);
   fChain->SetBranchAddress("B_DTF_M_PVC_Dplus_M", B_DTF_M_PVC_Dplus_M, &b_B_DTF_M_PVC_Dplus_M);
   fChain->SetBranchAddress("B_DTF_M_PVC_Dplus_MERR", B_DTF_M_PVC_Dplus_MERR, &b_B_DTF_M_PVC_Dplus_MERR);
   fChain->SetBranchAddress("B_DTF_M_PVC_Dplus_P", B_DTF_M_PVC_Dplus_P, &b_B_DTF_M_PVC_Dplus_P);
   fChain->SetBranchAddress("B_DTF_M_PVC_Dplus_PERR", B_DTF_M_PVC_Dplus_PERR, &b_B_DTF_M_PVC_Dplus_PERR);
   fChain->SetBranchAddress("B_DTF_M_PVC_Dplus_ctau", B_DTF_M_PVC_Dplus_ctau, &b_B_DTF_M_PVC_Dplus_ctau);
   fChain->SetBranchAddress("B_DTF_M_PVC_Dplus_ctauErr", B_DTF_M_PVC_Dplus_ctauErr, &b_B_DTF_M_PVC_Dplus_ctauErr);
   fChain->SetBranchAddress("B_DTF_M_PVC_Dplus_decayLength", B_DTF_M_PVC_Dplus_decayLength, &b_B_DTF_M_PVC_Dplus_decayLength);
   fChain->SetBranchAddress("B_DTF_M_PVC_Dplus_decayLengthErr", B_DTF_M_PVC_Dplus_decayLengthErr, &b_B_DTF_M_PVC_Dplus_decayLengthErr);
   fChain->SetBranchAddress("B_DTF_M_PVC_Dplus_piplus_0_ID", B_DTF_M_PVC_Dplus_piplus_0_ID, &b_B_DTF_M_PVC_Dplus_piplus_0_ID);
   fChain->SetBranchAddress("B_DTF_M_PVC_Dplus_piplus_0_PE", B_DTF_M_PVC_Dplus_piplus_0_PE, &b_B_DTF_M_PVC_Dplus_piplus_0_PE);
   fChain->SetBranchAddress("B_DTF_M_PVC_Dplus_piplus_0_PX", B_DTF_M_PVC_Dplus_piplus_0_PX, &b_B_DTF_M_PVC_Dplus_piplus_0_PX);
   fChain->SetBranchAddress("B_DTF_M_PVC_Dplus_piplus_0_PY", B_DTF_M_PVC_Dplus_piplus_0_PY, &b_B_DTF_M_PVC_Dplus_piplus_0_PY);
   fChain->SetBranchAddress("B_DTF_M_PVC_Dplus_piplus_0_PZ", B_DTF_M_PVC_Dplus_piplus_0_PZ, &b_B_DTF_M_PVC_Dplus_piplus_0_PZ);
   fChain->SetBranchAddress("B_DTF_M_PVC_Dplus_piplus_ID", B_DTF_M_PVC_Dplus_piplus_ID, &b_B_DTF_M_PVC_Dplus_piplus_ID);
   fChain->SetBranchAddress("B_DTF_M_PVC_Dplus_piplus_PE", B_DTF_M_PVC_Dplus_piplus_PE, &b_B_DTF_M_PVC_Dplus_piplus_PE);
   fChain->SetBranchAddress("B_DTF_M_PVC_Dplus_piplus_PX", B_DTF_M_PVC_Dplus_piplus_PX, &b_B_DTF_M_PVC_Dplus_piplus_PX);
   fChain->SetBranchAddress("B_DTF_M_PVC_Dplus_piplus_PY", B_DTF_M_PVC_Dplus_piplus_PY, &b_B_DTF_M_PVC_Dplus_piplus_PY);
   fChain->SetBranchAddress("B_DTF_M_PVC_Dplus_piplus_PZ", B_DTF_M_PVC_Dplus_piplus_PZ, &b_B_DTF_M_PVC_Dplus_piplus_PZ);
   fChain->SetBranchAddress("B_DTF_M_PVC_M", B_DTF_M_PVC_M, &b_B_DTF_M_PVC_M);
   fChain->SetBranchAddress("B_DTF_M_PVC_MERR", B_DTF_M_PVC_MERR, &b_B_DTF_M_PVC_MERR);
   fChain->SetBranchAddress("B_DTF_M_PVC_P", B_DTF_M_PVC_P, &b_B_DTF_M_PVC_P);
   fChain->SetBranchAddress("B_DTF_M_PVC_PERR", B_DTF_M_PVC_PERR, &b_B_DTF_M_PVC_PERR);
   fChain->SetBranchAddress("B_DTF_M_PVC_PV_X", B_DTF_M_PVC_PV_X, &b_B_DTF_M_PVC_PV_X);
   fChain->SetBranchAddress("B_DTF_M_PVC_PV_Y", B_DTF_M_PVC_PV_Y, &b_B_DTF_M_PVC_PV_Y);
   fChain->SetBranchAddress("B_DTF_M_PVC_PV_Z", B_DTF_M_PVC_PV_Z, &b_B_DTF_M_PVC_PV_Z);
   fChain->SetBranchAddress("B_DTF_M_PVC_PV_key", B_DTF_M_PVC_PV_key, &b_B_DTF_M_PVC_PV_key);
   fChain->SetBranchAddress("B_DTF_M_PVC_chi2", B_DTF_M_PVC_chi2, &b_B_DTF_M_PVC_chi2);
   fChain->SetBranchAddress("B_DTF_M_PVC_ctau", B_DTF_M_PVC_ctau, &b_B_DTF_M_PVC_ctau);
   fChain->SetBranchAddress("B_DTF_M_PVC_ctauErr", B_DTF_M_PVC_ctauErr, &b_B_DTF_M_PVC_ctauErr);
   fChain->SetBranchAddress("B_DTF_M_PVC_decayLength", B_DTF_M_PVC_decayLength, &b_B_DTF_M_PVC_decayLength);
   fChain->SetBranchAddress("B_DTF_M_PVC_decayLengthErr", B_DTF_M_PVC_decayLengthErr, &b_B_DTF_M_PVC_decayLengthErr);
   fChain->SetBranchAddress("B_DTF_M_PVC_nDOF", B_DTF_M_PVC_nDOF, &b_B_DTF_M_PVC_nDOF);
   fChain->SetBranchAddress("B_DTF_M_PVC_nIter", B_DTF_M_PVC_nIter, &b_B_DTF_M_PVC_nIter);
   fChain->SetBranchAddress("B_DTF_M_PVC_status", B_DTF_M_PVC_status, &b_B_DTF_M_PVC_status);
   fChain->SetBranchAddress("B_PVC_nPV", &B_PVC_nPV, &b_B_PVC_nPV);
   fChain->SetBranchAddress("B_PVC_D_splus_Kplus_0_ID", B_PVC_D_splus_Kplus_0_ID, &b_B_PVC_D_splus_Kplus_0_ID);
   fChain->SetBranchAddress("B_PVC_D_splus_Kplus_0_PE", B_PVC_D_splus_Kplus_0_PE, &b_B_PVC_D_splus_Kplus_0_PE);
   fChain->SetBranchAddress("B_PVC_D_splus_Kplus_0_PX", B_PVC_D_splus_Kplus_0_PX, &b_B_PVC_D_splus_Kplus_0_PX);
   fChain->SetBranchAddress("B_PVC_D_splus_Kplus_0_PY", B_PVC_D_splus_Kplus_0_PY, &b_B_PVC_D_splus_Kplus_0_PY);
   fChain->SetBranchAddress("B_PVC_D_splus_Kplus_0_PZ", B_PVC_D_splus_Kplus_0_PZ, &b_B_PVC_D_splus_Kplus_0_PZ);
   fChain->SetBranchAddress("B_PVC_D_splus_Kplus_ID", B_PVC_D_splus_Kplus_ID, &b_B_PVC_D_splus_Kplus_ID);
   fChain->SetBranchAddress("B_PVC_D_splus_Kplus_PE", B_PVC_D_splus_Kplus_PE, &b_B_PVC_D_splus_Kplus_PE);
   fChain->SetBranchAddress("B_PVC_D_splus_Kplus_PX", B_PVC_D_splus_Kplus_PX, &b_B_PVC_D_splus_Kplus_PX);
   fChain->SetBranchAddress("B_PVC_D_splus_Kplus_PY", B_PVC_D_splus_Kplus_PY, &b_B_PVC_D_splus_Kplus_PY);
   fChain->SetBranchAddress("B_PVC_D_splus_Kplus_PZ", B_PVC_D_splus_Kplus_PZ, &b_B_PVC_D_splus_Kplus_PZ);
   fChain->SetBranchAddress("B_PVC_D_splus_M", B_PVC_D_splus_M, &b_B_PVC_D_splus_M);
   fChain->SetBranchAddress("B_PVC_D_splus_MERR", B_PVC_D_splus_MERR, &b_B_PVC_D_splus_MERR);
   fChain->SetBranchAddress("B_PVC_D_splus_P", B_PVC_D_splus_P, &b_B_PVC_D_splus_P);
   fChain->SetBranchAddress("B_PVC_D_splus_PERR", B_PVC_D_splus_PERR, &b_B_PVC_D_splus_PERR);
   fChain->SetBranchAddress("B_PVC_D_splus_ctau", B_PVC_D_splus_ctau, &b_B_PVC_D_splus_ctau);
   fChain->SetBranchAddress("B_PVC_D_splus_ctauErr", B_PVC_D_splus_ctauErr, &b_B_PVC_D_splus_ctauErr);
   fChain->SetBranchAddress("B_PVC_D_splus_decayLength", B_PVC_D_splus_decayLength, &b_B_PVC_D_splus_decayLength);
   fChain->SetBranchAddress("B_PVC_D_splus_decayLengthErr", B_PVC_D_splus_decayLengthErr, &b_B_PVC_D_splus_decayLengthErr);
   fChain->SetBranchAddress("B_PVC_D_splus_piplus_ID", B_PVC_D_splus_piplus_ID, &b_B_PVC_D_splus_piplus_ID);
   fChain->SetBranchAddress("B_PVC_D_splus_piplus_PE", B_PVC_D_splus_piplus_PE, &b_B_PVC_D_splus_piplus_PE);
   fChain->SetBranchAddress("B_PVC_D_splus_piplus_PX", B_PVC_D_splus_piplus_PX, &b_B_PVC_D_splus_piplus_PX);
   fChain->SetBranchAddress("B_PVC_D_splus_piplus_PY", B_PVC_D_splus_piplus_PY, &b_B_PVC_D_splus_piplus_PY);
   fChain->SetBranchAddress("B_PVC_D_splus_piplus_PZ", B_PVC_D_splus_piplus_PZ, &b_B_PVC_D_splus_piplus_PZ);
   fChain->SetBranchAddress("B_PVC_Dplus_Kplus_ID", B_PVC_Dplus_Kplus_ID, &b_B_PVC_Dplus_Kplus_ID);
   fChain->SetBranchAddress("B_PVC_Dplus_Kplus_PE", B_PVC_Dplus_Kplus_PE, &b_B_PVC_Dplus_Kplus_PE);
   fChain->SetBranchAddress("B_PVC_Dplus_Kplus_PX", B_PVC_Dplus_Kplus_PX, &b_B_PVC_Dplus_Kplus_PX);
   fChain->SetBranchAddress("B_PVC_Dplus_Kplus_PY", B_PVC_Dplus_Kplus_PY, &b_B_PVC_Dplus_Kplus_PY);
   fChain->SetBranchAddress("B_PVC_Dplus_Kplus_PZ", B_PVC_Dplus_Kplus_PZ, &b_B_PVC_Dplus_Kplus_PZ);
   fChain->SetBranchAddress("B_PVC_Dplus_M", B_PVC_Dplus_M, &b_B_PVC_Dplus_M);
   fChain->SetBranchAddress("B_PVC_Dplus_MERR", B_PVC_Dplus_MERR, &b_B_PVC_Dplus_MERR);
   fChain->SetBranchAddress("B_PVC_Dplus_P", B_PVC_Dplus_P, &b_B_PVC_Dplus_P);
   fChain->SetBranchAddress("B_PVC_Dplus_PERR", B_PVC_Dplus_PERR, &b_B_PVC_Dplus_PERR);
   fChain->SetBranchAddress("B_PVC_Dplus_ctau", B_PVC_Dplus_ctau, &b_B_PVC_Dplus_ctau);
   fChain->SetBranchAddress("B_PVC_Dplus_ctauErr", B_PVC_Dplus_ctauErr, &b_B_PVC_Dplus_ctauErr);
   fChain->SetBranchAddress("B_PVC_Dplus_decayLength", B_PVC_Dplus_decayLength, &b_B_PVC_Dplus_decayLength);
   fChain->SetBranchAddress("B_PVC_Dplus_decayLengthErr", B_PVC_Dplus_decayLengthErr, &b_B_PVC_Dplus_decayLengthErr);
   fChain->SetBranchAddress("B_PVC_Dplus_piplus_0_ID", B_PVC_Dplus_piplus_0_ID, &b_B_PVC_Dplus_piplus_0_ID);
   fChain->SetBranchAddress("B_PVC_Dplus_piplus_0_PE", B_PVC_Dplus_piplus_0_PE, &b_B_PVC_Dplus_piplus_0_PE);
   fChain->SetBranchAddress("B_PVC_Dplus_piplus_0_PX", B_PVC_Dplus_piplus_0_PX, &b_B_PVC_Dplus_piplus_0_PX);
   fChain->SetBranchAddress("B_PVC_Dplus_piplus_0_PY", B_PVC_Dplus_piplus_0_PY, &b_B_PVC_Dplus_piplus_0_PY);
   fChain->SetBranchAddress("B_PVC_Dplus_piplus_0_PZ", B_PVC_Dplus_piplus_0_PZ, &b_B_PVC_Dplus_piplus_0_PZ);
   fChain->SetBranchAddress("B_PVC_Dplus_piplus_ID", B_PVC_Dplus_piplus_ID, &b_B_PVC_Dplus_piplus_ID);
   fChain->SetBranchAddress("B_PVC_Dplus_piplus_PE", B_PVC_Dplus_piplus_PE, &b_B_PVC_Dplus_piplus_PE);
   fChain->SetBranchAddress("B_PVC_Dplus_piplus_PX", B_PVC_Dplus_piplus_PX, &b_B_PVC_Dplus_piplus_PX);
   fChain->SetBranchAddress("B_PVC_Dplus_piplus_PY", B_PVC_Dplus_piplus_PY, &b_B_PVC_Dplus_piplus_PY);
   fChain->SetBranchAddress("B_PVC_Dplus_piplus_PZ", B_PVC_Dplus_piplus_PZ, &b_B_PVC_Dplus_piplus_PZ);
   fChain->SetBranchAddress("B_PVC_M", B_PVC_M, &b_B_PVC_M);
   fChain->SetBranchAddress("B_PVC_MERR", B_PVC_MERR, &b_B_PVC_MERR);
   fChain->SetBranchAddress("B_PVC_P", B_PVC_P, &b_B_PVC_P);
   fChain->SetBranchAddress("B_PVC_PERR", B_PVC_PERR, &b_B_PVC_PERR);
   fChain->SetBranchAddress("B_PVC_PV_X", B_PVC_PV_X, &b_B_PVC_PV_X);
   fChain->SetBranchAddress("B_PVC_PV_Y", B_PVC_PV_Y, &b_B_PVC_PV_Y);
   fChain->SetBranchAddress("B_PVC_PV_Z", B_PVC_PV_Z, &b_B_PVC_PV_Z);
   fChain->SetBranchAddress("B_PVC_PV_key", B_PVC_PV_key, &b_B_PVC_PV_key);
   fChain->SetBranchAddress("B_PVC_chi2", B_PVC_chi2, &b_B_PVC_chi2);
   fChain->SetBranchAddress("B_PVC_ctau", B_PVC_ctau, &b_B_PVC_ctau);
   fChain->SetBranchAddress("B_PVC_ctauErr", B_PVC_ctauErr, &b_B_PVC_ctauErr);
   fChain->SetBranchAddress("B_PVC_decayLength", B_PVC_decayLength, &b_B_PVC_decayLength);
   fChain->SetBranchAddress("B_PVC_decayLengthErr", B_PVC_decayLengthErr, &b_B_PVC_decayLengthErr);
   fChain->SetBranchAddress("B_PVC_nDOF", B_PVC_nDOF, &b_B_PVC_nDOF);
   fChain->SetBranchAddress("B_PVC_nIter", B_PVC_nIter, &b_B_PVC_nIter);
   fChain->SetBranchAddress("B_PVC_status", B_PVC_status, &b_B_PVC_status);
   fChain->SetBranchAddress("Dsminus_ETA", &Dsminus_ETA, &b_Dsminus_ETA);
   fChain->SetBranchAddress("Dsminus_MinIPCHI2", &Dsminus_MinIPCHI2, &b_Dsminus_MinIPCHI2);
   fChain->SetBranchAddress("Dsminus_CosTheta", &Dsminus_CosTheta, &b_Dsminus_CosTheta);
   fChain->SetBranchAddress("Dsminus_ENDVERTEX_X", &Dsminus_ENDVERTEX_X, &b_Dsminus_ENDVERTEX_X);
   fChain->SetBranchAddress("Dsminus_ENDVERTEX_Y", &Dsminus_ENDVERTEX_Y, &b_Dsminus_ENDVERTEX_Y);
   fChain->SetBranchAddress("Dsminus_ENDVERTEX_Z", &Dsminus_ENDVERTEX_Z, &b_Dsminus_ENDVERTEX_Z);
   fChain->SetBranchAddress("Dsminus_ENDVERTEX_XERR", &Dsminus_ENDVERTEX_XERR, &b_Dsminus_ENDVERTEX_XERR);
   fChain->SetBranchAddress("Dsminus_ENDVERTEX_YERR", &Dsminus_ENDVERTEX_YERR, &b_Dsminus_ENDVERTEX_YERR);
   fChain->SetBranchAddress("Dsminus_ENDVERTEX_ZERR", &Dsminus_ENDVERTEX_ZERR, &b_Dsminus_ENDVERTEX_ZERR);
   fChain->SetBranchAddress("Dsminus_ENDVERTEX_CHI2", &Dsminus_ENDVERTEX_CHI2, &b_Dsminus_ENDVERTEX_CHI2);
   fChain->SetBranchAddress("Dsminus_ENDVERTEX_NDOF", &Dsminus_ENDVERTEX_NDOF, &b_Dsminus_ENDVERTEX_NDOF);
   fChain->SetBranchAddress("Dsminus_ENDVERTEX_COV_", Dsminus_ENDVERTEX_COV_, &b_Dsminus_ENDVERTEX_COV_);
   fChain->SetBranchAddress("Dsminus_OWNPV_X", &Dsminus_OWNPV_X, &b_Dsminus_OWNPV_X);
   fChain->SetBranchAddress("Dsminus_OWNPV_Y", &Dsminus_OWNPV_Y, &b_Dsminus_OWNPV_Y);
   fChain->SetBranchAddress("Dsminus_OWNPV_Z", &Dsminus_OWNPV_Z, &b_Dsminus_OWNPV_Z);
   fChain->SetBranchAddress("Dsminus_OWNPV_XERR", &Dsminus_OWNPV_XERR, &b_Dsminus_OWNPV_XERR);
   fChain->SetBranchAddress("Dsminus_OWNPV_YERR", &Dsminus_OWNPV_YERR, &b_Dsminus_OWNPV_YERR);
   fChain->SetBranchAddress("Dsminus_OWNPV_ZERR", &Dsminus_OWNPV_ZERR, &b_Dsminus_OWNPV_ZERR);
   fChain->SetBranchAddress("Dsminus_OWNPV_CHI2", &Dsminus_OWNPV_CHI2, &b_Dsminus_OWNPV_CHI2);
   fChain->SetBranchAddress("Dsminus_OWNPV_NDOF", &Dsminus_OWNPV_NDOF, &b_Dsminus_OWNPV_NDOF);
   fChain->SetBranchAddress("Dsminus_OWNPV_COV_", Dsminus_OWNPV_COV_, &b_Dsminus_OWNPV_COV_);
   fChain->SetBranchAddress("Dsminus_IP_OWNPV", &Dsminus_IP_OWNPV, &b_Dsminus_IP_OWNPV);
   fChain->SetBranchAddress("Dsminus_IPCHI2_OWNPV", &Dsminus_IPCHI2_OWNPV, &b_Dsminus_IPCHI2_OWNPV);
   fChain->SetBranchAddress("Dsminus_FD_OWNPV", &Dsminus_FD_OWNPV, &b_Dsminus_FD_OWNPV);
   fChain->SetBranchAddress("Dsminus_FDCHI2_OWNPV", &Dsminus_FDCHI2_OWNPV, &b_Dsminus_FDCHI2_OWNPV);
   fChain->SetBranchAddress("Dsminus_DIRA_OWNPV", &Dsminus_DIRA_OWNPV, &b_Dsminus_DIRA_OWNPV);
   fChain->SetBranchAddress("Dsminus_ORIVX_X", &Dsminus_ORIVX_X, &b_Dsminus_ORIVX_X);
   fChain->SetBranchAddress("Dsminus_ORIVX_Y", &Dsminus_ORIVX_Y, &b_Dsminus_ORIVX_Y);
   fChain->SetBranchAddress("Dsminus_ORIVX_Z", &Dsminus_ORIVX_Z, &b_Dsminus_ORIVX_Z);
   fChain->SetBranchAddress("Dsminus_ORIVX_XERR", &Dsminus_ORIVX_XERR, &b_Dsminus_ORIVX_XERR);
   fChain->SetBranchAddress("Dsminus_ORIVX_YERR", &Dsminus_ORIVX_YERR, &b_Dsminus_ORIVX_YERR);
   fChain->SetBranchAddress("Dsminus_ORIVX_ZERR", &Dsminus_ORIVX_ZERR, &b_Dsminus_ORIVX_ZERR);
   fChain->SetBranchAddress("Dsminus_ORIVX_CHI2", &Dsminus_ORIVX_CHI2, &b_Dsminus_ORIVX_CHI2);
   fChain->SetBranchAddress("Dsminus_ORIVX_NDOF", &Dsminus_ORIVX_NDOF, &b_Dsminus_ORIVX_NDOF);
   fChain->SetBranchAddress("Dsminus_ORIVX_COV_", Dsminus_ORIVX_COV_, &b_Dsminus_ORIVX_COV_);
   fChain->SetBranchAddress("Dsminus_FD_ORIVX", &Dsminus_FD_ORIVX, &b_Dsminus_FD_ORIVX);
   fChain->SetBranchAddress("Dsminus_FDCHI2_ORIVX", &Dsminus_FDCHI2_ORIVX, &b_Dsminus_FDCHI2_ORIVX);
   fChain->SetBranchAddress("Dsminus_DIRA_ORIVX", &Dsminus_DIRA_ORIVX, &b_Dsminus_DIRA_ORIVX);
   fChain->SetBranchAddress("Dsminus_P", &Dsminus_P, &b_Dsminus_P);
   fChain->SetBranchAddress("Dsminus_PT", &Dsminus_PT, &b_Dsminus_PT);
   fChain->SetBranchAddress("Dsminus_PE", &Dsminus_PE, &b_Dsminus_PE);
   fChain->SetBranchAddress("Dsminus_PX", &Dsminus_PX, &b_Dsminus_PX);
   fChain->SetBranchAddress("Dsminus_PY", &Dsminus_PY, &b_Dsminus_PY);
   fChain->SetBranchAddress("Dsminus_PZ", &Dsminus_PZ, &b_Dsminus_PZ);
   fChain->SetBranchAddress("Dsminus_REFPX", &Dsminus_REFPX, &b_Dsminus_REFPX);
   fChain->SetBranchAddress("Dsminus_REFPY", &Dsminus_REFPY, &b_Dsminus_REFPY);
   fChain->SetBranchAddress("Dsminus_REFPZ", &Dsminus_REFPZ, &b_Dsminus_REFPZ);
   fChain->SetBranchAddress("Dsminus_MM", &Dsminus_MM, &b_Dsminus_MM);
   fChain->SetBranchAddress("Dsminus_MMERR", &Dsminus_MMERR, &b_Dsminus_MMERR);
   fChain->SetBranchAddress("Dsminus_M", &Dsminus_M, &b_Dsminus_M);
   fChain->SetBranchAddress("Dsminus_BKGCAT", &Dsminus_BKGCAT, &b_Dsminus_BKGCAT);
   fChain->SetBranchAddress("Dsminus_TRUEID", &Dsminus_TRUEID, &b_Dsminus_TRUEID);
   fChain->SetBranchAddress("Dsminus_TRUEP_E", &Dsminus_TRUEP_E, &b_Dsminus_TRUEP_E);
   fChain->SetBranchAddress("Dsminus_TRUEP_X", &Dsminus_TRUEP_X, &b_Dsminus_TRUEP_X);
   fChain->SetBranchAddress("Dsminus_TRUEP_Y", &Dsminus_TRUEP_Y, &b_Dsminus_TRUEP_Y);
   fChain->SetBranchAddress("Dsminus_TRUEP_Z", &Dsminus_TRUEP_Z, &b_Dsminus_TRUEP_Z);
   fChain->SetBranchAddress("Dsminus_TRUEPT", &Dsminus_TRUEPT, &b_Dsminus_TRUEPT);
   fChain->SetBranchAddress("Dsminus_TRUEORIGINVERTEX_X", &Dsminus_TRUEORIGINVERTEX_X, &b_Dsminus_TRUEORIGINVERTEX_X);
   fChain->SetBranchAddress("Dsminus_TRUEORIGINVERTEX_Y", &Dsminus_TRUEORIGINVERTEX_Y, &b_Dsminus_TRUEORIGINVERTEX_Y);
   fChain->SetBranchAddress("Dsminus_TRUEORIGINVERTEX_Z", &Dsminus_TRUEORIGINVERTEX_Z, &b_Dsminus_TRUEORIGINVERTEX_Z);
   fChain->SetBranchAddress("Dsminus_TRUEENDVERTEX_X", &Dsminus_TRUEENDVERTEX_X, &b_Dsminus_TRUEENDVERTEX_X);
   fChain->SetBranchAddress("Dsminus_TRUEENDVERTEX_Y", &Dsminus_TRUEENDVERTEX_Y, &b_Dsminus_TRUEENDVERTEX_Y);
   fChain->SetBranchAddress("Dsminus_TRUEENDVERTEX_Z", &Dsminus_TRUEENDVERTEX_Z, &b_Dsminus_TRUEENDVERTEX_Z);
   fChain->SetBranchAddress("Dsminus_TRUEISSTABLE", &Dsminus_TRUEISSTABLE, &b_Dsminus_TRUEISSTABLE);
   fChain->SetBranchAddress("Dsminus_TRUETAU", &Dsminus_TRUETAU, &b_Dsminus_TRUETAU);
   fChain->SetBranchAddress("Dsminus_ID", &Dsminus_ID, &b_Dsminus_ID);
   fChain->SetBranchAddress("Dsminus_TAU", &Dsminus_TAU, &b_Dsminus_TAU);
   fChain->SetBranchAddress("Dsminus_TAUERR", &Dsminus_TAUERR, &b_Dsminus_TAUERR);
   fChain->SetBranchAddress("Dsminus_TAUCHI2", &Dsminus_TAUCHI2, &b_Dsminus_TAUCHI2);
   fChain->SetBranchAddress("Dsminus_L0Global_Dec", &Dsminus_L0Global_Dec, &b_Dsminus_L0Global_Dec);
   fChain->SetBranchAddress("Dsminus_L0Global_TIS", &Dsminus_L0Global_TIS, &b_Dsminus_L0Global_TIS);
   fChain->SetBranchAddress("Dsminus_L0Global_TOS", &Dsminus_L0Global_TOS, &b_Dsminus_L0Global_TOS);
   fChain->SetBranchAddress("Dsminus_Hlt1Global_Dec", &Dsminus_Hlt1Global_Dec, &b_Dsminus_Hlt1Global_Dec);
   fChain->SetBranchAddress("Dsminus_Hlt1Global_TIS", &Dsminus_Hlt1Global_TIS, &b_Dsminus_Hlt1Global_TIS);
   fChain->SetBranchAddress("Dsminus_Hlt1Global_TOS", &Dsminus_Hlt1Global_TOS, &b_Dsminus_Hlt1Global_TOS);
   fChain->SetBranchAddress("Dsminus_Hlt1Phys_Dec", &Dsminus_Hlt1Phys_Dec, &b_Dsminus_Hlt1Phys_Dec);
   fChain->SetBranchAddress("Dsminus_Hlt1Phys_TIS", &Dsminus_Hlt1Phys_TIS, &b_Dsminus_Hlt1Phys_TIS);
   fChain->SetBranchAddress("Dsminus_Hlt1Phys_TOS", &Dsminus_Hlt1Phys_TOS, &b_Dsminus_Hlt1Phys_TOS);
   fChain->SetBranchAddress("Dsminus_Hlt2Global_Dec", &Dsminus_Hlt2Global_Dec, &b_Dsminus_Hlt2Global_Dec);
   fChain->SetBranchAddress("Dsminus_Hlt2Global_TIS", &Dsminus_Hlt2Global_TIS, &b_Dsminus_Hlt2Global_TIS);
   fChain->SetBranchAddress("Dsminus_Hlt2Global_TOS", &Dsminus_Hlt2Global_TOS, &b_Dsminus_Hlt2Global_TOS);
   fChain->SetBranchAddress("Dsminus_Hlt2Phys_Dec", &Dsminus_Hlt2Phys_Dec, &b_Dsminus_Hlt2Phys_Dec);
   fChain->SetBranchAddress("Dsminus_Hlt2Phys_TIS", &Dsminus_Hlt2Phys_TIS, &b_Dsminus_Hlt2Phys_TIS);
   fChain->SetBranchAddress("Dsminus_Hlt2Phys_TOS", &Dsminus_Hlt2Phys_TOS, &b_Dsminus_Hlt2Phys_TOS);
   fChain->SetBranchAddress("Dsminus_L0HadronDecision_Dec", &Dsminus_L0HadronDecision_Dec, &b_Dsminus_L0HadronDecision_Dec);
   fChain->SetBranchAddress("Dsminus_L0HadronDecision_TIS", &Dsminus_L0HadronDecision_TIS, &b_Dsminus_L0HadronDecision_TIS);
   fChain->SetBranchAddress("Dsminus_L0HadronDecision_TOS", &Dsminus_L0HadronDecision_TOS, &b_Dsminus_L0HadronDecision_TOS);
   fChain->SetBranchAddress("Dsminus_L0ElectronDecision_Dec", &Dsminus_L0ElectronDecision_Dec, &b_Dsminus_L0ElectronDecision_Dec);
   fChain->SetBranchAddress("Dsminus_L0ElectronDecision_TIS", &Dsminus_L0ElectronDecision_TIS, &b_Dsminus_L0ElectronDecision_TIS);
   fChain->SetBranchAddress("Dsminus_L0ElectronDecision_TOS", &Dsminus_L0ElectronDecision_TOS, &b_Dsminus_L0ElectronDecision_TOS);
   fChain->SetBranchAddress("Dsminus_L0ElectronHiDecision_Dec", &Dsminus_L0ElectronHiDecision_Dec, &b_Dsminus_L0ElectronHiDecision_Dec);
   fChain->SetBranchAddress("Dsminus_L0ElectronHiDecision_TIS", &Dsminus_L0ElectronHiDecision_TIS, &b_Dsminus_L0ElectronHiDecision_TIS);
   fChain->SetBranchAddress("Dsminus_L0ElectronHiDecision_TOS", &Dsminus_L0ElectronHiDecision_TOS, &b_Dsminus_L0ElectronHiDecision_TOS);
   fChain->SetBranchAddress("Dsminus_L0MuonDecision_Dec", &Dsminus_L0MuonDecision_Dec, &b_Dsminus_L0MuonDecision_Dec);
   fChain->SetBranchAddress("Dsminus_L0MuonDecision_TIS", &Dsminus_L0MuonDecision_TIS, &b_Dsminus_L0MuonDecision_TIS);
   fChain->SetBranchAddress("Dsminus_L0MuonDecision_TOS", &Dsminus_L0MuonDecision_TOS, &b_Dsminus_L0MuonDecision_TOS);
   fChain->SetBranchAddress("Dsminus_L0DiMuonDecision_Dec", &Dsminus_L0DiMuonDecision_Dec, &b_Dsminus_L0DiMuonDecision_Dec);
   fChain->SetBranchAddress("Dsminus_L0DiMuonDecision_TIS", &Dsminus_L0DiMuonDecision_TIS, &b_Dsminus_L0DiMuonDecision_TIS);
   fChain->SetBranchAddress("Dsminus_L0DiMuonDecision_TOS", &Dsminus_L0DiMuonDecision_TOS, &b_Dsminus_L0DiMuonDecision_TOS);
   fChain->SetBranchAddress("Dsminus_L0MuonHighDecision_Dec", &Dsminus_L0MuonHighDecision_Dec, &b_Dsminus_L0MuonHighDecision_Dec);
   fChain->SetBranchAddress("Dsminus_L0MuonHighDecision_TIS", &Dsminus_L0MuonHighDecision_TIS, &b_Dsminus_L0MuonHighDecision_TIS);
   fChain->SetBranchAddress("Dsminus_L0MuonHighDecision_TOS", &Dsminus_L0MuonHighDecision_TOS, &b_Dsminus_L0MuonHighDecision_TOS);
   fChain->SetBranchAddress("Dsminus_Hlt1TrackMVADecision_Dec", &Dsminus_Hlt1TrackMVADecision_Dec, &b_Dsminus_Hlt1TrackMVADecision_Dec);
   fChain->SetBranchAddress("Dsminus_Hlt1TrackMVADecision_TIS", &Dsminus_Hlt1TrackMVADecision_TIS, &b_Dsminus_Hlt1TrackMVADecision_TIS);
   fChain->SetBranchAddress("Dsminus_Hlt1TrackMVADecision_TOS", &Dsminus_Hlt1TrackMVADecision_TOS, &b_Dsminus_Hlt1TrackMVADecision_TOS);
   fChain->SetBranchAddress("Dsminus_Hlt1TwoTrackMVADecision_Dec", &Dsminus_Hlt1TwoTrackMVADecision_Dec, &b_Dsminus_Hlt1TwoTrackMVADecision_Dec);
   fChain->SetBranchAddress("Dsminus_Hlt1TwoTrackMVADecision_TIS", &Dsminus_Hlt1TwoTrackMVADecision_TIS, &b_Dsminus_Hlt1TwoTrackMVADecision_TIS);
   fChain->SetBranchAddress("Dsminus_Hlt1TwoTrackMVADecision_TOS", &Dsminus_Hlt1TwoTrackMVADecision_TOS, &b_Dsminus_Hlt1TwoTrackMVADecision_TOS);
   fChain->SetBranchAddress("Dsminus_Hlt1GlobalDecision_Dec", &Dsminus_Hlt1GlobalDecision_Dec, &b_Dsminus_Hlt1GlobalDecision_Dec);
   fChain->SetBranchAddress("Dsminus_Hlt1GlobalDecision_TIS", &Dsminus_Hlt1GlobalDecision_TIS, &b_Dsminus_Hlt1GlobalDecision_TIS);
   fChain->SetBranchAddress("Dsminus_Hlt1GlobalDecision_TOS", &Dsminus_Hlt1GlobalDecision_TOS, &b_Dsminus_Hlt1GlobalDecision_TOS);
   fChain->SetBranchAddress("Dsminus_Hlt1TrackAllL0Decision_Dec", &Dsminus_Hlt1TrackAllL0Decision_Dec, &b_Dsminus_Hlt1TrackAllL0Decision_Dec);
   fChain->SetBranchAddress("Dsminus_Hlt1TrackAllL0Decision_TIS", &Dsminus_Hlt1TrackAllL0Decision_TIS, &b_Dsminus_Hlt1TrackAllL0Decision_TIS);
   fChain->SetBranchAddress("Dsminus_Hlt1TrackAllL0Decision_TOS", &Dsminus_Hlt1TrackAllL0Decision_TOS, &b_Dsminus_Hlt1TrackAllL0Decision_TOS);
   fChain->SetBranchAddress("Dsminus_Hlt2Topo2BodyBBDTDecision_Dec", &Dsminus_Hlt2Topo2BodyBBDTDecision_Dec, &b_Dsminus_Hlt2Topo2BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("Dsminus_Hlt2Topo2BodyBBDTDecision_TIS", &Dsminus_Hlt2Topo2BodyBBDTDecision_TIS, &b_Dsminus_Hlt2Topo2BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("Dsminus_Hlt2Topo2BodyBBDTDecision_TOS", &Dsminus_Hlt2Topo2BodyBBDTDecision_TOS, &b_Dsminus_Hlt2Topo2BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("Dsminus_Hlt2Topo3BodyBBDTDecision_Dec", &Dsminus_Hlt2Topo3BodyBBDTDecision_Dec, &b_Dsminus_Hlt2Topo3BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("Dsminus_Hlt2Topo3BodyBBDTDecision_TIS", &Dsminus_Hlt2Topo3BodyBBDTDecision_TIS, &b_Dsminus_Hlt2Topo3BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("Dsminus_Hlt2Topo3BodyBBDTDecision_TOS", &Dsminus_Hlt2Topo3BodyBBDTDecision_TOS, &b_Dsminus_Hlt2Topo3BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("Dsminus_Hlt2Topo4BodyBBDTDecision_Dec", &Dsminus_Hlt2Topo4BodyBBDTDecision_Dec, &b_Dsminus_Hlt2Topo4BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("Dsminus_Hlt2Topo4BodyBBDTDecision_TIS", &Dsminus_Hlt2Topo4BodyBBDTDecision_TIS, &b_Dsminus_Hlt2Topo4BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("Dsminus_Hlt2Topo4BodyBBDTDecision_TOS", &Dsminus_Hlt2Topo4BodyBBDTDecision_TOS, &b_Dsminus_Hlt2Topo4BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("Dsminus_Hlt2BHadB02PpPpPmPmDecision_Dec", &Dsminus_Hlt2BHadB02PpPpPmPmDecision_Dec, &b_Dsminus_Hlt2BHadB02PpPpPmPmDecision_Dec);
   fChain->SetBranchAddress("Dsminus_Hlt2BHadB02PpPpPmPmDecision_TIS", &Dsminus_Hlt2BHadB02PpPpPmPmDecision_TIS, &b_Dsminus_Hlt2BHadB02PpPpPmPmDecision_TIS);
   fChain->SetBranchAddress("Dsminus_Hlt2BHadB02PpPpPmPmDecision_TOS", &Dsminus_Hlt2BHadB02PpPpPmPmDecision_TOS, &b_Dsminus_Hlt2BHadB02PpPpPmPmDecision_TOS);
   fChain->SetBranchAddress("Dsminus_Hlt2Topo2BodyDecision_Dec", &Dsminus_Hlt2Topo2BodyDecision_Dec, &b_Dsminus_Hlt2Topo2BodyDecision_Dec);
   fChain->SetBranchAddress("Dsminus_Hlt2Topo2BodyDecision_TIS", &Dsminus_Hlt2Topo2BodyDecision_TIS, &b_Dsminus_Hlt2Topo2BodyDecision_TIS);
   fChain->SetBranchAddress("Dsminus_Hlt2Topo2BodyDecision_TOS", &Dsminus_Hlt2Topo2BodyDecision_TOS, &b_Dsminus_Hlt2Topo2BodyDecision_TOS);
   fChain->SetBranchAddress("Dsminus_Hlt2Topo3BodyDecision_Dec", &Dsminus_Hlt2Topo3BodyDecision_Dec, &b_Dsminus_Hlt2Topo3BodyDecision_Dec);
   fChain->SetBranchAddress("Dsminus_Hlt2Topo3BodyDecision_TIS", &Dsminus_Hlt2Topo3BodyDecision_TIS, &b_Dsminus_Hlt2Topo3BodyDecision_TIS);
   fChain->SetBranchAddress("Dsminus_Hlt2Topo3BodyDecision_TOS", &Dsminus_Hlt2Topo3BodyDecision_TOS, &b_Dsminus_Hlt2Topo3BodyDecision_TOS);
   fChain->SetBranchAddress("Dsminus_Hlt2Topo4BodyDecision_Dec", &Dsminus_Hlt2Topo4BodyDecision_Dec, &b_Dsminus_Hlt2Topo4BodyDecision_Dec);
   fChain->SetBranchAddress("Dsminus_Hlt2Topo4BodyDecision_TIS", &Dsminus_Hlt2Topo4BodyDecision_TIS, &b_Dsminus_Hlt2Topo4BodyDecision_TIS);
   fChain->SetBranchAddress("Dsminus_Hlt2Topo4BodyDecision_TOS", &Dsminus_Hlt2Topo4BodyDecision_TOS, &b_Dsminus_Hlt2Topo4BodyDecision_TOS);
   fChain->SetBranchAddress("Dsminus_Hlt2GlobalDecision_Dec", &Dsminus_Hlt2GlobalDecision_Dec, &b_Dsminus_Hlt2GlobalDecision_Dec);
   fChain->SetBranchAddress("Dsminus_Hlt2GlobalDecision_TIS", &Dsminus_Hlt2GlobalDecision_TIS, &b_Dsminus_Hlt2GlobalDecision_TIS);
   fChain->SetBranchAddress("Dsminus_Hlt2GlobalDecision_TOS", &Dsminus_Hlt2GlobalDecision_TOS, &b_Dsminus_Hlt2GlobalDecision_TOS);
   fChain->SetBranchAddress("Dsminus_X", &Dsminus_X, &b_Dsminus_X);
   fChain->SetBranchAddress("Dsminus_Y", &Dsminus_Y, &b_Dsminus_Y);
   fChain->SetBranchAddress("Kplus_Dsminus_ETA", &Kplus_Dsminus_ETA, &b_Kplus_Dsminus_ETA);
   fChain->SetBranchAddress("Kplus_Dsminus_MinIPCHI2", &Kplus_Dsminus_MinIPCHI2, &b_Kplus_Dsminus_MinIPCHI2);
   fChain->SetBranchAddress("Kplus_Dsminus_MC12TuneV2_ProbNNe", &Kplus_Dsminus_MC12TuneV2_ProbNNe, &b_Kplus_Dsminus_MC12TuneV2_ProbNNe);
   fChain->SetBranchAddress("Kplus_Dsminus_MC12TuneV2_ProbNNmu", &Kplus_Dsminus_MC12TuneV2_ProbNNmu, &b_Kplus_Dsminus_MC12TuneV2_ProbNNmu);
   fChain->SetBranchAddress("Kplus_Dsminus_MC12TuneV2_ProbNNpi", &Kplus_Dsminus_MC12TuneV2_ProbNNpi, &b_Kplus_Dsminus_MC12TuneV2_ProbNNpi);
   fChain->SetBranchAddress("Kplus_Dsminus_MC12TuneV2_ProbNNk", &Kplus_Dsminus_MC12TuneV2_ProbNNk, &b_Kplus_Dsminus_MC12TuneV2_ProbNNk);
   fChain->SetBranchAddress("Kplus_Dsminus_MC12TuneV2_ProbNNp", &Kplus_Dsminus_MC12TuneV2_ProbNNp, &b_Kplus_Dsminus_MC12TuneV2_ProbNNp);
   fChain->SetBranchAddress("Kplus_Dsminus_MC12TuneV2_ProbNNghost", &Kplus_Dsminus_MC12TuneV2_ProbNNghost, &b_Kplus_Dsminus_MC12TuneV2_ProbNNghost);
   fChain->SetBranchAddress("Kplus_Dsminus_MC12TuneV3_ProbNNe", &Kplus_Dsminus_MC12TuneV3_ProbNNe, &b_Kplus_Dsminus_MC12TuneV3_ProbNNe);
   fChain->SetBranchAddress("Kplus_Dsminus_MC12TuneV3_ProbNNmu", &Kplus_Dsminus_MC12TuneV3_ProbNNmu, &b_Kplus_Dsminus_MC12TuneV3_ProbNNmu);
   fChain->SetBranchAddress("Kplus_Dsminus_MC12TuneV3_ProbNNpi", &Kplus_Dsminus_MC12TuneV3_ProbNNpi, &b_Kplus_Dsminus_MC12TuneV3_ProbNNpi);
   fChain->SetBranchAddress("Kplus_Dsminus_MC12TuneV3_ProbNNk", &Kplus_Dsminus_MC12TuneV3_ProbNNk, &b_Kplus_Dsminus_MC12TuneV3_ProbNNk);
   fChain->SetBranchAddress("Kplus_Dsminus_MC12TuneV3_ProbNNp", &Kplus_Dsminus_MC12TuneV3_ProbNNp, &b_Kplus_Dsminus_MC12TuneV3_ProbNNp);
   fChain->SetBranchAddress("Kplus_Dsminus_MC12TuneV3_ProbNNghost", &Kplus_Dsminus_MC12TuneV3_ProbNNghost, &b_Kplus_Dsminus_MC12TuneV3_ProbNNghost);
   fChain->SetBranchAddress("Kplus_Dsminus_MC12TuneV4_ProbNNe", &Kplus_Dsminus_MC12TuneV4_ProbNNe, &b_Kplus_Dsminus_MC12TuneV4_ProbNNe);
   fChain->SetBranchAddress("Kplus_Dsminus_MC12TuneV4_ProbNNmu", &Kplus_Dsminus_MC12TuneV4_ProbNNmu, &b_Kplus_Dsminus_MC12TuneV4_ProbNNmu);
   fChain->SetBranchAddress("Kplus_Dsminus_MC12TuneV4_ProbNNpi", &Kplus_Dsminus_MC12TuneV4_ProbNNpi, &b_Kplus_Dsminus_MC12TuneV4_ProbNNpi);
   fChain->SetBranchAddress("Kplus_Dsminus_MC12TuneV4_ProbNNk", &Kplus_Dsminus_MC12TuneV4_ProbNNk, &b_Kplus_Dsminus_MC12TuneV4_ProbNNk);
   fChain->SetBranchAddress("Kplus_Dsminus_MC12TuneV4_ProbNNp", &Kplus_Dsminus_MC12TuneV4_ProbNNp, &b_Kplus_Dsminus_MC12TuneV4_ProbNNp);
   fChain->SetBranchAddress("Kplus_Dsminus_MC12TuneV4_ProbNNghost", &Kplus_Dsminus_MC12TuneV4_ProbNNghost, &b_Kplus_Dsminus_MC12TuneV4_ProbNNghost);
   fChain->SetBranchAddress("Kplus_Dsminus_MC15TuneV1_ProbNNe", &Kplus_Dsminus_MC15TuneV1_ProbNNe, &b_Kplus_Dsminus_MC15TuneV1_ProbNNe);
   fChain->SetBranchAddress("Kplus_Dsminus_MC15TuneV1_ProbNNmu", &Kplus_Dsminus_MC15TuneV1_ProbNNmu, &b_Kplus_Dsminus_MC15TuneV1_ProbNNmu);
   fChain->SetBranchAddress("Kplus_Dsminus_MC15TuneV1_ProbNNpi", &Kplus_Dsminus_MC15TuneV1_ProbNNpi, &b_Kplus_Dsminus_MC15TuneV1_ProbNNpi);
   fChain->SetBranchAddress("Kplus_Dsminus_MC15TuneV1_ProbNNk", &Kplus_Dsminus_MC15TuneV1_ProbNNk, &b_Kplus_Dsminus_MC15TuneV1_ProbNNk);
   fChain->SetBranchAddress("Kplus_Dsminus_MC15TuneV1_ProbNNp", &Kplus_Dsminus_MC15TuneV1_ProbNNp, &b_Kplus_Dsminus_MC15TuneV1_ProbNNp);
   fChain->SetBranchAddress("Kplus_Dsminus_MC15TuneV1_ProbNNghost", &Kplus_Dsminus_MC15TuneV1_ProbNNghost, &b_Kplus_Dsminus_MC15TuneV1_ProbNNghost);
   fChain->SetBranchAddress("Kplus_Dsminus_CosTheta", &Kplus_Dsminus_CosTheta, &b_Kplus_Dsminus_CosTheta);
   fChain->SetBranchAddress("Kplus_Dsminus_OWNPV_X", &Kplus_Dsminus_OWNPV_X, &b_Kplus_Dsminus_OWNPV_X);
   fChain->SetBranchAddress("Kplus_Dsminus_OWNPV_Y", &Kplus_Dsminus_OWNPV_Y, &b_Kplus_Dsminus_OWNPV_Y);
   fChain->SetBranchAddress("Kplus_Dsminus_OWNPV_Z", &Kplus_Dsminus_OWNPV_Z, &b_Kplus_Dsminus_OWNPV_Z);
   fChain->SetBranchAddress("Kplus_Dsminus_OWNPV_XERR", &Kplus_Dsminus_OWNPV_XERR, &b_Kplus_Dsminus_OWNPV_XERR);
   fChain->SetBranchAddress("Kplus_Dsminus_OWNPV_YERR", &Kplus_Dsminus_OWNPV_YERR, &b_Kplus_Dsminus_OWNPV_YERR);
   fChain->SetBranchAddress("Kplus_Dsminus_OWNPV_ZERR", &Kplus_Dsminus_OWNPV_ZERR, &b_Kplus_Dsminus_OWNPV_ZERR);
   fChain->SetBranchAddress("Kplus_Dsminus_OWNPV_CHI2", &Kplus_Dsminus_OWNPV_CHI2, &b_Kplus_Dsminus_OWNPV_CHI2);
   fChain->SetBranchAddress("Kplus_Dsminus_OWNPV_NDOF", &Kplus_Dsminus_OWNPV_NDOF, &b_Kplus_Dsminus_OWNPV_NDOF);
   fChain->SetBranchAddress("Kplus_Dsminus_OWNPV_COV_", Kplus_Dsminus_OWNPV_COV_, &b_Kplus_Dsminus_OWNPV_COV_);
   fChain->SetBranchAddress("Kplus_Dsminus_IP_OWNPV", &Kplus_Dsminus_IP_OWNPV, &b_Kplus_Dsminus_IP_OWNPV);
   fChain->SetBranchAddress("Kplus_Dsminus_IPCHI2_OWNPV", &Kplus_Dsminus_IPCHI2_OWNPV, &b_Kplus_Dsminus_IPCHI2_OWNPV);
   fChain->SetBranchAddress("Kplus_Dsminus_ORIVX_X", &Kplus_Dsminus_ORIVX_X, &b_Kplus_Dsminus_ORIVX_X);
   fChain->SetBranchAddress("Kplus_Dsminus_ORIVX_Y", &Kplus_Dsminus_ORIVX_Y, &b_Kplus_Dsminus_ORIVX_Y);
   fChain->SetBranchAddress("Kplus_Dsminus_ORIVX_Z", &Kplus_Dsminus_ORIVX_Z, &b_Kplus_Dsminus_ORIVX_Z);
   fChain->SetBranchAddress("Kplus_Dsminus_ORIVX_XERR", &Kplus_Dsminus_ORIVX_XERR, &b_Kplus_Dsminus_ORIVX_XERR);
   fChain->SetBranchAddress("Kplus_Dsminus_ORIVX_YERR", &Kplus_Dsminus_ORIVX_YERR, &b_Kplus_Dsminus_ORIVX_YERR);
   fChain->SetBranchAddress("Kplus_Dsminus_ORIVX_ZERR", &Kplus_Dsminus_ORIVX_ZERR, &b_Kplus_Dsminus_ORIVX_ZERR);
   fChain->SetBranchAddress("Kplus_Dsminus_ORIVX_CHI2", &Kplus_Dsminus_ORIVX_CHI2, &b_Kplus_Dsminus_ORIVX_CHI2);
   fChain->SetBranchAddress("Kplus_Dsminus_ORIVX_NDOF", &Kplus_Dsminus_ORIVX_NDOF, &b_Kplus_Dsminus_ORIVX_NDOF);
   fChain->SetBranchAddress("Kplus_Dsminus_ORIVX_COV_", Kplus_Dsminus_ORIVX_COV_, &b_Kplus_Dsminus_ORIVX_COV_);
   fChain->SetBranchAddress("Kplus_Dsminus_P", &Kplus_Dsminus_P, &b_Kplus_Dsminus_P);
   fChain->SetBranchAddress("Kplus_Dsminus_PT", &Kplus_Dsminus_PT, &b_Kplus_Dsminus_PT);
   fChain->SetBranchAddress("Kplus_Dsminus_PE", &Kplus_Dsminus_PE, &b_Kplus_Dsminus_PE);
   fChain->SetBranchAddress("Kplus_Dsminus_PX", &Kplus_Dsminus_PX, &b_Kplus_Dsminus_PX);
   fChain->SetBranchAddress("Kplus_Dsminus_PY", &Kplus_Dsminus_PY, &b_Kplus_Dsminus_PY);
   fChain->SetBranchAddress("Kplus_Dsminus_PZ", &Kplus_Dsminus_PZ, &b_Kplus_Dsminus_PZ);
   fChain->SetBranchAddress("Kplus_Dsminus_REFPX", &Kplus_Dsminus_REFPX, &b_Kplus_Dsminus_REFPX);
   fChain->SetBranchAddress("Kplus_Dsminus_REFPY", &Kplus_Dsminus_REFPY, &b_Kplus_Dsminus_REFPY);
   fChain->SetBranchAddress("Kplus_Dsminus_REFPZ", &Kplus_Dsminus_REFPZ, &b_Kplus_Dsminus_REFPZ);
   fChain->SetBranchAddress("Kplus_Dsminus_M", &Kplus_Dsminus_M, &b_Kplus_Dsminus_M);
   fChain->SetBranchAddress("Kplus_Dsminus_AtVtx_PE", &Kplus_Dsminus_AtVtx_PE, &b_Kplus_Dsminus_AtVtx_PE);
   fChain->SetBranchAddress("Kplus_Dsminus_AtVtx_PX", &Kplus_Dsminus_AtVtx_PX, &b_Kplus_Dsminus_AtVtx_PX);
   fChain->SetBranchAddress("Kplus_Dsminus_AtVtx_PY", &Kplus_Dsminus_AtVtx_PY, &b_Kplus_Dsminus_AtVtx_PY);
   fChain->SetBranchAddress("Kplus_Dsminus_AtVtx_PZ", &Kplus_Dsminus_AtVtx_PZ, &b_Kplus_Dsminus_AtVtx_PZ);
   fChain->SetBranchAddress("Kplus_Dsminus_TRUEID", &Kplus_Dsminus_TRUEID, &b_Kplus_Dsminus_TRUEID);
   fChain->SetBranchAddress("Kplus_Dsminus_TRUEP_E", &Kplus_Dsminus_TRUEP_E, &b_Kplus_Dsminus_TRUEP_E);
   fChain->SetBranchAddress("Kplus_Dsminus_TRUEP_X", &Kplus_Dsminus_TRUEP_X, &b_Kplus_Dsminus_TRUEP_X);
   fChain->SetBranchAddress("Kplus_Dsminus_TRUEP_Y", &Kplus_Dsminus_TRUEP_Y, &b_Kplus_Dsminus_TRUEP_Y);
   fChain->SetBranchAddress("Kplus_Dsminus_TRUEP_Z", &Kplus_Dsminus_TRUEP_Z, &b_Kplus_Dsminus_TRUEP_Z);
   fChain->SetBranchAddress("Kplus_Dsminus_TRUEPT", &Kplus_Dsminus_TRUEPT, &b_Kplus_Dsminus_TRUEPT);
   fChain->SetBranchAddress("Kplus_Dsminus_TRUEORIGINVERTEX_X", &Kplus_Dsminus_TRUEORIGINVERTEX_X, &b_Kplus_Dsminus_TRUEORIGINVERTEX_X);
   fChain->SetBranchAddress("Kplus_Dsminus_TRUEORIGINVERTEX_Y", &Kplus_Dsminus_TRUEORIGINVERTEX_Y, &b_Kplus_Dsminus_TRUEORIGINVERTEX_Y);
   fChain->SetBranchAddress("Kplus_Dsminus_TRUEORIGINVERTEX_Z", &Kplus_Dsminus_TRUEORIGINVERTEX_Z, &b_Kplus_Dsminus_TRUEORIGINVERTEX_Z);
   fChain->SetBranchAddress("Kplus_Dsminus_TRUEENDVERTEX_X", &Kplus_Dsminus_TRUEENDVERTEX_X, &b_Kplus_Dsminus_TRUEENDVERTEX_X);
   fChain->SetBranchAddress("Kplus_Dsminus_TRUEENDVERTEX_Y", &Kplus_Dsminus_TRUEENDVERTEX_Y, &b_Kplus_Dsminus_TRUEENDVERTEX_Y);
   fChain->SetBranchAddress("Kplus_Dsminus_TRUEENDVERTEX_Z", &Kplus_Dsminus_TRUEENDVERTEX_Z, &b_Kplus_Dsminus_TRUEENDVERTEX_Z);
   fChain->SetBranchAddress("Kplus_Dsminus_TRUEISSTABLE", &Kplus_Dsminus_TRUEISSTABLE, &b_Kplus_Dsminus_TRUEISSTABLE);
   fChain->SetBranchAddress("Kplus_Dsminus_TRUETAU", &Kplus_Dsminus_TRUETAU, &b_Kplus_Dsminus_TRUETAU);
   fChain->SetBranchAddress("Kplus_Dsminus_ID", &Kplus_Dsminus_ID, &b_Kplus_Dsminus_ID);
   fChain->SetBranchAddress("Kplus_Dsminus_CombDLLMu", &Kplus_Dsminus_CombDLLMu, &b_Kplus_Dsminus_CombDLLMu);
   fChain->SetBranchAddress("Kplus_Dsminus_ProbNNmu", &Kplus_Dsminus_ProbNNmu, &b_Kplus_Dsminus_ProbNNmu);
   fChain->SetBranchAddress("Kplus_Dsminus_ProbNNghost", &Kplus_Dsminus_ProbNNghost, &b_Kplus_Dsminus_ProbNNghost);
   fChain->SetBranchAddress("Kplus_Dsminus_InMuonAcc", &Kplus_Dsminus_InMuonAcc, &b_Kplus_Dsminus_InMuonAcc);
   fChain->SetBranchAddress("Kplus_Dsminus_MuonDist2", &Kplus_Dsminus_MuonDist2, &b_Kplus_Dsminus_MuonDist2);
   fChain->SetBranchAddress("Kplus_Dsminus_regionInM2", &Kplus_Dsminus_regionInM2, &b_Kplus_Dsminus_regionInM2);
   fChain->SetBranchAddress("Kplus_Dsminus_hasMuon", &Kplus_Dsminus_hasMuon, &b_Kplus_Dsminus_hasMuon);
   fChain->SetBranchAddress("Kplus_Dsminus_isMuon", &Kplus_Dsminus_isMuon, &b_Kplus_Dsminus_isMuon);
   fChain->SetBranchAddress("Kplus_Dsminus_isMuonLoose", &Kplus_Dsminus_isMuonLoose, &b_Kplus_Dsminus_isMuonLoose);
   fChain->SetBranchAddress("Kplus_Dsminus_NShared", &Kplus_Dsminus_NShared, &b_Kplus_Dsminus_NShared);
   fChain->SetBranchAddress("Kplus_Dsminus_MuonLLmu", &Kplus_Dsminus_MuonLLmu, &b_Kplus_Dsminus_MuonLLmu);
   fChain->SetBranchAddress("Kplus_Dsminus_MuonLLbg", &Kplus_Dsminus_MuonLLbg, &b_Kplus_Dsminus_MuonLLbg);
   fChain->SetBranchAddress("Kplus_Dsminus_isMuonFromProto", &Kplus_Dsminus_isMuonFromProto, &b_Kplus_Dsminus_isMuonFromProto);
   fChain->SetBranchAddress("Kplus_Dsminus_PIDe", &Kplus_Dsminus_PIDe, &b_Kplus_Dsminus_PIDe);
   fChain->SetBranchAddress("Kplus_Dsminus_PIDmu", &Kplus_Dsminus_PIDmu, &b_Kplus_Dsminus_PIDmu);
   fChain->SetBranchAddress("Kplus_Dsminus_PIDK", &Kplus_Dsminus_PIDK, &b_Kplus_Dsminus_PIDK);
   fChain->SetBranchAddress("Kplus_Dsminus_PIDp", &Kplus_Dsminus_PIDp, &b_Kplus_Dsminus_PIDp);
   fChain->SetBranchAddress("Kplus_Dsminus_PIDd", &Kplus_Dsminus_PIDd, &b_Kplus_Dsminus_PIDd);
   fChain->SetBranchAddress("Kplus_Dsminus_ProbNNe", &Kplus_Dsminus_ProbNNe, &b_Kplus_Dsminus_ProbNNe);
   fChain->SetBranchAddress("Kplus_Dsminus_ProbNNk", &Kplus_Dsminus_ProbNNk, &b_Kplus_Dsminus_ProbNNk);
   fChain->SetBranchAddress("Kplus_Dsminus_ProbNNp", &Kplus_Dsminus_ProbNNp, &b_Kplus_Dsminus_ProbNNp);
   fChain->SetBranchAddress("Kplus_Dsminus_ProbNNpi", &Kplus_Dsminus_ProbNNpi, &b_Kplus_Dsminus_ProbNNpi);
   fChain->SetBranchAddress("Kplus_Dsminus_ProbNNd", &Kplus_Dsminus_ProbNNd, &b_Kplus_Dsminus_ProbNNd);
   fChain->SetBranchAddress("Kplus_Dsminus_hasRich", &Kplus_Dsminus_hasRich, &b_Kplus_Dsminus_hasRich);
   fChain->SetBranchAddress("Kplus_Dsminus_UsedRichAerogel", &Kplus_Dsminus_UsedRichAerogel, &b_Kplus_Dsminus_UsedRichAerogel);
   fChain->SetBranchAddress("Kplus_Dsminus_UsedRich1Gas", &Kplus_Dsminus_UsedRich1Gas, &b_Kplus_Dsminus_UsedRich1Gas);
   fChain->SetBranchAddress("Kplus_Dsminus_UsedRich2Gas", &Kplus_Dsminus_UsedRich2Gas, &b_Kplus_Dsminus_UsedRich2Gas);
   fChain->SetBranchAddress("Kplus_Dsminus_RichAboveElThres", &Kplus_Dsminus_RichAboveElThres, &b_Kplus_Dsminus_RichAboveElThres);
   fChain->SetBranchAddress("Kplus_Dsminus_RichAboveMuThres", &Kplus_Dsminus_RichAboveMuThres, &b_Kplus_Dsminus_RichAboveMuThres);
   fChain->SetBranchAddress("Kplus_Dsminus_RichAbovePiThres", &Kplus_Dsminus_RichAbovePiThres, &b_Kplus_Dsminus_RichAbovePiThres);
   fChain->SetBranchAddress("Kplus_Dsminus_RichAboveKaThres", &Kplus_Dsminus_RichAboveKaThres, &b_Kplus_Dsminus_RichAboveKaThres);
   fChain->SetBranchAddress("Kplus_Dsminus_RichAbovePrThres", &Kplus_Dsminus_RichAbovePrThres, &b_Kplus_Dsminus_RichAbovePrThres);
   fChain->SetBranchAddress("Kplus_Dsminus_hasCalo", &Kplus_Dsminus_hasCalo, &b_Kplus_Dsminus_hasCalo);
   fChain->SetBranchAddress("Kplus_Dsminus_L0Global_Dec", &Kplus_Dsminus_L0Global_Dec, &b_Kplus_Dsminus_L0Global_Dec);
   fChain->SetBranchAddress("Kplus_Dsminus_L0Global_TIS", &Kplus_Dsminus_L0Global_TIS, &b_Kplus_Dsminus_L0Global_TIS);
   fChain->SetBranchAddress("Kplus_Dsminus_L0Global_TOS", &Kplus_Dsminus_L0Global_TOS, &b_Kplus_Dsminus_L0Global_TOS);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt1Global_Dec", &Kplus_Dsminus_Hlt1Global_Dec, &b_Kplus_Dsminus_Hlt1Global_Dec);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt1Global_TIS", &Kplus_Dsminus_Hlt1Global_TIS, &b_Kplus_Dsminus_Hlt1Global_TIS);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt1Global_TOS", &Kplus_Dsminus_Hlt1Global_TOS, &b_Kplus_Dsminus_Hlt1Global_TOS);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt1Phys_Dec", &Kplus_Dsminus_Hlt1Phys_Dec, &b_Kplus_Dsminus_Hlt1Phys_Dec);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt1Phys_TIS", &Kplus_Dsminus_Hlt1Phys_TIS, &b_Kplus_Dsminus_Hlt1Phys_TIS);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt1Phys_TOS", &Kplus_Dsminus_Hlt1Phys_TOS, &b_Kplus_Dsminus_Hlt1Phys_TOS);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt2Global_Dec", &Kplus_Dsminus_Hlt2Global_Dec, &b_Kplus_Dsminus_Hlt2Global_Dec);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt2Global_TIS", &Kplus_Dsminus_Hlt2Global_TIS, &b_Kplus_Dsminus_Hlt2Global_TIS);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt2Global_TOS", &Kplus_Dsminus_Hlt2Global_TOS, &b_Kplus_Dsminus_Hlt2Global_TOS);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt2Phys_Dec", &Kplus_Dsminus_Hlt2Phys_Dec, &b_Kplus_Dsminus_Hlt2Phys_Dec);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt2Phys_TIS", &Kplus_Dsminus_Hlt2Phys_TIS, &b_Kplus_Dsminus_Hlt2Phys_TIS);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt2Phys_TOS", &Kplus_Dsminus_Hlt2Phys_TOS, &b_Kplus_Dsminus_Hlt2Phys_TOS);
   fChain->SetBranchAddress("Kplus_Dsminus_L0HadronDecision_Dec", &Kplus_Dsminus_L0HadronDecision_Dec, &b_Kplus_Dsminus_L0HadronDecision_Dec);
   fChain->SetBranchAddress("Kplus_Dsminus_L0HadronDecision_TIS", &Kplus_Dsminus_L0HadronDecision_TIS, &b_Kplus_Dsminus_L0HadronDecision_TIS);
   fChain->SetBranchAddress("Kplus_Dsminus_L0HadronDecision_TOS", &Kplus_Dsminus_L0HadronDecision_TOS, &b_Kplus_Dsminus_L0HadronDecision_TOS);
   fChain->SetBranchAddress("Kplus_Dsminus_L0ElectronDecision_Dec", &Kplus_Dsminus_L0ElectronDecision_Dec, &b_Kplus_Dsminus_L0ElectronDecision_Dec);
   fChain->SetBranchAddress("Kplus_Dsminus_L0ElectronDecision_TIS", &Kplus_Dsminus_L0ElectronDecision_TIS, &b_Kplus_Dsminus_L0ElectronDecision_TIS);
   fChain->SetBranchAddress("Kplus_Dsminus_L0ElectronDecision_TOS", &Kplus_Dsminus_L0ElectronDecision_TOS, &b_Kplus_Dsminus_L0ElectronDecision_TOS);
   fChain->SetBranchAddress("Kplus_Dsminus_L0ElectronHiDecision_Dec", &Kplus_Dsminus_L0ElectronHiDecision_Dec, &b_Kplus_Dsminus_L0ElectronHiDecision_Dec);
   fChain->SetBranchAddress("Kplus_Dsminus_L0ElectronHiDecision_TIS", &Kplus_Dsminus_L0ElectronHiDecision_TIS, &b_Kplus_Dsminus_L0ElectronHiDecision_TIS);
   fChain->SetBranchAddress("Kplus_Dsminus_L0ElectronHiDecision_TOS", &Kplus_Dsminus_L0ElectronHiDecision_TOS, &b_Kplus_Dsminus_L0ElectronHiDecision_TOS);
   fChain->SetBranchAddress("Kplus_Dsminus_L0MuonDecision_Dec", &Kplus_Dsminus_L0MuonDecision_Dec, &b_Kplus_Dsminus_L0MuonDecision_Dec);
   fChain->SetBranchAddress("Kplus_Dsminus_L0MuonDecision_TIS", &Kplus_Dsminus_L0MuonDecision_TIS, &b_Kplus_Dsminus_L0MuonDecision_TIS);
   fChain->SetBranchAddress("Kplus_Dsminus_L0MuonDecision_TOS", &Kplus_Dsminus_L0MuonDecision_TOS, &b_Kplus_Dsminus_L0MuonDecision_TOS);
   fChain->SetBranchAddress("Kplus_Dsminus_L0DiMuonDecision_Dec", &Kplus_Dsminus_L0DiMuonDecision_Dec, &b_Kplus_Dsminus_L0DiMuonDecision_Dec);
   fChain->SetBranchAddress("Kplus_Dsminus_L0DiMuonDecision_TIS", &Kplus_Dsminus_L0DiMuonDecision_TIS, &b_Kplus_Dsminus_L0DiMuonDecision_TIS);
   fChain->SetBranchAddress("Kplus_Dsminus_L0DiMuonDecision_TOS", &Kplus_Dsminus_L0DiMuonDecision_TOS, &b_Kplus_Dsminus_L0DiMuonDecision_TOS);
   fChain->SetBranchAddress("Kplus_Dsminus_L0MuonHighDecision_Dec", &Kplus_Dsminus_L0MuonHighDecision_Dec, &b_Kplus_Dsminus_L0MuonHighDecision_Dec);
   fChain->SetBranchAddress("Kplus_Dsminus_L0MuonHighDecision_TIS", &Kplus_Dsminus_L0MuonHighDecision_TIS, &b_Kplus_Dsminus_L0MuonHighDecision_TIS);
   fChain->SetBranchAddress("Kplus_Dsminus_L0MuonHighDecision_TOS", &Kplus_Dsminus_L0MuonHighDecision_TOS, &b_Kplus_Dsminus_L0MuonHighDecision_TOS);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt1TrackMVADecision_Dec", &Kplus_Dsminus_Hlt1TrackMVADecision_Dec, &b_Kplus_Dsminus_Hlt1TrackMVADecision_Dec);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt1TrackMVADecision_TIS", &Kplus_Dsminus_Hlt1TrackMVADecision_TIS, &b_Kplus_Dsminus_Hlt1TrackMVADecision_TIS);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt1TrackMVADecision_TOS", &Kplus_Dsminus_Hlt1TrackMVADecision_TOS, &b_Kplus_Dsminus_Hlt1TrackMVADecision_TOS);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt1TwoTrackMVADecision_Dec", &Kplus_Dsminus_Hlt1TwoTrackMVADecision_Dec, &b_Kplus_Dsminus_Hlt1TwoTrackMVADecision_Dec);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt1TwoTrackMVADecision_TIS", &Kplus_Dsminus_Hlt1TwoTrackMVADecision_TIS, &b_Kplus_Dsminus_Hlt1TwoTrackMVADecision_TIS);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt1TwoTrackMVADecision_TOS", &Kplus_Dsminus_Hlt1TwoTrackMVADecision_TOS, &b_Kplus_Dsminus_Hlt1TwoTrackMVADecision_TOS);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt1GlobalDecision_Dec", &Kplus_Dsminus_Hlt1GlobalDecision_Dec, &b_Kplus_Dsminus_Hlt1GlobalDecision_Dec);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt1GlobalDecision_TIS", &Kplus_Dsminus_Hlt1GlobalDecision_TIS, &b_Kplus_Dsminus_Hlt1GlobalDecision_TIS);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt1GlobalDecision_TOS", &Kplus_Dsminus_Hlt1GlobalDecision_TOS, &b_Kplus_Dsminus_Hlt1GlobalDecision_TOS);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt1TrackAllL0Decision_Dec", &Kplus_Dsminus_Hlt1TrackAllL0Decision_Dec, &b_Kplus_Dsminus_Hlt1TrackAllL0Decision_Dec);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt1TrackAllL0Decision_TIS", &Kplus_Dsminus_Hlt1TrackAllL0Decision_TIS, &b_Kplus_Dsminus_Hlt1TrackAllL0Decision_TIS);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt1TrackAllL0Decision_TOS", &Kplus_Dsminus_Hlt1TrackAllL0Decision_TOS, &b_Kplus_Dsminus_Hlt1TrackAllL0Decision_TOS);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt2Topo2BodyBBDTDecision_Dec", &Kplus_Dsminus_Hlt2Topo2BodyBBDTDecision_Dec, &b_Kplus_Dsminus_Hlt2Topo2BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt2Topo2BodyBBDTDecision_TIS", &Kplus_Dsminus_Hlt2Topo2BodyBBDTDecision_TIS, &b_Kplus_Dsminus_Hlt2Topo2BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt2Topo2BodyBBDTDecision_TOS", &Kplus_Dsminus_Hlt2Topo2BodyBBDTDecision_TOS, &b_Kplus_Dsminus_Hlt2Topo2BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt2Topo3BodyBBDTDecision_Dec", &Kplus_Dsminus_Hlt2Topo3BodyBBDTDecision_Dec, &b_Kplus_Dsminus_Hlt2Topo3BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt2Topo3BodyBBDTDecision_TIS", &Kplus_Dsminus_Hlt2Topo3BodyBBDTDecision_TIS, &b_Kplus_Dsminus_Hlt2Topo3BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt2Topo3BodyBBDTDecision_TOS", &Kplus_Dsminus_Hlt2Topo3BodyBBDTDecision_TOS, &b_Kplus_Dsminus_Hlt2Topo3BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt2Topo4BodyBBDTDecision_Dec", &Kplus_Dsminus_Hlt2Topo4BodyBBDTDecision_Dec, &b_Kplus_Dsminus_Hlt2Topo4BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt2Topo4BodyBBDTDecision_TIS", &Kplus_Dsminus_Hlt2Topo4BodyBBDTDecision_TIS, &b_Kplus_Dsminus_Hlt2Topo4BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt2Topo4BodyBBDTDecision_TOS", &Kplus_Dsminus_Hlt2Topo4BodyBBDTDecision_TOS, &b_Kplus_Dsminus_Hlt2Topo4BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_Dec", &Kplus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_Dec, &b_Kplus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_Dec);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_TIS", &Kplus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_TIS, &b_Kplus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_TIS);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_TOS", &Kplus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_TOS, &b_Kplus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_TOS);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt2Topo2BodyDecision_Dec", &Kplus_Dsminus_Hlt2Topo2BodyDecision_Dec, &b_Kplus_Dsminus_Hlt2Topo2BodyDecision_Dec);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt2Topo2BodyDecision_TIS", &Kplus_Dsminus_Hlt2Topo2BodyDecision_TIS, &b_Kplus_Dsminus_Hlt2Topo2BodyDecision_TIS);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt2Topo2BodyDecision_TOS", &Kplus_Dsminus_Hlt2Topo2BodyDecision_TOS, &b_Kplus_Dsminus_Hlt2Topo2BodyDecision_TOS);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt2Topo3BodyDecision_Dec", &Kplus_Dsminus_Hlt2Topo3BodyDecision_Dec, &b_Kplus_Dsminus_Hlt2Topo3BodyDecision_Dec);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt2Topo3BodyDecision_TIS", &Kplus_Dsminus_Hlt2Topo3BodyDecision_TIS, &b_Kplus_Dsminus_Hlt2Topo3BodyDecision_TIS);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt2Topo3BodyDecision_TOS", &Kplus_Dsminus_Hlt2Topo3BodyDecision_TOS, &b_Kplus_Dsminus_Hlt2Topo3BodyDecision_TOS);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt2Topo4BodyDecision_Dec", &Kplus_Dsminus_Hlt2Topo4BodyDecision_Dec, &b_Kplus_Dsminus_Hlt2Topo4BodyDecision_Dec);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt2Topo4BodyDecision_TIS", &Kplus_Dsminus_Hlt2Topo4BodyDecision_TIS, &b_Kplus_Dsminus_Hlt2Topo4BodyDecision_TIS);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt2Topo4BodyDecision_TOS", &Kplus_Dsminus_Hlt2Topo4BodyDecision_TOS, &b_Kplus_Dsminus_Hlt2Topo4BodyDecision_TOS);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt2GlobalDecision_Dec", &Kplus_Dsminus_Hlt2GlobalDecision_Dec, &b_Kplus_Dsminus_Hlt2GlobalDecision_Dec);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt2GlobalDecision_TIS", &Kplus_Dsminus_Hlt2GlobalDecision_TIS, &b_Kplus_Dsminus_Hlt2GlobalDecision_TIS);
   fChain->SetBranchAddress("Kplus_Dsminus_Hlt2GlobalDecision_TOS", &Kplus_Dsminus_Hlt2GlobalDecision_TOS, &b_Kplus_Dsminus_Hlt2GlobalDecision_TOS);
   fChain->SetBranchAddress("Kplus_Dsminus_TRACK_Type", &Kplus_Dsminus_TRACK_Type, &b_Kplus_Dsminus_TRACK_Type);
   fChain->SetBranchAddress("Kplus_Dsminus_TRACK_Key", &Kplus_Dsminus_TRACK_Key, &b_Kplus_Dsminus_TRACK_Key);
   fChain->SetBranchAddress("Kplus_Dsminus_TRACK_CHI2NDOF", &Kplus_Dsminus_TRACK_CHI2NDOF, &b_Kplus_Dsminus_TRACK_CHI2NDOF);
   fChain->SetBranchAddress("Kplus_Dsminus_TRACK_PCHI2", &Kplus_Dsminus_TRACK_PCHI2, &b_Kplus_Dsminus_TRACK_PCHI2);
   fChain->SetBranchAddress("Kplus_Dsminus_TRACK_MatchCHI2", &Kplus_Dsminus_TRACK_MatchCHI2, &b_Kplus_Dsminus_TRACK_MatchCHI2);
   fChain->SetBranchAddress("Kplus_Dsminus_TRACK_GhostProb", &Kplus_Dsminus_TRACK_GhostProb, &b_Kplus_Dsminus_TRACK_GhostProb);
   fChain->SetBranchAddress("Kplus_Dsminus_TRACK_CloneDist", &Kplus_Dsminus_TRACK_CloneDist, &b_Kplus_Dsminus_TRACK_CloneDist);
   fChain->SetBranchAddress("Kplus_Dsminus_TRACK_Likelihood", &Kplus_Dsminus_TRACK_Likelihood, &b_Kplus_Dsminus_TRACK_Likelihood);
   fChain->SetBranchAddress("Kplus_Dsminus_X", &Kplus_Dsminus_X, &b_Kplus_Dsminus_X);
   fChain->SetBranchAddress("Kplus_Dsminus_Y", &Kplus_Dsminus_Y, &b_Kplus_Dsminus_Y);
   fChain->SetBranchAddress("Kminus_Dsminus_ETA", &Kminus_Dsminus_ETA, &b_Kminus_Dsminus_ETA);
   fChain->SetBranchAddress("Kminus_Dsminus_MinIPCHI2", &Kminus_Dsminus_MinIPCHI2, &b_Kminus_Dsminus_MinIPCHI2);
   fChain->SetBranchAddress("Kminus_Dsminus_MC12TuneV2_ProbNNe", &Kminus_Dsminus_MC12TuneV2_ProbNNe, &b_Kminus_Dsminus_MC12TuneV2_ProbNNe);
   fChain->SetBranchAddress("Kminus_Dsminus_MC12TuneV2_ProbNNmu", &Kminus_Dsminus_MC12TuneV2_ProbNNmu, &b_Kminus_Dsminus_MC12TuneV2_ProbNNmu);
   fChain->SetBranchAddress("Kminus_Dsminus_MC12TuneV2_ProbNNpi", &Kminus_Dsminus_MC12TuneV2_ProbNNpi, &b_Kminus_Dsminus_MC12TuneV2_ProbNNpi);
   fChain->SetBranchAddress("Kminus_Dsminus_MC12TuneV2_ProbNNk", &Kminus_Dsminus_MC12TuneV2_ProbNNk, &b_Kminus_Dsminus_MC12TuneV2_ProbNNk);
   fChain->SetBranchAddress("Kminus_Dsminus_MC12TuneV2_ProbNNp", &Kminus_Dsminus_MC12TuneV2_ProbNNp, &b_Kminus_Dsminus_MC12TuneV2_ProbNNp);
   fChain->SetBranchAddress("Kminus_Dsminus_MC12TuneV2_ProbNNghost", &Kminus_Dsminus_MC12TuneV2_ProbNNghost, &b_Kminus_Dsminus_MC12TuneV2_ProbNNghost);
   fChain->SetBranchAddress("Kminus_Dsminus_MC12TuneV3_ProbNNe", &Kminus_Dsminus_MC12TuneV3_ProbNNe, &b_Kminus_Dsminus_MC12TuneV3_ProbNNe);
   fChain->SetBranchAddress("Kminus_Dsminus_MC12TuneV3_ProbNNmu", &Kminus_Dsminus_MC12TuneV3_ProbNNmu, &b_Kminus_Dsminus_MC12TuneV3_ProbNNmu);
   fChain->SetBranchAddress("Kminus_Dsminus_MC12TuneV3_ProbNNpi", &Kminus_Dsminus_MC12TuneV3_ProbNNpi, &b_Kminus_Dsminus_MC12TuneV3_ProbNNpi);
   fChain->SetBranchAddress("Kminus_Dsminus_MC12TuneV3_ProbNNk", &Kminus_Dsminus_MC12TuneV3_ProbNNk, &b_Kminus_Dsminus_MC12TuneV3_ProbNNk);
   fChain->SetBranchAddress("Kminus_Dsminus_MC12TuneV3_ProbNNp", &Kminus_Dsminus_MC12TuneV3_ProbNNp, &b_Kminus_Dsminus_MC12TuneV3_ProbNNp);
   fChain->SetBranchAddress("Kminus_Dsminus_MC12TuneV3_ProbNNghost", &Kminus_Dsminus_MC12TuneV3_ProbNNghost, &b_Kminus_Dsminus_MC12TuneV3_ProbNNghost);
   fChain->SetBranchAddress("Kminus_Dsminus_MC12TuneV4_ProbNNe", &Kminus_Dsminus_MC12TuneV4_ProbNNe, &b_Kminus_Dsminus_MC12TuneV4_ProbNNe);
   fChain->SetBranchAddress("Kminus_Dsminus_MC12TuneV4_ProbNNmu", &Kminus_Dsminus_MC12TuneV4_ProbNNmu, &b_Kminus_Dsminus_MC12TuneV4_ProbNNmu);
   fChain->SetBranchAddress("Kminus_Dsminus_MC12TuneV4_ProbNNpi", &Kminus_Dsminus_MC12TuneV4_ProbNNpi, &b_Kminus_Dsminus_MC12TuneV4_ProbNNpi);
   fChain->SetBranchAddress("Kminus_Dsminus_MC12TuneV4_ProbNNk", &Kminus_Dsminus_MC12TuneV4_ProbNNk, &b_Kminus_Dsminus_MC12TuneV4_ProbNNk);
   fChain->SetBranchAddress("Kminus_Dsminus_MC12TuneV4_ProbNNp", &Kminus_Dsminus_MC12TuneV4_ProbNNp, &b_Kminus_Dsminus_MC12TuneV4_ProbNNp);
   fChain->SetBranchAddress("Kminus_Dsminus_MC12TuneV4_ProbNNghost", &Kminus_Dsminus_MC12TuneV4_ProbNNghost, &b_Kminus_Dsminus_MC12TuneV4_ProbNNghost);
   fChain->SetBranchAddress("Kminus_Dsminus_MC15TuneV1_ProbNNe", &Kminus_Dsminus_MC15TuneV1_ProbNNe, &b_Kminus_Dsminus_MC15TuneV1_ProbNNe);
   fChain->SetBranchAddress("Kminus_Dsminus_MC15TuneV1_ProbNNmu", &Kminus_Dsminus_MC15TuneV1_ProbNNmu, &b_Kminus_Dsminus_MC15TuneV1_ProbNNmu);
   fChain->SetBranchAddress("Kminus_Dsminus_MC15TuneV1_ProbNNpi", &Kminus_Dsminus_MC15TuneV1_ProbNNpi, &b_Kminus_Dsminus_MC15TuneV1_ProbNNpi);
   fChain->SetBranchAddress("Kminus_Dsminus_MC15TuneV1_ProbNNk", &Kminus_Dsminus_MC15TuneV1_ProbNNk, &b_Kminus_Dsminus_MC15TuneV1_ProbNNk);
   fChain->SetBranchAddress("Kminus_Dsminus_MC15TuneV1_ProbNNp", &Kminus_Dsminus_MC15TuneV1_ProbNNp, &b_Kminus_Dsminus_MC15TuneV1_ProbNNp);
   fChain->SetBranchAddress("Kminus_Dsminus_MC15TuneV1_ProbNNghost", &Kminus_Dsminus_MC15TuneV1_ProbNNghost, &b_Kminus_Dsminus_MC15TuneV1_ProbNNghost);
   fChain->SetBranchAddress("Kminus_Dsminus_CosTheta", &Kminus_Dsminus_CosTheta, &b_Kminus_Dsminus_CosTheta);
   fChain->SetBranchAddress("Kminus_Dsminus_OWNPV_X", &Kminus_Dsminus_OWNPV_X, &b_Kminus_Dsminus_OWNPV_X);
   fChain->SetBranchAddress("Kminus_Dsminus_OWNPV_Y", &Kminus_Dsminus_OWNPV_Y, &b_Kminus_Dsminus_OWNPV_Y);
   fChain->SetBranchAddress("Kminus_Dsminus_OWNPV_Z", &Kminus_Dsminus_OWNPV_Z, &b_Kminus_Dsminus_OWNPV_Z);
   fChain->SetBranchAddress("Kminus_Dsminus_OWNPV_XERR", &Kminus_Dsminus_OWNPV_XERR, &b_Kminus_Dsminus_OWNPV_XERR);
   fChain->SetBranchAddress("Kminus_Dsminus_OWNPV_YERR", &Kminus_Dsminus_OWNPV_YERR, &b_Kminus_Dsminus_OWNPV_YERR);
   fChain->SetBranchAddress("Kminus_Dsminus_OWNPV_ZERR", &Kminus_Dsminus_OWNPV_ZERR, &b_Kminus_Dsminus_OWNPV_ZERR);
   fChain->SetBranchAddress("Kminus_Dsminus_OWNPV_CHI2", &Kminus_Dsminus_OWNPV_CHI2, &b_Kminus_Dsminus_OWNPV_CHI2);
   fChain->SetBranchAddress("Kminus_Dsminus_OWNPV_NDOF", &Kminus_Dsminus_OWNPV_NDOF, &b_Kminus_Dsminus_OWNPV_NDOF);
   fChain->SetBranchAddress("Kminus_Dsminus_OWNPV_COV_", Kminus_Dsminus_OWNPV_COV_, &b_Kminus_Dsminus_OWNPV_COV_);
   fChain->SetBranchAddress("Kminus_Dsminus_IP_OWNPV", &Kminus_Dsminus_IP_OWNPV, &b_Kminus_Dsminus_IP_OWNPV);
   fChain->SetBranchAddress("Kminus_Dsminus_IPCHI2_OWNPV", &Kminus_Dsminus_IPCHI2_OWNPV, &b_Kminus_Dsminus_IPCHI2_OWNPV);
   fChain->SetBranchAddress("Kminus_Dsminus_ORIVX_X", &Kminus_Dsminus_ORIVX_X, &b_Kminus_Dsminus_ORIVX_X);
   fChain->SetBranchAddress("Kminus_Dsminus_ORIVX_Y", &Kminus_Dsminus_ORIVX_Y, &b_Kminus_Dsminus_ORIVX_Y);
   fChain->SetBranchAddress("Kminus_Dsminus_ORIVX_Z", &Kminus_Dsminus_ORIVX_Z, &b_Kminus_Dsminus_ORIVX_Z);
   fChain->SetBranchAddress("Kminus_Dsminus_ORIVX_XERR", &Kminus_Dsminus_ORIVX_XERR, &b_Kminus_Dsminus_ORIVX_XERR);
   fChain->SetBranchAddress("Kminus_Dsminus_ORIVX_YERR", &Kminus_Dsminus_ORIVX_YERR, &b_Kminus_Dsminus_ORIVX_YERR);
   fChain->SetBranchAddress("Kminus_Dsminus_ORIVX_ZERR", &Kminus_Dsminus_ORIVX_ZERR, &b_Kminus_Dsminus_ORIVX_ZERR);
   fChain->SetBranchAddress("Kminus_Dsminus_ORIVX_CHI2", &Kminus_Dsminus_ORIVX_CHI2, &b_Kminus_Dsminus_ORIVX_CHI2);
   fChain->SetBranchAddress("Kminus_Dsminus_ORIVX_NDOF", &Kminus_Dsminus_ORIVX_NDOF, &b_Kminus_Dsminus_ORIVX_NDOF);
   fChain->SetBranchAddress("Kminus_Dsminus_ORIVX_COV_", Kminus_Dsminus_ORIVX_COV_, &b_Kminus_Dsminus_ORIVX_COV_);
   fChain->SetBranchAddress("Kminus_Dsminus_P", &Kminus_Dsminus_P, &b_Kminus_Dsminus_P);
   fChain->SetBranchAddress("Kminus_Dsminus_PT", &Kminus_Dsminus_PT, &b_Kminus_Dsminus_PT);
   fChain->SetBranchAddress("Kminus_Dsminus_PE", &Kminus_Dsminus_PE, &b_Kminus_Dsminus_PE);
   fChain->SetBranchAddress("Kminus_Dsminus_PX", &Kminus_Dsminus_PX, &b_Kminus_Dsminus_PX);
   fChain->SetBranchAddress("Kminus_Dsminus_PY", &Kminus_Dsminus_PY, &b_Kminus_Dsminus_PY);
   fChain->SetBranchAddress("Kminus_Dsminus_PZ", &Kminus_Dsminus_PZ, &b_Kminus_Dsminus_PZ);
   fChain->SetBranchAddress("Kminus_Dsminus_REFPX", &Kminus_Dsminus_REFPX, &b_Kminus_Dsminus_REFPX);
   fChain->SetBranchAddress("Kminus_Dsminus_REFPY", &Kminus_Dsminus_REFPY, &b_Kminus_Dsminus_REFPY);
   fChain->SetBranchAddress("Kminus_Dsminus_REFPZ", &Kminus_Dsminus_REFPZ, &b_Kminus_Dsminus_REFPZ);
   fChain->SetBranchAddress("Kminus_Dsminus_M", &Kminus_Dsminus_M, &b_Kminus_Dsminus_M);
   fChain->SetBranchAddress("Kminus_Dsminus_AtVtx_PE", &Kminus_Dsminus_AtVtx_PE, &b_Kminus_Dsminus_AtVtx_PE);
   fChain->SetBranchAddress("Kminus_Dsminus_AtVtx_PX", &Kminus_Dsminus_AtVtx_PX, &b_Kminus_Dsminus_AtVtx_PX);
   fChain->SetBranchAddress("Kminus_Dsminus_AtVtx_PY", &Kminus_Dsminus_AtVtx_PY, &b_Kminus_Dsminus_AtVtx_PY);
   fChain->SetBranchAddress("Kminus_Dsminus_AtVtx_PZ", &Kminus_Dsminus_AtVtx_PZ, &b_Kminus_Dsminus_AtVtx_PZ);
   fChain->SetBranchAddress("Kminus_Dsminus_TRUEID", &Kminus_Dsminus_TRUEID, &b_Kminus_Dsminus_TRUEID);
   fChain->SetBranchAddress("Kminus_Dsminus_TRUEP_E", &Kminus_Dsminus_TRUEP_E, &b_Kminus_Dsminus_TRUEP_E);
   fChain->SetBranchAddress("Kminus_Dsminus_TRUEP_X", &Kminus_Dsminus_TRUEP_X, &b_Kminus_Dsminus_TRUEP_X);
   fChain->SetBranchAddress("Kminus_Dsminus_TRUEP_Y", &Kminus_Dsminus_TRUEP_Y, &b_Kminus_Dsminus_TRUEP_Y);
   fChain->SetBranchAddress("Kminus_Dsminus_TRUEP_Z", &Kminus_Dsminus_TRUEP_Z, &b_Kminus_Dsminus_TRUEP_Z);
   fChain->SetBranchAddress("Kminus_Dsminus_TRUEPT", &Kminus_Dsminus_TRUEPT, &b_Kminus_Dsminus_TRUEPT);
   fChain->SetBranchAddress("Kminus_Dsminus_TRUEORIGINVERTEX_X", &Kminus_Dsminus_TRUEORIGINVERTEX_X, &b_Kminus_Dsminus_TRUEORIGINVERTEX_X);
   fChain->SetBranchAddress("Kminus_Dsminus_TRUEORIGINVERTEX_Y", &Kminus_Dsminus_TRUEORIGINVERTEX_Y, &b_Kminus_Dsminus_TRUEORIGINVERTEX_Y);
   fChain->SetBranchAddress("Kminus_Dsminus_TRUEORIGINVERTEX_Z", &Kminus_Dsminus_TRUEORIGINVERTEX_Z, &b_Kminus_Dsminus_TRUEORIGINVERTEX_Z);
   fChain->SetBranchAddress("Kminus_Dsminus_TRUEENDVERTEX_X", &Kminus_Dsminus_TRUEENDVERTEX_X, &b_Kminus_Dsminus_TRUEENDVERTEX_X);
   fChain->SetBranchAddress("Kminus_Dsminus_TRUEENDVERTEX_Y", &Kminus_Dsminus_TRUEENDVERTEX_Y, &b_Kminus_Dsminus_TRUEENDVERTEX_Y);
   fChain->SetBranchAddress("Kminus_Dsminus_TRUEENDVERTEX_Z", &Kminus_Dsminus_TRUEENDVERTEX_Z, &b_Kminus_Dsminus_TRUEENDVERTEX_Z);
   fChain->SetBranchAddress("Kminus_Dsminus_TRUEISSTABLE", &Kminus_Dsminus_TRUEISSTABLE, &b_Kminus_Dsminus_TRUEISSTABLE);
   fChain->SetBranchAddress("Kminus_Dsminus_TRUETAU", &Kminus_Dsminus_TRUETAU, &b_Kminus_Dsminus_TRUETAU);
   fChain->SetBranchAddress("Kminus_Dsminus_ID", &Kminus_Dsminus_ID, &b_Kminus_Dsminus_ID);
   fChain->SetBranchAddress("Kminus_Dsminus_CombDLLMu", &Kminus_Dsminus_CombDLLMu, &b_Kminus_Dsminus_CombDLLMu);
   fChain->SetBranchAddress("Kminus_Dsminus_ProbNNmu", &Kminus_Dsminus_ProbNNmu, &b_Kminus_Dsminus_ProbNNmu);
   fChain->SetBranchAddress("Kminus_Dsminus_ProbNNghost", &Kminus_Dsminus_ProbNNghost, &b_Kminus_Dsminus_ProbNNghost);
   fChain->SetBranchAddress("Kminus_Dsminus_InMuonAcc", &Kminus_Dsminus_InMuonAcc, &b_Kminus_Dsminus_InMuonAcc);
   fChain->SetBranchAddress("Kminus_Dsminus_MuonDist2", &Kminus_Dsminus_MuonDist2, &b_Kminus_Dsminus_MuonDist2);
   fChain->SetBranchAddress("Kminus_Dsminus_regionInM2", &Kminus_Dsminus_regionInM2, &b_Kminus_Dsminus_regionInM2);
   fChain->SetBranchAddress("Kminus_Dsminus_hasMuon", &Kminus_Dsminus_hasMuon, &b_Kminus_Dsminus_hasMuon);
   fChain->SetBranchAddress("Kminus_Dsminus_isMuon", &Kminus_Dsminus_isMuon, &b_Kminus_Dsminus_isMuon);
   fChain->SetBranchAddress("Kminus_Dsminus_isMuonLoose", &Kminus_Dsminus_isMuonLoose, &b_Kminus_Dsminus_isMuonLoose);
   fChain->SetBranchAddress("Kminus_Dsminus_NShared", &Kminus_Dsminus_NShared, &b_Kminus_Dsminus_NShared);
   fChain->SetBranchAddress("Kminus_Dsminus_MuonLLmu", &Kminus_Dsminus_MuonLLmu, &b_Kminus_Dsminus_MuonLLmu);
   fChain->SetBranchAddress("Kminus_Dsminus_MuonLLbg", &Kminus_Dsminus_MuonLLbg, &b_Kminus_Dsminus_MuonLLbg);
   fChain->SetBranchAddress("Kminus_Dsminus_isMuonFromProto", &Kminus_Dsminus_isMuonFromProto, &b_Kminus_Dsminus_isMuonFromProto);
   fChain->SetBranchAddress("Kminus_Dsminus_PIDe", &Kminus_Dsminus_PIDe, &b_Kminus_Dsminus_PIDe);
   fChain->SetBranchAddress("Kminus_Dsminus_PIDmu", &Kminus_Dsminus_PIDmu, &b_Kminus_Dsminus_PIDmu);
   fChain->SetBranchAddress("Kminus_Dsminus_PIDK", &Kminus_Dsminus_PIDK, &b_Kminus_Dsminus_PIDK);
   fChain->SetBranchAddress("Kminus_Dsminus_PIDp", &Kminus_Dsminus_PIDp, &b_Kminus_Dsminus_PIDp);
   fChain->SetBranchAddress("Kminus_Dsminus_PIDd", &Kminus_Dsminus_PIDd, &b_Kminus_Dsminus_PIDd);
   fChain->SetBranchAddress("Kminus_Dsminus_ProbNNe", &Kminus_Dsminus_ProbNNe, &b_Kminus_Dsminus_ProbNNe);
   fChain->SetBranchAddress("Kminus_Dsminus_ProbNNk", &Kminus_Dsminus_ProbNNk, &b_Kminus_Dsminus_ProbNNk);
   fChain->SetBranchAddress("Kminus_Dsminus_ProbNNp", &Kminus_Dsminus_ProbNNp, &b_Kminus_Dsminus_ProbNNp);
   fChain->SetBranchAddress("Kminus_Dsminus_ProbNNpi", &Kminus_Dsminus_ProbNNpi, &b_Kminus_Dsminus_ProbNNpi);
   fChain->SetBranchAddress("Kminus_Dsminus_ProbNNd", &Kminus_Dsminus_ProbNNd, &b_Kminus_Dsminus_ProbNNd);
   fChain->SetBranchAddress("Kminus_Dsminus_hasRich", &Kminus_Dsminus_hasRich, &b_Kminus_Dsminus_hasRich);
   fChain->SetBranchAddress("Kminus_Dsminus_UsedRichAerogel", &Kminus_Dsminus_UsedRichAerogel, &b_Kminus_Dsminus_UsedRichAerogel);
   fChain->SetBranchAddress("Kminus_Dsminus_UsedRich1Gas", &Kminus_Dsminus_UsedRich1Gas, &b_Kminus_Dsminus_UsedRich1Gas);
   fChain->SetBranchAddress("Kminus_Dsminus_UsedRich2Gas", &Kminus_Dsminus_UsedRich2Gas, &b_Kminus_Dsminus_UsedRich2Gas);
   fChain->SetBranchAddress("Kminus_Dsminus_RichAboveElThres", &Kminus_Dsminus_RichAboveElThres, &b_Kminus_Dsminus_RichAboveElThres);
   fChain->SetBranchAddress("Kminus_Dsminus_RichAboveMuThres", &Kminus_Dsminus_RichAboveMuThres, &b_Kminus_Dsminus_RichAboveMuThres);
   fChain->SetBranchAddress("Kminus_Dsminus_RichAbovePiThres", &Kminus_Dsminus_RichAbovePiThres, &b_Kminus_Dsminus_RichAbovePiThres);
   fChain->SetBranchAddress("Kminus_Dsminus_RichAboveKaThres", &Kminus_Dsminus_RichAboveKaThres, &b_Kminus_Dsminus_RichAboveKaThres);
   fChain->SetBranchAddress("Kminus_Dsminus_RichAbovePrThres", &Kminus_Dsminus_RichAbovePrThres, &b_Kminus_Dsminus_RichAbovePrThres);
   fChain->SetBranchAddress("Kminus_Dsminus_hasCalo", &Kminus_Dsminus_hasCalo, &b_Kminus_Dsminus_hasCalo);
   fChain->SetBranchAddress("Kminus_Dsminus_L0Global_Dec", &Kminus_Dsminus_L0Global_Dec, &b_Kminus_Dsminus_L0Global_Dec);
   fChain->SetBranchAddress("Kminus_Dsminus_L0Global_TIS", &Kminus_Dsminus_L0Global_TIS, &b_Kminus_Dsminus_L0Global_TIS);
   fChain->SetBranchAddress("Kminus_Dsminus_L0Global_TOS", &Kminus_Dsminus_L0Global_TOS, &b_Kminus_Dsminus_L0Global_TOS);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt1Global_Dec", &Kminus_Dsminus_Hlt1Global_Dec, &b_Kminus_Dsminus_Hlt1Global_Dec);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt1Global_TIS", &Kminus_Dsminus_Hlt1Global_TIS, &b_Kminus_Dsminus_Hlt1Global_TIS);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt1Global_TOS", &Kminus_Dsminus_Hlt1Global_TOS, &b_Kminus_Dsminus_Hlt1Global_TOS);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt1Phys_Dec", &Kminus_Dsminus_Hlt1Phys_Dec, &b_Kminus_Dsminus_Hlt1Phys_Dec);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt1Phys_TIS", &Kminus_Dsminus_Hlt1Phys_TIS, &b_Kminus_Dsminus_Hlt1Phys_TIS);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt1Phys_TOS", &Kminus_Dsminus_Hlt1Phys_TOS, &b_Kminus_Dsminus_Hlt1Phys_TOS);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt2Global_Dec", &Kminus_Dsminus_Hlt2Global_Dec, &b_Kminus_Dsminus_Hlt2Global_Dec);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt2Global_TIS", &Kminus_Dsminus_Hlt2Global_TIS, &b_Kminus_Dsminus_Hlt2Global_TIS);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt2Global_TOS", &Kminus_Dsminus_Hlt2Global_TOS, &b_Kminus_Dsminus_Hlt2Global_TOS);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt2Phys_Dec", &Kminus_Dsminus_Hlt2Phys_Dec, &b_Kminus_Dsminus_Hlt2Phys_Dec);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt2Phys_TIS", &Kminus_Dsminus_Hlt2Phys_TIS, &b_Kminus_Dsminus_Hlt2Phys_TIS);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt2Phys_TOS", &Kminus_Dsminus_Hlt2Phys_TOS, &b_Kminus_Dsminus_Hlt2Phys_TOS);
   fChain->SetBranchAddress("Kminus_Dsminus_L0HadronDecision_Dec", &Kminus_Dsminus_L0HadronDecision_Dec, &b_Kminus_Dsminus_L0HadronDecision_Dec);
   fChain->SetBranchAddress("Kminus_Dsminus_L0HadronDecision_TIS", &Kminus_Dsminus_L0HadronDecision_TIS, &b_Kminus_Dsminus_L0HadronDecision_TIS);
   fChain->SetBranchAddress("Kminus_Dsminus_L0HadronDecision_TOS", &Kminus_Dsminus_L0HadronDecision_TOS, &b_Kminus_Dsminus_L0HadronDecision_TOS);
   fChain->SetBranchAddress("Kminus_Dsminus_L0ElectronDecision_Dec", &Kminus_Dsminus_L0ElectronDecision_Dec, &b_Kminus_Dsminus_L0ElectronDecision_Dec);
   fChain->SetBranchAddress("Kminus_Dsminus_L0ElectronDecision_TIS", &Kminus_Dsminus_L0ElectronDecision_TIS, &b_Kminus_Dsminus_L0ElectronDecision_TIS);
   fChain->SetBranchAddress("Kminus_Dsminus_L0ElectronDecision_TOS", &Kminus_Dsminus_L0ElectronDecision_TOS, &b_Kminus_Dsminus_L0ElectronDecision_TOS);
   fChain->SetBranchAddress("Kminus_Dsminus_L0ElectronHiDecision_Dec", &Kminus_Dsminus_L0ElectronHiDecision_Dec, &b_Kminus_Dsminus_L0ElectronHiDecision_Dec);
   fChain->SetBranchAddress("Kminus_Dsminus_L0ElectronHiDecision_TIS", &Kminus_Dsminus_L0ElectronHiDecision_TIS, &b_Kminus_Dsminus_L0ElectronHiDecision_TIS);
   fChain->SetBranchAddress("Kminus_Dsminus_L0ElectronHiDecision_TOS", &Kminus_Dsminus_L0ElectronHiDecision_TOS, &b_Kminus_Dsminus_L0ElectronHiDecision_TOS);
   fChain->SetBranchAddress("Kminus_Dsminus_L0MuonDecision_Dec", &Kminus_Dsminus_L0MuonDecision_Dec, &b_Kminus_Dsminus_L0MuonDecision_Dec);
   fChain->SetBranchAddress("Kminus_Dsminus_L0MuonDecision_TIS", &Kminus_Dsminus_L0MuonDecision_TIS, &b_Kminus_Dsminus_L0MuonDecision_TIS);
   fChain->SetBranchAddress("Kminus_Dsminus_L0MuonDecision_TOS", &Kminus_Dsminus_L0MuonDecision_TOS, &b_Kminus_Dsminus_L0MuonDecision_TOS);
   fChain->SetBranchAddress("Kminus_Dsminus_L0DiMuonDecision_Dec", &Kminus_Dsminus_L0DiMuonDecision_Dec, &b_Kminus_Dsminus_L0DiMuonDecision_Dec);
   fChain->SetBranchAddress("Kminus_Dsminus_L0DiMuonDecision_TIS", &Kminus_Dsminus_L0DiMuonDecision_TIS, &b_Kminus_Dsminus_L0DiMuonDecision_TIS);
   fChain->SetBranchAddress("Kminus_Dsminus_L0DiMuonDecision_TOS", &Kminus_Dsminus_L0DiMuonDecision_TOS, &b_Kminus_Dsminus_L0DiMuonDecision_TOS);
   fChain->SetBranchAddress("Kminus_Dsminus_L0MuonHighDecision_Dec", &Kminus_Dsminus_L0MuonHighDecision_Dec, &b_Kminus_Dsminus_L0MuonHighDecision_Dec);
   fChain->SetBranchAddress("Kminus_Dsminus_L0MuonHighDecision_TIS", &Kminus_Dsminus_L0MuonHighDecision_TIS, &b_Kminus_Dsminus_L0MuonHighDecision_TIS);
   fChain->SetBranchAddress("Kminus_Dsminus_L0MuonHighDecision_TOS", &Kminus_Dsminus_L0MuonHighDecision_TOS, &b_Kminus_Dsminus_L0MuonHighDecision_TOS);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt1TrackMVADecision_Dec", &Kminus_Dsminus_Hlt1TrackMVADecision_Dec, &b_Kminus_Dsminus_Hlt1TrackMVADecision_Dec);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt1TrackMVADecision_TIS", &Kminus_Dsminus_Hlt1TrackMVADecision_TIS, &b_Kminus_Dsminus_Hlt1TrackMVADecision_TIS);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt1TrackMVADecision_TOS", &Kminus_Dsminus_Hlt1TrackMVADecision_TOS, &b_Kminus_Dsminus_Hlt1TrackMVADecision_TOS);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt1TwoTrackMVADecision_Dec", &Kminus_Dsminus_Hlt1TwoTrackMVADecision_Dec, &b_Kminus_Dsminus_Hlt1TwoTrackMVADecision_Dec);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt1TwoTrackMVADecision_TIS", &Kminus_Dsminus_Hlt1TwoTrackMVADecision_TIS, &b_Kminus_Dsminus_Hlt1TwoTrackMVADecision_TIS);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt1TwoTrackMVADecision_TOS", &Kminus_Dsminus_Hlt1TwoTrackMVADecision_TOS, &b_Kminus_Dsminus_Hlt1TwoTrackMVADecision_TOS);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt1GlobalDecision_Dec", &Kminus_Dsminus_Hlt1GlobalDecision_Dec, &b_Kminus_Dsminus_Hlt1GlobalDecision_Dec);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt1GlobalDecision_TIS", &Kminus_Dsminus_Hlt1GlobalDecision_TIS, &b_Kminus_Dsminus_Hlt1GlobalDecision_TIS);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt1GlobalDecision_TOS", &Kminus_Dsminus_Hlt1GlobalDecision_TOS, &b_Kminus_Dsminus_Hlt1GlobalDecision_TOS);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt1TrackAllL0Decision_Dec", &Kminus_Dsminus_Hlt1TrackAllL0Decision_Dec, &b_Kminus_Dsminus_Hlt1TrackAllL0Decision_Dec);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt1TrackAllL0Decision_TIS", &Kminus_Dsminus_Hlt1TrackAllL0Decision_TIS, &b_Kminus_Dsminus_Hlt1TrackAllL0Decision_TIS);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt1TrackAllL0Decision_TOS", &Kminus_Dsminus_Hlt1TrackAllL0Decision_TOS, &b_Kminus_Dsminus_Hlt1TrackAllL0Decision_TOS);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt2Topo2BodyBBDTDecision_Dec", &Kminus_Dsminus_Hlt2Topo2BodyBBDTDecision_Dec, &b_Kminus_Dsminus_Hlt2Topo2BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt2Topo2BodyBBDTDecision_TIS", &Kminus_Dsminus_Hlt2Topo2BodyBBDTDecision_TIS, &b_Kminus_Dsminus_Hlt2Topo2BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt2Topo2BodyBBDTDecision_TOS", &Kminus_Dsminus_Hlt2Topo2BodyBBDTDecision_TOS, &b_Kminus_Dsminus_Hlt2Topo2BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt2Topo3BodyBBDTDecision_Dec", &Kminus_Dsminus_Hlt2Topo3BodyBBDTDecision_Dec, &b_Kminus_Dsminus_Hlt2Topo3BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt2Topo3BodyBBDTDecision_TIS", &Kminus_Dsminus_Hlt2Topo3BodyBBDTDecision_TIS, &b_Kminus_Dsminus_Hlt2Topo3BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt2Topo3BodyBBDTDecision_TOS", &Kminus_Dsminus_Hlt2Topo3BodyBBDTDecision_TOS, &b_Kminus_Dsminus_Hlt2Topo3BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt2Topo4BodyBBDTDecision_Dec", &Kminus_Dsminus_Hlt2Topo4BodyBBDTDecision_Dec, &b_Kminus_Dsminus_Hlt2Topo4BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt2Topo4BodyBBDTDecision_TIS", &Kminus_Dsminus_Hlt2Topo4BodyBBDTDecision_TIS, &b_Kminus_Dsminus_Hlt2Topo4BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt2Topo4BodyBBDTDecision_TOS", &Kminus_Dsminus_Hlt2Topo4BodyBBDTDecision_TOS, &b_Kminus_Dsminus_Hlt2Topo4BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_Dec", &Kminus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_Dec, &b_Kminus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_Dec);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_TIS", &Kminus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_TIS, &b_Kminus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_TIS);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_TOS", &Kminus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_TOS, &b_Kminus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_TOS);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt2Topo2BodyDecision_Dec", &Kminus_Dsminus_Hlt2Topo2BodyDecision_Dec, &b_Kminus_Dsminus_Hlt2Topo2BodyDecision_Dec);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt2Topo2BodyDecision_TIS", &Kminus_Dsminus_Hlt2Topo2BodyDecision_TIS, &b_Kminus_Dsminus_Hlt2Topo2BodyDecision_TIS);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt2Topo2BodyDecision_TOS", &Kminus_Dsminus_Hlt2Topo2BodyDecision_TOS, &b_Kminus_Dsminus_Hlt2Topo2BodyDecision_TOS);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt2Topo3BodyDecision_Dec", &Kminus_Dsminus_Hlt2Topo3BodyDecision_Dec, &b_Kminus_Dsminus_Hlt2Topo3BodyDecision_Dec);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt2Topo3BodyDecision_TIS", &Kminus_Dsminus_Hlt2Topo3BodyDecision_TIS, &b_Kminus_Dsminus_Hlt2Topo3BodyDecision_TIS);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt2Topo3BodyDecision_TOS", &Kminus_Dsminus_Hlt2Topo3BodyDecision_TOS, &b_Kminus_Dsminus_Hlt2Topo3BodyDecision_TOS);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt2Topo4BodyDecision_Dec", &Kminus_Dsminus_Hlt2Topo4BodyDecision_Dec, &b_Kminus_Dsminus_Hlt2Topo4BodyDecision_Dec);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt2Topo4BodyDecision_TIS", &Kminus_Dsminus_Hlt2Topo4BodyDecision_TIS, &b_Kminus_Dsminus_Hlt2Topo4BodyDecision_TIS);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt2Topo4BodyDecision_TOS", &Kminus_Dsminus_Hlt2Topo4BodyDecision_TOS, &b_Kminus_Dsminus_Hlt2Topo4BodyDecision_TOS);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt2GlobalDecision_Dec", &Kminus_Dsminus_Hlt2GlobalDecision_Dec, &b_Kminus_Dsminus_Hlt2GlobalDecision_Dec);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt2GlobalDecision_TIS", &Kminus_Dsminus_Hlt2GlobalDecision_TIS, &b_Kminus_Dsminus_Hlt2GlobalDecision_TIS);
   fChain->SetBranchAddress("Kminus_Dsminus_Hlt2GlobalDecision_TOS", &Kminus_Dsminus_Hlt2GlobalDecision_TOS, &b_Kminus_Dsminus_Hlt2GlobalDecision_TOS);
   fChain->SetBranchAddress("Kminus_Dsminus_TRACK_Type", &Kminus_Dsminus_TRACK_Type, &b_Kminus_Dsminus_TRACK_Type);
   fChain->SetBranchAddress("Kminus_Dsminus_TRACK_Key", &Kminus_Dsminus_TRACK_Key, &b_Kminus_Dsminus_TRACK_Key);
   fChain->SetBranchAddress("Kminus_Dsminus_TRACK_CHI2NDOF", &Kminus_Dsminus_TRACK_CHI2NDOF, &b_Kminus_Dsminus_TRACK_CHI2NDOF);
   fChain->SetBranchAddress("Kminus_Dsminus_TRACK_PCHI2", &Kminus_Dsminus_TRACK_PCHI2, &b_Kminus_Dsminus_TRACK_PCHI2);
   fChain->SetBranchAddress("Kminus_Dsminus_TRACK_MatchCHI2", &Kminus_Dsminus_TRACK_MatchCHI2, &b_Kminus_Dsminus_TRACK_MatchCHI2);
   fChain->SetBranchAddress("Kminus_Dsminus_TRACK_GhostProb", &Kminus_Dsminus_TRACK_GhostProb, &b_Kminus_Dsminus_TRACK_GhostProb);
   fChain->SetBranchAddress("Kminus_Dsminus_TRACK_CloneDist", &Kminus_Dsminus_TRACK_CloneDist, &b_Kminus_Dsminus_TRACK_CloneDist);
   fChain->SetBranchAddress("Kminus_Dsminus_TRACK_Likelihood", &Kminus_Dsminus_TRACK_Likelihood, &b_Kminus_Dsminus_TRACK_Likelihood);
   fChain->SetBranchAddress("Kminus_Dsminus_X", &Kminus_Dsminus_X, &b_Kminus_Dsminus_X);
   fChain->SetBranchAddress("Kminus_Dsminus_Y", &Kminus_Dsminus_Y, &b_Kminus_Dsminus_Y);
   fChain->SetBranchAddress("piminus_Dsminus_ETA", &piminus_Dsminus_ETA, &b_piminus_Dsminus_ETA);
   fChain->SetBranchAddress("piminus_Dsminus_MinIPCHI2", &piminus_Dsminus_MinIPCHI2, &b_piminus_Dsminus_MinIPCHI2);
   fChain->SetBranchAddress("piminus_Dsminus_MC12TuneV2_ProbNNe", &piminus_Dsminus_MC12TuneV2_ProbNNe, &b_piminus_Dsminus_MC12TuneV2_ProbNNe);
   fChain->SetBranchAddress("piminus_Dsminus_MC12TuneV2_ProbNNmu", &piminus_Dsminus_MC12TuneV2_ProbNNmu, &b_piminus_Dsminus_MC12TuneV2_ProbNNmu);
   fChain->SetBranchAddress("piminus_Dsminus_MC12TuneV2_ProbNNpi", &piminus_Dsminus_MC12TuneV2_ProbNNpi, &b_piminus_Dsminus_MC12TuneV2_ProbNNpi);
   fChain->SetBranchAddress("piminus_Dsminus_MC12TuneV2_ProbNNk", &piminus_Dsminus_MC12TuneV2_ProbNNk, &b_piminus_Dsminus_MC12TuneV2_ProbNNk);
   fChain->SetBranchAddress("piminus_Dsminus_MC12TuneV2_ProbNNp", &piminus_Dsminus_MC12TuneV2_ProbNNp, &b_piminus_Dsminus_MC12TuneV2_ProbNNp);
   fChain->SetBranchAddress("piminus_Dsminus_MC12TuneV2_ProbNNghost", &piminus_Dsminus_MC12TuneV2_ProbNNghost, &b_piminus_Dsminus_MC12TuneV2_ProbNNghost);
   fChain->SetBranchAddress("piminus_Dsminus_MC12TuneV3_ProbNNe", &piminus_Dsminus_MC12TuneV3_ProbNNe, &b_piminus_Dsminus_MC12TuneV3_ProbNNe);
   fChain->SetBranchAddress("piminus_Dsminus_MC12TuneV3_ProbNNmu", &piminus_Dsminus_MC12TuneV3_ProbNNmu, &b_piminus_Dsminus_MC12TuneV3_ProbNNmu);
   fChain->SetBranchAddress("piminus_Dsminus_MC12TuneV3_ProbNNpi", &piminus_Dsminus_MC12TuneV3_ProbNNpi, &b_piminus_Dsminus_MC12TuneV3_ProbNNpi);
   fChain->SetBranchAddress("piminus_Dsminus_MC12TuneV3_ProbNNk", &piminus_Dsminus_MC12TuneV3_ProbNNk, &b_piminus_Dsminus_MC12TuneV3_ProbNNk);
   fChain->SetBranchAddress("piminus_Dsminus_MC12TuneV3_ProbNNp", &piminus_Dsminus_MC12TuneV3_ProbNNp, &b_piminus_Dsminus_MC12TuneV3_ProbNNp);
   fChain->SetBranchAddress("piminus_Dsminus_MC12TuneV3_ProbNNghost", &piminus_Dsminus_MC12TuneV3_ProbNNghost, &b_piminus_Dsminus_MC12TuneV3_ProbNNghost);
   fChain->SetBranchAddress("piminus_Dsminus_MC12TuneV4_ProbNNe", &piminus_Dsminus_MC12TuneV4_ProbNNe, &b_piminus_Dsminus_MC12TuneV4_ProbNNe);
   fChain->SetBranchAddress("piminus_Dsminus_MC12TuneV4_ProbNNmu", &piminus_Dsminus_MC12TuneV4_ProbNNmu, &b_piminus_Dsminus_MC12TuneV4_ProbNNmu);
   fChain->SetBranchAddress("piminus_Dsminus_MC12TuneV4_ProbNNpi", &piminus_Dsminus_MC12TuneV4_ProbNNpi, &b_piminus_Dsminus_MC12TuneV4_ProbNNpi);
   fChain->SetBranchAddress("piminus_Dsminus_MC12TuneV4_ProbNNk", &piminus_Dsminus_MC12TuneV4_ProbNNk, &b_piminus_Dsminus_MC12TuneV4_ProbNNk);
   fChain->SetBranchAddress("piminus_Dsminus_MC12TuneV4_ProbNNp", &piminus_Dsminus_MC12TuneV4_ProbNNp, &b_piminus_Dsminus_MC12TuneV4_ProbNNp);
   fChain->SetBranchAddress("piminus_Dsminus_MC12TuneV4_ProbNNghost", &piminus_Dsminus_MC12TuneV4_ProbNNghost, &b_piminus_Dsminus_MC12TuneV4_ProbNNghost);
   fChain->SetBranchAddress("piminus_Dsminus_MC15TuneV1_ProbNNe", &piminus_Dsminus_MC15TuneV1_ProbNNe, &b_piminus_Dsminus_MC15TuneV1_ProbNNe);
   fChain->SetBranchAddress("piminus_Dsminus_MC15TuneV1_ProbNNmu", &piminus_Dsminus_MC15TuneV1_ProbNNmu, &b_piminus_Dsminus_MC15TuneV1_ProbNNmu);
   fChain->SetBranchAddress("piminus_Dsminus_MC15TuneV1_ProbNNpi", &piminus_Dsminus_MC15TuneV1_ProbNNpi, &b_piminus_Dsminus_MC15TuneV1_ProbNNpi);
   fChain->SetBranchAddress("piminus_Dsminus_MC15TuneV1_ProbNNk", &piminus_Dsminus_MC15TuneV1_ProbNNk, &b_piminus_Dsminus_MC15TuneV1_ProbNNk);
   fChain->SetBranchAddress("piminus_Dsminus_MC15TuneV1_ProbNNp", &piminus_Dsminus_MC15TuneV1_ProbNNp, &b_piminus_Dsminus_MC15TuneV1_ProbNNp);
   fChain->SetBranchAddress("piminus_Dsminus_MC15TuneV1_ProbNNghost", &piminus_Dsminus_MC15TuneV1_ProbNNghost, &b_piminus_Dsminus_MC15TuneV1_ProbNNghost);
   fChain->SetBranchAddress("piminus_Dsminus_CosTheta", &piminus_Dsminus_CosTheta, &b_piminus_Dsminus_CosTheta);
   fChain->SetBranchAddress("piminus_Dsminus_OWNPV_X", &piminus_Dsminus_OWNPV_X, &b_piminus_Dsminus_OWNPV_X);
   fChain->SetBranchAddress("piminus_Dsminus_OWNPV_Y", &piminus_Dsminus_OWNPV_Y, &b_piminus_Dsminus_OWNPV_Y);
   fChain->SetBranchAddress("piminus_Dsminus_OWNPV_Z", &piminus_Dsminus_OWNPV_Z, &b_piminus_Dsminus_OWNPV_Z);
   fChain->SetBranchAddress("piminus_Dsminus_OWNPV_XERR", &piminus_Dsminus_OWNPV_XERR, &b_piminus_Dsminus_OWNPV_XERR);
   fChain->SetBranchAddress("piminus_Dsminus_OWNPV_YERR", &piminus_Dsminus_OWNPV_YERR, &b_piminus_Dsminus_OWNPV_YERR);
   fChain->SetBranchAddress("piminus_Dsminus_OWNPV_ZERR", &piminus_Dsminus_OWNPV_ZERR, &b_piminus_Dsminus_OWNPV_ZERR);
   fChain->SetBranchAddress("piminus_Dsminus_OWNPV_CHI2", &piminus_Dsminus_OWNPV_CHI2, &b_piminus_Dsminus_OWNPV_CHI2);
   fChain->SetBranchAddress("piminus_Dsminus_OWNPV_NDOF", &piminus_Dsminus_OWNPV_NDOF, &b_piminus_Dsminus_OWNPV_NDOF);
   fChain->SetBranchAddress("piminus_Dsminus_OWNPV_COV_", piminus_Dsminus_OWNPV_COV_, &b_piminus_Dsminus_OWNPV_COV_);
   fChain->SetBranchAddress("piminus_Dsminus_IP_OWNPV", &piminus_Dsminus_IP_OWNPV, &b_piminus_Dsminus_IP_OWNPV);
   fChain->SetBranchAddress("piminus_Dsminus_IPCHI2_OWNPV", &piminus_Dsminus_IPCHI2_OWNPV, &b_piminus_Dsminus_IPCHI2_OWNPV);
   fChain->SetBranchAddress("piminus_Dsminus_ORIVX_X", &piminus_Dsminus_ORIVX_X, &b_piminus_Dsminus_ORIVX_X);
   fChain->SetBranchAddress("piminus_Dsminus_ORIVX_Y", &piminus_Dsminus_ORIVX_Y, &b_piminus_Dsminus_ORIVX_Y);
   fChain->SetBranchAddress("piminus_Dsminus_ORIVX_Z", &piminus_Dsminus_ORIVX_Z, &b_piminus_Dsminus_ORIVX_Z);
   fChain->SetBranchAddress("piminus_Dsminus_ORIVX_XERR", &piminus_Dsminus_ORIVX_XERR, &b_piminus_Dsminus_ORIVX_XERR);
   fChain->SetBranchAddress("piminus_Dsminus_ORIVX_YERR", &piminus_Dsminus_ORIVX_YERR, &b_piminus_Dsminus_ORIVX_YERR);
   fChain->SetBranchAddress("piminus_Dsminus_ORIVX_ZERR", &piminus_Dsminus_ORIVX_ZERR, &b_piminus_Dsminus_ORIVX_ZERR);
   fChain->SetBranchAddress("piminus_Dsminus_ORIVX_CHI2", &piminus_Dsminus_ORIVX_CHI2, &b_piminus_Dsminus_ORIVX_CHI2);
   fChain->SetBranchAddress("piminus_Dsminus_ORIVX_NDOF", &piminus_Dsminus_ORIVX_NDOF, &b_piminus_Dsminus_ORIVX_NDOF);
   fChain->SetBranchAddress("piminus_Dsminus_ORIVX_COV_", piminus_Dsminus_ORIVX_COV_, &b_piminus_Dsminus_ORIVX_COV_);
   fChain->SetBranchAddress("piminus_Dsminus_P", &piminus_Dsminus_P, &b_piminus_Dsminus_P);
   fChain->SetBranchAddress("piminus_Dsminus_PT", &piminus_Dsminus_PT, &b_piminus_Dsminus_PT);
   fChain->SetBranchAddress("piminus_Dsminus_PE", &piminus_Dsminus_PE, &b_piminus_Dsminus_PE);
   fChain->SetBranchAddress("piminus_Dsminus_PX", &piminus_Dsminus_PX, &b_piminus_Dsminus_PX);
   fChain->SetBranchAddress("piminus_Dsminus_PY", &piminus_Dsminus_PY, &b_piminus_Dsminus_PY);
   fChain->SetBranchAddress("piminus_Dsminus_PZ", &piminus_Dsminus_PZ, &b_piminus_Dsminus_PZ);
   fChain->SetBranchAddress("piminus_Dsminus_REFPX", &piminus_Dsminus_REFPX, &b_piminus_Dsminus_REFPX);
   fChain->SetBranchAddress("piminus_Dsminus_REFPY", &piminus_Dsminus_REFPY, &b_piminus_Dsminus_REFPY);
   fChain->SetBranchAddress("piminus_Dsminus_REFPZ", &piminus_Dsminus_REFPZ, &b_piminus_Dsminus_REFPZ);
   fChain->SetBranchAddress("piminus_Dsminus_M", &piminus_Dsminus_M, &b_piminus_Dsminus_M);
   fChain->SetBranchAddress("piminus_Dsminus_AtVtx_PE", &piminus_Dsminus_AtVtx_PE, &b_piminus_Dsminus_AtVtx_PE);
   fChain->SetBranchAddress("piminus_Dsminus_AtVtx_PX", &piminus_Dsminus_AtVtx_PX, &b_piminus_Dsminus_AtVtx_PX);
   fChain->SetBranchAddress("piminus_Dsminus_AtVtx_PY", &piminus_Dsminus_AtVtx_PY, &b_piminus_Dsminus_AtVtx_PY);
   fChain->SetBranchAddress("piminus_Dsminus_AtVtx_PZ", &piminus_Dsminus_AtVtx_PZ, &b_piminus_Dsminus_AtVtx_PZ);
   fChain->SetBranchAddress("piminus_Dsminus_TRUEID", &piminus_Dsminus_TRUEID, &b_piminus_Dsminus_TRUEID);
   fChain->SetBranchAddress("piminus_Dsminus_TRUEP_E", &piminus_Dsminus_TRUEP_E, &b_piminus_Dsminus_TRUEP_E);
   fChain->SetBranchAddress("piminus_Dsminus_TRUEP_X", &piminus_Dsminus_TRUEP_X, &b_piminus_Dsminus_TRUEP_X);
   fChain->SetBranchAddress("piminus_Dsminus_TRUEP_Y", &piminus_Dsminus_TRUEP_Y, &b_piminus_Dsminus_TRUEP_Y);
   fChain->SetBranchAddress("piminus_Dsminus_TRUEP_Z", &piminus_Dsminus_TRUEP_Z, &b_piminus_Dsminus_TRUEP_Z);
   fChain->SetBranchAddress("piminus_Dsminus_TRUEPT", &piminus_Dsminus_TRUEPT, &b_piminus_Dsminus_TRUEPT);
   fChain->SetBranchAddress("piminus_Dsminus_TRUEORIGINVERTEX_X", &piminus_Dsminus_TRUEORIGINVERTEX_X, &b_piminus_Dsminus_TRUEORIGINVERTEX_X);
   fChain->SetBranchAddress("piminus_Dsminus_TRUEORIGINVERTEX_Y", &piminus_Dsminus_TRUEORIGINVERTEX_Y, &b_piminus_Dsminus_TRUEORIGINVERTEX_Y);
   fChain->SetBranchAddress("piminus_Dsminus_TRUEORIGINVERTEX_Z", &piminus_Dsminus_TRUEORIGINVERTEX_Z, &b_piminus_Dsminus_TRUEORIGINVERTEX_Z);
   fChain->SetBranchAddress("piminus_Dsminus_TRUEENDVERTEX_X", &piminus_Dsminus_TRUEENDVERTEX_X, &b_piminus_Dsminus_TRUEENDVERTEX_X);
   fChain->SetBranchAddress("piminus_Dsminus_TRUEENDVERTEX_Y", &piminus_Dsminus_TRUEENDVERTEX_Y, &b_piminus_Dsminus_TRUEENDVERTEX_Y);
   fChain->SetBranchAddress("piminus_Dsminus_TRUEENDVERTEX_Z", &piminus_Dsminus_TRUEENDVERTEX_Z, &b_piminus_Dsminus_TRUEENDVERTEX_Z);
   fChain->SetBranchAddress("piminus_Dsminus_TRUEISSTABLE", &piminus_Dsminus_TRUEISSTABLE, &b_piminus_Dsminus_TRUEISSTABLE);
   fChain->SetBranchAddress("piminus_Dsminus_TRUETAU", &piminus_Dsminus_TRUETAU, &b_piminus_Dsminus_TRUETAU);
   fChain->SetBranchAddress("piminus_Dsminus_ID", &piminus_Dsminus_ID, &b_piminus_Dsminus_ID);
   fChain->SetBranchAddress("piminus_Dsminus_CombDLLMu", &piminus_Dsminus_CombDLLMu, &b_piminus_Dsminus_CombDLLMu);
   fChain->SetBranchAddress("piminus_Dsminus_ProbNNmu", &piminus_Dsminus_ProbNNmu, &b_piminus_Dsminus_ProbNNmu);
   fChain->SetBranchAddress("piminus_Dsminus_ProbNNghost", &piminus_Dsminus_ProbNNghost, &b_piminus_Dsminus_ProbNNghost);
   fChain->SetBranchAddress("piminus_Dsminus_InMuonAcc", &piminus_Dsminus_InMuonAcc, &b_piminus_Dsminus_InMuonAcc);
   fChain->SetBranchAddress("piminus_Dsminus_MuonDist2", &piminus_Dsminus_MuonDist2, &b_piminus_Dsminus_MuonDist2);
   fChain->SetBranchAddress("piminus_Dsminus_regionInM2", &piminus_Dsminus_regionInM2, &b_piminus_Dsminus_regionInM2);
   fChain->SetBranchAddress("piminus_Dsminus_hasMuon", &piminus_Dsminus_hasMuon, &b_piminus_Dsminus_hasMuon);
   fChain->SetBranchAddress("piminus_Dsminus_isMuon", &piminus_Dsminus_isMuon, &b_piminus_Dsminus_isMuon);
   fChain->SetBranchAddress("piminus_Dsminus_isMuonLoose", &piminus_Dsminus_isMuonLoose, &b_piminus_Dsminus_isMuonLoose);
   fChain->SetBranchAddress("piminus_Dsminus_NShared", &piminus_Dsminus_NShared, &b_piminus_Dsminus_NShared);
   fChain->SetBranchAddress("piminus_Dsminus_MuonLLmu", &piminus_Dsminus_MuonLLmu, &b_piminus_Dsminus_MuonLLmu);
   fChain->SetBranchAddress("piminus_Dsminus_MuonLLbg", &piminus_Dsminus_MuonLLbg, &b_piminus_Dsminus_MuonLLbg);
   fChain->SetBranchAddress("piminus_Dsminus_isMuonFromProto", &piminus_Dsminus_isMuonFromProto, &b_piminus_Dsminus_isMuonFromProto);
   fChain->SetBranchAddress("piminus_Dsminus_PIDe", &piminus_Dsminus_PIDe, &b_piminus_Dsminus_PIDe);
   fChain->SetBranchAddress("piminus_Dsminus_PIDmu", &piminus_Dsminus_PIDmu, &b_piminus_Dsminus_PIDmu);
   fChain->SetBranchAddress("piminus_Dsminus_PIDK", &piminus_Dsminus_PIDK, &b_piminus_Dsminus_PIDK);
   fChain->SetBranchAddress("piminus_Dsminus_PIDp", &piminus_Dsminus_PIDp, &b_piminus_Dsminus_PIDp);
   fChain->SetBranchAddress("piminus_Dsminus_PIDd", &piminus_Dsminus_PIDd, &b_piminus_Dsminus_PIDd);
   fChain->SetBranchAddress("piminus_Dsminus_ProbNNe", &piminus_Dsminus_ProbNNe, &b_piminus_Dsminus_ProbNNe);
   fChain->SetBranchAddress("piminus_Dsminus_ProbNNk", &piminus_Dsminus_ProbNNk, &b_piminus_Dsminus_ProbNNk);
   fChain->SetBranchAddress("piminus_Dsminus_ProbNNp", &piminus_Dsminus_ProbNNp, &b_piminus_Dsminus_ProbNNp);
   fChain->SetBranchAddress("piminus_Dsminus_ProbNNpi", &piminus_Dsminus_ProbNNpi, &b_piminus_Dsminus_ProbNNpi);
   fChain->SetBranchAddress("piminus_Dsminus_ProbNNd", &piminus_Dsminus_ProbNNd, &b_piminus_Dsminus_ProbNNd);
   fChain->SetBranchAddress("piminus_Dsminus_hasRich", &piminus_Dsminus_hasRich, &b_piminus_Dsminus_hasRich);
   fChain->SetBranchAddress("piminus_Dsminus_UsedRichAerogel", &piminus_Dsminus_UsedRichAerogel, &b_piminus_Dsminus_UsedRichAerogel);
   fChain->SetBranchAddress("piminus_Dsminus_UsedRich1Gas", &piminus_Dsminus_UsedRich1Gas, &b_piminus_Dsminus_UsedRich1Gas);
   fChain->SetBranchAddress("piminus_Dsminus_UsedRich2Gas", &piminus_Dsminus_UsedRich2Gas, &b_piminus_Dsminus_UsedRich2Gas);
   fChain->SetBranchAddress("piminus_Dsminus_RichAboveElThres", &piminus_Dsminus_RichAboveElThres, &b_piminus_Dsminus_RichAboveElThres);
   fChain->SetBranchAddress("piminus_Dsminus_RichAboveMuThres", &piminus_Dsminus_RichAboveMuThres, &b_piminus_Dsminus_RichAboveMuThres);
   fChain->SetBranchAddress("piminus_Dsminus_RichAbovePiThres", &piminus_Dsminus_RichAbovePiThres, &b_piminus_Dsminus_RichAbovePiThres);
   fChain->SetBranchAddress("piminus_Dsminus_RichAboveKaThres", &piminus_Dsminus_RichAboveKaThres, &b_piminus_Dsminus_RichAboveKaThres);
   fChain->SetBranchAddress("piminus_Dsminus_RichAbovePrThres", &piminus_Dsminus_RichAbovePrThres, &b_piminus_Dsminus_RichAbovePrThres);
   fChain->SetBranchAddress("piminus_Dsminus_hasCalo", &piminus_Dsminus_hasCalo, &b_piminus_Dsminus_hasCalo);
   fChain->SetBranchAddress("piminus_Dsminus_L0Global_Dec", &piminus_Dsminus_L0Global_Dec, &b_piminus_Dsminus_L0Global_Dec);
   fChain->SetBranchAddress("piminus_Dsminus_L0Global_TIS", &piminus_Dsminus_L0Global_TIS, &b_piminus_Dsminus_L0Global_TIS);
   fChain->SetBranchAddress("piminus_Dsminus_L0Global_TOS", &piminus_Dsminus_L0Global_TOS, &b_piminus_Dsminus_L0Global_TOS);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt1Global_Dec", &piminus_Dsminus_Hlt1Global_Dec, &b_piminus_Dsminus_Hlt1Global_Dec);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt1Global_TIS", &piminus_Dsminus_Hlt1Global_TIS, &b_piminus_Dsminus_Hlt1Global_TIS);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt1Global_TOS", &piminus_Dsminus_Hlt1Global_TOS, &b_piminus_Dsminus_Hlt1Global_TOS);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt1Phys_Dec", &piminus_Dsminus_Hlt1Phys_Dec, &b_piminus_Dsminus_Hlt1Phys_Dec);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt1Phys_TIS", &piminus_Dsminus_Hlt1Phys_TIS, &b_piminus_Dsminus_Hlt1Phys_TIS);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt1Phys_TOS", &piminus_Dsminus_Hlt1Phys_TOS, &b_piminus_Dsminus_Hlt1Phys_TOS);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt2Global_Dec", &piminus_Dsminus_Hlt2Global_Dec, &b_piminus_Dsminus_Hlt2Global_Dec);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt2Global_TIS", &piminus_Dsminus_Hlt2Global_TIS, &b_piminus_Dsminus_Hlt2Global_TIS);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt2Global_TOS", &piminus_Dsminus_Hlt2Global_TOS, &b_piminus_Dsminus_Hlt2Global_TOS);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt2Phys_Dec", &piminus_Dsminus_Hlt2Phys_Dec, &b_piminus_Dsminus_Hlt2Phys_Dec);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt2Phys_TIS", &piminus_Dsminus_Hlt2Phys_TIS, &b_piminus_Dsminus_Hlt2Phys_TIS);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt2Phys_TOS", &piminus_Dsminus_Hlt2Phys_TOS, &b_piminus_Dsminus_Hlt2Phys_TOS);
   fChain->SetBranchAddress("piminus_Dsminus_L0HadronDecision_Dec", &piminus_Dsminus_L0HadronDecision_Dec, &b_piminus_Dsminus_L0HadronDecision_Dec);
   fChain->SetBranchAddress("piminus_Dsminus_L0HadronDecision_TIS", &piminus_Dsminus_L0HadronDecision_TIS, &b_piminus_Dsminus_L0HadronDecision_TIS);
   fChain->SetBranchAddress("piminus_Dsminus_L0HadronDecision_TOS", &piminus_Dsminus_L0HadronDecision_TOS, &b_piminus_Dsminus_L0HadronDecision_TOS);
   fChain->SetBranchAddress("piminus_Dsminus_L0ElectronDecision_Dec", &piminus_Dsminus_L0ElectronDecision_Dec, &b_piminus_Dsminus_L0ElectronDecision_Dec);
   fChain->SetBranchAddress("piminus_Dsminus_L0ElectronDecision_TIS", &piminus_Dsminus_L0ElectronDecision_TIS, &b_piminus_Dsminus_L0ElectronDecision_TIS);
   fChain->SetBranchAddress("piminus_Dsminus_L0ElectronDecision_TOS", &piminus_Dsminus_L0ElectronDecision_TOS, &b_piminus_Dsminus_L0ElectronDecision_TOS);
   fChain->SetBranchAddress("piminus_Dsminus_L0ElectronHiDecision_Dec", &piminus_Dsminus_L0ElectronHiDecision_Dec, &b_piminus_Dsminus_L0ElectronHiDecision_Dec);
   fChain->SetBranchAddress("piminus_Dsminus_L0ElectronHiDecision_TIS", &piminus_Dsminus_L0ElectronHiDecision_TIS, &b_piminus_Dsminus_L0ElectronHiDecision_TIS);
   fChain->SetBranchAddress("piminus_Dsminus_L0ElectronHiDecision_TOS", &piminus_Dsminus_L0ElectronHiDecision_TOS, &b_piminus_Dsminus_L0ElectronHiDecision_TOS);
   fChain->SetBranchAddress("piminus_Dsminus_L0MuonDecision_Dec", &piminus_Dsminus_L0MuonDecision_Dec, &b_piminus_Dsminus_L0MuonDecision_Dec);
   fChain->SetBranchAddress("piminus_Dsminus_L0MuonDecision_TIS", &piminus_Dsminus_L0MuonDecision_TIS, &b_piminus_Dsminus_L0MuonDecision_TIS);
   fChain->SetBranchAddress("piminus_Dsminus_L0MuonDecision_TOS", &piminus_Dsminus_L0MuonDecision_TOS, &b_piminus_Dsminus_L0MuonDecision_TOS);
   fChain->SetBranchAddress("piminus_Dsminus_L0DiMuonDecision_Dec", &piminus_Dsminus_L0DiMuonDecision_Dec, &b_piminus_Dsminus_L0DiMuonDecision_Dec);
   fChain->SetBranchAddress("piminus_Dsminus_L0DiMuonDecision_TIS", &piminus_Dsminus_L0DiMuonDecision_TIS, &b_piminus_Dsminus_L0DiMuonDecision_TIS);
   fChain->SetBranchAddress("piminus_Dsminus_L0DiMuonDecision_TOS", &piminus_Dsminus_L0DiMuonDecision_TOS, &b_piminus_Dsminus_L0DiMuonDecision_TOS);
   fChain->SetBranchAddress("piminus_Dsminus_L0MuonHighDecision_Dec", &piminus_Dsminus_L0MuonHighDecision_Dec, &b_piminus_Dsminus_L0MuonHighDecision_Dec);
   fChain->SetBranchAddress("piminus_Dsminus_L0MuonHighDecision_TIS", &piminus_Dsminus_L0MuonHighDecision_TIS, &b_piminus_Dsminus_L0MuonHighDecision_TIS);
   fChain->SetBranchAddress("piminus_Dsminus_L0MuonHighDecision_TOS", &piminus_Dsminus_L0MuonHighDecision_TOS, &b_piminus_Dsminus_L0MuonHighDecision_TOS);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt1TrackMVADecision_Dec", &piminus_Dsminus_Hlt1TrackMVADecision_Dec, &b_piminus_Dsminus_Hlt1TrackMVADecision_Dec);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt1TrackMVADecision_TIS", &piminus_Dsminus_Hlt1TrackMVADecision_TIS, &b_piminus_Dsminus_Hlt1TrackMVADecision_TIS);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt1TrackMVADecision_TOS", &piminus_Dsminus_Hlt1TrackMVADecision_TOS, &b_piminus_Dsminus_Hlt1TrackMVADecision_TOS);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt1TwoTrackMVADecision_Dec", &piminus_Dsminus_Hlt1TwoTrackMVADecision_Dec, &b_piminus_Dsminus_Hlt1TwoTrackMVADecision_Dec);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt1TwoTrackMVADecision_TIS", &piminus_Dsminus_Hlt1TwoTrackMVADecision_TIS, &b_piminus_Dsminus_Hlt1TwoTrackMVADecision_TIS);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt1TwoTrackMVADecision_TOS", &piminus_Dsminus_Hlt1TwoTrackMVADecision_TOS, &b_piminus_Dsminus_Hlt1TwoTrackMVADecision_TOS);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt1GlobalDecision_Dec", &piminus_Dsminus_Hlt1GlobalDecision_Dec, &b_piminus_Dsminus_Hlt1GlobalDecision_Dec);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt1GlobalDecision_TIS", &piminus_Dsminus_Hlt1GlobalDecision_TIS, &b_piminus_Dsminus_Hlt1GlobalDecision_TIS);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt1GlobalDecision_TOS", &piminus_Dsminus_Hlt1GlobalDecision_TOS, &b_piminus_Dsminus_Hlt1GlobalDecision_TOS);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt1TrackAllL0Decision_Dec", &piminus_Dsminus_Hlt1TrackAllL0Decision_Dec, &b_piminus_Dsminus_Hlt1TrackAllL0Decision_Dec);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt1TrackAllL0Decision_TIS", &piminus_Dsminus_Hlt1TrackAllL0Decision_TIS, &b_piminus_Dsminus_Hlt1TrackAllL0Decision_TIS);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt1TrackAllL0Decision_TOS", &piminus_Dsminus_Hlt1TrackAllL0Decision_TOS, &b_piminus_Dsminus_Hlt1TrackAllL0Decision_TOS);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt2Topo2BodyBBDTDecision_Dec", &piminus_Dsminus_Hlt2Topo2BodyBBDTDecision_Dec, &b_piminus_Dsminus_Hlt2Topo2BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt2Topo2BodyBBDTDecision_TIS", &piminus_Dsminus_Hlt2Topo2BodyBBDTDecision_TIS, &b_piminus_Dsminus_Hlt2Topo2BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt2Topo2BodyBBDTDecision_TOS", &piminus_Dsminus_Hlt2Topo2BodyBBDTDecision_TOS, &b_piminus_Dsminus_Hlt2Topo2BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt2Topo3BodyBBDTDecision_Dec", &piminus_Dsminus_Hlt2Topo3BodyBBDTDecision_Dec, &b_piminus_Dsminus_Hlt2Topo3BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt2Topo3BodyBBDTDecision_TIS", &piminus_Dsminus_Hlt2Topo3BodyBBDTDecision_TIS, &b_piminus_Dsminus_Hlt2Topo3BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt2Topo3BodyBBDTDecision_TOS", &piminus_Dsminus_Hlt2Topo3BodyBBDTDecision_TOS, &b_piminus_Dsminus_Hlt2Topo3BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt2Topo4BodyBBDTDecision_Dec", &piminus_Dsminus_Hlt2Topo4BodyBBDTDecision_Dec, &b_piminus_Dsminus_Hlt2Topo4BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt2Topo4BodyBBDTDecision_TIS", &piminus_Dsminus_Hlt2Topo4BodyBBDTDecision_TIS, &b_piminus_Dsminus_Hlt2Topo4BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt2Topo4BodyBBDTDecision_TOS", &piminus_Dsminus_Hlt2Topo4BodyBBDTDecision_TOS, &b_piminus_Dsminus_Hlt2Topo4BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_Dec", &piminus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_Dec, &b_piminus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_Dec);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_TIS", &piminus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_TIS, &b_piminus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_TIS);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_TOS", &piminus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_TOS, &b_piminus_Dsminus_Hlt2BHadB02PpPpPmPmDecision_TOS);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt2Topo2BodyDecision_Dec", &piminus_Dsminus_Hlt2Topo2BodyDecision_Dec, &b_piminus_Dsminus_Hlt2Topo2BodyDecision_Dec);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt2Topo2BodyDecision_TIS", &piminus_Dsminus_Hlt2Topo2BodyDecision_TIS, &b_piminus_Dsminus_Hlt2Topo2BodyDecision_TIS);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt2Topo2BodyDecision_TOS", &piminus_Dsminus_Hlt2Topo2BodyDecision_TOS, &b_piminus_Dsminus_Hlt2Topo2BodyDecision_TOS);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt2Topo3BodyDecision_Dec", &piminus_Dsminus_Hlt2Topo3BodyDecision_Dec, &b_piminus_Dsminus_Hlt2Topo3BodyDecision_Dec);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt2Topo3BodyDecision_TIS", &piminus_Dsminus_Hlt2Topo3BodyDecision_TIS, &b_piminus_Dsminus_Hlt2Topo3BodyDecision_TIS);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt2Topo3BodyDecision_TOS", &piminus_Dsminus_Hlt2Topo3BodyDecision_TOS, &b_piminus_Dsminus_Hlt2Topo3BodyDecision_TOS);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt2Topo4BodyDecision_Dec", &piminus_Dsminus_Hlt2Topo4BodyDecision_Dec, &b_piminus_Dsminus_Hlt2Topo4BodyDecision_Dec);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt2Topo4BodyDecision_TIS", &piminus_Dsminus_Hlt2Topo4BodyDecision_TIS, &b_piminus_Dsminus_Hlt2Topo4BodyDecision_TIS);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt2Topo4BodyDecision_TOS", &piminus_Dsminus_Hlt2Topo4BodyDecision_TOS, &b_piminus_Dsminus_Hlt2Topo4BodyDecision_TOS);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt2GlobalDecision_Dec", &piminus_Dsminus_Hlt2GlobalDecision_Dec, &b_piminus_Dsminus_Hlt2GlobalDecision_Dec);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt2GlobalDecision_TIS", &piminus_Dsminus_Hlt2GlobalDecision_TIS, &b_piminus_Dsminus_Hlt2GlobalDecision_TIS);
   fChain->SetBranchAddress("piminus_Dsminus_Hlt2GlobalDecision_TOS", &piminus_Dsminus_Hlt2GlobalDecision_TOS, &b_piminus_Dsminus_Hlt2GlobalDecision_TOS);
   fChain->SetBranchAddress("piminus_Dsminus_TRACK_Type", &piminus_Dsminus_TRACK_Type, &b_piminus_Dsminus_TRACK_Type);
   fChain->SetBranchAddress("piminus_Dsminus_TRACK_Key", &piminus_Dsminus_TRACK_Key, &b_piminus_Dsminus_TRACK_Key);
   fChain->SetBranchAddress("piminus_Dsminus_TRACK_CHI2NDOF", &piminus_Dsminus_TRACK_CHI2NDOF, &b_piminus_Dsminus_TRACK_CHI2NDOF);
   fChain->SetBranchAddress("piminus_Dsminus_TRACK_PCHI2", &piminus_Dsminus_TRACK_PCHI2, &b_piminus_Dsminus_TRACK_PCHI2);
   fChain->SetBranchAddress("piminus_Dsminus_TRACK_MatchCHI2", &piminus_Dsminus_TRACK_MatchCHI2, &b_piminus_Dsminus_TRACK_MatchCHI2);
   fChain->SetBranchAddress("piminus_Dsminus_TRACK_GhostProb", &piminus_Dsminus_TRACK_GhostProb, &b_piminus_Dsminus_TRACK_GhostProb);
   fChain->SetBranchAddress("piminus_Dsminus_TRACK_CloneDist", &piminus_Dsminus_TRACK_CloneDist, &b_piminus_Dsminus_TRACK_CloneDist);
   fChain->SetBranchAddress("piminus_Dsminus_TRACK_Likelihood", &piminus_Dsminus_TRACK_Likelihood, &b_piminus_Dsminus_TRACK_Likelihood);
   fChain->SetBranchAddress("piminus_Dsminus_X", &piminus_Dsminus_X, &b_piminus_Dsminus_X);
   fChain->SetBranchAddress("piminus_Dsminus_Y", &piminus_Dsminus_Y, &b_piminus_Dsminus_Y);
   fChain->SetBranchAddress("Dplus_ETA", &Dplus_ETA, &b_Dplus_ETA);
   fChain->SetBranchAddress("Dplus_MinIPCHI2", &Dplus_MinIPCHI2, &b_Dplus_MinIPCHI2);
   fChain->SetBranchAddress("Dplus_CosTheta", &Dplus_CosTheta, &b_Dplus_CosTheta);
   fChain->SetBranchAddress("Dplus_ENDVERTEX_X", &Dplus_ENDVERTEX_X, &b_Dplus_ENDVERTEX_X);
   fChain->SetBranchAddress("Dplus_ENDVERTEX_Y", &Dplus_ENDVERTEX_Y, &b_Dplus_ENDVERTEX_Y);
   fChain->SetBranchAddress("Dplus_ENDVERTEX_Z", &Dplus_ENDVERTEX_Z, &b_Dplus_ENDVERTEX_Z);
   fChain->SetBranchAddress("Dplus_ENDVERTEX_XERR", &Dplus_ENDVERTEX_XERR, &b_Dplus_ENDVERTEX_XERR);
   fChain->SetBranchAddress("Dplus_ENDVERTEX_YERR", &Dplus_ENDVERTEX_YERR, &b_Dplus_ENDVERTEX_YERR);
   fChain->SetBranchAddress("Dplus_ENDVERTEX_ZERR", &Dplus_ENDVERTEX_ZERR, &b_Dplus_ENDVERTEX_ZERR);
   fChain->SetBranchAddress("Dplus_ENDVERTEX_CHI2", &Dplus_ENDVERTEX_CHI2, &b_Dplus_ENDVERTEX_CHI2);
   fChain->SetBranchAddress("Dplus_ENDVERTEX_NDOF", &Dplus_ENDVERTEX_NDOF, &b_Dplus_ENDVERTEX_NDOF);
   fChain->SetBranchAddress("Dplus_ENDVERTEX_COV_", Dplus_ENDVERTEX_COV_, &b_Dplus_ENDVERTEX_COV_);
   fChain->SetBranchAddress("Dplus_OWNPV_X", &Dplus_OWNPV_X, &b_Dplus_OWNPV_X);
   fChain->SetBranchAddress("Dplus_OWNPV_Y", &Dplus_OWNPV_Y, &b_Dplus_OWNPV_Y);
   fChain->SetBranchAddress("Dplus_OWNPV_Z", &Dplus_OWNPV_Z, &b_Dplus_OWNPV_Z);
   fChain->SetBranchAddress("Dplus_OWNPV_XERR", &Dplus_OWNPV_XERR, &b_Dplus_OWNPV_XERR);
   fChain->SetBranchAddress("Dplus_OWNPV_YERR", &Dplus_OWNPV_YERR, &b_Dplus_OWNPV_YERR);
   fChain->SetBranchAddress("Dplus_OWNPV_ZERR", &Dplus_OWNPV_ZERR, &b_Dplus_OWNPV_ZERR);
   fChain->SetBranchAddress("Dplus_OWNPV_CHI2", &Dplus_OWNPV_CHI2, &b_Dplus_OWNPV_CHI2);
   fChain->SetBranchAddress("Dplus_OWNPV_NDOF", &Dplus_OWNPV_NDOF, &b_Dplus_OWNPV_NDOF);
   fChain->SetBranchAddress("Dplus_OWNPV_COV_", Dplus_OWNPV_COV_, &b_Dplus_OWNPV_COV_);
   fChain->SetBranchAddress("Dplus_IP_OWNPV", &Dplus_IP_OWNPV, &b_Dplus_IP_OWNPV);
   fChain->SetBranchAddress("Dplus_IPCHI2_OWNPV", &Dplus_IPCHI2_OWNPV, &b_Dplus_IPCHI2_OWNPV);
   fChain->SetBranchAddress("Dplus_FD_OWNPV", &Dplus_FD_OWNPV, &b_Dplus_FD_OWNPV);
   fChain->SetBranchAddress("Dplus_FDCHI2_OWNPV", &Dplus_FDCHI2_OWNPV, &b_Dplus_FDCHI2_OWNPV);
   fChain->SetBranchAddress("Dplus_DIRA_OWNPV", &Dplus_DIRA_OWNPV, &b_Dplus_DIRA_OWNPV);
   fChain->SetBranchAddress("Dplus_ORIVX_X", &Dplus_ORIVX_X, &b_Dplus_ORIVX_X);
   fChain->SetBranchAddress("Dplus_ORIVX_Y", &Dplus_ORIVX_Y, &b_Dplus_ORIVX_Y);
   fChain->SetBranchAddress("Dplus_ORIVX_Z", &Dplus_ORIVX_Z, &b_Dplus_ORIVX_Z);
   fChain->SetBranchAddress("Dplus_ORIVX_XERR", &Dplus_ORIVX_XERR, &b_Dplus_ORIVX_XERR);
   fChain->SetBranchAddress("Dplus_ORIVX_YERR", &Dplus_ORIVX_YERR, &b_Dplus_ORIVX_YERR);
   fChain->SetBranchAddress("Dplus_ORIVX_ZERR", &Dplus_ORIVX_ZERR, &b_Dplus_ORIVX_ZERR);
   fChain->SetBranchAddress("Dplus_ORIVX_CHI2", &Dplus_ORIVX_CHI2, &b_Dplus_ORIVX_CHI2);
   fChain->SetBranchAddress("Dplus_ORIVX_NDOF", &Dplus_ORIVX_NDOF, &b_Dplus_ORIVX_NDOF);
   fChain->SetBranchAddress("Dplus_ORIVX_COV_", Dplus_ORIVX_COV_, &b_Dplus_ORIVX_COV_);
   fChain->SetBranchAddress("Dplus_FD_ORIVX", &Dplus_FD_ORIVX, &b_Dplus_FD_ORIVX);
   fChain->SetBranchAddress("Dplus_FDCHI2_ORIVX", &Dplus_FDCHI2_ORIVX, &b_Dplus_FDCHI2_ORIVX);
   fChain->SetBranchAddress("Dplus_DIRA_ORIVX", &Dplus_DIRA_ORIVX, &b_Dplus_DIRA_ORIVX);
   fChain->SetBranchAddress("Dplus_P", &Dplus_P, &b_Dplus_P);
   fChain->SetBranchAddress("Dplus_PT", &Dplus_PT, &b_Dplus_PT);
   fChain->SetBranchAddress("Dplus_PE", &Dplus_PE, &b_Dplus_PE);
   fChain->SetBranchAddress("Dplus_PX", &Dplus_PX, &b_Dplus_PX);
   fChain->SetBranchAddress("Dplus_PY", &Dplus_PY, &b_Dplus_PY);
   fChain->SetBranchAddress("Dplus_PZ", &Dplus_PZ, &b_Dplus_PZ);
   fChain->SetBranchAddress("Dplus_REFPX", &Dplus_REFPX, &b_Dplus_REFPX);
   fChain->SetBranchAddress("Dplus_REFPY", &Dplus_REFPY, &b_Dplus_REFPY);
   fChain->SetBranchAddress("Dplus_REFPZ", &Dplus_REFPZ, &b_Dplus_REFPZ);
   fChain->SetBranchAddress("Dplus_MM", &Dplus_MM, &b_Dplus_MM);
   fChain->SetBranchAddress("Dplus_MMERR", &Dplus_MMERR, &b_Dplus_MMERR);
   fChain->SetBranchAddress("Dplus_M", &Dplus_M, &b_Dplus_M);
   fChain->SetBranchAddress("Dplus_BKGCAT", &Dplus_BKGCAT, &b_Dplus_BKGCAT);
   fChain->SetBranchAddress("Dplus_TRUEID", &Dplus_TRUEID, &b_Dplus_TRUEID);
   fChain->SetBranchAddress("Dplus_TRUEP_E", &Dplus_TRUEP_E, &b_Dplus_TRUEP_E);
   fChain->SetBranchAddress("Dplus_TRUEP_X", &Dplus_TRUEP_X, &b_Dplus_TRUEP_X);
   fChain->SetBranchAddress("Dplus_TRUEP_Y", &Dplus_TRUEP_Y, &b_Dplus_TRUEP_Y);
   fChain->SetBranchAddress("Dplus_TRUEP_Z", &Dplus_TRUEP_Z, &b_Dplus_TRUEP_Z);
   fChain->SetBranchAddress("Dplus_TRUEPT", &Dplus_TRUEPT, &b_Dplus_TRUEPT);
   fChain->SetBranchAddress("Dplus_TRUEORIGINVERTEX_X", &Dplus_TRUEORIGINVERTEX_X, &b_Dplus_TRUEORIGINVERTEX_X);
   fChain->SetBranchAddress("Dplus_TRUEORIGINVERTEX_Y", &Dplus_TRUEORIGINVERTEX_Y, &b_Dplus_TRUEORIGINVERTEX_Y);
   fChain->SetBranchAddress("Dplus_TRUEORIGINVERTEX_Z", &Dplus_TRUEORIGINVERTEX_Z, &b_Dplus_TRUEORIGINVERTEX_Z);
   fChain->SetBranchAddress("Dplus_TRUEENDVERTEX_X", &Dplus_TRUEENDVERTEX_X, &b_Dplus_TRUEENDVERTEX_X);
   fChain->SetBranchAddress("Dplus_TRUEENDVERTEX_Y", &Dplus_TRUEENDVERTEX_Y, &b_Dplus_TRUEENDVERTEX_Y);
   fChain->SetBranchAddress("Dplus_TRUEENDVERTEX_Z", &Dplus_TRUEENDVERTEX_Z, &b_Dplus_TRUEENDVERTEX_Z);
   fChain->SetBranchAddress("Dplus_TRUEISSTABLE", &Dplus_TRUEISSTABLE, &b_Dplus_TRUEISSTABLE);
   fChain->SetBranchAddress("Dplus_TRUETAU", &Dplus_TRUETAU, &b_Dplus_TRUETAU);
   fChain->SetBranchAddress("Dplus_ID", &Dplus_ID, &b_Dplus_ID);
   fChain->SetBranchAddress("Dplus_TAU", &Dplus_TAU, &b_Dplus_TAU);
   fChain->SetBranchAddress("Dplus_TAUERR", &Dplus_TAUERR, &b_Dplus_TAUERR);
   fChain->SetBranchAddress("Dplus_TAUCHI2", &Dplus_TAUCHI2, &b_Dplus_TAUCHI2);
   fChain->SetBranchAddress("Dplus_L0Global_Dec", &Dplus_L0Global_Dec, &b_Dplus_L0Global_Dec);
   fChain->SetBranchAddress("Dplus_L0Global_TIS", &Dplus_L0Global_TIS, &b_Dplus_L0Global_TIS);
   fChain->SetBranchAddress("Dplus_L0Global_TOS", &Dplus_L0Global_TOS, &b_Dplus_L0Global_TOS);
   fChain->SetBranchAddress("Dplus_Hlt1Global_Dec", &Dplus_Hlt1Global_Dec, &b_Dplus_Hlt1Global_Dec);
   fChain->SetBranchAddress("Dplus_Hlt1Global_TIS", &Dplus_Hlt1Global_TIS, &b_Dplus_Hlt1Global_TIS);
   fChain->SetBranchAddress("Dplus_Hlt1Global_TOS", &Dplus_Hlt1Global_TOS, &b_Dplus_Hlt1Global_TOS);
   fChain->SetBranchAddress("Dplus_Hlt1Phys_Dec", &Dplus_Hlt1Phys_Dec, &b_Dplus_Hlt1Phys_Dec);
   fChain->SetBranchAddress("Dplus_Hlt1Phys_TIS", &Dplus_Hlt1Phys_TIS, &b_Dplus_Hlt1Phys_TIS);
   fChain->SetBranchAddress("Dplus_Hlt1Phys_TOS", &Dplus_Hlt1Phys_TOS, &b_Dplus_Hlt1Phys_TOS);
   fChain->SetBranchAddress("Dplus_Hlt2Global_Dec", &Dplus_Hlt2Global_Dec, &b_Dplus_Hlt2Global_Dec);
   fChain->SetBranchAddress("Dplus_Hlt2Global_TIS", &Dplus_Hlt2Global_TIS, &b_Dplus_Hlt2Global_TIS);
   fChain->SetBranchAddress("Dplus_Hlt2Global_TOS", &Dplus_Hlt2Global_TOS, &b_Dplus_Hlt2Global_TOS);
   fChain->SetBranchAddress("Dplus_Hlt2Phys_Dec", &Dplus_Hlt2Phys_Dec, &b_Dplus_Hlt2Phys_Dec);
   fChain->SetBranchAddress("Dplus_Hlt2Phys_TIS", &Dplus_Hlt2Phys_TIS, &b_Dplus_Hlt2Phys_TIS);
   fChain->SetBranchAddress("Dplus_Hlt2Phys_TOS", &Dplus_Hlt2Phys_TOS, &b_Dplus_Hlt2Phys_TOS);
   fChain->SetBranchAddress("Dplus_L0HadronDecision_Dec", &Dplus_L0HadronDecision_Dec, &b_Dplus_L0HadronDecision_Dec);
   fChain->SetBranchAddress("Dplus_L0HadronDecision_TIS", &Dplus_L0HadronDecision_TIS, &b_Dplus_L0HadronDecision_TIS);
   fChain->SetBranchAddress("Dplus_L0HadronDecision_TOS", &Dplus_L0HadronDecision_TOS, &b_Dplus_L0HadronDecision_TOS);
   fChain->SetBranchAddress("Dplus_L0ElectronDecision_Dec", &Dplus_L0ElectronDecision_Dec, &b_Dplus_L0ElectronDecision_Dec);
   fChain->SetBranchAddress("Dplus_L0ElectronDecision_TIS", &Dplus_L0ElectronDecision_TIS, &b_Dplus_L0ElectronDecision_TIS);
   fChain->SetBranchAddress("Dplus_L0ElectronDecision_TOS", &Dplus_L0ElectronDecision_TOS, &b_Dplus_L0ElectronDecision_TOS);
   fChain->SetBranchAddress("Dplus_L0ElectronHiDecision_Dec", &Dplus_L0ElectronHiDecision_Dec, &b_Dplus_L0ElectronHiDecision_Dec);
   fChain->SetBranchAddress("Dplus_L0ElectronHiDecision_TIS", &Dplus_L0ElectronHiDecision_TIS, &b_Dplus_L0ElectronHiDecision_TIS);
   fChain->SetBranchAddress("Dplus_L0ElectronHiDecision_TOS", &Dplus_L0ElectronHiDecision_TOS, &b_Dplus_L0ElectronHiDecision_TOS);
   fChain->SetBranchAddress("Dplus_L0MuonDecision_Dec", &Dplus_L0MuonDecision_Dec, &b_Dplus_L0MuonDecision_Dec);
   fChain->SetBranchAddress("Dplus_L0MuonDecision_TIS", &Dplus_L0MuonDecision_TIS, &b_Dplus_L0MuonDecision_TIS);
   fChain->SetBranchAddress("Dplus_L0MuonDecision_TOS", &Dplus_L0MuonDecision_TOS, &b_Dplus_L0MuonDecision_TOS);
   fChain->SetBranchAddress("Dplus_L0DiMuonDecision_Dec", &Dplus_L0DiMuonDecision_Dec, &b_Dplus_L0DiMuonDecision_Dec);
   fChain->SetBranchAddress("Dplus_L0DiMuonDecision_TIS", &Dplus_L0DiMuonDecision_TIS, &b_Dplus_L0DiMuonDecision_TIS);
   fChain->SetBranchAddress("Dplus_L0DiMuonDecision_TOS", &Dplus_L0DiMuonDecision_TOS, &b_Dplus_L0DiMuonDecision_TOS);
   fChain->SetBranchAddress("Dplus_L0MuonHighDecision_Dec", &Dplus_L0MuonHighDecision_Dec, &b_Dplus_L0MuonHighDecision_Dec);
   fChain->SetBranchAddress("Dplus_L0MuonHighDecision_TIS", &Dplus_L0MuonHighDecision_TIS, &b_Dplus_L0MuonHighDecision_TIS);
   fChain->SetBranchAddress("Dplus_L0MuonHighDecision_TOS", &Dplus_L0MuonHighDecision_TOS, &b_Dplus_L0MuonHighDecision_TOS);
   fChain->SetBranchAddress("Dplus_Hlt1TrackMVADecision_Dec", &Dplus_Hlt1TrackMVADecision_Dec, &b_Dplus_Hlt1TrackMVADecision_Dec);
   fChain->SetBranchAddress("Dplus_Hlt1TrackMVADecision_TIS", &Dplus_Hlt1TrackMVADecision_TIS, &b_Dplus_Hlt1TrackMVADecision_TIS);
   fChain->SetBranchAddress("Dplus_Hlt1TrackMVADecision_TOS", &Dplus_Hlt1TrackMVADecision_TOS, &b_Dplus_Hlt1TrackMVADecision_TOS);
   fChain->SetBranchAddress("Dplus_Hlt1TwoTrackMVADecision_Dec", &Dplus_Hlt1TwoTrackMVADecision_Dec, &b_Dplus_Hlt1TwoTrackMVADecision_Dec);
   fChain->SetBranchAddress("Dplus_Hlt1TwoTrackMVADecision_TIS", &Dplus_Hlt1TwoTrackMVADecision_TIS, &b_Dplus_Hlt1TwoTrackMVADecision_TIS);
   fChain->SetBranchAddress("Dplus_Hlt1TwoTrackMVADecision_TOS", &Dplus_Hlt1TwoTrackMVADecision_TOS, &b_Dplus_Hlt1TwoTrackMVADecision_TOS);
   fChain->SetBranchAddress("Dplus_Hlt1GlobalDecision_Dec", &Dplus_Hlt1GlobalDecision_Dec, &b_Dplus_Hlt1GlobalDecision_Dec);
   fChain->SetBranchAddress("Dplus_Hlt1GlobalDecision_TIS", &Dplus_Hlt1GlobalDecision_TIS, &b_Dplus_Hlt1GlobalDecision_TIS);
   fChain->SetBranchAddress("Dplus_Hlt1GlobalDecision_TOS", &Dplus_Hlt1GlobalDecision_TOS, &b_Dplus_Hlt1GlobalDecision_TOS);
   fChain->SetBranchAddress("Dplus_Hlt1TrackAllL0Decision_Dec", &Dplus_Hlt1TrackAllL0Decision_Dec, &b_Dplus_Hlt1TrackAllL0Decision_Dec);
   fChain->SetBranchAddress("Dplus_Hlt1TrackAllL0Decision_TIS", &Dplus_Hlt1TrackAllL0Decision_TIS, &b_Dplus_Hlt1TrackAllL0Decision_TIS);
   fChain->SetBranchAddress("Dplus_Hlt1TrackAllL0Decision_TOS", &Dplus_Hlt1TrackAllL0Decision_TOS, &b_Dplus_Hlt1TrackAllL0Decision_TOS);
   fChain->SetBranchAddress("Dplus_Hlt2Topo2BodyBBDTDecision_Dec", &Dplus_Hlt2Topo2BodyBBDTDecision_Dec, &b_Dplus_Hlt2Topo2BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("Dplus_Hlt2Topo2BodyBBDTDecision_TIS", &Dplus_Hlt2Topo2BodyBBDTDecision_TIS, &b_Dplus_Hlt2Topo2BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("Dplus_Hlt2Topo2BodyBBDTDecision_TOS", &Dplus_Hlt2Topo2BodyBBDTDecision_TOS, &b_Dplus_Hlt2Topo2BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("Dplus_Hlt2Topo3BodyBBDTDecision_Dec", &Dplus_Hlt2Topo3BodyBBDTDecision_Dec, &b_Dplus_Hlt2Topo3BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("Dplus_Hlt2Topo3BodyBBDTDecision_TIS", &Dplus_Hlt2Topo3BodyBBDTDecision_TIS, &b_Dplus_Hlt2Topo3BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("Dplus_Hlt2Topo3BodyBBDTDecision_TOS", &Dplus_Hlt2Topo3BodyBBDTDecision_TOS, &b_Dplus_Hlt2Topo3BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("Dplus_Hlt2Topo4BodyBBDTDecision_Dec", &Dplus_Hlt2Topo4BodyBBDTDecision_Dec, &b_Dplus_Hlt2Topo4BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("Dplus_Hlt2Topo4BodyBBDTDecision_TIS", &Dplus_Hlt2Topo4BodyBBDTDecision_TIS, &b_Dplus_Hlt2Topo4BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("Dplus_Hlt2Topo4BodyBBDTDecision_TOS", &Dplus_Hlt2Topo4BodyBBDTDecision_TOS, &b_Dplus_Hlt2Topo4BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("Dplus_Hlt2BHadB02PpPpPmPmDecision_Dec", &Dplus_Hlt2BHadB02PpPpPmPmDecision_Dec, &b_Dplus_Hlt2BHadB02PpPpPmPmDecision_Dec);
   fChain->SetBranchAddress("Dplus_Hlt2BHadB02PpPpPmPmDecision_TIS", &Dplus_Hlt2BHadB02PpPpPmPmDecision_TIS, &b_Dplus_Hlt2BHadB02PpPpPmPmDecision_TIS);
   fChain->SetBranchAddress("Dplus_Hlt2BHadB02PpPpPmPmDecision_TOS", &Dplus_Hlt2BHadB02PpPpPmPmDecision_TOS, &b_Dplus_Hlt2BHadB02PpPpPmPmDecision_TOS);
   fChain->SetBranchAddress("Dplus_Hlt2Topo2BodyDecision_Dec", &Dplus_Hlt2Topo2BodyDecision_Dec, &b_Dplus_Hlt2Topo2BodyDecision_Dec);
   fChain->SetBranchAddress("Dplus_Hlt2Topo2BodyDecision_TIS", &Dplus_Hlt2Topo2BodyDecision_TIS, &b_Dplus_Hlt2Topo2BodyDecision_TIS);
   fChain->SetBranchAddress("Dplus_Hlt2Topo2BodyDecision_TOS", &Dplus_Hlt2Topo2BodyDecision_TOS, &b_Dplus_Hlt2Topo2BodyDecision_TOS);
   fChain->SetBranchAddress("Dplus_Hlt2Topo3BodyDecision_Dec", &Dplus_Hlt2Topo3BodyDecision_Dec, &b_Dplus_Hlt2Topo3BodyDecision_Dec);
   fChain->SetBranchAddress("Dplus_Hlt2Topo3BodyDecision_TIS", &Dplus_Hlt2Topo3BodyDecision_TIS, &b_Dplus_Hlt2Topo3BodyDecision_TIS);
   fChain->SetBranchAddress("Dplus_Hlt2Topo3BodyDecision_TOS", &Dplus_Hlt2Topo3BodyDecision_TOS, &b_Dplus_Hlt2Topo3BodyDecision_TOS);
   fChain->SetBranchAddress("Dplus_Hlt2Topo4BodyDecision_Dec", &Dplus_Hlt2Topo4BodyDecision_Dec, &b_Dplus_Hlt2Topo4BodyDecision_Dec);
   fChain->SetBranchAddress("Dplus_Hlt2Topo4BodyDecision_TIS", &Dplus_Hlt2Topo4BodyDecision_TIS, &b_Dplus_Hlt2Topo4BodyDecision_TIS);
   fChain->SetBranchAddress("Dplus_Hlt2Topo4BodyDecision_TOS", &Dplus_Hlt2Topo4BodyDecision_TOS, &b_Dplus_Hlt2Topo4BodyDecision_TOS);
   fChain->SetBranchAddress("Dplus_Hlt2GlobalDecision_Dec", &Dplus_Hlt2GlobalDecision_Dec, &b_Dplus_Hlt2GlobalDecision_Dec);
   fChain->SetBranchAddress("Dplus_Hlt2GlobalDecision_TIS", &Dplus_Hlt2GlobalDecision_TIS, &b_Dplus_Hlt2GlobalDecision_TIS);
   fChain->SetBranchAddress("Dplus_Hlt2GlobalDecision_TOS", &Dplus_Hlt2GlobalDecision_TOS, &b_Dplus_Hlt2GlobalDecision_TOS);
   fChain->SetBranchAddress("Dplus_X", &Dplus_X, &b_Dplus_X);
   fChain->SetBranchAddress("Dplus_Y", &Dplus_Y, &b_Dplus_Y);
   fChain->SetBranchAddress("Kminus_Dplus_ETA", &Kminus_Dplus_ETA, &b_Kminus_Dplus_ETA);
   fChain->SetBranchAddress("Kminus_Dplus_MinIPCHI2", &Kminus_Dplus_MinIPCHI2, &b_Kminus_Dplus_MinIPCHI2);
   fChain->SetBranchAddress("Kminus_Dplus_MC12TuneV2_ProbNNe", &Kminus_Dplus_MC12TuneV2_ProbNNe, &b_Kminus_Dplus_MC12TuneV2_ProbNNe);
   fChain->SetBranchAddress("Kminus_Dplus_MC12TuneV2_ProbNNmu", &Kminus_Dplus_MC12TuneV2_ProbNNmu, &b_Kminus_Dplus_MC12TuneV2_ProbNNmu);
   fChain->SetBranchAddress("Kminus_Dplus_MC12TuneV2_ProbNNpi", &Kminus_Dplus_MC12TuneV2_ProbNNpi, &b_Kminus_Dplus_MC12TuneV2_ProbNNpi);
   fChain->SetBranchAddress("Kminus_Dplus_MC12TuneV2_ProbNNk", &Kminus_Dplus_MC12TuneV2_ProbNNk, &b_Kminus_Dplus_MC12TuneV2_ProbNNk);
   fChain->SetBranchAddress("Kminus_Dplus_MC12TuneV2_ProbNNp", &Kminus_Dplus_MC12TuneV2_ProbNNp, &b_Kminus_Dplus_MC12TuneV2_ProbNNp);
   fChain->SetBranchAddress("Kminus_Dplus_MC12TuneV2_ProbNNghost", &Kminus_Dplus_MC12TuneV2_ProbNNghost, &b_Kminus_Dplus_MC12TuneV2_ProbNNghost);
   fChain->SetBranchAddress("Kminus_Dplus_MC12TuneV3_ProbNNe", &Kminus_Dplus_MC12TuneV3_ProbNNe, &b_Kminus_Dplus_MC12TuneV3_ProbNNe);
   fChain->SetBranchAddress("Kminus_Dplus_MC12TuneV3_ProbNNmu", &Kminus_Dplus_MC12TuneV3_ProbNNmu, &b_Kminus_Dplus_MC12TuneV3_ProbNNmu);
   fChain->SetBranchAddress("Kminus_Dplus_MC12TuneV3_ProbNNpi", &Kminus_Dplus_MC12TuneV3_ProbNNpi, &b_Kminus_Dplus_MC12TuneV3_ProbNNpi);
   fChain->SetBranchAddress("Kminus_Dplus_MC12TuneV3_ProbNNk", &Kminus_Dplus_MC12TuneV3_ProbNNk, &b_Kminus_Dplus_MC12TuneV3_ProbNNk);
   fChain->SetBranchAddress("Kminus_Dplus_MC12TuneV3_ProbNNp", &Kminus_Dplus_MC12TuneV3_ProbNNp, &b_Kminus_Dplus_MC12TuneV3_ProbNNp);
   fChain->SetBranchAddress("Kminus_Dplus_MC12TuneV3_ProbNNghost", &Kminus_Dplus_MC12TuneV3_ProbNNghost, &b_Kminus_Dplus_MC12TuneV3_ProbNNghost);
   fChain->SetBranchAddress("Kminus_Dplus_MC12TuneV4_ProbNNe", &Kminus_Dplus_MC12TuneV4_ProbNNe, &b_Kminus_Dplus_MC12TuneV4_ProbNNe);
   fChain->SetBranchAddress("Kminus_Dplus_MC12TuneV4_ProbNNmu", &Kminus_Dplus_MC12TuneV4_ProbNNmu, &b_Kminus_Dplus_MC12TuneV4_ProbNNmu);
   fChain->SetBranchAddress("Kminus_Dplus_MC12TuneV4_ProbNNpi", &Kminus_Dplus_MC12TuneV4_ProbNNpi, &b_Kminus_Dplus_MC12TuneV4_ProbNNpi);
   fChain->SetBranchAddress("Kminus_Dplus_MC12TuneV4_ProbNNk", &Kminus_Dplus_MC12TuneV4_ProbNNk, &b_Kminus_Dplus_MC12TuneV4_ProbNNk);
   fChain->SetBranchAddress("Kminus_Dplus_MC12TuneV4_ProbNNp", &Kminus_Dplus_MC12TuneV4_ProbNNp, &b_Kminus_Dplus_MC12TuneV4_ProbNNp);
   fChain->SetBranchAddress("Kminus_Dplus_MC12TuneV4_ProbNNghost", &Kminus_Dplus_MC12TuneV4_ProbNNghost, &b_Kminus_Dplus_MC12TuneV4_ProbNNghost);
   fChain->SetBranchAddress("Kminus_Dplus_MC15TuneV1_ProbNNe", &Kminus_Dplus_MC15TuneV1_ProbNNe, &b_Kminus_Dplus_MC15TuneV1_ProbNNe);
   fChain->SetBranchAddress("Kminus_Dplus_MC15TuneV1_ProbNNmu", &Kminus_Dplus_MC15TuneV1_ProbNNmu, &b_Kminus_Dplus_MC15TuneV1_ProbNNmu);
   fChain->SetBranchAddress("Kminus_Dplus_MC15TuneV1_ProbNNpi", &Kminus_Dplus_MC15TuneV1_ProbNNpi, &b_Kminus_Dplus_MC15TuneV1_ProbNNpi);
   fChain->SetBranchAddress("Kminus_Dplus_MC15TuneV1_ProbNNk", &Kminus_Dplus_MC15TuneV1_ProbNNk, &b_Kminus_Dplus_MC15TuneV1_ProbNNk);
   fChain->SetBranchAddress("Kminus_Dplus_MC15TuneV1_ProbNNp", &Kminus_Dplus_MC15TuneV1_ProbNNp, &b_Kminus_Dplus_MC15TuneV1_ProbNNp);
   fChain->SetBranchAddress("Kminus_Dplus_MC15TuneV1_ProbNNghost", &Kminus_Dplus_MC15TuneV1_ProbNNghost, &b_Kminus_Dplus_MC15TuneV1_ProbNNghost);
   fChain->SetBranchAddress("Kminus_Dplus_CosTheta", &Kminus_Dplus_CosTheta, &b_Kminus_Dplus_CosTheta);
   fChain->SetBranchAddress("Kminus_Dplus_OWNPV_X", &Kminus_Dplus_OWNPV_X, &b_Kminus_Dplus_OWNPV_X);
   fChain->SetBranchAddress("Kminus_Dplus_OWNPV_Y", &Kminus_Dplus_OWNPV_Y, &b_Kminus_Dplus_OWNPV_Y);
   fChain->SetBranchAddress("Kminus_Dplus_OWNPV_Z", &Kminus_Dplus_OWNPV_Z, &b_Kminus_Dplus_OWNPV_Z);
   fChain->SetBranchAddress("Kminus_Dplus_OWNPV_XERR", &Kminus_Dplus_OWNPV_XERR, &b_Kminus_Dplus_OWNPV_XERR);
   fChain->SetBranchAddress("Kminus_Dplus_OWNPV_YERR", &Kminus_Dplus_OWNPV_YERR, &b_Kminus_Dplus_OWNPV_YERR);
   fChain->SetBranchAddress("Kminus_Dplus_OWNPV_ZERR", &Kminus_Dplus_OWNPV_ZERR, &b_Kminus_Dplus_OWNPV_ZERR);
   fChain->SetBranchAddress("Kminus_Dplus_OWNPV_CHI2", &Kminus_Dplus_OWNPV_CHI2, &b_Kminus_Dplus_OWNPV_CHI2);
   fChain->SetBranchAddress("Kminus_Dplus_OWNPV_NDOF", &Kminus_Dplus_OWNPV_NDOF, &b_Kminus_Dplus_OWNPV_NDOF);
   fChain->SetBranchAddress("Kminus_Dplus_OWNPV_COV_", Kminus_Dplus_OWNPV_COV_, &b_Kminus_Dplus_OWNPV_COV_);
   fChain->SetBranchAddress("Kminus_Dplus_IP_OWNPV", &Kminus_Dplus_IP_OWNPV, &b_Kminus_Dplus_IP_OWNPV);
   fChain->SetBranchAddress("Kminus_Dplus_IPCHI2_OWNPV", &Kminus_Dplus_IPCHI2_OWNPV, &b_Kminus_Dplus_IPCHI2_OWNPV);
   fChain->SetBranchAddress("Kminus_Dplus_ORIVX_X", &Kminus_Dplus_ORIVX_X, &b_Kminus_Dplus_ORIVX_X);
   fChain->SetBranchAddress("Kminus_Dplus_ORIVX_Y", &Kminus_Dplus_ORIVX_Y, &b_Kminus_Dplus_ORIVX_Y);
   fChain->SetBranchAddress("Kminus_Dplus_ORIVX_Z", &Kminus_Dplus_ORIVX_Z, &b_Kminus_Dplus_ORIVX_Z);
   fChain->SetBranchAddress("Kminus_Dplus_ORIVX_XERR", &Kminus_Dplus_ORIVX_XERR, &b_Kminus_Dplus_ORIVX_XERR);
   fChain->SetBranchAddress("Kminus_Dplus_ORIVX_YERR", &Kminus_Dplus_ORIVX_YERR, &b_Kminus_Dplus_ORIVX_YERR);
   fChain->SetBranchAddress("Kminus_Dplus_ORIVX_ZERR", &Kminus_Dplus_ORIVX_ZERR, &b_Kminus_Dplus_ORIVX_ZERR);
   fChain->SetBranchAddress("Kminus_Dplus_ORIVX_CHI2", &Kminus_Dplus_ORIVX_CHI2, &b_Kminus_Dplus_ORIVX_CHI2);
   fChain->SetBranchAddress("Kminus_Dplus_ORIVX_NDOF", &Kminus_Dplus_ORIVX_NDOF, &b_Kminus_Dplus_ORIVX_NDOF);
   fChain->SetBranchAddress("Kminus_Dplus_ORIVX_COV_", Kminus_Dplus_ORIVX_COV_, &b_Kminus_Dplus_ORIVX_COV_);
   fChain->SetBranchAddress("Kminus_Dplus_P", &Kminus_Dplus_P, &b_Kminus_Dplus_P);
   fChain->SetBranchAddress("Kminus_Dplus_PT", &Kminus_Dplus_PT, &b_Kminus_Dplus_PT);
   fChain->SetBranchAddress("Kminus_Dplus_PE", &Kminus_Dplus_PE, &b_Kminus_Dplus_PE);
   fChain->SetBranchAddress("Kminus_Dplus_PX", &Kminus_Dplus_PX, &b_Kminus_Dplus_PX);
   fChain->SetBranchAddress("Kminus_Dplus_PY", &Kminus_Dplus_PY, &b_Kminus_Dplus_PY);
   fChain->SetBranchAddress("Kminus_Dplus_PZ", &Kminus_Dplus_PZ, &b_Kminus_Dplus_PZ);
   fChain->SetBranchAddress("Kminus_Dplus_REFPX", &Kminus_Dplus_REFPX, &b_Kminus_Dplus_REFPX);
   fChain->SetBranchAddress("Kminus_Dplus_REFPY", &Kminus_Dplus_REFPY, &b_Kminus_Dplus_REFPY);
   fChain->SetBranchAddress("Kminus_Dplus_REFPZ", &Kminus_Dplus_REFPZ, &b_Kminus_Dplus_REFPZ);
   fChain->SetBranchAddress("Kminus_Dplus_M", &Kminus_Dplus_M, &b_Kminus_Dplus_M);
   fChain->SetBranchAddress("Kminus_Dplus_AtVtx_PE", &Kminus_Dplus_AtVtx_PE, &b_Kminus_Dplus_AtVtx_PE);
   fChain->SetBranchAddress("Kminus_Dplus_AtVtx_PX", &Kminus_Dplus_AtVtx_PX, &b_Kminus_Dplus_AtVtx_PX);
   fChain->SetBranchAddress("Kminus_Dplus_AtVtx_PY", &Kminus_Dplus_AtVtx_PY, &b_Kminus_Dplus_AtVtx_PY);
   fChain->SetBranchAddress("Kminus_Dplus_AtVtx_PZ", &Kminus_Dplus_AtVtx_PZ, &b_Kminus_Dplus_AtVtx_PZ);
   fChain->SetBranchAddress("Kminus_Dplus_TRUEID", &Kminus_Dplus_TRUEID, &b_Kminus_Dplus_TRUEID);
   fChain->SetBranchAddress("Kminus_Dplus_TRUEP_E", &Kminus_Dplus_TRUEP_E, &b_Kminus_Dplus_TRUEP_E);
   fChain->SetBranchAddress("Kminus_Dplus_TRUEP_X", &Kminus_Dplus_TRUEP_X, &b_Kminus_Dplus_TRUEP_X);
   fChain->SetBranchAddress("Kminus_Dplus_TRUEP_Y", &Kminus_Dplus_TRUEP_Y, &b_Kminus_Dplus_TRUEP_Y);
   fChain->SetBranchAddress("Kminus_Dplus_TRUEP_Z", &Kminus_Dplus_TRUEP_Z, &b_Kminus_Dplus_TRUEP_Z);
   fChain->SetBranchAddress("Kminus_Dplus_TRUEPT", &Kminus_Dplus_TRUEPT, &b_Kminus_Dplus_TRUEPT);
   fChain->SetBranchAddress("Kminus_Dplus_TRUEORIGINVERTEX_X", &Kminus_Dplus_TRUEORIGINVERTEX_X, &b_Kminus_Dplus_TRUEORIGINVERTEX_X);
   fChain->SetBranchAddress("Kminus_Dplus_TRUEORIGINVERTEX_Y", &Kminus_Dplus_TRUEORIGINVERTEX_Y, &b_Kminus_Dplus_TRUEORIGINVERTEX_Y);
   fChain->SetBranchAddress("Kminus_Dplus_TRUEORIGINVERTEX_Z", &Kminus_Dplus_TRUEORIGINVERTEX_Z, &b_Kminus_Dplus_TRUEORIGINVERTEX_Z);
   fChain->SetBranchAddress("Kminus_Dplus_TRUEENDVERTEX_X", &Kminus_Dplus_TRUEENDVERTEX_X, &b_Kminus_Dplus_TRUEENDVERTEX_X);
   fChain->SetBranchAddress("Kminus_Dplus_TRUEENDVERTEX_Y", &Kminus_Dplus_TRUEENDVERTEX_Y, &b_Kminus_Dplus_TRUEENDVERTEX_Y);
   fChain->SetBranchAddress("Kminus_Dplus_TRUEENDVERTEX_Z", &Kminus_Dplus_TRUEENDVERTEX_Z, &b_Kminus_Dplus_TRUEENDVERTEX_Z);
   fChain->SetBranchAddress("Kminus_Dplus_TRUEISSTABLE", &Kminus_Dplus_TRUEISSTABLE, &b_Kminus_Dplus_TRUEISSTABLE);
   fChain->SetBranchAddress("Kminus_Dplus_TRUETAU", &Kminus_Dplus_TRUETAU, &b_Kminus_Dplus_TRUETAU);
   fChain->SetBranchAddress("Kminus_Dplus_ID", &Kminus_Dplus_ID, &b_Kminus_Dplus_ID);
   fChain->SetBranchAddress("Kminus_Dplus_CombDLLMu", &Kminus_Dplus_CombDLLMu, &b_Kminus_Dplus_CombDLLMu);
   fChain->SetBranchAddress("Kminus_Dplus_ProbNNmu", &Kminus_Dplus_ProbNNmu, &b_Kminus_Dplus_ProbNNmu);
   fChain->SetBranchAddress("Kminus_Dplus_ProbNNghost", &Kminus_Dplus_ProbNNghost, &b_Kminus_Dplus_ProbNNghost);
   fChain->SetBranchAddress("Kminus_Dplus_InMuonAcc", &Kminus_Dplus_InMuonAcc, &b_Kminus_Dplus_InMuonAcc);
   fChain->SetBranchAddress("Kminus_Dplus_MuonDist2", &Kminus_Dplus_MuonDist2, &b_Kminus_Dplus_MuonDist2);
   fChain->SetBranchAddress("Kminus_Dplus_regionInM2", &Kminus_Dplus_regionInM2, &b_Kminus_Dplus_regionInM2);
   fChain->SetBranchAddress("Kminus_Dplus_hasMuon", &Kminus_Dplus_hasMuon, &b_Kminus_Dplus_hasMuon);
   fChain->SetBranchAddress("Kminus_Dplus_isMuon", &Kminus_Dplus_isMuon, &b_Kminus_Dplus_isMuon);
   fChain->SetBranchAddress("Kminus_Dplus_isMuonLoose", &Kminus_Dplus_isMuonLoose, &b_Kminus_Dplus_isMuonLoose);
   fChain->SetBranchAddress("Kminus_Dplus_NShared", &Kminus_Dplus_NShared, &b_Kminus_Dplus_NShared);
   fChain->SetBranchAddress("Kminus_Dplus_MuonLLmu", &Kminus_Dplus_MuonLLmu, &b_Kminus_Dplus_MuonLLmu);
   fChain->SetBranchAddress("Kminus_Dplus_MuonLLbg", &Kminus_Dplus_MuonLLbg, &b_Kminus_Dplus_MuonLLbg);
   fChain->SetBranchAddress("Kminus_Dplus_isMuonFromProto", &Kminus_Dplus_isMuonFromProto, &b_Kminus_Dplus_isMuonFromProto);
   fChain->SetBranchAddress("Kminus_Dplus_PIDe", &Kminus_Dplus_PIDe, &b_Kminus_Dplus_PIDe);
   fChain->SetBranchAddress("Kminus_Dplus_PIDmu", &Kminus_Dplus_PIDmu, &b_Kminus_Dplus_PIDmu);
   fChain->SetBranchAddress("Kminus_Dplus_PIDK", &Kminus_Dplus_PIDK, &b_Kminus_Dplus_PIDK);
   fChain->SetBranchAddress("Kminus_Dplus_PIDp", &Kminus_Dplus_PIDp, &b_Kminus_Dplus_PIDp);
   fChain->SetBranchAddress("Kminus_Dplus_PIDd", &Kminus_Dplus_PIDd, &b_Kminus_Dplus_PIDd);
   fChain->SetBranchAddress("Kminus_Dplus_ProbNNe", &Kminus_Dplus_ProbNNe, &b_Kminus_Dplus_ProbNNe);
   fChain->SetBranchAddress("Kminus_Dplus_ProbNNk", &Kminus_Dplus_ProbNNk, &b_Kminus_Dplus_ProbNNk);
   fChain->SetBranchAddress("Kminus_Dplus_ProbNNp", &Kminus_Dplus_ProbNNp, &b_Kminus_Dplus_ProbNNp);
   fChain->SetBranchAddress("Kminus_Dplus_ProbNNpi", &Kminus_Dplus_ProbNNpi, &b_Kminus_Dplus_ProbNNpi);
   fChain->SetBranchAddress("Kminus_Dplus_ProbNNd", &Kminus_Dplus_ProbNNd, &b_Kminus_Dplus_ProbNNd);
   fChain->SetBranchAddress("Kminus_Dplus_hasRich", &Kminus_Dplus_hasRich, &b_Kminus_Dplus_hasRich);
   fChain->SetBranchAddress("Kminus_Dplus_UsedRichAerogel", &Kminus_Dplus_UsedRichAerogel, &b_Kminus_Dplus_UsedRichAerogel);
   fChain->SetBranchAddress("Kminus_Dplus_UsedRich1Gas", &Kminus_Dplus_UsedRich1Gas, &b_Kminus_Dplus_UsedRich1Gas);
   fChain->SetBranchAddress("Kminus_Dplus_UsedRich2Gas", &Kminus_Dplus_UsedRich2Gas, &b_Kminus_Dplus_UsedRich2Gas);
   fChain->SetBranchAddress("Kminus_Dplus_RichAboveElThres", &Kminus_Dplus_RichAboveElThres, &b_Kminus_Dplus_RichAboveElThres);
   fChain->SetBranchAddress("Kminus_Dplus_RichAboveMuThres", &Kminus_Dplus_RichAboveMuThres, &b_Kminus_Dplus_RichAboveMuThres);
   fChain->SetBranchAddress("Kminus_Dplus_RichAbovePiThres", &Kminus_Dplus_RichAbovePiThres, &b_Kminus_Dplus_RichAbovePiThres);
   fChain->SetBranchAddress("Kminus_Dplus_RichAboveKaThres", &Kminus_Dplus_RichAboveKaThres, &b_Kminus_Dplus_RichAboveKaThres);
   fChain->SetBranchAddress("Kminus_Dplus_RichAbovePrThres", &Kminus_Dplus_RichAbovePrThres, &b_Kminus_Dplus_RichAbovePrThres);
   fChain->SetBranchAddress("Kminus_Dplus_hasCalo", &Kminus_Dplus_hasCalo, &b_Kminus_Dplus_hasCalo);
   fChain->SetBranchAddress("Kminus_Dplus_L0Global_Dec", &Kminus_Dplus_L0Global_Dec, &b_Kminus_Dplus_L0Global_Dec);
   fChain->SetBranchAddress("Kminus_Dplus_L0Global_TIS", &Kminus_Dplus_L0Global_TIS, &b_Kminus_Dplus_L0Global_TIS);
   fChain->SetBranchAddress("Kminus_Dplus_L0Global_TOS", &Kminus_Dplus_L0Global_TOS, &b_Kminus_Dplus_L0Global_TOS);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt1Global_Dec", &Kminus_Dplus_Hlt1Global_Dec, &b_Kminus_Dplus_Hlt1Global_Dec);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt1Global_TIS", &Kminus_Dplus_Hlt1Global_TIS, &b_Kminus_Dplus_Hlt1Global_TIS);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt1Global_TOS", &Kminus_Dplus_Hlt1Global_TOS, &b_Kminus_Dplus_Hlt1Global_TOS);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt1Phys_Dec", &Kminus_Dplus_Hlt1Phys_Dec, &b_Kminus_Dplus_Hlt1Phys_Dec);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt1Phys_TIS", &Kminus_Dplus_Hlt1Phys_TIS, &b_Kminus_Dplus_Hlt1Phys_TIS);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt1Phys_TOS", &Kminus_Dplus_Hlt1Phys_TOS, &b_Kminus_Dplus_Hlt1Phys_TOS);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt2Global_Dec", &Kminus_Dplus_Hlt2Global_Dec, &b_Kminus_Dplus_Hlt2Global_Dec);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt2Global_TIS", &Kminus_Dplus_Hlt2Global_TIS, &b_Kminus_Dplus_Hlt2Global_TIS);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt2Global_TOS", &Kminus_Dplus_Hlt2Global_TOS, &b_Kminus_Dplus_Hlt2Global_TOS);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt2Phys_Dec", &Kminus_Dplus_Hlt2Phys_Dec, &b_Kminus_Dplus_Hlt2Phys_Dec);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt2Phys_TIS", &Kminus_Dplus_Hlt2Phys_TIS, &b_Kminus_Dplus_Hlt2Phys_TIS);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt2Phys_TOS", &Kminus_Dplus_Hlt2Phys_TOS, &b_Kminus_Dplus_Hlt2Phys_TOS);
   fChain->SetBranchAddress("Kminus_Dplus_L0HadronDecision_Dec", &Kminus_Dplus_L0HadronDecision_Dec, &b_Kminus_Dplus_L0HadronDecision_Dec);
   fChain->SetBranchAddress("Kminus_Dplus_L0HadronDecision_TIS", &Kminus_Dplus_L0HadronDecision_TIS, &b_Kminus_Dplus_L0HadronDecision_TIS);
   fChain->SetBranchAddress("Kminus_Dplus_L0HadronDecision_TOS", &Kminus_Dplus_L0HadronDecision_TOS, &b_Kminus_Dplus_L0HadronDecision_TOS);
   fChain->SetBranchAddress("Kminus_Dplus_L0ElectronDecision_Dec", &Kminus_Dplus_L0ElectronDecision_Dec, &b_Kminus_Dplus_L0ElectronDecision_Dec);
   fChain->SetBranchAddress("Kminus_Dplus_L0ElectronDecision_TIS", &Kminus_Dplus_L0ElectronDecision_TIS, &b_Kminus_Dplus_L0ElectronDecision_TIS);
   fChain->SetBranchAddress("Kminus_Dplus_L0ElectronDecision_TOS", &Kminus_Dplus_L0ElectronDecision_TOS, &b_Kminus_Dplus_L0ElectronDecision_TOS);
   fChain->SetBranchAddress("Kminus_Dplus_L0ElectronHiDecision_Dec", &Kminus_Dplus_L0ElectronHiDecision_Dec, &b_Kminus_Dplus_L0ElectronHiDecision_Dec);
   fChain->SetBranchAddress("Kminus_Dplus_L0ElectronHiDecision_TIS", &Kminus_Dplus_L0ElectronHiDecision_TIS, &b_Kminus_Dplus_L0ElectronHiDecision_TIS);
   fChain->SetBranchAddress("Kminus_Dplus_L0ElectronHiDecision_TOS", &Kminus_Dplus_L0ElectronHiDecision_TOS, &b_Kminus_Dplus_L0ElectronHiDecision_TOS);
   fChain->SetBranchAddress("Kminus_Dplus_L0MuonDecision_Dec", &Kminus_Dplus_L0MuonDecision_Dec, &b_Kminus_Dplus_L0MuonDecision_Dec);
   fChain->SetBranchAddress("Kminus_Dplus_L0MuonDecision_TIS", &Kminus_Dplus_L0MuonDecision_TIS, &b_Kminus_Dplus_L0MuonDecision_TIS);
   fChain->SetBranchAddress("Kminus_Dplus_L0MuonDecision_TOS", &Kminus_Dplus_L0MuonDecision_TOS, &b_Kminus_Dplus_L0MuonDecision_TOS);
   fChain->SetBranchAddress("Kminus_Dplus_L0DiMuonDecision_Dec", &Kminus_Dplus_L0DiMuonDecision_Dec, &b_Kminus_Dplus_L0DiMuonDecision_Dec);
   fChain->SetBranchAddress("Kminus_Dplus_L0DiMuonDecision_TIS", &Kminus_Dplus_L0DiMuonDecision_TIS, &b_Kminus_Dplus_L0DiMuonDecision_TIS);
   fChain->SetBranchAddress("Kminus_Dplus_L0DiMuonDecision_TOS", &Kminus_Dplus_L0DiMuonDecision_TOS, &b_Kminus_Dplus_L0DiMuonDecision_TOS);
   fChain->SetBranchAddress("Kminus_Dplus_L0MuonHighDecision_Dec", &Kminus_Dplus_L0MuonHighDecision_Dec, &b_Kminus_Dplus_L0MuonHighDecision_Dec);
   fChain->SetBranchAddress("Kminus_Dplus_L0MuonHighDecision_TIS", &Kminus_Dplus_L0MuonHighDecision_TIS, &b_Kminus_Dplus_L0MuonHighDecision_TIS);
   fChain->SetBranchAddress("Kminus_Dplus_L0MuonHighDecision_TOS", &Kminus_Dplus_L0MuonHighDecision_TOS, &b_Kminus_Dplus_L0MuonHighDecision_TOS);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt1TrackMVADecision_Dec", &Kminus_Dplus_Hlt1TrackMVADecision_Dec, &b_Kminus_Dplus_Hlt1TrackMVADecision_Dec);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt1TrackMVADecision_TIS", &Kminus_Dplus_Hlt1TrackMVADecision_TIS, &b_Kminus_Dplus_Hlt1TrackMVADecision_TIS);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt1TrackMVADecision_TOS", &Kminus_Dplus_Hlt1TrackMVADecision_TOS, &b_Kminus_Dplus_Hlt1TrackMVADecision_TOS);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt1TwoTrackMVADecision_Dec", &Kminus_Dplus_Hlt1TwoTrackMVADecision_Dec, &b_Kminus_Dplus_Hlt1TwoTrackMVADecision_Dec);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt1TwoTrackMVADecision_TIS", &Kminus_Dplus_Hlt1TwoTrackMVADecision_TIS, &b_Kminus_Dplus_Hlt1TwoTrackMVADecision_TIS);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt1TwoTrackMVADecision_TOS", &Kminus_Dplus_Hlt1TwoTrackMVADecision_TOS, &b_Kminus_Dplus_Hlt1TwoTrackMVADecision_TOS);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt1GlobalDecision_Dec", &Kminus_Dplus_Hlt1GlobalDecision_Dec, &b_Kminus_Dplus_Hlt1GlobalDecision_Dec);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt1GlobalDecision_TIS", &Kminus_Dplus_Hlt1GlobalDecision_TIS, &b_Kminus_Dplus_Hlt1GlobalDecision_TIS);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt1GlobalDecision_TOS", &Kminus_Dplus_Hlt1GlobalDecision_TOS, &b_Kminus_Dplus_Hlt1GlobalDecision_TOS);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt1TrackAllL0Decision_Dec", &Kminus_Dplus_Hlt1TrackAllL0Decision_Dec, &b_Kminus_Dplus_Hlt1TrackAllL0Decision_Dec);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt1TrackAllL0Decision_TIS", &Kminus_Dplus_Hlt1TrackAllL0Decision_TIS, &b_Kminus_Dplus_Hlt1TrackAllL0Decision_TIS);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt1TrackAllL0Decision_TOS", &Kminus_Dplus_Hlt1TrackAllL0Decision_TOS, &b_Kminus_Dplus_Hlt1TrackAllL0Decision_TOS);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt2Topo2BodyBBDTDecision_Dec", &Kminus_Dplus_Hlt2Topo2BodyBBDTDecision_Dec, &b_Kminus_Dplus_Hlt2Topo2BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt2Topo2BodyBBDTDecision_TIS", &Kminus_Dplus_Hlt2Topo2BodyBBDTDecision_TIS, &b_Kminus_Dplus_Hlt2Topo2BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt2Topo2BodyBBDTDecision_TOS", &Kminus_Dplus_Hlt2Topo2BodyBBDTDecision_TOS, &b_Kminus_Dplus_Hlt2Topo2BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt2Topo3BodyBBDTDecision_Dec", &Kminus_Dplus_Hlt2Topo3BodyBBDTDecision_Dec, &b_Kminus_Dplus_Hlt2Topo3BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt2Topo3BodyBBDTDecision_TIS", &Kminus_Dplus_Hlt2Topo3BodyBBDTDecision_TIS, &b_Kminus_Dplus_Hlt2Topo3BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt2Topo3BodyBBDTDecision_TOS", &Kminus_Dplus_Hlt2Topo3BodyBBDTDecision_TOS, &b_Kminus_Dplus_Hlt2Topo3BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt2Topo4BodyBBDTDecision_Dec", &Kminus_Dplus_Hlt2Topo4BodyBBDTDecision_Dec, &b_Kminus_Dplus_Hlt2Topo4BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt2Topo4BodyBBDTDecision_TIS", &Kminus_Dplus_Hlt2Topo4BodyBBDTDecision_TIS, &b_Kminus_Dplus_Hlt2Topo4BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt2Topo4BodyBBDTDecision_TOS", &Kminus_Dplus_Hlt2Topo4BodyBBDTDecision_TOS, &b_Kminus_Dplus_Hlt2Topo4BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt2BHadB02PpPpPmPmDecision_Dec", &Kminus_Dplus_Hlt2BHadB02PpPpPmPmDecision_Dec, &b_Kminus_Dplus_Hlt2BHadB02PpPpPmPmDecision_Dec);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt2BHadB02PpPpPmPmDecision_TIS", &Kminus_Dplus_Hlt2BHadB02PpPpPmPmDecision_TIS, &b_Kminus_Dplus_Hlt2BHadB02PpPpPmPmDecision_TIS);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt2BHadB02PpPpPmPmDecision_TOS", &Kminus_Dplus_Hlt2BHadB02PpPpPmPmDecision_TOS, &b_Kminus_Dplus_Hlt2BHadB02PpPpPmPmDecision_TOS);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt2Topo2BodyDecision_Dec", &Kminus_Dplus_Hlt2Topo2BodyDecision_Dec, &b_Kminus_Dplus_Hlt2Topo2BodyDecision_Dec);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt2Topo2BodyDecision_TIS", &Kminus_Dplus_Hlt2Topo2BodyDecision_TIS, &b_Kminus_Dplus_Hlt2Topo2BodyDecision_TIS);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt2Topo2BodyDecision_TOS", &Kminus_Dplus_Hlt2Topo2BodyDecision_TOS, &b_Kminus_Dplus_Hlt2Topo2BodyDecision_TOS);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt2Topo3BodyDecision_Dec", &Kminus_Dplus_Hlt2Topo3BodyDecision_Dec, &b_Kminus_Dplus_Hlt2Topo3BodyDecision_Dec);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt2Topo3BodyDecision_TIS", &Kminus_Dplus_Hlt2Topo3BodyDecision_TIS, &b_Kminus_Dplus_Hlt2Topo3BodyDecision_TIS);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt2Topo3BodyDecision_TOS", &Kminus_Dplus_Hlt2Topo3BodyDecision_TOS, &b_Kminus_Dplus_Hlt2Topo3BodyDecision_TOS);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt2Topo4BodyDecision_Dec", &Kminus_Dplus_Hlt2Topo4BodyDecision_Dec, &b_Kminus_Dplus_Hlt2Topo4BodyDecision_Dec);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt2Topo4BodyDecision_TIS", &Kminus_Dplus_Hlt2Topo4BodyDecision_TIS, &b_Kminus_Dplus_Hlt2Topo4BodyDecision_TIS);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt2Topo4BodyDecision_TOS", &Kminus_Dplus_Hlt2Topo4BodyDecision_TOS, &b_Kminus_Dplus_Hlt2Topo4BodyDecision_TOS);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt2GlobalDecision_Dec", &Kminus_Dplus_Hlt2GlobalDecision_Dec, &b_Kminus_Dplus_Hlt2GlobalDecision_Dec);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt2GlobalDecision_TIS", &Kminus_Dplus_Hlt2GlobalDecision_TIS, &b_Kminus_Dplus_Hlt2GlobalDecision_TIS);
   fChain->SetBranchAddress("Kminus_Dplus_Hlt2GlobalDecision_TOS", &Kminus_Dplus_Hlt2GlobalDecision_TOS, &b_Kminus_Dplus_Hlt2GlobalDecision_TOS);
   fChain->SetBranchAddress("Kminus_Dplus_TRACK_Type", &Kminus_Dplus_TRACK_Type, &b_Kminus_Dplus_TRACK_Type);
   fChain->SetBranchAddress("Kminus_Dplus_TRACK_Key", &Kminus_Dplus_TRACK_Key, &b_Kminus_Dplus_TRACK_Key);
   fChain->SetBranchAddress("Kminus_Dplus_TRACK_CHI2NDOF", &Kminus_Dplus_TRACK_CHI2NDOF, &b_Kminus_Dplus_TRACK_CHI2NDOF);
   fChain->SetBranchAddress("Kminus_Dplus_TRACK_PCHI2", &Kminus_Dplus_TRACK_PCHI2, &b_Kminus_Dplus_TRACK_PCHI2);
   fChain->SetBranchAddress("Kminus_Dplus_TRACK_MatchCHI2", &Kminus_Dplus_TRACK_MatchCHI2, &b_Kminus_Dplus_TRACK_MatchCHI2);
   fChain->SetBranchAddress("Kminus_Dplus_TRACK_GhostProb", &Kminus_Dplus_TRACK_GhostProb, &b_Kminus_Dplus_TRACK_GhostProb);
   fChain->SetBranchAddress("Kminus_Dplus_TRACK_CloneDist", &Kminus_Dplus_TRACK_CloneDist, &b_Kminus_Dplus_TRACK_CloneDist);
   fChain->SetBranchAddress("Kminus_Dplus_TRACK_Likelihood", &Kminus_Dplus_TRACK_Likelihood, &b_Kminus_Dplus_TRACK_Likelihood);
   fChain->SetBranchAddress("Kminus_Dplus_X", &Kminus_Dplus_X, &b_Kminus_Dplus_X);
   fChain->SetBranchAddress("Kminus_Dplus_Y", &Kminus_Dplus_Y, &b_Kminus_Dplus_Y);
   fChain->SetBranchAddress("piplus1_Dplus_ETA", &piplus1_Dplus_ETA, &b_piplus1_Dplus_ETA);
   fChain->SetBranchAddress("piplus1_Dplus_MinIPCHI2", &piplus1_Dplus_MinIPCHI2, &b_piplus1_Dplus_MinIPCHI2);
   fChain->SetBranchAddress("piplus1_Dplus_MC12TuneV2_ProbNNe", &piplus1_Dplus_MC12TuneV2_ProbNNe, &b_piplus1_Dplus_MC12TuneV2_ProbNNe);
   fChain->SetBranchAddress("piplus1_Dplus_MC12TuneV2_ProbNNmu", &piplus1_Dplus_MC12TuneV2_ProbNNmu, &b_piplus1_Dplus_MC12TuneV2_ProbNNmu);
   fChain->SetBranchAddress("piplus1_Dplus_MC12TuneV2_ProbNNpi", &piplus1_Dplus_MC12TuneV2_ProbNNpi, &b_piplus1_Dplus_MC12TuneV2_ProbNNpi);
   fChain->SetBranchAddress("piplus1_Dplus_MC12TuneV2_ProbNNk", &piplus1_Dplus_MC12TuneV2_ProbNNk, &b_piplus1_Dplus_MC12TuneV2_ProbNNk);
   fChain->SetBranchAddress("piplus1_Dplus_MC12TuneV2_ProbNNp", &piplus1_Dplus_MC12TuneV2_ProbNNp, &b_piplus1_Dplus_MC12TuneV2_ProbNNp);
   fChain->SetBranchAddress("piplus1_Dplus_MC12TuneV2_ProbNNghost", &piplus1_Dplus_MC12TuneV2_ProbNNghost, &b_piplus1_Dplus_MC12TuneV2_ProbNNghost);
   fChain->SetBranchAddress("piplus1_Dplus_MC12TuneV3_ProbNNe", &piplus1_Dplus_MC12TuneV3_ProbNNe, &b_piplus1_Dplus_MC12TuneV3_ProbNNe);
   fChain->SetBranchAddress("piplus1_Dplus_MC12TuneV3_ProbNNmu", &piplus1_Dplus_MC12TuneV3_ProbNNmu, &b_piplus1_Dplus_MC12TuneV3_ProbNNmu);
   fChain->SetBranchAddress("piplus1_Dplus_MC12TuneV3_ProbNNpi", &piplus1_Dplus_MC12TuneV3_ProbNNpi, &b_piplus1_Dplus_MC12TuneV3_ProbNNpi);
   fChain->SetBranchAddress("piplus1_Dplus_MC12TuneV3_ProbNNk", &piplus1_Dplus_MC12TuneV3_ProbNNk, &b_piplus1_Dplus_MC12TuneV3_ProbNNk);
   fChain->SetBranchAddress("piplus1_Dplus_MC12TuneV3_ProbNNp", &piplus1_Dplus_MC12TuneV3_ProbNNp, &b_piplus1_Dplus_MC12TuneV3_ProbNNp);
   fChain->SetBranchAddress("piplus1_Dplus_MC12TuneV3_ProbNNghost", &piplus1_Dplus_MC12TuneV3_ProbNNghost, &b_piplus1_Dplus_MC12TuneV3_ProbNNghost);
   fChain->SetBranchAddress("piplus1_Dplus_MC12TuneV4_ProbNNe", &piplus1_Dplus_MC12TuneV4_ProbNNe, &b_piplus1_Dplus_MC12TuneV4_ProbNNe);
   fChain->SetBranchAddress("piplus1_Dplus_MC12TuneV4_ProbNNmu", &piplus1_Dplus_MC12TuneV4_ProbNNmu, &b_piplus1_Dplus_MC12TuneV4_ProbNNmu);
   fChain->SetBranchAddress("piplus1_Dplus_MC12TuneV4_ProbNNpi", &piplus1_Dplus_MC12TuneV4_ProbNNpi, &b_piplus1_Dplus_MC12TuneV4_ProbNNpi);
   fChain->SetBranchAddress("piplus1_Dplus_MC12TuneV4_ProbNNk", &piplus1_Dplus_MC12TuneV4_ProbNNk, &b_piplus1_Dplus_MC12TuneV4_ProbNNk);
   fChain->SetBranchAddress("piplus1_Dplus_MC12TuneV4_ProbNNp", &piplus1_Dplus_MC12TuneV4_ProbNNp, &b_piplus1_Dplus_MC12TuneV4_ProbNNp);
   fChain->SetBranchAddress("piplus1_Dplus_MC12TuneV4_ProbNNghost", &piplus1_Dplus_MC12TuneV4_ProbNNghost, &b_piplus1_Dplus_MC12TuneV4_ProbNNghost);
   fChain->SetBranchAddress("piplus1_Dplus_MC15TuneV1_ProbNNe", &piplus1_Dplus_MC15TuneV1_ProbNNe, &b_piplus1_Dplus_MC15TuneV1_ProbNNe);
   fChain->SetBranchAddress("piplus1_Dplus_MC15TuneV1_ProbNNmu", &piplus1_Dplus_MC15TuneV1_ProbNNmu, &b_piplus1_Dplus_MC15TuneV1_ProbNNmu);
   fChain->SetBranchAddress("piplus1_Dplus_MC15TuneV1_ProbNNpi", &piplus1_Dplus_MC15TuneV1_ProbNNpi, &b_piplus1_Dplus_MC15TuneV1_ProbNNpi);
   fChain->SetBranchAddress("piplus1_Dplus_MC15TuneV1_ProbNNk", &piplus1_Dplus_MC15TuneV1_ProbNNk, &b_piplus1_Dplus_MC15TuneV1_ProbNNk);
   fChain->SetBranchAddress("piplus1_Dplus_MC15TuneV1_ProbNNp", &piplus1_Dplus_MC15TuneV1_ProbNNp, &b_piplus1_Dplus_MC15TuneV1_ProbNNp);
   fChain->SetBranchAddress("piplus1_Dplus_MC15TuneV1_ProbNNghost", &piplus1_Dplus_MC15TuneV1_ProbNNghost, &b_piplus1_Dplus_MC15TuneV1_ProbNNghost);
   fChain->SetBranchAddress("piplus1_Dplus_CosTheta", &piplus1_Dplus_CosTheta, &b_piplus1_Dplus_CosTheta);
   fChain->SetBranchAddress("piplus1_Dplus_OWNPV_X", &piplus1_Dplus_OWNPV_X, &b_piplus1_Dplus_OWNPV_X);
   fChain->SetBranchAddress("piplus1_Dplus_OWNPV_Y", &piplus1_Dplus_OWNPV_Y, &b_piplus1_Dplus_OWNPV_Y);
   fChain->SetBranchAddress("piplus1_Dplus_OWNPV_Z", &piplus1_Dplus_OWNPV_Z, &b_piplus1_Dplus_OWNPV_Z);
   fChain->SetBranchAddress("piplus1_Dplus_OWNPV_XERR", &piplus1_Dplus_OWNPV_XERR, &b_piplus1_Dplus_OWNPV_XERR);
   fChain->SetBranchAddress("piplus1_Dplus_OWNPV_YERR", &piplus1_Dplus_OWNPV_YERR, &b_piplus1_Dplus_OWNPV_YERR);
   fChain->SetBranchAddress("piplus1_Dplus_OWNPV_ZERR", &piplus1_Dplus_OWNPV_ZERR, &b_piplus1_Dplus_OWNPV_ZERR);
   fChain->SetBranchAddress("piplus1_Dplus_OWNPV_CHI2", &piplus1_Dplus_OWNPV_CHI2, &b_piplus1_Dplus_OWNPV_CHI2);
   fChain->SetBranchAddress("piplus1_Dplus_OWNPV_NDOF", &piplus1_Dplus_OWNPV_NDOF, &b_piplus1_Dplus_OWNPV_NDOF);
   fChain->SetBranchAddress("piplus1_Dplus_OWNPV_COV_", piplus1_Dplus_OWNPV_COV_, &b_piplus1_Dplus_OWNPV_COV_);
   fChain->SetBranchAddress("piplus1_Dplus_IP_OWNPV", &piplus1_Dplus_IP_OWNPV, &b_piplus1_Dplus_IP_OWNPV);
   fChain->SetBranchAddress("piplus1_Dplus_IPCHI2_OWNPV", &piplus1_Dplus_IPCHI2_OWNPV, &b_piplus1_Dplus_IPCHI2_OWNPV);
   fChain->SetBranchAddress("piplus1_Dplus_ORIVX_X", &piplus1_Dplus_ORIVX_X, &b_piplus1_Dplus_ORIVX_X);
   fChain->SetBranchAddress("piplus1_Dplus_ORIVX_Y", &piplus1_Dplus_ORIVX_Y, &b_piplus1_Dplus_ORIVX_Y);
   fChain->SetBranchAddress("piplus1_Dplus_ORIVX_Z", &piplus1_Dplus_ORIVX_Z, &b_piplus1_Dplus_ORIVX_Z);
   fChain->SetBranchAddress("piplus1_Dplus_ORIVX_XERR", &piplus1_Dplus_ORIVX_XERR, &b_piplus1_Dplus_ORIVX_XERR);
   fChain->SetBranchAddress("piplus1_Dplus_ORIVX_YERR", &piplus1_Dplus_ORIVX_YERR, &b_piplus1_Dplus_ORIVX_YERR);
   fChain->SetBranchAddress("piplus1_Dplus_ORIVX_ZERR", &piplus1_Dplus_ORIVX_ZERR, &b_piplus1_Dplus_ORIVX_ZERR);
   fChain->SetBranchAddress("piplus1_Dplus_ORIVX_CHI2", &piplus1_Dplus_ORIVX_CHI2, &b_piplus1_Dplus_ORIVX_CHI2);
   fChain->SetBranchAddress("piplus1_Dplus_ORIVX_NDOF", &piplus1_Dplus_ORIVX_NDOF, &b_piplus1_Dplus_ORIVX_NDOF);
   fChain->SetBranchAddress("piplus1_Dplus_ORIVX_COV_", piplus1_Dplus_ORIVX_COV_, &b_piplus1_Dplus_ORIVX_COV_);
   fChain->SetBranchAddress("piplus1_Dplus_P", &piplus1_Dplus_P, &b_piplus1_Dplus_P);
   fChain->SetBranchAddress("piplus1_Dplus_PT", &piplus1_Dplus_PT, &b_piplus1_Dplus_PT);
   fChain->SetBranchAddress("piplus1_Dplus_PE", &piplus1_Dplus_PE, &b_piplus1_Dplus_PE);
   fChain->SetBranchAddress("piplus1_Dplus_PX", &piplus1_Dplus_PX, &b_piplus1_Dplus_PX);
   fChain->SetBranchAddress("piplus1_Dplus_PY", &piplus1_Dplus_PY, &b_piplus1_Dplus_PY);
   fChain->SetBranchAddress("piplus1_Dplus_PZ", &piplus1_Dplus_PZ, &b_piplus1_Dplus_PZ);
   fChain->SetBranchAddress("piplus1_Dplus_REFPX", &piplus1_Dplus_REFPX, &b_piplus1_Dplus_REFPX);
   fChain->SetBranchAddress("piplus1_Dplus_REFPY", &piplus1_Dplus_REFPY, &b_piplus1_Dplus_REFPY);
   fChain->SetBranchAddress("piplus1_Dplus_REFPZ", &piplus1_Dplus_REFPZ, &b_piplus1_Dplus_REFPZ);
   fChain->SetBranchAddress("piplus1_Dplus_M", &piplus1_Dplus_M, &b_piplus1_Dplus_M);
   fChain->SetBranchAddress("piplus1_Dplus_AtVtx_PE", &piplus1_Dplus_AtVtx_PE, &b_piplus1_Dplus_AtVtx_PE);
   fChain->SetBranchAddress("piplus1_Dplus_AtVtx_PX", &piplus1_Dplus_AtVtx_PX, &b_piplus1_Dplus_AtVtx_PX);
   fChain->SetBranchAddress("piplus1_Dplus_AtVtx_PY", &piplus1_Dplus_AtVtx_PY, &b_piplus1_Dplus_AtVtx_PY);
   fChain->SetBranchAddress("piplus1_Dplus_AtVtx_PZ", &piplus1_Dplus_AtVtx_PZ, &b_piplus1_Dplus_AtVtx_PZ);
   fChain->SetBranchAddress("piplus1_Dplus_TRUEID", &piplus1_Dplus_TRUEID, &b_piplus1_Dplus_TRUEID);
   fChain->SetBranchAddress("piplus1_Dplus_TRUEP_E", &piplus1_Dplus_TRUEP_E, &b_piplus1_Dplus_TRUEP_E);
   fChain->SetBranchAddress("piplus1_Dplus_TRUEP_X", &piplus1_Dplus_TRUEP_X, &b_piplus1_Dplus_TRUEP_X);
   fChain->SetBranchAddress("piplus1_Dplus_TRUEP_Y", &piplus1_Dplus_TRUEP_Y, &b_piplus1_Dplus_TRUEP_Y);
   fChain->SetBranchAddress("piplus1_Dplus_TRUEP_Z", &piplus1_Dplus_TRUEP_Z, &b_piplus1_Dplus_TRUEP_Z);
   fChain->SetBranchAddress("piplus1_Dplus_TRUEPT", &piplus1_Dplus_TRUEPT, &b_piplus1_Dplus_TRUEPT);
   fChain->SetBranchAddress("piplus1_Dplus_TRUEORIGINVERTEX_X", &piplus1_Dplus_TRUEORIGINVERTEX_X, &b_piplus1_Dplus_TRUEORIGINVERTEX_X);
   fChain->SetBranchAddress("piplus1_Dplus_TRUEORIGINVERTEX_Y", &piplus1_Dplus_TRUEORIGINVERTEX_Y, &b_piplus1_Dplus_TRUEORIGINVERTEX_Y);
   fChain->SetBranchAddress("piplus1_Dplus_TRUEORIGINVERTEX_Z", &piplus1_Dplus_TRUEORIGINVERTEX_Z, &b_piplus1_Dplus_TRUEORIGINVERTEX_Z);
   fChain->SetBranchAddress("piplus1_Dplus_TRUEENDVERTEX_X", &piplus1_Dplus_TRUEENDVERTEX_X, &b_piplus1_Dplus_TRUEENDVERTEX_X);
   fChain->SetBranchAddress("piplus1_Dplus_TRUEENDVERTEX_Y", &piplus1_Dplus_TRUEENDVERTEX_Y, &b_piplus1_Dplus_TRUEENDVERTEX_Y);
   fChain->SetBranchAddress("piplus1_Dplus_TRUEENDVERTEX_Z", &piplus1_Dplus_TRUEENDVERTEX_Z, &b_piplus1_Dplus_TRUEENDVERTEX_Z);
   fChain->SetBranchAddress("piplus1_Dplus_TRUEISSTABLE", &piplus1_Dplus_TRUEISSTABLE, &b_piplus1_Dplus_TRUEISSTABLE);
   fChain->SetBranchAddress("piplus1_Dplus_TRUETAU", &piplus1_Dplus_TRUETAU, &b_piplus1_Dplus_TRUETAU);
   fChain->SetBranchAddress("piplus1_Dplus_ID", &piplus1_Dplus_ID, &b_piplus1_Dplus_ID);
   fChain->SetBranchAddress("piplus1_Dplus_CombDLLMu", &piplus1_Dplus_CombDLLMu, &b_piplus1_Dplus_CombDLLMu);
   fChain->SetBranchAddress("piplus1_Dplus_ProbNNmu", &piplus1_Dplus_ProbNNmu, &b_piplus1_Dplus_ProbNNmu);
   fChain->SetBranchAddress("piplus1_Dplus_ProbNNghost", &piplus1_Dplus_ProbNNghost, &b_piplus1_Dplus_ProbNNghost);
   fChain->SetBranchAddress("piplus1_Dplus_InMuonAcc", &piplus1_Dplus_InMuonAcc, &b_piplus1_Dplus_InMuonAcc);
   fChain->SetBranchAddress("piplus1_Dplus_MuonDist2", &piplus1_Dplus_MuonDist2, &b_piplus1_Dplus_MuonDist2);
   fChain->SetBranchAddress("piplus1_Dplus_regionInM2", &piplus1_Dplus_regionInM2, &b_piplus1_Dplus_regionInM2);
   fChain->SetBranchAddress("piplus1_Dplus_hasMuon", &piplus1_Dplus_hasMuon, &b_piplus1_Dplus_hasMuon);
   fChain->SetBranchAddress("piplus1_Dplus_isMuon", &piplus1_Dplus_isMuon, &b_piplus1_Dplus_isMuon);
   fChain->SetBranchAddress("piplus1_Dplus_isMuonLoose", &piplus1_Dplus_isMuonLoose, &b_piplus1_Dplus_isMuonLoose);
   fChain->SetBranchAddress("piplus1_Dplus_NShared", &piplus1_Dplus_NShared, &b_piplus1_Dplus_NShared);
   fChain->SetBranchAddress("piplus1_Dplus_MuonLLmu", &piplus1_Dplus_MuonLLmu, &b_piplus1_Dplus_MuonLLmu);
   fChain->SetBranchAddress("piplus1_Dplus_MuonLLbg", &piplus1_Dplus_MuonLLbg, &b_piplus1_Dplus_MuonLLbg);
   fChain->SetBranchAddress("piplus1_Dplus_isMuonFromProto", &piplus1_Dplus_isMuonFromProto, &b_piplus1_Dplus_isMuonFromProto);
   fChain->SetBranchAddress("piplus1_Dplus_PIDe", &piplus1_Dplus_PIDe, &b_piplus1_Dplus_PIDe);
   fChain->SetBranchAddress("piplus1_Dplus_PIDmu", &piplus1_Dplus_PIDmu, &b_piplus1_Dplus_PIDmu);
   fChain->SetBranchAddress("piplus1_Dplus_PIDK", &piplus1_Dplus_PIDK, &b_piplus1_Dplus_PIDK);
   fChain->SetBranchAddress("piplus1_Dplus_PIDp", &piplus1_Dplus_PIDp, &b_piplus1_Dplus_PIDp);
   fChain->SetBranchAddress("piplus1_Dplus_PIDd", &piplus1_Dplus_PIDd, &b_piplus1_Dplus_PIDd);
   fChain->SetBranchAddress("piplus1_Dplus_ProbNNe", &piplus1_Dplus_ProbNNe, &b_piplus1_Dplus_ProbNNe);
   fChain->SetBranchAddress("piplus1_Dplus_ProbNNk", &piplus1_Dplus_ProbNNk, &b_piplus1_Dplus_ProbNNk);
   fChain->SetBranchAddress("piplus1_Dplus_ProbNNp", &piplus1_Dplus_ProbNNp, &b_piplus1_Dplus_ProbNNp);
   fChain->SetBranchAddress("piplus1_Dplus_ProbNNpi", &piplus1_Dplus_ProbNNpi, &b_piplus1_Dplus_ProbNNpi);
   fChain->SetBranchAddress("piplus1_Dplus_ProbNNd", &piplus1_Dplus_ProbNNd, &b_piplus1_Dplus_ProbNNd);
   fChain->SetBranchAddress("piplus1_Dplus_hasRich", &piplus1_Dplus_hasRich, &b_piplus1_Dplus_hasRich);
   fChain->SetBranchAddress("piplus1_Dplus_UsedRichAerogel", &piplus1_Dplus_UsedRichAerogel, &b_piplus1_Dplus_UsedRichAerogel);
   fChain->SetBranchAddress("piplus1_Dplus_UsedRich1Gas", &piplus1_Dplus_UsedRich1Gas, &b_piplus1_Dplus_UsedRich1Gas);
   fChain->SetBranchAddress("piplus1_Dplus_UsedRich2Gas", &piplus1_Dplus_UsedRich2Gas, &b_piplus1_Dplus_UsedRich2Gas);
   fChain->SetBranchAddress("piplus1_Dplus_RichAboveElThres", &piplus1_Dplus_RichAboveElThres, &b_piplus1_Dplus_RichAboveElThres);
   fChain->SetBranchAddress("piplus1_Dplus_RichAboveMuThres", &piplus1_Dplus_RichAboveMuThres, &b_piplus1_Dplus_RichAboveMuThres);
   fChain->SetBranchAddress("piplus1_Dplus_RichAbovePiThres", &piplus1_Dplus_RichAbovePiThres, &b_piplus1_Dplus_RichAbovePiThres);
   fChain->SetBranchAddress("piplus1_Dplus_RichAboveKaThres", &piplus1_Dplus_RichAboveKaThres, &b_piplus1_Dplus_RichAboveKaThres);
   fChain->SetBranchAddress("piplus1_Dplus_RichAbovePrThres", &piplus1_Dplus_RichAbovePrThres, &b_piplus1_Dplus_RichAbovePrThres);
   fChain->SetBranchAddress("piplus1_Dplus_hasCalo", &piplus1_Dplus_hasCalo, &b_piplus1_Dplus_hasCalo);
   fChain->SetBranchAddress("piplus1_Dplus_L0Global_Dec", &piplus1_Dplus_L0Global_Dec, &b_piplus1_Dplus_L0Global_Dec);
   fChain->SetBranchAddress("piplus1_Dplus_L0Global_TIS", &piplus1_Dplus_L0Global_TIS, &b_piplus1_Dplus_L0Global_TIS);
   fChain->SetBranchAddress("piplus1_Dplus_L0Global_TOS", &piplus1_Dplus_L0Global_TOS, &b_piplus1_Dplus_L0Global_TOS);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt1Global_Dec", &piplus1_Dplus_Hlt1Global_Dec, &b_piplus1_Dplus_Hlt1Global_Dec);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt1Global_TIS", &piplus1_Dplus_Hlt1Global_TIS, &b_piplus1_Dplus_Hlt1Global_TIS);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt1Global_TOS", &piplus1_Dplus_Hlt1Global_TOS, &b_piplus1_Dplus_Hlt1Global_TOS);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt1Phys_Dec", &piplus1_Dplus_Hlt1Phys_Dec, &b_piplus1_Dplus_Hlt1Phys_Dec);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt1Phys_TIS", &piplus1_Dplus_Hlt1Phys_TIS, &b_piplus1_Dplus_Hlt1Phys_TIS);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt1Phys_TOS", &piplus1_Dplus_Hlt1Phys_TOS, &b_piplus1_Dplus_Hlt1Phys_TOS);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt2Global_Dec", &piplus1_Dplus_Hlt2Global_Dec, &b_piplus1_Dplus_Hlt2Global_Dec);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt2Global_TIS", &piplus1_Dplus_Hlt2Global_TIS, &b_piplus1_Dplus_Hlt2Global_TIS);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt2Global_TOS", &piplus1_Dplus_Hlt2Global_TOS, &b_piplus1_Dplus_Hlt2Global_TOS);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt2Phys_Dec", &piplus1_Dplus_Hlt2Phys_Dec, &b_piplus1_Dplus_Hlt2Phys_Dec);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt2Phys_TIS", &piplus1_Dplus_Hlt2Phys_TIS, &b_piplus1_Dplus_Hlt2Phys_TIS);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt2Phys_TOS", &piplus1_Dplus_Hlt2Phys_TOS, &b_piplus1_Dplus_Hlt2Phys_TOS);
   fChain->SetBranchAddress("piplus1_Dplus_L0HadronDecision_Dec", &piplus1_Dplus_L0HadronDecision_Dec, &b_piplus1_Dplus_L0HadronDecision_Dec);
   fChain->SetBranchAddress("piplus1_Dplus_L0HadronDecision_TIS", &piplus1_Dplus_L0HadronDecision_TIS, &b_piplus1_Dplus_L0HadronDecision_TIS);
   fChain->SetBranchAddress("piplus1_Dplus_L0HadronDecision_TOS", &piplus1_Dplus_L0HadronDecision_TOS, &b_piplus1_Dplus_L0HadronDecision_TOS);
   fChain->SetBranchAddress("piplus1_Dplus_L0ElectronDecision_Dec", &piplus1_Dplus_L0ElectronDecision_Dec, &b_piplus1_Dplus_L0ElectronDecision_Dec);
   fChain->SetBranchAddress("piplus1_Dplus_L0ElectronDecision_TIS", &piplus1_Dplus_L0ElectronDecision_TIS, &b_piplus1_Dplus_L0ElectronDecision_TIS);
   fChain->SetBranchAddress("piplus1_Dplus_L0ElectronDecision_TOS", &piplus1_Dplus_L0ElectronDecision_TOS, &b_piplus1_Dplus_L0ElectronDecision_TOS);
   fChain->SetBranchAddress("piplus1_Dplus_L0ElectronHiDecision_Dec", &piplus1_Dplus_L0ElectronHiDecision_Dec, &b_piplus1_Dplus_L0ElectronHiDecision_Dec);
   fChain->SetBranchAddress("piplus1_Dplus_L0ElectronHiDecision_TIS", &piplus1_Dplus_L0ElectronHiDecision_TIS, &b_piplus1_Dplus_L0ElectronHiDecision_TIS);
   fChain->SetBranchAddress("piplus1_Dplus_L0ElectronHiDecision_TOS", &piplus1_Dplus_L0ElectronHiDecision_TOS, &b_piplus1_Dplus_L0ElectronHiDecision_TOS);
   fChain->SetBranchAddress("piplus1_Dplus_L0MuonDecision_Dec", &piplus1_Dplus_L0MuonDecision_Dec, &b_piplus1_Dplus_L0MuonDecision_Dec);
   fChain->SetBranchAddress("piplus1_Dplus_L0MuonDecision_TIS", &piplus1_Dplus_L0MuonDecision_TIS, &b_piplus1_Dplus_L0MuonDecision_TIS);
   fChain->SetBranchAddress("piplus1_Dplus_L0MuonDecision_TOS", &piplus1_Dplus_L0MuonDecision_TOS, &b_piplus1_Dplus_L0MuonDecision_TOS);
   fChain->SetBranchAddress("piplus1_Dplus_L0DiMuonDecision_Dec", &piplus1_Dplus_L0DiMuonDecision_Dec, &b_piplus1_Dplus_L0DiMuonDecision_Dec);
   fChain->SetBranchAddress("piplus1_Dplus_L0DiMuonDecision_TIS", &piplus1_Dplus_L0DiMuonDecision_TIS, &b_piplus1_Dplus_L0DiMuonDecision_TIS);
   fChain->SetBranchAddress("piplus1_Dplus_L0DiMuonDecision_TOS", &piplus1_Dplus_L0DiMuonDecision_TOS, &b_piplus1_Dplus_L0DiMuonDecision_TOS);
   fChain->SetBranchAddress("piplus1_Dplus_L0MuonHighDecision_Dec", &piplus1_Dplus_L0MuonHighDecision_Dec, &b_piplus1_Dplus_L0MuonHighDecision_Dec);
   fChain->SetBranchAddress("piplus1_Dplus_L0MuonHighDecision_TIS", &piplus1_Dplus_L0MuonHighDecision_TIS, &b_piplus1_Dplus_L0MuonHighDecision_TIS);
   fChain->SetBranchAddress("piplus1_Dplus_L0MuonHighDecision_TOS", &piplus1_Dplus_L0MuonHighDecision_TOS, &b_piplus1_Dplus_L0MuonHighDecision_TOS);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt1TrackMVADecision_Dec", &piplus1_Dplus_Hlt1TrackMVADecision_Dec, &b_piplus1_Dplus_Hlt1TrackMVADecision_Dec);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt1TrackMVADecision_TIS", &piplus1_Dplus_Hlt1TrackMVADecision_TIS, &b_piplus1_Dplus_Hlt1TrackMVADecision_TIS);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt1TrackMVADecision_TOS", &piplus1_Dplus_Hlt1TrackMVADecision_TOS, &b_piplus1_Dplus_Hlt1TrackMVADecision_TOS);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt1TwoTrackMVADecision_Dec", &piplus1_Dplus_Hlt1TwoTrackMVADecision_Dec, &b_piplus1_Dplus_Hlt1TwoTrackMVADecision_Dec);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt1TwoTrackMVADecision_TIS", &piplus1_Dplus_Hlt1TwoTrackMVADecision_TIS, &b_piplus1_Dplus_Hlt1TwoTrackMVADecision_TIS);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt1TwoTrackMVADecision_TOS", &piplus1_Dplus_Hlt1TwoTrackMVADecision_TOS, &b_piplus1_Dplus_Hlt1TwoTrackMVADecision_TOS);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt1GlobalDecision_Dec", &piplus1_Dplus_Hlt1GlobalDecision_Dec, &b_piplus1_Dplus_Hlt1GlobalDecision_Dec);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt1GlobalDecision_TIS", &piplus1_Dplus_Hlt1GlobalDecision_TIS, &b_piplus1_Dplus_Hlt1GlobalDecision_TIS);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt1GlobalDecision_TOS", &piplus1_Dplus_Hlt1GlobalDecision_TOS, &b_piplus1_Dplus_Hlt1GlobalDecision_TOS);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt1TrackAllL0Decision_Dec", &piplus1_Dplus_Hlt1TrackAllL0Decision_Dec, &b_piplus1_Dplus_Hlt1TrackAllL0Decision_Dec);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt1TrackAllL0Decision_TIS", &piplus1_Dplus_Hlt1TrackAllL0Decision_TIS, &b_piplus1_Dplus_Hlt1TrackAllL0Decision_TIS);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt1TrackAllL0Decision_TOS", &piplus1_Dplus_Hlt1TrackAllL0Decision_TOS, &b_piplus1_Dplus_Hlt1TrackAllL0Decision_TOS);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt2Topo2BodyBBDTDecision_Dec", &piplus1_Dplus_Hlt2Topo2BodyBBDTDecision_Dec, &b_piplus1_Dplus_Hlt2Topo2BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt2Topo2BodyBBDTDecision_TIS", &piplus1_Dplus_Hlt2Topo2BodyBBDTDecision_TIS, &b_piplus1_Dplus_Hlt2Topo2BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt2Topo2BodyBBDTDecision_TOS", &piplus1_Dplus_Hlt2Topo2BodyBBDTDecision_TOS, &b_piplus1_Dplus_Hlt2Topo2BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt2Topo3BodyBBDTDecision_Dec", &piplus1_Dplus_Hlt2Topo3BodyBBDTDecision_Dec, &b_piplus1_Dplus_Hlt2Topo3BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt2Topo3BodyBBDTDecision_TIS", &piplus1_Dplus_Hlt2Topo3BodyBBDTDecision_TIS, &b_piplus1_Dplus_Hlt2Topo3BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt2Topo3BodyBBDTDecision_TOS", &piplus1_Dplus_Hlt2Topo3BodyBBDTDecision_TOS, &b_piplus1_Dplus_Hlt2Topo3BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt2Topo4BodyBBDTDecision_Dec", &piplus1_Dplus_Hlt2Topo4BodyBBDTDecision_Dec, &b_piplus1_Dplus_Hlt2Topo4BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt2Topo4BodyBBDTDecision_TIS", &piplus1_Dplus_Hlt2Topo4BodyBBDTDecision_TIS, &b_piplus1_Dplus_Hlt2Topo4BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt2Topo4BodyBBDTDecision_TOS", &piplus1_Dplus_Hlt2Topo4BodyBBDTDecision_TOS, &b_piplus1_Dplus_Hlt2Topo4BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt2BHadB02PpPpPmPmDecision_Dec", &piplus1_Dplus_Hlt2BHadB02PpPpPmPmDecision_Dec, &b_piplus1_Dplus_Hlt2BHadB02PpPpPmPmDecision_Dec);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt2BHadB02PpPpPmPmDecision_TIS", &piplus1_Dplus_Hlt2BHadB02PpPpPmPmDecision_TIS, &b_piplus1_Dplus_Hlt2BHadB02PpPpPmPmDecision_TIS);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt2BHadB02PpPpPmPmDecision_TOS", &piplus1_Dplus_Hlt2BHadB02PpPpPmPmDecision_TOS, &b_piplus1_Dplus_Hlt2BHadB02PpPpPmPmDecision_TOS);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt2Topo2BodyDecision_Dec", &piplus1_Dplus_Hlt2Topo2BodyDecision_Dec, &b_piplus1_Dplus_Hlt2Topo2BodyDecision_Dec);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt2Topo2BodyDecision_TIS", &piplus1_Dplus_Hlt2Topo2BodyDecision_TIS, &b_piplus1_Dplus_Hlt2Topo2BodyDecision_TIS);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt2Topo2BodyDecision_TOS", &piplus1_Dplus_Hlt2Topo2BodyDecision_TOS, &b_piplus1_Dplus_Hlt2Topo2BodyDecision_TOS);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt2Topo3BodyDecision_Dec", &piplus1_Dplus_Hlt2Topo3BodyDecision_Dec, &b_piplus1_Dplus_Hlt2Topo3BodyDecision_Dec);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt2Topo3BodyDecision_TIS", &piplus1_Dplus_Hlt2Topo3BodyDecision_TIS, &b_piplus1_Dplus_Hlt2Topo3BodyDecision_TIS);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt2Topo3BodyDecision_TOS", &piplus1_Dplus_Hlt2Topo3BodyDecision_TOS, &b_piplus1_Dplus_Hlt2Topo3BodyDecision_TOS);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt2Topo4BodyDecision_Dec", &piplus1_Dplus_Hlt2Topo4BodyDecision_Dec, &b_piplus1_Dplus_Hlt2Topo4BodyDecision_Dec);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt2Topo4BodyDecision_TIS", &piplus1_Dplus_Hlt2Topo4BodyDecision_TIS, &b_piplus1_Dplus_Hlt2Topo4BodyDecision_TIS);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt2Topo4BodyDecision_TOS", &piplus1_Dplus_Hlt2Topo4BodyDecision_TOS, &b_piplus1_Dplus_Hlt2Topo4BodyDecision_TOS);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt2GlobalDecision_Dec", &piplus1_Dplus_Hlt2GlobalDecision_Dec, &b_piplus1_Dplus_Hlt2GlobalDecision_Dec);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt2GlobalDecision_TIS", &piplus1_Dplus_Hlt2GlobalDecision_TIS, &b_piplus1_Dplus_Hlt2GlobalDecision_TIS);
   fChain->SetBranchAddress("piplus1_Dplus_Hlt2GlobalDecision_TOS", &piplus1_Dplus_Hlt2GlobalDecision_TOS, &b_piplus1_Dplus_Hlt2GlobalDecision_TOS);
   fChain->SetBranchAddress("piplus1_Dplus_TRACK_Type", &piplus1_Dplus_TRACK_Type, &b_piplus1_Dplus_TRACK_Type);
   fChain->SetBranchAddress("piplus1_Dplus_TRACK_Key", &piplus1_Dplus_TRACK_Key, &b_piplus1_Dplus_TRACK_Key);
   fChain->SetBranchAddress("piplus1_Dplus_TRACK_CHI2NDOF", &piplus1_Dplus_TRACK_CHI2NDOF, &b_piplus1_Dplus_TRACK_CHI2NDOF);
   fChain->SetBranchAddress("piplus1_Dplus_TRACK_PCHI2", &piplus1_Dplus_TRACK_PCHI2, &b_piplus1_Dplus_TRACK_PCHI2);
   fChain->SetBranchAddress("piplus1_Dplus_TRACK_MatchCHI2", &piplus1_Dplus_TRACK_MatchCHI2, &b_piplus1_Dplus_TRACK_MatchCHI2);
   fChain->SetBranchAddress("piplus1_Dplus_TRACK_GhostProb", &piplus1_Dplus_TRACK_GhostProb, &b_piplus1_Dplus_TRACK_GhostProb);
   fChain->SetBranchAddress("piplus1_Dplus_TRACK_CloneDist", &piplus1_Dplus_TRACK_CloneDist, &b_piplus1_Dplus_TRACK_CloneDist);
   fChain->SetBranchAddress("piplus1_Dplus_TRACK_Likelihood", &piplus1_Dplus_TRACK_Likelihood, &b_piplus1_Dplus_TRACK_Likelihood);
   fChain->SetBranchAddress("piplus1_Dplus_X", &piplus1_Dplus_X, &b_piplus1_Dplus_X);
   fChain->SetBranchAddress("piplus1_Dplus_Y", &piplus1_Dplus_Y, &b_piplus1_Dplus_Y);
   fChain->SetBranchAddress("piplus2_Dplus_ETA", &piplus2_Dplus_ETA, &b_piplus2_Dplus_ETA);
   fChain->SetBranchAddress("piplus2_Dplus_MinIPCHI2", &piplus2_Dplus_MinIPCHI2, &b_piplus2_Dplus_MinIPCHI2);
   fChain->SetBranchAddress("piplus2_Dplus_MC12TuneV2_ProbNNe", &piplus2_Dplus_MC12TuneV2_ProbNNe, &b_piplus2_Dplus_MC12TuneV2_ProbNNe);
   fChain->SetBranchAddress("piplus2_Dplus_MC12TuneV2_ProbNNmu", &piplus2_Dplus_MC12TuneV2_ProbNNmu, &b_piplus2_Dplus_MC12TuneV2_ProbNNmu);
   fChain->SetBranchAddress("piplus2_Dplus_MC12TuneV2_ProbNNpi", &piplus2_Dplus_MC12TuneV2_ProbNNpi, &b_piplus2_Dplus_MC12TuneV2_ProbNNpi);
   fChain->SetBranchAddress("piplus2_Dplus_MC12TuneV2_ProbNNk", &piplus2_Dplus_MC12TuneV2_ProbNNk, &b_piplus2_Dplus_MC12TuneV2_ProbNNk);
   fChain->SetBranchAddress("piplus2_Dplus_MC12TuneV2_ProbNNp", &piplus2_Dplus_MC12TuneV2_ProbNNp, &b_piplus2_Dplus_MC12TuneV2_ProbNNp);
   fChain->SetBranchAddress("piplus2_Dplus_MC12TuneV2_ProbNNghost", &piplus2_Dplus_MC12TuneV2_ProbNNghost, &b_piplus2_Dplus_MC12TuneV2_ProbNNghost);
   fChain->SetBranchAddress("piplus2_Dplus_MC12TuneV3_ProbNNe", &piplus2_Dplus_MC12TuneV3_ProbNNe, &b_piplus2_Dplus_MC12TuneV3_ProbNNe);
   fChain->SetBranchAddress("piplus2_Dplus_MC12TuneV3_ProbNNmu", &piplus2_Dplus_MC12TuneV3_ProbNNmu, &b_piplus2_Dplus_MC12TuneV3_ProbNNmu);
   fChain->SetBranchAddress("piplus2_Dplus_MC12TuneV3_ProbNNpi", &piplus2_Dplus_MC12TuneV3_ProbNNpi, &b_piplus2_Dplus_MC12TuneV3_ProbNNpi);
   fChain->SetBranchAddress("piplus2_Dplus_MC12TuneV3_ProbNNk", &piplus2_Dplus_MC12TuneV3_ProbNNk, &b_piplus2_Dplus_MC12TuneV3_ProbNNk);
   fChain->SetBranchAddress("piplus2_Dplus_MC12TuneV3_ProbNNp", &piplus2_Dplus_MC12TuneV3_ProbNNp, &b_piplus2_Dplus_MC12TuneV3_ProbNNp);
   fChain->SetBranchAddress("piplus2_Dplus_MC12TuneV3_ProbNNghost", &piplus2_Dplus_MC12TuneV3_ProbNNghost, &b_piplus2_Dplus_MC12TuneV3_ProbNNghost);
   fChain->SetBranchAddress("piplus2_Dplus_MC12TuneV4_ProbNNe", &piplus2_Dplus_MC12TuneV4_ProbNNe, &b_piplus2_Dplus_MC12TuneV4_ProbNNe);
   fChain->SetBranchAddress("piplus2_Dplus_MC12TuneV4_ProbNNmu", &piplus2_Dplus_MC12TuneV4_ProbNNmu, &b_piplus2_Dplus_MC12TuneV4_ProbNNmu);
   fChain->SetBranchAddress("piplus2_Dplus_MC12TuneV4_ProbNNpi", &piplus2_Dplus_MC12TuneV4_ProbNNpi, &b_piplus2_Dplus_MC12TuneV4_ProbNNpi);
   fChain->SetBranchAddress("piplus2_Dplus_MC12TuneV4_ProbNNk", &piplus2_Dplus_MC12TuneV4_ProbNNk, &b_piplus2_Dplus_MC12TuneV4_ProbNNk);
   fChain->SetBranchAddress("piplus2_Dplus_MC12TuneV4_ProbNNp", &piplus2_Dplus_MC12TuneV4_ProbNNp, &b_piplus2_Dplus_MC12TuneV4_ProbNNp);
   fChain->SetBranchAddress("piplus2_Dplus_MC12TuneV4_ProbNNghost", &piplus2_Dplus_MC12TuneV4_ProbNNghost, &b_piplus2_Dplus_MC12TuneV4_ProbNNghost);
   fChain->SetBranchAddress("piplus2_Dplus_MC15TuneV1_ProbNNe", &piplus2_Dplus_MC15TuneV1_ProbNNe, &b_piplus2_Dplus_MC15TuneV1_ProbNNe);
   fChain->SetBranchAddress("piplus2_Dplus_MC15TuneV1_ProbNNmu", &piplus2_Dplus_MC15TuneV1_ProbNNmu, &b_piplus2_Dplus_MC15TuneV1_ProbNNmu);
   fChain->SetBranchAddress("piplus2_Dplus_MC15TuneV1_ProbNNpi", &piplus2_Dplus_MC15TuneV1_ProbNNpi, &b_piplus2_Dplus_MC15TuneV1_ProbNNpi);
   fChain->SetBranchAddress("piplus2_Dplus_MC15TuneV1_ProbNNk", &piplus2_Dplus_MC15TuneV1_ProbNNk, &b_piplus2_Dplus_MC15TuneV1_ProbNNk);
   fChain->SetBranchAddress("piplus2_Dplus_MC15TuneV1_ProbNNp", &piplus2_Dplus_MC15TuneV1_ProbNNp, &b_piplus2_Dplus_MC15TuneV1_ProbNNp);
   fChain->SetBranchAddress("piplus2_Dplus_MC15TuneV1_ProbNNghost", &piplus2_Dplus_MC15TuneV1_ProbNNghost, &b_piplus2_Dplus_MC15TuneV1_ProbNNghost);
   fChain->SetBranchAddress("piplus2_Dplus_CosTheta", &piplus2_Dplus_CosTheta, &b_piplus2_Dplus_CosTheta);
   fChain->SetBranchAddress("piplus2_Dplus_OWNPV_X", &piplus2_Dplus_OWNPV_X, &b_piplus2_Dplus_OWNPV_X);
   fChain->SetBranchAddress("piplus2_Dplus_OWNPV_Y", &piplus2_Dplus_OWNPV_Y, &b_piplus2_Dplus_OWNPV_Y);
   fChain->SetBranchAddress("piplus2_Dplus_OWNPV_Z", &piplus2_Dplus_OWNPV_Z, &b_piplus2_Dplus_OWNPV_Z);
   fChain->SetBranchAddress("piplus2_Dplus_OWNPV_XERR", &piplus2_Dplus_OWNPV_XERR, &b_piplus2_Dplus_OWNPV_XERR);
   fChain->SetBranchAddress("piplus2_Dplus_OWNPV_YERR", &piplus2_Dplus_OWNPV_YERR, &b_piplus2_Dplus_OWNPV_YERR);
   fChain->SetBranchAddress("piplus2_Dplus_OWNPV_ZERR", &piplus2_Dplus_OWNPV_ZERR, &b_piplus2_Dplus_OWNPV_ZERR);
   fChain->SetBranchAddress("piplus2_Dplus_OWNPV_CHI2", &piplus2_Dplus_OWNPV_CHI2, &b_piplus2_Dplus_OWNPV_CHI2);
   fChain->SetBranchAddress("piplus2_Dplus_OWNPV_NDOF", &piplus2_Dplus_OWNPV_NDOF, &b_piplus2_Dplus_OWNPV_NDOF);
   fChain->SetBranchAddress("piplus2_Dplus_OWNPV_COV_", piplus2_Dplus_OWNPV_COV_, &b_piplus2_Dplus_OWNPV_COV_);
   fChain->SetBranchAddress("piplus2_Dplus_IP_OWNPV", &piplus2_Dplus_IP_OWNPV, &b_piplus2_Dplus_IP_OWNPV);
   fChain->SetBranchAddress("piplus2_Dplus_IPCHI2_OWNPV", &piplus2_Dplus_IPCHI2_OWNPV, &b_piplus2_Dplus_IPCHI2_OWNPV);
   fChain->SetBranchAddress("piplus2_Dplus_ORIVX_X", &piplus2_Dplus_ORIVX_X, &b_piplus2_Dplus_ORIVX_X);
   fChain->SetBranchAddress("piplus2_Dplus_ORIVX_Y", &piplus2_Dplus_ORIVX_Y, &b_piplus2_Dplus_ORIVX_Y);
   fChain->SetBranchAddress("piplus2_Dplus_ORIVX_Z", &piplus2_Dplus_ORIVX_Z, &b_piplus2_Dplus_ORIVX_Z);
   fChain->SetBranchAddress("piplus2_Dplus_ORIVX_XERR", &piplus2_Dplus_ORIVX_XERR, &b_piplus2_Dplus_ORIVX_XERR);
   fChain->SetBranchAddress("piplus2_Dplus_ORIVX_YERR", &piplus2_Dplus_ORIVX_YERR, &b_piplus2_Dplus_ORIVX_YERR);
   fChain->SetBranchAddress("piplus2_Dplus_ORIVX_ZERR", &piplus2_Dplus_ORIVX_ZERR, &b_piplus2_Dplus_ORIVX_ZERR);
   fChain->SetBranchAddress("piplus2_Dplus_ORIVX_CHI2", &piplus2_Dplus_ORIVX_CHI2, &b_piplus2_Dplus_ORIVX_CHI2);
   fChain->SetBranchAddress("piplus2_Dplus_ORIVX_NDOF", &piplus2_Dplus_ORIVX_NDOF, &b_piplus2_Dplus_ORIVX_NDOF);
   fChain->SetBranchAddress("piplus2_Dplus_ORIVX_COV_", piplus2_Dplus_ORIVX_COV_, &b_piplus2_Dplus_ORIVX_COV_);
   fChain->SetBranchAddress("piplus2_Dplus_P", &piplus2_Dplus_P, &b_piplus2_Dplus_P);
   fChain->SetBranchAddress("piplus2_Dplus_PT", &piplus2_Dplus_PT, &b_piplus2_Dplus_PT);
   fChain->SetBranchAddress("piplus2_Dplus_PE", &piplus2_Dplus_PE, &b_piplus2_Dplus_PE);
   fChain->SetBranchAddress("piplus2_Dplus_PX", &piplus2_Dplus_PX, &b_piplus2_Dplus_PX);
   fChain->SetBranchAddress("piplus2_Dplus_PY", &piplus2_Dplus_PY, &b_piplus2_Dplus_PY);
   fChain->SetBranchAddress("piplus2_Dplus_PZ", &piplus2_Dplus_PZ, &b_piplus2_Dplus_PZ);
   fChain->SetBranchAddress("piplus2_Dplus_REFPX", &piplus2_Dplus_REFPX, &b_piplus2_Dplus_REFPX);
   fChain->SetBranchAddress("piplus2_Dplus_REFPY", &piplus2_Dplus_REFPY, &b_piplus2_Dplus_REFPY);
   fChain->SetBranchAddress("piplus2_Dplus_REFPZ", &piplus2_Dplus_REFPZ, &b_piplus2_Dplus_REFPZ);
   fChain->SetBranchAddress("piplus2_Dplus_M", &piplus2_Dplus_M, &b_piplus2_Dplus_M);
   fChain->SetBranchAddress("piplus2_Dplus_AtVtx_PE", &piplus2_Dplus_AtVtx_PE, &b_piplus2_Dplus_AtVtx_PE);
   fChain->SetBranchAddress("piplus2_Dplus_AtVtx_PX", &piplus2_Dplus_AtVtx_PX, &b_piplus2_Dplus_AtVtx_PX);
   fChain->SetBranchAddress("piplus2_Dplus_AtVtx_PY", &piplus2_Dplus_AtVtx_PY, &b_piplus2_Dplus_AtVtx_PY);
   fChain->SetBranchAddress("piplus2_Dplus_AtVtx_PZ", &piplus2_Dplus_AtVtx_PZ, &b_piplus2_Dplus_AtVtx_PZ);
   fChain->SetBranchAddress("piplus2_Dplus_TRUEID", &piplus2_Dplus_TRUEID, &b_piplus2_Dplus_TRUEID);
   fChain->SetBranchAddress("piplus2_Dplus_TRUEP_E", &piplus2_Dplus_TRUEP_E, &b_piplus2_Dplus_TRUEP_E);
   fChain->SetBranchAddress("piplus2_Dplus_TRUEP_X", &piplus2_Dplus_TRUEP_X, &b_piplus2_Dplus_TRUEP_X);
   fChain->SetBranchAddress("piplus2_Dplus_TRUEP_Y", &piplus2_Dplus_TRUEP_Y, &b_piplus2_Dplus_TRUEP_Y);
   fChain->SetBranchAddress("piplus2_Dplus_TRUEP_Z", &piplus2_Dplus_TRUEP_Z, &b_piplus2_Dplus_TRUEP_Z);
   fChain->SetBranchAddress("piplus2_Dplus_TRUEPT", &piplus2_Dplus_TRUEPT, &b_piplus2_Dplus_TRUEPT);
   fChain->SetBranchAddress("piplus2_Dplus_TRUEORIGINVERTEX_X", &piplus2_Dplus_TRUEORIGINVERTEX_X, &b_piplus2_Dplus_TRUEORIGINVERTEX_X);
   fChain->SetBranchAddress("piplus2_Dplus_TRUEORIGINVERTEX_Y", &piplus2_Dplus_TRUEORIGINVERTEX_Y, &b_piplus2_Dplus_TRUEORIGINVERTEX_Y);
   fChain->SetBranchAddress("piplus2_Dplus_TRUEORIGINVERTEX_Z", &piplus2_Dplus_TRUEORIGINVERTEX_Z, &b_piplus2_Dplus_TRUEORIGINVERTEX_Z);
   fChain->SetBranchAddress("piplus2_Dplus_TRUEENDVERTEX_X", &piplus2_Dplus_TRUEENDVERTEX_X, &b_piplus2_Dplus_TRUEENDVERTEX_X);
   fChain->SetBranchAddress("piplus2_Dplus_TRUEENDVERTEX_Y", &piplus2_Dplus_TRUEENDVERTEX_Y, &b_piplus2_Dplus_TRUEENDVERTEX_Y);
   fChain->SetBranchAddress("piplus2_Dplus_TRUEENDVERTEX_Z", &piplus2_Dplus_TRUEENDVERTEX_Z, &b_piplus2_Dplus_TRUEENDVERTEX_Z);
   fChain->SetBranchAddress("piplus2_Dplus_TRUEISSTABLE", &piplus2_Dplus_TRUEISSTABLE, &b_piplus2_Dplus_TRUEISSTABLE);
   fChain->SetBranchAddress("piplus2_Dplus_TRUETAU", &piplus2_Dplus_TRUETAU, &b_piplus2_Dplus_TRUETAU);
   fChain->SetBranchAddress("piplus2_Dplus_ID", &piplus2_Dplus_ID, &b_piplus2_Dplus_ID);
   fChain->SetBranchAddress("piplus2_Dplus_CombDLLMu", &piplus2_Dplus_CombDLLMu, &b_piplus2_Dplus_CombDLLMu);
   fChain->SetBranchAddress("piplus2_Dplus_ProbNNmu", &piplus2_Dplus_ProbNNmu, &b_piplus2_Dplus_ProbNNmu);
   fChain->SetBranchAddress("piplus2_Dplus_ProbNNghost", &piplus2_Dplus_ProbNNghost, &b_piplus2_Dplus_ProbNNghost);
   fChain->SetBranchAddress("piplus2_Dplus_InMuonAcc", &piplus2_Dplus_InMuonAcc, &b_piplus2_Dplus_InMuonAcc);
   fChain->SetBranchAddress("piplus2_Dplus_MuonDist2", &piplus2_Dplus_MuonDist2, &b_piplus2_Dplus_MuonDist2);
   fChain->SetBranchAddress("piplus2_Dplus_regionInM2", &piplus2_Dplus_regionInM2, &b_piplus2_Dplus_regionInM2);
   fChain->SetBranchAddress("piplus2_Dplus_hasMuon", &piplus2_Dplus_hasMuon, &b_piplus2_Dplus_hasMuon);
   fChain->SetBranchAddress("piplus2_Dplus_isMuon", &piplus2_Dplus_isMuon, &b_piplus2_Dplus_isMuon);
   fChain->SetBranchAddress("piplus2_Dplus_isMuonLoose", &piplus2_Dplus_isMuonLoose, &b_piplus2_Dplus_isMuonLoose);
   fChain->SetBranchAddress("piplus2_Dplus_NShared", &piplus2_Dplus_NShared, &b_piplus2_Dplus_NShared);
   fChain->SetBranchAddress("piplus2_Dplus_MuonLLmu", &piplus2_Dplus_MuonLLmu, &b_piplus2_Dplus_MuonLLmu);
   fChain->SetBranchAddress("piplus2_Dplus_MuonLLbg", &piplus2_Dplus_MuonLLbg, &b_piplus2_Dplus_MuonLLbg);
   fChain->SetBranchAddress("piplus2_Dplus_isMuonFromProto", &piplus2_Dplus_isMuonFromProto, &b_piplus2_Dplus_isMuonFromProto);
   fChain->SetBranchAddress("piplus2_Dplus_PIDe", &piplus2_Dplus_PIDe, &b_piplus2_Dplus_PIDe);
   fChain->SetBranchAddress("piplus2_Dplus_PIDmu", &piplus2_Dplus_PIDmu, &b_piplus2_Dplus_PIDmu);
   fChain->SetBranchAddress("piplus2_Dplus_PIDK", &piplus2_Dplus_PIDK, &b_piplus2_Dplus_PIDK);
   fChain->SetBranchAddress("piplus2_Dplus_PIDp", &piplus2_Dplus_PIDp, &b_piplus2_Dplus_PIDp);
   fChain->SetBranchAddress("piplus2_Dplus_PIDd", &piplus2_Dplus_PIDd, &b_piplus2_Dplus_PIDd);
   fChain->SetBranchAddress("piplus2_Dplus_ProbNNe", &piplus2_Dplus_ProbNNe, &b_piplus2_Dplus_ProbNNe);
   fChain->SetBranchAddress("piplus2_Dplus_ProbNNk", &piplus2_Dplus_ProbNNk, &b_piplus2_Dplus_ProbNNk);
   fChain->SetBranchAddress("piplus2_Dplus_ProbNNp", &piplus2_Dplus_ProbNNp, &b_piplus2_Dplus_ProbNNp);
   fChain->SetBranchAddress("piplus2_Dplus_ProbNNpi", &piplus2_Dplus_ProbNNpi, &b_piplus2_Dplus_ProbNNpi);
   fChain->SetBranchAddress("piplus2_Dplus_ProbNNd", &piplus2_Dplus_ProbNNd, &b_piplus2_Dplus_ProbNNd);
   fChain->SetBranchAddress("piplus2_Dplus_hasRich", &piplus2_Dplus_hasRich, &b_piplus2_Dplus_hasRich);
   fChain->SetBranchAddress("piplus2_Dplus_UsedRichAerogel", &piplus2_Dplus_UsedRichAerogel, &b_piplus2_Dplus_UsedRichAerogel);
   fChain->SetBranchAddress("piplus2_Dplus_UsedRich1Gas", &piplus2_Dplus_UsedRich1Gas, &b_piplus2_Dplus_UsedRich1Gas);
   fChain->SetBranchAddress("piplus2_Dplus_UsedRich2Gas", &piplus2_Dplus_UsedRich2Gas, &b_piplus2_Dplus_UsedRich2Gas);
   fChain->SetBranchAddress("piplus2_Dplus_RichAboveElThres", &piplus2_Dplus_RichAboveElThres, &b_piplus2_Dplus_RichAboveElThres);
   fChain->SetBranchAddress("piplus2_Dplus_RichAboveMuThres", &piplus2_Dplus_RichAboveMuThres, &b_piplus2_Dplus_RichAboveMuThres);
   fChain->SetBranchAddress("piplus2_Dplus_RichAbovePiThres", &piplus2_Dplus_RichAbovePiThres, &b_piplus2_Dplus_RichAbovePiThres);
   fChain->SetBranchAddress("piplus2_Dplus_RichAboveKaThres", &piplus2_Dplus_RichAboveKaThres, &b_piplus2_Dplus_RichAboveKaThres);
   fChain->SetBranchAddress("piplus2_Dplus_RichAbovePrThres", &piplus2_Dplus_RichAbovePrThres, &b_piplus2_Dplus_RichAbovePrThres);
   fChain->SetBranchAddress("piplus2_Dplus_hasCalo", &piplus2_Dplus_hasCalo, &b_piplus2_Dplus_hasCalo);
   fChain->SetBranchAddress("piplus2_Dplus_L0Global_Dec", &piplus2_Dplus_L0Global_Dec, &b_piplus2_Dplus_L0Global_Dec);
   fChain->SetBranchAddress("piplus2_Dplus_L0Global_TIS", &piplus2_Dplus_L0Global_TIS, &b_piplus2_Dplus_L0Global_TIS);
   fChain->SetBranchAddress("piplus2_Dplus_L0Global_TOS", &piplus2_Dplus_L0Global_TOS, &b_piplus2_Dplus_L0Global_TOS);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt1Global_Dec", &piplus2_Dplus_Hlt1Global_Dec, &b_piplus2_Dplus_Hlt1Global_Dec);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt1Global_TIS", &piplus2_Dplus_Hlt1Global_TIS, &b_piplus2_Dplus_Hlt1Global_TIS);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt1Global_TOS", &piplus2_Dplus_Hlt1Global_TOS, &b_piplus2_Dplus_Hlt1Global_TOS);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt1Phys_Dec", &piplus2_Dplus_Hlt1Phys_Dec, &b_piplus2_Dplus_Hlt1Phys_Dec);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt1Phys_TIS", &piplus2_Dplus_Hlt1Phys_TIS, &b_piplus2_Dplus_Hlt1Phys_TIS);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt1Phys_TOS", &piplus2_Dplus_Hlt1Phys_TOS, &b_piplus2_Dplus_Hlt1Phys_TOS);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt2Global_Dec", &piplus2_Dplus_Hlt2Global_Dec, &b_piplus2_Dplus_Hlt2Global_Dec);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt2Global_TIS", &piplus2_Dplus_Hlt2Global_TIS, &b_piplus2_Dplus_Hlt2Global_TIS);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt2Global_TOS", &piplus2_Dplus_Hlt2Global_TOS, &b_piplus2_Dplus_Hlt2Global_TOS);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt2Phys_Dec", &piplus2_Dplus_Hlt2Phys_Dec, &b_piplus2_Dplus_Hlt2Phys_Dec);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt2Phys_TIS", &piplus2_Dplus_Hlt2Phys_TIS, &b_piplus2_Dplus_Hlt2Phys_TIS);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt2Phys_TOS", &piplus2_Dplus_Hlt2Phys_TOS, &b_piplus2_Dplus_Hlt2Phys_TOS);
   fChain->SetBranchAddress("piplus2_Dplus_L0HadronDecision_Dec", &piplus2_Dplus_L0HadronDecision_Dec, &b_piplus2_Dplus_L0HadronDecision_Dec);
   fChain->SetBranchAddress("piplus2_Dplus_L0HadronDecision_TIS", &piplus2_Dplus_L0HadronDecision_TIS, &b_piplus2_Dplus_L0HadronDecision_TIS);
   fChain->SetBranchAddress("piplus2_Dplus_L0HadronDecision_TOS", &piplus2_Dplus_L0HadronDecision_TOS, &b_piplus2_Dplus_L0HadronDecision_TOS);
   fChain->SetBranchAddress("piplus2_Dplus_L0ElectronDecision_Dec", &piplus2_Dplus_L0ElectronDecision_Dec, &b_piplus2_Dplus_L0ElectronDecision_Dec);
   fChain->SetBranchAddress("piplus2_Dplus_L0ElectronDecision_TIS", &piplus2_Dplus_L0ElectronDecision_TIS, &b_piplus2_Dplus_L0ElectronDecision_TIS);
   fChain->SetBranchAddress("piplus2_Dplus_L0ElectronDecision_TOS", &piplus2_Dplus_L0ElectronDecision_TOS, &b_piplus2_Dplus_L0ElectronDecision_TOS);
   fChain->SetBranchAddress("piplus2_Dplus_L0ElectronHiDecision_Dec", &piplus2_Dplus_L0ElectronHiDecision_Dec, &b_piplus2_Dplus_L0ElectronHiDecision_Dec);
   fChain->SetBranchAddress("piplus2_Dplus_L0ElectronHiDecision_TIS", &piplus2_Dplus_L0ElectronHiDecision_TIS, &b_piplus2_Dplus_L0ElectronHiDecision_TIS);
   fChain->SetBranchAddress("piplus2_Dplus_L0ElectronHiDecision_TOS", &piplus2_Dplus_L0ElectronHiDecision_TOS, &b_piplus2_Dplus_L0ElectronHiDecision_TOS);
   fChain->SetBranchAddress("piplus2_Dplus_L0MuonDecision_Dec", &piplus2_Dplus_L0MuonDecision_Dec, &b_piplus2_Dplus_L0MuonDecision_Dec);
   fChain->SetBranchAddress("piplus2_Dplus_L0MuonDecision_TIS", &piplus2_Dplus_L0MuonDecision_TIS, &b_piplus2_Dplus_L0MuonDecision_TIS);
   fChain->SetBranchAddress("piplus2_Dplus_L0MuonDecision_TOS", &piplus2_Dplus_L0MuonDecision_TOS, &b_piplus2_Dplus_L0MuonDecision_TOS);
   fChain->SetBranchAddress("piplus2_Dplus_L0DiMuonDecision_Dec", &piplus2_Dplus_L0DiMuonDecision_Dec, &b_piplus2_Dplus_L0DiMuonDecision_Dec);
   fChain->SetBranchAddress("piplus2_Dplus_L0DiMuonDecision_TIS", &piplus2_Dplus_L0DiMuonDecision_TIS, &b_piplus2_Dplus_L0DiMuonDecision_TIS);
   fChain->SetBranchAddress("piplus2_Dplus_L0DiMuonDecision_TOS", &piplus2_Dplus_L0DiMuonDecision_TOS, &b_piplus2_Dplus_L0DiMuonDecision_TOS);
   fChain->SetBranchAddress("piplus2_Dplus_L0MuonHighDecision_Dec", &piplus2_Dplus_L0MuonHighDecision_Dec, &b_piplus2_Dplus_L0MuonHighDecision_Dec);
   fChain->SetBranchAddress("piplus2_Dplus_L0MuonHighDecision_TIS", &piplus2_Dplus_L0MuonHighDecision_TIS, &b_piplus2_Dplus_L0MuonHighDecision_TIS);
   fChain->SetBranchAddress("piplus2_Dplus_L0MuonHighDecision_TOS", &piplus2_Dplus_L0MuonHighDecision_TOS, &b_piplus2_Dplus_L0MuonHighDecision_TOS);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt1TrackMVADecision_Dec", &piplus2_Dplus_Hlt1TrackMVADecision_Dec, &b_piplus2_Dplus_Hlt1TrackMVADecision_Dec);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt1TrackMVADecision_TIS", &piplus2_Dplus_Hlt1TrackMVADecision_TIS, &b_piplus2_Dplus_Hlt1TrackMVADecision_TIS);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt1TrackMVADecision_TOS", &piplus2_Dplus_Hlt1TrackMVADecision_TOS, &b_piplus2_Dplus_Hlt1TrackMVADecision_TOS);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt1TwoTrackMVADecision_Dec", &piplus2_Dplus_Hlt1TwoTrackMVADecision_Dec, &b_piplus2_Dplus_Hlt1TwoTrackMVADecision_Dec);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt1TwoTrackMVADecision_TIS", &piplus2_Dplus_Hlt1TwoTrackMVADecision_TIS, &b_piplus2_Dplus_Hlt1TwoTrackMVADecision_TIS);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt1TwoTrackMVADecision_TOS", &piplus2_Dplus_Hlt1TwoTrackMVADecision_TOS, &b_piplus2_Dplus_Hlt1TwoTrackMVADecision_TOS);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt1GlobalDecision_Dec", &piplus2_Dplus_Hlt1GlobalDecision_Dec, &b_piplus2_Dplus_Hlt1GlobalDecision_Dec);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt1GlobalDecision_TIS", &piplus2_Dplus_Hlt1GlobalDecision_TIS, &b_piplus2_Dplus_Hlt1GlobalDecision_TIS);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt1GlobalDecision_TOS", &piplus2_Dplus_Hlt1GlobalDecision_TOS, &b_piplus2_Dplus_Hlt1GlobalDecision_TOS);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt1TrackAllL0Decision_Dec", &piplus2_Dplus_Hlt1TrackAllL0Decision_Dec, &b_piplus2_Dplus_Hlt1TrackAllL0Decision_Dec);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt1TrackAllL0Decision_TIS", &piplus2_Dplus_Hlt1TrackAllL0Decision_TIS, &b_piplus2_Dplus_Hlt1TrackAllL0Decision_TIS);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt1TrackAllL0Decision_TOS", &piplus2_Dplus_Hlt1TrackAllL0Decision_TOS, &b_piplus2_Dplus_Hlt1TrackAllL0Decision_TOS);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt2Topo2BodyBBDTDecision_Dec", &piplus2_Dplus_Hlt2Topo2BodyBBDTDecision_Dec, &b_piplus2_Dplus_Hlt2Topo2BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt2Topo2BodyBBDTDecision_TIS", &piplus2_Dplus_Hlt2Topo2BodyBBDTDecision_TIS, &b_piplus2_Dplus_Hlt2Topo2BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt2Topo2BodyBBDTDecision_TOS", &piplus2_Dplus_Hlt2Topo2BodyBBDTDecision_TOS, &b_piplus2_Dplus_Hlt2Topo2BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt2Topo3BodyBBDTDecision_Dec", &piplus2_Dplus_Hlt2Topo3BodyBBDTDecision_Dec, &b_piplus2_Dplus_Hlt2Topo3BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt2Topo3BodyBBDTDecision_TIS", &piplus2_Dplus_Hlt2Topo3BodyBBDTDecision_TIS, &b_piplus2_Dplus_Hlt2Topo3BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt2Topo3BodyBBDTDecision_TOS", &piplus2_Dplus_Hlt2Topo3BodyBBDTDecision_TOS, &b_piplus2_Dplus_Hlt2Topo3BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt2Topo4BodyBBDTDecision_Dec", &piplus2_Dplus_Hlt2Topo4BodyBBDTDecision_Dec, &b_piplus2_Dplus_Hlt2Topo4BodyBBDTDecision_Dec);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt2Topo4BodyBBDTDecision_TIS", &piplus2_Dplus_Hlt2Topo4BodyBBDTDecision_TIS, &b_piplus2_Dplus_Hlt2Topo4BodyBBDTDecision_TIS);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt2Topo4BodyBBDTDecision_TOS", &piplus2_Dplus_Hlt2Topo4BodyBBDTDecision_TOS, &b_piplus2_Dplus_Hlt2Topo4BodyBBDTDecision_TOS);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt2BHadB02PpPpPmPmDecision_Dec", &piplus2_Dplus_Hlt2BHadB02PpPpPmPmDecision_Dec, &b_piplus2_Dplus_Hlt2BHadB02PpPpPmPmDecision_Dec);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt2BHadB02PpPpPmPmDecision_TIS", &piplus2_Dplus_Hlt2BHadB02PpPpPmPmDecision_TIS, &b_piplus2_Dplus_Hlt2BHadB02PpPpPmPmDecision_TIS);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt2BHadB02PpPpPmPmDecision_TOS", &piplus2_Dplus_Hlt2BHadB02PpPpPmPmDecision_TOS, &b_piplus2_Dplus_Hlt2BHadB02PpPpPmPmDecision_TOS);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt2Topo2BodyDecision_Dec", &piplus2_Dplus_Hlt2Topo2BodyDecision_Dec, &b_piplus2_Dplus_Hlt2Topo2BodyDecision_Dec);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt2Topo2BodyDecision_TIS", &piplus2_Dplus_Hlt2Topo2BodyDecision_TIS, &b_piplus2_Dplus_Hlt2Topo2BodyDecision_TIS);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt2Topo2BodyDecision_TOS", &piplus2_Dplus_Hlt2Topo2BodyDecision_TOS, &b_piplus2_Dplus_Hlt2Topo2BodyDecision_TOS);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt2Topo3BodyDecision_Dec", &piplus2_Dplus_Hlt2Topo3BodyDecision_Dec, &b_piplus2_Dplus_Hlt2Topo3BodyDecision_Dec);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt2Topo3BodyDecision_TIS", &piplus2_Dplus_Hlt2Topo3BodyDecision_TIS, &b_piplus2_Dplus_Hlt2Topo3BodyDecision_TIS);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt2Topo3BodyDecision_TOS", &piplus2_Dplus_Hlt2Topo3BodyDecision_TOS, &b_piplus2_Dplus_Hlt2Topo3BodyDecision_TOS);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt2Topo4BodyDecision_Dec", &piplus2_Dplus_Hlt2Topo4BodyDecision_Dec, &b_piplus2_Dplus_Hlt2Topo4BodyDecision_Dec);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt2Topo4BodyDecision_TIS", &piplus2_Dplus_Hlt2Topo4BodyDecision_TIS, &b_piplus2_Dplus_Hlt2Topo4BodyDecision_TIS);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt2Topo4BodyDecision_TOS", &piplus2_Dplus_Hlt2Topo4BodyDecision_TOS, &b_piplus2_Dplus_Hlt2Topo4BodyDecision_TOS);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt2GlobalDecision_Dec", &piplus2_Dplus_Hlt2GlobalDecision_Dec, &b_piplus2_Dplus_Hlt2GlobalDecision_Dec);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt2GlobalDecision_TIS", &piplus2_Dplus_Hlt2GlobalDecision_TIS, &b_piplus2_Dplus_Hlt2GlobalDecision_TIS);
   fChain->SetBranchAddress("piplus2_Dplus_Hlt2GlobalDecision_TOS", &piplus2_Dplus_Hlt2GlobalDecision_TOS, &b_piplus2_Dplus_Hlt2GlobalDecision_TOS);
   fChain->SetBranchAddress("piplus2_Dplus_TRACK_Type", &piplus2_Dplus_TRACK_Type, &b_piplus2_Dplus_TRACK_Type);
   fChain->SetBranchAddress("piplus2_Dplus_TRACK_Key", &piplus2_Dplus_TRACK_Key, &b_piplus2_Dplus_TRACK_Key);
   fChain->SetBranchAddress("piplus2_Dplus_TRACK_CHI2NDOF", &piplus2_Dplus_TRACK_CHI2NDOF, &b_piplus2_Dplus_TRACK_CHI2NDOF);
   fChain->SetBranchAddress("piplus2_Dplus_TRACK_PCHI2", &piplus2_Dplus_TRACK_PCHI2, &b_piplus2_Dplus_TRACK_PCHI2);
   fChain->SetBranchAddress("piplus2_Dplus_TRACK_MatchCHI2", &piplus2_Dplus_TRACK_MatchCHI2, &b_piplus2_Dplus_TRACK_MatchCHI2);
   fChain->SetBranchAddress("piplus2_Dplus_TRACK_GhostProb", &piplus2_Dplus_TRACK_GhostProb, &b_piplus2_Dplus_TRACK_GhostProb);
   fChain->SetBranchAddress("piplus2_Dplus_TRACK_CloneDist", &piplus2_Dplus_TRACK_CloneDist, &b_piplus2_Dplus_TRACK_CloneDist);
   fChain->SetBranchAddress("piplus2_Dplus_TRACK_Likelihood", &piplus2_Dplus_TRACK_Likelihood, &b_piplus2_Dplus_TRACK_Likelihood);
   fChain->SetBranchAddress("piplus2_Dplus_X", &piplus2_Dplus_X, &b_piplus2_Dplus_X);
   fChain->SetBranchAddress("piplus2_Dplus_Y", &piplus2_Dplus_Y, &b_piplus2_Dplus_Y);
   fChain->SetBranchAddress("nCandidate", &nCandidate, &b_nCandidate);
   fChain->SetBranchAddress("totCandidates", &totCandidates, &b_totCandidates);
   fChain->SetBranchAddress("EventInSequence", &EventInSequence, &b_EventInSequence);
   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("BCID", &BCID, &b_BCID);
   fChain->SetBranchAddress("BCType", &BCType, &b_BCType);
   fChain->SetBranchAddress("OdinTCK", &OdinTCK, &b_OdinTCK);
   fChain->SetBranchAddress("L0DUTCK", &L0DUTCK, &b_L0DUTCK);
   fChain->SetBranchAddress("HLT1TCK", &HLT1TCK, &b_HLT1TCK);
   fChain->SetBranchAddress("HLT2TCK", &HLT2TCK, &b_HLT2TCK);
   fChain->SetBranchAddress("GpsTime", &GpsTime, &b_GpsTime);
   fChain->SetBranchAddress("Polarity", &Polarity, &b_Polarity);
   fChain->SetBranchAddress("L0Data_DiMuon_Pt", &L0Data_DiMuon_Pt, &b_L0Data_DiMuon_Pt);
   fChain->SetBranchAddress("L0Data_DiMuonProd_Pt1Pt2", &L0Data_DiMuonProd_Pt1Pt2, &b_L0Data_DiMuonProd_Pt1Pt2);
   fChain->SetBranchAddress("L0Data_Electron_Et", &L0Data_Electron_Et, &b_L0Data_Electron_Et);
   fChain->SetBranchAddress("L0Data_GlobalPi0_Et", &L0Data_GlobalPi0_Et, &b_L0Data_GlobalPi0_Et);
   fChain->SetBranchAddress("L0Data_Hadron_Et", &L0Data_Hadron_Et, &b_L0Data_Hadron_Et);
   fChain->SetBranchAddress("L0Data_LocalPi0_Et", &L0Data_LocalPi0_Et, &b_L0Data_LocalPi0_Et);
   fChain->SetBranchAddress("L0Data_Muon1_Pt", &L0Data_Muon1_Pt, &b_L0Data_Muon1_Pt);
   fChain->SetBranchAddress("L0Data_Muon1_Sgn", &L0Data_Muon1_Sgn, &b_L0Data_Muon1_Sgn);
   fChain->SetBranchAddress("L0Data_Muon2_Pt", &L0Data_Muon2_Pt, &b_L0Data_Muon2_Pt);
   fChain->SetBranchAddress("L0Data_Muon2_Sgn", &L0Data_Muon2_Sgn, &b_L0Data_Muon2_Sgn);
   fChain->SetBranchAddress("L0Data_Muon3_Pt", &L0Data_Muon3_Pt, &b_L0Data_Muon3_Pt);
   fChain->SetBranchAddress("L0Data_Muon3_Sgn", &L0Data_Muon3_Sgn, &b_L0Data_Muon3_Sgn);
   fChain->SetBranchAddress("L0Data_PUHits_Mult", &L0Data_PUHits_Mult, &b_L0Data_PUHits_Mult);
   fChain->SetBranchAddress("L0Data_PUPeak1_Cont", &L0Data_PUPeak1_Cont, &b_L0Data_PUPeak1_Cont);
   fChain->SetBranchAddress("L0Data_PUPeak1_Pos", &L0Data_PUPeak1_Pos, &b_L0Data_PUPeak1_Pos);
   fChain->SetBranchAddress("L0Data_PUPeak2_Cont", &L0Data_PUPeak2_Cont, &b_L0Data_PUPeak2_Cont);
   fChain->SetBranchAddress("L0Data_PUPeak2_Pos", &L0Data_PUPeak2_Pos, &b_L0Data_PUPeak2_Pos);
   fChain->SetBranchAddress("L0Data_Photon_Et", &L0Data_Photon_Et, &b_L0Data_Photon_Et);
   fChain->SetBranchAddress("L0Data_Spd_Mult", &L0Data_Spd_Mult, &b_L0Data_Spd_Mult);
   fChain->SetBranchAddress("L0Data_Sum_Et", &L0Data_Sum_Et, &b_L0Data_Sum_Et);
   fChain->SetBranchAddress("L0Data_Sum_Et,Next1", &L0Data_Sum_Et_Next1, &b_L0Data_Sum_Et_Next1);
   fChain->SetBranchAddress("L0Data_Sum_Et,Next2", &L0Data_Sum_Et_Next2, &b_L0Data_Sum_Et_Next2);
   fChain->SetBranchAddress("L0Data_Sum_Et,Prev1", &L0Data_Sum_Et_Prev1, &b_L0Data_Sum_Et_Prev1);
   fChain->SetBranchAddress("L0Data_Sum_Et,Prev2", &L0Data_Sum_Et_Prev2, &b_L0Data_Sum_Et_Prev2);
   fChain->SetBranchAddress("nPV", &nPV, &b_nPV);
   fChain->SetBranchAddress("PVX", PVX, &b_PVX);
   fChain->SetBranchAddress("PVY", PVY, &b_PVY);
   fChain->SetBranchAddress("PVZ", PVZ, &b_PVZ);
   fChain->SetBranchAddress("PVXERR", PVXERR, &b_PVXERR);
   fChain->SetBranchAddress("PVYERR", PVYERR, &b_PVYERR);
   fChain->SetBranchAddress("PVZERR", PVZERR, &b_PVZERR);
   fChain->SetBranchAddress("PVCHI2", PVCHI2, &b_PVCHI2);
   fChain->SetBranchAddress("PVNDOF", PVNDOF, &b_PVNDOF);
   fChain->SetBranchAddress("PVNTRACKS", PVNTRACKS, &b_PVNTRACKS);
   fChain->SetBranchAddress("nPVs", &nPVs, &b_nPVs);
   fChain->SetBranchAddress("nTracks", &nTracks, &b_nTracks);
   fChain->SetBranchAddress("nLongTracks", &nLongTracks, &b_nLongTracks);
   fChain->SetBranchAddress("nDownstreamTracks", &nDownstreamTracks, &b_nDownstreamTracks);
   fChain->SetBranchAddress("nUpstreamTracks", &nUpstreamTracks, &b_nUpstreamTracks);
   fChain->SetBranchAddress("nVeloTracks", &nVeloTracks, &b_nVeloTracks);
   fChain->SetBranchAddress("nTTracks", &nTTracks, &b_nTTracks);
   fChain->SetBranchAddress("nBackTracks", &nBackTracks, &b_nBackTracks);
   fChain->SetBranchAddress("nRich1Hits", &nRich1Hits, &b_nRich1Hits);
   fChain->SetBranchAddress("nRich2Hits", &nRich2Hits, &b_nRich2Hits);
   fChain->SetBranchAddress("nVeloClusters", &nVeloClusters, &b_nVeloClusters);
   fChain->SetBranchAddress("nITClusters", &nITClusters, &b_nITClusters);
   fChain->SetBranchAddress("nTTClusters", &nTTClusters, &b_nTTClusters);
   fChain->SetBranchAddress("nOTClusters", &nOTClusters, &b_nOTClusters);
   fChain->SetBranchAddress("nSPDHits", &nSPDHits, &b_nSPDHits);
   fChain->SetBranchAddress("nMuonCoordsS0", &nMuonCoordsS0, &b_nMuonCoordsS0);
   fChain->SetBranchAddress("nMuonCoordsS1", &nMuonCoordsS1, &b_nMuonCoordsS1);
   fChain->SetBranchAddress("nMuonCoordsS2", &nMuonCoordsS2, &b_nMuonCoordsS2);
   fChain->SetBranchAddress("nMuonCoordsS3", &nMuonCoordsS3, &b_nMuonCoordsS3);
   fChain->SetBranchAddress("nMuonCoordsS4", &nMuonCoordsS4, &b_nMuonCoordsS4);
   fChain->SetBranchAddress("nMuonTracks", &nMuonTracks, &b_nMuonTracks);
   fChain->SetBranchAddress("year", &year, &b_year);
   fChain->SetBranchAddress("magFlag", &magFlag, &b_magFlag);
   fChain->SetBranchAddress("rdmNumber", &rdmNumber, &b_rdmNumber);
   fChain->SetBranchAddress("m_Dsminus_pi2K", &m_Dsminus_pi2K, &b_m_Dsminus_pi2K);
   fChain->SetBranchAddress("m_Dsminus_p2K", &m_Dsminus_p2K, &b_m_Dsminus_p2K);
   fChain->SetBranchAddress("m_KK_Dsminus", &m_KK_Dsminus, &b_m_KK_Dsminus);
   fChain->SetBranchAddress("m_Dplus_K2pi1", &m_Dplus_K2pi1, &b_m_Dplus_K2pi1);
   fChain->SetBranchAddress("m_Dplus_K2pi2", &m_Dplus_K2pi2, &b_m_Dplus_K2pi2);
   fChain->SetBranchAddress("m_Dplus_p2pi1", &m_Dplus_p2pi1, &b_m_Dplus_p2pi1);
   fChain->SetBranchAddress("m_Dplus_p2pi2", &m_Dplus_p2pi2, &b_m_Dplus_p2pi2);
   fChain->SetBranchAddress("m_KK_Dplus_K2pi1", &m_KK_Dplus_K2pi1, &b_m_KK_Dplus_K2pi1);
   fChain->SetBranchAddress("m_KK_Dplus_K2pi2", &m_KK_Dplus_K2pi2, &b_m_KK_Dplus_K2pi2);
   fChain->SetBranchAddress("Kplus_Dsminus_MC15TuneV1_ProbNNk_corr", &Kplus_Dsminus_MC15TuneV1_ProbNNk_corr, &b_Kplus_Dsminus_MC15TuneV1_ProbNNk_corr);
   fChain->SetBranchAddress("Kminus_Dsminus_MC15TuneV1_ProbNNk_corr", &Kminus_Dsminus_MC15TuneV1_ProbNNk_corr, &b_Kminus_Dsminus_MC15TuneV1_ProbNNk_corr);
   fChain->SetBranchAddress("piminus_Dsminus_MC15TuneV1_ProbNNpi_corr", &piminus_Dsminus_MC15TuneV1_ProbNNpi_corr, &b_piminus_Dsminus_MC15TuneV1_ProbNNpi_corr);
   fChain->SetBranchAddress("Kminus_Dplus_MC15TuneV1_ProbNNk_corr", &Kminus_Dplus_MC15TuneV1_ProbNNk_corr, &b_Kminus_Dplus_MC15TuneV1_ProbNNk_corr);
   fChain->SetBranchAddress("piplus1_Dplus_MC15TuneV1_ProbNNpi_corr", &piplus1_Dplus_MC15TuneV1_ProbNNpi_corr, &b_piplus1_Dplus_MC15TuneV1_ProbNNpi_corr);
   fChain->SetBranchAddress("piplus2_Dplus_MC15TuneV1_ProbNNpi_corr", &piplus2_Dplus_MC15TuneV1_ProbNNpi_corr, &b_piplus2_Dplus_MC15TuneV1_ProbNNpi_corr);
   fChain->SetBranchAddress("Kplus_Dsminus_PIDK_corr", &Kplus_Dsminus_PIDK_corr, &b_Kplus_Dsminus_PIDK_corr);
   fChain->SetBranchAddress("Kminus_Dsminus_PIDK_corr", &Kminus_Dsminus_PIDK_corr, &b_Kminus_Dsminus_PIDK_corr);
   fChain->SetBranchAddress("Kminus_Dplus_PIDK_corr", &Kminus_Dplus_PIDK_corr, &b_Kminus_Dplus_PIDK_corr);
   fChain->SetBranchAddress("angle_Kminus_Dplus_piplus1_Dplus", &angle_Kminus_Dplus_piplus1_Dplus, &b_angle_Kminus_Dplus_piplus1_Dplus);
   fChain->SetBranchAddress("angle_Kminus_Dplus_piplus2_Dplus", &angle_Kminus_Dplus_piplus2_Dplus, &b_angle_Kminus_Dplus_piplus2_Dplus);
   fChain->SetBranchAddress("angle_piplus2_Dplus_piplus1_Dplus", &angle_piplus2_Dplus_piplus1_Dplus, &b_angle_piplus2_Dplus_piplus1_Dplus);
   fChain->SetBranchAddress("angle_Kplus_Dsminus_Kminus_Dsminus", &angle_Kplus_Dsminus_Kminus_Dsminus, &b_angle_Kplus_Dsminus_Kminus_Dsminus);
   fChain->SetBranchAddress("angle_Kplus_Dsminus_piminus_Dsminus", &angle_Kplus_Dsminus_piminus_Dsminus, &b_angle_Kplus_Dsminus_piminus_Dsminus);
   fChain->SetBranchAddress("angle_piminus_Dsminus_Kminus_Dsminus", &angle_piminus_Dsminus_Kminus_Dsminus, &b_angle_piminus_Dsminus_Kminus_Dsminus);
   fChain->SetBranchAddress("angle_Kminus_Dplus_Kplus_Dsminus", &angle_Kminus_Dplus_Kplus_Dsminus, &b_angle_Kminus_Dplus_Kplus_Dsminus);
   fChain->SetBranchAddress("angle_Kminus_Dplus_Kminus_Dsminus", &angle_Kminus_Dplus_Kminus_Dsminus, &b_angle_Kminus_Dplus_Kminus_Dsminus);
   fChain->SetBranchAddress("angle_Kminus_Dplus_piminus_Dsminus", &angle_Kminus_Dplus_piminus_Dsminus, &b_angle_Kminus_Dplus_piminus_Dsminus);
   fChain->SetBranchAddress("angle_piplus1_Dplus_Kplus_Dsminus", &angle_piplus1_Dplus_Kplus_Dsminus, &b_angle_piplus1_Dplus_Kplus_Dsminus);
   fChain->SetBranchAddress("angle_piplus1_Dplus_Kminus_Dsminus", &angle_piplus1_Dplus_Kminus_Dsminus, &b_angle_piplus1_Dplus_Kminus_Dsminus);
   fChain->SetBranchAddress("angle_piplus1_Dplus_piminus_Dsminus", &angle_piplus1_Dplus_piminus_Dsminus, &b_angle_piplus1_Dplus_piminus_Dsminus);
   fChain->SetBranchAddress("angle_piplus2_Dplus_Kplus_Dsminus", &angle_piplus2_Dplus_Kplus_Dsminus, &b_angle_piplus2_Dplus_Kplus_Dsminus);
   fChain->SetBranchAddress("angle_piplus2_Dplus_Kminus_Dsminus", &angle_piplus2_Dplus_Kminus_Dsminus, &b_angle_piplus2_Dplus_Kminus_Dsminus);
   fChain->SetBranchAddress("angle_piplus2_Dplus_piminus_Dsminus", &angle_piplus2_Dplus_piminus_Dsminus, &b_angle_piplus2_Dplus_piminus_Dsminus);
   fChain->SetBranchAddress("prodProbNNx", &prodProbNNx, &b_prodProbNNx);
   fChain->SetBranchAddress("B_DTFM_M_0", &B_DTFM_M_0, &b_B_DTFM_M_0);
   Notify();
}

Bool_t DecayTree::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void DecayTree::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t DecayTree::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef DecayTree_cxx
